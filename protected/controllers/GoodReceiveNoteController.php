<?php
/*********************************************************
        -*- File: GoodReceiveNoteController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.02.06
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class GoodReceiveNoteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = $printId = '';
		$model=new GoodReceiveNote;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['GoodReceiveNote']))
		{
            
			$model->attributes=$_POST['GoodReceiveNote'];
			$model->supplierId=$_POST['supplierId'];
			$poNo = $_POST['GoodReceiveNote']['poId'];
			$poModel = PurchaseOrder::model()->find('poNo like :poNo',array(':poNo'=>$poNo));
			if(!empty($poModel)) $model->poId = $poModel->id;
			
            if($model->save()) 
			{
				// processing stock details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) : if($qty>0) :
						$modelStock = new Stock;
						$modelStock->grnId = $model->id;
						$modelStock->qty = $qty;
						$modelStock->itemId = $_POST['proId'][$_POST['pk'][$key]];		
						$modelStock->costPrice = $_POST['costPrice'][$_POST['pk'][$key]];
						$modelStock->discountPercent = $_POST['discount'][$_POST['pk'][$key]];
						$modelStock->save();
					 endif; endforeach;	
				endif;
				$printId = $model->id;
				
				$msg = "Good Receive Note Saved Successfully";
                $model=new GoodReceiveNote;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
			'printId'=>$printId,
		));
	}

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionDirectGrn()
    {
        $msg = $printId = '';
        $model = new PurchaseOrder;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['PurchaseOrder']))
        {
            $model->attributes=$_POST['PurchaseOrder'];
            $model->status = PurchaseOrder::STATUS_AUTO_GRN;
            if($model->save())
            {
                // processing purchase order details
                if(isset($_POST['qty'])) :
                    foreach($_POST['qty'] as $key=>$qty) :
                        if($qty>0) :
                            $modelPoDetails = new PoDetails;
                            $modelPoDetails->poId = $model->id;
                            $modelPoDetails->qty = $qty;
                            $modelPoDetails->itemId = $_POST['proId'][$_POST['pk'][$key]];
                            $modelPoDetails->costPrice = $_POST['costPrice'][$_POST['pk'][$key]];
                            $modelPoDetails->currentStock = $_POST['currentStock'][$_POST['pk'][$key]];
                            $modelPoDetails->soldQty = $_POST['soldQty'][$_POST['pk'][$key]];
                            $modelPoDetails->save();
                        endif;
                    endforeach;
                endif;

                // good receive note processing
                $modelGrn = new GoodReceiveNote;
                $modelGrn->supplierId=$_POST['PurchaseOrder']['supplierId'];
                $modelGrn->poId = $model->id;
                $modelGrn->grnNo = $_POST['grnNo'];
                $modelGrn->totalQty = $_POST['PurchaseOrder']['totalQty'];
                $modelGrn->totalWeight = $_POST['PurchaseOrder']['totalWeight'];
                $modelGrn->totalPrice = $_POST['PurchaseOrder']['totalPrice'];
                $modelGrn->totalDiscount = GoodReceiveNote::AUTO_GRN_DISCOUNT_AMOUNT;
                if($modelGrn->save())
                {
                    // processing stock details
                    if(isset($_POST['qty'])) :
                        foreach($_POST['qty'] as $key=>$qty) : if($qty>0) :
                            $modelStock = new Stock;
                            $modelStock->grnId = $modelGrn->id;
                            $modelStock->qty = $qty;
                            $modelStock->itemId = $_POST['proId'][$_POST['pk'][$key]];
                            $modelStock->costPrice = $_POST['costPrice'][$_POST['pk'][$key]];
                            $modelStock->discountPercent = GoodReceiveNote::AUTO_GRN_DISCOUNT_AMOUNT;
                            $modelStock->save();
                        endif; endforeach;
                    endif;
                    $printId = $modelGrn->id;
                    $msg = "Good Receive Note Saved Successfully";
                    $model=new GoodReceiveNote;
                }
            }
        }
        $this->render('directGrn',array(
            'model'=>$model,
            'printId'=>$printId,
            'msg'=>$msg,
        ));
    }
	
	/**
	 * Creates a new model for GRN Approval/ Purchase Invoice.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionGrnApprove()
	{
    	$msg = '';
		$todayGrn = NULL;
		$today = date("Y-m-d");
		$model=new GoodReceiveNote;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		// current day grn without approval
		$todayGrn = GoodReceiveNote::model()->findAll('status like :status',array(':status'=>GoodReceiveNote::STATUS_ACTIVE));
		//$todayGrn = GoodReceiveNote::model()->findAll('grnDate like :grnDate and status like :status',array(':grnDate'=>"$today%", ':status'=>GoodReceiveNote::STATUS_ACTIVE));
		
		if(isset($_POST['grnId']))
		{
			// grn approved process
			foreach($_POST['grnId'] as $key=>$grnId)
			{	
				$checkedAtt = "checkedid".$key;
				
				if(isset($_POST[$checkedAtt])!=NULL) // checked
				{											
					$grn = GoodReceiveNote::model()->findByPk($grnId);
					$stockModel = array();
					$subdept = InvSettings::getAllActiveSubDept();

					if(!empty($subdept)) 
					{
						$sqlGrn = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND
								   c.subdeptId IN(".$subdept.") AND s.grnId=".$grnId." AND s.status=".Stock::STATUS_ACTIVE." ORDER BY s.stockDate DESC";	 
						$stockModel = Stock::model()->findBySql($sqlGrn);
					}
					
					// inventory not initialize
					if(empty($stockModel))
					{
						$criteria = new CDbCriteria;
						$criteria->addCondition('grnId='.$grnId); //$criteria->addInCondition("wall_id" , $wall_ids ) ; $wall_ids = array ( 1, 2, 3, 4 );
						$stockUpd = Stock::model()->updateAll(array('status' => Stock::STATUS_APPROVED,'stockDate'=>date("Y-m-d H:i:s")), $criteria); 
						if($stockUpd) :
							// grnInformations & update
							$grnModel = GoodReceiveNote::model()->findByPk($grnId);
							$criteriagrn = new CDbCriteria;
							$criteriagrn->addCondition('id='.$grnId);
							$grnUpd = GoodReceiveNote::model()->updateAll(array('status' => GoodReceiveNote::STATUS_APPROVED), $criteriagrn);
							
							// supplier wise deposit insert
							if($grnUpd) :
								$supModel = new SupplierDeposit;
								$supModel->supId =  $grnModel->supplierId;
								$supModel->grnId = $grnId;
								$supModel->amount = $grnModel->totalPrice - $grnModel->totalDiscount;
								$supModel->save();
							endif;
						endif;

                        // parent items processing for chield stock approval
                        $grnItemModel = Stock::model()->findAll($criteria);
                        if(!empty($grnItemModel)) :
                            foreach($grnItemModel as $gKey=>$gData) :
                                // process chield item push in sales invoice and details to decreases parent stock
                                if($gData->item->isParent==Items::IS_PARENTS)
                                {
                                    // processing parent data
                                    $totalQty = $totalWeight = $totalPrice = $totalDiscount = $totalTax = 0;
                                    foreach(ItemParents::getAllItemParentsByChield($gData->itemId,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)
                                    {
                                        if(is_numeric($dataParents->qty)) $totalQty+= $gData->qty*$dataParents->qty;
                                        else $totalWeight+= $gData->qty*$dataParents->qty;

                                        $totalPrice+= $gData->qty*($dataParents->qty*$dataParents->parent->costPrice); // sellPrice
                                        $totalDiscount+= $gData->qty*($dataParents->qty*Items::getItemWiseOfferOriPrice($dataParents->parentId));
                                        $totalTax+= $gData->qty*($dataParents->qty*Items::getItemPriceWithVatOri($dataParents->parentId));
                                    }

                                    // save to sales invoice
                                    $modelSalesInvoice = new SalesInvoice;
                                    $modelSalesInvoice->invNo = 'IN'.date("Ymdhis");
                                    $modelSalesInvoice->custId 	= Customer::DEFAULT_CUST;
                                    $modelSalesInvoice->totalQty 	= $totalQty;
                                    $modelSalesInvoice->totalWeight = $totalWeight;
                                    $modelSalesInvoice->totalPrice 	= $totalPrice;
                                    $modelSalesInvoice->totalDiscount 	= $totalDiscount;
                                    $modelSalesInvoice->totalTax 	= $totalTax;
                                    $modelSalesInvoice->status	= SalesInvoice::STATUS_ACTIVE;
                                    if($modelSalesInvoice->save())
                                    {
                                        // customer cart product insert
                                        foreach(ItemParents::getAllItemParentsByChield($gData->itemId,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)
                                        {
                                            $modelSalesDetails = new SalesDetails;
                                            $modelSalesDetails->salesId = $modelSalesInvoice->id;
                                            $modelSalesDetails->itemId = $dataParents->parentId;
                                            $modelSalesDetails->qty = $gData->qty*$dataParents->qty;
                                            $modelSalesDetails->costPrice = $dataParents->parent->costPrice;
                                            $modelSalesDetails->salesPrice 	= $dataParents->parent->costPrice;
                                            $modelSalesDetails->discountPrice = $gData->qty*($dataParents->qty*Items::getItemWiseOfferOriPrice($dataParents->parentId));
                                            $modelSalesDetails->vatPrice = $gData->qty*($dataParents->qty*Items::getItemPriceWithVatOri($dataParents->parentId));
                                            $modelSalesDetails->status = SalesDetails::STATUS_ACTIVE;
                                            $modelSalesDetails->save();
                                        }
                                    }
                                }
                            endforeach;
                        endif;
						//$msg.= '<div class="notification note-success"><p>Good Receive '.$grn->grnNo.' Approved Successfully</p></div>';
                        $this->refresh();
					}
					else
					{
						$msg.= '<div class="notification note-error"><p>Items of '.$grn->grnNo.' is in Inventory Process !</p></div>';
					}
				}	
			}
		}
		$this->render('_grnapproved',array(
			'model'=>$model,
			'todayGrn'=>$todayGrn,
            'msg'=>$msg,
		));
	}
	
	/*
	 * print particular grn.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionPrintGrn()
	{					
		$msg = $printId = '';
		$model=new GoodReceiveNote;
		if(isset($_POST['GoodReceiveNote']))
		{
			$grnModel = GoodReceiveNote::model()->find('grnNo=:grnNo',array(':grnNo'=>$_POST['GoodReceiveNote']['grnNo']));
			if(!empty($grnModel)) $printId = $grnModel->id;
		}
		
		$this->render('printGrn',array(
			'model'=>$model,
			'printId'=>$printId,
            'msg'=>$msg,
		));
	}

	/*
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
    	$msg = '';
		$model=new GoodReceiveNote;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['GoodReceiveNote']))
		{	
			$model = GoodReceiveNote::model()->find('grnNo=:grnNo',array(':grnNo'=>$_POST['GoodReceiveNote']['grnNo']));
			$grnId = $model->id;
			$model->attributes=$_POST['GoodReceiveNote'];
			
            if($model->save()) 
			{
				// processing stock details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) :
						$sql = "UPDATE pos_stock set qty=".$qty.",discountPercent=".$_POST['discount'][$_POST['pk'][$key]].",stockDate='".date("Y-m-d H:i:s")."' WHERE grnId=".$grnId." AND itemId=".$_POST['proId'][$_POST['pk'][$key]]; 
						$command = Yii::app()->db->createCommand($sql);
						$command->execute();
					endforeach;	
				endif;
				
				$msg = "Good Receive Note Updated Successfully";
                $model=new GoodReceiveNote;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/*
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdateApproved()
	{
    	$msg = '';
		$model=new GoodReceiveNote;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		if(isset($_POST['GoodReceiveNote']))
		{	
			$model = GoodReceiveNote::model()->find('grnNo=:grnNo',array(':grnNo'=>$_POST['GoodReceiveNote']['grnNo']));
			$grnId = $model->id;
			$model->attributes=$_POST['GoodReceiveNote'];
			
			$stockModel = array();
			$subdept = InvSettings::getAllActiveSubDept();
			if(!empty($subdept)) 
			{
				$sqlGrn = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND	
						  c.subdeptId IN(".$subdept.") AND s.grnId=".$grnId." AND s.status=".Stock::STATUS_APPROVED." ORDER BY s.stockDate DESC";
				$stockModel = Stock::model()->findBySql($sqlGrn);
			}
			
			// inventory not initialize
			if(empty($stockModel))
			{
				if($model->save()) 
				{
					// processing stock details
					if(isset($_POST['qty'])) :
						$totalAmount = $totalDiscount = 0;
						foreach($_POST['qty'] as $key=>$qty) :
							$totalAmount+=$qty*$_POST['costPrice'][$_POST['pk'][$key]];
							$totalDiscount+=($qty*$_POST['costPrice'][$_POST['pk'][$key]])*$_POST['discount'][$_POST['pk'][$key]]/100;						
							$sql = "update pos_stock set qty=".$qty.", discountPercent=".$_POST['discount'][$_POST['pk'][$key]].", stockDate='".date("Y-m-d H:i:s")."' where grnId=".$grnId." and itemId=".$_POST['proId'][$_POST['pk'][$key]]; 
							$command = Yii::app()->db->createCommand($sql);
							$command->execute();
							//$stock = Stock::model()->find('grnId=:grnId and itemId=:itemId',array(':grnId' => $model->id, 'itemId'=> $_POST['proId'][$_POST['pk'][$key]]));
							//$stock->update(array('qty' => $qty, 'discountPercent' => $_POST['discount'][$_POST['pk'][$key]], 'stockDate'=>date("Y-m-d H:i:s"))); 
						endforeach;	
						
						// update supplier deposit
						$netAmount = $totalAmount-$totalDiscount;
						$sqldep = "update pos_supplier_deposit set amount=".$netAmount.", moAt='".date("Y-m-d H:i:s")."' 
						           where grnId=".$grnId; 
						$commanddep = Yii::app()->db->createCommand($sqldep);
						$commanddep->execute();
					endif;
					
					$msg = '<div class="notification note-success"><p>Good Receive Note Updated Successfully</p></div>';
					$model=new GoodReceiveNote;
					//$this->redirect(array('view','id'=>$model->id));
				}
			}
			else
				$msg.= '<div class="notification note-error"><p>Items of '.$_POST['GoodReceiveNote']['grnNo'].' is in Inventory Process !</p></div>';
		}
		
		$this->render('updateApproved',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('GoodReceiveNote');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new GoodReceiveNote('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GoodReceiveNote']))
			$model->attributes=$_GET['GoodReceiveNote'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GoodReceiveNote the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=GoodReceiveNote::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GoodReceiveNote $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='good-receive-note-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
