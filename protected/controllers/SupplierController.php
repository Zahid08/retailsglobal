<?php
/*********************************************************
        -*- File: SupplierController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.02.02
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class SupplierController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new Supplier;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Supplier']))
		{
			$model->attributes=$_POST['Supplier'];
            if($model->save()) {
				$msg = "Supplier Saved Successfully";
                $model=new Supplier;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Supplier']))
		{
			$model->attributes=$_POST['Supplier'];
			if($model->save())
            {
				$name =$model->attributes['name'];
				$email = $model->attributes['email'];			
				if($model->status==Customer::STATUS_ACTIVE)
				{
					// Update User Status
					$command = Yii::app()->db
					    ->createCommand("UPDATE pos_user SET status ='".User::STATUS_ACTIVE."'  WHERE supplierId=:supplierId")
					    ->bindValues(array(':supplierId' =>$id))
					    ->execute();

					// get User name 
				    $userData = Yii::app()->db->createCommand()
						    ->select('username')
						    ->from('pos_user')						   
						    ->where('supplierId=:supplierId', array(':supplierId'=>$id))
						    ->queryRow();					

                    $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
                    $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                       array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
                    $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

                    $body = $this->renderPartial('supplierActivationEmail',array(
                              'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
                              'name'=>$name,
                              'userName'=>$userData['username']),true
                    );

                    Yii::app()->mailer->IsHTML(true);
                    Yii::app()->mailer->SMTPAuth = true;
                    Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
                    Yii::app()->mailer->From = $companyModel->email; //'admin@unlockliveretail.com';
                    Yii::app()->mailer->FromName = $companyModel->name;
                    Yii::app()->mailer->AddReplyTo('admin@unlockliveretail.com');
                    Yii::app()->mailer->AddAddress($email);
                    Yii::app()->mailer->Subject = 'Account Activation confirmation from '.$companyModel->name;
                    Yii::app()->mailer->Body = $body;
                    Yii::app()->mailer->Send();
				}
                $msg = "Supplier Updated Successfully";
			}
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Supplier');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new Supplier('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Supplier']))
			$model->attributes=$_GET['Supplier'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Supplier the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Supplier::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Supplier $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='supplier-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
