<?php
/*********************************************************
-*- File: ItemsController.php
-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
-*- Date: 2014.02.02
-*- Position:  protected/controller
-*- YII-*- version 1.1.13
/*********************************************************/

class PosController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
     * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
     */
    public $metaTitle 	 	 = NULL;
    public $metaKeywords 	 = NULL;
    public $metaDescriptions = NULL;
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {

        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Items the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Items::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadVariationModel($id)
    {
        $model=ItemsVariation::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Items $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='items-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
