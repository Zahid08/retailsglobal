<?php
/*********************************************************
        -*- File: SalesInvoiceController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.02.14
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class SalesInvoiceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $orderModel = SalesInvoice::model()->findByPk($id);
      
        $customerModel = Customer::model()->find(array('condition'=>'id=:id','params'=>array(':id'=>$orderModel->custId)));
   
        $orderDetailsModel = SalesDetails::model()->findAll(array('condition'=>'salesId=:salesId','params'=>array(':salesId'=>$id),'order'=>'crAt DESC'));
        $this->render('view',array(
            'companyModel'=>$companyModel,
            'orderModel'=>$orderModel,
            'customerModel'=>$customerModel,
            'orderDetailsModel'=>$orderDetailsModel,
        ));
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionInvoicePreview($id)
    {
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                        array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $orderModel = SalesInvoice::model()->findByPk($id);
        
        $customerModel = Customer::model()->find(array('condition'=>'id=:id','params'=>array(':id'=>$orderModel->custId)));
        
        $orderDetailsModel = SalesDetails::model()->findAll(array('condition'=>'salesId=:salesId','params'=>array(':salesId'=>$id),'order'=>'crAt DESC'));
        $this->render('invoicePreview',array(
            'companyModel'=>$companyModel,'branchModel'=>$branchModel,
            'orderModel'=>$orderModel,
            'customerModel'=>$customerModel,
            'orderDetailsModel'=>$orderDetailsModel,
        ));
    }

	/**
	 * Creates a new model. for default pos
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($salesReturn='')
	{


    	$msg = $invoice =$salesInvoiceId= '';
		$model=new SalesInvoice;
		
		// is customer or general
		if(isset($_REQUEST['customer']) && $_REQUEST['customer']='yes') $isCustomer = 1;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

        $modelSave = false;
        $custModel = '';

		if(isset($_POST['SalesInvoice']))
		{
			$model->attributes=$_POST['SalesInvoice'];

			// customer wise sales or not
			if(isset($_POST['SalesInvoice']['custId']) && !empty($_POST['SalesInvoice']['custId'])) 
			{
				$custModel = Customer::model()->find('phone=:phone',array(':phone'=>$_POST['SalesInvoice']['custId']));

				if(!empty($custModel)){
                    $model->custId = $custModel->id;


                    // Send SMS
                    $company = Company::model()->find();
                    $smsData = [
                        "charset" => "UTF-8",
                        "mobile" => $custModel->phone,
                        "smsText" => $company->sms_text
                    ];
                    Company::sendSms($smsData);
                } else{
                    $model->custId = Customer::DEFAULT_CUST;
                }
			} 
			else $model->custId = Customer::DEFAULT_CUST; 
            
            // coupon/instant discount processing
			if(isset($_POST['couponNo']) && !empty($_POST['couponNo'])) 
			{
				$couponModel = Coupons::model()->find('couponNo=:couponNo',array(':couponNo'=>$_POST['couponNo']));
				if(!empty($couponModel)) $model->couponId = $couponModel->id;
				else $model->couponId = Coupons::DEFAULT_COUPON; 	
			} 
            else $model->couponId = Coupons::DEFAULT_COUPON; 
            $model->status = SalesInvoice::STATUS_ACTIVE;

            if($model->save()) 
			{

                $modelSave = true;

                $salesInvoiceId=$model->id;
				// processing sales details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) :
						if($qty>0) :
							$modelSales = new SalesDetails;
							$modelSales->salesId = $model->id;
							$modelSales->qty = $qty;
							$modelSales->itemId = $_POST['proId'][$_POST['pk'][$key]];		
							$modelSales->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
							$modelSales->salesPrice = $_POST['sellPriceAmount'][$_POST['pk'][$key]];
							$modelSales->discountPrice = $_POST['discountAmount'][$_POST['pk'][$key]];
							$modelSales->vatPrice = $_POST['taxAmount'][$_POST['pk'][$key]];
							$modelSales->status = SalesDetails::STATUS_ACTIVE;
							if($modelSales->save())
							{
								$discount = $_POST['discountPercent'][$_POST['pk'][$key]];
									
								// damage/wastage calculations
								if(($discount/100)>=1) 
								{
									$modeldw = new DamageWastage;
									$modeldw->type = $_POST['type'][$_POST['pk'][$key]];	
									$modeldw->itemId = $_POST['proId'][$_POST['pk'][$key]];	
									$modeldw->qty = round($discount/100,2);	
									$modeldw->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
									$modeldw->save();
								}	
							}
						endif;
					endforeach;	
					$invoice = $model->id;
				endif;
				
				// processing loyalty income
				if(isset($_POST['SalesInvoice']['custId']) && isset($_POST['loyaltyRate']))
				{
					if(!empty($_POST['SalesInvoice']['loyaltyPaid'])) $loyaltyPaid = $_POST['SalesInvoice']['loyaltyPaid'];
					else $loyaltyPaid = 0;

                    $returnPrice = (isset($_POST['returnPrice'])) ? $_POST['returnPrice'] : 0;
					
					// without vat and [Total Amount  - (Total Discount + Total Reedem point)]
					$loyaltyAmount = (($model->totalPrice - ($model->totalDiscount+$loyaltyPaid + $returnPrice))*$_POST['loyaltyRate'])/100;
					$loyaltyAmount = round($loyaltyAmount,2);
					
					$modelLoyalty = new CustomerLoyalty;
					$modelLoyalty->custId =  $model->custId;
					$modelLoyalty->salesId = $model->id;
					$modelLoyalty->loyaltyAmount = $loyaltyAmount;
					$modelLoyalty->save();
				}
                
                // update coupon used code
                if($model->couponId > Coupons::DEFAULT_COUPON)
                {
                    $sql = "UPDATE pos_coupons set status=".Coupons::STATUS_INACTIVE." WHERE id=".$model->couponId; 
                    $command = Yii::app()->db->createCommand($sql);
                    $command->execute();
                }
                // reset model
                $model=new SalesInvoice;

                // New customer create
                if(!empty($_POST['Customer'])){
                    $cus = $_POST['Customer'];
                    $custId = '';
                    $name = '';
                    $addressline = '';
                    $phone = '';
                    if(!empty($cus['custId'])){
                        $custId = $cus['custId'];
                    }
                    if(!empty($cus['name'])){
                        $name = $cus['name'];
                    }if(!empty($cus['addressline'])){
                        $addressline = $cus['addressline'];
                    }if(!empty($cus['phone'])){
                        $phone = $cus['phone'];
                    }
                    $customerModel = new Customer();
                    $customerModel->custId = $custId;
                    $customerModel->name = $name;
                    $customerModel->addressline = $addressline;
                    $customerModel->phone = $phone;
                    $customerModel->custType = 1;
                    $customerModel->status = 1;
                    $customerModel->save();

                    $sqlSalesInvoice = "UPDATE pos_sales_invoice set custId=".$customerModel->id." WHERE id=".$salesInvoiceId;
                    $commandExecute = Yii::app()->db->createCommand($sqlSalesInvoice);
                    $commandExecute->execute();

                    // Send SMS
                    $company = Company::model()->find();
                    $smsData = [
                        "charset" => "UTF-8",
                        "mobile" => $phone,
                        "smsText" => $company->sms_text
                    ];
                    Company::sendSms($smsData);
                }
            }
		}
		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
            'invoice'=>$invoice,
            'salesReturn'=>$salesReturn,
            'modelSave'=> $modelSave,
            'custModel'=> $custModel
		));
	}

	/**
	 * Creates a new model. for full pos with due
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreateSpecial()
	{
		$msg = $invoice = '';
		$model = new SalesInvoice;

		// is customer or general
		if(isset($_REQUEST['customer']) && $_REQUEST['customer']='yes') $isCustomer = 1;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SalesInvoice']))
		{
			$model->attributes=$_POST['SalesInvoice'];

			// customer wise sales or not
			if(isset($_POST['SalesInvoice']['custId']) && !empty($_POST['SalesInvoice']['custId']))
			{
                $custModel = Customer::model()->find('name=:name',array(':name'=>$_POST['SalesInvoice']['custId']));
                if(!empty($custModel)) $model->custId = $custModel->id;
                else $model->custId = Customer::DEFAULT_CUST;
			}
			else $model->custId = Customer::DEFAULT_CUST;
			$model->status = SalesInvoice::STATUS_ACTIVE;

			if($model->save())
			{
				// processing sales details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) :
						if($qty>0) :
							$modelSales = new SalesDetails;
							$modelSales->salesId = $model->id;
							$modelSales->qty = $qty;
							$modelSales->itemId = $_POST['proId'][$_POST['pk'][$key]];
							$modelSales->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
							$modelSales->salesPrice = $_POST['sellPriceAmount'][$_POST['pk'][$key]];
							$modelSales->discountPrice = $_POST['discountAmount'][$_POST['pk'][$key]];
							$modelSales->vatPrice = $_POST['taxAmount'][$_POST['pk'][$key]];
							$modelSales->status = SalesDetails::STATUS_ACTIVE;
							if($modelSales->save())
							{
								$discount = $_POST['discountPercent'][$_POST['pk'][$key]];

								// damage/wastage calculations
								if(($discount/100)>=1)
								{
									$modeldw = new DamageWastage;
									$modeldw->type = $_POST['type'][$_POST['pk'][$key]];
									$modeldw->itemId = $_POST['proId'][$_POST['pk'][$key]];
									$modeldw->qty = round($discount/100,2);
									$modeldw->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
									$modeldw->save();
								}
							}
						endif;
					endforeach;
					$invoice = $model->id;
				endif;
				// reset model
				$model=new SalesInvoice;
                $this->redirect(array('salesInvoice/printInvoice','invoice'=>$invoice,'type'=>'invoice'));
			}
		}
		$this->render('_formSpecial',array(
			'model'=>$model,
			'msg'=>$msg,
			'invoice'=>$invoice,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SalesInvoice']))
		{
			$model->attributes=$_POST['SalesInvoice'];
			if($model->save())
            	$msg = "SalesInvoice Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/**
	 * print particular invoice.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionPrintInvoice()
	{
        $msg = $invoice = $type = '';
        if(isset($_REQUEST['invoice']) && !empty($_REQUEST['invoice'])) $invoice = $_REQUEST['invoice'];
        if(isset($_REQUEST['type']) && !empty($_REQUEST['type'])) $type = $_REQUEST['type'];

        $model=new SalesInvoice;
        if(isset($_POST['SalesInvoice']))
        {
            $invModel = SalesInvoice::model()->find('invNo=:invNo',array(':invNo'=>$_POST['SalesInvoice']['invNo']));
            if(!empty($invModel)) $invoice = $invModel->id;
            else $msg = '<div class="notification note-error"><p>Wrong Invoice No.</p></div>';
        }
        $this->render('printInvoice',array(
            'model'=>$model,
            'invoice'=>$invoice,
            'msg'=>$msg,'type'=>$type,
        ));

		/*$msg = $invoice = '';
		$model=new SalesInvoice;
		if(isset($_POST['SalesInvoice']))
		{
			$invModel = SalesInvoice::model()->find('invNo=:invNo',array(':invNo'=>$_POST['SalesInvoice']['invNo']));
			if(!empty($invModel)) $invoice = $invModel->id;
		}
		
		$this->render('printInvoice',array(
			'model'=>$model,
			'invoice'=>$invoice,
            'msg'=>$msg,
		));*/
	}
	
	/**
	 * Sales Return
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionSalesReturn()
	{
    	$msg = $invoice = '';
		$itemArr = $salesItemArr =$salesInvoiceId= array();
		$model = new SalesInvoice;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		// processing stock details
		if(isset($_POST['qty'])) :
            foreach($_POST['is_defect'] as $is_defect_key=>$is_defectItem) :
                if ($is_defectItem==1){
                    foreach($_POST['qty'] as $key=>$qty) :
                        if($qty>0) :
                        $dwNo='DW'.date("Ymdhis");
                        $modelWastage = new DamageWastage();
                        $modelWastage->dwNo =$dwNo;
                        $modelWastage->type = $_POST['proId'][$_POST['pk'][$is_defect_key]];
                        $modelWastage->itemId = $_POST['proId'][$_POST['pk'][$is_defect_key]];
                        $modelWastage->qty = $is_defectItem;
                        $modelWastage->costPrice = $_POST['costPrice'][$_POST['pk'][$is_defect_key]];
                        $modelWastage->save();
                        $itemArr[] = $modelWastage->itemId;
                        $invoice = $_POST['invId'];
                        if(!empty($itemArr)) :
                            foreach($itemArr as $key=>$data) :
                                $sql = "UPDATE pos_sales_details set status=".SalesDetails::STATUS_DAMAGE." WHERE salesId=".$invoice." AND itemId=".$data;
                                $command = Yii::app()->db->createCommand($sql);
                                $command->execute();
                            endforeach;
                        endif;
                        endif;
                    endforeach;
                }else{
                    foreach($_POST['qty'] as $key=>$qty) :
                        if($qty>0) :
                            $salesReturnModel = new SalesReturn;
                            $salesReturnModel->salesId = $_POST['invId'];
                            $salesReturnModel->itemId = $_POST['proId'][$_POST['pk'][$key]];
                            $salesReturnModel->qty = $qty;
                            $salesReturnModel->totalPrice = $_POST['netPrice'][$_POST['pk'][$key]];
                            $salesReturnModel->totalTax = $_POST['vatPrice'][$_POST['pk'][$key]];
                            $salesReturnModel->totalDiscount = $_POST['discountPrice'][$_POST['pk'][$key]];
                            $salesReturnModel->totalSpDiscount = $_POST['totalSpDiscount'][$_POST['pk'][$key]];
                            $salesReturnModel->totalInsDiscount = $_POST['totalInsDiscount'][$_POST['pk'][$key]];
                            $salesReturnModel->save();
                            $itemArr[] = $salesReturnModel->itemId;
                        endif;
                    endforeach;
                    $invoice = $_POST['invId'];
                    $msg = "Sales Return Successful !";

                    // get all items by invoice to compare all item return or not
                    $salesItemsModel =  SalesDetails::model()->findAll(array('condition'=>'salesId=:salesId', 'params'=>array(':salesId'=>$invoice)));
                    if(!empty($salesItemsModel)) foreach($salesItemsModel as $key=>$data) $salesItemArr[] = $data->itemId;$salesInvoiceId[] = $data->salesId;
                    if(empty(array_diff($salesItemArr,$itemArr))) $orderStatus = SalesInvoice::STATUS_CANCEL;
                    else $orderStatus = SalesInvoice::STATUS_RETURN;

                    // update sales invoice return status
                    $sql = "UPDATE pos_sales_invoice set status=".$orderStatus." WHERE id=".$_POST['invId'];
                    $command = Yii::app()->db->createCommand($sql);
                    $command->execute();

                    // update sales details status
                    if(!empty($itemArr)) :
                        foreach($itemArr as $key=>$data) :
                            $sql = "UPDATE pos_sales_details set status=".SalesDetails::STATUS_RETURN." WHERE salesId=".$invoice." AND itemId=".$data;
                            $command = Yii::app()->db->createCommand($sql);
                            $command->execute();
                        endforeach;
                    endif;
                }
            endforeach;

            if (!empty($salesInvoiceId)){
                $this->redirect(array('salesInvoice/create','salesReturn'=>json_encode($salesInvoiceId)));
            }
        endif;
		
		$this->render('salesReturn',array(
			'model'=>$model,
            'msg'=>$msg,
			'invoice'=>$invoice,
			'itemArr'=>$itemArr,
		));
	}
	
	// sales mismatch
	public function actionSalesMismatch()
	{
		// AMOUNT mismatch
		$sql = "SELECT s.salesId AS inv, ROUND(v.totalPrice,2) AS totalPrice,ROUND(SUM(s.qty*s.salesPrice),2) AS salesPrice FROM pos_sales_invoice v,
				pos_sales_details s WHERE s.salesId=v.id GROUP BY s.salesId ORDER BY v.orderDate DESC";
				
		// TAX mismatch
		/*$sql = "SELECT s.salesId AS inv, v.totalTax AS totalTax, ROUND(SUM(s.vatPrice),2) AS vatPrice FROM pos_sales_invoice v, 
				pos_sales_details s WHERE s.salesId=v.id GROUP BY s.salesId ORDER BY v.orderDate DESC";*/
				
		$model = Yii::app()->db->createCommand($sql)->queryAll();
		
		if(!empty($model)) :
		
			//echo 'INVOICE--->INVOICE TX--->DETAILS TAX<hr/>';
			
			echo 'INVOICE AMOUNT--->DETAILS AMOUNT------>DIFFERENCE<hr/>';
			$sumInvAmount = $sumDetAmount = 0;
			foreach($model as $key=>$data) :
				if($data['totalPrice']!=$data['salesPrice'])
				{
					$sumInvAmount+=$data['totalPrice'];
					$sumDetAmount+=$data['salesPrice'];
                    //echo $sumInvAmount.'--->'.$sumDetAmount.'---->'.($sumInvAmount-$sumDetAmount);
					//echo $data['inv'].'--->'.$data['totalPrice'].'--->'.$data['salesPrice'].'<hr/>';
					/*$sql = "UPDATE pos_sales_invoice set totalPrice=".$data['salesPrice']." WHERE id=".$data['inv']; 
					$command = Yii::app()->db->createCommand($sql);
					$command->execute();*/
				}
				
				/*if($data['totalTax']!=$data['vatPrice'])
				{
					echo $data['inv'].'--->'.$data['totalTax'].'--->'.$data['vatPrice'].'<hr/>';
					/*$sql = "UPDATE pos_sales_invoice set totalTax=".$data['vatPrice']." WHERE id=".$data['inv']; 
					$command = Yii::app()->db->createCommand($sql);
					$command->execute();
				}*/
                if(($data['totalPrice']-$data['salesPrice'])>0)
                    echo $data['inv'].'--->'.$data['totalPrice'].'--->'.$data['salesPrice'].'----><font color="red">'.($data['totalPrice']-$data['salesPrice']).'</font><hr/>';
                else echo $data['inv'].'--->'.$data['totalPrice'].'--->'.$data['salesPrice'].'---->'.($data['totalPrice']-$data['salesPrice']).'<hr/>';
			endforeach;
		endif;
	}
	
	// import data from local pos
	public function actionImportLocalData()
	{
		echo 'inn';
	}
	
	// export data for local pos
	public function actionExportLocalData()
	{
        $date = date("Y-m-d");
        $cycle = 1;
        $new_line = "\n";
        $new_tab_single = "\t";
        $new_tab_double = "\t\t";
        $new_tab_triple= "\t\t\t";

        // exporting company with branch data
        $compModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
        if(!empty($compModel))
        {
            $xmlDataComp= '<?xml version="1.0" encoding="UTF-8" ?>'.$new_line;
            $xmlDataComp.= '<database name="local_retail_sales">'.$new_line;
            $xmlDataComp.= $new_tab_single.'<table name="pos_company">'.$new_line;
                /* Create the first child element */
                $xmlDataComp .= $new_tab_double.'<item>'.$new_line;
                $xmlDataComp .= $new_tab_triple.'<id>'.$compModel->id.'</id>'.$new_line;
                $xmlDataComp .= $new_tab_triple.'<name>'.$compModel->name.'</name>'.$new_line;
                $xmlDataComp .= $new_tab_triple.'<detailsinfo>'.$compModel->detailsinfo.'</detailsinfo>'.$new_line;
                if(!empty($branchModel))
                {
                    $xmlDataComp .= $new_tab_triple.'<addressline>'.$branchModel->addressline.'</addressline>'.$new_line;
                    $xmlDataComp .= $new_tab_triple.'<phone>'.$branchModel->phone.'</phone>'.$new_line;
                    $xmlDataComp .= $new_tab_triple.'<vatrn>'.$branchModel->vatrn.'</vatrn>'.$new_line;
                }
                $xmlDataComp .= $new_tab_double.'</item>'.$new_line;
            $xmlDataComp .= $new_tab_single.'</table>'.$new_line;
            $xmlDataComp .= '</database>'.$new_line;
        }

        // exporting item data
        $itemModel = Items::model()->findAll('status=:status',array(':status'=>Items::STATUS_ACTIVE));
        if(!empty($itemModel))
        {
            $xmlDataItems= '<?xml version="1.0" encoding="UTF-8" ?>'.$new_line;
            $xmlDataItems.= '<database name="local_retail_sales">'.$new_line;
            $xmlDataItems.= $new_tab_single.'<table name="pos_items">'.$new_line;
            foreach($itemModel as $itemKey=>$itemData)
            {
                // offer or discount by durations
                $sqlOffer = "SELECT * FROM pos_poffers WHERE itemId=".$itemData->id." AND branchId=".Yii::app()->session['branchId']."
                             AND startDate<='".$date."' AND endDate>='".$date."'";
                $offerModel = Poffers::model()->findBySql($sqlOffer);
                if(!empty($offerModel))
                    $discountPercent = $offerModel->package->discount;
                else $discountPercent = 0;

                // finding item actual cost price FIFO (N.B : if negative stock then get current stock price)
                $costPrice = $itemData->costPrice;
                $sellQty = $stockSum = 0;
                $stockModel = array();
                $sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']."
                             AND s.itemId=".$itemData->id;
                $ressells = Yii::app()->db->createCommand($sqlsells)->queryRow();
                if(!empty($ressells['qty'])) $sellQty   =  $ressells['qty'];
                $stockModel =  Stock::model()->findAll(array('condition'=>'branchId=:branchId AND itemId=:itemId',
                                                            'params'=>array(':branchId'=>Yii::app()->session['branchId'],':itemId'=>$itemData->id),
                                                            'order'=>'stockDate ASC'));

                if(!empty($stockModel)) :
                    foreach($stockModel as $sKey=>$sData) :
                        $stockSum+=$sData->qty;
                        if($sellQty<$stockSum)
                        {
                            $costPrice = $sData->costPrice;
                            break;
                        }
                    endforeach;
                endif;

                /* Create the first child element */
                $xmlDataItems .= $new_tab_double.'<item code="'.$itemData->itemCode.'">'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<id>'.$itemData->id.'</id>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<barCode>'.$itemData->barCode.'</barCode>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<itemName>'.$itemData->itemName.'</itemName>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<costPrice>'.$costPrice.'</costPrice>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<sellPrice>'.$itemData->sellPrice.'</sellPrice>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<discountPrice>'.$discountPercent.'</discountPrice>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<taxRate>'.$itemData->tax->taxRate.'</taxRate>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<isWeighted>'.$itemData->isWeighted.'</isWeighted>'.$new_line;
                    $xmlDataItems .= $new_tab_triple.'<crAt>'.$itemData->crAt.'</crAt>'.$new_line;
                $xmlDataItems .= $new_tab_double.'</item>'.$new_line;
            }
            $xmlDataItems .= $new_tab_single.'</table>'.$new_line;
            $xmlDataItems .= '</database>'.$new_line;
        }

        // exporting customer & payment methods data
        $custModel = Customer::model()->findAll('status=:status',array(':status'=>Customer::STATUS_ACTIVE));
        $pmModel = CardTypes::model()->findAll('status=:status',array(':status'=>CardTypes::STATUS_ACTIVE));
        if(!empty($custModel))
        {
            $xmlDataCust= '<?xml version="1.0" encoding="UTF-8" ?>'.$new_line;
            $xmlDataCust.= '<database name="local_retail_sales">'.$new_line;
            if(!empty($pmModel))
            {
                $xmlDataCust.= $new_tab_single.'<table name="pos_payment_method">'.$new_line;
                foreach($pmModel as $pmKey=>$pmData)
                {
                    /* Create the first child element */
                    $xmlDataCust .= $new_tab_double.'<item>'.$new_line;
                    $xmlDataCust .= $new_tab_triple.'<id>'.$pmData->id.'</id>'.$new_line;
                    $xmlDataCust .= $new_tab_triple.'<name>'.$pmData->name.'</name>'.$new_line;
                    $xmlDataCust .= $new_tab_double.'</item>'.$new_line;
                }
                $xmlDataCust .= $new_tab_single.'</table>'.$new_line;
            }
            $xmlDataCust.= $new_tab_single.'<table name="pos_customer">'.$new_line;
            foreach($custModel as $custKey=>$custData)
            {
                /* Create the first child element */
                $xmlDataCust .= $new_tab_double.'<customer custId="'.$custData->custId.'">'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<id>'.$custData->id.'</id>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<title>'.$custData->title.'</title>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<name>'.$custData->name.'</name>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<addressline>'.$custData->addressline.'</addressline>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<city>'.$custData->city.'</city>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<phone>'.$custData->phone.'</phone>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<custType>'.$custData->custType0->name.'</custType>'.$new_line;
                $xmlDataCust .= $new_tab_triple.'<loyaltyRate>'.$custData->custType0->loyaltyRate.'</loyaltyRate>'.$new_line;
                $xmlDataCust .= $new_tab_double.'</customer>'.$new_line;
            }
            $xmlDataCust .= $new_tab_single.'</table>'.$new_line;
            $xmlDataCust .= '</database>'.$new_line;
        }
        // generate fresh sales xml data
        $xmlDataSales= '<?xml version="1.0" encoding="UTF-8" ?>'.$new_line;
        $xmlDataSales.= '<database name="local_retail_sales">'.$new_line;
        $xmlDataSales.= $new_tab_single.'<table name="pos_sales_invoice">'.$new_line;
        $xmlDataSales .= $new_tab_single.'</table>'.$new_line;
        $xmlDataSales .= '</database>'.$new_line;

        // write to xml
        $xmlRoot = dirname(Yii::app()->request->scriptFile).'/media/exportLocalPosData/';
        $compXml = Yii::app()->session['orgId'].'_'.date("Ymdhis");
        if(!file_exists($xmlRoot.$compXml)) mkdir($xmlRoot.$compXml, 0777, true);

        $compXmlfile = $xmlRoot.$compXml.'/pos_company.xml';
        file_put_contents($compXmlfile, $xmlDataComp);
        $itemXmlfile = $xmlRoot.$compXml.'/pos_items.xml';
        file_put_contents($itemXmlfile, $xmlDataItems);
        $custXmlfile = $xmlRoot.$compXml.'/pos_customers.xml';
        file_put_contents($custXmlfile, $xmlDataCust);
        $salesXmlfile = $xmlRoot.$compXml.'/pos_sales.xml';
        file_put_contents($salesXmlfile,$xmlDataSales);

        // zip archive and download
        $valid_files = array();
        $xmlFiles = scandir($xmlRoot.$compXml,1);
        if(is_array($xmlFiles)) {
            foreach($xmlFiles as $file) {
                if($file!='.' && $file!='..' && file_exists($xmlRoot.$compXml.'/'.$file)) {
                    $valid_files[] = $file;
                }
            }
        }
        if(count($valid_files > 0))
        {
            $zip = new ZipArchive();
            $zip_name = 'exported_server_retails_'.$date.'.zip';
            if($zip->open($xmlRoot.$zip_name, ZIPARCHIVE::CREATE)!==TRUE){
                echo "* Sorry ZIP creation failed at this time";
                exit;
            }

            foreach($valid_files as $file){
                $zip->addFile($xmlRoot.$compXml.'/'.$file, $file);
            }
            $zip->close();
            if(file_exists($xmlRoot.$zip_name))
            {
                // force to download the zip
                header("Pragma: public");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private",false);
                header('Content-type: application/zip');
                header('Content-Disposition: attachment; filename="'.$zip_name.'"');
                readfile($xmlRoot.$zip_name);
                $this->rrmdir($xmlRoot.$compXml); // unlink xml dir
                unlink($xmlRoot.$zip_name); // unlink from project root
            }
        }
        else
        {
            echo "No valid files to zip";
            exit;
        }
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SalesInvoice');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new SalesInvoice('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SalesInvoice']))
			$model->attributes=$_GET['SalesInvoice'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SalesInvoice the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SalesInvoice::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SalesInvoice $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sales-invoice-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    // delete directory with files
    public function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
