<?php
/*********************************************************
-*- logo: CompanyController.php
-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
-*- Date: 2014.02.24
-*- Position:  protected/controller
-*- YII-*- version 1.1.13
/*********************************************************/

class CompanyController extends Controller
{
    /**
     * @var string the default layout for the @views. Defaults to '//layouts/adminColumn', meaning
     * using two-column layout. See 'protected/@views/layouts/adminColumn.php'.
     */
    public $metaTitle 	 	 = NULL;
    public $metaKeywords 	 = NULL;
    public $metaDescriptions = NULL;
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $msg = '';
        $model=new Company;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Company']))
        {
            $model->attributes=$_POST['Company'];

            if (CUploadedFile::getInstance($model,'logo') && CUploadedFile::getInstance($model,'favicon'))
            {
                $uploaddir = dirname(Yii::app()->request->scriptFile) . '/media/system/';

                // favicon
                $explogonameIco = CUploadedFile::getInstance($model,'favicon');
                $newfaviconname = date("YmdHis") .'_'. $explogonameIco;
                $favicon = $uploaddir.'/'.$newfaviconname;
                $model->favicon = '/media/system/'.$newfaviconname;

                // logo
                $explogonameLogo = CUploadedFile::getInstance($model,'logo');
                $newlogoname = date("YmdHis") .'_'. $explogonameLogo;
                $logo = $uploaddir.'/'.$newlogoname;
                $model->logo = '/media/system/'.$newlogoname;
            }
            if($model->save())
            {
                // server subDomain processing

                // logo resize with favicon upload
                $explogonameIco->saveAs($favicon);
                if(CUploadedFile::getInstance($model,'logo') && $explogonameLogo->saveAs($logo))  {
                    $image = Yii::app()->image->load($logo);
                    //$image->resize(150, 110)->quality(100)->sharpen(20);
                    $image->save();
                }
                $msg = "Company Added Successfully";
                $model=new Company;
            }
        }

        $this->render('_form',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $msg = '';
        $model=$this->loadModel($id);
        $modellogo = $model->logo;
        $modelfavicon = $model->favicon;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Company']))
        {
            $model->attributes=$_POST['Company'];
            $uploaddir = dirname(Yii::app()->request->scriptFile) . '/media/system/';

            // favicon processing
            if (CUploadedFile::getInstance($model,'favicon')) {
                $explogonameIco = CUploadedFile::getInstance($model,'favicon');
                $newfaviconname = date("YmdHis") .'_'. $explogonameIco;
                $favicon = $uploaddir.'/'.$newfaviconname;
                $model->favicon = '/media/system/'.$newfaviconname;
                if($explogonameIco->saveAs($favicon))
                {
                    //-------delete previous------//
                    if(file_exists($modelfavicon)) unlink($modelfavicon);
                }
            }
            else $model->favicon = $modelfavicon;

            // logo processing
            if (CUploadedFile::getInstance($model,'logo')) {
                $explogonameLogo = CUploadedFile::getInstance($model,'logo');
                $newlogoname = date("YmdHis") .'_'. $explogonameLogo;
                $logo = $uploaddir.'/'.$newlogoname;
                $model->logo = '/media/system/'.$newlogoname;
            }
            else $model->logo = $modellogo;

            if($model->save()) {
                if(CUploadedFile::getInstance($model,'logo') && $explogonameLogo->saveAs($logo))  {
                    //-------delete previous------//
                    if(file_exists($modellogo)) unlink($modellogo);
                    $image = Yii::app()->image->load($logo);
                    //$image->resize(150, 110)->quality(100)->sharpen(20);
                    $image->save();
                }
                $msg = "Company Updated Successfully";
            }
        }

        $this->render('_form',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        if(file_exists(dirname(Yii::app()->request->scriptFile).$model->favicon)) unlink(dirname(Yii::app()->request->scriptFile).$model->favicon); //--favicon delete
        if(file_exists(dirname(Yii::app()->request->scriptFile).$model->logo)) unlink(dirname(Yii::app()->request->scriptFile).$model->logo); //--logo delete
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Company');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (isset($_GET['pageSize'])) {
            //
            // pageSize will be set on user's state
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            //
            // unset the parameter as it
            // would interfere with pager
            // and repetitive page size change
            unset($_GET['pageSize']);
        }

        $model=new Company('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Company']))
            $model->attributes=$_GET['Company'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Company the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Company::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Company $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='company-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
