<?php

/* * *******************************************************
  -*- File: SubdepartmentController.php
  -*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
  -*- Date: 2014.03.06
  -*- Position:  protected/controller
  -*- YII-*- version 1.1.13
  /******************************************************** */

class SubdepartmentController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
     * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
     */
    public $metaTitle = NULL;
    public $metaKeywords = NULL;
    public $metaDescriptions = NULL;
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $msg = '';
        $model = new Subdepartment;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Subdepartment'])) {
            $model->attributes = $_POST['Subdepartment'];
            if ($model->save()) {
                $msg = "Sub Category  Saved Successfully";
                $model = new Subdepartment;
                //$this->redirect(array('view','id'=>$model->id));
                if (!empty($_POST['Subdepartment']['price'])){
                    echo "<pre>";
                    print_r($model);
                    exit();
                }

            }
        }

        $this->render('_form', array(
            'model' => $model,
            'msg' => $msg,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $msg = '';
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if (isset($_POST['Subdepartment'])) {
            $model->attributes = $_POST['Subdepartment'];
            if ($model->save())
                $msg = "Sub Category  Updated Successfully";
            //$this->redirect(array('admin'));

            //Update Price After Import Previous Price
            if (!empty($_POST['Subdepartment']['price']) && $_POST['Subdepartment']['retrieve_previous_price']==0){

                $sqlItems = "SELECT * from pos_items  where variation=0 and subdeptId='".$model->id."'";
                $modelItemData = Yii::app()->db->createCommand($sqlItems)->queryAll();
                $backupDataArray=[];
                $variationsDataArray=[];


                foreach ($modelItemData as $modelItems){
                    $backupDataArray[]=[
                        'itemId'=>$modelItems['id'],
                        'subdeptId'=>$model->id,
                        'sellPrice'=>$modelItems['sellPrice']
                    ];
                    $updateSql="UPDATE `pos_items` SET `sellPrice`='".$_POST['Subdepartment']['price']."' WHERE id='".$modelItems['id']."'";
                    Yii::app()->db->createCommand($updateSql)->execute();

                    if ($modelItems['variation_type']==2){

                        $sqlVariationsItems = "SELECT * from pos_items  where  variation='".$modelItems['id']."'";
                        $modelVariationsItem = Yii::app()->db->createCommand($sqlVariationsItems)->queryAll();

                        foreach ($modelVariationsItem as $variationData){
                            $varationsIdAndPriceSql="SELECT * from pos_items_variation WHERE item_id='".$variationData['id']."'";
                            $modelSpecialGetingVariaitonId = Yii::app()->db->createCommand($varationsIdAndPriceSql)->queryRow();

                            $variationsDataArray[]=[
                                'variationsId'=>$modelSpecialGetingVariaitonId['id'],
                                'sellPrice'=>$variationData['sellPrice']
                            ];

                            $updateVariationsSql11="UPDATE `pos_items_variation` SET `sell_price`='".$_POST['Subdepartment']['price']."' WHERE item_id='".$variationData['id']."'";
                            Yii::app()->db->createCommand($updateVariationsSql11)->execute();
                        }

                        $updateVariationsSql="UPDATE `pos_items` SET `sellPrice`='".$_POST['Subdepartment']['price']."' WHERE variation='".$modelItems['id']."'";
                        Yii::app()->db->createCommand($updateVariationsSql)->execute();



                    }
                }

                $get_company_infoJE = json_encode($backupDataArray);
                $getCompanyInfofileW = fopen("itemPrice.txt", "w") or die("Unable to open file!");
                fwrite($getCompanyInfofileW, $get_company_infoJE);

                $variationInfo = json_encode($variationsDataArray);
                $getVariationInfo = fopen("variationPrice.txt", "w") or die("Unable to open file!");
                fwrite($getVariationInfo, $variationInfo);

            }

            //Retrive Price After Import Previous Price
            if (!empty($_POST['Subdepartment']['retrieve_previous_price']) && $_POST['Subdepartment']['retrieve_previous_price']==1){
                $getPriceList = array_values((array)$this->getTextJsonFile('itemPrice'));
                if ($getPriceList) {
                    foreach ($getPriceList as $getPriceListItems) {
                        $updateSql = "UPDATE `pos_items` SET `sellPrice`='" . $getPriceListItems->sellPrice . "' WHERE id='" . $getPriceListItems->itemId . "'";
                        Yii::app()->db->createCommand($updateSql)->execute();
                        $msg = "Retrieve Price  Updated Successfully";
                    }
                }

                $getVariationPriceList = array_values((array)$this->getTextJsonFile('variationPrice'));
                if ($getVariationPriceList) {
                    foreach ($getVariationPriceList as $getVariationPriceListItems) {
                        $updateVariationDataSql = "UPDATE `pos_items_variation` SET `sell_price`='".$getVariationPriceListItems->sellPrice."' WHERE id='".$getVariationPriceListItems->variationsId."'";
                        Yii::app()->db->createCommand($updateVariationDataSql)->execute();
                        $msg = "Retrieve Price  Updated Successfully";
                    }
                }

            }

        }

        $this->render('_form', array(
            'model' => $model,
            'msg' => $msg,
        ));
    }

    public function getTextJsonFile($fileName){

        $getCompanyInfofileR    = fopen($fileName.".txt", "r") or die("Unable to open file!");
        $getCompanyInfofileV    = fread($getCompanyInfofileR,filesize($fileName.".txt"));

        return json_decode($getCompanyInfofileV);
    }


    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Subdepartment');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        if (isset($_GET['pageSize'])) {
            //
            // pageSize will be set on user's state
            Yii::app()->user->setState('pageSize', (int) $_GET['pageSize']);
            //
            // unset the parameter as it
            // would interfere with pager
            // and repetitive page size change
            unset($_GET['pageSize']);
        }

        $model = new Subdepartment('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Subdepartment']))
            $model->attributes = $_GET['Subdepartment'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Subdepartment the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Subdepartment::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Subdepartment $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'subdepartment-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
