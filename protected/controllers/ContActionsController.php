<?php
/*Description:  Operation List Controller*/
class ContActionsController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) /*View Specific Operation*/
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()/*Operation index page*/
	{
		$dataProvider=new CActiveDataProvider('ContActions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	/* Auto Load Controller and Action */
	public function actionAddControllerAction()/*Generate Controller and Operation*/
	{
		/* Open Controller Directory and check esisting files*/
		
		$dir = dirname(__FILE__);
		$currentControllerList = array();
		$currentActionList = array();
		if ($handle = opendir($dir)) {
		$patterns = '/Controller.php/';
		$replacements = '';
		$pattern = '/public function action/';	
		
		/* Collect All files*/
		 while (false !== ($file = readdir($handle))) {	
		    	if (preg_match($patterns, $file)) {
			    	$tempfile = $file;	      
			    	/* Remove Controller.php parts from collected files*/	 
				  	$contName = preg_replace($patterns, $replacements, $tempfile);      
				 	 
		    		$exists = Controllers::model()->exists('name=:name',array('name'=>$this->lcfirst(trim($contName))));
				
					/* If exist just get that controller Id, not save again*/
					if($exists){		
						$contmodel = Controllers::model()->find('name=:name',array('name'=>$this->lcfirst(trim($contName))));				
				} 
				/* If not exist save this controller and get Id*/
				else{
					//$numcontNE++;
					$contmodel = new Controllers;		
					$contmodel->name = $this->lcfirst(trim($contName));
					$contmodel->module = Yii::app()->db->tablePrefix;
					$contmodel->status = 1;
					$contmodel->save();					
				}
				/* Read Current Controller file*/    
				// $finlenames =  "D:/xampp/htdocs".Yii::app()->request->baseUrl."/protected/controllers/".$file;				  
				 $finlenames =  $dir."/".$file;
				 $fileActions1 = file($finlenames);	
				 $tnumAct = 0;
				 //echo $contName."<br>";
				 $tnumActNE = 0;  
				 /* Get every line of this controller file */   
				 $descflag = 0;	
	
		   		 foreach ($fileActions1 as $line_num => $line) 
				 {
						/* Check Is this a Action Method if true Then Get name else do nothing */ 

						/* Save Controller Description */  
						if(preg_match('/\/\*Description: /',htmlspecialchars($line))){
							$controllerDesc = mb_strstr(preg_replace('/\/\*Description: /', '', htmlspecialchars($line)), '*/', true);
							Controllers::model()->updateByPk(($contmodel->id), array('description'=>trim($controllerDesc)));
							//echo "------".$controllerDesc."<br>";
						}
						if (preg_match($pattern, htmlspecialchars($line))){ 
							/* Collect Only Action Name  */
							//echo htmlspecialchars($line)."<br>";
							$actionsName = mb_strstr(preg_replace($pattern, $replacements, htmlspecialchars($line)), '(', true);
							if((!preg_match('/\*/',$actionsName)) and (strlen($actionsName)>2)){
								$exists1 = ContActions::model()->find('name=:name and contId=:contId',array('name'=>$this->lcfirst(trim($actionsName)),'contId'=>$contmodel->id));
								
								/* Save Action Description */  
								$text = "";
								if(preg_match('!/\*.*?\*/!s', htmlspecialchars($line))){ 
									$text = preg_replace('/\/\*/','',mb_strstr(mb_strstr(htmlspecialchars($line), '*/', true),'/*',false));
								 }

								/* If not exist save Action*/
								if(empty($exists1)){	
									
									$actmodel = new ContActions;		
									$actmodel->name = $this->lcfirst(trim($actionsName));									
									$actmodel->contId = $contmodel->id;
									$actmodel->layout = 1;
									$actmodel->status = 1;
									$actmodel->description = trim($text);
									$actmodel->save();	

									$cmodel=new OrgUsertypeActions;
									$cmodel->usertypeId = UserType::getSuperUserType();
									$cmodel->contActionsId = $actmodel->id;
									$cmodel->status = OrgUsertypeActions::STATUS_ACTIVE;
									$cmodel->save();	
									$actionsId = $actmodel->id;	
									
								} 
								else
								{
									//ContActions::model()->updateByPk(($exists1->id), array('description'=>trim($text)));		
									$actionsId = $exists1->id;	
								}  
								$currentActionList[] = $actionsId;
							}	
						}
					}
					$currentControllerList[] = $contmodel->id;
		    	}
		    }
		    closedir($handle);
		}	
	   
	   $criteria = new CDbCriteria;
	   $criteria->addNotInCondition('id',$currentControllerList);
	   Controllers::model()->deleteAll($criteria);
	   
	   $criteria = new CDbCriteria;
	   $criteria->addNotInCondition('id',$currentActionList);
	   ContActions::model()->deleteAll($criteria);
	   $this->redirect('admin');
	}
	
	// - update controller action --// 
	public function actionUpdateDesc() /*Update*/
	{
		$msg = '';
		if(isset($_POST['ContActions']))
		{			
			$model = ContActions::model()->findByPk($_POST['id']);
			$model->attributes = $_POST['ContActions'];
			if($model->save()) $msg = '<font color="blue">Controller action udated successfully</font>';	
		}
		else $model = ContActions::model()->findByPk($_REQUEST['id']);

		$this->renderPartial('_form',array(
			'model'=>$model,
		    'msg'=>$msg) ,false,true
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()/*Operations List*/
	{
		
		if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
		
		$model=new ContActions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ContActions']))
			$model->attributes=$_GET['ContActions'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=ContActions::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cont-actions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function lcfirst($string) {
		if($string!="")
	      $string{0} = strtolower($string{0});
	      return $string;
	}
}
