<?php
/*********************************************************
        -*- File: SocialNetworkController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.02.17
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class SocialNetworkController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new SocialNetwork;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SocialNetwork']))
		{
			$model->attributes=$_POST['SocialNetwork'];
			if(CUploadedFile::getInstance($model,'icon'))
            {
				$uploaddir = dirname(Yii::app()->request->scriptFile) . '/media/socialIcons/';
                 // Icon
				$explogonameIco = CUploadedFile::getInstance($model,'icon');
                $newfaviconname = date("YmdHis") .'_'. $explogonameIco;
                $favicon = $uploaddir.'/'.$newfaviconname;
                $model->icon = '/media/socialIcons/'.$newfaviconname;
				$explogonameIco->saveAs($favicon);
            }
			echo $model->icon;
			
            if($model->save()) {				
				$msg = "Social Network Saved Successfully";
                $model=new SocialNetwork;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);
		$modelIcon = $model->icon;
        

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		$uploaddir = dirname(Yii::app()->request->scriptFile) . '/media/socialIcons/';
		if(isset($_POST['SocialNetwork']))
		{
			$model->attributes=$_POST['SocialNetwork'];
			// favicon processing
            if (CUploadedFile::getInstance($model,'icon')) {
                $explogonameIco = CUploadedFile::getInstance($model,'icon');
                $newfaviconname = date("YmdHis") .'_'. $explogonameIco;
                $favicon = $uploaddir.'/'.$newfaviconname;
                $model->icon = '/media/socialIcons/'.$newfaviconname;
                if($explogonameIco->saveAs($favicon))
                {
                    //-------delete previous------//
                    if(file_exists($modelIcon)) unlink($modelIcon);
                }
            }
            else $model->icon = $modelIcon;
			
			if($model->save())
            	$msg = "SocialNetwork Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		if(file_exists(dirname(Yii::app()->request->scriptFile).$model->icon)) unlink(dirname(Yii::app()->request->scriptFile).$model->icon); //--favicon delete
        $model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SocialNetwork');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new SocialNetwork('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SocialNetwork']))
			$model->attributes=$_GET['SocialNetwork'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SocialNetwork the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SocialNetwork::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SocialNetwork $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='social-network-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
