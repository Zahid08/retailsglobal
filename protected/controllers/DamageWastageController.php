<?php
/*********************************************************
        -*- File: DamageWastageController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.03.04
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class DamageWastageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new DamageWastage;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['qty']) && isset($_POST['type']))
		{
			$ptr = 0;
			// processing sales details
			foreach($_POST['qty'] as $key=>$qty) :
				if($qty>0) :
					$model = new DamageWastage;
					$model->dwNo = $_POST['DamageWastage']['dwNo'];
					$model->type = $_POST['type'][$_POST['pk'][$key]];	
					$model->itemId = $_POST['proId'][$_POST['pk'][$key]];	
					$model->qty = $qty;	
					$model->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
					$model->save();
					$ptr = 1;
				endif;
			endforeach;	
			
			if($ptr==1) :
				$msg = "Damage Wastage Saved Successfully";
				$model=new DamageWastage;
			endif;
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/*
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
    	$msg = '';
		$model=new DamageWastage;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['DamageWastage']))
		{	
			$model = DamageWastage::model()->find('dwNo=:dwNo',array(':dwNo'=>$_POST['DamageWastage']['dwNo']));
			$model->attributes=$_POST['DamageWastage'];
			
			// processing dw details
			if(isset($_POST['qty'])) :
				foreach($_POST['qty'] as $key=>$qty) :
					$sql = "update pos_damage_wastage set qty=".$qty.", moAt='".date("Y-m-d H:i:s")."' where dwNo='".$_POST['DamageWastage']['dwNo']."' and itemId=".$_POST['proId'][$_POST['pk'][$key]]; 
					$command = Yii::app()->db->createCommand($sql);
					$command->execute();
				endforeach;	
			endif;
			
			$msg = "Damage Wastage Updated Successfully";
			$model=new DamageWastage;
			//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DamageWastage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new DamageWastage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DamageWastage']))
			$model->attributes=$_GET['DamageWastage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return DamageWastage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=DamageWastage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param DamageWastage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='damage-wastage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
