<?php
/*********************************************************
        -*- File: InventoryController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.04.01
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class InventoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	/** inventory initialize/starts
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$msg = '';
		$count = 0;
		
		// check how many inventory done
		$invModel = Inventory::model()->findAll();
		if(!empty($invModel)) $count = count($invModel);
		
		if(isset($_POST['yt0']))
		{
			if(!empty($_POST['subdeptId'])) // check by sub department
			{
				// check alreday initialize/not
				$subDeptInvModel = InvSettings::model()->find('subDeptId=:subDeptId AND status=:status',array(
															  ':subDeptId'=>$_POST['subdeptId'],':status'=>InvSettings::STATUS_ACTIVE));
				if(!empty($subDeptInvModel)) 
					$msg = '<div class="notification note-error"><p>Inventory alreday Initialize for this Sub Department !</p></div>';
				else
				{
					$model = new InvSettings;
					$model->invNo = $_POST['invNo'].$_POST['subdeptId'];
					$model->subDeptId = $_POST['subdeptId'];
					$model->status = InvSettings::STATUS_ACTIVE;
					$model->save();
					$msg = '<div class="notification note-success"><p>Inventory Initialize Successfully for this Sub Department !</p></div>';	
				}
			}
			else if(!empty($_POST['deptId'])) // check by department
			{
				// check alreday initialize/not
				$criteria=new CDbCriteria;
				$subDeptArr = array();
				if($_POST['deptId']=='all') $subdeptAll = Subdepartment::model()->findAll('status like :status',array(':status'=>Subdepartment::STATUS_ACTIVE));
				else $subdeptAll = Subdepartment::model()->findAll('deptId like :deptId and status like :status',array(':deptId'=>$_POST['deptId'],':status'=>Subdepartment::STATUS_ACTIVE));
				
				if(!empty($subdeptAll)) :
					foreach($subdeptAll as $data) $subDeptArr[] = $data->id;
				endif; 
				$criteria->addInCondition('subDeptId',$subDeptArr);
				$criteria->group = 'subDeptId DESC';
				$subDeptInvModel = InvSettings::model()->findAll($criteria);
				
				if(!empty($subDeptInvModel)) 
				{
					foreach($subDeptInvModel as $key=>$data) : 							
						if($data->status==InvSettings::STATUS_INACTIVE)
						{
							$model = new InvSettings;
							$model->invNo = $_POST['invNo'].$_POST['deptId'];
							$model->subDeptId = $data->subDeptId;
							$model->status = InvSettings::STATUS_ACTIVE;
							if($model->save())
								$msg.= '<div class="notification note-success"><p>Inventory Initialize Successfully for Sub Department : '.$data->subDept->name.' !</p></div>';
						}
						else $msg.= '<div class="notification note-error"><p>Inventory alreday Initialize for Sub Department : '.$data->subDept->name.' !</p></div>';
					endforeach;
				}
				else
				{
					if(!empty($subdeptAll)) :
						foreach($subdeptAll as $key=>$data) :
							$model = new InvSettings;
							$model->invNo = $_POST['invNo'].$_POST['deptId'];
							$model->subDeptId = $data->id;
							$model->status = InvSettings::STATUS_ACTIVE;
							$model->save();
						endforeach;
						$msg = '<div class="notification note-success"><p>Inventory Initialize Successfully for this Department !</p></div>';	
					endif;
				}
			}
			else $msg = '<div class="notification note-error"><p>Select at lesat 1 Department/Sub Department !</p></div>'; 
		}
		
		$this->render('index',array(
			'msg'=>$msg,
			'count'=>$count
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new Inventory;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		// check inventory initialize/not
		if(isset($_POST['qty']) && !empty($_POST['Inventory']['invNo']))
		{
			// get invinitilize datetime
			$invIniDateTimeModel = InvSettings::model()->find('invNo=:invNo',array(':invNo'=>$_POST['Inventory']['invNo']));
			if(!empty($invIniDateTimeModel)) $invIniDateTime = $invIniDateTimeModel->crAt;
			
			// processing sales details
			foreach($_POST['qty'] as $key=>$qty) :
				if($qty>0) :
     				$ptr = 0;
					$model = new Inventory;
					$model->invNo = $_POST['Inventory']['invNo'];
					$model->itemId = $_POST['proId'][$_POST['pk'][$key]];	
					$model->qty = $qty;	
					
					// find adjust quantity
					$itemStock = Stock::getItemWiseStockWhileInventory($_POST['proId'][$_POST['pk'][$key]],$invIniDateTime);
					$prevSum = Inventory::getInvItemQtySumByInvNo($_POST['Inventory']['invNo'],$_POST['proId'][$_POST['pk'][$key]]);
					$prevAdjQty = Inventory::getInvAdjItemQtySumByInvNo($_POST['Inventory']['invNo'],$_POST['proId'][$_POST['pk'][$key]]);
					
					$model->adjustQty = ($qty+$prevSum) - ($itemStock+$prevAdjQty);
					$model->costPrice = $_POST['sellPriceAmount'][$_POST['pk'][$key]];
					$model->invDate = date("Y-m-d H:i:s", time() + $key+1);
					$model->save();
					$ptr = 1;
					//echo $qty.'--->'.$itemStock.'--->'.$prevSum.'-->'.$model->adjustQty.'<br/>';
				endif;
			endforeach;	
			
			if($ptr==1) :				
				// inventory summary 
				$invSummary = new InvSummary;
				$invSummary->invNo = $_POST['Inventory']['invNo'];
                $invSummary->totalQty = $_POST['totalQty'];
				$invSummary->totalWeight = $_POST['totalWeight'];
				$invSummary->totalPrice = $_POST['totalPrice'];
				
				// get total adjQty by inv
				$prevTotalAdjQtySummary = InvSummary::getInvItemAdjQtySumByInvNo($_POST['Inventory']['invNo']);
				$latestTotalAdjQtyDetails = Inventory::getInvAdjItemQtySumByInvNoOnly($_POST['Inventory']['invNo']);
				$invSummary->totalAdjustQty = $latestTotalAdjQtyDetails - ($prevTotalAdjQtySummary);
                $invSummary->totalAdjPrice = Inventory::getInvAdjItemPriceSumByInvNoOnly($_POST['Inventory']['invNo']);
                //echo '<pre>'; print_r($invSummary); exit();

				$invSummary->save();
				$msg = '<div class="notification note-success"><p>Inventory Added Successfully</p></div>';
				$model=new Inventory;
			endif;
		}
		else $msg = '<div class="notification note-error"><p>Please input Inventory Number !</p></div>';
		 
		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/** inventory update
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
    {
        $msg = '';
		$model=new Inventory;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		
		// check inventory initialize/not
		if(isset($_POST['qty']) && !empty($_POST['Inventory']['invNo']))
		{
            echo count($_POST['qty']).'<hr/>';
            echo '<pre>'; print_r($_POST['qty']); exit();

            /*
			// get invinitilize datetime
			$invIniDateTimeModel = InvSettings::model()->find('invNo=:invNo',array(':invNo'=>$_POST['Inventory']['invNo']));
			if(!empty($invIniDateTimeModel)) $invIniDateTime = $invIniDateTimeModel->crAt;
			
			// processing sales details
			$totalQty = $totalWeight = $totalPrice = 0;
			foreach($_POST['qty'] as $key=>$qty) :
				if($qty>0) :
     				$ptr = 0;
					$model = Inventory::model()->findByPk($_POST['pk'][$key]);
                    $model->qty = $qty;

                    // summary calculations
                    if($model->item->isWeighted=='yes') $totalWeight+=$qty;
                    else $totalQty+=$qty;
                    $totalPrice+=($qty*$model->costPrice);

                    // find adjust quantity
                    $itemStock = Stock::getItemWiseStockWhileInventory($model->itemId,$invIniDateTime);
                    $prevSum = Inventory::getInvItemQtySumByInvNo($_POST['Inventory']['invNo'],$model->itemId);
                    $prevAdjQty = Inventory::getInvAdjItemQtySumByInvNo($_POST['Inventory']['invNo'],$model->itemId);

                    $model->adjustQty = ($qty+$prevSum) - ($itemStock+$prevAdjQty);
                    //$model->save();
                    $ptr = 1;
				endif;
			endforeach;

			if($ptr==1) :
				// inventory summary 
				$invSummary = new InvSummary;
				$invSummary->invNo = $_POST['Inventory']['invNo'];
				$invSummary->totalQty = $totalQty;
				$invSummary->totalWeight = $totalWeight;
				$invSummary->totalPrice = $totalPrice;
				
				// get total adjQty by inv
				$prevTotalAdjQtySummary = InvSummary::getInvItemAdjQtySumByInvNo($_POST['Inventory']['invNo']);
				$latestTotalAdjQtyDetails = Inventory::getInvAdjItemQtySumByInvNoOnly($_POST['Inventory']['invNo']);
				$invSummary->totalAdjustQty = $latestTotalAdjQtyDetails - ($prevTotalAdjQtySummary);
                $invSummary->totalAdjPrice = Inventory::getInvAdjItemPriceSumByInvNoOnly($_POST['Inventory']['invNo']);

				//$invSummary->save();
				$msg = '<div class="notification note-success"><p>Inventory Added Successfully</p></div>';
				$model=new Inventory;
			endif;*/
		}
		else $msg = '<div class="notification note-error"><p>Please input Inventory Number !</p></div>';

		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/** inventory approved
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionApproved()
	{
    	$msg = '';
		$model=new Inventory;

		if(isset($_POST['Inventory']))
		{
			// update inventory
			$sql = "update pos_inventory set status=".Inventory::STATUS_ACTIVE.", moAt='".date("Y-m-d H:i:s")."' where invNo='".$_POST['Inventory']['invNo']."' AND branchId=".Yii::app()->session['branchId']." AND status=".Inventory::STATUS_INACTIVE; 
			$command = Yii::app()->db->createCommand($sql);
			$command->execute();
						
			// update inventory summary
			$sqlsummary = "update pos_inv_summary set status=".InvSummary::STATUS_ACTIVE.", moAt='".date("Y-m-d H:i:s")."' where invNo='".$_POST['Inventory']['invNo']."'";
			$command = Yii::app()->db->createCommand($sqlsummary);
			$command->execute();
						
			$msg = "Inventory Approved & Adjust Successfully";
		}

		$this->render('approved',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	/** inventory close/stop
	 * Lists all models.
	 */
	public function actionInventoryClose()
	{
		$msg = '';
		$count = 0;
		
		// check how many inventory done
		$invModel = Inventory::model()->findAll();
		if(!empty($invModel)) $count = count($invModel);
		
		if(isset($_POST['yt0']))
		{
			if(!empty($_POST['subdeptId'])) 
			{
				// check alreday initialize/not
				$subDeptInvModel = InvSettings::model()->find('subDeptId=:subDeptId and status=:status',array(':subDeptId'=>$_POST['subdeptId'],':status'=>InvSettings::STATUS_ACTIVE));
				if(!empty($subDeptInvModel)) 
				{
					InvSettings::model()->updateByPk($subDeptInvModel->id,array('status'=>InvSettings::STATUS_INACTIVE));
					$msg = '<div class="notification note-success"><p>Inventory Close Successfully for this Sub Department !</p></div>';
					
					// close inventory items
					$sql = "update pos_inventory set status=".Inventory::STATUS_CLOSE.", moAt='".date("Y-m-d H:i:s")."' where branchId=".Yii::app()->session['branchId']." and status=".Inventory::STATUS_ACTIVE." and itemId IN (SELECT i.id FROM pos_items i, pos_category c WHERE i.catId=c.id AND c.subdeptId IN(".$_POST['subdeptId']."))";
					$command = Yii::app()->db->createCommand($sql);
					$command->execute();
				}
				else $msg = '<div class="notification note-error"><p>Inventory not Initialize for this Sub Department !</p></div>';
			}
			else if(!empty($_POST['deptId'])) // check by department
			{
				// check alreday initialize/not
				$criteria=new CDbCriteria;
				$criteria->condition = 'status like :status';
	     		$criteria->params = array(':status'=>Subdepartment::STATUS_ACTIVE);
				
				$subDeptArr = array();
				if($_POST['deptId']=='all') $subdeptAll = Subdepartment::model()->findAll('status like :status',array(':status'=>Subdepartment::STATUS_ACTIVE));
				else $subdeptAll = Subdepartment::model()->findAll('deptId like :deptId and status like :status',array(':deptId'=>$_POST['deptId'],':status'=>Subdepartment::STATUS_ACTIVE));
				
				if(!empty($subdeptAll)) :
					foreach($subdeptAll as $data) $subDeptArr[] = $data->id;
				endif; 
				$criteria->addInCondition('subDeptId',$subDeptArr);
				$subDeptInvModel = InvSettings::model()->findAll($criteria);
				
				if(!empty($subDeptInvModel)) 
				{
					foreach($subDeptInvModel as $key=>$data) : 							
						InvSettings::model()->updateByPk($data->id,array('status'=>InvSettings::STATUS_INACTIVE));
						$msg.= '<div class="notification note-success"><p>Inventory Close Successfully for Sub Department : '.$data->subDept->name.' !</p></div>';
						
						// close inventory items
						$sql = "update pos_inventory set status=".Inventory::STATUS_CLOSE.", moAt='".date("Y-m-d H:i:s")."' where branchId=".Yii::app()->session['branchId']." 
								and status=".Inventory::STATUS_ACTIVE." and itemId IN (SELECT i.id FROM pos_items i, pos_category c WHERE i.catId=c.id AND c.subdeptId IN(".$data->subDept->id."))";
						$command = Yii::app()->db->createCommand($sql);
						$command->execute();
					endforeach;
				}
				else $msg.= '<div class="notification note-error"><p>Inventory not Initialize for this Department !</p></div>';	
			}
			else $msg = '<div class="notification note-error"><p>Select at lesat 1 Department/Sub Department !</p></div>'; 
		}
		
		$this->render('close',array(
			'msg'=>$msg,
			'count'=>$count
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new Inventory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Inventory']))
			$model->attributes=$_GET['Inventory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Inventory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Inventory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Inventory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='inventory-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
