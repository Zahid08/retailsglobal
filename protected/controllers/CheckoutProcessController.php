<?php
/*********************************************************
        -*- File: CheckoutProcessController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.01.26
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.16
/*********************************************************/
class CheckoutProcessController extends Controller
{
	/**
	 * @var string the default layout for the @views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/@views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
    public $layout =  '//layouts/eshop_checkout_column';
	public $defaultAction = 'process';
	
	/**
	 * Declares class-based actions.
	 */		
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/@views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    // global permission to all cont actions
    public function accessRules()
    {
        return array(
            array('allow',
                'users'=>array('*')
            ),
        );
    }

    // customer checkout ist step signin
    public function actionCheckout()
    {
        $errMsg = '';
        $this->metaTitle = 'Checkout Process';
        $model = new CustomerCheckout;

        // after post data
        if(Yii::app()->request->isPostRequest)
        {
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];
            $customerModel = Customer::model()->find(array('condition'=>'email=:email', 'params'=>array(':email'=>$email)));
            if(!empty($customerModel))
            {
                $userModel = User::model()->find(array('condition'=>'custId=:custId', 'params'=>array(':custId'=>$customerModel->id)));
                if(!empty($userModel)) $username = $userModel->username;
                else $username = '';
            }
            else $username = '';

            if(!empty($email))
            {
                // check for authentications
                if(!empty($password))
                {
                    $model = new LoginForm;
                    $model->branchId = $_REQUEST['branchId'];
                    $model->username = $username;
                    $model->password =   $password;

                    // validate user input and redirect to the previous page if valid
                    if($model->validate() && $model->login()) $this->redirect('checkoutShipping');
                    else $errMsg = 'Email OR Password is incorrect, please try again !';
                }
                else
                {
                    if(empty($customerModel))
                    {
                        if(isset(Yii::app()->session['cust_email']) && !empty(Yii::app()->session['cust_email'])) unset(Yii::app()->session['cust_email']);
                        Yii::app()->session['cust_email'] = $email;
                        $this->redirect(array('checkoutShipping'));
                    }
                    else $errMsg = 'Email already exists, please try another !';
                }
            }
            else $errMsg = 'Email can,t be empty, please input !';
        }
        $this->render('checkoutSignin',array(
            'model'=>$model,'errMsg'=>$errMsg,
            //'cart'=>$cart,'total_qty'=>$total_qty,'total_price'=>$total_price,
        ));
    }

    // customer checkout 2nd step shipping
    public function actionCheckoutShipping()
    {
        $this->metaTitle = 'Billing & Shipping Details';

        $errMsg = $billingStr = $shippingStr = '';
        $modelBillingValidate = $modelShippingValidate = 0;
        $shipping = 'yes'; $shDisplay = 'style="display:none;"';

        $modelCheckout = new CustomerCheckout;
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidationCustomerCheckout($modelCheckout);

        // customer auth check
        if(!empty(Yii::app()->session['custId']))
        {
            $model = Customer::model()->findByPk(Yii::app()->session['custId']);
            $postModel = 'Customer';
        }
        else
        {
            $model = new CustomerCheckout;
            if(isset(Yii::app()->session['cust_email']) && !empty(Yii::app()->session['cust_email'])) $model->email = Yii::app()->session['cust_email'];
            $postModel = 'CustomerCheckout';
        }

        // after post data
        if(isset($_POST[$postModel]))
        {
            $model->attributes = $_POST[$postModel];

            // billing shipping different
            if(isset($_REQUEST['optionsRadiosShipping']) && !empty($_REQUEST['optionsRadiosShipping']) && $_REQUEST['optionsRadiosShipping']=='no')
            {
                // shipping initiate
                $modelCheckout->attributes = $_POST['CustomerCheckout'];
                if(!empty(Yii::app()->session['custId'])) // checkout model values set for validations
                {
                    $modelCheckout->title = $model->title;
                    $modelCheckout->name = $model->name;
                    $modelCheckout->addressline = $model->addressline;
                    $modelCheckout->city = $model->city;
                    $modelCheckout->zipCode = $model->zipCode;
                    $modelCheckout->email = $model->email;
                    $modelCheckout->phone = $model->phone;
                }
                $shipping = 'no';
                $shDisplay = 'style="display:block;"';
                $modelCheckout->isShipping = CustomerCheckout::IS_SHIPPING;

                if($model->validate()) // billing processing
                {
                    if(isset(Yii::app()->session['cust_billing_address']) && !empty(Yii::app()->session['cust_billing_address'])) unset(Yii::app()->session['cust_billing_address']);
                    $billingStr.='<span>Name : '.$model->title.'&nbsp;'.$model->name.'</span><br/>';
                    $billingStr.='<span>Address : '.$model->addressline.',&nbsp;'.$model->city.',&nbsp;'.$model->zipCode.'</span><br/>';
                    $billingStr.='<span>Email : '.$model->email.'</span><br/>';
                    $billingStr.='<span class="tel">Phone : '.$model->phone.'</span><br/>';
                    Yii::app()->session['cust_billing_address'] = Yii::app()->session['cust_shipping_address'] = $billingStr;
                    $modelBillingValidate = 1;
                }
                if($modelCheckout->validate()) // shipping processing
                {
                    if(isset(Yii::app()->session['cust_shipping_address']) && !empty(Yii::app()->session['cust_shipping_address'])) unset(Yii::app()->session['cust_shipping_address']);
                    $shippingStr.='<span>Name : '.$modelCheckout->sh_fname.'&nbsp;'.$modelCheckout->sh_lastname.'</span><br/>';
                    $shippingStr.='<span>Address : '.$modelCheckout->sh_streetaddress.',&nbsp;'.$modelCheckout->sh_city.',&nbsp;'.$modelCheckout->sh_zip.'</span><br/>';
                    $shippingStr.='<span>Email : '.$model->email.'</span><br/>';
                    $shippingStr.='<span class="tel">Phone : '.$modelCheckout->sh_phone.'</span><br/>';
                    Yii::app()->session['cust_shipping_address'] = $shippingStr;
                    $modelShippingValidate = 1;
                }
                if($modelBillingValidate==1 && $modelShippingValidate==1) $this->redirect(array('checkoutShippingOptions'));
            }
            else // billing shipping same
            {
                $model->isShipping = CustomerCheckout::IS_NOT_SHIPPING;
                if($model->validate())
                {
                    if(isset(Yii::app()->session['cust_billing_address']) && !empty(Yii::app()->session['cust_billing_address'])) unset(Yii::app()->session['cust_billing_address']);
                    if(isset(Yii::app()->session['cust_shipping_address']) && !empty(Yii::app()->session['cust_shipping_address'])) unset(Yii::app()->session['cust_shipping_address']);

                    $billingStr.='<span>Name : '.$model->title.'&nbsp;'.$model->name.'</span><br/>';
                    $billingStr.='<span>Address : '.$model->addressline.',&nbsp;'.$model->city.',&nbsp;'.$model->zipCode.'</span><br/>';
                    $billingStr.='<span>Email : '.$model->email.'</span><br/>';
                    $billingStr.='<span class="tel">Phone : '.$model->phone.'</span><br/>';
                    Yii::app()->session['cust_billing_address'] = Yii::app()->session['cust_shipping_address'] = $billingStr;
                    $this->redirect(array('checkoutShippingOptions'));
                }
            }
        }

        $this->render('checkoutShipping',array(
            'errMsg'=>$errMsg,
            'model'=>$model,'modelCheckout'=>$modelCheckout,
            'shipping'=>$shipping,'shDisplay'=>$shDisplay,
        ));
    }

    // checkout 3rd step shipping Options
    public function actionCheckoutShippingOptions()
    {
        $this->metaTitle = 'Shipping Options';
        $errMsg = '';
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));

        // after post data
        if(Yii::app()->request->isPostRequest)
        {
            if(!empty($_REQUEST['shippingMethod']))
            {
                if(isset(Yii::app()->session['cust_shipping_method']) && !empty(Yii::app()->session['cust_shipping_method'])) unset(Yii::app()->session['cust_shipping_method']);
                Yii::app()->session['cust_shipping_method'] = $_REQUEST['shippingMethod'];
                $this->redirect(array('checkoutPaymentOptions'));
            }
            else $errMsg = 'Please select at least 1 shipping method !';
        }
        $this->render('checkoutShippingOptions',array(
            'companyModel'=>$companyModel,'errMsg'=>$errMsg
        ));
    }

    // checkout 4th step shipping Options
    public function actionCheckoutPaymentOptions()
    {
        $this->metaTitle = 'Payment Options';
        $errMsg = '';
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));

        // after post data
        if(Yii::app()->request->isPostRequest)
        {
            if(!empty($_REQUEST['paymentMethod']))
            {
                if(isset(Yii::app()->session['cust_payment_method']) && !empty(Yii::app()->session['cust_payment_method'])) unset(Yii::app()->session['cust_payment_method']);
                Yii::app()->session['cust_payment_method'] = $_REQUEST['paymentMethod'];
                $this->redirect(array('checkoutReview'));
            }
            else $errMsg = 'Please select at least 1 payment method !';
        }
        $this->render('checkoutPaymentOptions',array(
            'companyModel'=>$companyModel,'errMsg'=>$errMsg
        ));
    }

    // checkout final and review
    public function actionCheckoutReview()
    {
        $this->metaTitle = 'Final Checkout';
        $errMsg = $cart_items = '';
        $shippingModel = $paymentModel = array();
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));

        // shopping cart items
        $cart = new Cart('cart');
        $total_qty = $total_price = $total_price_vat = $total_price_discount = 0;
        if($cart->hasItems()) :
            foreach ($cart->getItems() as $order_code=>$quantity ) :
                $cart_items.= $cart->getItemName($order_code).',';
                $total_qty = $total_qty + $quantity;
                /*if(Items::getItemWiseOfferPrice($order_code)>0)
                    $total_price += $quantity*Items::getItemWiseOfferPrice($order_code);*/
                $total_price += $quantity*$cart->getItemPrice($order_code);
                $total_price_vat+= $quantity*Items::getItemPriceWithVatOri($order_code);
                $total_price_discount+= $quantity*Items::getItemWiseOfferOriPrice($order_code);
            endforeach;
        endif;

        // shipping & payment method
        if(!empty(Yii::app()->session['cust_shipping_method'])) $shippingModel = ShippingMethod::model()->findByPk(Yii::app()->session['cust_shipping_method']);
        if(!empty(Yii::app()->session['cust_payment_method'])) $paymentModel = PaymentMethod::model()->findByPk(Yii::app()->session['cust_payment_method']);

        // customer information
        if(!empty(Yii::app()->session['custId']))
        {
            $customerModel = Customer::model()->find(Yii::app()->session['custId']);
            $customerName = $customerModel->title.'&nbsp;'.$customerModel->name;
            $customerEmail = $customerModel->email;
        }
        else // get from session billing
        {
            $billingArr = $customerNameArr = array();
            $billingArr = explode('</span>',Yii::app()->session['cust_billing_address']);
            if(!empty($billingArr))
            {
                $customerNameArr = explode(':',$billingArr[0]);
                $customerName = (!empty($customerNameArr))?$customerNameArr[1]:'';
                $customerEmail = Yii::app()->session['cust_email'];
            }
        }

        // after post data
        if(Yii::app()->request->isPostRequest)
        {
            // save to sales invoice and details
            if($cart->hasItems())
            {
                // save to sales invoice
                $modelSalesInvoice = new SalesInvoice;
                $modelSalesInvoice->invNo = 'IN'.date("Ymdhis");
                $modelSalesInvoice->custId 		= (!empty(Yii::app()->session['custId']))?Yii::app()->session['custId']:Customer::DEFAULT_CUST;
                $modelSalesInvoice->totalQty 	= $_REQUEST['total_qty'];
                $modelSalesInvoice->totalPrice 	= $_REQUEST['total_price']-$_REQUEST['total_price_vat'];
                $modelSalesInvoice->totalDiscount 	= $_REQUEST['total_price_discount'];
                $modelSalesInvoice->totalTax 	= $_REQUEST['total_price_vat'];
                $modelSalesInvoice->totalShipping = $_REQUEST['total_shipping'];
                $modelSalesInvoice->isEcommerce = SalesInvoice::IS_ECOMMERCE;
                $modelSalesInvoice->billingAddress = Yii::app()->session['cust_billing_address'];
                $modelSalesInvoice->shippingAddress = Yii::app()->session['cust_shipping_address'];
                $modelSalesInvoice->shippingId = (!empty(Yii::app()->session['cust_shipping_method']))?Yii::app()->session['cust_shipping_method']:0;
                $modelSalesInvoice->paymentId = (!empty(Yii::app()->session['cust_payment_method']))?Yii::app()->session['cust_payment_method']:0;
                $modelSalesInvoice->status	= SalesInvoice::STATUS_PROCESSING;

                if($modelSalesInvoice->save())
                {
                    // customer cart product insert
                    foreach ($cart->getItems() as $order_code=>$quantity)
                    {
                        $modelSalesDetails = new SalesDetails;
                        $modelSalesDetails->salesId = $modelSalesInvoice->id;
                        $modelSalesDetails->itemId = $order_code;
                        $modelSalesDetails->qty = $quantity;
                        $modelSalesDetails->costPrice = $cart->getItemCostPrice($order_code);
                        $modelSalesDetails->salesPrice 	= $cart->getItemSalesPrice($order_code);
                        $modelSalesDetails->discountPrice = $quantity*Items::getItemWiseOfferOriPrice($order_code);
                        $modelSalesDetails->vatPrice = $quantity*Items::getItemPriceWithVatOri($order_code);
                        $itemPrice = (Items::getItemWiseOfferPrice($order_code)>0)?Items::getItemWiseOfferPrice($order_code):$cart->getItemPrice($order_code);
                        $modelSalesDetails->shippingPrice = ShippingMethod::getShippingMethodPrice($modelSalesInvoice->shippingId,$quantity*$itemPrice);
                        $modelSalesDetails->status = SalesDetails::STATUS_INACTIVE;
                        if($modelSalesDetails->save())
                        {
                            $cart->setItemQuantity($order_code,0);
                            $cart->save();
                        }
                    }
                    // payment process & redirection
                    if($paymentModel->apiShortName==PaymentMethod::API_GENERAL)
                        $this->redirect(array('checkoutSuccessEmail','id'=>$modelSalesInvoice->id));
                    else $this->redirect(array('checkoutPaymentProcess','id'=>$modelSalesInvoice->id));
                }
            }
        }
        $this->render('checkoutReview',array(
            'errMsg'=>$errMsg,
            'companyModel'=>$companyModel,'customerName'=>$customerName,'customerEmail'=>$customerEmail,
            'shippingModel'=>$shippingModel,'paymentModel'=>$paymentModel,
            'cart'=>$cart,'total_qty'=>$total_qty,'total_price'=>$total_price,'cart_items'=>$cart_items,
            'total_price_vat'=>$total_price_vat,'total_price_discount'=>$total_price_discount,
        ));
    }

    // payment method processing
    public function actionCheckoutPaymentProcess($id)
    {
        $modelSalesInvoice = SalesInvoice::model()->findByPk($id);
        if(!empty($modelSalesInvoice))
        {
            // global settings
            $protocol = 'http://';
            if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https://';
            $queryString = '';
            $successUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutSuccess';
            $failureUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutFailure';
            $cancelUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutCancel';

            // customer information
            if(!empty($modelSalesInvoice->custId) && $modelSalesInvoice->custId>0)
            {
                $customerModel = Customer::model()->find($modelSalesInvoice->custId);
                $customerName = $customerModel->title.'&nbsp;'.$customerModel->name;
                $customerEmail = $customerModel->email;
                $customerPhone = $customerModel->phone;
                $customerAddress = $customerModel->addressline;
                $customerCity = $customerModel->city;
                $customerZip = '1215';
            }
            else // get from session billing
            {
                $billingArr = $customerNameArr = $customerAddressArr = $customerAddressFinalArr = $customerPhoneArr = array();
                $billingArr = explode('</span>',Yii::app()->session['cust_billing_address']);
                if(!empty($billingArr))
                {
                    $customerNameArr = explode(':',$billingArr[0]);
                    $customerAddressArr = explode(':',$billingArr[1]);
                    $customerAddressFinalArr = explode(',',$customerAddressArr[1]);
                    $customerPhoneArr = explode(':',$billingArr[3]);

                    $customerName = (!empty($customerNameArr))?$customerNameArr[1]:'';
                    $customerEmail = Yii::app()->session['cust_email'];
                    $customerPhone = (!empty($customerPhoneArr))?$customerPhoneArr[1]:'';
                    $customerAddress = (!empty($customerAddressFinalArr))?$customerAddressFinalArr[0]:'';
                    $customerCity = (!empty($customerAddressFinalArr))?$customerAddressFinalArr[1]:'';
                    $customerZip = (!empty($customerAddressFinalArr))?$customerAddressFinalArr[2]:'';
                }
            }

            // payment gateway processing
            $paymentMethodModel = PaymentMethod::model()->findByPk($modelSalesInvoice->paymentId);
            if(!empty($paymentMethodModel))
            {
                // paypal processing
                if($paymentMethodModel->apiShortName==PaymentMethod::API_PAYPAL)
                {
                    $paypalEmail = 'info@unlocklive.com'; // 'zaimovic.r@gmail.com';
                    $queryString .= "?business=".urlencode($paypalEmail)."&";
                    $queryString .= "upload=1&";
                    $queryString .= "no_shipping=1&";
                    $queryString .= "cmd=_cart&";

                    $queryString .= "currency_code=USD&";
                    $queryString .= "lc=USD&";
                    $queryString .= "rm=2&";

                    $queryString .= "custom=".$id."&";
                    //$queryString .= "order_id=".$order_id."&";
                    $queryString .= "invoice=".urlencode($modelSalesInvoice->invNo)."&";
                    $queryString .= "lc_1=lc_1&";
                    //$queryString .= "quantity=".$total_itemm_qty."&"; // not working
                    $queryString .= "item_number_1=".$modelSalesInvoice->totalQty."&";
                    $queryString .= "item_name_1=".urlencode(SalesDetails::getAllItemNameByInvoice($id))."&";
                    $queryString .= "amount_1=".(($modelSalesInvoice->totalPrice+$modelSalesInvoice->totalTax)-$modelSalesInvoice->totalDiscount)."&";

                    $queryString .= "redirect_cmd=_xclick&";
                    $queryString .= "return=".urlencode($successUrl)."&";
                    $queryString .= "notify_url=".urlencode($failureUrl)."&";
                    $queryString .= "cancel_return=".urlencode($cancelUrl)."&";

                    header('location:https://www.paypal.com/cgi-bin/webscr'.$queryString);
                    //header('location:https://sandbox.paypal.com/cgi-bin/webscr'.$queryString);
                    exit();
                }
                // skrill processing
                else if($paymentMethodModel->apiShortName==PaymentMethod::API_SKRILL)
                {
                    $skrilEmail = 'zaimovic.r@gmail.com';
                    $queryString .= "?pay_to_email=".urlencode($skrilEmail)."&";
                    $queryString .= "status_url=".urlencode($successUrl);
                    $queryString .= "language=EN&";
                    $queryString .= "amount=".($modelSalesInvoice->totalPrice+$modelSalesInvoice->totalTax)."&";
                    $queryString .= "currency=USD&";
                    $queryString .= "detail1_description=".urlencode(SalesDetails::getAllItemNameByInvoice($id))."&";
                    $queryString .= "detail1_text=License&";
                    $queryString .= "transaction_id=".urlencode($modelSalesInvoice->invNo)."&";
                    header('location:https://www.moneybookers.com/app/payment.pl'.$queryString);
                    exit();
                }
                else $this->render('checkoutFailure');
            }
        }
    }

    // checkout success page
    public function actionCheckoutSuccess()
    {
        //echo '<pre>';print_r($_REQUEST);echo '</pre>'; exit();
        $this->metaTitle = 'Payment Successful';
        $discountShippingArr = array();
        $cart = new Cart('cart');
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                       array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        // success payment method process with easy pay
        if(!empty(Yii::app()->session['cust_payment_method'])) $paymentMethodModel = PaymentMethod::model()->findByPk(Yii::app()->session['cust_payment_method']);
        if($paymentMethodModel->apiShortName==PaymentMethod::API_EASY_PAY && $_REQUEST['pay_status']=='success')
        {
            // save to sales invoice and details
            if($cart->hasItems())
            {
                // save to sales invoice
                $modelSalesInvoice = new SalesInvoice;
                $modelSalesInvoice->invNo = $_REQUEST['mer_txnid'];
                $modelSalesInvoice->custId 		= (!empty(Yii::app()->session['custId']))?Yii::app()->session['custId']:Customer::DEFAULT_CUST;
                $modelSalesInvoice->totalQty 	= $_REQUEST['opt_a'];
                $modelSalesInvoice->totalPrice 	= $_REQUEST['opt_b']-$_REQUEST['opt_c'];
                $modelSalesInvoice->totalTax 	= $_REQUEST['opt_c'];

                // shipping,discount # process
                $discountShippingArr = explode('#',$_REQUEST['opt_d']);
                if(!empty($discountShippingArr)) {
                    $modelSalesInvoice->totalShipping = $discountShippingArr[0];
                    $modelSalesInvoice->totalDiscount = $discountShippingArr[1];
                }
                $modelSalesInvoice->isEcommerce = SalesInvoice::IS_ECOMMERCE;
                $modelSalesInvoice->billingAddress = Yii::app()->session['cust_billing_address'];
                $modelSalesInvoice->shippingAddress = Yii::app()->session['cust_shipping_address'];
                $modelSalesInvoice->shippingId = (!empty(Yii::app()->session['cust_shipping_method']))?Yii::app()->session['cust_shipping_method']:0;
                $modelSalesInvoice->paymentId = (!empty(Yii::app()->session['cust_payment_method']))?Yii::app()->session['cust_payment_method']:0;
                $modelSalesInvoice->status	= SalesInvoice::STATUS_PROCESSING;

                if($modelSalesInvoice->save())
                {
                    // customer cart product insert
                    foreach($cart->getItems() as $order_code=>$quantity)
                    {
                        $modelSalesDetails = new SalesDetails;
                        $modelSalesDetails->salesId = $modelSalesInvoice->id;
                        $modelSalesDetails->itemId = $order_code;
                        $modelSalesDetails->qty = $quantity;
                        $modelSalesDetails->costPrice = $cart->getItemCostPrice($order_code);
                        $modelSalesDetails->salesPrice 	= $cart->getItemSalesPrice($order_code);
                        $modelSalesDetails->discountPrice = $quantity*Items::getItemWiseOfferOriPrice($order_code);
                        $modelSalesDetails->vatPrice = $quantity*Items::getItemPriceWithVatOri($order_code);
                        $itemPrice = (Items::getItemWiseOfferPrice($order_code)>0)?Items::getItemWiseOfferPrice($order_code):$cart->getItemPrice($order_code);
                        $modelSalesDetails->shippingPrice = ShippingMethod::getShippingMethodPrice($modelSalesInvoice->shippingId,$quantity*$itemPrice);
                        $modelSalesDetails->status = SalesDetails::STATUS_INACTIVE;
                        if($modelSalesDetails->save())
                        {
                            $cart->setItemQuantity($order_code,0);
                            $cart->save();
                        }
                    }
                    // payment process & redirection
                    $this->redirect(array('checkoutSuccessEmail','id'=>$modelSalesInvoice->id));
                }
            }
        }
        // success payment method process with paypal
        else if($paymentMethodModel->apiShortName==PaymentMethod::API_PAYPAL && $_REQUEST['payment_status']=='Completed')
        {
            $this->redirect(array('checkoutSuccessEmail','id'=>$_REQUEST['custom']));
        }
    }

    // checkout success email page
    public function actionCheckoutSuccessEmail($id)
    {
        $this->metaTitle = 'Payment Successful';
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                       array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        $orderModel = SalesInvoice::model()->findByPk($id);
        $orderDetailsModel = SalesDetails::model()->findAll(array('condition'=>'salesId=:salesId','params'=>array(':salesId'=>$id),'order'=>'crAt DESC'));

        // customer information
        if(!empty($orderModel->custId) && $orderModel->custId>0) $customerEmail = $orderModel->cust->email;
        else $customerEmail = Yii::app()->session['cust_email'];

        // email goes here $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $body = $this->renderPartial('checkoutEmail',array(
                'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
                'orderModel'=>$orderModel,
                'orderDetailsModel'=>$orderDetailsModel),true
        );
        Yii::app()->mailer->IsHTML(true);
        Yii::app()->mailer->SMTPAuth = true;
        Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
        Yii::app()->mailer->From = $companyModel->email; //'admin@unlockliveretail.com';
        Yii::app()->mailer->FromName = $companyModel->name;
        Yii::app()->mailer->AddReplyTo('admin@unlockliveretail.com');
        Yii::app()->mailer->AddAddress($customerEmail);
        Yii::app()->mailer->Subject = 'Order confirmation from '.$companyModel->name;
        Yii::app()->mailer->Body = $body;
        if(Yii::app()->mailer->Send())
        {
            // reset cart and session
            unset(Yii::app()->session['cust_email']);
            unset(Yii::app()->session['cust_billing_address']);
            unset(Yii::app()->session['cust_shipping_address']);
            unset(Yii::app()->session['cust_payment_method']);
            unset(Yii::app()->session['cust_shipping_method']);
            $this->render('checkoutSuccess');
        }
        else $this->render('checkoutFailure');
    }

    // checkout pending page
    public function actionCheckoutFailure()
    {
        $this->metaTitle = 'Payment Pending';
        $this->render('checkoutFailure');
    }

    // checkout cancel page
    public function actionCheckoutCancel()
    {
        $this->metaTitle = 'Payment Cancelled';
        $this->render('checkoutCancel');
    }

    /*
     * Performs the AJAX validation for customer checkout
     * @param Customer checkout $modelCheckout the model to be validated
     */
    protected function performAjaxValidationCustomerCheckout($modelCheckout)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='billing-shipping-form')
        {
            echo CActiveForm::validate($modelCheckout);
            Yii::app()->end();
        }
    }
}
