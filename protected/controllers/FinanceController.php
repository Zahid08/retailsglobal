<?php
/*********************************************************
        -*- File: FinanceController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.04.11
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class FinanceController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new Finance;
        $model->isBank = SupplierWidraw::IS_CASH;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Finance']))
		{
			$model->attributes=$_POST['Finance'];
            // bank a/c processing
            if(!empty($_POST['Finance']['bankAcNo']))
            {
                $model->bankId = Bank::getBankByAccount($_POST['Finance']['bankAcNo']);
                if($model->amount>Bank::getBankAccountCurrentBalance($model->bankId))
                {
                    $msg = '<div class="notification note-error"><p>Expense Amount is greater than Bank Balance !</p></div>';
                    $this->render('_form',array(
                        'model'=>$model,
                        'msg'=>$msg,
                    ));
                    exit();
                }
            }

            if($model->save())
            {
                $msg = '<div class="notification note-success"><p>Misc. Income/Expense Saved Successfully</p></div>';
                $model=new Finance;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);
        ($model->bankId>0)?$model->isBank = SupplierWidraw::IS_BANK:$model->isBank = SupplierWidraw::IS_CASH;
        if($model->bankId>0) $model->bankId = $model->bank->name;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Finance']))
		{
			$model->attributes=$_POST['Finance'];

            // bank a/c processing
            if(!empty($_POST['Finance']['bankAcNo']))
            {
                $model->bankId = Bank::getBankByAccount($_POST['Finance']['bankAcNo']);
                if($model->amount>Bank::getBankAccountCurrentBalance($model->bankId))
                {
                    $msg = '<div class="notification note-error"><p>Expense Amount is greater than Bank Balance !</p></div>';
                    $this->render('_form',array(
                        'model'=>$model,
                        'msg'=>$msg,
                    ));
                    exit();
                }
            }

			if($model->save())
            	$msg = '<div class="notification note-success"><p>Misc. Income/Expense Updated Successfully</p></div>';
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Finance');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new Finance('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Finance']))
			$model->attributes=$_GET['Finance'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Finance the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Finance::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Finance $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='finance-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
