<?php
/*********************************************************
        -*- File: EshopController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.01.26
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.16
/*********************************************************/
class EshopController extends Controller
{
	/**
	 * @var string the default layout for the @views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/@views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
    public $layout =  '//layouts/eshop_main_column';
	public $defaultAction = 'index';
	
	/**
	 * Declares class-based actions.
	 */		
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/@views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
 * @return array action filters
 */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
	
	// global permission to all cont actions
	public function accessRules()
	{
        return array(
            array('allow',
                'users'=>array('*')
            ),
        );
	}

    // default loads
    public function actionIndex()
    {
        $this->metaTitle = 'Home';
        $itemAdvertiseModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND status=:status',
                                                            'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),
                                                            'group'=>'itemName','order'=>'rand(),crAt DESC','limit' => 2,
                                            ));

        /*$sliderModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND status=:status',
                                                    'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),
                                                    'group'=>'itemName','order'=>'rand(),crAt DESC','limit'=>5,
                                        ));*/

        $sliderModel = Sliders::model()->findAll(array('condition'=>'status=:status','params'=>array(':status'=>Sliders::STATUS_ACTIVE),
                                                       'order'=>'rand(),crAt DESC','limit'=>8,
                                        ));
        $this->render('index',array(
            'itemAdvertiseModel'=>$itemAdvertiseModel,
            'sliderModel'=>$sliderModel,
        ));
    }
    
    // all dept,subdept,category listing
    public function actionAllDeptSubDeptCat()
    {
        $this->metaTitle = 'All Category';
        $this->render('allDeptSubDeptCat',array());
    }

    // product listing page by category
    public function actionProByCat()
    {
        $brandModel = $catModel = array();
        $criteria=new CDbCriteria;

        // request data cat
        if(isset($_REQUEST['catId']) && !empty($_REQUEST['catId']))
        {
            $catId = $_REQUEST['catId'];
            $catModel = Category::model()->findByPk($catId);
            $this->metaTitle = $catModel->name;
            $criteria->condition = 'catId=:catId AND isEcommerce=:isEcommerce AND status=:status';
            $criteria->params =array(':catId'=>$catId,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE);
            $criteria->group = 'itemName';
            $criteria->order = 'crAt DESC';
            $sqlBrand = "SELECT distinct(brandId) FROM pos_items WHERE catId=".$catId." AND isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY itemName";
        }
        
        $brandModel = Items::model()->findAllBySql($sqlBrand);
        $dataProvider=new CActiveDataProvider('Items', array(
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params->defaultPageSize),
            ),
            'criteria'=>$criteria,
        ));
        $this->render('proByCat',array(
            'catModel'=>$catModel,'brandModel'=>$brandModel,
            'dataProvider'=>$dataProvider
        ));
    }

    // product listing page by item search
    public function actionProByCatSearch()
    {
        $searchItems = '';
        $criteria=new CDbCriteria;

        // items search
        if(isset($_REQUEST['searchItems']) && !empty($_REQUEST['searchItems']))
        {
            $searchItems = $_REQUEST['searchItems'];
            $match ='%'.$_REQUEST['searchItems'].'%';
            $criteria->condition = 'itemName LIKE :itemName AND isEcommerce=:isEcommerce AND status=:status';
            $criteria->params =array(':itemName'=>$match,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE);
            $criteria->group = 'itemName';
            $criteria->order = 'crAt DESC';
        }
        $dataProvider=new CActiveDataProvider('Items', array(
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params->defaultPageSize),
            ),
            'criteria'=>$criteria,
        ));
        $this->render('proByCatSearch',array(
            'searchItems'=>$searchItems,'dataProvider'=>$dataProvider
        ));
    }

    // product listing ajax filtering page by dept/subdept/cat
    public function actionProByCatAjax()
    {
        // request data processing
        $brandArr = array();
        if(isset($_POST['catId']) && !empty($_POST['catId'])) $catId = $_POST['catId'];
        if(isset($_POST['brand']) && !empty($_POST['brand']))
        {
            if (strpos($_POST['brand'],',') !== false) foreach(explode(',',$_POST['brand']) as $key=>$data)  $brandArr[] = $data;
            else $brandArr[] = $_POST['brand'];
        }

        // query filtering
        $criteria=new CDbCriteria;
        $criteria->condition = 'catId=:catId and isEcommerce=:isEcommerce and status=:status';
        $criteria->params =array(':catId'=>$catId,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE);
        if(!empty($brandArr)) $criteria->addInCondition("brandId", $brandArr);
        if(isset($_POST['startPrice']) && !empty($_POST['startPrice']) && isset($_POST['endPrice']) && !empty($_POST['endPrice']))
            $criteria->addBetweenCondition('sellPrice',$_POST['startPrice'],$_POST['endPrice']);

        $criteria->group = 'itemName';
        $criteria->order = 'crAt DESC';
        $dataProvider=new CActiveDataProvider('Items', array(
            'pagination'=>array(
                'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params->defaultPageSize),
            ),
            'criteria'=>$criteria,
        ));

        if(Yii::app()->request->isAjaxRequest)
            $this->renderPartial('proByCatAjax',array(
                'dataProvider'=>$dataProvider,'ajax'=>true
            ),false,true);
    }

    // product details page
    public function actionProDetails($id)
    {
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $model = Items::model()->findByPk($id);
        $this->metaOgUrl = 'http://www.'.$companyModel->domain.'/eshop/proDetails/'.$id;
        $this->metaOgImage = $model->itemImages[0]->image;

        /*$defaultAttTypeModel = AttributesType::model()->findAll(array('condition'=>'isDefault=:isDefault AND status=:status',
                                                                      'params'=>array(':isDefault'=>AttributesType::NOT_DEFAULT_ATTRIBUTES,':status'=>AttributesType::STATUS_ACTIVE)));*/

        $defaultAttTypeModel = AttributesType::model()->findAll(array('condition'=>'status=:status',
                                                                      'params'=>array(':status'=>AttributesType::STATUS_ACTIVE)));

        $modelReview = new ItemReview;
        $modelItemReview = ItemReview::model()->findAll(array('condition'=>'itemId=:itemId AND status=:status',
                                                              'params'=>array(':itemId'=>$id,':status'=>ItemReview::STATUS_ACTIVE),
                                                              'order'=>'crAt DESC'
                           ));
        $this->metaTitle = $model->itemName;

        $this->render('proDetails',array(
            'model'=>$model,'modelReview'=>$modelReview,'modelItemReview'=>$modelItemReview,
            'defaultAttTypeModel'=>$defaultAttTypeModel,
        ));
    }

    // change item price by attributes config
    public function actionChangeItemPriceStrByAttributes()
    {
        $priceStr = '';
        if(!empty($_POST['itemId'])) $model = Items::model()->findByPk($_POST['itemId']);
        if(Items::getItemWiseOfferPrice($model->id)>0) :
            $priceStr.='<div class="price-current">'.Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($model->id).'</div>
            <div class="price-prev" style="text-decoration: line-through;">
               '.Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id).'
            </div>';
        else :
            $priceStr.='<div class="price-current">'.Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id).'</div>';
       endif;
       echo $priceStr;
    }

    // product add to cart options
    public function actionAddToCart()
    {
        $cart = new Cart('cart');
        if(!empty($_POST['itemId']) && !empty($_POST['quantity'])) {
            $quantity = $cart->getItemQuantity($_POST['itemId']) + $_POST['quantity'];
            $cart->setItemQuantity($_POST['itemId'], $quantity);
        }
        $cart->save();
        echo json_encode($_POST['itemId']);
    }

    // update shopping bag by ajax
    public function actionUpdateCart()
    {
        $cart = new Cart('cart');
        $total_qty = $total_price = 0;
        $cart = new Cart('cart');
        if($cart->hasItems()) :
            foreach ($cart->getItems() as $order_code=>$quantity ) :
                $total_qty = $total_qty + $quantity;
                if(Items::getItemWiseOfferPrice($order_code)>0)
                    $total_price+= $quantity*Items::getItemWiseOfferPrice($order_code);
                else $total_price+= $quantity*$cart->getItemPrice($order_code);
            endforeach;
        endif;

        if(Yii::app()->request->isAjaxRequest)
        $this->renderPartial('_proByCart',array(
            'cart'=>$cart,'total_qty'=>$total_qty,'total_price'=>$total_price,
            'ajax'=>true
        ),false,true);
    }

    // remove from cart options
    public function actionRemoveFromCart()
    {
        $cart = new Cart('cart');
        if(!empty($_POST['itemId'])) $cart->setItemQuantity($_POST['itemId'], 0);
        $cart->save();
        echo json_encode($_POST['itemId']);
    }

    // cart update options
    public function actionUpdateQty()
    {
        $cartArr = array();
        $cart = new Cart('cart');
        if(!empty($_POST['itemId']) && !empty($_POST['quantity'])) {
            $cart->setItemQuantity($_POST['itemId'], $_POST['quantity']);
        }
        $cart->save();

        // finding total price
        $total_price = $total_discount = 0;
        foreach ($cart->getItems() as $order_code=>$quantity) :
            /*if(Items::getItemWiseOfferPrice($order_code)>0)
                $total_price += $quantity*Items::getItemWiseOfferPrice($order_code);*/
            $total_price += $quantity*$cart->getItemPrice($order_code);
            $total_discount += $quantity*Items::getItemWiseOfferOriPrice($order_code);
        endforeach;
        $cartArr['total_price'] = $total_price;
        $cartArr['total_discount'] = $total_discount;
        echo json_encode($cartArr);
    }

    // customer shopping bag info
    public function actionViewMyCart()
    {
        $this->metaTitle = 'Shopping Cart';
        $cart = new Cart('cart');
        $total_qty = $total_price = $total_discount = 0;
        $cart = new Cart('cart');
        if($cart->hasItems()) :
            foreach ($cart->getItems() as $order_code=>$quantity ) :
                $total_qty = $total_qty + $quantity;
                /*if(Items::getItemWiseOfferPrice($order_code)>0)
                    $total_price += $quantity*Items::getItemWiseOfferPrice($order_code);*/
                $total_price += $quantity*$cart->getItemPrice($order_code);
                $total_discount += $quantity*Items::getItemWiseOfferOriPrice($order_code);
            endforeach;
        endif;
        $this->render('viewMyCart',array(
            'cart'=>$cart,'total_qty'=>$total_qty,
            'total_price'=>$total_price,'total_discount'=>$total_discount,
        ));
    }

    // customer registration/sign up
    public function actionCustomerSignup()
    {
        $this->metaTitle = 'Customer/Supplier Signup';
        $msg = '';
        $modelSupplier = new SupplierSignup;
        $modelCustomer = new CustomerSignup;

        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                       array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidationSupplier($modelSupplier);
        $this->performAjaxValidationCustomer($modelCustomer);

        // supplier processing
        if(isset($_POST['SupplierSignup']))
        {
            // data insert by transaction
            $modelSupplierMain = new Supplier;
            $transaction = $modelSupplierMain->dbConnection->beginTransaction();
            try
            {
                // supplier processing
                $modelSupplier->attributes=$_POST['SupplierSignup'];
                $modelSupplierMain->suppId = $_POST['SupplierSignup']['suppId'];
                $modelSupplierMain->name = $_POST['SupplierSignup']['name'];
                $modelSupplierMain->address = $_POST['SupplierSignup']['address'];
                $modelSupplierMain->city = $_POST['SupplierSignup']['city'];
                $modelSupplierMain->email = $_POST['SupplierSignup']['email'];
                $modelSupplierMain->phone = $_POST['SupplierSignup']['phone'];
                $modelSupplierMain->crType = Supplier::CREDIT_SUPP;
                $modelSupplierMain->status = Supplier::STATUS_INACTIVE;

                if($modelSupplier->validate() && $modelSupplierMain->save())  // supplier save
                {
                    // user processing
                    $modelUser = new User;
                    $modelUser->branchId = Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
                    $modelUser->isSupplier = User::IS_SUPPLIER;
                    $modelUser->supplierId = $modelSupplierMain->id;
                    $modelUser->username = $_POST['SupplierSignup']['username'];
                    $salt = $modelUser->generateSalt();
                    $modelUser->password = $modelUser->hashPassword($_POST['SupplierSignup']['password'],$salt);
                    $modelUser->salt = $salt;
                    $modelUser->conf_password = $modelUser->hashPassword($_POST['SupplierSignup']['conf_password'],$salt);
                    $modelUser->totalLoginTime = 0;
                    $modelUser->status = User::STATUS_PENDING;
                    $modelUser->save();

                    // roll processing
                    $modelRoles = new Roles;
                    $modelRoles->userTypeId = UserType::getSupplierType();
                    $modelRoles->userId = $modelUser->id;
                    $modelRoles->status = Roles::STATUS_ACTIVE;
                    if($modelRoles->save())
                    {
                        $transaction->commit();
                        // email goes here $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $body = $this->renderPartial('customerRegEmail',array(
                                        'regType'=>'supplier',
                                        'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
                                        'username'=>$_POST['SupplierSignup']['username'],
                                        'password'=>$_POST['SupplierSignup']['password']),true
                                );
                        Yii::app()->mailer->IsHTML(true);
                        Yii::app()->mailer->SMTPAuth = true;
                        Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
                        Yii::app()->mailer->From = $companyModel->email; //'admin@unlockliveretail.com';
                        Yii::app()->mailer->FromName = $companyModel->name;
                        Yii::app()->mailer->AddReplyTo('admin@unlockliveretail.com');
                        Yii::app()->mailer->AddAddress($_POST['SupplierSignup']['email']);
                        Yii::app()->mailer->Subject = 'Supplier signup confirmation from '.$companyModel->name;
                        Yii::app()->mailer->Body = $body;
                        Yii::app()->mailer->Send();
                        $msg = '<div class="notification note-success"><p>Signup as supplier is successful, please check your email for details.</p></div>';
                        $modelSupplier = new SupplierSignup;
                    }
                    else throw new Exception();
                }
                else throw new Exception();
            }
            catch (Exception $e) {$transaction->rollback();}
        }

        // customer processing
        else if(isset($_POST['CustomerSignup']))
        {
            // data insert by transaction
            $modelCustomerMain = new Customer;
            $transaction = $modelCustomerMain->dbConnection->beginTransaction();;
            try
            {
                // customer processing
                $modelCustomer->attributes=$_POST['CustomerSignup'];
                $modelCustomerMain->title = $_POST['CustomerSignup']['title'];
                $modelCustomerMain->custId = $_POST['CustomerSignup']['custId'];
                $modelCustomerMain->custType = Customer::STATUS_GENERAL;
                $modelCustomerMain->name = $_POST['CustomerSignup']['name'];
                $modelCustomerMain->gender = $_POST['CustomerSignup']['gender'];
                $modelCustomerMain->addressline = $_POST['CustomerSignup']['addressline'];
                $modelCustomerMain->city = $_POST['CustomerSignup']['city'];
                $modelCustomerMain->phone = $_POST['CustomerSignup']['phone'];
                $modelCustomerMain->email = $_POST['CustomerSignup']['email'];
                $modelCustomerMain->status = Customer::STATUS_INACTIVE;

                if($modelCustomer->validate() && $modelCustomerMain->save())  // customer save
                {
                    // user processing
                    $modelUser = new User;
                    $modelUser->branchId = Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
                    $modelUser->isSupplier = User::IS_NOT_SUPPLIER;
                    $modelUser->custId = $modelCustomerMain->id;
                    $modelUser->username = $_POST['CustomerSignup']['username'];
                    $salt = $modelUser->generateSalt();
                    $modelUser->password = $modelUser->hashPassword($_POST['CustomerSignup']['password'],$salt);
                    $modelUser->salt = $salt;
                    $modelUser->conf_password = $modelUser->hashPassword($_POST['CustomerSignup']['conf_password'],$salt);
                    $modelUser->totalLoginTime = 0;
                    $modelUser->status = User::STATUS_PENDING;
                    $modelUser->save();

                    // roll processing
                    $modelRoles = new Roles;
                    $modelRoles->userTypeId = UserType::getCustomerType();
                    $modelRoles->userId = $modelUser->id;
                    $modelRoles->status = Roles::STATUS_ACTIVE;
                    if($modelRoles->save())
                    {
                        $transaction->commit();
                        // email goes here $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                        $body = $this->renderPartial('customerRegEmail',array(
                                'regType'=>'customer',
                                'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
                                'username'=>$_POST['CustomerSignup']['username'],
                                'password'=>$_POST['CustomerSignup']['password']),true
                        );
                        Yii::app()->mailer->IsHTML(true);
                        Yii::app()->mailer->SMTPAuth = true;
                        Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
                        Yii::app()->mailer->From = $companyModel->email; //'admin@unlockliveretail.com';
                        Yii::app()->mailer->FromName = $companyModel->name;
                        Yii::app()->mailer->AddReplyTo('admin@unlockliveretail.com');
                        Yii::app()->mailer->AddAddress($_POST['CustomerSignup']['email']);
                        Yii::app()->mailer->Subject = 'Customer signup confirmation from '.$companyModel->name;
                        Yii::app()->mailer->Body = $body;
                        Yii::app()->mailer->Send();
                        $msg = '<div class="notification note-success"><p>Signup as customer is successful, please check your email for details.</p></div>';
                        $modelCustomer = new CustomerSignup;
                    }
                    else throw new Exception();
                }
                else throw new Exception();
            }
            catch (Exception $e) {$transaction->rollback();}
        }
        $this->render('customerSignup',array(
            'modelCustomer'=>$modelCustomer,
            'modelSupplier'=>$modelSupplier,
            'msg'=>$msg,
        ));
    }

    // customer registration/sign in
    public function actionCustomerSignin()
    {
        $this->metaTitle = 'Customer Signin';
        $msg = '';
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
            {
                unset(Yii::app()->session['visibilityarray']);
                Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(Yii::app()->session['usertypesIds']);

                if(!empty(Yii::app()->session['custId'])) $this->redirect(array('eshop/customerDashboard')); // customer
                else // others
                {
                    if(UserRights::getVisibility('user', 'dashboard')) $this->redirect(array('user/dashboard'));
                    else
                    {
                        if(UserRights::getVisibility('salesInvoice', 'create')) $this->redirect(array('salesInvoice/create'));
                        else $this->redirect(array('eshop/index'));
                    }
                }
            }
        }      
        $this->render('customerSignin',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    /**
    * Reset Password
    **/
    public function actionResetPassword()
    {
        $this->metaTitle = 'Customer Signin';
        $msg = '';
        $model=new LoginForm;
        if(!empty($_REQUEST['msg']) || !empty($_REQUEST['msg'])=='success')
            $msg = '<div class="notification note-success"><p>Your request is successful, Please check your email for next steps</p></div>';
        else
        {
            // after post data
            if (!empty($_POST['username']) || !empty($_POST['email'])) {
                $userName = $_POST['username'];
                $userEmail = $_POST['email'];

                // user wise processing
                if (isset($userName) && !empty($userName)) {
                    $user = Yii::app()->db->createCommand()
                        ->select('u.id,u.password,c.title,c.email,c.name')
                        ->from('pos_user u')
                        ->join('pos_customer c', 'u.custId = c.id')
                        ->where('u.username=:username', array(':username' => $userName))
                        ->queryRow();
                    if (!empty($user)) $this->sendPasswordResetEmail($user['email'], $user['id'], $user['title'], $user['name'], $user['password']);
                    else $msg = '<div class="notification note-error"><p>User name ' . $userName . ' is not valid !</p></div>';
                } // email processing
                else if (isset($userEmail) && !empty($userEmail)) {
                    $user = Yii::app()->db->createCommand()
                        ->select('u.id,u.password,c.title,c.email,c.name')
                        ->from('pos_user u')
                        ->join('pos_customer c', 'u.custId = c.id')
                        ->where('c.email=:email', array(':email' => $userEmail))
                        ->queryRow();
                    if (!empty($user)) $this->sendPasswordResetEmail($user['email'], $user['id'], $user['title'], $user['name'], $user['password']);
                    else  $msg = '<div class="notification note-error"><p>Email ' . $userEmail . ' is not valid !</p></div>';
                }
            } else $msg = '<div class="notification note-error"><p>You have to provide your user name or email !</p></div>';
        }

        $this->render('customerSignin',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    /**
    * Password Reset Email
    */
    public function sendPasswordResetEmail($email,$userId,$userTitle,$userName,$token)
    {
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                        array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));
        $protocol = 'http://';
        if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https://';
        $queryString = '';
        $url = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/eshop/resetPasswordToken/id/'.$userId.'/token/'.$token;

        // email goes here $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $body = $this->renderPartial('customerCngpsdEmail',array(
                                    'regType'=>'supplier',
                                    'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
                                    'username'=>$userTitle.'&nbsp;'.$userName,
                                    'url'=>$url),true
               );
        Yii::app()->mailer->IsHTML(true);
        Yii::app()->mailer->SMTPAuth = true;
        Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
        Yii::app()->mailer->From = $companyModel->email;
        Yii::app()->mailer->FromName = $companyModel->name;
        Yii::app()->mailer->AddReplyTo($companyModel->email);
        Yii::app()->mailer->AddAddress($email);
        Yii::app()->mailer->Subject = 'Customer password reset request';
        Yii::app()->mailer->Body = $body;
        Yii::app()->mailer->Send();
        $this->redirect(array('resetPassword','msg'=>'success'));
    }

    /**
    * from email reset password token
    **/
    public function actionResetPasswordToken()
    {
        $this->metaTitle = 'Reset Password';
        $msg = '';
        if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])) $id = $_REQUEST['id'];
        $find = User::model()->findByPk($id);

        $model=new LoginForm;
        $form = new CustomerChangePassword;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // login/signin as customer
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];

            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login())
            {
                unset(Yii::app()->session['visibilityarray']);
                Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(Yii::app()->session['usertypesIds']);

                if(!empty(Yii::app()->session['custId'])) $this->redirect(array('eshop/customerDashboard')); // customer
                else // others
                {
                    if(UserRights::getVisibility('user', 'dashboard')) $this->redirect(array('user/dashboard'));
                    else
                    {
                        if(UserRights::getVisibility('salesInvoice', 'create')) $this->redirect(array('salesInvoice/create'));
                        else $this->redirect(array('eshop/index'));
                    }
                }
            }
        }

        if(isset($_POST['CustomerChangePassword']))
        {
            $form->attributes=$_POST['CustomerChangePassword'];

            // validate user input and redirect to the previous page if valid
            if($form->validate())
            {
                $salt = uniqid('',true);
                $find['password'] = md5($salt.$form['password']);
                $find['salt'] = $salt;
                User::model()->updateByPk($find['id'],array('password'=>$find['password'],'salt'=>$find['salt']));
                $msg = '<div class="notification note-success"><p>Password changed successfully</p></div>';
            }
        }
        $this->render('customerPasswordReset',array(
            'model'=>$model,
            'form'=>$form,
            'msg'=>$msg,
        ));
    }

    // submit newsletter
    public function actionNewsLetter()
    {
        unset(Yii::app()->session['newsletterMsg']);
        $this->metaTitle = 'Newsletter Subscribe';
        $model = new Newsletter;
        $this->performAjaxValidationNewsLetter($model);

        if(isset($_POST['Newsletter']))
        {
            $model->attributes=$_POST['Newsletter'];
            if($model->validate())
            {
                if($model->save())
                {
                    Yii::app()->session['newsletterMsg'] =  '<h4 class="alert alert-success">Your E-mail subscribe successfully</h4>';
                    $this->redirect(Yii::app()->homeUrl);
                }
            }
            else
            {
                Yii::app()->session['newsletterMsg'] = '<h4 class="alert alert-danger">Your E-mail is not valid or already in Subscribe</h4>';
                $this->redirect(Yii::app()->homeUrl);
            }
        }
    }

    // Customer Contact Us 
    public function actionContractUs()
    {
        $model=new ContactForm;
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                        array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-Type: text/plain; charset=UTF-8";
                mail($companyModel->email,$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
       }
       $this->render('contactUs',array('model'=>$model,'companyModel'=>$companyModel,'branchModel'=>$branchModel));
    }


    // customer dashboard
    public function actionCustomerDashboard()
    {
        if(empty(Yii::app()->session['custId'])) $this->redirect(array('customerSignin'));

        $this->metaTitle = 'Customer Dashboard';
        $this->layout = '//layouts/eshop_customer_column';
        $model = array();
        if(!empty(Yii::app()->session['custId']))
        {
            $model = Customer::model()->findByPk(Yii::app()->session['custId']);
            $modelUser = User::model()->find(array('condition'=>'custId=:custId', 'params'=>array(':custId'=>Yii::app()->session['custId'])));
        }
        $this->render('customerDashboard',array(
            'model'=>$model,'modelUser'=>$modelUser,
        ));
    }

    // customer info updates
    public function actionCustomerInfo($id)
    {
        if(empty(Yii::app()->session['custId'])) $this->redirect(array('customerSignin'));

        $this->layout = '//layouts/eshop_customer_column';
        $msg='';
        $this->metaTitle = 'Update Information';
        $modelCustomer = Customer::model()->findByPk($id);

        $this->performAjaxValidationCustomerInfo($modelCustomer);

        if(isset($_POST['Customer']))
        {
            $modelCustomer->attributes=$_POST['Customer'];

            if($modelCustomer->save())
                $msg = "Information Updated Successfully";
            //$this->redirect(array('admin'));
        }
        $this->render('customerInfo',array(
            'modelCustomer'=>$modelCustomer,
            'msg'=>$msg,
        ));
    }

    // customer Wish list
    public function actionCustomerWishlist()
    {
        if(empty(Yii::app()->session['custId'])) $this->redirect(array('customerSignin'));

        $this->layout = '//layouts/eshop_customer_column';
        $wishListModel=array();
        $this->metaTitle = 'My WishList';
        if(!empty(Yii::app()->session['custId']))
        {
            $wishListModel = Wishlist::model()->findAll(array('condition'=>'custId=:custId AND status=:status',
                                                              'params'=>array(':custId'=>Yii::app()->session['custId'],':status'=>Wishlist::STATUS_ACTIVE)));
        }
        $this->render('customerWishList',array(
            'wishListModel'=>$wishListModel,
        ));
    }

    // add/move to wish list
    public function actionAddMoveToWishList()
    {
        if(!empty($_POST['itemId'])) :
            $existsCheck = Wishlist::model()->exists('itemId=:itemId AND custId=:custId AND status=:status',
                array(':itemId'=>$_POST['itemId'],':custId'=>Yii::app()->session['custId'],':status'=>Wishlist::STATUS_ACTIVE));

            if(!empty($existsCheck)) echo json_encode("exists");
            else
            {
                $model = new Wishlist;
                $model->itemId = $_POST['itemId'];
                $model->custId = Yii::app()->session['custId'] ;
                if($model->save())
                {
                    // delete item from my cart
                    $cart = new Cart('cart');
                    $cart->setItemQuantity($_POST['itemId'], 0);
                    $cart->save();
                    echo json_encode(Wishlist::countWishListByCustomer(Yii::app()->session['custId']));
                }
            }
        //else : $this->redirect(array('eshop/customerSignin'));
        else : echo json_encode("invalid");
        endif;
    }

    // Remove Data From Wish List
    public function actionRemoveFromWishList()
    {
        if(!empty($_POST['id']))
        {
            if(Wishlist::model()->deleteByPk($_POST['id']))
                echo json_encode(Wishlist::countWishListByCustomer(Yii::app()->session['custId']));
            else echo json_encode("invalid");
        }
    }

    // product add to cart from wish list
    public function actionAddToCartFromWishList()
    {
        // add to cart
        $cart = new Cart('cart');
        if(!empty($_POST['itemId']) && !empty($_POST['quantity'])) {
            $quantity = $cart->getItemQuantity($_POST['itemId']) + $_POST['quantity'];
            $cart->setItemQuantity($_POST['itemId'], $quantity);
        }
        $cart->save();

        // delete from wish list
        if(!empty($_POST['id']))
        {
            if(Wishlist::model()->deleteByPk($_REQUEST['id']))
                echo json_encode(Wishlist::countWishListByCustomer(Yii::app()->session['custId']));
            else echo json_encode("invalid");
        }
    }

    // customer order list
    public function actionCustomerOrder()
    {
        if(empty(Yii::app()->session['custId'])) $this->redirect(array('customerSignin'));

        $this->layout = '//layouts/eshop_customer_column';
        $orderModel=array();
        $this->metaTitle = 'My Order';
        if(!empty(Yii::app()->session['custId']))
        {
            $orderModel = SalesInvoice::model()->findAll(array('condition'=>'custId=:custId AND isEcommerce=:isEcommerce',
                                                               'params'=>array(':custId'=>Yii::app()->session['custId'],':isEcommerce'=>SalesInvoice::IS_ECOMMERCE),
                                                               'order'=>'orderDate DESC',
                          ));
        }
        $this->render('customerOrder',array(
            'orderModel'=>$orderModel,
        ));
    }

    // Customer Oder Details
    public function actionCustomerOderDetails($id)
    {
        if(empty(Yii::app()->session['custId'])) $this->redirect(array('customerSignin'));

        $this->layout = '//layouts/eshop_customer_column';
        $this->metaTitle = 'My Order Details';

        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $orderModel = SalesInvoice::model()->findByPk($id);
        $orderDetailsModel = SalesDetails::model()->findAll(array('condition'=>'salesId=:salesId','params'=>array(':salesId'=>$id),'order'=>'crAt DESC'));
        $this->render('customerOrderDetails',array(
            'companyModel'=>$companyModel,
            'orderModel'=>$orderModel,
            'orderDetailsModel'=>$orderDetailsModel,
        ));
    }

    /**
     * customer change password
     **/
    public function actionCustomerChangePassword()
    {
        $this->layout = '//layouts/eshop_customer_column';
        $this->metaTitle = 'Change Password';
        $msg = '';
        $form2 = new UserChangePassword;
        $find = User::model()->findByPk(Yii::app()->user->id);
        $form2->oldpass = $find->password;

        if(isset($find))
        {
            if(isset($_POST['UserChangePassword']))
            {
                $form2->attributes=$_POST['UserChangePassword'];
                $vopass = $form2->verifyoldpass = md5($find->salt.$form2->verifyoldpass);

                if($form2->validate())
                {
                    $salt = uniqid('',true);
                    $find->password = $find->hashPassword($form2->password,$salt);
                    $find->salt = $salt;
                    User::model()->updateByPk($find->id,array('password'=>$find->password,'salt'=>$find->salt));
                    $msg = '<div class="notification note-success"><p>Password Changed Successfully</p></div>';
                }
                else
                {
                    $form2->verifyoldpass = "";
                    $form2->password = "";
                    $form2->verifyPassword = "";
                }
            }
        }
        $this->render('customerChangePassword',array('form'=>$form2,'msg'=>$msg));
    }

    // dynamic contents details view
    public function actionContentsDetails($id)
    {
        if(isset($id) && !empty($id))
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'typeId = :typeId';
            $criteria->params =array(':typeId'=>$id);
            $contentDetails = Contents::model()->find($criteria);
            $this->metaTitle = $contentDetails->type->name;
        }
        $this->render('contentsDetails',array(
            'contentsDetails'=>$contentDetails,
        ));
    }

    // item search auto suggest
    public function actionAutoSuggestItemSearch()
    {
        if(!empty($_GET['term']))
        {
            $match ='%'.$_GET['term'].'%';
            $model = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND itemName LIKE :itemName AND status=:status',
                                                    'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':itemName'=>$match,':status'=>Items::STATUS_ACTIVE),
                                                    'group'=>'itemName','order'=>'crAt DESC','limit' => 25,
                                     ));
            if(!empty($model))
            {
                foreach($model as $key=>$data) {
                    $dataItem[] = array(
                        //'label'=>$data->itemName,  // label for dropdown list
                        'value'=>$data->itemName,  // value for input field
                        'id'=>$data->id,          // return value from autocomplete
                    );
                }
            }
            echo CJSON::encode($dataItem);
            exit;
        } return false;
    }

    // item review ajax
    public function actionItemReview()
    {
        $modelReview = new ItemReview;
        $this->performAjaxValidationItemReview($modelReview);

        if(isset($_POST['ItemReview']))
        {
            $modelReview->attributes=$_POST['ItemReview'];
            if(empty($modelReview->rating)) echo $msg = '<div class="notification note-error"><p style="margin-bottom:0px;">Review can,t be empty, please pick a Rating !</p></div>';
            else
            {
                if($modelReview->save()) echo $msg = '<div class="notification note-success"><p style="margin-bottom:0px;">Thank you for your Review, Your Review is waiting for Approval.</p></div>';
            }
        }
    }

    // item review normal
    public function actionProReview($id)
    {
        $msg='';
        $this->layout="//layouts/eshop_main_column";
        $this->metaTitle = 'Product Review';
        $modelReview = new ItemReview;
        $modelItemReview = ItemReview::model()->findAll(array('condition'=>'itemId=:itemId AND status=:status',
            'params'=>array(':itemId'=>$id,':status'=>ItemReview::STATUS_ACTIVE),
            'order'=>'crAt DESC'
        ));

        // Ajax Validation
        $this->performAjaxValidationAjaxReview($modelReview);
        if(isset($_POST['ItemReview']))
        {
            $modelReview->attributes=$_POST['ItemReview'];
            if($modelReview->save()) {
                $msg = "Thank You For Your Review.Your Review is Waiting For Approval !";
                $modelReview = new ItemReview;
            }
        }
        $this->render('customerProductReview',array(
            'modelItemReview'=>$modelItemReview,
            'modelReview'=>$modelReview,
            'id'=>$id,
            'msg'=>$msg,
        ));
    }

    /**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = '//layouts/fullcolumn';
		$this->metaTitle = 'Sign In';
		
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				unset(Yii::app()->session['visibilityarray']);
				Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(Yii::app()->session['usertypesIds']);
				
				// checking dashboard/sales/index permission
				if(UserRights::getVisibility('user', 'dashboard')) $this->redirect(array('user/dashboard'));
				else
				{
					if(UserRights::getVisibility('salesInvoice', 'create')) $this->redirect(array('salesInvoice/create'));	
					else 
                    {
                        if(!empty(Yii::app()->session['supplierId'])) $this->redirect(array('items/create'));
                        $this->redirect(array('eshop/index'));	
                    }
				}
			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(array('eshop/login')); // Yii::app()->homeUrl
	}

    /* test all email temporary */
    public function actionEmailCheck()
    {
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                        array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        /*$this->render('customerRegEmail',array(
            'regType'=>'customer',
            'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
            'username'=>'badal',
            'password'=>'kzaman')
        );*/

        // email goes here $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $body = $this->renderPartial('customerRegEmail',array(
            'regType'=>'customer',
            'companyModel'=>$companyModel,'branchModel'=>$branchModel,'socialModel'=>$socialModel,
            'username'=>'badal',
            'password'=>'kzaman'),true
        );
        Yii::app()->mailer->IsHTML(true);
        Yii::app()->mailer->SMTPAuth = true;
        Yii::app()->mailer->Host = 'mail.unlockliveretail.com';
        Yii::app()->mailer->From = $companyModel->email; //'admin@unlockliveretail.com';
        Yii::app()->mailer->FromName = $companyModel->name;
        Yii::app()->mailer->AddReplyTo('admin@unlockliveretail.com');
        Yii::app()->mailer->AddAddress('kzaman.badal@gmail.com'); // hal.sanjib@yahoo.com
        Yii::app()->mailer->Subject = 'Customer signup confirmation from '.$companyModel->name;
        Yii::app()->mailer->Body = $body;
        Yii::app()->mailer->Send();
    }

    /*
     * Performs the AJAX validation for supplier.
     * @param Supplier $modelSupplier the model to be validated
     */
    protected function performAjaxValidationSupplier($modelSupplier)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='supplier-form')
        {
            echo CActiveForm::validate($modelSupplier);
            Yii::app()->end();
        }
    }
    /*
     * Performs the AJAX validation for customer
     * @param Customer $modelCustomer the model to be validated
     */
    protected function performAjaxValidationCustomer($modelCustomer)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
        {
            echo CActiveForm::validate($modelCustomer);
            Yii::app()->end();
        }
    }

    // customer review ajax validation
    protected function performAjaxValidationAjaxReview($modelCustomer)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='item-Review-Item')
        {
            echo CActiveForm::validate($modelCustomer);
            Yii::app()->end();
        }
    }

    // customer info update ajax validation
    protected function performAjaxValidationCustomerInfo($modelCustomer)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='customer-form')
        {
            echo CActiveForm::validate($modelCustomer);
            Yii::app()->end();
        }
    }

    // item review ajax validation
    protected function performAjaxValidationItemReview($modelReview)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='item-Review')
        {
            echo CActiveForm::validate($modelReview);
            Yii::app()->end();
        }
    }

    /*
     * Performs the AJAX validation for customer newsletter
     * @param Customer checkout $model the model to be validated
     */
    protected function performAjaxValidationNewsLetter($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='newsletter-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
