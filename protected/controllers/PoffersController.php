<?php
/*********************************************************
        -*- File: PoffersController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.02.26
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class PoffersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new Poffers;
        $itemArr = array();
        $branch='';
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		if(isset($_POST['Poffers']))
		{
            if ($_POST['branch']!='all'){
                $branchId = $_POST['branch'];
            }else if ($_POST['branch']=='all'){
                $branchId=0;
            }else{
                $branchId= Yii::app()->session['branchId'];
            }

		    if (empty($_POST['Poffers']['catId'])){
		        if (!isset($_POST['proId'])) {
                    $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select Item Code !</p></div>';
                }elseif (empty($_POST['Poffers']['startDate'])){
                    $msg = '<div class="notification note-error"><p style="padding-left:20px;">All star marked * fields are mandatory, please fill up all mandatory fields2222.</p></div>';
                }else{
                    if (isset($_POST['proId']) && !empty($_POST['proId'])) :
                        foreach ($_POST['proId'] as $key => $proId) :
                            if (!in_array($proId, $itemArr) && !empty($proId)) :
                                $model = new Poffers;
                                $model->offerNo = $_POST['Poffers']['offerNo'];
                                $model->packageId = $_POST['Poffers']['packageId'];
                                $model->itemId = $proId;
                                $model->startDate = $_POST['Poffers']['startDate'];
                                $model->endDate = $_POST['Poffers']['endDate'];
                                $model->branchId =$branchId;
                                $model->save();
                                $itemArr[] = $proId;
                            endif;
                        endforeach;
                    endif;
                    $msg = "Offers Added Successfully";
                    $model = new Poffers;
                }
            }else {
                    $childSubCatId = $_POST['Poffers']['catId'];
                    $sql = "SELECT id FROM pos_items WHERE catId='" . $childSubCatId . "'";
                    $ItemModel = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($ItemModel) {
                        foreach ($ItemModel as $key => $items) :
                            if (!in_array($items['id'], $itemArr)) :
                                $model = new Poffers;
                                $model->offerNo = $_POST['Poffers']['offerNo'];
                                $model->packageId = $_POST['Poffers']['packageId'];
                                $model->itemId = $items['id'];
                                $model->startDate = $_POST['Poffers']['startDate'];
                                $model->endDate = $_POST['Poffers']['endDate'];
                                $model->branchId =$branchId;
                                $model->save();
                                $itemArr[] = $items['id'];
                            endif;
                        endforeach;
                    }
                $msg = "Offers Added Successfully";
                $model = new Poffers;
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
            'branch'=>$branch,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Poffers']))
		{
			$model->attributes = $_POST['Poffers'];
			if($model->save())
				$msg = "Offers Updated Successfully";
		}

		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Poffers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new Poffers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Poffers']))
			$model->attributes=$_GET['Poffers'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Poffers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Poffers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Poffers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='poffers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
