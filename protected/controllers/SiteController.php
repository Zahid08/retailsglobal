<?php
/*********************************************************
        -*- File: SiteController.php	
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/controller
		-*-  YII-*- version 1.1.13
/*********************************************************/
class SiteController extends Controller
{
	public $defaultAction = 'login';
	
	/**
	 * Declares class-based actions.
	 */		
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}
	
	// default loads
	public function actionIndex()
	{
		$this->layout = '//layouts/eshop_fullcolumn';
		$this->render('index');
	}

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $this->redirect(array('eshop/login'));
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
            //echo '<pre>';
            //echo print_r(Yii::app()->errorHandler->error);
            //exit();
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
	
	/* refresh session and menus */
	public function actionRefresh()
	{
		unset(Yii::app()->session['visibilityarray']);
		$this->redirect(array('user/dashboard'));
	}
	
	// database backup system
	public function actionDbBackup()
	{
		UsefulFunction::backupDb(dirname(Yii::app()->request->scriptFile) . '/media/database/db_'.date("Y-m-d").'.sql');
		$this->redirect(array('user/dashboard'));
	}

    // Redactor Image Upload for Editor
    public function actionRedactorimageUp()
    {
        // create directory if not exists
        if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/redactor/'.Yii::app()->session['orgId']))
        {
            mkdir(dirname(Yii::app()->request->scriptFile).'/media/redactor/'.Yii::app()->session['orgId'], 0777, true);
        }

        $uploadedFile = CUploadedFile::getInstanceByName('file');
        if (!empty($uploadedFile))
        {
            $rnd = rand().date("ymdhis");  // generate random number between 0-9999
            $fileName = "{$rnd}.{$uploadedFile->extensionName}";  // random number + file name
            if ($uploadedFile->saveAs(dirname(Yii::app()->request->scriptFile).'/media/redactor/'.Yii::app()->session['orgId'].'/'.$fileName))
            {
                //echo stripslashes(json_encode(CHtml::image(Yii::app()->baseUrl . '/uploads/redactor/'.$fileName)));
                echo stripslashes(CHtml::image(Yii::app()->baseUrl . '/media/redactor/'.Yii::app()->session['orgId'].'/'.$fileName));
                Yii::app()->end();
            }
        }
        throw new CHttpException(400, 'The request cannot be fulfilled due to bad syntax');
    }
}