<?php
/*********************************************************
        -*- File: CouponsController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.03.24
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class CouponsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
        $ptr = 0;
		$model=new Coupons;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Coupons']))
		{
			$model->attributes=$_POST['Coupons'];
            $couponQty = $model->couponQty;
            $couponStartDate = $model->couponStartDate;
            $couponEndDate = $model->couponEndDate;
            $couponAmount = $model->couponAmount;
            $status = $model->status;
            $subdeptId=$model->subdeptId;

            if ($model->subdeptId==''){
                $subdeptId=0;
            }

			if($model->validate())
            {
                for($i=1;$i<=$couponQty;$i++)
                {
                    $model=new Coupons;
                    $model->couponQty = $couponQty;
                    $model->couponNo = 'CU'.date("YmdHis").$i;
                    $model->couponStartDate=$couponStartDate;
                    $model->couponEndDate=$couponEndDate;
                    $model->couponAmount=$couponAmount;
                    $model->status=$status;
                    $model->subdeptId=$subdeptId;
                    if($model->save()) $ptr = 1;
                }
                if($ptr==1)
                {
                    $msg = "Coupons Saved Successfully";
                    $model=new Coupons;
                }
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Coupons']))
		{
			$model->attributes=$_POST['Coupons'];
            $model->couponQty = Coupons::DEFAULT_QTY;
            $subdeptId=$model->subdeptId;
            if ($model->subdeptId==''){
                $subdeptId=0;
            }
            $model->subdeptId=$subdeptId;
			if($model->save())
            	$msg = "Coupons Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Coupons');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new Coupons('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Coupons']))
			$model->attributes=$_GET['Coupons'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Coupons the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Coupons::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Coupons $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='coupons-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
