<?php
/*********************************************************
        -*- File: TemporaryWorkerWidrawController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.04.09
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class TemporaryWorkerWidrawController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new TemporaryWorkerWidraw;
        $model->bankId = SupplierWidraw::IS_CASH;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TemporaryWorkerWidraw']))
		{
			$model->attributes=$_POST['TemporaryWorkerWidraw'];

            // bank a/c processing
            if(!empty($_POST['TemporaryWorkerWidraw']['bankAcNo']))
            {
                $model->bankId = Bank::getBankByAccount($_POST['TemporaryWorkerWidraw']['bankAcNo']);
                if($model->amount>Bank::getBankAccountCurrentBalance($model->bankId))
                {
                    $msg = '<div class="notification note-error"><p>Payment Amount is greater than Bank Balance !</p></div>';
                    $this->render('_form',array(
                        'model'=>$model,
                        'msg'=>$msg,
                    ));
                    exit();
                }
            }

            // Temporary Worker NO processing
            $modelTwNo = TemporaryWorker::model()->find('twNo=:twNo',array(':twNo'=>$_POST['TemporaryWorkerWidraw']['twNo']));
            if(!empty($modelTwNo))
            {
                $sqlTwDeposit = "SELECT SUM(qty*costPrice) AS totalamount FROM `pos_temporary_worker` WHERE twNo='".$modelTwNo->twNo."' AND branchId=".Yii::app()->SESSION['branchId']."
                                 AND qty=rqty ORDER BY crAt DESC";
                $modelTwDeposit = Yii::app()->db->createCommand($sqlTwDeposit)->queryRow();
                $totalTwDeposit = $modelTwDeposit['totalamount'];

                // worker balane processing
                $twBalance = TemporaryWorker::getWorkerBalance($model->wkId);
                if($twBalance>=$model->amount)
                {
                    // check workerAmount & worker wise withdraw
                    $sqlTwWithdraw = "SELECT SUM(amount) AS totalamount FROM `pos_temporary_worker_widraw` WHERE twNo='".$modelTwNo->twNo."' AND branchId=".Yii::app()->SESSION['branchId']." ORDER BY crAt DESC";
                    $modelTwWithdraw  = Yii::app()->db->createCommand($sqlTwWithdraw)->queryRow();
                    $totalTwWithdraw = $modelTwWithdraw['totalamount']+$model->amount;

                    if($totalTwDeposit>=$totalTwWithdraw)
                    {
                        if($model->save())
                        {
                            $msg = '<div class="notification note-success"><p>Credit Supplier Payment is Successfull.</p></div>';
                            $model=new TemporaryWorkerWidraw;
                        }
                    }
                    else $msg = '<div class="notification note-error"><p>Payment Amount is greater than Temporary Worker NO. Amount !</p></div>';
                }
                else $msg = '<div class="notification note-error"><p>Payment Amount is greater than Worker balance !</p></div>';
            }
            else $msg = '<div class="notification note-error"><p>Wrong Temporary Worker NO. Please use correct Temporary Worker NO.</p></div>';
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['TemporaryWorkerWidraw']))
		{
			$model->attributes=$_POST['TemporaryWorkerWidraw'];
			if($model->save())
            	$msg = "TemporaryWorkerWidraw Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TemporaryWorkerWidraw');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new TemporaryWorkerWidraw('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TemporaryWorkerWidraw']))
			$model->attributes=$_GET['TemporaryWorkerWidraw'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TemporaryWorkerWidraw the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TemporaryWorkerWidraw::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TemporaryWorkerWidraw $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='temporary-worker-widraw-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
