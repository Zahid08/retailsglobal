<?php
/*********************************************************
        -*- File: SupplierWidrawController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.04.10
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class SupplierWidrawController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/** credit supplier payment
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreatecredit()
	{				
    	$msg = '';
		$model=new SupplierWidraw;
        $model->isBank = SupplierWidraw::IS_CASH;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SupplierWidraw']))
		{
			$model->attributes=$_POST['SupplierWidraw'];

            // bank a/c processing
            if(!empty($_POST['SupplierWidraw']['bankAcNo']))
            {
                $model->bankId = Bank::getBankByAccount($_POST['SupplierWidraw']['bankAcNo']);
                if($model->amount>Bank::getBankAccountCurrentBalance($model->bankId))
                {
                    $msg = '<div class="notification note-error"><p>Credit Payment Amount is greater than Bank Balance !</p></div>';
                    $this->render('_form',array(
                        'model'=>$model,
                        'msg'=>$msg,
                    ));
                    exit();
                }
            }

			// grn processing
			$modelgrn = GoodReceiveNote::model()->find('grnNo=:grnNo',array(':grnNo'=>$_POST['SupplierWidraw']['pgrnNo']));
			if(!empty($modelgrn))
			{
				$grnId = $model->id;
				$model->pgrnNo = 'P'.$modelgrn->grnNo;
				$model->type = SupplierWidraw::GRN_WIDRAW;
				
				// grn amount and balane processing
				$suppBalance = Supplier::getSupplierBalance($model->supId);
				
				$modelgrndeposit = SupplierDeposit::model()->find('grnId=:grnId',array(':grnId'=>$modelgrn->id));
				if(!empty($modelgrndeposit)) $grnAmount = $modelgrndeposit->amount;
				else $grnAmount = 0;
				
				if($suppBalance>=$model->amount) 
				{
					// check grnAmount & grnwise widraw
					$sqlGrnWidraw = "SELECT SUM(amount) AS totalamount FROM pos_supplier_widraw WHERE pgrnNo LIKE '%".$modelgrn->grnNo."' AND branchId=".Yii::app()->SESSION['branchId']." ORDER BY crAt desc";
					$modelGrnWidraw  = Yii::app()->db->createCommand($sqlGrnWidraw)->queryRow();
					$totalWidrawGrn = $modelGrnWidraw['totalamount']+$model->amount;
					
					if($grnAmount>=$totalWidrawGrn) 
					{
						if($model->save()) 
						{
							$msg = '<div class="notification note-success"><p>Credit Supplier Payment is Successfull.</p></div>';
							$model=new SupplierWidraw;
						}
					}
					else $msg = '<div class="notification note-error"><p>Credit Payment Amount is greater than GRN Amount !</p></div>';					
				}
				else $msg = '<div class="notification note-error"><p>Credit Payment Amount is greater than Supplier balance !</p></div>';
			}
			else $msg = '<div class="notification note-error"><p>Wrong GRN NO. Please use correct GRN NO.</p></div>';
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/** consig supplier payment
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreateconsig()
	{
    	$msg = '';
		$model=new SupplierWidraw;
        $model->isBank = SupplierWidraw::IS_CASH;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SupplierWidraw']))
		{
			$model->attributes=$_POST['SupplierWidraw'];

            // bank a/c processing
            if(!empty($_POST['SupplierWidraw']['bankAcNo']))
            {
                $model->bankId = Bank::getBankByAccount($_POST['SupplierWidraw']['bankAcNo']);
                if($model->amount>Bank::getBankAccountCurrentBalance($model->bankId))
                {
                    $msg = '<div class="notification note-error"><p>Credit Payment Amount is greater than Bank Balance !</p></div>';
                    $this->render('_consigform',array(
                        'model'=>$model,
                        'msg'=>$msg,
                    ));
                    exit();
                }
            }

			// supplier processing
			$modelsupp = Supplier::model()->findByPk($model->supId);
			if(!empty($modelsupp))
			{
				$model->type = SupplierWidraw::GRN_WIDRAW;
				$model->pgrnNo = 'PCGN'.date("Ymdhis");
				
				// supp consig amount and balane processing
				$suppBalance = Supplier::getSupplierBalanceConsig($modelsupp->id);
				$suppConsigAmount = Supplier::getSupplierConsigSales($modelsupp->id,$modelsupp->consignmentDay);
				
				if($suppBalance>=$model->amount)
				{
					if($suppConsigAmount>=$model->amount)
					{
						if($model->save()) 
						{
							$msg = '<div class="notification note-success"><p>Consigment Supplier Payment is Successfull.</p></div>';
							$model=new SupplierWidraw;
						}
					}
					else $msg = '<div class="notification note-error"><p>Payment Amount is greater than Consigment Day Amount !</p></div>';
				}
				else $msg = '<div class="notification note-error"><p>Payment Amount is greater than Supplier balance !</p></div>';
			}
			else $msg = '<div class="notification note-error"><p>Wrong Suppler, Please select correct Suppler !</p></div>';
		}

		$this->render('_consigform',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['SupplierWidraw']))
		{
			$model->attributes=$_POST['SupplierWidraw'];
			if($model->save())
            	$msg = "SupplierWidraw Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('SupplierWidraw');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new SupplierWidraw('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SupplierWidraw']))
			$model->attributes=$_GET['SupplierWidraw'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SupplierWidraw the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SupplierWidraw::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SupplierWidraw $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='supplier-widraw-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
