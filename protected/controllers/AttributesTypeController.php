<?php
/*********************************************************
        -*- File: AttributesTypeController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.01.06
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class AttributesTypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new AttributesType;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['AttributesType']))
		{
			$model->attributes=$_POST['AttributesType'];
            //echo '<pre>';print_r($model->attributes);exit();

            if($model->save()) {
				$msg = "Attributes Type Saved Successfully";
                $model=new AttributesType;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
    	$msg = '';
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['AttributesType']))
		{
			$model->attributes=$_POST['AttributesType'];
			if($model->save())
            	$msg = "Attributes Type Updated Successfully";
				//$this->redirect(array('admin'));
		}

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AttributesType');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new AttributesType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AttributesType']))
			$model->attributes=$_GET['AttributesType'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AttributesType the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AttributesType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AttributesType $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='attributes-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
