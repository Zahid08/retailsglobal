<?php
/*********************************************************
        -*- File: TemporaryWorkerController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2015.03.10
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/
class TemporaryWorkerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = '';
		$model=new TemporaryWorker;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['qty']))
        {
            $ptr = 0;
            // processing sales details
            foreach($_POST['qty'] as $key=>$qty) :
                if($qty>0) :
                    $model = new TemporaryWorker;
                    $model->twNo = $_POST['TemporaryWorker']['twNo'];
                    $model->wkId = $_POST['TemporaryWorker']['wkId'];
                    $model->itemId = $_POST['proId'][$_POST['pk'][$key]];
                    $model->qty = $qty;
                    $model->costPrice = $_POST['sellPriceAmount'][$_POST['pk'][$key]];
                    $model->save();
                    $ptr = 1;
                endif;
            endforeach;
            if($ptr==1) :
                $msg = "Temporary Worker Assigned Successfully";
                $model=new TemporaryWorker;
            endif;
        }

		$this->render('_form',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

	/*
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
    	$msg = '';
        $model=new TemporaryWorker;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

        if(isset($_POST['TemporaryWorker']))
        {
            $model = TemporaryWorker::model()->find('twNo=:twNo',array(':twNo'=>$_POST['TemporaryWorker']['twNo']));
            $model->attributes=$_POST['TemporaryWorker'];

            // processing dw details
            if(isset($_POST['qty'])) :
                foreach($_POST['qty'] as $key=>$qty) :
                    if($qty>0) :
                        $sql = "update pos_temporary_worker set qty=".$qty.", moAt='".date("Y-m-d H:i:s")."' where twNo='".$_POST['TemporaryWorker']['twNo']."' and itemId=".$_POST['proId'][$_POST['pk'][$key]];
                        $command = Yii::app()->db->createCommand($sql);
                        $command->execute();
                    endif;
                endforeach;
            endif;

            $msg = "Temporary Worker Updated Successfully";
            $model=new TemporaryWorker;
            //$this->redirect(array('view','id'=>$model->id));
        }
		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

    /*
	 * Receive a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionReceive()
    {
        $msg = '';
        $model=new TemporaryWorker;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['TemporaryWorker']))
        {
            $model = TemporaryWorker::model()->find('twNo=:twNo',array(':twNo'=>$_POST['TemporaryWorker']['twNo']));
            $model->attributes=$_POST['TemporaryWorker'];

            // processing dw details
            if(isset($_POST['qty'])) :
                foreach($_POST['qty'] as $key=>$qty) :
                    if($qty>0) :
                        $sql = "update pos_temporary_worker set rqty=rqty+".$qty.", moAt='".date("Y-m-d H:i:s")."' where twNo='".$_POST['TemporaryWorker']['twNo']."' and itemId=".$_POST['proId'][$_POST['pk'][$key]];
                        $command = Yii::app()->db->createCommand($sql);
                        $command->execute();
                    endif;
                endforeach;
            endif;

            $msg = "Temporary Worker Received Successfully";
            $model=new TemporaryWorker;
            //$this->redirect(array('view','id'=>$model->id));
        }
        $this->render('receive',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TemporaryWorker');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new TemporaryWorker('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TemporaryWorker']))
			$model->attributes=$_GET['TemporaryWorker'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return TemporaryWorker the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TemporaryWorker::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param TemporaryWorker $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='temporary-worker-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
