<?php
/*********************************************************
        -*- File: ReportXlsController.php
		-*- Author: Md.kamruzzaman <kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/config
		-*-  YII-*- version 1.1.16
/*********************************************************/
class ReportXlsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    // supplier lists with date range
    public function actionSupplierList()
    {
        $model = $dataXls = array();		
		$startDate='';
		$endDate='';
		$supplier='';
		$companyName='';
		$branchAddress='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['supplier']) && !empty($_REQUEST['supplier']) ) $supplier = $_REQUEST['supplier'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		

        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];


		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Suppliers List');
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From : '." ".$startDate." to ".$endDate);

        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Type');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Name');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Phone');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Email');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Created');
        $i = 7;
        if(!empty($model))
        {
	        foreach($model as $key=>$data)
	        {
	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->suppId);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->crType);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->phone);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->email);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->crAt);
	            $i++;
	        }
        }
        $xlxFile = 'SuppliersList';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
        
    }
	
	/*
	* itemList  XLX REport
	*/
	public function actionItemList()
    {

        $model = $dataXls = array();		
		$startDate='';
		$endDate='';
		$supplier='';
		$companyName='';
		$branchAddress='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['supplier']) && !empty($_REQUEST['supplier']) ) $supplier = $_REQUEST['supplier'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		

        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];


		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Items List');
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From : '." ".$startDate." to ".$endDate);

        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Barcode');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Category');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Brand');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Creation');
        $i = 7;
        if(!empty($model))
        {
	        foreach($model as $key=>$data)
	        {
	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,($data->barCode==0)?'':$data->barCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->cat->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->brand->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->isParent==Items::IS_PARENTS?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->crAt);
	            $i++;
	        }
        }
        $xlxFile = 'itemsList';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
    }
	
	// item search xlx generations
	public function actionSearchItems()
	{
        $model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['brandName']) && !empty($_REQUEST['brandName']) ) $brandName = 'Brand : '.$_REQUEST['brandName'].', ';
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier : '.$_REQUEST['suppName'].', ';
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = 'Category : '.$_REQUEST['catName'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('F1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('F2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('F3','Item Search Result');
        $objPHPExcel->getActiveSheet()->setCellValue('C4',$brandName."".$suppName."".$catName);

        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Brand');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Category');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Bar Code');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Sell Price');
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Weighted');
        $i = 7;
        if(!empty($model))
        {
	        foreach($model as $key=>$data)
	        {
	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->brand->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->cat->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->barCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->costPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->sellPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->isWeighted);
	            $i++;
	        }
        }
        $xlxFile = 'itemSearch';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
    }

	/*
	* suppliercreditPayment XLX 
	*/
	public function actionSuppliercreditPayment(){

        $model = $dataXls = $modelwidraw = array();
		$companyName='';
		$branchAddress='';	
		$startDate='';
		$endDate='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		if(isset(Yii::app()->session['modelwidraw']) && !empty(Yii::app()->session['modelwidraw']))
		{
			  $modelwidraw = Yii::app()->session['modelwidraw'];
			  unset(Yii::app()->session['modelwidraw']);
			  Yii::app()->session['reportModelPreviousSecound'] = $modelwidraw;
		}else $modelwidraw = Yii::app()->session['reportModelPreviousSecound'];

				

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('F1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('F2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('F3','Suppliers Credit Payment Summary');
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From :'."".$startDate." to ".$endDate);

        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','GRN Date');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','GRN No.');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Received');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','');

        $objPHPExcel->getActiveSheet()->setCellValue('F6','Payment Date');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','PGRN No.');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Payment');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Remarks');

        $i = 7;
        if(!empty($model))
        {
        	$srl = $totalReceipt = 0;
            $grnNo = '';
            $amount = 0;
	        foreach($model as $key=>$data)
	        {
	            if(!empty($data->amount)){
                    $totalReceipt+=$data->amount;
                }
                if(!empty($data->grn)){
                    $grnNo = $data->grn->grnNo;
                }if(!empty($data->amount)){
                    $amount = $data->amount;
                }
	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->crAt);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$grnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$amount);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total Received:');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalReceipt);
        }
        $j=7;
        if(!empty($modelwidraw))
        {
        	$totalPayment = 0;
        	foreach($modelwidraw as $data)
        	{
                if(!empty($data->amount)) {
                    $totalPayment += $data->amount;
                }
        		$objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$data->crAt);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$data->pgrnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$data->amount);
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$j,($data->bankId)>0?'Bank':'Cash');
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$j,$data->remarks);
	            $j++;
        	} 
        	$objPHPExcel->getActiveSheet()->setCellValue('G'.$j,'Total Payment:');
        	$objPHPExcel->getActiveSheet()->setCellValue('H'.$j,$totalPayment);
        }
        $objPHPExcel->getActiveSheet()->setCellValue('A'.($i+1),'BALANCE ');
        if(!empty($totalReceipt) || !empty($totalPayment)){
            $objPHPExcel->getActiveSheet()->setCellValue('B'.($i+1), round($totalReceipt-$totalPayment,2));
        }

        

        $xlxFile = 'SuppliersCreditPaymentSummary';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}


	/*
	*  damageList  XLX Report
	*/

	public function actionDamageList()
	{

		$model = $dataXls = array();
		$companyName='';$branchAddress='';	
		$brandName='';	$suppName='';
		$catName=''; $deptName='';
		$startDate=''; $endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['brandName']) && !empty($_REQUEST['brandName']) ) $brandName = 'Brand : '.$_REQUEST['brandName'].', ';
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier : '.$_REQUEST['suppName'].', ';
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = 'Category : '.$_REQUEST['catName'];
		if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName']) ) $deptName = 'Department  : '.$_REQUEST['deptName'];
		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('E1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('E2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('E3','Items Damage List');
        $objPHPExcel->getActiveSheet()->setCellValue('D4',$deptName.'Date From :'."".$startDate." to ".$endDate);



        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Date');        

        
        $i = 7;
        if(!empty($model))
        {
        	$srl = $totalReceipt = $totalQty = $totalPrice =0;
	        foreach($model as $key=>$data)
	        {
                $dataQty = '';
                $dwNo = '';
                $itemCode = '';
                $itemName = '';
                $supplierName = '';
                $isEcommerce = '';
                $isParent = '';
                $damDate = '';
                $costPrice = '';
	            if(!empty($data->qty)){
                    $totalQty+=$data->qty;
                    $totalPrice+=$data->qty*$data->costPrice;
                    $dataQty = $data->qty;
                }
                if(!empty($data->dwNo)){
                   $dwNo = $data->dwNo;
                }
                if(!empty($data->item)){
                   $itemCode = $data->item->itemCode;
                }
                if(!empty($data->item)){
                   $itemName = $data->item->itemName;
                }if(!empty($data->item)){
                   $supplierName = $data->item->supplier->name;
                }
                if(!empty($data->item)){
                    $isEcommerce = $data->item->isEcommerce;
                }if(!empty($data->item)){
                    $isParent = $data->item->isParent;
                }
                if(!empty($data->damDate)){
                    $damDate = $data->damDate;
                }
                if(!empty($data->costPrice)){
                    $costPrice = $data->costPrice;
                }

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$dwNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$supplierName);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$dataQty);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$dataQty*$costPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$isParent==Items::IS_PARENTS?"Yes":"No");
	            
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$damDate);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalPrice);
        }

        $xlxFile = 'damageList';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	

	}


	/*
	*  wastageList XLX Report 
	*/
	
	public function actionWastageList()
	{

		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';
		$deptName='';
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('E1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('E2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('E3','Items Wastage List');
        $objPHPExcel->getActiveSheet()->setCellValue('D4','Date From :'."".$startDate." to ".$endDate);


        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Weight');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Cost Price');

        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');

        $objPHPExcel->getActiveSheet()->setCellValue('J6','Date');        

        
        $i = 7;
        if(!empty($model))
        {
        	$srl = $totalReceipt = $totalQty = $totalPrice =0;
	        foreach($model as $key=>$data)
	        {
	        	
				$totalQty+=$data->qty;
				$totalPrice+=$data->qty*$data->costPrice;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->dwNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->item->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->qty);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->qty*$data->costPrice);	         
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->damDate);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalPrice);
        }

        $xlxFile = 'wastageList';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}

	/*
	* priceChangeReport  xlx Report
	*/
	public function actionPriceChangeReport()
	{
		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';
		$deptName='';
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		
	
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		
		

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('F1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('F2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('F3','Price Change Report');
        $objPHPExcel->getActiveSheet()->setCellValue('E4','Date From :'."".$startDate." to ".$endDate);

         	 		
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Changed Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Changed Sell Price');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Change Date');        
        $objPHPExcel->getActiveSheet()->setCellValue('K6','Changes By');        

        /*
		!empty(Yii::app()->session['reportModel'])  
		*/
        $i = 7;
        if(!empty($model))
        {
        	$totalsellPrice  = $totalCostPrice = 0;
	        foreach($model as $key=>$data)
	        {
	        	$totalsellPrice+=$data->sellPrice;
				$totalCostPrice+=$data->costPrice;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->icnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->item->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->costPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->sellPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->changeDate);
	            $objPHPExcel->getActiveSheet()->setCellValue('K'.$i,$data->crBy0->username);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalCostPrice);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalsellPrice);
        }

        $xlxFile = 'priceChangeReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}

	/*
	* PurchaseSummaryList  xlx Report
	*/
	public function actionPurchaseSummaryList()
	{
		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';
		$deptName='';
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName ='Supplier  '. $_REQUEST['suppName'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Purchase Order Summary Report');
        $objPHPExcel->getActiveSheet()->setCellValue('C4',$suppName.'Date From :'."".$startDate." to ".$endDate);

        	 		
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Weight');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Price');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','PO Date');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Remarks');      
             
 
        $i = 7;
        if(!empty($model))
        {
        	$totalQty = $totalWeight = $totalPrice = 0;
	        foreach($model as $key=>$data)
	        {
	        	$totalQty+=$data->totalQty;
				$totalWeight+=$data->totalWeight;
				$totalPrice+=$data->totalPrice;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->poNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->totalQty);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->totalWeight);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->totalPrice);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->poDate);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->remarks);	            
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalWeight);
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalPrice);
        }

        $xlxFile = 'purchaseSummaryList';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}

	/*
	*  purchaseReturnDetailsList  xlx Report 
	*/
	 public function actionPurchaseReturnDetailsList()
	 {

		$model = $dataXls = array();
		$companyName='';		$branchAddress='';			$brandName='';
		$suppName='';		$catName='';		$prNo='';		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		if(isset($_REQUEST['prNo']) && !empty($_REQUEST['prNo']) ) $prNo = $_REQUEST['prNo'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Purchase Return Details Report');
        $objPHPExcel->getActiveSheet()->setCellValue('D4','PR No. '.$prNo);

        //Sl. No 	Item Code 	Description 	Supplier 	Quantity 	Cost Price 	PR Date	 		
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Child Item'); 

        $objPHPExcel->getActiveSheet()->setCellValue('I6','PR Date');             
             
 
        $i = 7;
        if(!empty($model))
        {
        	$totalQty  = $totalPrice = 0;
	        foreach($model as $key=>$data)
	        {
	        	$totalQty+=$data->qty;
				$totalPrice+=$data->qty*$data->costPrice;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->item->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data->qty,2));
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round(($data->qty*$data->costPrice),2));
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
 				$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->prDate);	                        
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney(round($totalPrice,2)));
	        
        }

        $xlxFile = 'PurchaseReturnDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}

	/*
	* purchaseReturnSummaryList  xlx Report
	*/

	public function actionPurchaseReturnSummaryList()
	{


		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';
		$suppName='';
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier  : '.$_REQUEST['suppName'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Purchase Return Summary Report');
         $objPHPExcel->getActiveSheet()->setCellValue('B4',$suppName.'Date From :'."".$startDate." to ".$endDate);

        //Sl. No 	Item Code 	Description 	Supplier 	Quantity 	Cost Price 	PR Date	 		
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','PR Date');             
             
 
        $i = 7;
        if(!empty($model))
        {
        	$totalQty = $totalPrice = 0;
	        foreach($model as $key=>$data)
	        {
	            $totalQty+=$data['totalQty'];
				$totalPrice+=$data['totalPrice'];

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['prNo']);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($data['totalQty'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['totalPrice'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data['prDate']);	                             
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($totalQty,2));
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney(round($totalPrice,2)));
	        
        }

        $xlxFile = 'PurchaseReturnSummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}

	/*
	* grnDetailsList Report xlx
	*/
	public function actionGrnDetailsList()
	{
		$model = $dataXls = array();
		$companyName='';	$branchAddress='';	
		$brandName='';	$suppName='';
		$catName='';	$suppName='';
		$startDate='';	$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier  : '.$_REQUEST['suppName'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Goods Receipt Details Report');
         $objPHPExcel->getActiveSheet()->setCellValue('C4',$suppName.'Date From :'."".$startDate." to ".$endDate);

        //Sl. No 	Item Code 	Description 	Supplier 	Quantity 	Cost Price 	PR Date	 		
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','GRN No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Cost Price');   
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');           
        $objPHPExcel->getActiveSheet()->setCellValue('J6','GRN Date');             
             
 
        $i = 7;
        if(!empty($model))
        {
        	$totalQty  = $totalPrice = 0;
	        foreach($model as $key=>$data)
	        {
	           	$totalQty+=$data->qty;
				$totalPrice+=$data->qty*$data->costPrice;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->grn->grnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->item->supplier->name);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->qty);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round(($data->qty*$data->costPrice),2));	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
 				$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->stockDate);

	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('EB'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($totalPrice,2));
	        
        }

        $xlxFile = 'GoodsReceiptDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}


	/*
	* grmSummaryList  xlx Report
	*/
	public function actionGrmSummaryList()
	{
		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';
		$suppName='';
		$catName='';		
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier  : '.$_REQUEST['suppName'];		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			$model = Yii::app()->session['reportModel'];
			unset(Yii::app()->session['reportModel']);
			Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('E1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('E2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Goods Receipt Summary Report');
        $objPHPExcel->getActiveSheet()->setCellValue('C4',$suppName.'Date From :'."".$startDate." to ".$endDate);

        //Sl. No 	PO No. 	GRN No. 	Supplier 	Quantity 	Weight 	Price 	Discount 	PO Date 	Remarks
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','PO No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','GRN No.');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Weight');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Price');             
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Discount');             
        $objPHPExcel->getActiveSheet()->setCellValue('I6','PO Date');             
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Remarks');             
             
 
        $i = 7;
        if(!empty($model))        {
        	$totalQty = $totalWeight = $totalPrice = $totalDiscount = 0;
	        foreach($model as $key=>$data)
	        {
	           	$totalQty+=$data->totalQty;
				$totalWeight+=$data->totalWeight;
				$totalPrice+=$data->totalPrice;
				$totalDiscount+=$data->totalDiscount; 

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->po->poNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->grnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->totalQty);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->totalWeight);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->totalPrice);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->totalDiscount);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->grnDate);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->remarks);	                             
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalWeight);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalPrice);
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$totalDiscount);
	        
        }

        $xlxFile = 'GoodsReceiptSummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	
	/*
	*  grmDiscoubtReport  xlx Report
	*/
    public function actionGrmDiscoubtReport()
	{
		$model = $dataXls = array();
		$companyName='';
		$branchAddress='';	
		$brandName='';		
		$catName='';		
		$startDate='';
		$endDate='';
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];		
		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			$model = Yii::app()->session['reportModel'];
			unset(Yii::app()->session['reportModel']);
			Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('E1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('E2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Goods Receipt Discount Report');
        $objPHPExcel->getActiveSheet()->setCellValue('D4','Date From :'."".$startDate." to ".$endDate);

        //PO No. 	GRN No. 	Supplier 	Quantity 	Weight 	Discount 	PO Date 	Remarks
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','PO No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','GRN No.');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Weight');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Discount');             
        $objPHPExcel->getActiveSheet()->setCellValue('H6','PO Date');             
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Remarks');           
                     
        $i = 7;
        if(!empty($model))        {
        	$totalQty = $totalWeight = $totalPrice = $totalDiscount = 0;
	        foreach($model as $key=>$data)
	        {
	            $totalQty+=$data->totalQty;
				$totalWeight+=$data->totalWeight;
				$totalDiscount+=$data->totalDiscount;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->po->poNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->grnNo);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->totalQty);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->totalWeight);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->totalDiscount);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->grnDate);	                             
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->remarks);                          
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalQty);
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalWeight);
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalDiscount);	        
	        
        }

        $xlxFile = 'GoodsReceiptDiscountReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/*
	* itemWiseStock  xlx report 
	*/
	public function actionItemWiseStock()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$supplier='';		$companyName='';
		$branchAddress='';		$deptName='';
		$subdeptName='';		$suppName='';
		$itemCode='';		$catName='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['supplier']) && !empty($_REQUEST['supplier']) ) $supplier = $_REQUEST['supplier'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName']) ) $deptName = 'Department :'.$_REQUEST['deptName'];
		if(isset($_REQUEST['subdeptName']) && !empty($_REQUEST['subdeptName']) ) $subdeptName = 'Sub Department :'.$_REQUEST['subdeptName'];
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier :'.$_REQUEST['suppName'];
		if(isset($_REQUEST['itemCode']) && !empty($_REQUEST['itemCode']) ) $itemCode = 'Item Code :'.$_REQUEST['itemCode'];
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = 'Category :'.$_REQUEST['catName'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Item wise stock Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4',$deptName.', '.$subdeptName.', '.$suppName.', '.$itemCode.', '.$catName);
        $objPHPExcel->getActiveSheet()->setCellValue('C5','Date From :'.$endDate);

        //Sl. No 	Item Code 	Description 	Supplier 	Stock
        $objPHPExcel->getActiveSheet()->setCellValue('A7','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B7','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C7','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('D7','Supplier');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Child Item'); 
        $objPHPExcel->getActiveSheet()->setCellValue('G7','Stock');           
                     
        $i = 8;
        if(!empty($model))    
		{
        	$totalQty = $totalWeight = $totalPrice = $totalDiscount = 0;
	        foreach($model as $key=>$data)
	        {
	            $stockQty = Stock::getItemWiseStock($data->itemId);
				$totalQty+=$stockQty;

	            $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->item->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->supplier->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
 		        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$stockQty);                         
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalQty);        
	    }

        $xlxFile = 'ItemwisestockReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/*
	*stockValuationSummary  xlx Report
	*/
	public function actionStockValuationSummary()
	{
		$model = $dataXls = array();
		$endDate='';
		$companyName='';
		$branchAddress='';			
		
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];		
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
        $objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Stock valuation summary Report');
        $objPHPExcel->getActiveSheet()->setCellValue('B4',"Date till ".$endDate);

        //Sl. No 	Item Code 	Description 	Supplier 	Stock
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Department');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total');           
                     
        $i = 7;
        if(!empty($model))        {
        	$totalAmount  = 0;
	        foreach($model as $key=>$data)
	        {
			    $sql = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId IN(SELECT id FROM pos_department WHERE id=".$data->id.") and s.stockDate<='".$endDate."' and s.branchId=".Yii::app()->session['branchId']." and s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";
						$stockModel = Stock::model()->findAllBySql($sql);	
				if(!empty($stockModel)) :
					$amount = 0;
					foreach($stockModel as $dataStock) :
						$amount+=Stock::getItemWiseStock($dataStock->itemId)*$dataStock->item->costPrice;
					endforeach;
					$totalAmount+=$amount;			
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney($amount, true));
	            endif;
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney($totalAmount, true));        
	    }

        $xlxFile = 'StockvaluationsummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	
	/*
	* invSummaryReport xlx Report 
	*/
	
	public function actionInvSummaryReport()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$supplier='';		$companyName='';
		$branchAddress='';		$deptName='';
		$subdeptName='';		$suppName='';
		$itemCode='';		$catName='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['supplier']) && !empty($_REQUEST['supplier']) ) $supplier = $_REQUEST['supplier'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName']) ) $deptName = $_REQUEST['deptName'];
		if(isset($_REQUEST['subdeptName']) && !empty($_REQUEST['subdeptName']) ) $subdeptName = $_REQUEST['subdeptName'];
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = $_REQUEST['suppName'];
		if(isset($_REQUEST['itemCode']) && !empty($_REQUEST['itemCode']) ) $itemCode = $_REQUEST['itemCode'];
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = $_REQUEST['catName'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Inventory Summary Report');
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From :'."".$startDate." to ".$endDate);

        //		 	 		 	
        $objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','IV No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total Quantity');           
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Total Weight');           
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Total Adjust Quantity');           
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Total Price');           
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Total Adjust Price');           
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Date');           
                     
        $i = 7;
        if(!empty($model)){
        	$totalQty = $totalWeight = $totalPrice = $totaladjQty = $totaladjPrice = 0;
	        foreach($model as $key=>$data)
	        {
				$totalQty+=$data['tQty']; $totalPrice+=$data['tPrice'];
				$totalWeight+=$data['tWeight'];
				$totaladjQty+=$data['tAdQty'];
				$totaladjPrice+=$data['tAdPrice'];
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['invNo']);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['tQty']);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['tWeight']);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data['tAdQty']);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data['tPrice']);
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data['tAdPrice']);
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data['invDate']);
	            
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalQty);        
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalWeight);        
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totaladjQty);        
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($totalPrice,2));        
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($totaladjPrice,2));        
	    }

        $xlxFile = 'InventorySummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	
	/*
	* cardWiseSells xlx report
	*/
	public function actionCardWiseSells()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Deposit Details Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Date From :'."".$startDate." to ".$endDate);

     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','VAT');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Amount');         
                     
        $i = 7;
        if(!empty($model)){
        	$srl = $totalnetAmount = $totalbankCommision = 0;
	        foreach($model as $key=>$data)
	        {
				$netAmount = $data->cardPaid;
				$bankCommision = round((($netAmount*$data->card->commision)/100),2);
				$totalnetAmount+=$netAmount;
				$totalbankCommision+=$bankCommision;
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->card->name);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$netAmount);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->card->commision);
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$bankCommision);
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->orderDate);
	            
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalnetAmount);        
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'');        
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalbankCommision);       
	    }

        $xlxFile = 'BankDepositDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	
	/*
	* vatReport  xlx report 
	*/
	public function actionVatReport()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';
		$date='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['date']) && !empty($_REQUEST['date']) ) $date = $_REQUEST['date'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Vat Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Date From :'."".$startDate." to ".$endDate);

     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank/Card ');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total Collection');        
                     
        $i = 7;
        if(!empty($model)){
        	$srl = $totalnetAmount = $totalAmount = $totalbankCommision = 0;
	        foreach($model as $key=>$data)
	        {
				$vatTotal = 0;
				$srl++; 
				$sqlvatTotal = "SELECT SUM(s.vatPrice) AS vatTotal FROM pos_sales_details s,pos_sales_invoice i WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']." AND s.salesDate BETWEEN '".$startDate."' 
								AND '".$endDate."' AND s.itemId IN(SELECT id FROM pos_items WHERE taxId=(SELECT id FROM pos_tax WHERE taxRate=".$data->taxRate."))";
				$resvatsells = Yii::app()->db->createCommand($sqlvatTotal)->queryRow();
				if(!empty($resvatsells['vatTotal'])) $vatTotal   =  round($resvatsells['vatTotal'],2);	
				$totalAmount+=$vatTotal;
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->taxRate.'%');
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$vatTotal);
	            
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalAmount);     
	    }

        $xlxFile = 'vatReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/*
	* miscIncome  xlx report
	*/
	public function actionMiscIncome()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';
		$catModel='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['catModel']) && !empty($_REQUEST['catModel']) ) $date = $_REQUEST['catModel'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Misc. Income Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Date From :'."".$startDate." to ".$endDate);

     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Date');        
                     
        $i = 7;
        if(!empty($model)){
        	$totalAmount = 0;
	        foreach($model as $key=>$data)
	        {
				$totalAmount+=$data->amount;			
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->remarks);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->amount);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->crAt);
	            
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalAmount);     
	    }

        $xlxFile = 'Misc.IncomeReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	
	/*
	* miscExpense xlx report
	*/
	
	public function actionMiscExpense()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';
		$catModel='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['catModel']) && !empty($_REQUEST['catModel']) ) $date = $_REQUEST['catModel'];
		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Misc. Expense Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Date From :'."".$startDate." to ".$endDate);

     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Description');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Amount');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','By');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');
                     
        $i = 7;
        if(!empty($model)){
        	$totalAmount = 0;
	        foreach($model as $key=>$data)
	        {
				$totalAmount+=$data->amount;
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->remarks);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->amount);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,($data->bankId)>0?'Bank':'Cash');
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->crAt);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($totalAmount,2));     
	    }

        $xlxFile = 'Misc.ExpenseReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/****************
	*  HourlySales  xlx REport
	****************/
	public function actionHourlySales()
	{
		$model = $dataXls = array();
		$startDate='';			$companyName='';
		$branchAddress='';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Hourly Sales Report');
        $objPHPExcel->getActiveSheet()->setCellValue('D4','Date:'."".$startDate);
		//Sl. No 	Hour 	Cash 	Card 	Reedem Amount 	Gross Total 	Avg. Busket
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Hour');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Cash');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Card');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Discount');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Reedem Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Coupon Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Gross Total');        
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Avg. Busket');        
                     
        $i = 7;
        if(!empty($model)){

        	$srl = $cashPaid = $cardPaid = $totalDiscount = $loyaltyPaid = $totalCouponAmount = $netPaid = $totalPaid = 0;
	        foreach($model as $key=>$val)
	        {
				$nextKey = $key+1;					
				$sql = "SELECT COUNT(id) AS totalBill,SUM(totalPrice) AS totalPrice,SUM(cashPaid)-SUM(totalChangeAmount) AS cashPaid,SUM(cardPaid) AS cardPaid,SUM(totalDiscount) AS totalDiscount,SUM(loyaltyPaid) AS loyaltyPaid FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate>'".$startDate." ".$key."' AND orderDate<'".$startDate." ".$nextKey."' AND isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND status=".SalesDetails::STATUS_ACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d %H') ORDER BY orderDate ASC";
				$data = Yii::app()->db->createCommand($sql)->queryRow();
				
				if(!empty($data)) :
					$srl++; 					
					$cashPaid+=$data['cashPaid'];
                    $cardPaid+=$data['cardPaid'];
                    $totalDiscount+=$data['totalDiscount'];
                    $loyaltyPaid+=$data['loyaltyPaid'];
                    $totalPaid+=$data['totalPrice'];

                    // coupon Amount Processing
                    $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate>'".$startDate." ".$key."' AND orderDate<'".$startDate." ".$nextKey."' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate ASC";
                    $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                    $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
				
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
		            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$val);
		            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($data['cashPaid'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['cardPaid'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data['totalDiscount'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($data['loyaltyPaid'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, round($modelCouponAmount['totalCouponAmount'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round($data['totalPrice'],2));
		            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,round($data['totalPrice']/$data['totalBill'],2));
		        $i++;
	            endif;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney(round($cashPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney(round($cardPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,UsefulFunction::formatMoney(round($totalDiscount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney(round($loyaltyPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,UsefulFunction::formatMoney(round($totalCouponAmount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,UsefulFunction::formatMoney(round($totalPaid,2)));      
	    }

        $xlxFile = 'HourlySalesReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}
	
	/*************
	*Current Sales xlx Report  Report
	*************/
	
	public function actionCurrentSales()
	{
		$model = $dataXls = array();
		$date='';			$companyName='';
		$branchAddress='';		
		
		if(isset($_REQUEST['date']) && !empty($_REQUEST['date']) ) $date = $_REQUEST['date'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Current Sales');
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date Time :'."".$date);
		//Sl. No 	Cash 	Card 	Reedem Amount 	Gross Total
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Cash');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Card');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Discount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Reedem Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Coupon Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Gross Total');        
       
		
        $i = 7;
        if(!empty($model)){
        	$srl = $cashPaid = $cardPaid = $totalDiscount = $totalCouponAmount = $loyaltyPaid = $netPaid = $totalPaid = 0;
        	
	        foreach($model as $key=>$data)
	        {

				$srl++;
				if(!empty($data['cashPaid'])){
                    $cashPaid+=$data['cashPaid']-$data['totalChangeAmount'];
                    $cardPaid+=$data['cardPaid'];
                }
                if(!empty($data['totalDiscount'])){
                    $totalDiscount+=$data['totalDiscount'];
                }
                if(!empty($data['loyaltyPaid'])){
                    $loyaltyPaid+=$data['loyaltyPaid'];
                }
                if(!empty($data['totalPrice'])){
                    $totalPaid+=$data['totalPrice'];
                }

                $dataId = '';
                if(!empty($data['id'])){
                    $dataId = $data['id'];

                }
				// coupon Amount Processing
                $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.id=".$dataId." AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate>=DATE_SUB(NOW(),INTERVAL 10 HOUR) AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate ASC";
                $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                if(!empty($modelCouponAmount['totalCouponAmount'])){
                    $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
                }

				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,round(($data['cashPaid']-$data['totalChangeAmount']),2));
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($data['cardPaid'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['totalDiscount'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data['loyaltyPaid'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($modelCouponAmount['totalCouponAmount'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($data['totalPrice'],2));
				
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,UsefulFunction::formatMoney(round($cashPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney(round($cardPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney(round($totalDiscount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,UsefulFunction::formatMoney(round($loyaltyPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney(round($totalCouponAmount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,UsefulFunction::formatMoney(round($totalPaid,2)));      
	    }

        $xlxFile = 'CurrentSales';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}

	/************
	*  Department Wise Sales Report xlx Report 
	*********/
	
	public function actionDepartmentWiseSales()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';	
		$salesType='';	
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType'])) $salesType = $_REQUEST['salesType'];		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg=' (Online Sale)';
		else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg=' (Local Sale)'; 
		else $typeMsg='';
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Department Wise Sales Report'.$typeMsg);
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date Time :'.$startDate.' to '.$endDate);
		//Department 	Cost of Sales 	Net Sales 	VAT 	Gross Sales 	Gross Profit 	GP %
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Department');	
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Cost of Sales');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Net Sales');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','VAT');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Discount');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Gross Sales');        
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Gross Profit');        
        $objPHPExcel->getActiveSheet()->setCellValue('I6','GP %');        
       
		
        $i = 7;
        if(!empty($model)){
        	 $srl = $totalCostSales  = $totalNetSales = $totalNetSalesVat = $totalSalesDiscount = 0;
			$filter = '';
			if(isset($salesType) && !empty($salesType))
			   $filter.=' AND v.isEcommerce='.$salesType;
	        foreach($model as $key=>$data)
	        {
				$sqlcostsales = "SELECT ROUND(SUM(s.qty*s.costPrice),2) AS totalCostPrice FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".SalesDetails::STATUS_ACTIVE." ORDER BY s.salesDate DESC";
				$modelcostsales = Yii::app()->db->createCommand($sqlcostsales)->queryRow();
				
				// net sales
				$sqlnetsales = "SELECT ROUND((SUM(s.qty*s.salesPrice)-SUM(s.discountPrice)),2) AS totalSell FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
				$modelnetsales = Yii::app()->db->createCommand($sqlnetsales)->queryRow();
				
				// net vat
				$sqlnetsalesvat = "SELECT ROUND(SUM(s.vatPrice),2) AS totalTax FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
				$modelnetsalesvat = Yii::app()->db->createCommand($sqlnetsalesvat)->queryRow();
                
                // net discount
				$sqlnetsalesdiscount = "SELECT ROUND(SUM(s.discountPrice),2) AS totalDiscount FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
				$modelnetsalesdiscount = Yii::app()->db->createCommand($sqlnetsalesdiscount)->queryRow();
				
				$srl++; 
				$totalCostSales+=$modelcostsales['totalCostPrice'];
				$totalNetSales+=$modelnetsales['totalSell'];
				$totalNetSalesVat+= $modelnetsalesvat['totalTax'];
                $totalSalesDiscount+= $modelnetsalesdiscount['totalDiscount'];

				$rDate='';
				if($modelnetsales['totalSell']>0)
				$rDate = round((($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'])*100)/$modelnetsales['totalSell'],2);

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->name);	                
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney($modelcostsales['totalCostPrice'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney($modelnetsales['totalSell'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,UsefulFunction::formatMoney($modelnetsalesvat['totalTax'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney($modelnetsalesdiscount['totalDiscount'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,UsefulFunction::formatMoney($modelnetsales['totalSell']+$modelnetsalesvat['totalTax']+$totalSalesDiscount['totalDiscount'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,UsefulFunction::formatMoney($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'], true));
	            $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$rDate);

	            $i++; 				
	        }
			$rDate2='';			
			if($totalNetSales>0) $rDate2 = round((($totalNetSales-$totalCostSales)*100)/$totalNetSales,2);
				
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,UsefulFunction::formatMoney($totalCostSales, true));      
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney($totalNetSales, true));      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,UsefulFunction::formatMoney($totalNetSalesVat, true));      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney($totalSalesDiscount, true));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,UsefulFunction::formatMoney($totalNetSales+$totalNetSalesVat+$totalSalesDiscount, true));
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,UsefulFunction::formatMoney($totalNetSales-$totalCostSales, true));      
	        $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$rDate2);      
	    }

        $xlxFile = 'DepartmentWiseSalesReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);

        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}
	
	/********************
	*  Category Wise Sales Report  xlx Report
	*************/
	public function actionCategoryWiseSales()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';		$branchAddress='';		
		$deptName='';	$subdeptName='';		
		$catName= $salesType = '';	
		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName']) ) $deptName = 'Department : '.$_REQUEST['deptName'];
		if(isset($_REQUEST['subdeptName']) && !empty($_REQUEST['subdeptName']) ) $subdeptName = ', Sub Department  :'.$_REQUEST['subdeptName'];
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = ', Category :'.$_REQUEST['catName'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType'])) $salesType = $_REQUEST['salesType'];
		
				
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg=' (Online Sale)';
		else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg=' (Local Sale)'; 
		else $typeMsg='';
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Category Wise Sales Report '.$typeMsg);
        $objPHPExcel->getActiveSheet()->setCellValue('A4',$deptName."".$subdeptName."".$catName);
        $objPHPExcel->getActiveSheet()->setCellValue('B5','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Item Code 	Description 	Sold Quantity 	Sold Amount
     	$objPHPExcel->getActiveSheet()->setCellValue('A7','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B7','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C7','Description');        
        $objPHPExcel->getActiveSheet()->setCellValue('D7','Sold Quantity');        
        $objPHPExcel->getActiveSheet()->setCellValue('E7','Sold Amount');      
		
        $i = 8;
        if(!empty($model)){
        	$srl = $totalQty  = $totalAmount = 0;
	        foreach($model as $key=>$data)
	        {
				$srl++; 
				$itemModel = Items::model()->findByPk($data['itemId']);
				$totalQty+=$data['totalSalesQty'];
				$totalAmount+=$data['totalAmount'];
					
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$itemModel->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$itemModel->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['totalSalesQty'],2));
	            $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data['totalAmount'],2));
				
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');      
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalQty,2));      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($totalAmount,2));         
	    }
        $xlxFile = 'CategoryWiseSalesReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}
	/*********
	*  Sales Movement Report xlx Report
	***********/
	public function actionSalesMovement()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';		$branchAddress='';
		$deptName='';		$subdeptName='';	
		$catName='';		$suppName='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName']) ) $deptName = 'Department : '.$_REQUEST['deptName'];
		if(isset($_REQUEST['subdeptName']) && !empty($_REQUEST['subdeptName']) ) $subdeptName = 'Sub Department  :'.$_REQUEST['subdeptName'];
		if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName']) ) $catName = 'Category :'.$_REQUEST['catName'];
		if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = 'Supplier  :'.$_REQUEST['suppName'];		
        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Sales Movement Report');
        $objPHPExcel->getActiveSheet()->setCellValue('A4',$deptName.", ".$subdeptName.", ".$suppName.", ".$catName);
        $objPHPExcel->getActiveSheet()->setCellValue('B5','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Item Code 	Description 	Sold Quantity 	Sold Amount
     	$objPHPExcel->getActiveSheet()->setCellValue('A7','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B7','Item Code');
        $objPHPExcel->getActiveSheet()->setCellValue('C7','Description');        
        $objPHPExcel->getActiveSheet()->setCellValue('D7','Sold Quantity');        
        
        $i = 8;
        if(!empty($model)){
        	echo '<pre>';
        	print_r($model);
        	exit();
        	$totalQty  = 0;
	        foreach($model as $key=>$data)
	        {			
				$itemModel = Items::model()->findByPk($data['itemId']);
				$totalQty+=$data['totalSales'];
					
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$itemModel->itemCode);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$itemModel->itemName);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['totalSales'],2));
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');      
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalQty,2));    
	    }
        $xlxFile = 'SalesMovementReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}
	
	/**********
	*	salesReturn xlx Report
	**************/
	public function actionSalesReturn()
	{
		$model = $dataXls = array();
		$startDate = $endDate='';
		$companyName='';
		$branchAddress='';
		$salesType ='';
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType']) ) $salesType = $_REQUEST['salesType'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else
        {
            $model = Yii::app()->session['reportModelPrevious'];
            unset(Yii::app()->session['reportModelPrevious']);
        }

        if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg = ' (Online Sale)';
	    else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg = ' (Local Sale)';
	    else $typeMsg = '';
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Sales Return Report '.$typeMsg);        
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date From :'."".$startDate.' to '.$endDate);

		//Sl. No 	Item Code 	Description 	Sold Quantity 	Sold Amount
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Return Date');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total Qty');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Total Amount');       
        
        $i = 7;		
        if(!empty($model)){
        	$totalnetAmount = $totalQty =  0;
	        foreach($model as $key=>$data)
	        {			
				$totalnetAmount+=$data['totalAmount'];
				$totalQty+=$data['totalQty'];
					
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['returnDate']);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['totalQty']);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['totalAmount']);
	            $i++;
	        }
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total : ');      
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalQty);    
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalnetAmount);    
	    }
        $xlxFile = 'SalesReturnReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }		
	}
	
	/********************
	* SalesDiscount xlx Report 
	**********************/
	public function actionSalesDiscount()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';		
		$salesType='';		
				
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType']) ) $salesType = $_REQUEST['salesType'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];

		if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg = ' (Online Sale)';
	    else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg = ' (Local Sale)';
	    else $typeMsg = '';
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Sales Discount Report' .$typeMsg);        
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Order Date 	Invoice No. 	Total Discount
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Order Date');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Invoice No.');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Total Discount');        
        
        $i = 7;
		
        if(!empty($model)){
        	$srl = $totalDiscount =  0;
	        foreach($model as $key=>$data)
	        {		
			
			if($data['totalDiscount']>0) :			
			 $totalDiscount+=$data['totalDiscount'];
					
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
	            $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['orderDate']);
	            $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['invNo']);
	            $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['totalDiscount']);
	            $i++;
			 endif;
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total :');    
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalDiscount);    
	    }
        $xlxFile = 'SalesDiscountReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	/************
	* specialDiscountReport  
	**********************/
	public function actionSpecialDiscountReport()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Special Discount Report');        
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Sales Date 	Total Special Discount
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Sales Date ');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total Special Discount');        
        
        $i = 7;
		
        if(!empty($model)){
        	$srl = $totalDiscount =  0;
	        foreach($model as $key=>$data)
	        {
			if($data['tsDiscount']>0) :
			$totalDiscount+=$data['tsDiscount'];
					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['orderDate']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['tsDiscount']);		
			$i++;
			 endif;
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total :');    
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalDiscount);    
	    }
        $xlxFile = 'specialDiscountReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/***********************
	* Basket Average Analysis Report  xlx Report
	**********************/
	public function actionBasketAverage()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';
		$salesType = '';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType']) ) $salesType = $_REQUEST['salesType'];
		
		if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg = ' (Online Sale)';
	    else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg = ' (Local Sale)';
	    else $typeMsg = '';

		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];	
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Basket Average Analysis Report '.$typeMsg);        
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Sales Date 	Total Sales 	Total Bill 	Busket Average
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Sales Date ');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Total Sales');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Total Bill');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Busket Average');        
          
        
        $i = 7;
		
        if(!empty($model)){
        	$totalnetAmount = $totalBill =  0;
			
	        foreach($model as $key=>$data)
	        {
				$totalnetAmount+=$data['totalAmount'];
				$totalBill+=$data['totalBill'];
						
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['orderDate']);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['totalAmount']);		
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['totalBill']);		
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round(($data['totalAmount']/$data['totalBill']),2));		
				$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total :');    
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalnetAmount);    
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalBill);    
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round(($totalnetAmount/$totalBill),2));    
	    }
        $xlxFile = 'BasketAverageAnalysisReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/************
	* cardReport xlx Report
	**********************/
	public function actionCardReport()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';		
		$cash='';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['cash']) && !empty($_REQUEST['cash']) ) $cash = 'Cashier :'.$_REQUEST['cash'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];	
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Card Wise Cashier Summary Report');        
        $objPHPExcel->getActiveSheet()->setCellValue('B4',$cash);        
        $objPHPExcel->getActiveSheet()->setCellValue('A5','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Sales Date 	Total Sales 	Total Bill 	Busket Average
     	$objPHPExcel->getActiveSheet()->setCellValue('A7','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B7','Bank/Card');
        $objPHPExcel->getActiveSheet()->setCellValue('C7','Total Collection');        
        $objPHPExcel->getActiveSheet()->setCellValue('D7','Date');        
   
       
        $i = 8;
		
        if(!empty($model)){
        	$netAmount = $totalnetAmount = $totalbankCommision = 0;
	        foreach($model as $key=>$data)
	        {
			$netAmount = $data->cardPaid;
			$totalnetAmount+=$netAmount;
					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->card->name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$netAmount);		
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->orderDate);	
			$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total :');    
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalnetAmount);      
	    }
        $xlxFile = 'CardWiseCashierSummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/************
	* collectionReport  xlx Report
	**********************/
	public function actionCollectionReport()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';		
		$cash='';
		$salesType='';		
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['cash']) && !empty($_REQUEST['cash']) ) $cash = 'Cashier :'.$_REQUEST['cash'];
		if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType']) ) $salesType =$_REQUEST['salesType'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];	
		

		if($salesType==SalesInvoice::IS_ECOMMERCE) $typeMsg = ' (Online Sale)';
	    else if($salesType==SalesInvoice::IS_NOT_ECOMMERCE) $typeMsg = ' (Local Sale)';
	    else $typeMsg = '';
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Counter wise sales/collection Report' .$typeMsg);     
       
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Sales Date 	Total Sales 	Total Bill 	Busket Average
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','POS');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Cashier');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Cash');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Card');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Discount');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Reedem Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Coupon Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Gross Total');        
   
       
        $i = 7;
		
        if(!empty($model)){
        	$cashPaid = $cardPaid = $loyaltyPaid = $totalCouponAmount =  $netPaid = $totalPaid = $totalDiscount = 0;
	        foreach($model as $key=>$data)
	        {
			$cashPaid+=$data['cashPaid'];
			$cardPaid+=$data['cardPaid'];
			$loyaltyPaid+=$data['loyaltyPaid'];
			$totalDiscount+=$data['totalDiscount'];
			
			$netPaid=($data['totalPrice']-$data['cardPaid'])+$data['cardPaid']+$data['loyaltyPaid'];
			$totalPaid+=$netPaid;

			$sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.crBy=".$data['crBy']." AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate BETWEEN '".$startDate."' AND '".$endDate."' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
            $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
            $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['pos']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['cashier']);		
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['cashPaid'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data['cardPaid'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($data['totalDiscount'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($data['loyaltyPaid'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round($modelCouponAmount['totalCouponAmount'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,round($netPaid,2));	
			$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total :');    
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,UsefulFunction::formatMoney(round($cashPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,UsefulFunction::formatMoney(round($cardPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney(round($totalDiscount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,UsefulFunction::formatMoney(round($loyaltyPaid,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,UsefulFunction::formatMoney(round($totalCouponAmount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,UsefulFunction::formatMoney(round($totalPaid,2)));      
	    }
        $xlxFile = 'CounterwisesalescollectionReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/************
	* purchaseDetailsList  xlx Report
	**********************/
	public function actionPurchaseDetailsList()
	{
		$model = $dataXls = array();
		$startDate='';		$endDate='';
		$companyName='';
		$branchAddress='';	
			
		
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];	
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Purchase Order Details Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date From :'."".$startDate.' to '.$endDate);
		//Sl. No 	Sales Date 	Total Sales 	Total Bill 	Busket Average
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','PO No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Item Code');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Description');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Supplier');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Quantity');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Ecommerce Item');
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Child Item');             
        $objPHPExcel->getActiveSheet()->setCellValue('J6','PO Date');        
   
       
        $i = 7;
		
        if(!empty($model)){
        	$totalQty  = $totalPrice = 0;
	        foreach($model as $key=>$data)
	        {
			$totalQty+=$data->qty;
			$totalPrice+=$data->qty*$data->costPrice;
					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->po->poNo);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemCode);		
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->itemName);	
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->item->supplier->name);	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->qty);	
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round(($data->qty*$data->costPrice),2));	
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No");
 			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->item->isParent==Items::IS_PARENTS?"Yes":"No");
	
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i,$data->crAt);	
			$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,'Total :');     
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$totalQty);      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($totalPrice,2));      
	    }
        $xlxFile = 'PurchaseOrderDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}
	
	/************
	* pos Tracer  xlx Report
	**********************/
	public function actionPosTracer()
	{
		$model = $dataXls = array();
		$date='';	
		$companyName='';
		$branchAddress='';	
			
		if(isset($_REQUEST['date']) && !empty($_REQUEST['date']) ) $date = $_REQUEST['date'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		else $model = Yii::app()->session['reportModelPrevious'];	
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','POS Tracer Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('D4','Date:'.$date);
		//Sl. No 	POS 	Cashier 	Cash 	Card 	Reedem Amount 	Gross Total 	Avg Busket
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','POS');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Cashier');        
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Cash');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Card');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Discount');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Reedem Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Coupon Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Gross Total');        
        $objPHPExcel->getActiveSheet()->setCellValue('J6','Avg Busket');        
   
       
        $i = 7;
		
        if(!empty($model)){
        	$cashPaid = $cardPaid = $totalDiscount = $totalCouponAmount = $loyaltyPaid = $netPaid = $totalPaid = 0;
	        foreach($model as $key=>$data)
	        {
			
			$cashPaid+=$data['cashPaid'];
			$cardPaid+=$data['cardPaid'];
            $totalDiscount+=$data['totalDiscount'];
			$loyaltyPaid+=$data['loyaltyPaid'];
			$totalPaid+=$data['totalPrice'];

			// coupon Amount Processing
            $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.crBy=".$data['crBy']." AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate LIKE '".date("Y-m-d")."%' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
            $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
            $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];

					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['pos']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['cashier']);		
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($data['cashPaid'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($data['cardPaid'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($data['totalDiscount'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data['loyaltyPaid']);	
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$modelCouponAmount['totalCouponAmount']);	
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,round($data['totalPrice'],2));	
			$objPHPExcel->getActiveSheet()->setCellValue('J'.$i,round(($data['totalPrice']/$data['totalBill']),2));	
			$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total :');     
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$cashPaid);      
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$cardPaid);      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,UsefulFunction::formatMoney(round($totalDiscount,2)));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$loyaltyPaid);      
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round(($totalCouponAmount),2));      
	        $objPHPExcel->getActiveSheet()->setCellValue('I'.$i,round(($totalPaid),2));      
	    }
        $xlxFile = 'POSTracerReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}

	/****************
	*dailyGeneralLedger xlx Report 
	***********************/
	public function actionDailyGeneralLedger()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';	
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		/*
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	*/
		
		if(isset(Yii::app()->session['datlyGL']) && !empty(Yii::app()->session['datlyGL']))
		{
			  $model = Yii::app()->session['datlyGL'];	  
		}
		//echo $model['expenseTotal'];
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','GL Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('A4','Date from:'.$startDate.' to '. $endDate);
		//Sl. No 	POS 	Cashier 	Cash 	Card 	Reedem Amount 	Gross Total 	Avg Busket
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Income');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Expense');   
   
       
        if(!empty($model)){        		
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'7','Net Sales : '.$model['netSell']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'7','Purchase  :'.$model['purchase']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'8','Purchase Return : '.$model['purchesReturn']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'8','Damages   :'.$model['damage']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'9','Misc. Income : '.$model['miscIncome']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'9','Wastages  :'.$model['wastage']);		
			
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'10','Transfer Out : '.'0');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'10','Misc. Expense  :'.$model['miscExpense']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'11','Closing Stock : '.$model['closingstok']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'11','Transfer In:'.'0');
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'12',''.' ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'12','Opening Stock  :'.$model['openingStok']);

			$objPHPExcel->getActiveSheet()->setCellValue('A'.'13',''.'0');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'13','Bank Commision :'.$model['bankComission']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'14',''.'');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'14','Profit :'.$model['profit']);
			$objPHPExcel->getActiveSheet()->setCellValue('A'.'15',''.$model['incomeTotal']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.'15',''.$model['expenseTotal']);	
			
	    }
        $xlxFile = 'GLReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
		unset(Yii::app()->session['datlyGL']);	
	}

	/****************
	*invDetails xlx Report 
	***********************/
	public function actionInvDetails()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';	
		$itemCode='';	
		$invNo='';	
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['itemCode']) && !empty($_REQUEST['itemCode']) ) $itemCode = 'Item Code'.$_REQUEST['itemCode'];
		if(isset($_REQUEST['invNo']) && !empty($_REQUEST['invNo']) ) $invNo = 'Inventory NO. '.$_REQUEST['invNo'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('D3','Inventory Details Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('D4','Date from '.$startDate.' to  '.$endDate );
        $objPHPExcel->getActiveSheet()->setCellValue('D5',$itemCode.','.$invNo);
		//Sl. No 	IV No. 	Item Code 	Description 	Quantity 	Cost Price 	Adjust Quantity 	Adjust Price 	Date
     	$objPHPExcel->getActiveSheet()->setCellValue('A7','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B7','IV No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C7','Item Code');        
        $objPHPExcel->getActiveSheet()->setCellValue('D7','Description');        
        $objPHPExcel->getActiveSheet()->setCellValue('E7','Quantity');        
        $objPHPExcel->getActiveSheet()->setCellValue('F7','Cost Price');        
        $objPHPExcel->getActiveSheet()->setCellValue('G7','Adjust Quantity');        
        $objPHPExcel->getActiveSheet()->setCellValue('H7','Adjust Price');        
        $objPHPExcel->getActiveSheet()->setCellValue('I7','Adjust Date');        
   
       
        $i = 8;
		
        if(!empty($model)){
        	$totalQty = $totalPrice = $totaladjQty = $totaladjPrice = 0;
	        foreach($model as $key=>$data)
	        {
			$totalQty+=$data->qty; $totalPrice+=($data->qty*$data->costPrice);
			$totaladjQty+=$data->adjustQty;
			$totaladjPrice+=($data->adjustQty*$data->costPrice);
					
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->invNo);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->item->itemCode);		
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->item->itemName);	
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->qty);	
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round(($data->qty*$data->costPrice),2));	
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->adjustQty);	
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round(($data->adjustQty*$data->costPrice),2));	
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$i,$data->invDate);	
			$i++;			
	        }	          
	        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total :');     
	        $objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalQty);      
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($totalPrice,2));      
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totaladjQty);      
	        $objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round($totaladjPrice,2));      
	    }
        $xlxFile = 'InventoryDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }	
	}

	/****************
	*supplierconsigPayment xlx Report 
	***********************/
	public function actionSupplierconsigPayment()
	{
		$model = $dataXls =  $modelwidraw = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';
		
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}		
		else $model = Yii::app()->session['reportModelPrevious'];	

		if(isset(Yii::app()->session['modelwidraw']) && !empty(Yii::app()->session['modelwidraw']))
		{
			  $modelwidraw = Yii::app()->session['modelwidraw'];
			  unset(Yii::app()->session['modelwidraw']);
			  Yii::app()->session['reportModelPreviousSecound'] = $modelwidraw;
		}else $modelwidraw = Yii::app()->session['reportModelPreviousSecound'];
		
		
		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('D1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('D2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Suppliers Consignment Payment Summary');  
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date from '.$startDate.' to  '.$endDate );
  
		//Sl. No 	IV No. 	Item Code 	Description 	Quantity 	Cost Price 	Adjust Quantity 	Adjust Price 	Date
		//Sl. No  	
		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Sales Date');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Received');        

 //		 			
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Payment Date ');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','PGRN No. ');        
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Adjust Quantity');        
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Payment');        
        $objPHPExcel->getActiveSheet()->setCellValue('I6','Remarks');        
        $i = 7;
		
        if(!empty($model)){
        	$srl = $totalReceipt = 0;
	        foreach($model as $data)   
	        {	
	        $srl++;		
	        $totalReceipt+=$data['totalAmount'];

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$startDate.'  '.$endDate);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['totalAmount']);
			$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total Received :');			
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalReceipt);			
	         
	    }
	    $j=7;
		if(!empty($modelwidraw)){
        	$totalPayment = 0;
	        foreach($modelwidraw as $key=>$data)
	        {
            if(!empty($data->amount)){
                $totalPayment+=$data->amount;
            }
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$j,$data->crAt);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$j,$data->pgrnNo);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$data->amount);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$j,($data->bankId)>0?'Bank':'Cash');
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$j,$data->remarks);
			$j++;			
	        }	
	        $objPHPExcel->getActiveSheet()->setCellValue('F'.$j,'Total Payment :');          
	        $objPHPExcel->getActiveSheet()->setCellValue('G'.$j,$totalPayment);         
	    }
        $balence = 0;
	    if(!empty($totalReceipt) || !empty($totalPayment)){
            $balence=round($totalReceipt-$totalPayment,2);
        }

		$k=$j+1;
	    $objPHPExcel->getActiveSheet()->setCellValue('A'.$k,'BALANCE :');          
	    $objPHPExcel->getActiveSheet()->setCellValue('B'.$k,$balence);   
		
        $xlxFile = 'SuppliersConsignmentPaymentSummary';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
     unset(Yii::app()->session['supplierConsingPayment']);		
	}


	/****************
	* supplier consig Payment xlx Report
	***********************/
	public function actionTempWorkerList()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';		
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('E1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('E2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('E3','Temporary Worker Items Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('E4','Date from '.$startDate.' to  '.$endDate );
  
		// Sl. No 	IV No. 	Item Code 	Description 	Quantity 	Cost Price 	Adjust Quantity 	Adjust Price 	Date
		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
     	$objPHPExcel->getActiveSheet()->setCellValue('B6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','TW Invoice No.');
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Item Name');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Receive Quantity');
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Cost Price');
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Date');
        $i = 7;
		
        if(!empty($model)){
        	$totalQty=$totalPrice= $rTotalQty = 0;
	        foreach($model as $key=>$data)
	        {
            if(!empty($data->qty)){
                $totalQty+=$data->qty;
                $rTotalQty+=$data->rqty;
                $totalPrice+=$data->qty*$data->costPrice;
            }
            $username = '';
            $twNo = '';
                $itemName = '';
                $qty = '';
                $rqty = '';
                $costPrice = '';
                $twDate = '';
            if (!empty($data->wk)){
                $username = $data->wk->username;
            }
            if (!empty($data->twNo)){
                $twNo = $data->twNo;
            } if (!empty($data->item)){
                $itemName = $data->item->itemName;
            }if (!empty($data->qty)){
                $qty = $data->qty;
            }if (!empty($data->rqty)){
                $rqty = $data->rqty;
            }if (!empty($data->costPrice)){
                $costPrice = $data->costPrice;
            }if (!empty($data->twDate)){
                $twDate = $data->twDate;
            }
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$username);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$twNo);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$itemName);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$qty);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$rqty);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$qty*$costPrice);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,$twDate);
			$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total Receipt : ');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$totalQty);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$rTotalQty);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$totalPrice);
	         
	    } 
		
		
        $xlxFile = 'TemporaryWorkerInvoice';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
     unset(Yii::app()->session['supplierConsingPayment']);		
	}
	
	
	/****************
	*tempWorkerPayment xlx Report 
	***********************/
	public function actionTempWorkerPayment()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';		
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Temporary Worker Payment');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date  '.$startDate.' to '.$endDate);
  
		//Sl. No 	IV No. 	Item Code 	Description 	Quantity 	Cost Price 	Adjust Quantity 	Adjust Price 	Date
		//Sl. No  
		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
     	$objPHPExcel->getActiveSheet()->setCellValue('B6','Worker');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Invoice No.');            
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
		$objPHPExcel->getActiveSheet()->setCellValue('E6','Payment');        
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Created');    
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Remarks');    
             
        $i = 7;
		
        if(!empty($model)){
        	$srl=0; $totalAmount= 0;
	        foreach($model as $key=>$data)
	        {	
	        $totalAmount+=$data->amount; 

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->wk->username);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->twNo);			
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->amount);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,($data->bankId)>0?'Bank':'Cash');
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->crAt);	
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,$data->remarks);	
			$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total: ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$totalAmount);	
	         
	    } 		
		
        $xlxFile = 'TemporaryWorkerPaymentInvoice';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
     unset(Yii::app()->session['supplierConsingPayment']);		
	}
	/****************
	*tempWorkerPayment xlx Report 
	***********************/
	
	/****************
	* tempWorkerBalanceSummary xlx Report 
	***********************/
	public function actionTempWorkerBalanceSummary()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName='';
		$branchAddress='';		
			
		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		
		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Temporary Worker Balance Summary Report');     
  
		//Sl. No 	IV No. 	Item Code 	Description 	Quantity 	Cost Price 	Adjust Quantity 	Adjust Price 	Date
		//Sl. No  
		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
     	$objPHPExcel->getActiveSheet()->setCellValue('B6','Worker');               
		$objPHPExcel->getActiveSheet()->setCellValue('C6','Balance');       
		
		$i = 7;
		
        if(!empty($model)){
        	$srl=0; $totalPrice= 0;
	        foreach($model as $key=>$data)
	        {	
				$srl++;
				$balance =TemporaryWorker::getWorkerBalance($data->id);
				$totalPrice+=$balance;
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$key+1);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->username);						
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$balance);
				$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total: ');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$totalPrice);	
	         
	    } 		
		
        $xlxFile = 'TemporaryWorkerBalanceSummaryReportInvoice';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
     unset(Yii::app()->session['supplierConsingPayment']);		
	
	}
	
	/****************
	* tempWorkerBalanceSummary xlx Report 
	***********************/
	
	


	/**
	*bankDepositSummaryReport*
	*/
	public function actionBankDepositSummaryReport()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Deposit Summary Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','A/C No.');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');  
        $i=7;

        if(!empty($model)){
        	$srl = 0;
			$totalPrice = 0;
	        foreach($model as $key=>$data)
	        {	
	        $srl++; 
			$totalPrice+=$data['tAmount'];

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['accountNo']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['tAmount']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data['depositDate']);
			
			$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalPrice,2));		
		} 		
		
        $xlxFile = 'BankDepositSummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	/**
	*bankDepositSummaryReport*
	*/

	/**
	*bankDepositDetailsReport*
	*/
	public function actionBankDepositDetailsReport()
	{

		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Deposit Details Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','A/C No.');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');  
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Remarks');  
        $i=7;

        if(!empty($model)){
        	$srl = 0;
			$totalPrice = 0;
	        foreach($model as $key=>$data)
	        {	
	        $srl++; 
			$totalPrice+=$data->amount;

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->bank->name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->bank->accountNo);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->amount);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->depositDate);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->comments);
			
			$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalPrice,2));		
		} 		
		
        $xlxFile = 'BankDepositDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	/**
	*bankDepositDetailsReport*
	*/
    /**
	*bankWithdrawSummaryReport*
	*/
	public function actionBankWithdrawSummaryReport()
	{

		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Withdraw Summary Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','A/C No.');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');  
  
        $i=7;

        if(!empty($model)){
        	$srl = 0;
			$totalPrice = 0;
	        foreach($model as $key=>$data)
	        {	
	        $srl++; 
            $totalPrice+=$data['tAmount'];

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data['name']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data['accountNo']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data['tAmount']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data['withdrawDate']);
		    $i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalPrice,2));		
		} 		
		
        $xlxFile = 'BankWithdrawSummaryReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	/**
	*bankWithdrawSummaryReport*
	*/

	/**
	*bankWithdrawDetailsReport*
	*/

	public function actionBankWithdrawDetailsReport()
	{

		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Withdraw Details Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','A/C No.');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');  
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Remarks');  
  
        $i=7;

        if(!empty($model)){
        	$srl = 0;
			$totalPrice = 0;
	        foreach($model as $key=>$data)
	        {	
	        $srl++; 
            $totalPrice+=$data->amount;	

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->bank->name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->bank->accountNo);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$data->amount);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$data->withdrawDate);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$data->comments);
		    $i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalPrice,2));		
		} 		
		
        $xlxFile = 'BankWithdrawDetailsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	/**
	*bankWithdrawDetailsReport*
	*/
	/**
	*cashInHands*
	*/
	public function actionCashInHands()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';	
		$modelWithdraw='';
        $modelSuppWithdraw='';
        $modelWkWithdraw = '';
		$modelMiscExp=''; $balance='';			

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		 
		if(isset($_REQUEST['modelWithdraw']) && !empty($_REQUEST['modelWithdraw'])) $modelWithdraw = $_REQUEST['modelWithdraw'];
		if(isset($_REQUEST['modelSuppWithdraw']) && !empty($_REQUEST['modelSuppWithdraw'])) $modelSuppWithdraw = $_REQUEST['modelSuppWithdraw'];
        if(isset($_REQUEST['modelWkWithdraw']) && !empty($_REQUEST['modelWkWithdraw'])) $modelWkWithdraw = $_REQUEST['modelWkWithdraw'];
		if(isset($_REQUEST['modelMiscExp']) && !empty($_REQUEST['modelMiscExp'])) $modelMiscExp = $_REQUEST['modelMiscExp'];
		if(isset($_REQUEST['balance']) && !empty($_REQUEST['balance'])) $balance = $_REQUEST['balance'];


		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Cash in Hands Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Cash Withdraw');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Supplier Payment(Cash)');
        $objPHPExcel->getActiveSheet()->setCellValue('D6','Worker Payment(Cash)');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Misc. Expense(Cash)');
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Balance');
          
        $i=7;     

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,'1');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,!empty($modelWithdraw)?$modelWithdraw:0);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,!empty($modelSuppWithdraw)?$modelSuppWithdraw:0);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$i,!empty($modelWkWithdraw)?$modelWkWithdraw:0);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,!empty($modelMiscExp)?$modelMiscExp:0);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,$balance);


        $xlxFile = 'CashinHandsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	/**
	/**
	*bank Current Balance*
	*/
	public function actionBankCurrentBalance()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];


		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Bank Current Balance Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Bank');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','A/C No.');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Total Deposit');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Total Withdraw');  
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Supplier Payment');  
        $objPHPExcel->getActiveSheet()->setCellValue('G6','Misc. Expense');  
        $objPHPExcel->getActiveSheet()->setCellValue('H6','Balance');  
  
        $i=7;

        if(!empty($model)){
        	$srl = $totalDeposit = $totalWithdraw = $totalSuppWithdraw = $totalMiscExp = $balance = $grandBalance = 0;
	        foreach($model as $key=>$data)
	        {	
	        $srl++; 
            // total deposit
            $sqlDeposit = "SELECT SUM(amount) AS totalAmount FROM pos_bank_deposit WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".BankDeposit::STATUS_ACTIVE;
	        $modelDeposit = Yii::app()->db->createCommand($sqlDeposit)->queryRow();
            $totalDeposit+=$modelDeposit['totalAmount'];

            // total widraw
            $sqlWithdraw = "SELECT SUM(amount) AS totalAmount FROM `pos_bank_withdraw` WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".BankWithdraw::STATUS_ACTIVE;
            $modelWithdraw = Yii::app()->db->createCommand($sqlWithdraw)->queryRow();
            $totalWithdraw+=$modelWithdraw['totalAmount'];

            // supplier withdraw
            $sqlSuppWithdraw = "SELECT SUM(amount) AS totalAmount FROM pos_supplier_widraw WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".SupplierWidraw::STATUS_ACTIVE;
	        $modelSuppWithdraw = Yii::app()->db->createCommand($sqlSuppWithdraw)->queryRow();
            $totalSuppWithdraw+=$modelSuppWithdraw['totalAmount'];

            // misc expense
            $sqlMiscExp = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND type=".Finance::STATUS_EXPENSE." AND status=".Finance::STATUS_ACTIVE;
	        $modelMiscExp = Yii::app()->db->createCommand($sqlMiscExp)->queryRow();
            $totalMiscExp+=$modelMiscExp['totalAmount'];
            
            $balance = $modelDeposit['totalAmount']-($modelWithdraw['totalAmount']+$modelSuppWithdraw['totalAmount']+$modelMiscExp['totalAmount']);
            $grandBalance+=round($balance,2);


			$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$data->name);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$data->accountNo);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,!empty($modelDeposit['totalAmount'])?$modelDeposit['totalAmount']:0);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,!empty($modelWithdraw['totalAmount'])?$modelWithdraw['totalAmount']:0);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,!empty($modelSuppWithdraw['totalAmount'])?$modelSuppWithdraw['totalAmount']:0);
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,!empty($modelMiscExp['totalAmount'])?$modelMiscExp['totalAmount']:0);
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round($balance,2));
		    $i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,'Total : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($totalDeposit,2));		
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($totalWithdraw,2));		
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,round($totalSuppWithdraw,2));		
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$i,round($totalMiscExp,2));		
			$objPHPExcel->getActiveSheet()->setCellValue('H'.$i,round($grandBalance,2));		
		} 		
		
        $xlxFile = 'BankCurrentBalanceReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	/**
	*bankCurrentBalance*
	*/

	/*
	* couponReport
	*/
	public function actionCouponReport()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Coupons Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Coupon No');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Coupon Amount');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Start Date');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','End Date');  
        $objPHPExcel->getActiveSheet()->setCellValue('F6','Status');  
  
        $i=7;

        if(!empty($model)){
        	$srl=0;
            $cuponAMount=0;
	        foreach($model as $key=>$value)
	        {	
		        $srl++; 
	            $cuponAMount+=$value->couponAmount;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$value->couponNo);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($value->couponAmount,2));
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$value->couponStartDate);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$value->couponEndDate);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$i,($value->status==1)?'Not Used':'Used');
			    $i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total Amount : ');
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,round($cuponAMount,2));  	
		} 		
		
        $xlxFile = 'CouponsReport';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	//coupon wise sales
	public function actionCouponWiseSales()
	{
		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';				

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];	

		$objPHPExcel = Yii::app()->excel;
        $objPHPExcel->getActiveSheet()->setCellValue('C1',$companyName);
        $objPHPExcel->getActiveSheet()->setCellValue('C2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('C3','Coupon Wise Sales Report');  
        $objPHPExcel->getActiveSheet()->setCellValue('C4','Date from '.$startDate.' to  '.$endDate );
  		
     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');    
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Invoice No.');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Coupon No');       
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Amount');        
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Date');     
  
        $i=7;

        if(!empty($model)){
        	$srl=0;
            $cuponAMount=0;
	        foreach($model as $key=>$value)
	        {	
		        $srl++; 
	            $cuponAMount+=$value->coupon->couponAmount;	
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$value->invNo);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$value->coupon->couponNo);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($value->coupon->couponAmount,2));
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$value->crAt);				
			    $i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,'Total Amount : ');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,round($cuponAMount,2));  	
		} 		
		
        $xlxFile = 'Coupon_Wise_Sales_Report';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';
        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}

	/*
	* couponReport
	*/
	/*	* Supplier Balanace Summary	*/
	public function actionSupplierBalanaceSummary()
	{

		$model = $dataXls = array();
		$startDate = $endDate = '';	
		$companyName=''; $branchAddress='';	
		$crType='';			

		if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
		if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
		if(isset($_REQUEST['companyName']) && !empty($_REQUEST['companyName']) ) $companyName = $_REQUEST['companyName'];
		if(isset($_REQUEST['branchAddress']) && !empty($_REQUEST['branchAddress']) ) $branchAddress = $_REQUEST['branchAddress'];
		if(isset($_REQUEST['crType']) && !empty($_REQUEST['crType']) ) $crType = $_REQUEST['crType'];
		
		if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
		{
			  $model = Yii::app()->session['reportModel'];			  
			  unset(Yii::app()->session['reportModel']);
			  Yii::app()->session['reportModelPrevious'] = $model;
		}
		
		else $model = Yii::app()->session['reportModelPrevious'];

		$objPHPExcel = Yii::app()->excel;

        $objPHPExcel->getActiveSheet()->setCellValue('B1',$companyName);

        $objPHPExcel->getActiveSheet()->setCellValue('B2',$branchAddress);
        $objPHPExcel->getActiveSheet()->setCellValue('B3','Suppliers Balance Summary');
        $objPHPExcel->getActiveSheet()->setCellValue('B4','Supplier Type :'.$crType.' Supplier');

     	$objPHPExcel->getActiveSheet()->setCellValue('A6','Sl. No');
        $objPHPExcel->getActiveSheet()->setCellValue('B6','Supplier ID');
        $objPHPExcel->getActiveSheet()->setCellValue('C6','Supplier Name');
		$objPHPExcel->getActiveSheet()->setCellValue('D6','Type');
        $objPHPExcel->getActiveSheet()->setCellValue('E6','Balance');

        $i=7;

        if(!empty($model)){
        	$srl=0;
            $totalBalance=0;
	        foreach($model as $key=>$value)
	        {	
		        $suppBalance = ($value->crType==Supplier::CREDIT_SUPP)?Supplier::getSupplierBalance($value->id):Supplier::getSupplierBalanceConsig($value->id);
                $totalBalance+=$suppBalance; 

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i,$srl);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$value->suppId);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$value->name);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$value->crType);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,$suppBalance);
				$i++;			
	        }   
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,'Total Amount : ');
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$i,round($totalBalance,2));
		} 		
		
        $xlxFile = 'SuppliersBalanceSummary';
        $xlxRoot = Yii::getPathOfAlias("webroot").'/media/exportXls/';

        $filenameXls = $xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx';
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($filenameXls);
        // download and delete xls
        if(file_exists($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/xlsx');
            header('Content-Disposition: attachment; filename='.$xlxFile.'.xlsx');
            readfile($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
            @unlink($xlxRoot.Yii::app()->session['orgId'].'_'.$xlxFile.'.xlsx');
        }
	}
	/*	* Supplier Balanace Summary	*/






	

}
