<?php
/*********************************************************
        -*- File: PurchaseOrderController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 2014.02.06
        -*- Position:  protected/controller
		-*- YII-*- version 1.1.13
/*********************************************************/

class PurchaseOrderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
    	$msg = $printId = '';
		$model=new PurchaseOrder;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['PurchaseOrder']))
		{		
			$model->attributes=$_POST['PurchaseOrder'];
            $model->status = PurchaseOrder::STATUS_ACTIVE;
            if($model->save()) 
			{
				// processing purchase order details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) :
						if($qty>0) :
							$modelPoDetails = new PoDetails;
							$modelPoDetails->poId = $model->id;
							$modelPoDetails->qty = $qty;
							$modelPoDetails->itemId = $_POST['proId'][$_POST['pk'][$key]];		
							$modelPoDetails->costPrice = $_POST['costPrice'][$_POST['pk'][$key]];
							$modelPoDetails->currentStock = $_POST['currentStock'][$_POST['pk'][$key]];		
							$modelPoDetails->soldQty = $_POST['soldQty'][$_POST['pk'][$key]];
							$modelPoDetails->save();
						endif;
					endforeach;	
				endif;
				$printId = $model->id;
				
				$msg = "Purchase Order Saved Successfully";
                $model=new PurchaseOrder;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('_form',array(
			'model'=>$model,
            'printId'=>$printId,
			'msg'=>$msg,
		));
	}

	/*
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
    	$msg = '';
		$model=new PurchaseOrder;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['PurchaseOrder']))
		{	
			$model = PurchaseOrder::model()->find('poNo=:poNo',array(':poNo'=>$_POST['PurchaseOrder']['poNo']));
			$poId = $model->id;
			$model->attributes=$_POST['PurchaseOrder'];
			
            if($model->save()) 
			{
				// processing po details
				if(isset($_POST['qty'])) :
					foreach($_POST['qty'] as $key=>$qty) :
						$sql = "update pos_po_details set qty=".$qty.", moAt='".date("Y-m-d H:i:s")."' where poId=".$poId." and itemId=".$_POST['proId'][$_POST['pk'][$key]]; 
						$command = Yii::app()->db->createCommand($sql);
						$command->execute();
					endforeach;	
				endif;
				
				$msg = "Purchase Order Updated Successfully";
                $model=new PurchaseOrder;
                //$this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('update',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/*
	 * Return a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionReturn()
	{
    	$msg = $printId = '';
		$return = 0;
		$model=new PoReturns;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

        /* auto pr starts
        $sql = "SELECT * FROM `pos_stock` WHERE stockDate LIKE '%2015-04-27%'";
        $model = Stock::model()->findAllBySql($sql);
        foreach($model as $key=>$data) :
            $modelPoDetails = new PoReturns;
            $modelPoDetails->prNo = 'PR20150427073610';
            $modelPoDetails->qty = $data->qty;
            $modelPoDetails->itemId = $data->itemId;
            $modelPoDetails->costPrice = $data->item->costPrice;
            //echo '<pre>'; print_r($modelPoDetails->attributes);
            $modelPoDetails->save();
        endforeach;
        exit();
        // auto pr starts ends */

		if(isset($_POST['PoReturns']))
		{		
			// processing purchase return details
			if(isset($_POST['qty'])) :
				foreach($_POST['qty'] as $key=>$qty) :
					if($qty>0) :
						$modelPoDetails = new PoReturns;
						$modelPoDetails->prNo = $_POST['poNo'];
						$modelPoDetails->qty = $qty;
						$modelPoDetails->itemId = $_POST['proId'][$_POST['pk'][$key]];		
						$modelPoDetails->costPrice = $_POST['costPrice'][$_POST['pk'][$key]];
						if($modelPoDetails->save()) $return = 1;
					endif;
				endforeach;	
			endif;
			
			// supplier withdraw processing
			if($return==1) :
				$printId = $_POST['poNo'];
				$modelSuppReturn = new SupplierWidraw;
				$modelSuppReturn->supId = $_POST['PoReturns']['supplierId'];
				$modelSuppReturn->pgrnNo = 'PR'.date("Ymdhis");;
				$modelSuppReturn->type = SupplierWidraw::PO_RETURNS;
				$modelSuppReturn->amount = $_POST['totalPrice'];		
				$modelSuppReturn->save();
							
				$msg = "Purchase Return Successful";
				$model=new PoReturns;
			endif;
			//$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('return',array(
			'model'=>$model,
			'printId'=>$printId,
            'msg'=>$msg,
		));
	}
	
	/*
	 * Cancel a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionCancel()
	{
    	$msg = '';
		$model=new PurchaseOrder;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['PurchaseOrder']))
		{	
			$model = PurchaseOrder::model()->find('poNo=:poNo',array(':poNo'=>$_POST['PurchaseOrder']['poNo']));
			$poId = $model->id;
			
			$grnNoModel = GoodReceiveNote::model()->find('poId=:poId',array(':poId'=>$poId));
			if(empty($grnNoModel)) 
			{
				// processing po details delete
				$sql = "delete from pos_po_details where poId=".$poId; 
				$command = Yii::app()->db->createCommand($sql);
				$command->execute();
				
				// delete po
				if($model->delete())
				{	
					$msg = "Purchase Order Cancel Successfully";
					$model=new PurchaseOrder;
				}
			}
			else $msg = 'GRN Alreday done for this PO !';
		}

		$this->render('cancel',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	/*
	 * print particular po.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionPrintPo()
	{					
		$msg = $printId = '';
		$model=new PurchaseOrder;
		if(isset($_POST['PurchaseOrder']))
		{
			$poModel = PurchaseOrder::model()->find('poNo=:poNo',array(':poNo'=>$_POST['PurchaseOrder']['poNo']));
			if(!empty($poModel)) $printId = $poModel->id;
		}
		
		$this->render('printPo',array(
			'model'=>$model,
			'printId'=>$printId,
            'msg'=>$msg,
		));
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PurchaseOrder');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
    	if (isset($_GET['pageSize'])) {
			//
			// pageSize will be set on user's state
			Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
			//
			// unset the parameter as it
			// would interfere with pager
			// and repetitive page size change
			unset($_GET['pageSize']);
		}
        
		$model=new PurchaseOrder('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PurchaseOrder']))
			$model->attributes=$_GET['PurchaseOrder'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PurchaseOrder the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PurchaseOrder::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PurchaseOrder $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='purchase-order-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
