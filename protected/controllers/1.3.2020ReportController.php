<?php
/*********************************************************
        -*- File: ReportController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/config
		-*-  YII-*- version 1.1.16
/*********************************************************/
class ReportController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	// supplier lists with date range
	public function actionSupplierList() 
	{
		$msg = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT * FROM pos_supplier WHERE crAt BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and status=".Supplier::STATUS_ACTIVE." ORDER BY crAt DESC";
				$model = Supplier::model()->findAllBySql($sql);
			}		
		}
		$this->render('supplierList',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}

	// supplier credit payments
	public function actionSuppliercreditPayment() 
	{
		$msg = $startDate = $endDate = $supplier = '';
		$model = $modelwidraw = array();
		
		if(isset($_POST['supId']) && !empty($_POST['supId']))
		{
			$supplier = $_POST['supId'];
			$filter = "WHERE supId=".$supplier." AND branchId=".Yii::app()->session['branchId'];
			
			// after post data
			if(isset($_POST['startDate']) && isset($_POST['endDate']))
			{
				if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
				else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
				else
				{
					$filter.=" AND crAt BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'";
					$startDate = $_POST['startDate'];
					$endDate = $_POST['endDate'];
				}		
			}
			$sql = "SELECT * FROM pos_supplier_deposit ".$filter." AND status=".SupplierDeposit::STATUS_ACTIVE." ORDER BY crAt DESC";
			$model = SupplierDeposit::model()->findAllBySql($sql);
			
			$sqlw = "SELECT * FROM pos_supplier_widraw ".$filter." AND status=".SupplierWidraw::STATUS_ACTIVE." ORDER BY crAt DESC";
			$modelwidraw = SupplierWidraw::model()->findAllBySql($sqlw);
		}
		else $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select Supplier !</p></div>';
		
		$this->render('suppliercreditPayment',array(
			'model'=>$model,'modelwidraw'=>$modelwidraw,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'supplier'=>$supplier
		));
	}
	
	// supplier consig payments
	public function actionSupplierconsigPayment() 
	{
		$msg = $startDate = $endDate = $supplier = '';
		$model = $modelwidraw = array();
		
		if(isset($_POST['supId']) && !empty($_POST['supId']))
		{
			$supplier = $_POST['supId'];
			$filter = "WHERE supId=".$supplier." AND branchId=".Yii::app()->session['branchId'];
			
			// after post data
			if(isset($_POST['startDate']) && isset($_POST['endDate']))
			{
				if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
				else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
				else
				{
					$filter.=" AND crAt BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'";
					$startDate = $_POST['startDate'];
					$endDate = $_POST['endDate'];
				}		
			}
			
			/*$sql = "SELECT i.invNo as inv,i.orderDate as date,(s.qty*t.costPrice) AS amount FROM pos_sales_invoice i, pos_sales_details s, pos_items t WHERE i.id=s.salesId AND s.itemId=t.id AND i.branchId=".Yii::app()->session['branchId']."
				    AND s.itemId IN(SELECT id FROM pos_items WHERE supplierId=".$supplier.") ORDER BY s.salesDate DESC";*/
					
			$sql = "SELECT sum(s.qty*s.costPrice) AS totalAmount,s.salesDate as date FROM pos_sales_invoice i, pos_sales_details s, pos_items t WHERE i.id=s.salesId AND s.itemId=t.id AND i.branchId=".Yii::app()->session['branchId']." AND s.itemId IN(SELECT id FROM pos_items WHERE supplierId=".$supplier.") AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' GROUP BY s.itemId ORDER BY s.salesDate DESC";
			$model = Yii::app()->db->createCommand($sql)->queryAll();
			
			$sqlw = "SELECT * FROM pos_supplier_widraw ".$filter." AND status=".Supplier::STATUS_ACTIVE." ORDER BY crAt DESC";
			$modelwidraw = SupplierWidraw::model()->findAllBySql($sqlw);
		}
		else $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select Supplier !</p></div>';
		
		$this->render('supplierconsigPayment',array(
			'model'=>$model,'modelwidraw'=>$modelwidraw,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'supplier'=>$supplier
		));
	}

    // supplier balance summary report
    public function actionSupplierBalanaceSummary()
    {
        $msg =  $filter = $crType = '';
        $model= array();

        if(isset($_POST['crType']) && !empty($_POST['crType']))
        {
            $crType = $_POST['crType'];
            $sql = "SELECT * FROM pos_supplier WHERE crType='".$crType."' AND status=".Supplier::STATUS_ACTIVE." ORDER BY crAt DESC";
        }
        else $sql = "SELECT * FROM pos_supplier WHERE status=".Supplier::STATUS_ACTIVE." ORDER BY crAt DESC";
        $model= Supplier::model()->findAllBySql($sql);

        $this->render('supplierBalanaceSummary',array(
            'model'=>$model, 'msg'=>$msg,'crType'=>$crType
        ));
    }
	
	// item lists with date range
	public function actionItemList() 
	{
		$msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{                                   
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
                
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND isParent='.$isParent;
                }
				$sql = "SELECT * FROM pos_items WHERE crAt BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND status=".Items::STATUS_ACTIVE." ORDER BY crAt DESC";
				$model = Items::model()->findAllBySql($sql);
			}		
		}
		$this->render('itemList',array(
			'model'=>$model,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// item damage lists with date range
	public function actionDamageList() 
	{
		$msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$dept = $deptName = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
                
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                }
                
				// dept wise
				if(!empty($_POST['deptId'])) 
				{
					$dept = $_POST['deptId'];
					if($_POST['deptId']=='all') // ALL departments
					{
						$sql = "SELECT w.* FROM pos_damage_wastage w,pos_items i WHERE w.itemId=i.id AND w.type=".DamageWastage::TYPE_DAMAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";	
					}
					else
					{
						$sql = "SELECT w.* FROM pos_damage_wastage w, pos_items i, pos_category c, pos_subdepartment d 
								WHERE w.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$dept." AND w.type=".DamageWastage::TYPE_DAMAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";
					}
					
					// finding dept,subdept,category
					if($_POST['deptId']=='all') $deptName = 'All';
					else 
                    {
						$deptModel = Department::model()->findByPk($_POST['deptId']);
						if(!empty($deptModel)) $deptName = $deptModel->name;
					}
				}
				else
				{
					$sql = "SELECT w.* FROM pos_damage_wastage w, pos_items i WHERE w.itemId=i.id AND w.type=".DamageWastage::TYPE_DAMAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";	
				}
				$model = DamageWastage::model()->findAllBySql($sql);
			}		
		}
		$this->render('damageList',array(
			'model'=>$model,
            'msg'=>$msg,
			'dept'=>$dept,'deptName'=>$deptName,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// item wastage lists with date range
	public function actionWastageList() 
	{
        $msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$dept = $deptName = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
                
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                }
                
				// dept wise
				if(!empty($_POST['deptId'])) 
				{
					$dept = $_POST['deptId'];
					if($_POST['deptId']=='all') // ALL departments
					{
						$sql = "SELECT w.* FROM pos_damage_wastage w,pos_items i WHERE w.itemId=i.id AND w.type=".DamageWastage::TYPE_WASTAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";	
					}
					else
					{
						$sql = "SELECT w.* FROM pos_damage_wastage w, pos_items i, pos_category c, pos_subdepartment d 
								WHERE w.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$dept." AND w.type=".DamageWastage::TYPE_WASTAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";
					}
					
					// finding dept,subdept,category
					if($_POST['deptId']=='all') $deptName = 'All';
					else 
                    {
						$deptModel = Department::model()->findByPk($_POST['deptId']);
						if(!empty($deptModel)) $deptName = $deptModel->name;
					}
				}
				else
				{
					$sql = "SELECT w.* FROM pos_damage_wastage w, pos_items i WHERE w.itemId=i.id AND w.type=".DamageWastage::TYPE_WASTAGE." AND w.branchId=".Yii::app()->session['branchId']." AND w.damDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND w.status=".DamageWastage::STATUS_ACTIVE." ORDER BY w.damDate DESC";	
				}
				$model = DamageWastage::model()->findAllBySql($sql);
			}		
		}
		$this->render('wastageList',array(
			'model'=>$model,
            'msg'=>$msg,
			'dept'=>$dept,'deptName'=>$deptName,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}

	// item wastage lists with date range
	public function actionTempWorkerList() 
	{
		$msg = $startDate = $endDate = '';
		$dept = $deptName = $wkId = $twNo = '';
		$model=array();  

		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{

			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				// dept wise

				if(!empty($_POST['wkId'])) 
				{
					$wkId=$_POST['wkId'];
					
					if(isset($_POST['twNo']) && !empty($_POST['twNo']) )
					{
						$twNo = $_POST['twNo'];						
						$sql = "SELECT * FROM pos_temporary_worker WHERE twNo='".$_POST['twNo']."' AND wkId=".$_POST['wkId']." AND branchId=".Yii::app()->session['branchId']." AND twDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".TemporaryWorker::STATUS_ACTIVE." ORDER BY twDate DESC";
					}else{
						$sql = "SELECT * FROM pos_temporary_worker WHERE wkId=".$_POST['wkId']." AND branchId=".Yii::app()->session['branchId']." AND twDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".TemporaryWorker::STATUS_ACTIVE." ORDER BY twDate DESC";
					}
				}
				else if(empty($_POST['wkId']) && !empty($_POST['twNo']))
				{
					$twNo = $_POST['twNo'];
					$sql = "SELECT * FROM pos_temporary_worker WHERE twNo='".$_POST['twNo']."' AND branchId=".Yii::app()->session['branchId']." AND twDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".TemporaryWorker::STATUS_ACTIVE." ORDER BY twDate DESC";

				} 
				else {
					$sql = "SELECT * FROM pos_temporary_worker WHERE branchId=".Yii::app()->session['branchId']." AND twDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".TemporaryWorker::STATUS_ACTIVE." ORDER BY twDate DESC";
				}
			
			   $model = TemporaryWorker::model()->findAllBySql($sql);
			}		
		}

		$this->render('tempWorkerList',array(
			'model'=>$model,
            'msg'=>$msg,
            'twNo'=>$twNo,
			'wkId'=>$wkId,'deptName'=>$deptName,
			'startDate'=>$startDate,'endDate'=>$endDate
		));

	}
	
	// Temporary Worker Payment date wise
	public function actionTempWorkerPayment()
	{
		$msg = $startDate = $endDate=  '';
		$wkId = $twNo = $filter = '';
		$model=array(); 
				
		if(isset($_POST['wkId']) && isset($_POST['startDate']) )
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select Start Date!</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select End Date!</p></div>';
			else
			{
				$startDate=$_POST['startDate'];
				$endDate=$_POST['endDate'];
				// pos_temporary_worker_widraw				
				if(isset($_POST['wkId']) && !empty($_POST['wkId']) )
				{
					$wkId=$_POST['wkId'];
					$filter.='AND wkId="'.$_POST['wkId'].'"';
				}
				
				if(isset($_POST['twNo']) && !empty($_POST['twNo']) )
				{
					$twNo=$_POST['twNo'];
					$filter.=' AND twNo="'.$_POST['twNo'].'"';
				}
				//echo $_POST['startDate'];  // STR_TO_DATE(crAt, '%Y-%m-%d')
				$sql = "SELECT * FROM pos_temporary_worker_widraw WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(crAt, '%Y-%m-%d') BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' $filter AND status=".TemporaryWorkerWidraw::STATUS_ACTIVE." ORDER BY crAt DESC";
				$model = TemporaryWorkerWidraw::model()->findAllBySql($sql);			
			}
		}

		$this->render('tempWorkerPayment',array(
			'model'=>$model,
			'msg'=>$msg,
			'twNo'=>$twNo,'endDate'=>$endDate,
			'wkId'=>$wkId, 'startDate'=>$startDate
		));
	}
	
	// tempWorker Balance Summary
	public function actionTempWorkerBalanceSummary()
	{
		$msg = $startDate = $endDate = '';
		 $wkId = $twNo = $filter = '';
		$model=array();
		
		if(isset($_POST['wkId']) && !empty($_POST['wkId']))
		{
			$wkId = $_POST['wkId'];
			$sql = "SELECT * FROM pos_user WHERE branchId=".Yii::app()->session['branchId']." AND id=".$wkId."";
		}
		else $sql = "SELECT * FROM pos_user WHERE branchId=".Yii::app()->session['branchId']." AND id IN(SELECT userId FROM pos_roles WHERE userTypeId=".UserType::getTemporaryWorkerType().")";
		$model= User::model()->findAllBySql($sql);

		$this->render('tempWorkerBalanceSummary',array(
				'model'=>$model,
				'msg'=>$msg,
				'twNo'=>$twNo,
				'wkId'=>$wkId,
				'startDate'=>$startDate,'endDate'=>$endDate
		));
	}  

	// item price change with date range
	public function actionPriceChangeReport() 
	{
		$msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
                
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                }
				$sql = "SELECT p.* FROM pos_items_price p,pos_items i WHERE p.itemId=i.id AND p.branchId=".Yii::app()->session['branchId']." AND p.changeDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND p.status=".ItemsPrice::STATUS_ACTIVE." ORDER BY p.changeDate DESC";
                $model = ItemsPrice::model()->findAllBySql($sql);
			}		
		}
		$this->render('priceChangeReport',array(
			'model'=>$model,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// purchase summary with date range
	public function actionPurchaseSummaryList() 
	{
		$msg = $supplierId = $suppName = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if(!empty($_POST['supplierId']))
				{
					$supplierId = $_POST['supplierId'];
					$sql = "SELECT * FROM pos_purchase_order WHERE branchId=".Yii::app()->session['branchId']." AND supplierId=".$_POST['supplierId']." AND poDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and status<>".PurchaseOrder::STATUS_INACTIVE." ORDER BY poDate DESC";
					
					$suppModel = Supplier::model()->findByPk($_POST['supplierId']);
					if(!empty($suppModel)) $suppName = $suppModel->name;	
				}
				else
				{
					$sql = "SELECT * FROM pos_purchase_order WHERE branchId=".Yii::app()->session['branchId']." AND poDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and status=".PurchaseOrder::STATUS_ACTIVE." ORDER BY poDate DESC";
				}
				$model = PurchaseOrder::model()->findAllBySql($sql);
			}		
		}
		$this->render('purchaseSummaryList',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'supplierId'=>$supplierId,'suppName'=>$suppName
		));
	}
	
	// purchase order details with date range
	public function actionPurchaseDetailsList() 
	{
		$msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
                
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND isParent='.$isParent;
                }
                
				$sql = "SELECT d.* FROM pos_purchase_order p, pos_po_details d,pos_items i WHERE d.poId=p.id AND d.itemId=i.id AND p.branchId=".Yii::app()->SESSION['branchId']." AND d.crAt BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND d.status=".PoDetails::STATUS_ACTIVE." ORDER BY d.crAt DESC";
				$model = PoDetails::model()->findAllBySql($sql);
			}		
		}
		$this->render('purchaseDetailsList',array(
			'model'=>$model,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// purchase return summary with date range
	public function actionPurchaseReturnSummaryList() 
	{
		$msg = $supplierId = $suppName = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if(!empty($_POST['supplierId']))
				{
					$supplierId = $_POST['supplierId'];
					$sql = "SELECT r.prNo,r.prDate,SUM(r.qty) AS totalQty,SUM(r.qty*r.costPrice) AS totalPrice FROM pos_items i, pos_po_returns r WHERE r.itemId=i.id AND i.supplierId=".$supplierId."
					        AND r.branchId=".Yii::app()->session['branchId']." AND r.prDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and r.status=".PoReturns::STATUS_ACTIVE." 
							GROUP BY prNo ORDER BY prDate DESC";
					
					$suppModel = Supplier::model()->findByPk($_POST['supplierId']);
					if(!empty($suppModel)) $suppName = $suppModel->name;	
				}
				else
				{
					$sql = "SELECT r.prNo,r.prDate,SUM(r.qty) AS totalQty,SUM(r.qty*r.costPrice) AS totalPrice FROM pos_po_returns r WHERE r.branchId=".Yii::app()->session['branchId']." 
						    AND r.prDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and r.status=".PoReturns::STATUS_ACTIVE." GROUP BY prNo ORDER BY prDate DESC";
				}
				$model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}
		$this->render('purchaseReturnSummaryList',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'supplierId'=>$supplierId,'suppName'=>$suppName
		));
	}
	
	// purchase return details with date range
	public function actionPurchaseReturnDetailsList() 
	{
		$msg = $prNo = '';
		$model=array();
		
		// after post data
		if(isset($_POST['yt0']))
		{
			if(isset($_POST['prNo']) && !empty($_POST['prNo']))
			{
				$prNo = $_POST['prNo'];
				$sql = "SELECT * FROM pos_po_returns r WHERE r.branchId=".Yii::app()->session['branchId']." AND r.prNo='".$prNo."' and r.status=".PoReturns::STATUS_ACTIVE." ORDER BY prDate DESC";
				$model = PoReturns::model()->findAllBySql($sql);	
			}
			else $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select PR No. !</p></div>';
		}
		
		$this->render('purchaseReturnDetailsList',array(
			'model'=>$model,
            'msg'=>$msg,
			'prNo'=>$prNo
		));
	}
	
	// grn summary with date range STATUS_APPROVED
	public function actionGrnSummaryList() 
	{
		$msg = $supplierId = $suppName = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if(!empty($_POST['supplierId']))
				{
					$supplierId = $_POST['supplierId'];
					$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND n.supplierId=".$_POST['supplierId']." AND p.branchId=".Yii::app()->session['branchId']." AND n.grnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND n.status=".GoodReceiveNote::STATUS_APPROVED." ORDER BY n.grnDate DESC";	
					$suppModel = Supplier::model()->findByPk($_POST['supplierId']);
				if(!empty($suppModel)) $suppName = $suppModel->name;
				}
				else
				{
					$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.grnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND n.status=".GoodReceiveNote::STATUS_APPROVED." ORDER BY n.grnDate DESC";
				}
				$model = GoodReceiveNote::model()->findAllBySql($sql);
			}		
		}
		$this->render('grnSummaryList',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'supplierId'=>$supplierId,'suppName'=>$suppName
		));
	}
	
	// purchase order details/stock with date range 
	public function actionGrnDetailsList() 
	{
        $msg = $startDate = $endDate = $isEcommerce = $isParent = $filter = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];

                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                }
                
				$sql = "SELECT * FROM pos_stock s,pos_items i WHERE s.itemId=i.id AND s.stockDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ". $filter." AND s.status=".Stock::STATUS_APPROVED." ORDER BY stockDate DESC";
				
				$model = Stock::model()->findAllBySql($sql);
			}		
		}
		$this->render('grnDetailsList',array(
			'model'=>$model,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// grn discount report STATUS_APPROVED
	public function actionGrnDiscountReport() 
	{
		$msg = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				
				$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.grnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND n.status=".GoodReceiveNote::STATUS_APPROVED." ORDER BY n.grnDate DESC";
				$model = GoodReceiveNote::model()->findAllBySql($sql);
			}		
		}
		$this->render('grnDiscountReport',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// inventory initialized report
	public function actionInvInitialized() 
	{
		$msg = '';
		$model=InvSettings::model()->findAll('status=:status',array(':status'=>InvSettings::STATUS_ACTIVE));
		
		$this->render('invInitialized',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}
	
	// inventory summary report by date time
	public function actionInvSummaryReport() 
	{
		$msg = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				
				$sql = "SELECT invNo,invDate,SUM(s.totalQty) AS tQty,SUM(s.totalWeight) AS tWeight,SUM(s.totalAdjustQty) AS tAdQty,SUM(s.totalPrice) 
				        AS tPrice,SUM(s.totalAdjPrice) AS tAdPrice FROM pos_inv_summary s WHERE s.branchId=".Yii::app()->session['branchId']." 
						AND s.invDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND s.status=".InvSummary::STATUS_ACTIVE."
						GROUP BY s.invNo ORDER BY s.invDate DESC";
				$model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}
		$this->render('invSummary',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// inventory details report by date time
	public function actionInvDetailsReport() 
	{
		$sql = $msg = $startDate = $endDate = $invNo = $itemCode = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['invNo']) && empty($_POST['itemCode'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please input invoice NO. OR item Code !</p></div>';
			else
			{
				// category wise
				if(!empty($_POST['invNo'])) 
				{
					$invNo = $_POST['invNo'];
					$sql = "SELECT * FROM pos_inventory WHERE invNo='".$_POST['invNo']."' AND branchId=".Yii::app()->session['branchId']." 
							AND status=".Inventory::STATUS_CLOSE." ORDER BY invDate DESC";
				}
				// item wise wise
				else if(!empty($_POST['itemCode'])) 
				{
					$itemCode = $_POST['itemCode'];
					if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
					else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';		
					else
					{
						$startDate = $_POST['startDate'];
						$endDate = $_POST['endDate'];
						$itemModel = Items::model()->find('itemCode=:itemCode',array(':itemCode'=>$_POST['itemCode']));
						if(!empty($itemModel))
						{
							$sql = "SELECT * FROM pos_inventory WHERE itemId='".$itemModel->id."' AND branchId=".Yii::app()->session['branchId']." 
									AND invDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".Inventory::STATUS_CLOSE."
									ORDER BY invDate DESC";
						}
					}
				}
				$model = Inventory::model()->findAllBySql($sql);
			}
		}
		
		$this->render('invDetails',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'invNo'=>$invNo,'itemCode'=>$itemCode
		));
	}
	
	// item wise stock report 
	public function actionItemWiseStock() 
	{
		$msg = $dept = $subdept = $cat = $supplierId = $filter = $isEcommerce = $isParent= '';
		$deptName = $subdeptName = $catName = $itemCode = $suppName = '';
		$endDate = $itemCode = '';
        $modelStock = $modelTransfer = array();
		
		// after post data
		if(isset($_POST['endDate']))
		{
			if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else if(empty($_POST['deptId']) && empty($_POST['subdeptId']) && empty($_POST['catId']) && empty($_POST['supplierId']) && empty($_POST['itemCode'])) 
				$msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one department/sub department/category/supplier/itemCode !</p></div>';
			else
			{
				$endDate = $_POST['endDate'];
				$cat = $_POST['catId'];
				$subdept = $_POST['subdeptId'];
				$dept = $_POST['deptId'];
				$supplierId = $_POST['supplierId'];
				$itemCode = $_POST['itemCode'];
				
				if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                } 
				
				// category wise
				if(!empty($_POST['catId'])) 
				{
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.catId IN(SELECT id FROM pos_category WHERE id=".$cat.")
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.catId IN(SELECT id FROM pos_category WHERE id=".$cat.") AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
				}
				// sub dept wise
				else if(!empty($_POST['subdeptId'])) 
				{
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId IN(SELECT id FROM pos_subdepartment WHERE id=".$subdept.")
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s,pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId IN(SELECT id FROM pos_subdepartment WHERE id=".$subdept.") AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
				}
				// dept wise
				else if(!empty($_POST['deptId'])) 
				{
					if($_POST['deptId']=='all') // all departments
					{
                        $sqlStock = "SELECT s.* FROM pos_stock s,pos_items i WHERE s.itemId=i.id AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
						             AND s.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                        WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                        AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                        GROUP BY s.itemId ORDER BY s.stockDate DESC";
					}
					else
					{
                        $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id
                                     AND d.deptId=".$dept." AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                     AND s.branchId=".Yii::app()->session['branchId']." ".$filter."  AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s,pos_category c,pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id
                                        AND d.deptId=".$dept." AND s.itemId NOT IN(SELECT itemId FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                        AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId) AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter."
                                        AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
					}
				}
				// supplierId wise
				else if(!empty($_POST['supplierId'])) 
				{
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.supplierId=".$supplierId."
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.supplierId=".$supplierId." AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
				}
				// itemCode wise
				else if(!empty($_POST['itemCode'])) 
				{
					$itemCode = $_POST['itemCode'];
					$sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
					             AND s.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                    AND s.itemId NOT IN (SELECT itemId FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and branchId=".Yii::app()->session['branchId']."
                                    AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='2015-07-15' AND s.status=".Stock::STATUS_APPROVED." GROUP BY itemId)
					                AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
				}
				else
                {
                    $sqlStock = $sqlTransfer = "";
                }
				$modelStock = Stock::model()->findAllBySql($sqlStock);
                $modelTransfer = Stock::model()->findAllBySql($sqlTransfer);
				
				// finding dept,subdept,category
				if($_POST['deptId']=='all') $deptName = 'All';
				else 
                {
					$deptModel = Department::model()->findByPk($_POST['deptId']);
					if(!empty($deptModel)) $deptName = $deptModel->name;
				}
				
				$subdeptModel = Subdepartment::model()->findByPk($_POST['subdeptId']);
				if(!empty($subdeptModel)) $subdeptName = $subdeptModel->name;
				
				$catModel = Category::model()->findByPk($_POST['catId']);
				if(!empty($catModel)) $catName = $catModel->name;
				
				$catModel = Category::model()->findByPk($cat);
				if(!empty($catModel)) $catName = $catModel->name;
				
				$suppModel = Supplier::model()->findByPk($_POST['supplierId']);
				if(!empty($suppModel)) $suppName = $suppModel->name;
			}		
		}
		$this->render('itemWiseStock',array(
			'modelStock'=>$modelStock,'modelTransfer'=>$modelTransfer,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
			'dept'=>$dept,'deptName'=>$deptName,
			'subdept'=>$subdept,'subdeptName'=>$subdeptName,
			'cat'=>$cat,'catName'=>$catName,'itemCode'=>$itemCode,
			'supplierId'=>$supplierId,'suppName'=>$suppName,'endDate'=>$endDate,
		));
	}

    // item wise stock report
    public function actionItemWiseGlobalStock()
    {
        $msg = $filter = $isEcommerce =  $endDate = $itemCode = $branchName = '';
        $branch = Yii::app()->session['branchId'];
        $branchModel = $modelStock = $modelTransfer = array();

        // after post data
        if(isset($_POST['endDate']))
        {
            if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else if(empty($_POST['branch']) && empty($_POST['itemCode']))
                $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one branch/itemCode !</p></div>';
            else
            {
                $endDate = $_POST['endDate'];
                $branch = $_POST['branch'];
                $itemCode = $_POST['itemCode'];
                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                // all branch
                if($branch=='all')
                {
                    $branchName = 'All Branch';
                    // itemCode wise
                    if(!empty($_POST['itemCode']))
                    {
                        $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
					                ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                    AND s.itemId NOT IN (SELECT itemId FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='2015-07-15'
                                    AND s.status=".Stock::STATUS_APPROVED." GROUP BY itemId) ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                    else
                    {
                        $sqlStock = "SELECT s.* FROM pos_stock s,pos_items i WHERE s.itemId=i.id AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='" . $_POST['endDate'] . "'
                                    " . $filter . " AND s.status=" . Stock::STATUS_APPROVED . " GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' ".$filter." AND s.status=" . StockTransfer::STATUS_ACTIVE . "
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                }
                else // individual branch
                {
                    $branchModel = Branch::model()->findByPk($branch);
                    if(!empty($branchModel)) $branchName = $branchModel->name;

                    // itemCode wise
                    if(!empty($_POST['itemCode']))
                    {
                        $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
					             AND s.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                    AND s.itemId NOT IN (SELECT itemId FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and branchId=".Yii::app()->session['branchId']."
                                    AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='2015-07-15' AND s.status=".Stock::STATUS_APPROVED." GROUP BY itemId)
					                AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                    else
                    {
                        // branch wise
                        $sqlStock = "SELECT s.* FROM pos_stock s,pos_items i WHERE s.itemId=i.id AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                     AND s.branchId=".$branch." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                WHERE branchId=".$branch." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".$branch." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                }
                $modelStock = Stock::model()->findAllBySql($sqlStock);
                $modelTransfer = Stock::model()->findAllBySql($sqlTransfer);
            }
        }
        $this->render('itemWiseGlobalStock',array(
            'modelStock'=>$modelStock,'modelTransfer'=>$modelTransfer,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'branch'=>$branch,'branchName'=>$branchName,'itemCode'=>$itemCode,'endDate'=>$endDate
        ));
    }

	// stock valuation summary report
	public function actionStockValuationSummary() 
	{
		$msg = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['endDate']))
		{
			if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$endDate = $_POST['endDate'];
				$model = Department::model()->findAll('status=:status',array(':status'=>Department::STATUS_ACTIVE));
			}		
		}
		$this->render('stockValuationSummary',array(
			'model'=>$model,
            'msg'=>$msg,
			'endDate'=>$endDate
		));
	}

    // stock transfer summary report
    public function actionStockTransferSummary()
    {
        $msg = $startDate = $endDate = $toBranchId = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else if(empty($_POST['toBranchId'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select store !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];
                $toBranchId = $_POST['toBranchId'];
                $sql = "SELECT transferNo,SUM(qty) AS totalQty,ROUND(SUM(qty*costPrice),2) AS totalCostPrice,stockDate FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']."
                        AND toBranchId=".$toBranchId." AND stockDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' GROUP BY transferNo ORDER BY stockDate DESC;";
                $model = Yii::app()->db->createCommand($sql)->queryAll();
            }
        }
        $this->render('stockTransferSummary',array(
            'model'=>$model, 'msg'=>$msg,'toBranchId'=>$toBranchId,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }

    // stock transfer details report
    public function actionStockTransferDetails()
    {
        $msg = $startDate = $endDate = $toBranchId = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else if(empty($_POST['toBranchId'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select store !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];
                $toBranchId = $_POST['toBranchId'];
                $sql = "SELECT * FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']."
                        AND toBranchId=".$toBranchId." AND stockDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ORDER BY stockDate DESC;";
                $model = StockTransfer::model()->findAllBySql($sql);
            }
        }
        $this->render('stockTransferDetails',array(
            'model'=>$model, 'msg'=>$msg,'toBranchId'=>$toBranchId,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }
	
	// hourly wise sales report 
	public function actionHourlySales() 
	{
		$msg = $startDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				/*$sql = "SELECT COUNT(id) AS totalBill,DATE_FORMAT(DATE_SUB(orderDate,INTERVAL 1 HOUR), '%h') AS prevhours, DATE_FORMAT(orderDate, '%r') AS hours, SUM(cashPaid) AS cashPaid,SUM(cardPaid) AS cardPaid,SUM(loyaltyPaid) AS loyaltyPaid FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$_POST['startDate']."%' AND status=".SalesDetails::STATUS_ACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d %H') ORDER BY orderDate ASC";
				$model = Yii::app()->db->createCommand($sql)->queryAll();*/
				
				$model = array('00'=>'12:00 - 01:00 AM',
							   '01'=>'01:00 - 02:00 AM',
							   '02'=>'02:00 - 03:00 AM',
							   '03'=>'03:00 - 04:00 AM',
							   '04'=>'04:00 - 05:00 AM',
							   '05'=>'05:00 - 06:00 AM',
							   '06'=>'06:00 - 07:00 AM',
							   '07'=>'07:00 - 08:00 AM',
							   '08'=>'08:00 - 09:00 AM',
							   '09'=>'09:00 - 10:00 AM',	
							   '10'=>'10:00 - 11:00 AM',
							   '11'=>'11:00 - 12:00 AM',
							   '12'=>'12:00 - 01:00 PM',
							   '13'=>'01:00 - 02:00 PM',
							   '14'=>'02:00 - 03:00 PM',
							   '15'=>'03:00 - 04:00 PM',
							   '16'=>'04:00 - 05:00 PM',
							   '17'=>'05:00 - 06:00 PM',
							   '18'=>'06:00 - 07:00 PM',
							   '19'=>'07:00 - 08:00 PM',
							   '20'=>'08:00 - 09:00 PM',
							   '21'=>'09:00 - 10:00 PM',
							   '22'=>'10:00 - 11:00 PM',			
							   '23'=>'11:00 - 12:00 PM',			
				);
			}		
		}
		$this->render('hourlySales',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate
		));
	}
	
	// current sales report (just last hour sales amount)
	public function actionCurrentSales() 
	{
		$msg = '';
        $sql = "SELECT id,orderDate,totalPrice,totalTax,cashPaid,totalChangeAmount,cardPaid,totalDiscount,spDiscount,instantDiscount,loyaltyPaid FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId'] . "
                AND orderDate>=DATE_SUB(NOW(),INTERVAL 10 HOUR) AND isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND status<>".SalesDetails::STATUS_INACTIVE . " ORDER BY orderDate DESC";
		$model = Yii::app()->db->createCommand($sql)->queryAll();
		$this->render('currentSales',array(
			'model'=>$model,
            'msg'=>$msg,
		));	
	}
	
	// department wise sales report
	public function actionDepartmentWiseSales() 
	{
		$msg = $startDate = $endDate = $filter = $salesType = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				if(isset($_POST['salesType']) && !empty($_POST['salesType']) )
				{
					$salesType = $_POST['salesType'];
				}
				$model = Department::model()->findAll('status=:status',array(':status'=>Department::STATUS_ACTIVE));
			}		
		}
		$this->render('departmentWiseSales',array(
			'model'=>$model,
            'msg'=>$msg, 'salesType'=>$salesType,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// category wise sales report 
	public function actionCategoryWiseSales() 
	{
		$msg = $dept = $subdept = $cat = '';
		$deptName = $subdeptName = $catName = '';
		$startDate = $endDate = $salesType = $filter = $filter2 = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else if(empty($_POST['deptId']) && empty($_POST['subdeptId']) && empty($_POST['catId'])) 
				$msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one department/sub department/category !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$cat = $_POST['catId'];
				$subdept = $_POST['subdeptId'];
				$dept = $_POST['deptId'];
				if(isset($_POST['salesType']) && !empty($_POST['salesType']) )
				{
				
					$salesType = $_POST['salesType'];
					$filter = ' AND v.isEcommerce='.$salesType;
                    $filter2 = ' AND i.isEcommerce='.$salesType;
				}
				
				// category wise
				if(!empty($_POST['catId'])) 
				{
					$sql = "SELECT s.*,SUM(s.qty) AS totalSalesQty,ROUND((SUM(s.qty*s.salesPrice)+SUM(s.vatPrice)),2) AS totalAmount FROM pos_items i, pos_sales_invoice v, pos_sales_details s WHERE v.id=s.salesId AND s.itemId=i.id
					        AND i.catId=".$cat." AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter."
							AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
				}
				// sub dept wise
				else if(!empty($_POST['subdeptId'])) 
				{
					$sql = "SELECT s.*,SUM(s.qty) AS totalSalesQty,ROUND((SUM(s.qty*s.salesPrice)+SUM(s.vatPrice)),2) AS totalAmount FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c WHERE v.id=s.salesId
					        AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=".$subdept." AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' 
							AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
				}
				// dept wise
				else if(!empty($_POST['deptId'])) 
				{
					if($_POST['deptId']=='all') // all departments
					{
						$sql = "SELECT s.*,SUM(s.qty) AS totalSalesQty,ROUND((SUM(s.qty*s.salesPrice)+SUM(s.vatPrice)),2) AS totalAmount FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'
						        AND i.branchId=".Yii::app()->session['branchId']." ".$filter2." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
					}
					else
					{
						$sql = "SELECT s.*,SUM(s.qty) AS totalSalesQty,ROUND((SUM(s.qty*s.salesPrice)+SUM(s.vatPrice)),2) AS totalAmount FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d
								WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$dept." AND s.salesDate BETWEEN '".$_POST['startDate']."' 
								AND '".$_POST['endDate']."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
					}
				}
				else $sql = "";
				$model = Yii::app()->db->createCommand($sql)->queryAll();
				
				// finding dept,subdept,category
				if($_POST['deptId']=='all') $deptName = 'All';
				else {
					$deptModel = Department::model()->findByPk($_POST['deptId']);
					if(!empty($deptModel)) $deptName = $deptModel->name;
				}
				
				$subdeptModel = Subdepartment::model()->findByPk($_POST['subdeptId']);
				if(!empty($subdeptModel)) $subdeptName = $subdeptModel->name;
				
				$catModel = Category::model()->findByPk($_POST['catId']);
				if(!empty($catModel)) $catName = $catModel->name;
				
				$catModel = Category::model()->findByPk($cat);
				if(!empty($catModel)) $catName = $catModel->name;
			}		
		}
		$this->render('categoryWiseSales',array(
			'model'=>$model,
            'msg'=>$msg,'salesType'=>$salesType,
			'dept'=>$dept,'deptName'=>$deptName,
			'subdept'=>$subdept,'subdeptName'=>$subdeptName,
			'cat'=>$cat,'catName'=>$catName,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// item wise sales movement report 
	public function actionSalesMovement() 
	{
		$msg = $dept = $subdept = $cat = $supplier = '';
		$deptName = $subdeptName = $catName = $suppName = '';
		$startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else if(empty($_POST['supId']) && empty($_POST['deptId']) && empty($_POST['subdeptId']) && empty($_POST['catId'])) 
				$msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one department/sub department/category/supplier !</p></div>';
			else 
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				
				$cat = $_POST['catId'];
				$subdept = $_POST['subdeptId'];
				$dept = $_POST['deptId'];
				
				$supplier = $_POST['supId'];

				// supplier wise
				if(isset($_POST['supId']) && !empty($_POST['supId']))
				{
					$sql = "SELECT s.*,SUM(s.qty) AS totalSales FROM pos_items i, pos_sales_invoice v, pos_sales_details s WHERE v.id=s.salesId AND s.itemId=i.id AND i.supplierId=".$supplier."
					        AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND v.branchId=".Yii::app()->session['branchId']." AND s.status<>".SalesDetails::STATUS_INACTIVE."
					        GROUP BY s.itemId ORDER BY s.salesDate DESC";
				}
					
				// category wise
				else if(!empty($_POST['catId'])) 
				{
					$sql = "SELECT s.*,SUM(s.qty) AS totalSales FROM pos_items i, pos_sales_invoice v, pos_sales_details s WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId IN(SELECT id FROM pos_category
                            WHERE id=".$cat.") AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND v.branchId=".Yii::app()->session['branchId']."
                            AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
				}
				// sub dept wise
				else if(!empty($_POST['subdeptId'])) 
				{
					$sql = "SELECT s.*,SUM(s.qty) AS totalSales FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id
                            AND c.subdeptId IN(SELECT id FROM pos_subdepartment WHERE id=".$subdept.") AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'
                            AND v.branchId=".Yii::app()->session['branchId']." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
				}
				// dept wise
				else if(!empty($_POST['deptId'])) 
				{
					if($_POST['deptId']=='all') // all departments
					{
						$sql = "SELECT s.*,SUM(s.qty) AS totalSales FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'
						        AND i.branchId=".Yii::app()->session['branchId']." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
					}
					else 
					{
						$sql = "SELECT s.*,SUM(s.qty) AS totalSales FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id 
								AND c.subdeptId=d.id AND d.deptId IN(SELECT id FROM pos_department WHERE id=".$dept.") AND s.salesDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' 
								AND v.branchId=".Yii::app()->session['branchId']." AND s.status<>".SalesDetails::STATUS_INACTIVE." GROUP BY s.itemId ORDER BY s.salesDate DESC";
					}
				}
				else $sql = ""; //die($sql);
				$model = Yii::app()->db->createCommand($sql)->queryAll();
				
				// finding dept,subdept,category,supplier
				if($_POST['deptId']=='all') $deptName = 'All';
				else {
					$deptModel = Department::model()->findByPk($_POST['deptId']);
					if(!empty($deptModel)) $deptName = $deptModel->name;
				}
				
				$subdeptModel = Subdepartment::model()->findByPk($_POST['subdeptId']);
				if(!empty($subdeptModel)) $subdeptName = $subdeptModel->name;
				
				$catModel = Category::model()->findByPk($_POST['catId']);
				if(!empty($catModel)) $catName = $catModel->name;
				
				$catModel = Category::model()->findByPk($cat);
				if(!empty($catModel)) $catName = $catModel->name;
				
				$supModel = Supplier::model()->findByPk($supplier);
				if(!empty($supModel)) $suppName = $supModel->name;
			}
		}
		$this->render('salesMovement',array(
			'model'=>$model,
            'msg'=>$msg,
			'dept'=>$dept,'deptName'=>$deptName,
			'subdept'=>$subdept,'subdeptName'=>$subdeptName,
			'cat'=>$cat,'catName'=>$catName,'supplier'=>$supplier,'suppName'=>$suppName,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// sales basket average report
	public function actionBasketAverage() 
	{
		$model=array();
		$msg = $startDate = $endDate = $card = $salesType = $filter = '';
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				if(isset($_POST['salesType']) && !empty($_POST['salesType']) )
				{
					$salesType = $_POST['salesType'];
					 $filter=' AND isEcommerce='.$salesType;
				}
				
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT COUNT(id) AS totalBill,DATE_FORMAT(orderDate, '%Y-%m-%d') AS orderDate,SUM(totalPrice)+SUM(totalTax) AS totalAmount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." 
				        AND orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND status<>".SalesInvoice::STATUS_INACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d') ORDER BY orderDate DESC";
				$model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}

		$this->render('basketAverage',array(
			'model'=>$model,
            'msg'=>$msg, 'salesType'=>$salesType,
			'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}
	
	// sales return summary report
	public function actionSalesReturn() 
	{
		$model=array();
		$msg = $startDate = $endDate = $card = $salesType = $filter = '';
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];

				if(isset($_POST['salesType']) && !empty($_POST['salesType']) )
				{
					$salesType = $_POST['salesType'];
					$filter=' AND s.isEcommerce='.$salesType;
				}

				$sql = "SELECT s.invNo,s.isEcommerce,DATE_FORMAT(r.returnDate, '%Y-%m-%d') AS returnDate,SUM(r.qty) AS totalQty, SUM(r.totalPrice) AS totalAmount FROM pos_sales_return r INNER JOIN pos_sales_invoice s WHERE s.id=r.salesId AND r.branchId=".Yii::app()->session['branchId']."
				        AND r.returnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND r.status=".SalesReturn::STATUS_ACTIVE." GROUP BY DATE_FORMAT(r.returnDate, '%Y%m%d') ORDER BY r.returnDate DESC";
                $model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}

		$this->render('salesReturn',array(
			'model'=>$model,
            'msg'=>$msg,'salesType'=>$salesType,
			'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}
	
	// sales discount summary report
	public function actionSalesDiscount() 
	{
		$model=array();
		$msg = $startDate = $endDate = $card = $salesType = $filter = '';
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];	
				if(isset($_POST['salesType']) && !empty($_POST['salesType']) )
				{
					$salesType = $_POST['salesType'];
					$filter = " AND isEcommerce=".$salesType;
				}
				$sql = "SELECT invNo,orderDate,totalPrice,instantDiscount,totalDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ".$filter." AND status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY orderDate DESC";
				$model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}

		$this->render('salesDiscount',array(
			'model'=>$model,
            'msg'=>$msg, 'salesType'=>$salesType,
			'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}
	
	// sales special discount report
	public function actionSpecialDiscountReport() 
	{
		$msg = $startDate = $endDate = '';
		$model=array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT orderDate,SUM(spDiscount) AS tsDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'
				        AND status<>".SalesInvoice::STATUS_INACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d') ORDER BY orderDate DESC";
				$model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}
		$this->render('specialDiscountReport',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// card wise sells report
	public function actionCardWiseSells() 
	{
		$msg = $startDate = $endDate = $card ='';
		$date = date("Y-m-d");
		$filter = "WHERE cardId!=0";
		
		// bank/cardwise filtering
		if(isset($_POST['card']) && !empty($_POST['card']))
		{
			$card = $_POST['card'];
			$filter.=" AND cardId=".$card;
		}
		
		// daily bank report
		$sql = "SELECT * FROM pos_sales_invoice ".$filter." AND branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$date."%' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
		$model = SalesInvoice::model()->findAllBySql($sql);
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT * FROM pos_sales_invoice ".$filter." AND branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' and status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
				$model = SalesInvoice::model()->findAllBySql($sql);
			}		
		}
		$this->render('cardWiseSells',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'card'=>$card
		));
	}
	
	// card wise sells report by cashier
	public function actionCardReport() 
	{
		$msg = $startDate = $endDate = $card ='';
		$date = date("Y-m-d");
		$filter = "WHERE cardId!=0";
		
		// bank/cardwise filtering
		if(isset($_POST['card']) && !empty($_POST['card']))
		{
			$card = $_POST['card'];
			$filter.=" AND cardId=".$card;
		}
		
		// daily bank report
		$sql = "SELECT * FROM pos_sales_invoice ".$filter." AND branchId=".Yii::app()->session['branchId']." AND crBy=".Yii::app()->user->id." AND orderDate LIKE '".$date."%'
		        AND status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY orderDate DESC";
		$model = SalesInvoice::model()->findAllBySql($sql);
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT * FROM pos_sales_invoice ".$filter." AND branchId=".Yii::app()->session['branchId']." AND crBy=".Yii::app()->user->id." AND orderDate BETWEEN '".$_POST['startDate']."'
				        AND '".$_POST['endDate']."' and status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY orderDate DESC";
				$model = SalesInvoice::model()->findAllBySql($sql);
			}	
		}
		$this->render('cardReport',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
			'card'=>$card
		));
	}
	
	// vat report
	public function actionVatReport() 
	{
		$model=array();
		$msg = $startDate = $endDate = '';
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$model = Tax::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Tax::STATUS_ACTIVE)));
			}		
		}
		$this->render('vatReport',array(
			'model'=>$model,
            'msg'=>$msg,
			'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}
	
	// sales basket average report
	public function actionCollectionReport() 
	{
		$model=array();
		$msg = $startDate = $endDate = $card = $salesType  =  '';
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
            {
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$sql = "SELECT t.name AS pos, u.username AS cashier,i.crBy,SUM(CASE WHEN i.spDiscount=0 THEN i.totalPrice+i.totalTax ELSE i.totalPrice END) AS totalPrice,SUM(i.cashPaid)-SUM(i.totalChangeAmount) AS cashPaid,SUM(i.cardPaid) AS cardPaid,SUM(i.totalDiscount) AS totalDiscount,SUM(i.instantDiscount) AS instantDiscount,SUM(i.loyaltyPaid) AS loyaltyPaid
                        FROM pos_sales_invoice i,pos_tracer t,pos_user u WHERE i.crBy=t.cashierId AND i.crBy=u.id AND i.branchId=".Yii::app()->session['branchId']." AND t.branchId=".Yii::app()->session['branchId']." AND i.orderDate BETWEEN '".$_POST['startDate']."'
						AND '".$_POST['endDate']."' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status<>".SalesInvoice::STATUS_INACTIVE." GROUP BY i.crBy ORDER BY i.orderDate DESC";
                $model = Yii::app()->db->createCommand($sql)->queryAll();
			}		
		}
		$this->render('collectionReport',array(
			'model'=>$model,'msg'=>$msg, 'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}
	
	// sales basket average report
	public function actionPosTracer() 
	{
		$msg = '';
		$sql = "SELECT t.name AS pos, u.username AS cashier,i.crBy,count(i.id) as totalBill,SUM(CASE WHEN i.spDiscount=0 THEN i.totalPrice+i.totalTax ELSE i.totalPrice END) AS totalPrice,SUM(i.cashPaid)-SUM(i.totalChangeAmount) AS cashPaid,SUM(i.cardPaid) AS cardPaid,SUM(i.totalDiscount) AS totalDiscount,SUM(i.instantDiscount) AS instantDiscount,SUM(i.loyaltyPaid) AS loyaltyPaid FROM pos_sales_invoice i,pos_tracer t,
		        pos_user u WHERE i.crBy=t.cashierId AND i.crBy=u.id AND i.branchId=".Yii::app()->session['branchId']." AND t.branchId=".Yii::app()->session['branchId']." AND i.orderDate LIKE '".date("Y-m-d")."%'
				AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status<>".SalesInvoice::STATUS_INACTIVE." GROUP BY i.crBy ORDER BY i.orderDate DESC";
		$model = Yii::app()->db->createCommand($sql)->queryAll();
		
		$this->render('posTracer',array(
			'model'=>$model,
            'msg'=>$msg,
		));
	}

    // select invoices for sales special report
    public function actionSpecialSalesSelect()
    {
        $model = $modelSpecialSales = $specialSalesArr = array();
        $msg = $startDate = $endDate = $card ='';
        $date = date("Y-m-d");

        // after post data
        if(isset($_REQUEST['startDate']) && isset($_REQUEST['endDate']))
        {
            if(empty($_REQUEST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_REQUEST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_REQUEST['startDate'];
                $endDate = $_REQUEST['endDate'];
                $sql = "SELECT * FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$startDate."' AND '".$endDate."'
                        AND status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY orderDate DESC";
                $model = SalesInvoice::model()->findAllBySql($sql);
                $sqlSpecialSales = "SELECT * FROM pos_sales_special WHERE branchId=".Yii::app()->session['branchId']." AND startDate='".$startDate."' AND endDate='".$endDate."'
                                    AND status=".SalesSpecial::STATUS_ACTIVE." ORDER BY crAt DESC";
                $modelSpecialSales = SalesSpecial::model()->findBySql($sqlSpecialSales);
                if(!empty($modelSpecialSales)) :
                    foreach(explode(',',$modelSpecialSales->invoices) as $key=>$data) :
                        $specialSalesArr[$key] = $data;
                    endforeach;
                endif;
            }
        }
        $this->render('specialSalesSelect',array(
            'model'=>$model,'specialSalesArr'=>$specialSalesArr,
            'msg'=>$msg,
            'startDate'=>$startDate,'endDate'=>$endDate,
        ));
    }

    // specialSalesSummary report
    public function actionSpecialSalesSummary()
    {
        $msg = $startDate = $endDate = $salesIds = $branchName = '';
        $branch = Yii::app()->session['branchId'];

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];
                $branch = $_POST['branch'];
                // all branch
                if($branch=='all')
                {
                    $branchName = 'All Branch';
                    $sqlSpecialSales = "SELECT GROUP_CONCAT(invoices SEPARATOR ',') as salesIds FROM pos_sales_special WHERE startDate>='".$startDate."'
                                        AND endDate<='".$endDate."' AND STATUS=".SalesSpecial::STATUS_ACTIVE;
                    $modelSpecialSales = Yii::app()->db->createCommand($sqlSpecialSales)->queryRow();
                    $salesIds = $modelSpecialSales['salesIds'];
                }
                else
                {
                    $branchModel = Branch::model()->findByPk($branch);
                    if(!empty($branchModel)) $branchName = $branchModel->name;
                    $sqlSpecialSales = "SELECT GROUP_CONCAT(invoices SEPARATOR ',') as salesIds FROM pos_sales_special WHERE branchId=".$branch." AND startDate>='".$startDate."'
                                        AND endDate<='".$endDate."' AND STATUS=".SalesSpecial::STATUS_ACTIVE;
                    $modelSpecialSales = Yii::app()->db->createCommand($sqlSpecialSales)->queryRow();
                    $salesIds = $modelSpecialSales['salesIds'];
                }
            }
        }
        $this->render('specialSalesSummary',array(
            'msg'=>$msg,'salesIds'=>$salesIds,'branch'=>$branch,'branchName'=>$branchName,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }

    // specialSalesDetails report
    public function actionSpecialSalesDetails()
    {
        $msg = $startDate = $endDate = $salesIds = $branchName = '';
        $branch = Yii::app()->session['branchId'];
        $model = array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];
                $branch = $_POST['branch'];

                // all branch
                if($branch=='all')
                {
                    $branchName = 'All Branch';
                    $sqlSpecialSales = "SELECT GROUP_CONCAT(invoices SEPARATOR ',') as salesIds FROM pos_sales_special WHERE startDate>='".$startDate."'
                                        AND endDate<='".$endDate."' AND STATUS=".SalesSpecial::STATUS_ACTIVE;
                    $modelSpecialSales  = Yii::app()->db->createCommand($sqlSpecialSales)->queryRow();
                    $salesIds = $modelSpecialSales['salesIds'];
                }
                else
                {
                    $branchModel = Branch::model()->findByPk($branch);
                    if(!empty($branchModel)) $branchName = $branchModel->name;
                    $sqlSpecialSales = "SELECT GROUP_CONCAT(invoices SEPARATOR ',') as salesIds FROM pos_sales_special WHERE branchId=".$branch." AND startDate>='".$startDate."'
                                        AND endDate<='".$endDate."' AND STATUS=".SalesSpecial::STATUS_ACTIVE;
                    $modelSpecialSales  = Yii::app()->db->createCommand($sqlSpecialSales)->queryRow();
                    $salesIds = $modelSpecialSales['salesIds'];
                }
                if(!empty($salesIds))
                {
                    $sql = "SELECT id,invNo,orderDate,totalPrice,totalTax,cashPaid,totalChangeAmount,cardPaid,totalDiscount,spDiscount,instantDiscount,loyaltyPaid FROM pos_sales_invoice
                            WHERE id IN(".$salesIds.") AND status<>".SalesDetails::STATUS_INACTIVE." ORDER BY orderDate DESC";
                    $model = Yii::app()->db->createCommand($sql)->queryAll();
                }
            }
        }
        $this->render('specialSalesDetails',array(
            'msg'=>$msg,'model'=>$model,'branch'=>$branch,'branchName'=>$branchName,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }
	
	// daily/date wise gl/profit/loss
	public function actionDailyGeneralLedger() 
	{
		$msg = $startDate = $endDate = '';
		$pdate = date('Y-m-d', strtotime(' -1 day'));
		
		// finding net sales [ Income ] -> (SUM(totalPrice)+SUM(totalTax)) - SUM(totalDiscount) AS totalSell
		$sqlsales = "SELECT SUM(totalPrice) AS totalSell FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$pdate."%' AND 
		            status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
		$modelsales = Yii::app()->db->createCommand($sqlsales)->queryRow();
		
		// sales return will adjust with sales [ Internal ]
		$sqlsalesreturn = "SELECT SUM(totalPrice) AS totalAmount FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND returnDate LIKE '".$pdate."%' AND status=".SalesReturn::STATUS_ACTIVE." 
		                   ORDER BY returnDate DESC";
		$modelsalesreturn = Yii::app()->db->createCommand($sqlsalesreturn)->queryRow();
		
		// purchase return [ Income ]
		$sqlpr = "SELECT SUM(costPrice) AS totalCost FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND prDate LIKE '".$pdate."%' AND status=".PoReturns::STATUS_ACTIVE." ORDER BY prDate DESC";
		$modelpr = Yii::app()->db->createCommand($sqlpr)->queryRow();
		
		// misc income [ Income ]
		$sqlmi = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND type=".Finance::STATUS_INCOME." AND crAt LIKE '".$pdate."%' 
		          AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
		$modelmi = Yii::app()->db->createCommand($sqlmi)->queryRow();
		
		// purchase discount from supplier as Misc. [ Income ] 
		$sqlpurchasediscount = "SELECT SUM(n.totalDiscount) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate LIKE '".$pdate."%' ORDER BY n.grnDate DESC";
		$modelpurchasediscount = Yii::app()->db->createCommand($sqlpurchasediscount)->queryRow();
		
		// finding net sales [ Income ] costPrice for closing stock
		$sqlcostsales =  "SELECT SUM(d.qty*d.costPrice) AS totalCostPrice FROM pos_sales_invoice i,pos_sales_details d WHERE d.salesId=i.id AND i.branchId=".Yii::app()->session['branchId']." AND d.salesDate LIKE '".$pdate."%' AND 
		                 d.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
		$modelcostsales = Yii::app()->db->createCommand($sqlcostsales)->queryRow();
		
		
		
		// purchase total with non sell stock [ Expense ] -> (SUM(g.totalPrice)-SUM(g.totalDiscount)) AS totalPurchase
		$sqlpurchase = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." 
		                AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate LIKE '".$pdate."%' ORDER BY n.grnDate DESC";
		$modelpurchase = Yii::app()->db->createCommand($sqlpurchase)->queryRow();
		
		// opening stock - from start to last day closing stock all grn costing [expense]
		$sqlopeningstock = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate<'".$pdate."' ORDER BY n.grnDate DESC";
		$modelopeningstock = Yii::app()->db->createCommand($sqlopeningstock)->queryRow();
		
		// damage [expense]
		$sqldam = "SELECT SUM(costPrice) AS totalAmount FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND type=".DamageWastage::TYPE_DAMAGE." AND damDate LIKE '".$pdate."%' 
		          AND STATUS=".DamageWastage::STATUS_ACTIVE." ORDER BY damDate DESC";
		$modeldam = Yii::app()->db->createCommand($sqldam)->queryRow();
		
		// wastage [expense]
		$sqlwas = "SELECT SUM(costPrice) AS totalAmount FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND type=".DamageWastage::TYPE_WASTAGE." AND damDate LIKE '".$pdate."%' 
		          AND STATUS=".DamageWastage::STATUS_ACTIVE." ORDER BY damDate DESC";
		$modelwas = Yii::app()->db->createCommand($sqlwas)->queryRow();
		
		// finding bank commision [ Expense ]
		$sqlbc = "SELECT (((SUM(i.totalPrice)+SUM(i.totalTax)) - SUM(i.totalDiscount))*p.commision)/100 AS totalCommision FROM pos_sales_invoice i,pos_card_types p WHERE i.cardId=p.id
		            AND i.branchId=branchId=".Yii::app()->session['branchId']." AND i.cardId!=0 AND i.orderDate LIKE '".$pdate."%' AND  i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
		$modelbc = Yii::app()->db->createCommand($sqlbc)->queryRow();
		
		// misc expense [ Expense ]
		$sqlmiex = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND type=".Finance::STATUS_EXPENSE." AND crAt LIKE '".$pdate."%' 
		          AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
		$modelmiex = Yii::app()->db->createCommand($sqlmiex)->queryRow();
		
		//  sales discount as Misc. [ Expense ] 
		$sqlsalesdiscountexpense = "SELECT SUM(totalDiscount) AS totalSell FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$pdate."%' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
		$modelsalesdiscountexpense = Yii::app()->db->createCommand($sqlsalesdiscountexpense)->queryRow();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				
				// finding net sales [ Income ]
				$sqlsales = "SELECT SUM(totalPrice) AS totalSell FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$_POST['startDate']."' 
				             AND '".$_POST['endDate']."' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
				$modelsales = Yii::app()->db->createCommand($sqlsales)->queryRow();
				
				// sales return will adjust with sales [ Internal ]
				$sqlsalesreturn = "SELECT SUM(totalPrice) AS totalAmount FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND returnDate BETWEEN '".$_POST['startDate']."' 
				                   AND '".$_POST['endDate']."' AND status=".SalesReturn::STATUS_ACTIVE." ORDER BY returnDate DESC";
				$modelsalesreturn = Yii::app()->db->createCommand($sqlsalesreturn)->queryRow();
				
				// purchase return [ Income ]
				$sqlpr = "SELECT SUM(costPrice) AS totalCost FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND prDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."'
				          AND status=".PoReturns::STATUS_ACTIVE." ORDER BY prDate DESC";
				$modelpr = Yii::app()->db->createCommand($sqlpr)->queryRow();
				
				// misc income [ Income ]
				$sqlmi = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND type=".Finance::STATUS_INCOME." AND crAt BETWEEN '".$_POST['startDate']."' 
				          AND '".$_POST['endDate']."' AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
				$modelmi = Yii::app()->db->createCommand($sqlmi)->queryRow();
				
				// purchase discount from supplier as Misc. [ Income ] 
				$sqlpurchasediscount = "SELECT SUM(n.totalDiscount) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ORDER BY n.grnDate DESC";
				$modelpurchasediscount = Yii::app()->db->createCommand($sqlpurchasediscount)->queryRow();
				
				// finding net sales [ Income ] costPrice for closing stock
				$sqlcostsales = "SELECT SUM(d.qty*d.costPrice) AS totalCostPrice FROM pos_sales_invoice i,pos_sales_details d WHERE d.salesId=i.id AND i.branchId=".Yii::app()->session['branchId']." AND d.salesDate 
				                 BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND d.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
				$modelcostsales = Yii::app()->db->createCommand($sqlcostsales)->queryRow();

				// purchase total with non sell stock [ Expense ]
				$sqlpurchase = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." 
		                        AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' ORDER BY n.grnDate DESC";
				$modelpurchase = Yii::app()->db->createCommand($sqlpurchase)->queryRow();
				
				// opening stock - from start to last day closing stock all grn costing [expense]
				$sqlopeningstock = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate<'".$_POST['endDate']."' ORDER BY n.grnDate DESC";
				$modelopeningstock = Yii::app()->db->createCommand($sqlopeningstock)->queryRow();
				
				// damage [expense]
				$sqldam = "SELECT SUM(costPrice) AS totalAmount FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND type=".DamageWastage::TYPE_DAMAGE." AND damDate 
				           BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND STATUS=".DamageWastage::STATUS_ACTIVE." ORDER BY damDate DESC";
				$modeldam = Yii::app()->db->createCommand($sqldam)->queryRow();
				
				// wastage [expense]
				$sqlwas = "SELECT SUM(costPrice) AS totalAmount FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND type=".DamageWastage::TYPE_WASTAGE." AND damDate 
				           BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND STATUS=".DamageWastage::STATUS_ACTIVE." ORDER BY damDate DESC";
				$modelwas = Yii::app()->db->createCommand($sqlwas)->queryRow();
				
				// finding bank commision [ Expense ]
				$sqlbc = "SELECT (((SUM(i.totalPrice)+SUM(i.totalTax)) - SUM(i.totalDiscount))*p.commision)/100 AS totalCommision FROM pos_sales_invoice i,pos_card_types p WHERE i.cardId=p.id
						  AND i.branchId=branchId=".Yii::app()->session['branchId']." AND i.cardId!=0 AND i.orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' 
						  AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
				$modelbc = Yii::app()->db->createCommand($sqlbc)->queryRow();
				
				// misc income [ Expense ]
				$sqlmiex = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND type=".Finance::STATUS_EXPENSE." AND crAt 
				            BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
				$modelmiex = Yii::app()->db->createCommand($sqlmiex)->queryRow();
				
				//  sales discount as Misc. [ Expense ] 
				$sqlsalesdiscountexpense = "SELECT SUM(totalDiscount) AS totalSell FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
				$modelsalesdiscountexpense = Yii::app()->db->createCommand($sqlsalesdiscountexpense)->queryRow();
			}		
		}
		$this->render('dailyGeneralLedger',array(
			'modelsales'=>$modelsales,'modelsalesreturn'=>$modelsalesreturn,'modelpr'=>$modelpr,'modelmi'=>$modelmi,'modelcostsales'=>$modelcostsales,'modelpurchasediscount'=>$modelpurchasediscount,
			'modelpurchase'=>$modelpurchase,'modeldam'=>$modeldam,'modelwas'=>$modelwas,'modelbc'=>$modelbc,'modelmiex'=>$modelmiex,'modelopeningstock'=>$modelopeningstock,'modelsalesdiscountexpense'=>$modelsalesdiscountexpense,
            'msg'=>$msg,
			'pdate'=>$pdate,
			'startDate'=>$startDate,'endDate'=>$endDate,
		));
	}

    // bank deposit summary report
    public function actionBankDepositSummaryReport()
    {
        $msg = $startDate = $endDate = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                $sql = "SELECT b.*,d.*,SUM(d.amount) AS tAmount FROM pos_bank b,pos_bank_deposit d WHERE b.id=d.bankId AND d.branchId=".Yii::app()->session['branchId']."
						AND d.depositDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND d.status=".BankDeposit::STATUS_ACTIVE." GROUP BY bankId ORDER BY depositDate DESC";
                $model = Yii::app()->db->createCommand($sql)->queryAll();
            }
        }
        $this->render('bankDepositSummary',array(
            'model'=>$model,
            'msg'=>$msg,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }

    // bank deposit details report
    public function actionBankDepositDetailsReport()
    {
        $msg = $filter = $startDate = $endDate = $bank = $accountNo = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                // bank/ac wise
                if(!empty($_POST['bank'])) $bank = $_POST['bank'];
                if(!empty($_POST['accountNo']))
                {
                    $accountNo = $_POST['accountNo'];
                    if(!empty(Bank::getBankByAccount($accountNo)))
                        $filter.=" AND bankId=".Bank::getBankByAccount($accountNo);
                }

                $sql = "SELECT * FROM pos_bank_deposit WHERE branchId=".Yii::app()->session['branchId']." $filter
                        AND depositDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".BankDeposit::STATUS_ACTIVE." ORDER BY depositDate DESC";
                $model = BankDeposit::model()->findAllBySql($sql);
            }
        }
        $this->render('bankDepositDetails',array(
            'model'=>$model,
            'msg'=>$msg,
            'startDate'=>$startDate,'endDate'=>$endDate,
            'bank'=>$bank,'accountNo'=>$accountNo
        ));
    }

    // bank withdraw summary report
    public function actionBankWithdrawSummaryReport()
    {
        $msg = $startDate = $endDate = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                $sql = "SELECT b.*,d.*,SUM(d.amount) AS tAmount FROM pos_bank b,pos_bank_withdraw d WHERE b.id=d.bankId AND d.branchId=".Yii::app()->session['branchId']."
						AND d.withdrawDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND d.status=".BankWithdraw::STATUS_ACTIVE." GROUP BY bankId ORDER BY withdrawDate DESC";
                $model = Yii::app()->db->createCommand($sql)->queryAll();
            }
        }
        $this->render('bankWithdrawSummary',array(
            'model'=>$model,
            'msg'=>$msg,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }

    // bank withdraw details report
    public function actionBankWithdrawDetailsReport()
    {
        $msg = $filter = $startDate = $endDate = $bank = $accountNo = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                // bank/ac wise
                if(!empty($_POST['bank'])) $bank = $_POST['bank'];
                if(!empty($_POST['accountNo']))
                {
                    $accountNo = $_POST['accountNo'];
                    if(!empty(Bank::getBankByAccount($accountNo)))
                        $filter.=" AND bankId=".Bank::getBankByAccount($accountNo);
                }

                $sql = "SELECT * FROM pos_bank_withdraw WHERE branchId=".Yii::app()->session['branchId']." $filter
                        AND withdrawDate BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".BankWithdraw::STATUS_ACTIVE." ORDER BY withdrawDate DESC";
                $model = BankWithdraw::model()->findAllBySql($sql);
            }
        }
        $this->render('bankWithdrawDetails',array(
            'model'=>$model,
            'msg'=>$msg,
            'startDate'=>$startDate,'endDate'=>$endDate,
            'bank'=>$bank,'accountNo'=>$accountNo
        ));
    }

    // bank current balance
    public function actionBankCurrentBalance()
    {
        $bankModel = Bank::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Bank::STATUS_ACTIVE), 'order'=>'name'));

        $this->render('bankCurrentBalance',array(
            'bankModel'=>$bankModel
        ));
    }

    // cash in hands report
    public function actionCashInHands()
    {
        // total widraw
        $sqlWithdraw = "SELECT SUM(amount) AS totalAmount FROM `pos_bank_withdraw` WHERE branchId=".Yii::app()->session['branchId']." AND status=".BankWithdraw::STATUS_ACTIVE;
        $modelWithdraw = Yii::app()->db->createCommand($sqlWithdraw)->queryRow();

        // supplier withdraw
        $sqlSuppWithdraw = "SELECT SUM(amount) AS totalAmount FROM pos_supplier_widraw WHERE branchId=".Yii::app()->session['branchId']." AND bankId=0 AND status=".SupplierWidraw::STATUS_ACTIVE;
        $modelSuppWithdraw = Yii::app()->db->createCommand($sqlSuppWithdraw)->queryRow();

        // worker withdraw
        $sqlWkWithdraw = "SELECT SUM(amount) AS totalAmount FROM `pos_temporary_worker_widraw` WHERE branchId=".Yii::app()->session['branchId']." AND bankId=0 AND status=".TemporaryWorkerWidraw::STATUS_ACTIVE;
        $modelWkWithdraw = Yii::app()->db->createCommand($sqlWkWithdraw)->queryRow();

        // misc expense
        $sqlMiscExp = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND bankId=0 AND type=".Finance::STATUS_EXPENSE." AND status=".Finance::STATUS_ACTIVE;
        $modelMiscExp = Yii::app()->db->createCommand($sqlMiscExp)->queryRow();
        $balance = $modelWithdraw['totalAmount']-($modelSuppWithdraw['totalAmount']+$modelWkWithdraw['totalAmount']+$modelMiscExp['totalAmount']);

        $this->render('cashInHands',array(
            'modelWithdraw'=>$modelWithdraw,'modelSuppWithdraw'=>$modelSuppWithdraw,'modelWkWithdraw'=>$modelWkWithdraw,
            'modelMiscExp'=>$modelMiscExp,'balance'=>$balance,
        ));
    }

	// misc income report by category
	public function actionMiscIncome() 
	{
		$msg = $cat = $startDate = $endDate = '';
		$model = $catModel = array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['catId'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one category !</p></div>';
			else if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{
				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$cat = $_POST['catId'];
				$catModel = FinanceCategory::model()->findByPk($cat);
				
				// category wise
				if(!empty($_POST['catId'])) 
				{
					$sql = "SELECT * FROM pos_finance WHERE catId=".$cat." AND TYPE=".Finance::STATUS_INCOME." AND branchId=".Yii::app()->session['branchId']."
							AND DATE_FORMAT(crAt, '%Y-%m-%d') BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
				}
				else $sql = "";
				$model = Finance::model()->findAllBySql($sql);
			}		
		}
		$this->render('miscIncome',array(
			'model'=>$model,
            'msg'=>$msg,
			'cat'=>$cat,'catModel'=>$catModel,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}
	
	// misc expense report by category
	public function actionMiscExpense() 
	{
		$msg = $cat = $startDate = $endDate = '';
		$model = $catModel = array();
		
		// after post data
		if(isset($_POST['startDate']) && isset($_POST['endDate']))
		{
			if(empty($_POST['catId'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one category !</p></div>';
			else if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
			else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
			else
			{

				$startDate = $_POST['startDate'];
				$endDate = $_POST['endDate'];
				$cat = $_POST['catId'];
				$catModel = FinanceCategory::model()->findByPk($cat);
				
				// category wise
				if(!empty($_POST['catId'])) 
				{
					$sql = "SELECT * FROM pos_finance WHERE catId=".$cat." AND TYPE=".Finance::STATUS_EXPENSE." AND branchId=".Yii::app()->session['branchId']."
							AND DATE_FORMAT(crAt, '%Y-%m-%d') BETWEEN '".$_POST['startDate']."' AND '".$_POST['endDate']."' AND status=".Finance::STATUS_ACTIVE." ORDER BY crAt DESC";
				}
				else $sql = "";
				$model = Finance::model()->findAllBySql($sql);
			}		
		}
		$this->render('miscExpense',array(
			'model'=>$model,
            'msg'=>$msg,
			'cat'=>$cat,'catModel'=>$catModel,
			'startDate'=>$startDate,'endDate'=>$endDate
		));
	}

	// couponsReport
	public function actionCouponsReport()
	{
        $msg = $filter = $startDate = $endDate = $bank = $accountNo = $isUsed = '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                // isUsed
                if(!empty($_POST['isUsed']))
                {
                    $isUsed = $status = $_POST['isUsed'];
                    $filter.=" AND status=".$status;
                }
                $sql = "SELECT * FROM pos_coupons WHERE couponStartDate >= '".$startDate."' AND DATE_FORMAT(couponEndDate, '%Y-%m-%d')<='".$endDate."' ".$filter." ORDER BY couponStartDate DESC";
                $model = Coupons::model()->findAllBySql($sql);
            }
        }
        $this->render('couponsReport',array(
            'model'=>$model,
            'msg'=>$msg,
            'isUsed'=>$isUsed,
            'startDate'=>$startDate,'endDate'=>$endDate            
        ));
	}

	// coupons Wise Sales Report
	public function actionCouponWiseSales()
	{
        $msg = $filter = $startDate = $endDate = $bank = $accountNo =  '';
        $model=array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];
                
                $sql = "SELECT * FROM pos_sales_invoice  WHERE DATE_FORMAT(crAt,'%Y-%m-%d') BETWEEN'".$startDate."' AND '".$endDate."' AND branchId='".Yii::app()->session['branchId']."' AND couponId>0 AND status<>".SalesInvoice::STATUS_INACTIVE;
                $model = SalesInvoice::model()->findAllBySql($sql);
            }
        }
        $this->render('couponWiseSales',array(
            'model'=>$model,
            'msg'=>$msg,          
            'startDate'=>$startDate,'endDate'=>$endDate            
        ));
	}

    // coupons Wise Sales Report
    public function actionServiceChargeReport()
    {
        $msg =  $startDate = $endDate = '';
        $netSales = $netReturn  = $serviceChargeAmount = 0;
        $model = $modelReturn = array();

        // after post data
        if(isset($_POST['startDate']) && isset($_POST['endDate']))
        {
            if(empty($_POST['startDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select start date !</p></div>';
            else if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else
            {
                $startDate = $_POST['startDate'];
                $endDate = $_POST['endDate'];

                $sql = "SELECT ROUND((SUM(s.qty*s.salesPrice)-SUM(s.discountPrice)),2) AS totalSell FROM pos_sales_invoice v, pos_sales_details s WHERE v.id=s.salesId
                        AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." AND s.status<>".SalesDetails::STATUS_INACTIVE." ORDER BY s.salesDate DESC";
                $model = Yii::app()->db->createCommand($sql)->queryRow();
                $netSales = $model['totalSell'];

                $sqlReturnAmount = "SELECT SUM(r.totalPrice) AS totalReturnAmount FROM pos_sales_return r WHERE r.branchId=".Yii::app()->session['branchId']." AND r.returnDate BETWEEN '".$startDate."' AND '".$endDate."' AND r.status=".SalesReturn::STATUS_ACTIVE." ORDER BY r.returnDate DESC";
                $modelReturn = Yii::app()->db->createCommand($sqlReturnAmount)->queryRow();
                $netReturn = $modelReturn['totalReturnAmount'];
                $serviceChargeAmount =  round(((($netSales-$netReturn)*10)/100),2);
            }
        }
        $this->render('serviceChargeReport',array(
            'model'=>$model,
            'msg'=>$msg,'netSales'=>$netSales,'netReturn'=>$netReturn,'serviceChargeAmount'=>$serviceChargeAmount,
            'startDate'=>$startDate,'endDate'=>$endDate
        ));
    }

    // method for all pdf Report Generate
    public function actionPdfReportGenerate()
    {
        $pdfFile = '';
        $model = array();
		$modelwidraw = array();
        $startDate = '';
        $endDate = '';
        $prNo='';
        $deptName='';
        $subdeptName = '';
        $suppName = '';
        $itemCode = '';
        $catName = '';
        $catModel = '';
        $brandName='';
        $modelWithdraw='';
        $modelSuppWithdraw='';
        $modelWkWithdraw = '';
        $modelMiscExp='';
        $balance='';
        $crType = '';
        $salesType = '';
        $netSales = $netReturn  = $serviceChargeAmount = 0;

        if(isset($_REQUEST['startDate']) && !empty($_REQUEST['startDate']) ) $startDate = $_REQUEST['startDate'];
        if(isset($_REQUEST['endDate']) && !empty($_REQUEST['endDate']) ) $endDate = $_REQUEST['endDate'];
        if(isset($_REQUEST['suppName']) && !empty($_REQUEST['suppName']) ) $suppName = $_REQUEST['suppName'];
        if(isset($_REQUEST['prNo']) && !empty($_REQUEST['prNo']) ) $prNo = $_REQUEST['prNo'];
        if(isset($_REQUEST['pdfFile']) && !empty($_REQUEST['pdfFile'])) $pdfFile = $_REQUEST['pdfFile'];
        if(isset($_REQUEST['deptName']) && !empty($_REQUEST['deptName'])) $deptName = $_REQUEST['deptName'];
        if(isset($_REQUEST['subdeptName']) && !empty($_REQUEST['subdeptName'])) $deptName = $_REQUEST['subdeptName'];
        if(isset($_REQUEST['itemCode']) && !empty($_REQUEST['itemCode'])) $itemCode = $_REQUEST['itemCode'];
        if(isset($_REQUEST['catName']) && !empty($_REQUEST['catName'])) $catName = $_REQUEST['catName'];
        if(isset($_REQUEST['catModel']) && !empty($_REQUEST['catModel'])) $catModel = $_REQUEST['catModel'];
        if(isset($_REQUEST['brandName']) && !empty($_REQUEST['brandName'])) $brandName = $_REQUEST['brandName'];

        if(isset($_REQUEST['modelWithdraw']) && !empty($_REQUEST['modelWithdraw'])) $modelWithdraw = $_REQUEST['modelWithdraw'];
        if(isset($_REQUEST['modelSuppWithdraw']) && !empty($_REQUEST['modelSuppWithdraw'])) $modelSuppWithdraw = $_REQUEST['modelSuppWithdraw'];
        if(isset($_REQUEST['modelWkWithdraw']) && !empty($_REQUEST['modelWkWithdraw'])) $modelWkWithdraw = $_REQUEST['modelWkWithdraw'];
        if(isset($_REQUEST['modelMiscExp']) && !empty($_REQUEST['modelMiscExp'])) $modelMiscExp = $_REQUEST['modelMiscExp'];
        if(isset($_REQUEST['balance']) && !empty($_REQUEST['balance'])) $balance = $_REQUEST['balance'];
        if(isset($_REQUEST['crType']) && !empty($_REQUEST['crType'])) $crType = $_REQUEST['crType'];
        if(isset($_REQUEST['salesType']) && !empty($_REQUEST['salesType'])) $salesType = $_REQUEST['salesType'];

        if(isset($_REQUEST['netSales']) && !empty($_REQUEST['netSales'])) $netSales = $_REQUEST['netSales'];
        if(isset($_REQUEST['netReturn']) && !empty($_REQUEST['netReturn'])) $netReturn = $_REQUEST['netReturn'];
        if(isset($_REQUEST['serviceChargeAmount']) && !empty($_REQUEST['serviceChargeAmount'])) $serviceChargeAmount = $_REQUEST['serviceChargeAmount'];

        if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
        {
            $model = Yii::app()->session['reportModel'];
            unset(Yii::app()->session['reportModel']);
            Yii::app()->session['reportModelPrevious'] = $model;
        }
        else $model = Yii::app()->session['reportModelPrevious'];    
        
        if(isset(Yii::app()->session['modelwidraw']) && !empty(Yii::app()->session['modelwidraw']))
        {
            $modelwidraw = Yii::app()->session['modelwidraw'];
            unset(Yii::app()->session['modelwidraw']);
            Yii::app()->session['reportModelPreviousSecound'] = $modelwidraw;
        }
        else $modelwidraw = Yii::app()->session['reportModelPreviousSecound'];
		

        // pdf generations
        $stylesheet= file_get_contents(Yii::getPathOfAlias("webroot").'/'.Yii::app()->params['skinDefault'].'/css/style.css');
        $stylesheet.= file_get_contents(Yii::getPathOfAlias("webroot").'/'.Yii::app()->params['skinDefault'].'/css/style-metro.css');
        $stylesheet.= file_get_contents(Yii::getPathOfAlias("webroot").'/'.Yii::app()->params['skinDefault'].'/plugins/bootstrap/css/bootstrap.min.css');
        $stylesheet.= file_get_contents(Yii::getPathOfAlias("webroot").'/css/form.css');
        $pdfRoot = Yii::getPathOfAlias("webroot").'/media/exportPdf/';

        $mPdf = Yii::app()->ePdf->mpdf('', 'A4');
        $mPdf->WriteHTML($stylesheet, 1);
        $mPdf->WriteHTML($this->renderPartial($pdfFile,array(
            'model'=>$model,
            'modelwidraw'=>$modelwidraw,
            'startDate'=>$startDate,
            'endDate'=>$endDate,
            'suppName'=>$suppName,
            'prNo'=>$prNo,
            'deptName'=>$deptName,
            'subdeptName'=>$subdeptName,
            'itemCode'=>$itemCode,
            'catName'=>$catName,
            'catModel'=>$catModel,
            'brandName'=>$brandName,
            'modelWithdraw'=>$modelWithdraw,
            'modelSuppWithdraw'=>$modelSuppWithdraw,'modelWkWithdraw'=>$modelWkWithdraw,
            'modelMiscExp'=>$modelMiscExp,
            'balance'=>$balance,'crType'=>$crType,
			'salesType'=>$salesType,'netSales'=>$netSales,'netReturn'=>$netReturn,'serviceChargeAmount'=>$serviceChargeAmount,
        ), true));
        $mPdf->Output($pdfRoot.Yii::app()->session['orgId'].'_'.$pdfFile.'.pdf');

        if(file_exists($pdfRoot.Yii::app()->session['orgId'].'_'.$pdfFile.'.pdf'))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private",false);
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename='.$pdfFile.'.pdf');
            readfile($pdfRoot.Yii::app()->session['orgId'].'_'.$pdfFile.'.pdf');
            @unlink($pdfRoot.Yii::app()->session['orgId'].'_'.$pdfFile.'.pdf');
        }
    }
	
}