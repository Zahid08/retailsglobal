<?php
/*********************************************************
        -*- File: AjaxController.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/config
		-*-  YII-*- version 1.1.13
/*********************************************************/
class AjaxController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
	 * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
	 */
	public $metaTitle 	 	 = NULL;
	public $metaKeywords 	 = NULL;
	public $metaDescriptions = NULL;
	public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    public function actionDynamicDistrict()
    {
        if(isset($_POST['divisions']))
        {
            $branch=new  Branch();
            $district=$branch->getAllDistricByDivision($_POST['divisions']);
            foreach($district as $key=>$val){
                echo CHtml::tag('option', array('value'=>$key), CHtml::encode($val),true);
            }
        }
        else echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Sub Department"),true);
    }

    public function actionDynamicUpozela()
    {
        if(isset($_POST['districts']))
        {
            $branch=new  Branch();
            $upozela=$branch->getAllUpozela($_POST['districts']);
            foreach($upozela as $key=>$val){
                echo CHtml::tag('option', array('value'=>$key), CHtml::encode($val),true);
            }
        }
        else echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Sub Department"),true);
    }


	// get sub department by department
	public function actionDynamicSubDepartment() 
	{
		if(isset($_POST['deptId'])) 
		{
			$data = Subdepartment::model()->findAll('deptId like :deptId',array(':deptId'=>$_POST['deptId']));		
			$data = CHtml::listData($data,'id','name'); 
			echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Sub Department"),true);
			foreach($data as $key=>$val)
				echo CHtml::tag('option', array('value'=>$key), CHtml::encode($val),true);
		}
		else echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Sub Department"),true);
	}
	
	// get category by sub department
	public function actionDynamicCategory() 
	{
		if(isset($_POST['subdeptId'])) 
		{
			$data = Category::model()->findAll('subdeptId like :subdeptId',array(':subdeptId'=>$_POST['subdeptId']));		
			$data = CHtml::listData($data,'id','name'); 
			echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Category"),true);
			foreach($data as $key=>$val)
				echo CHtml::tag('option', array('value'=>$key), CHtml::encode($val),true);
		}
		else echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Category"),true);
	}
	
	// get finance category by type
	public function actionDynamicFinanceCatByType() 
	{
		if(isset($_POST['type'])) 
		{
			$data = FinanceCategory::model()->findAll('type=:type and status=:status',array(':type'=>$_POST['type'],':status'=>FinanceCategory::STATUS_ACTIVE));		
			$data = CHtml::listData($data,'id','title'); 
			echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Category"),true);
			foreach($data as $key=>$val)
				echo CHtml::tag('option', array('value'=>$key), CHtml::encode($val),true);
		}
		else echo CHtml::tag('option', array('value'=>""), CHtml::encode("Select Category"),true);
	}
	
	// purchase order all products by supplier 
	public function actionPurchaseProducts() 
	{
		if(isset($_POST['PurchaseOrder']['supplierId']))
		{
			$supplierId = $_POST['PurchaseOrder']['supplierId'];
			echo $supplierId;

			$suppModel = Supplier::model()->findByPk($supplierId);
			$itemModel = Items::model()->findAll('status=:status',
                                           array(':status'=>Items::STATUS_ACTIVE));

//			echo "<pre>";print_r($itemModel);
//			exit();

			if(!empty($itemModel)) :
            	$this->renderPartial('_itemwisePurchase',array('suppModel'=>$suppModel,'itemModel'=>$itemModel,'ajax'=>true),false,true); 
			else :
				$this->renderPartial('_itemwisePurchaseBlank',array('ajax'=>true),false,true);
			endif;
				
		}
	}
	
	// po edit order all products by po id 
	public function actionUpdateProductsByPo() 
	{
		if(isset($_POST['poNo']))
		{
			$poNo = $_POST['poNo'];
			$poNoModel = PurchaseOrder::model()->find('poNo=:poNo',array(':poNo'=>$poNo));
			if(!empty($poNoModel)) :
				$grnNoModel = GoodReceiveNote::model()->find('poId=:poId',array(':poId'=>$poNoModel->id));
				if(!empty($grnNoModel) && $grnNoModel->status==GoodReceiveNote::STATUS_APPROVED) 
					echo '<div style=" background:#fae2e3; border:1px dashed #e02222; margin:5px 0px; padding:5px 10px;color:red; font-size:14px;">GRN Alreday Approved for this PO !</div>';
				else
				{
					$poitemModel = PoDetails::model()->findAll('poId like :poId',array(':poId'=>$poNoModel->id));
					$this->renderPartial('_itemwisePurchaseUpdate',array('poNoModel'=>$poNoModel,'itemModel'=>$poitemModel,'ajax'=>true),false,true); 
				}
			else : 
				$this->renderPartial('_itemwisePurchaseUpdateBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// purchase return all products by supplier 
	public function actionPurchaseReturn() 
	{
		if(isset($_POST['PoReturns']['supplierId']))
		{
			$supplierId = $_POST['PoReturns']['supplierId'];
			$suppModel = Supplier::model()->findByPk($supplierId);
			$itemModel = Items::model()->findAll('supplierId like :supplierId AND isParent=:isParent AND status=:status',
                                            array(':supplierId'=>$supplierId,':isParent'=>Items::IS_NOT_PARENTS,':status'=>Items::STATUS_ACTIVE));
			if(!empty($itemModel)) :
            	$this->renderPartial('_itemwisePurchaseReturn',array('suppModel'=>$suppModel,'itemModel'=>$itemModel,'ajax'=>true),false,true); 
			else :
				$this->renderPartial('_itemwisePurchaseBlank',array('ajax'=>true),false,true);
			endif;
				
		}
	}
	
	// po cancel order all products by po id 
	public function actionCancelProductsByPo() 
	{
		if(isset($_POST['poNo']))
		{
			$poNo = $_POST['poNo'];
			$poNoModel = PurchaseOrder::model()->find('poNo=:poNo',array(':poNo'=>$poNo));
			if(!empty($poNoModel)) :
				$grnNoModel = GoodReceiveNote::model()->find('poId=:poId',array(':poId'=>$poNoModel->id));
				if(!empty($grnNoModel)) 
					echo '<div style=" background:#fae2e3; border:1px dashed #e02222; margin:5px 0px; padding:5px 10px;color:red; font-size:14px;">GRN Alreday done for this PO !</div>';
				else
				{
					$poitemModel = PoDetails::model()->findAll('poId like :poId',array(':poId'=>$poNoModel->id));
					$this->renderPartial('_itemwisePurchaseCancel',array('poNoModel'=>$poNoModel,'itemModel'=>$poitemModel,'ajax'=>true),false,true); 
				}
			else : 
				$this->renderPartial('_itemwisePurchaseUpdateBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// supplier info by supplierid at grn
	public function actionGrnSupplierInfo() 
	{
		if(isset($_POST['GoodReceiveNote']['supplierId']))
		{
			$supplierId = $_POST['GoodReceiveNote']['supplierId'];
			$suppModel = Supplier::model()->findByPk($supplierId);
            $this->renderPartial('_grnSupplierInfo',array('suppModel'=>$suppModel,'ajax'=>true),false,true); 
		}
	}
	
	// supplier info by supplierid at credit payment
	public function actionPaymentSupplierInfoCredit() 
	{
		if(isset($_POST['SupplierWidraw']['supId']))
		{
			$supplierId = $_POST['SupplierWidraw']['supId'];
			$suppModel = Supplier::model()->findByPk($supplierId);
            $this->renderPartial('_paymentSupplierInfoCredit',array('suppModel'=>$suppModel,'ajax'=>true),false,true); 
		}
	}
	
	// supplier info by supplierid at consig payment
	public function actionPaymentSupplierInfoConsig() 
	{
		if(isset($_POST['SupplierWidraw']['supId']))
		{
			$supplierId = $_POST['SupplierWidraw']['supId'];
			$suppModel = Supplier::model()->findByPk($supplierId);
            $this->renderPartial('_paymentSupplierInfoConsig',array('suppModel'=>$suppModel,'ajax'=>true),false,true); 
		}
	}

    // temporary worker ajax info balance while payment
    public function actionTemporaryWorkerInfo()
    {
        if(isset($_POST['TemporaryWorkerWidraw']['wkId']))
        {
            $wkId = $_POST['TemporaryWorkerWidraw']['wkId'];
            $this->renderPartial('_paymentTmpWorkerInfo',array('wkId'=>$wkId,'ajax'=>true),false,true);
        }
    }
	
	// grn order all products by po 
	public function actionGrnProductsByPo() 
	{
		if(isset($_POST['poNo']))
		{
			$poNo = $_POST['poNo'];
			$poModel = PurchaseOrder::model()->find('poNo like :poNo',array(':poNo'=>$poNo));
			if(!empty($poModel)) :
				$poitemModel = PoDetails::model()->findAll('poId like :poId',array(':poId'=>$poModel->id));
            	$this->renderPartial('_itemwiseGrn',array('poModel'=>$poModel,'itemModel'=>$poitemModel,'ajax'=>true),false,true); 
			else : 
				$this->renderPartial('_itemwiseGrnBlank',array('ajax'=>true),false,true);
			endif;
		}
	}

    // auto grn all products by supplier
    public function actionDirectGrnProductsBySupplier()
    {
        if(isset($_POST['PurchaseOrder']['supplierId']))
        {
            $supplierId = $_POST['PurchaseOrder']['supplierId'];
            $suppModel = Supplier::model()->findByPk($supplierId);
            $itemModel = Items::model()->findAll('supplierId like :supplierId AND status=:status',
                                           array(':supplierId'=>$supplierId,':status'=>Items::STATUS_ACTIVE));
                                           
            if(!empty($itemModel)) :
                $this->renderPartial('_itemwisePurchase',array('suppModel'=>$suppModel,'itemModel'=>$itemModel,'ajax'=>true),false,true);
            else :
                $this->renderPartial('_itemwisePurchaseBlank',array('ajax'=>true),false,true);
            endif;

        }
    }
	
	// grn edit order all products by po 
	public function actionGrnUpdateProductsByGrn() 
	{
		if(isset($_POST['grnNo']) && isset($_POST['type']))
		{
			$grnNo = $_POST['grnNo'];
			$type = $_POST['type'];
			$grnNoModel = GoodReceiveNote::model()->find('grnNo=:grnNo',array(':grnNo'=>$grnNo));
			if(!empty($grnNoModel)) :
				if($type=='updateOnly')
				{
					if($grnNoModel->status==GoodReceiveNote::STATUS_APPROVED)
						echo '<div style=" background:#fae2e3; border:1px dashed #e02222; margin:5px 0px; padding:5px 10px;color:red; font-size:14px;">Can,t update Approved GRN !</div>';
					else
					{
                        //$sqlGrn = "SELECT * FROM pos_stock WHERE grnId=".$grnNoModel->id." AND itemId NOT IN(SELECT id FROM pos_items WHERE isParent=".Items::IS_PARENTS.")";
                        //$grnitemModel = Stock::model()->findAllBySql($sqlGrn);
						$grnitemModel = Stock::model()->findAll('grnId like :grnId',array(':grnId'=>$grnNoModel->id));
						$this->renderPartial('_itemwiseGrnUpdate',
							array('grnNoModel'=>$grnNoModel,'itemModel'=>$grnitemModel,'ajax'=>true),false,true); 
					}
				}
				else
				{
                    //$sqlGrn = "SELECT * FROM pos_stock WHERE grnId=".$grnNoModel->id." AND itemId NOT IN(SELECT id FROM pos_items WHERE isParent=".Items::IS_PARENTS.")";
                    //$grnitemModel = Stock::model()->findAllBySql($sqlGrn);
					$grnitemModel = Stock::model()->findAll('grnId like :grnId',array(':grnId'=>$grnNoModel->id));
					$this->renderPartial('_itemwiseGrnUpdate',
						array('grnNoModel'=>$grnNoModel,'itemModel'=>$grnitemModel,'ajax'=>true),false,true);		
				}
			else : 
				$this->renderPartial('_itemwiseGrnUpdateBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// get item info By item code
	public function actionGetItemInfoByCode() 
	{
		if(isset($_POST['itemCode']))
		{
			$itemCode = $_POST['itemCode'];
			$itemModel = Items::model()->find('itemCode=:itemCode',array(':itemCode'=>$itemCode));
			if(!empty($itemModel)) :
                $totalCostPrice = 0;
                if($itemModel->isParent==Items::IS_PARENTS) :
                    foreach(ItemParents::getAllItemParentsByChield($itemModel->id,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)  :
                        $totalCostPrice+= $dataParents->qty*$dataParents->parent->costPrice;
                    endforeach;
                else : $totalCostPrice = $itemModel->costPrice;
                endif;
            	$this->renderPartial('_iteminfoByCode',array('itemModel'=>$itemModel,'totalCostPrice'=>$totalCostPrice,'ajax'=>true),false,true);
			else : 
				$this->renderPartial('_iteminfoByCodeBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// validate Product By Code
	public function actionProductByCode() 
	{

		if(isset($_POST['code']))
		{
			$itemArr = array();
			$itemCode = $_POST['code'];
			if(isset($_POST['weight'])) $weight = $_POST['weight'];
			else $weight = 0;


			if($itemCode!="" || $itemCode!=0) 
			{

				$date = date("Y-m-d");
				$itemModel = Items::model()->find('itemCode=:itemCode OR barCode=:barCode',array(':itemCode'=>$itemCode, ':barCode'=>$itemCode));

				if(!empty($itemModel) && $itemModel->status==Items::STATUS_ACTIVE) :
					$itemArr['itemId'] = $itemModel->id;
					$itemArr['itemCode'] = $itemModel->itemCode;
					$itemArr['itemName'] = $itemModel->itemName;
					
					// finding item actual cost price FIFO (N.B : if negative stock then get current stock price) rules = currentStock-grnQtySumByDesc < sellQty
                    $costPrice = $currentStock = $stockSum = 0;
                    $currentStock = Stock::getItemWiseStock($itemModel->id);

                    // negative stock then current cost price
                    if($currentStock<=0) $costPrice = $itemModel->costPrice;
                    else
                    {
                        $stockModel = array();
                        // general stock model
                        $stockModel = Stock::model()->findAll(array('condition'=>'branchId=:branchId AND itemId=:itemId AND status=:status',
                                                                    'params'=>array(':branchId'=>Yii::app()->session['branchId'],':itemId'=>$itemModel->id,':status'=>Stock::STATUS_APPROVED),
                                                                    'order'=>'stockDate DESC'));
                        // stock transfer model
                        if (empty($stockModel))
                        {
                            $stockModel = StockTransfer::model()->findAll(array('condition'=>'toBranchId=:toBranchId AND itemId=:itemId AND status=:status',
                                                                                'params'=>array(':toBranchId'=>Yii::app()->session['branchId'],':itemId'=>$itemModel->id,':status'=>StockTransfer::STATUS_ACTIVE),
                                                                                'order'=>'stockDate DESC'));
                        }
                        if (!empty($stockModel))
                        {
                            foreach ($stockModel as $sKey => $sData) :
                                $stockSum += $sData->qty;
                                if (($currentStock - $stockSum) < 1) // 1 = default sold quantity
                                {
                                    $costPrice = $sData->costPrice;
                                    break;
                                }
                            endforeach;
                        }
                        else $costPrice = $itemModel->costPrice;
                    }

                    // assign values to ajax response
					$itemArr['costPrice'] = $costPrice;
					$itemArr['sellPrice'] = $itemModel->sellPrice;
					$itemArr['isWeighted'] = $itemModel->isWeighted;
					$itemArr['weight'] = $weight;
					//$itemArr['taxPercent'] = $itemModel->tax->taxRate;

					$itemArr['taxPercent'] = Tax::getTaxByTaxId($itemModel->taxId);
                    $itemArr['currentStock'] = $currentStock;

                    $checkBranchSql = "SELECT * FROM pos_poffers WHERE itemId=".$itemModel->id." AND startDate<='".$date."' AND endDate>='".$date."'";
                    $checknBranchModel = Poffers::model()->findBySql($checkBranchSql);

                    $brancId=Yii::app()->session['branchId'];
                   if (!empty($checknBranchModel)){
                       if ($checknBranchModel->branchId==0) {
                           $brancId = $checknBranchModel->branchId;
                       }
                   }
                   $status=1;
					// offer or discount by durations
					$sqlOffer = "SELECT * FROM pos_poffers WHERE itemId=".$itemModel->id." AND branchId=".$brancId." AND status=".$status." AND startDate<='".$date."' AND endDate>='".$date."'";
					$offerModel = Poffers::model()->findBySql($sqlOffer);
					if(!empty($offerModel)){

                        $discountPercentage=$offerModel->package->discount;
                        if ($offerModel->package->discount==0 || empty($offerModel->package->discount)){
                            $discountPercentage=$this->percentageOf( $offerModel->package->flat_amount, $itemModel->sellPrice );
                        }
						$itemArr['discountPercent'] = $discountPercentage;
					}
					else $itemArr['discountPercent'] = '0';
					echo CJSON::encode($itemArr);
				else : 
					echo CJSON::encode('invalid');
				endif;
			}
			else echo CJSON::encode('null');
		}
	}

    function percentageOf( $number, $everything, $decimals = 2 ){
        return round( $number / $everything * 100, $decimals );
    }

    // validate Product By Code
    public function actionStProductByCode()
    {
        if(isset($_POST['code']))
        {
            $itemArr = array();
            $itemCode = $_POST['code'];
            if(isset($_POST['weight'])) $weight = $_POST['weight'];
            else $weight = 0;



            if($itemCode!="" || $itemCode!=0)
            {
                $date = date("Y-m-d");
                $itemModel = Items::model()->find('itemCode=:itemCode OR barCode=:barCode',array(':itemCode'=>$itemCode, ':barCode'=>$itemCode));

                if(!empty($itemModel) && $itemModel->status==Items::STATUS_ACTIVE) :
                    $itemArr['itemId'] = $itemModel->id;
                    $itemArr['itemCode'] = $itemModel->itemCode;
                    $itemArr['itemName'] = $itemModel->itemName;

                    // finding item actual cost price FIFO (N.B : if negative stock then get current stock price) rules = currentStock-grnQtySumByDesc < sellQty
                    $costPrice = $currentStock = $stockSum = 0;
                    $currentStock = Stock::getStItemWiseStock($itemModel->id);

                    // negative stock then current cost price
                    if($currentStock<=0) $costPrice = $itemModel->costPrice;
                    else
                    {
                        $stockModel = array();
                        // general stock model
                        $stockModel = Stock::model()->findAll(array('condition'=>'branchId=:branchId AND itemId=:itemId AND status=:status',
                            'params'=>array(':branchId'=>Yii::app()->session['branchId'],':itemId'=>$itemModel->id,':status'=>Stock::STATUS_APPROVED),
                            'order'=>'stockDate DESC'));
                        // stock transfer model
                        if (empty($stockModel))
                        {
                            $stockModel = StockTransfer::model()->findAll(array('condition'=>'toBranchId=:toBranchId AND itemId=:itemId',
                                'params'=>array(':toBranchId'=>Yii::app()->session['branchId'],':itemId'=>$itemModel->id),
                                'order'=>'stockDate DESC'));
                        }
                        if (!empty($stockModel))
                        {
                            foreach ($stockModel as $sKey => $sData) :
                                $stockSum += $sData->qty;
                                if (($currentStock - $stockSum) < 1) // 1 = default sold quantity
                                {
                                    $costPrice = $sData->costPrice;
                                    break;
                                }
                            endforeach;
                        }
                        else $costPrice = $itemModel->costPrice;
                    }

                    // assign values to ajax response
                    $itemArr['costPrice'] = $costPrice;
                    $itemArr['sellPrice'] = $itemModel->sellPrice;
                    $itemArr['isWeighted'] = $itemModel->isWeighted;
                    $itemArr['weight'] = $weight;
                    //$itemArr['taxPercent'] = $itemModel->tax->taxRate;
                    $itemArr['currentStock'] = $currentStock;

                    // offer or discount by durations
                    $sqlOffer = "SELECT * FROM pos_poffers WHERE itemId=".$itemModel->id." AND branchId=".Yii::app()->session['branchId']." AND startDate<='".$date."' AND endDate>='".$date."'";
                    $offerModel = Poffers::model()->findBySql($sqlOffer);
                    if(!empty($offerModel))
                        $itemArr['discountPercent'] = $offerModel->package->discount;
                    else $itemArr['discountPercent'] = '0';
                    echo CJSON::encode($itemArr);
                else :
                    echo CJSON::encode('invalid');
                endif;
            }
            else echo CJSON::encode('null');
        }
    }

    // item fifo wise costPrice quantity variations
    public function actionItemFifoCostPrice()
    {
        if(isset($_POST['itemId'])) $itemId = $_POST['itemId'];
        if(isset($_POST['salesQty'])) $salesQty = $_POST['salesQty'];
        $itemModel = Items::model()->findByPk($itemId);

        // finding item actual cost price FIFO (N.B : if negative stock then get current stock price) rules = currentStock-grnQtySumByDesc < sellQty
        $costPrice = $currentStock = $stockSum = 0;
        $currentStock = Stock::getItemWiseStock($itemId);

        // negative stock then current cost price
        if($currentStock<=0) $costPrice = $itemModel->costPrice;
        else
        {
            // general stock model
            $stockModel = array();
            $stockModel = Stock::model()->findAll(array('condition'=>'branchId=:branchId AND itemId=:itemId AND status=:status',
                                                        'params'=>array(':branchId'=>Yii::app()->session['branchId'],':itemId'=>$itemId,':status'=>Stock::STATUS_APPROVED),
                                                        'order'=>'stockDate DESC'));
            // stock transfer model
            if (empty($stockModel))
            {
                $stockModel = StockTransfer::model()->findAll(array('condition'=>'toBranchId=:toBranchId AND itemId=:itemId AND status=:status',
                                                                    'params'=>array(':toBranchId'=>Yii::app()->session['branchId'],':itemId'=>$itemId,':status'=>StockTransfer::STATUS_ACTIVE),
                                                                    'order'=>'stockDate DESC'));
            }
            if(!empty($stockModel))
            {
                foreach ($stockModel as $sKey => $sData) :
                    $stockSum += $sData->qty;
                    if (($currentStock - $stockSum) < $salesQty) {
                        $costPrice = $sData->costPrice;
                        break;
                    }
                endforeach;
            }
            else $costPrice = $itemModel->costPrice;
        }
        echo CJSON::encode($costPrice);
    }
	
	// validate Product By Code
	public function actionProductByCodeBarcode() 
	{

		if(isset($_POST['code']))
		{
			$itemArr = array();
			$itemCode = $_POST['code'];
			
			if($itemCode!="" || $itemCode!=0) 
			{
				$date = date("Y-m-d");
				$itemModel = Items::model()->find('itemCode like :itemCode',array(':itemCode'=>"%$itemCode%"));

               // $todayGrn = GoodReceiveNote::model()->findAll('grnDate like :grnDate and status like :status',array(':grnDate'=>"$today%", ':status'=>GoodReceiveNote::STATUS_ACTIVE));
                if (empty($itemModel)){
                    $variationsModel= ItemsVariation::model()->find('item_code like :item_code',array(':item_code'=>"%$itemCode%"));
                }

				if(!empty($itemModel)) :

					// checked barcode image exists or not
					if(is_file(Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$itemModel->barCode.".png"))
					{
						$itemArr['itemId'] = $itemModel->id;
						$itemArr['itemCode'] = $itemModel->itemCode;
						$itemArr['itemName'] = $itemModel->itemName;
						$itemArr['costPrice'] = $itemModel->costPrice;
						$itemArr['sellPrice'] = $itemModel->sellPrice;

						$itemArr['taxPercent'] = Items::getTaxRateById($itemModel->taxId);

						echo CJSON::encode($itemArr);
					}
					else echo CJSON::encode('invalid');
				elseif(!empty($variationsModel)) :
                    if(is_file(Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$variationsModel->bar_code.".png")) :
                        $itemArr['itemId'] = $variationsModel->id;
                        $itemArr['itemCode'] = $variationsModel->item_code;
                       $itemArr['itemName'] = 'Blank';
                        $itemArr['costPrice'] = $variationsModel->cost_price;
                        $itemArr['sellPrice'] = $variationsModel->sell_price;

                        echo CJSON::encode($itemArr);
                    endif;
                else:
					echo CJSON::encode('invalid');
				endif;
			}
			else echo CJSON::encode('null');
		}
	}
	
	// validate Product for inventory
	public function actionProductByValidInventory() 
	{
		if(isset($_POST['code']))
		{
			$itemArr = $invModel = array();
			$itemCode = $_POST['code'];
			
			if($itemCode!="" || $itemCode!=0) 
			{
				$date = date("Y-m-d");
				$itemModel = Items::model()->find('itemCode=:itemCode',array(':itemCode'=>$itemCode));
				if(!empty($itemModel)) :
				
					// checked inventory initialized or not
					$subdept = InvSettings::getAllActiveSubDept();
					if(!empty($subdept)) 
					{
						$sqlInv = "SELECT i.id FROM pos_items i, pos_category c WHERE i.catId=c.id AND c.subdeptId IN(".$subdept.") AND i.id=".$itemModel->id." AND i.status=".Items::STATUS_ACTIVE." ORDER BY i.id ASC";
						$invModel = Stock::model()->findBySql($sqlInv);
					}
					if(!empty($invModel))
					{
						$itemArr['itemId'] = $itemModel->id;
						$itemArr['itemCode'] = $itemModel->itemCode;
						$itemArr['itemName'] = $itemModel->itemName;
						$itemArr['costPrice'] = $itemModel->costPrice;
						$itemArr['sellPrice'] = $itemModel->sellPrice;
						$itemArr['isWeighted'] = $itemModel->isWeighted;
						$itemArr['taxPercent'] = $itemModel->tax->taxRate;
						
						echo CJSON::encode($itemArr);
					}
					else echo CJSON::encode('notinv');
				else : 
					echo CJSON::encode('invalid');
				endif;
			}
			else echo CJSON::encode('null');
		}
	}
	
	// get item code by item title
	public function actionItemCodeByTitle() 
	{
		if(isset($_POST['itemName']))
		{
			$itemName = $_POST['itemName'];
			
			if($itemName!="") 
			{
				$itemModel = Items::model()->find('itemName=:itemName',array(':itemName'=>$itemName));
				if(!empty($itemModel)) :
					echo $itemModel->itemCode;
				endif;
			}
		}
	}
	
	// get item stock available by Id
	public function actionCheckStockByItemId() 
	{
		if(isset($_POST['itemId']))
		{
			$itemId = $_POST['itemId'];
			if($itemId!="") 
				echo $stockAvail = Stock::getItemWiseStock($itemId);
		}
	}
	
	// customer info by id
	public function actionGetCustomerInfo() 
	{
		if(!empty($_POST['custId']))
		{
            if(!empty($_POST['type']) && $_POST['type']=='special') {
                $custModel = Customer::model()->find('phone=:phone',array(':phone'=>$_POST['custId']));
            } else{
                $custModel = Customer::model()->find('phone=:phone',array(':phone'=>$_POST['custId']));
            }

			if(!empty($custModel)) {
                $this->renderPartial('_customerInfo',array('custModel'=>$custModel,'ajax'=>true),false,true);
            }else{
			    $custModel = new Customer();
                $this->renderPartial('_customerInfo',array('custModel'=>$custModel,'ajax'=>true),false,true);
            }
			//else $this->renderPartial('_customerInfoBlank',array('ajax'=>true),false,true);
		}else{
            $custModel = new Customer();
            $this->renderPartial('_customerInfo',array('custModel'=>$custModel,'ajax'=>true),false,true);
        }
	}

    // coupon info by id
    public function actionGetCouponInfo()
    {
        if(isset($_POST['couponNo']))
        {
            $date = date('Y-m-d');
            $couponNo = $_POST['couponNo'];
            $sql = "SELECT couponAmount FROM `pos_coupons` c WHERE c.`couponStartDate`<='".$date."' AND c.`couponEndDate`>='".$date."' AND c.`status`=".Coupons::STATUS_ACTIVE."
                    AND c.`couponNo`='".$couponNo."' ORDER BY c.crAt DESC";
            $couponModel = Coupons::model()->findBySql($sql);
            if(!empty($couponModel)) echo $couponModel->couponAmount;
            else echo 'invalid';
        }
    }
	
	// damage/wastage edit order all products by po id 
	public function actionUpdateProductsBydwNo() 
	{
		if(isset($_POST['dwNo']))
		{
			$dwNo = $_POST['dwNo'];
			$dwNoModel = DamageWastage::model()->exists('dwNo=:dwNo',array(':dwNo'=>$dwNo));
			if(!empty($dwNoModel)) :
				$dwitemModel = DamageWastage::model()->findAll('dwNo=:dwNo',array(':dwNo'=>$dwNo));
				$this->renderPartial('_itemwiseDwUpdate',array('itemModel'=>$dwitemModel,'ajax'=>true),false,true); 
			else : 
				$this->renderPartial('_itemwiseDwUpdateBlank',array('ajax'=>true),false,true);
			endif;
		}
	}

    // temporary worker edit order all products by po id
    public function actionUpdateProductsBytwNo()
    {
        if(isset($_POST['twNo']))
        {
            $twNo = $_POST['twNo'];
            $twNoModel = TemporaryWorker::model()->find('twNo=:twNo AND rqty=:rqty',array(':twNo'=>$twNo,':rqty'=>0));
            if(!empty($twNoModel)) :
                $twItemModel = TemporaryWorker::model()->findAll('twNo=:twNo AND rqty=:rqty',array(':twNo'=>$twNo,':rqty'=>0));
                $this->renderPartial('_itemwiseTwUpdate',array('twNoModel'=>$twNoModel,'itemModel'=>$twItemModel,'ajax'=>true),false,true);
            else :
                $this->renderPartial('_itemwiseTwUpdateBlank',array('ajax'=>true),false,true);
            endif;
        }
    }

    // temporary worker receive order all products by po id
    public function actionReceiveProductsBytwNo()
    {
        if(isset($_POST['twNo']))
        {
            $twNo = $_POST['twNo'];
            $twNoModel = TemporaryWorker::model()->find('twNo=:twNo',array(':twNo'=>$twNo));
            if(!empty($twNoModel)) :
                $twItemModel = TemporaryWorker::model()->findAll('twNo=:twNo',array(':twNo'=>$twNo));
                $this->renderPartial('_itemwiseTwReceive',array('twNoModel'=>$twNoModel,'itemModel'=>$twItemModel,'ajax'=>true),false,true);
            else :
                $this->renderPartial('_itemwiseTwReceiveBlank',array('ajax'=>true),false,true);
            endif;
        }
    }

	// inventory edit by invNo
	public function actionInvUpdateByinvNo() 
	{
		if(isset($_POST['invNo']))
		{
			$invNo = $_POST['invNo'];
			$invNoModel = Inventory::model()->exists('invNo=:invNo and status=:status',array(':invNo'=>$invNo, ':status'=>Inventory::STATUS_INACTIVE));
			if(!empty($invNoModel)) :
				$invitemModel = Inventory::model()->findAll(array('condition'=>'invNo=:invNo AND status=:status',
																   'params'=>array(':invNo'=>$invNo,':status'=>Inventory::STATUS_INACTIVE),
																   'order'=>'itemId',
																   //'limit'=>'1'
															));
				$this->renderPartial('_itemwiseInvUpdate',array('itemModel'=>$invitemModel,'ajax'=>true),false,true); 
			else : 
				$this->renderPartial('_itemwiseInvUpdateBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// inventory approved by invNo
	public function actionInvApprovedByinvNo() 
	{
		if(isset($_POST['invNo']))
		{
			$invNo = $_POST['invNo'];
			$invNoModel = Inventory::model()->exists('invNo=:invNo and status=:status',array(':invNo'=>$invNo, ':status'=>Inventory::STATUS_INACTIVE));
			if(!empty($invNoModel)) :
				$invitemModel = Inventory::model()->findAll(array('condition'=>'invNo=:invNo AND status=:status',
																   'params'=>array(':invNo'=>$invNo,':status'=>Inventory::STATUS_INACTIVE),
																   'order'=>'itemId'
															));
				$this->renderPartial('_itemwiseInvApproved',array('itemModel'=>$invitemModel,'ajax'=>true),false,true); 
			else : 
				$this->renderPartial('_itemwiseInvApprovedBlank',array('ajax'=>true),false,true);
			endif;
		}
	}
	
	// sales return by sales invoice No including sp+instant discount
	public function actionSalesReturnByInvoice() 
	{
		if(isset($_POST['invNo']))
		{
			$invNo = $_POST['invNo'];
			$invModel = SalesInvoice::model()->find('invNo=:invNo',array(':invNo'=>$invNo));
			if(!empty($invModel)) :
				$srl = "SELECT * FROM pos_sales_details WHERE salesId=".$invModel->id." AND status !=".SalesDetails::STATUS_DAMAGE." AND itemId NOT IN(SELECT itemId FROM pos_sales_return WHERE salesId=".$invModel->id.")";
				//$itemModel = SalesDetails::model()->findAll('salesId=:salesId',array(':salesId'=>$invModel->id));
				$itemModel = SalesDetails::model()->findAllBySql($srl);

				$this->renderPartial('_salesReturnByInvoice',
					array('invModel'=>$invModel,'itemModel'=>$itemModel,'ajax'=>true),false,true);		
			else : 
				$this->renderPartial('_salesReturnByInvoiceBlank',array('ajax'=>true),false,true);
			endif;
		}
	}


	public function actionUploadVariationImage(){
	    if(!empty($_POST['request'])){
            $request = $_POST['request'];
            if($request == 2){

                $size = [
                    'small',
                    'medium',
                    'large'
                ];
                if($_SERVER['HTTP_HOST'] == '172.90.20.14'){
                    $path = $_POST['path'];
                    $pathExplode = explode('/pluspointretail/', $path);
                    $return_text = 0;

                    // Check file exist or not
                    if( file_exists($pathExplode[1]) ){

                        // Remove file
                        unlink($pathExplode[1]);

                        // Set status
                        $return_text = 1;
                    }else{

                        // Set status
                        $return_text = 0;
                    }

                    // Return status
                    echo $return_text;

                }else{
                    $path = $_POST['path'];
                    $mainPath = ltrim($path, '/');
                    $return_text = 0;

                    // Check file exist or not
                    if( file_exists($mainPath) ){

                        // Remove file
                        unlink($mainPath);

                        // Set status
                        $return_text = 1;
                    }else{

                        // Set status
                        $return_text = 0;
                    }

                    // Return status
                    echo $return_text;
                }
                exit();
            }
        }

	    $returnValue = [];
        if(isset($_FILES['ItemsVariation']['name'])) {
            $i = 0;
            foreach ($_FILES['ItemsVariation']['name'] as $img){
                if(is_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'])) {
                    $name = $img['image'];
                    $size = $_FILES["ItemsVariation"]["size"]["$i"]['image'];
                    $ext = explode(".", $name);
                    $allowed_ext = array("png", "jpg", "jpeg");
                    if(!empty($name)){
                        if(in_array($ext['1'], $allowed_ext))
                        {
                            if($size < (1024*10240))
                            {
                                // 73 x 100  Small Size
                                $new_image = '';
                                $new_name = $ext['0'] . '-small.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                $new_width=73;
                                $new_height = ($height/$width)*73;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);


                                // 290 x 400 Medium Size
                                $new_image = '';
                                $new_name = $ext['0'] . '-medium.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                $new_width=290;
                                $new_height = ($height/$width)*290;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);


                                // 1300 x 1800 Large Size
                                $new_image = '';
                                $new_name = $ext['0'] . '-large.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]["$i"]['image']);
                                }
                                $new_width=1300;
                                $new_height = ($height/$width)*1300;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);

                                $mainImage = $ext['0'].'.'.$ext['1'];
                                $returnValue['image']["$mainImage"] = Yii::app()->baseUrl.'/'.$path;
                                //echo '<img width="80px" src="'.$path.'" />';
                            }
                            else
                            {
                                $returnValue['warning'] = 'Image File size must be less than 8 MB';
                                //echo '<p style="color: red">Image File size must be less than 8 MB</p>';
                            }
                        }
                        else
                        {
                            $returnValue['invalid'] = 'Invalid Image File';
                            //echo '<p style="color: red">Invalid Image File</p>';
                        }
                    }
                }

                $i++;
            }

        }

        echo json_encode($returnValue);
        die();
    }
    // update order status by order id
    public function actionUpdateOrderStatusById()
    {
        if(!empty($_POST['orderId']) && !empty($_POST['orderStatus']))
        {
            // update invoice
            $sqlInvoice = "UPDATE pos_sales_invoice set status=".$_POST['orderStatus']." WHERE id=".$_POST['orderId'];
            Yii::app()->db->createCommand($sqlInvoice)->execute();

            // update invoice details
            $sqlInvoiceDetails = "UPDATE pos_sales_details set status=".$_POST['orderStatus']." WHERE salesId=".$_POST['orderId'];
            Yii::app()->db->createCommand($sqlInvoiceDetails)->execute();
            echo json_encode('success');
        }
    }

	// delete item image individual
	public function actionDeleteItemImageById() 
	{
		if(isset($_POST['id']) && !empty($_POST['id'])) 
			if(ItemImages::model()->findByPk($_POST['id'])->delete()) echo $_POST['id'];
	}

    // select invoices for sales special report
    public function actionSpecialSalesSelectSave()
    {
        if(isset($_POST['invoices']))
        {
            $invoices = $_POST['invoices'];
            $startDate = $_POST['startDate'];
            $endDate = $_POST['endDate'];

            $sqlSpecialSales = "SELECT * FROM pos_sales_special WHERE branchId=".Yii::app()->session['branchId']." AND startDate='".$startDate."' AND endDate='".$endDate."'
                                AND status=".SalesSpecial::STATUS_ACTIVE." ORDER BY crAt DESC";
            $model = SalesSpecial::model()->findBySql($sqlSpecialSales);
            if(empty($model)) $model = new SalesSpecial;
            $model->invoices = $invoices;
            $model->startDate = $startDate;
            $model->endDate = $endDate;
            if($model->save()) echo json_encode('success');
        }
    }
	
	// Items Auto complete generations
	public function actionAutoCompleteItems()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$itemName = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 10);
			$criteria = new CDbCriteria;					
			$criteria->condition='itemName LIKE :itemName';
			$criteria->params=array(':itemName' => "%$itemName%");
            $criteria->group = 'itemName';
			$criteria->order = 'id ASC';
			$custArray = Items::model()->findAll($criteria);
			
			if(!empty($custArray))
			{
				$returnVal = '';
				foreach($custArray as $data)
				{
					//$returnVal.= $data->getAttribute('custId').'|'.$data->getAttribute('id')."\n";
					$returnVal.= $data->getAttribute('itemName')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// Po Autocomplete generations
	public function actionAutoCompletePoNo()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$poNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='poNo LIKE :poNo AND branchId=:branchId AND status=:status';
			$criteria->params=array(':poNo' => "%$poNo%",':branchId'=>Yii::app()->SESSION['branchId'],':status'=>PurchaseOrder::STATUS_ACTIVE);
			$criteria->order = 'poDate desc';
			$poArray = PurchaseOrder::model()->findAll($criteria);
			
			if(!empty($poArray))
			{
				$returnVal = '';
				foreach($poArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('poNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// PR Autocomplete generations
	public function actionAutoCompletePrNo()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$prNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='prNo LIKE :prNo AND branchId=:branchId';
			$criteria->params=array(':prNo' => "%$prNo%",':branchId'=>Yii::app()->session['branchId']);	
			$criteria->group = 'prNo';
			$criteria->order = 'prDate desc';
			$poArray = PoReturns::model()->findAll($criteria);
			
			if(!empty($poArray))
			{
				$returnVal = '';
				foreach($poArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('prNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// GRN Autocomplete generations active only
	public function actionAutoCompleteGrnNo()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$grnNo = $_GET['q']; 
			
			/* this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='grnNo LIKE :grnNo and status like :status';
			$criteria->params=array(':grnNo' => "%$grnNo%", ':status'=>GoodReceiveNote::STATUS_ACTIVE);
			$criteria->order = 'grnDate desc';
			$grnArray = GoodReceiveNote::model()->findAll($criteria);*/
			
			$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND 
			        p.branchId=".Yii::app()->session['branchId']." AND n.grnNo LIKE '%".$grnNo."%'
					AND n.status=".GoodReceiveNote::STATUS_ACTIVE." ORDER BY n.grnDate DESC";
			$grnArray = GoodReceiveNote::model()->findAllBySql($sql);
			
			if(!empty($grnArray))
			{
				$returnVal = '';
				foreach($grnArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('grnNo')."\n";
				}
			}
			else $returnVal = "No processed GRN found";		
			echo $returnVal;
		}
	}
	
	// GRN Autocomplete generations approved
	public function actionAutoCompleteGrnNoApproved()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$grnNo = $_GET['q']; 
			
			/* this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='grnNo LIKE :grnNo and status like :status';
			$criteria->params=array(':grnNo' => "%$grnNo%", ':status'=>GoodReceiveNote::STATUS_APPROVED);
			$criteria->order = 'grnDate desc';
			$grnArray = GoodReceiveNote::model()->findAll($criteria);*/
			
			$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND 
			        p.branchId=".Yii::app()->session['branchId']." AND n.grnNo LIKE '%".$grnNo."%'
					AND n.status=".GoodReceiveNote::STATUS_APPROVED." ORDER BY n.grnDate DESC";
			$grnArray = GoodReceiveNote::model()->findAllBySql($sql);
			
			if(!empty($grnArray))
			{
				$returnVal = '';
				foreach($grnArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('grnNo')."\n";
				}
			}
			else $returnVal = "No Approved GRN found";		
			echo $returnVal;
		}
	}
	
	// GRN Autocomplete generations approved BY Supplier
	public function actionAutoCompleteGrnNoApprovedBySupplier()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$grnNo = $_GET['q']; 
			$suppId = $_GET['suppId'];
			
			/* this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='supplierId=:supplierId AND grnNo LIKE :grnNo AND status=:status';
			$criteria->params=array(':supplierId'=>$suppId,':grnNo' => "%$grnNo%", 
								    ':status'=>GoodReceiveNote::STATUS_APPROVED);
			$criteria->order = 'grnDate desc';
			$grnArray = GoodReceiveNote::model()->findAll($criteria);*/
			
			$sql = "SELECT n.* FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND 
			        p.branchId=".Yii::app()->session['branchId']." AND n.grnNo LIKE '%".$grnNo."%'
					AND n.supplierId=".$suppId." AND n.status=".GoodReceiveNote::STATUS_APPROVED." ORDER BY n.grnDate DESC";
			$grnArray = GoodReceiveNote::model()->findAllBySql($sql);
			
			
			if(!empty($grnArray))
			{
				$returnVal = '';
				foreach($grnArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('grnNo')."\n";
				}
			}
			else $returnVal = "No Approved GRN found";		
			echo $returnVal;
		}
	}
	
	// sales InvoiceNo Autocomplete generations
	public function actionAutoCompleteSalesInvoice()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$invNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='invNo LIKE :invNo AND branchId=:branchId';
			$criteria->params=array(':invNo' => "%$invNo%",':branchId'=>Yii::app()->session['branchId']);		
			$criteria->order = 'orderDate desc';
			$poArray = SalesInvoice::model()->findAll($criteria);
			
			/*$sql = "SELECT id,invNo FROM pos_sales_invoice WHERE id NOT IN(SELECT salesId FROM pos_sales_return) AND invNo LIKE '%".$invNo."%' AND branchId=".Yii::app()->session['branchId']." 
					GROUP BY id ORDER BY orderDate desc";
			$poArray = SalesInvoice::model()->findAllBySql($sql);*/

			if(!empty($poArray))
			{
				$returnVal = '';
				foreach($poArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('invNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// Customer Autocomplete generations
	public function actionAutoCompleteCustomer()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$custId = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 10);
			$criteria = new CDbCriteria;					
			$criteria->condition='phone LIKE :phone';
			$criteria->params=array(':phone' => "%$custId%");
			$criteria->order = 'id ASC';
			$custArray = Customer::model()->findAll($criteria);
			
			if(!empty($custArray))
			{
				$returnVal = '';
				foreach($custArray as $data)
				{
					//$returnVal.= $data->getAttribute('custId').'|'.$data->getAttribute('id')."\n";
					$returnVal.= $data->getAttribute('phone')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}

	// Customer Autocomplete generations special due invoice
	public function actionAutoCompleteCustomerSpecial()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$custId = $_GET['q'];
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 10);
			$criteria = new CDbCriteria;
			$criteria->condition='phone LIKE :phone OR title LIKE :title OR name LIKE :name';
			$criteria->params=array(':phone' => "%$custId%",':title' => "%$custId%",':name' => "%$custId%");
			$criteria->order = 'id ASC';
			$custArray = Customer::model()->findAll($criteria);

			if(!empty($custArray))
			{
				$returnVal = '';
				foreach($custArray as $data)
				{
					//$returnVal.= $data->getAttribute('name').'|'.$data->getAttribute('id')."\n";
					//$returnVal.= !empty($data->getAttribute('title'))?$data->getAttribute('title').'. '.$data->getAttribute('name').'('.$data->getAttribute('custId').")\n":$data->getAttribute('name').'('.$data->getAttribute('custId').")\n";
                    $returnVal.= $data->getAttribute('phone')."\n";
				}
			}
			else $returnVal = "No record found";
			echo $returnVal;
		}
	}
	
	// Customer Autocomplete institutional
	public function actionAutoCompleteCustomerInstitutional()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$custId = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 10);
			$criteria = new CDbCriteria;					
			$criteria->condition='custId LIKE :custId and custType LIKE :custType';
			$criteria->params=array(':custId' => "%$custId%", ':custType' => Customer::CUST_INSTITUTIONAL);	
			$criteria->order = 'id ASC';
			$custArray = Customer::model()->findAll($criteria);
			
			if(!empty($custArray))
			{
				$returnVal = '';
				foreach($custArray as $data)
				{
					//$returnVal.= $data->getAttribute('custId').'|'.$data->getAttribute('id')."\n";
					$returnVal.= $data->getAttribute('custId')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}

	// damage/wastage no Autocomplete generations
	public function actionAutoCompletedwNo()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$dwNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='dwNo LIKE :dwNo and branchId=:branchId';
			$criteria->params=array(':dwNo' => "%$dwNo%",':branchId'=>Yii::app()->session['branchId']);	
			$criteria->group = 'dwNo';
			$criteria->order = 'damDate DESC';
			$poArray = DamageWastage::model()->findAll($criteria);
			
			if(!empty($poArray))
			{
				$returnVal = '';
				foreach($poArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('dwNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}

    // temporary worker no Autocomplete generations
    public function actionAutoCompletetwNo()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $twNo = $_GET['q'];
            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->condition='twNo LIKE :twNo and branchId=:branchId';
            $criteria->params=array(':twNo' => "%$twNo%",':branchId'=>Yii::app()->session['branchId']);
            $criteria->group = 'twNo';
            $criteria->order = 'twDate DESC';
            $poArray = TemporaryWorker::model()->findAll($criteria);

            if(!empty($poArray))
            {
                $returnVal = '';
                foreach($poArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('twNo')."\n";
                }
            }
            else $returnVal = "No record found";
            echo $returnVal;
        }
    }
	
	// inventory for entry suggession
	public function actionAutoCompleteinvNoEntry()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$invNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='invNo LIKE :invNo and status=:status and branchId=:branchId';
			$criteria->params=array(':invNo' => "%$invNo%",':status'=>Inventory::STATUS_ACTIVE,
								    ':branchId'=>Yii::app()->session['branchId']);	
			$criteria->group = 'invNo';
			$criteria->order = 'crAt desc';
			$invArray = InvSettings::model()->findAll($criteria);
			
			if(!empty($invArray))
			{
				$returnVal = '';
				foreach($invArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('invNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// inventory for approval suggession
	public function actionAutoCompleteinvNo()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$invNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='invNo LIKE :invNo and status=:status and branchId=:branchId';
			$criteria->params=array(':invNo' => "%$invNo%",':status'=>Inventory::STATUS_INACTIVE,
								    ':branchId'=>Yii::app()->session['branchId']);	
			$criteria->group = 'invNo';
			$criteria->order = 'invDate desc';
			$invArray = Inventory::model()->findAll($criteria);
			
			if(!empty($invArray))
			{
				$returnVal = '';
				foreach($invArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('invNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}
	
	// inventory for report suggession
	public function actionAutoCompleteinvNoClosed()
	{
		if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
		{
			$invNo = $_GET['q']; 
			// this was set with the "max" attribute of the CAutoComplete widget
			$limit = min($_GET['limit'], 50);
			$criteria = new CDbCriteria;					
			$criteria->condition='invNo LIKE :invNo and status=:status and branchId=:branchId';
			$criteria->params=array(':invNo' => "%$invNo%",':status'=>Inventory::STATUS_CLOSE,
								    ':branchId'=>Yii::app()->session['branchId']);	
			$criteria->group = 'invNo';
			$criteria->order = 'invDate desc';
			$invArray = Inventory::model()->findAll($criteria);
			
			if(!empty($invArray))
			{
				$returnVal = '';
				foreach($invArray as $data)
				{
					//$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
					$returnVal .= $data->getAttribute('invNo')."\n";
				}
			}
			else $returnVal = "No record found";		
			echo $returnVal;
		}
	}

    // bank account name auto suggest
    public function actionAutoCompleteBankAccountsName()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $name = $_GET['q'];
            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->condition='name LIKE :name and status=:status';
            $criteria->params=array(':name' => "%$name%",':status'=>Bank::STATUS_ACTIVE);
            $criteria->group = 'name';
            $criteria->order = 'name';
            $invArray = Bank::model()->findAll($criteria);

            if(!empty($invArray))
            {
                $returnVal = '';
                foreach($invArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('name')."\n";
                }
            }
            else $returnVal = "No record found";
            echo $returnVal;
        }
    }

    // bank account branch auto suggest
    public function actionAutoCompleteBankBranchName()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $branch = $_GET['q'];
            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->condition='branch LIKE :branch and status=:status';
            $criteria->params=array(':branch' => "%$branch%",':status'=>Bank::STATUS_ACTIVE);
            $criteria->group = 'branch';
            $criteria->order = 'branch';
            $invArray = Bank::model()->findAll($criteria);

            if(!empty($invArray))
            {
                $returnVal = '';
                foreach($invArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('branch')."\n";
                }
            }
            else $returnVal = "No record found";
            echo $returnVal;
        }
    }
    public function actionAutoCompleteBankAccountsNo()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['bankId']))
        {
            $q = $_GET['q'];
            $bankId = $_GET['bankId'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->condition='name=:name and accountNo LIKE :accountNo and status=:status';
            $criteria->params=array(':name' =>$bankId,':accountNo' => "%$q%",':status'=>Bank::STATUS_ACTIVE);
            $criteria->group = 'accountNo';
            $criteria->order = 'accountNo';
            $invArray = Bank::model()->findAll($criteria);

            if(!empty($invArray))
            {
                $returnVal = '';
                foreach($invArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('accountNo')."\n";
                }
            }
            else $returnVal = "No record found";
            echo $returnVal;
        }
    }

    // temporary Worker branch auto suggest
    public function actionAutoCompleteTwNoByWorker()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $grnNo = $_GET['q'];
            $wkId = $_GET['wkId'];
            $wkArray = TemporaryWorker::model()->findAll(array('condition'=>'wkId=:wkId',
                                                                'params'=>array(':wkId'=>(int) $wkId),
                                                                'group'=>'twNo',
                        ));
            if(!empty($wkArray))
            {
                $returnVal = '';
                foreach($wkArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('twNo')."\n";
                }
            }
            else $returnVal = "No Temporary Worker Number Found";
            echo $returnVal;
        }
    }

    // auto complete valid coupon
    public function actionAutoCompleteCoupon()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $date = date('Y-m-d');
            $couponNo = $_GET['q'];
            $sql = "SELECT * FROM `pos_coupons` c WHERE c.`couponStartDate`<='".$date."' AND c.`couponEndDate`>='".$date."' AND c.`status`=".Coupons::STATUS_ACTIVE."
                    AND c.`couponNo` LIKE '%".$couponNo."%' ORDER BY c.crAt DESC";
            $couponArray = Coupons::model()->findAllBySql($sql);

            if(!empty($couponArray))
            {
                $returnVal = '';
                foreach($couponArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('couponNo')."\n";
                }
            }
            else $returnVal = "No active coupon found";
            echo $returnVal;
        }
    }

    public function actionAutoCompleteStockTransfer()
    {
        if(Yii::app()->request->isAjaxRequest && isset($_GET['q']))
        {
            $invNo = $_GET['q'];
            $toBranchId = $_GET['branchTo'];

            // this was set with the "max" attribute of the CAutoComplete widget
            $limit = min($_GET['limit'], 50);
            $criteria = new CDbCriteria;
            $criteria->condition='transferNo LIKE :transferNo AND toBranchId=:toBranchId';
            $criteria->params=array(':transferNo' => "%$invNo%",':toBranchId'=>$toBranchId);
            $criteria->order = 'id desc';

            $poArray = StockTransfer::model()->findAll($criteria);

            if(!empty($poArray))
            {
                $returnVal = '';
                foreach($poArray as $data)
                {
                    //$returnVal.= $userAccount->getAttribute('poNo').'|'.$userAccount->getAttribute('id')."\n";
                    $returnVal .= $data->getAttribute('transferNo')."\n";
                }
            }
            else $returnVal = "No record found";
            echo $returnVal;
        }
    }
	
}