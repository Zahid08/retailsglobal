<?php
class OrgUsertypeActionsController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$userTypeList = UserType::getUserType(UserType::STATUS_ACTIVE);
		echo $this->render('create', array('userTypeList'=>$userTypeList));		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=OrgUsertypeActions::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='org-usertype-actions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionDynamicusertype() {
  
		$data = OrgUsertype::getUserType($_POST['orgId']);
		echo CHtml::tag('option', array('value'=>""),CHtml::encode("Select a User Type"),true);
		foreach($data as $value=>$usertype)
			{
				echo CHtml::tag('option', array('value'=>$value),CHtml::encode($usertype),true);
			}		
	}
	public function actionDynamicuser() {
  
		$data = User::getuserbyorgut($_POST['usertypeId'],$_POST['orgId']);
		echo CHtml::tag('option', array('value'=>""),CHtml::encode("Select a User"),true);
		echo CHtml::tag('option', array('value'=>"all"),CHtml::encode("All"),true);
		foreach($data as $value=>$user)
			{
				echo CHtml::tag('option', array('value'=>$value),CHtml::encode($user),true);
			}		
	}
   	
	// user roll permission     
	public function actionDynamicactions() {  			
		if(isset($_POST['whattodo'])){
			if($_POST['whattodo']=='changeUserType'){
				if(isset($_POST['id']) and $_POST['id']!="" and isset($_POST['module']) and $_POST['module']!=""){
					echo $this->renderPartial('_form', array('selectedUserType'=>$_POST['id'],'module'=>$_POST['module'])); 
				}
			}
			elseif($_POST['whattodo']=='changeUserTypetaskwise'){
				if(isset($_POST['id']) and $_POST['id']!="" and isset($_POST['module']) and $_POST['module']!=""){
					echo $this->renderPartial('_formTask', array('selectedUserType'=>$_POST['id'],'module'=>$_POST['module'])); 
				}
			}
			elseif($_POST['whattodo']=='changeActionPermision'){
				
				if(isset($_POST['usertypeid']) and $_POST['usertypeid']!="" and isset($_POST['actionid']) and $_POST['actionid']!=""){
					$exists = OrgUsertypeActions::model()->find('usertypeId=:usertypeId and contActionsId=:contActionsId',array(':usertypeId'=>$_POST['usertypeid'],':contActionsId'=>$_POST['actionid']));	
					if(!empty($exists)){
						//echo Yii::app()->db->tablePrefix;
						 
						if($_POST['task']=='true'){
							$exists->status = OrgUsertypeActions::STATUS_ACTIVE;
						}
						else{
							$exists->status = OrgUsertypeActions::STATUS_INACTIVE;
							
						}
						$m = $exists->update(array('status'));
						if($m)
							echo "done";
						else
							echo "Not";
					}
					else{
						$cmodel=new OrgUsertypeActions;
						$cmodel->usertypeId = $_POST['usertypeid'];
						$cmodel->contActionsId = $_POST['actionid'];
						$cmodel->status = OrgUsertypeActions::STATUS_ACTIVE;				
						if($cmodel->save())
							echo "done";
						else
							echo "Not";
					}
				}
			}
			elseif($_POST['whattodo']=='changeTaskPermision'){
				
				if(isset($_POST['usertypeid']) and $_POST['usertypeid']!="" and isset($_POST['taskid']) and $_POST['taskid']!=""){
					
					$taskModel = Task::model()->findByPk($_POST['taskid']);
					if(!empty($taskModel->tasks)){
						foreach($taskModel->tasks as $taskactions){
							$exists = OrgUsertypeActions::model()->find('usertypeId=:usertypeId and contActionsId=:contActionsId',array(':usertypeId'=>$_POST['usertypeid'],':contActionsId'=>$taskactions->actionId));	
							if(!empty($exists)){
								
								if($_POST['task']=='true'){
									$exists->status = OrgUsertypeActions::STATUS_ACTIVE;
								}
								else{
									$exists->status = OrgUsertypeActions::STATUS_INACTIVE;
									
								}
								$m = $exists->update(array('status'));
								if($m)
									echo "done";
								else
									echo "Not";
							}
							else{
								$cmodel=new OrgUsertypeActions;
								$cmodel->usertypeId = $_POST['usertypeid'];
								$cmodel->contActionsId = $taskactions->actionId;
								$cmodel->status = OrgUsertypeActions::STATUS_ACTIVE;				
								if($cmodel->save())
									echo "done";
								else
									echo "Not";
							}
						}
					}
				}
			} 
			elseif($_POST['whattodo']=='changeAllTaskPermision'){
				
				if(isset($_POST['usertypeid']) and $_POST['usertypeid']!="" and isset($_POST['modulename']) and $_POST['modulename']!=""){
					$task = $_POST['task'];
					$taskList = Task::getTaskListByModule(Task::STATUS_ACTIVE,$_POST['modulename']);
					
					foreach($taskList as $taskModel){
						//$taskModel = Task::model()->findByPk($_POST['taskid']);
						if(!empty($taskModel->tasks)){
							foreach($taskModel->tasks as $taskactions){
								$exists = OrgUsertypeActions::model()->find('usertypeId=:usertypeId and contActionsId=:contActionsId',array(':usertypeId'=>$_POST['usertypeid'],':contActionsId'=>$taskactions->actionId));	
								if(!empty($exists)){
									
									if($_POST['task']=='true'){
										$exists->status = OrgUsertypeActions::STATUS_ACTIVE;
									}
									else{
										$exists->status = OrgUsertypeActions::STATUS_INACTIVE;
										
									}
									$m = $exists->update(array('status'));
									if($m)
										echo "done";
									else
										echo "Not";
								}
								else{
									$cmodel=new OrgUsertypeActions;
									$cmodel->usertypeId = $_POST['usertypeid'];
									$cmodel->contActionsId = $taskactions->actionId;
									$cmodel->status = OrgUsertypeActions::STATUS_ACTIVE;				
									if($cmodel->save())
										echo "done";
									else
										echo "Not";
								}
							}
						}
					}
				}
			}
			elseif($_POST['whattodo']=='changeControllerPermision'){
				
				if(isset($_POST['usertypeid']) and $_POST['usertypeid']!="" and isset($_POST['contid']) and $_POST['contid']!=""){
					$task = $_POST['task'];
					foreach(ContActions::getactionsByController(1, $_POST['contid']) as $actionid=>$actionname){
					
					$exists = OrgUsertypeActions::model()->find('usertypeId=:usertypeId and contActionsId=:contActionsId',array(':usertypeId'=>$_POST['usertypeid'],':contActionsId'=>$actionid));	
					if(!empty($exists)){
							if($_POST['task']=='true'){
								$exists->status = OrgUsertypeActions::STATUS_ACTIVE;
							}
							else{
								$exists->status = OrgUsertypeActions::STATUS_INACTIVE;
								
							}
							
							$m = $exists->save();
							if($m)
								echo $task."done  ";
							else
								echo "Not";
							unset($exists);
						}
						else{
							$cmodel=new OrgUsertypeActions;
							$cmodel->usertypeId = $_POST['usertypeid'];
							$cmodel->contActionsId = $actionid;
							$cmodel->status = OrgUsertypeActions::STATUS_ACTIVE;				
							if($cmodel->save())
								echo "done";
							else
								echo "Not";
						}
					}
					
				}
			}
		}
	}            
}
