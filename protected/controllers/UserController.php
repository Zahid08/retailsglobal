<?php
class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//array('ext.yiibooster.filters.BootstrapFilter - delete')
		);
	}
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionDashboard()
	{
		$this->metaTitle = 'Dashboard';
        $modelCustomerPending = Customer::model()->findAll(array('condition'=>'status = :status',
                                                                'params'=>array(':status' => Customer::STATUS_INACTIVE),
                                                                'order'=>'crAt DESC','limit'=>10,
                                                            ));
        $modelOrderPending = SalesInvoice::model()->findAll(array('condition'=>'status=:status AND isEcommerce=:isEcommerce',
                                                                'params'=>array(':status' => SalesInvoice::STATUS_PROCESSING,':isEcommerce'=>SalesInvoice::IS_ECOMMERCE),
                                                                'order'=>'crAt DESC','limit'=>10,
                                                            ));
		$this->render('dashboard',array(
            'modelCustomerPending'=>$modelCustomerPending,
            'modelOrderPending'=>$modelOrderPending,
        ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{		
		$msg = '';
		$model = new User;	
		$model->isSupplier = User::IS_NOT_SUPPLIER;	
		$userTypeArr = array();
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
		
			if($model->validate()) 
			{				
				if(!isset($_POST['userTypeId']) && empty($_POST['userTypeId'])) $msg = '<div class="notification note-error"><p>Please select one or more user role !</p></div>';
			    
				$salt = uniqid('',true);
				$model->password = $model->hashPassword($model->password,$salt);
				$model->salt = $salt;
                $model->generateAuthKey();
				$model->conf_password = $model->hashPassword($model->conf_password,$salt);
				$model->totalLoginTime = 0;
				$model->status = User::STATUS_ACTIVE;
				
				if($msg=="" || empty($msg))
				{
					if($model->save())
					{
						foreach($_POST['userTypeId'] as $ur)
						{
							$modelr = new Roles;
							$modelr->userTypeId = $ur;
							$modelr->userId = $model->id;
							$modelr->status = Roles::STATUS_ACTIVE;
							$modelr->save();
						}						
						$msg = '<div class="notification note-success"><p>User Added Successfully</p></div>';
					}
				}
			}
		}

		$this->render('_form',array(
			'model'=>$model,'userTypeArr'=>$userTypeArr,
			'msg'=>$msg,
		));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$msg = '';
		$model=$this->loadModel($id);
		if(!empty($model->supplierId) && $model->supplierId>0) $model->isSupplier = User::IS_SUPPLIER;
		else $model->isSupplier = User::IS_NOT_SUPPLIER;
		
		// select userType by user
		$userTypeArr = array();
		$userTypeArr = CHtml::listData(Roles::model()->findAll('userId=:userId',array(':userId'=>$id)), 'userTypeId', 'userTypeId');
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->validate()) 
			{
				if(!isset($_POST['userTypeId']) && empty($_POST['userTypeId'])) $msg = '<div class="notification note-error"><p>Please select one or more user role !</p></div>';
				
				$salt = uniqid('',true);		
				$model->password = $model->hashPassword($model->password,$salt);
				$model->salt = $salt;
				$model->conf_password = $model->hashPassword($model->conf_password,$salt);
				
				if($msg=="" || empty($msg))
				{
					if($model->save())
					{
						foreach($_POST['userTypeId'] as $ur)
						{
							if(!in_array($ur,$userTypeArr)) 
							{
								$modelr = new Roles;
								$modelr->userTypeId = $ur;
								$modelr->userId = $model->id;
								$modelr->status = Roles::STATUS_ACTIVE;
								$modelr->save();
							}
						}		
							
						// delete if roles deselected
						foreach($userTypeArr as $cr)
						{
							if(!in_array($cr,$_POST['userTypeId'])) 
							{
								$rolesPk = Roles::model()->find(array('condition'=>'userTypeId=:userTypeId and userId=:userId',
																	  'params'=>array(':userTypeId'=>$cr,':userId'=>$id)));
								if(!empty($rolesPk)) $rolesPk->delete();
							}
						}				
						$msg = '<div class="notification note-success"><p>User Updated Successfully</p></div>';
					}
				}
			}
		}

		$this->render('_form',array(
			'model'=>$model,'userTypeArr'=>$userTypeArr,
			'msg'=>$msg,
		));
	}
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionChangepassword()
	{
		$msg = '';
		$form2 = new UserChangePassword;
    	$find = $this->loadModel(Yii::app()->user->id);   	
    	$form2->oldpass = $find->password;
    	
    	if(isset($find)) 
		{
    		if(isset($_POST['UserChangePassword']))
			{
				$form2->attributes=$_POST['UserChangePassword'];
				$vopass = $form2->verifyoldpass = md5($find->salt.$form2->verifyoldpass);
				
				if($form2->validate()) 
				{
					$salt = uniqid('',true);
					$find->password = $find->hashPassword($form2->password,$salt);
					$find->salt = $salt;	
					User::model()->updateByPk($find->id,array('password'=>$find->password,'salt'=>$find->salt));		
					//$this->redirect(array('view','id'=>$find->id));
					$msg = "Password Changed Successfully";
				}
				else{
					$form2->verifyoldpass = "";
					$form2->password = "";
					$form2->verifyPassword = "";
				}
			} 
    	} 	
		$this->render('changepassword',array('form'=>$form2,'msg'=>$msg));	    	
	}
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('User');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		// page size drop down changed
		if (isset($_GET['pageSize'])) {
		//
		// pageSize will be set on user's state
		Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
		//
		// unset the parameter as it
		// would interfere with pager
		// and repetitive page size change
		unset($_GET['pageSize']);
		}
		
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	public function actionApproveduser()
	{
		$whattodo =  $_POST['whattodo'];
		if($whattodo=="getForm"){
			$id = $_POST['id'];

			$whattodo = '"t"';
			$prevalue = User::model()->findByPk($id);
			
			$str = "<div class='form'  style='min-height:90px;'>";
				$str .= "<form id='update-description' action='' method='post' style='width:90%; '>";
					$str .= "<FIELDSET class='fieldset'>";
						$str .= "<LEGEND class='legend'>Update Information</LEGEND>";
						$str .= "<div class='row'>";
							$str .= "<label for='contDesc' style='width:100%; float:left; text-align:left; '>Status</label>";
							$str .= "<select id='status' name='status' style='width:350px'>";
								if($prevalue->status==1) 
									$str .= "<option value='1' selected='selected' >Active</option>";
								else	
									$str .= "<option value='1'>Active</option>";
								if($prevalue->status==2) 	
									$str .= "<option value='2' selected='selected'>Pending</option>";	
								else
									$str .= "<option value='2'>Pending</option>";	
								if($prevalue->status==3)
									$str .= "<option value='3' selected='selected'>Disable</option>";	
								else 	
									$str .= "<option value='3'>Disable</option>";	
							$str .= "</select>";	
						$str .= "</div>";
	
						$str .= "<div class='row'>&nbsp;</div>";
					$str .= "</FIELDSET>";
					$str .= "<div class='row buttons'>";
						
						$str .= "<input type='button' name='updatebtnreq' value='Update' style='width:100px;' id='updatebtnreq' onClick='approveuser($id,".$whattodo.");'/>";	
						$str .= "<input type='button' name='updatebtnreq' value='Cancel' style='width:100px;' id='closewindow' onClick='approveuser($id,\"CloseWindow\");'/>";	
					$str .= "</div>";
				$str .= "</form>";
			$str .= "</div>";
	
			echo $str;
		}
		else{
			$id = $_POST['id'];
			$status = $_POST['status'];			
			$updateActions =  User::model()->updateByPk($id,array('status'=>$status));
			echo 'Successfully Update ';
		}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
