<?php
/*********************************************************
-*- File: ItemsController.php
-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
-*- Date: 2014.02.02
-*- Position:  protected/controller
-*- YII-*- version 1.1.13
/*********************************************************/

class ItemsController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/adminColumn', meaning
     * using two-column layout. See 'protected/views/layouts/adminColumn.php'.
     */
    public $metaTitle 	 	 = NULL;
    public $metaKeywords 	 = NULL;
    public $metaDescriptions = NULL;
    public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $msg = '';
        $model=new Items;
        $model->caseCount = Items::DEFAULT_CASE_COUNT;
        $model->barCode = '';
        $attTypeModel = AttributesType::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>AttributesType::STATUS_ACTIVE)));

        //$getSlugName = Items::model()->findAll(['']);

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);



        if(isset($_POST['Items']))
        {
            $model->attributes=$_POST['Items'];

            //$slug = strtolower($this->ItemNameSlug($model->itemName));

            $slug = $model::ItemsSlug($model->itemName);

            $model->slugName = $slug;

            $catModel = Category::model()->findByPk($model->catId);
            if(!empty($catModel)) $catName = $catModel->name;

            $newItemCode=$model->itemCode;
            if ($catName){
                $pieace = explode(" ", $catName);
                $newItemCode=$pieace[0].'-'.$model->itemCode;
            }

            $model->itemCode=$newItemCode;

            if($model->save())
            {
                if($_POST['Items']['variation_type'] == '1'){


                    // Purchase Order Save

                    $purchaseOrder = new PurchaseOrder;
                    $purchaseOrder->branchId = $model->brandId;
                    $purchaseOrder->supplierId = $model->supplierId;
                    $purchaseOrder->poNo = 'PO'.date("Ymdhis");
                    $purchaseOrder->totalQty = $model->item_quantity;
                    $purchaseOrder->totalPrice = $model->sellPrice;
                    $purchaseOrder->totalWeight = 0;
                    $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                    $purchaseOrder->save();

                    //PoDetails Save
                    $modelPoDetails = new PoDetails;
                    $modelPoDetails->poId = $purchaseOrder->id;
                    $modelPoDetails->qty = $model->item_quantity;
                    $modelPoDetails->itemId = $model->id;
                    $modelPoDetails->costPrice = $model->sellPrice;
                    $modelPoDetails->currentStock = 0;
                    $modelPoDetails->soldQty = 0;
                    $modelPoDetails->save();

                    // Good Received Saved
                    $modelGrn = new GoodReceiveNote;
                    $modelGrn->supplierId= $model->supplierId;
                    $modelGrn->poId = $purchaseOrder->id;
                    $modelGrn->grnNo = 'GRN'.date("Ymdhis");
                    $modelGrn->totalQty = $model->item_quantity;
                    $modelGrn->totalWeight = 0;
                    $modelGrn->totalPrice = $model->sellPrice;
                    $modelGrn->totalDiscount = 0;
                    $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                    $modelGrn->save();

                    // Stock Saved
                    $modelStock = new Stock;
                    $modelStock->grnId = $modelGrn->id;
                    $modelStock->qty = $model->item_quantity;
                    $modelStock->itemId = $model->id;
                    $modelStock->costPrice = $model->sellPrice;
                    $modelStock->discountPercent = 0;
                    $modelStock->status = Stock::STATUS_APPROVED;
                    $modelStock->save();



                }
                /*
                elseif ($_POST['Items']['variation_type'] == '2'){

                    // Save Items Variation
                    if(isset($_POST['ItemsVariation']) && !empty($_POST['ItemsVariation'])){
                        $images = $_FILES['ItemsVariation']['name'];
                        $imgresults = [];


                        $e = 0;
                        foreach ($_POST['ItemsVariation'] as $variation){
                            $imageexplode = explode('.', $images[$e]['image']);

                            if(!empty($images[$e]['image'])){
                                $variation['image'] = 'media/catalog/product/'.$imageexplode['0'].'-large.'.$imageexplode['1'];
                            }else{
                                $variation['image'] = 'media/catalog/product/blank-image.png';
                            }
                            $itemsvariation = new ItemsVariation();

                            $itemsvariation->attributes=$variation;

                            $itemsvariation->item_id = $model->id;
                            $itemsvariation->item_code = $_POST['Items']['itemCode'];
                            $itemsvariation->bar_code = $_POST['Items']['barCode'];
                            $itemsvariation->save();

                            $e++;
                        }

                    }
                }*/


                /*           $selfFind = Items::model()->findByPk($model->id);
                           if($isExist=='yes')
                                $selfFind->slugName = $slug.'-'.$model->id;
                           else
                               $selfFind->slugName = $slug;
                           $selfFind->save();*/

                // barcode generations : if a item is not weighted and barcode field blank
                if(empty($_POST['Items']['barCode']))
                {
                    // create directory if not exists
                    if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId']))
                    {
                        mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId'], 0777, true);
                    }

                    // barcode ean13
                    $uiniqueId = date("ymdhis").mt_rand(0,9);
                    $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                    $barcode = new Barcodeean128();
                    $barcode->draw($uiniqueId,$location,"png",true);

                    // update barcode field
                    $command = Yii::app()->db->createCommand();
                    $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$model->id));
                }

                // attributes processing
                $ptrAtt = 0;
                if(isset($_POST['attId']) && !empty($_POST['attId'])) :
                    foreach($_POST['attId'] as $key=>$data) :
                        $itemAttModel = new ItemAttributes;
                        $itemAttModel->attTypeId = $_POST['attType'][$key];
                        $itemAttModel->attId = $data;
                        $itemAttModel->itemId = $model->id;
                        if($itemAttModel->save()) $ptrAtt = 1;
                    endforeach;

                    // update IS_ECOMMERCE field on items
                    if($ptrAtt==1) Items::model()->updateByPk($model->id,array('isEcommerce'=>Items::IS_ECOMMERCE));
                endif;

                // image processing
                // image processing
                $uploadedFile = CUploadedFile::getInstancesByName('Items[image]');
                if (isset($uploadedFile) && count($uploadedFile) > 0) {
                    foreach ($uploadedFile as $imageKey => $expImage) {
                        $twoSegmentDir = FileHelper::createTwoDirSegmentFromImageName($expImage->name);
                        $relativeDirPath = 'catalog/product' . '/' . $twoSegmentDir;
                        $filePath = FileHelper::getUniqueFilenameWithPath($expImage->name, $relativeDirPath);
                        $fileName = FileHelper::getFilename($filePath);

                        $itemImagesModel = new ItemImages;
                        $itemImagesModel->itemId = $model->id;
                        $itemImagesModel->image = $twoSegmentDir . '/' . $fileName;

                        // Alt Text
                        $itemImagesModel->alt_text = isset($_POST['Items']['image']['alt_text'][$imageKey]) ? $_POST['Items']['image']['alt_text'][$imageKey] : null;

                        // Sort Order
                        $itemImagesModel->sort_order = isset($_POST['Items']['image']['sort_order'][$imageKey]) ? $_POST['Items']['image']['sort_order'][$imageKey] : null;

                        // Large Image
                        if(isset($_POST['Items']['image']['large_image']) && $_POST['Items']['image']['large_image'] == $imageKey){
                            $itemImagesModel->large_image = 1;
                        }

                        // Medium Image
                        if(isset($_POST['Items']['image']['medium_image']) && $_POST['Items']['image']['medium_image'] == $imageKey){
                            $itemImagesModel->medium_image = 1;
                        }

                        // Small Image
                        if(isset($_POST['Items']['image']['small_image']) && $_POST['Items']['image']['small_image'] == $imageKey){
                            $itemImagesModel->small_image = 1;
                        }

                        if($itemImagesModel->save()){
                            $expImage->saveAs($filePath);

                            $imageType = FileHelper::getFileExt($fileName);

                            // Small Image
                            $cacheImageSmall = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_SMALL);
                            ImageManager::thumbnail($filePath, $cacheImageSmall, UsefulFunction::IMAGE_SIZE_SMALL, $imageType, true);

                            // Medium Image
                            $cacheImageMedium = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_MEDIUM);
                            ImageManager::thumbnail($filePath, $cacheImageMedium, UsefulFunction::IMAGE_SIZE_MEDIUM, $imageType, true);

                            // Large Image
                            $cacheImageLarge = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_LARGE);
                            ImageManager::thumbnail($filePath, $cacheImageLarge, UsefulFunction::IMAGE_SIZE_LARGE, $imageType, true);
                        }
                    }
                    // update IS_ECOMMERCE field on items
                    Items::model()->updateByPk($model->id,array('isEcommerce'=>Items::IS_ECOMMERCE));
                }


                // parent items processing
                if(isset($_POST['qty']) && !empty($_POST['qty'])) :
                    $ptrParents = 0;
                    foreach($_POST['qty'] as $key=>$qty) :
                        if($qty>0) :
                            $modelParents = new ItemParents;
                            $modelParents->itemId = $model->id;
                            $modelParents->parentId = $_POST['proId'][$_POST['pk'][$key]];
                            $modelParents->qty = $qty;
                            if($modelParents->save()) $ptrParents = 1;
                        endif;
                    endforeach;
                    // update items status
                    if($ptrParents==1) Items::model()->updateByPk($model->id,array('isParent'=>Items::IS_PARENTS));
                endif;

                if($model->variation_type == 2){
                    $this->redirect(array('items/variation?item_id='.$model->id));
                }
                $msg = "Items Saved Successfully";
                $model=new Items;



            }
        }
        $itemsvariation = [new ItemsVariation()];
        $this->render('_form',array(
            'model'=>$model,'attTypeModel'=>$attTypeModel,
            'itemsvariation'=>$itemsvariation,
            'msg'=>$msg,
        ));
    }


    /*  protected function makeUnique($slug)
      {
          $uniqueSlug = $slug;
          $iteration = 0;
          while (!$this->validateSlug($uniqueSlug)) {
              $iteration++;
              $uniqueSlug = $this->generateUniqueSlug($slug, $iteration);
          }
          return $uniqueSlug;
      }*/

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {

        $msg = '';
        $model=$this->loadModel($id);
        $attTypeModel = AttributesType::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>AttributesType::STATUS_ACTIVE)));

        //$getSlugName = Items::model()->findAll();


        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        $oldSlug =  $model->slugName;


        if(isset($_POST['Items']))
        {

            $model->attributes=$_POST['Items'];

            $slug = $model::ItemsSlug($model->itemName, $oldSlug);
            $model->slugName = $slug;

            if($model->save())
            {


                if($_POST['Items']['variation_type'] == '1'){

                    // Stock Saved
                    $modelStock = Stock::model()->find(array('condition'=>'itemId=:itemId',
                        'params'=>array(':itemId'=>$model->id)));

                    if(!empty($modelStock)){
                        $modelStock->costPrice = $model->sellPrice;
                        $modelStock->discountPercent = 0;
                        $modelStock->qty = $model->item_quantity;
                        $modelStock->update();
                    }else{
                        // Purchase Order Save
                        $purchaseOrder = new PurchaseOrder;
                        $purchaseOrder->branchId = $model->brandId;
                        $purchaseOrder->supplierId = $model->supplierId;
                        $purchaseOrder->poNo = 'PO'.time();
                        $purchaseOrder->totalQty = $model->item_quantity;
                        $purchaseOrder->totalPrice = $model->sellPrice;
                        $purchaseOrder->totalWeight = 0;
                        $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                        $purchaseOrder->save();

                        //PoDetails Save
                        $modelPoDetails = new PoDetails;
                        $modelPoDetails->poId = $purchaseOrder->id;
                        $modelPoDetails->qty = $model->item_quantity;
                        $modelPoDetails->itemId = $model->id;
                        $modelPoDetails->costPrice = $model->sellPrice;
                        $modelPoDetails->currentStock = 0;
                        $modelPoDetails->soldQty = 0;
                        $modelPoDetails->save();

                        // Good Received Saved
                        $modelGrn = new GoodReceiveNote;
                        $modelGrn->supplierId= $model->supplierId;
                        $modelGrn->poId = $purchaseOrder->id;
                        $modelGrn->grnNo = 'GRN'.time();
                        $modelGrn->totalQty = $model->item_quantity;
                        $modelGrn->totalWeight = 0;
                        $modelGrn->totalPrice = $model->sellPrice;
                        $modelGrn->totalDiscount = 0;
                        $modelGrn->save();

                        // Stock Saved
                        $modelStock = new Stock;
                        $modelStock->grnId = $modelGrn->id;
                        $modelStock->qty = $model->item_quantity;
                        $modelStock->itemId = $model->id;
                        $modelStock->costPrice = $model->sellPrice;
                        $modelStock->discountPercent = 0;
                        $modelStock->save();


                    }


                    // Good Received Saved
                    $modelGrn = GoodReceiveNote::model()->find(array('condition'=>'id=:id',
                        'params'=>array(':id'=>$modelStock->grnId)));
                    if(!empty($modelGrn)){
                        $modelGrn->supplierId= $model->supplierId;
                        $modelGrn->totalQty = $model->item_quantity;
                        $modelGrn->totalWeight = 0;
                        $modelGrn->totalPrice = $model->sellPrice;
                        $modelGrn->totalDiscount = 0;
                        $modelGrn->update();

                    }

                    // Purchase Order Save
                    $purchaseOrder = PurchaseOrder::model()->find(array('condition'=>'id=:id',
                        'params'=>array(':id'=>$modelGrn->poId)));

                    if(!empty($purchaseOrder)){
                        //$purchaseOrder->branchId = $model->brandId;
                        $purchaseOrder->supplierId = $model->supplierId;
                        $purchaseOrder->totalQty = $model->item_quantity;
                        $purchaseOrder->totalPrice = $model->sellPrice;
                        $purchaseOrder->totalWeight = 0;
                        $purchaseOrder->update();
                    }


                    //PoDetails Save
                    $modelPoDetails = PoDetails::model()->find(array('condition'=>'poId=:poId',
                        'params'=>array(':poId'=>$modelGrn->poId)));
                    if(!empty($modelPoDetails)){
                        $modelPoDetails->qty = $model->item_quantity;
                        $modelPoDetails->itemId = $model->id;
                        $modelPoDetails->costPrice = $model->sellPrice;
                        $modelPoDetails->currentStock = 0;
                        $modelPoDetails->soldQty = 0;
                        $modelPoDetails->update();
                    }



                    ItemsVariation::model()->deleteAll(array('condition'=>'item_id=:item_id','params'=>array(':item_id'=>$model->id)));


                }
                /*
                elseif ($_POST['Items']['variation_type'] == '2'){

                    if(isset($_POST['ItemsVariation']) && !empty($_POST['ItemsVariation'])){

                        $images = $_FILES['ItemsVariation']['name'];

                        $imgresults = [];

                        $e = 0;
                        foreach ($_POST['ItemsVariation'] as $key => $variation){

                            if(!empty($variation['id'])){

                                $itemsvariation = ItemsVariation::model()->findByPk($variation['id']);

                                $imageexplode = explode('.', $images[$e]['image']);
                                if(!empty($images[$e]['image'])){
                                    $variation['image'] = 'media/catalog/product/'.$imageexplode['0'].'-large.'.$imageexplode['1'];
                                }else{
                                    $variation['image'] = $itemsvariation->image;
                                }



                                $itemsvariation->attributes=$variation;

                                $itemsvariation->item_id = $model->id;
                                $itemsvariation->item_code = $_POST['Items']['itemCode'];
                                $itemsvariation->bar_code = $_POST['Items']['barCode'];
                                $itemsvariation->update();

                            }else{
                                $itemsvariation = new ItemsVariation();

                                $imageexplode = explode('.', $images[$e]['image']);
                                if(!empty($images[$e]['image'])){
                                    $variation['image'] = 'media/catalog/product/'.$imageexplode['0'].'-large.'.$imageexplode['1'];
                                }else{
                                    $variation['image'] = $itemsvariation->image;
                                }

                                $itemsvariation->attributes=$variation;


                                $itemsvariation->item_id = $model->id;
                                $itemsvariation->item_code = $_POST['Items']['itemCode'];
                                $itemsvariation->bar_code = $_POST['Items']['barCode'];
                                $itemsvariation->save();
                            }
                            $e++;
                        }

                    }
                }*/


                // barcode generations : if a item is not weighted and barcode field blank
                if(empty($_POST['Items']['barCode']))
                {
                    // create directory if not exists
                    if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId']))
                    {
                        mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId'], 0777, true);
                    }

                    // barcode ean13
                    $uiniqueId = date("ymdhis").mt_rand(0,9);
                    $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                    $barcode = new Barcodeean128();
                    $barcode->draw($uiniqueId,$location,"png",true);

                    // update barcode field
                    $command = Yii::app()->db->createCommand();
                    $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$model->id));
                }

                // attributes processing
                $ptrAtt = 0;
                if(isset($_POST['attId']) && !empty($_POST['attId'])) :
                    foreach($_POST['attId'] as $key=>$data) :
                        if(!empty($_POST['attIdPrev'][$key]))
                        {
                            if(!empty($data)) ItemAttributes::model()->updateByPk($_POST['attIdPrev'][$key],array('attId'=>$data));
                            else ItemAttributes::model()->deleteByPk($_POST['attIdPrev'][$key]);
                        }
                        else
                        {
                            $itemAttModel = new ItemAttributes;
                            $itemAttModel->attTypeId = $_POST['attType'][$key];
                            $itemAttModel->attId = $data;
                            $itemAttModel->itemId = $model->id;
                            if($itemAttModel->save()) $ptrAtt = 1;
                        }
                    endforeach;
                    // update IS_ECOMMERCE field on items
                    if($ptrAtt==1)  Items::model()->updateByPk($model->id,array('isEcommerce'=>Items::IS_ECOMMERCE));
                endif;

                // image processing
                $uploadedFile = CUploadedFile::getInstancesByName('Items[image]');
                if (isset($uploadedFile) && count($uploadedFile) > 0) {
                    foreach ($uploadedFile as $imageKey => $expImage) {
                        $twoSegmentDir = FileHelper::createTwoDirSegmentFromImageName($expImage->name);
                        $relativeDirPath = 'catalog/product' . '/' . $twoSegmentDir;
                        $filePath = FileHelper::getUniqueFilenameWithPath($expImage->name, $relativeDirPath);
                        $fileName = FileHelper::getFilename($filePath);

                        $itemImagesModel = new ItemImages;
                        $itemImagesModel->itemId = $model->id;
                        $itemImagesModel->image = $twoSegmentDir . '/' . $fileName;

                        // Alt Text
                        $itemImagesModel->alt_text = isset($_POST['Items']['image']['alt_text'][$imageKey]) ? $_POST['Items']['image']['alt_text'][$imageKey] : null;

                        // Sort Order
                        $itemImagesModel->sort_order = isset($_POST['Items']['image']['sort_order'][$imageKey]) ? $_POST['Items']['image']['sort_order'][$imageKey] : null;

                        // Large Image
                        if(isset($_POST['Items']['image']['large_image']) && $_POST['Items']['image']['large_image'] == $imageKey){
                            $itemImagesModel->large_image = 1;
                        }

                        // Medium Image
                        if(isset($_POST['Items']['image']['medium_image']) && $_POST['Items']['image']['medium_image'] == $imageKey){
                            $itemImagesModel->medium_image = 1;
                        }

                        // Small Image
                        if(isset($_POST['Items']['image']['small_image']) && $_POST['Items']['image']['small_image'] == $imageKey){
                            $itemImagesModel->small_image = 1;
                        }

                        if($itemImagesModel->save()){
                            $expImage->saveAs($filePath);

                            $imageType = FileHelper::getFileExt($fileName);

                            // Small Image
                            $cacheImageSmall = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_SMALL);
                            ImageManager::thumbnail($filePath, $cacheImageSmall, UsefulFunction::IMAGE_SIZE_SMALL, $imageType, true);

                            // Medium Image
                            $cacheImageMedium = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_MEDIUM);
                            ImageManager::thumbnail($filePath, $cacheImageMedium, UsefulFunction::IMAGE_SIZE_MEDIUM, $imageType, true);

                            // Large Image
                            $cacheImageLarge = FileHelper::getCacheFilePath($itemImagesModel->image, UsefulFunction::IMAGE_TYPE_LARGE);
                            ImageManager::thumbnail($filePath, $cacheImageLarge, UsefulFunction::IMAGE_SIZE_LARGE, $imageType, true);
                        }
                    }
                    // update IS_ECOMMERCE field on items
                    Items::model()->updateByPk($model->id,array('isEcommerce'=>Items::IS_ECOMMERCE));
                }

                // parent items processing
                if(!empty($_POST['proId']) && !empty($_POST['qty']))
                {
                    // previous parents items
                    $ptrParents = 0;
                    $parentsArr = array();
                    $parentsArr = CHtml::listData(ItemParents::model()->findAll(array('condition'=>'itemId=:itemId AND status=:status',
                        'params'=>array(':itemId'=>$model->id, ':status'=>ItemParents::STATUS_ACTIVE))), 'parentId', 'parentId');
                    // new parents inserted/update to existing
                    foreach($_POST['proId'] as $key=>$parents) :
                        if($_POST['qty'][$_POST['pk'][$key]]>0) {
                            if(!in_array($parents, $parentsArr))  // insert
                            {
                                $modelParents = new ItemParents;
                                $modelParents->itemId = $model->id;
                                $modelParents->parentId = $parents;
                                $modelParents->qty = $_POST['qty'][$_POST['pk'][$key]];
                                if($modelParents->save()) $ptrParents = 1;
                            }
                            else // update
                            {
                                $modelParents = ItemParents::model()->find(array('condition'=>'parentId=:parentId AND itemId=:itemId',
                                    'params'=>array(':parentId'=>$parents,':itemId'=>$model->id)));
                                $modelParents->qty = $_POST['qty'][$_POST['pk'][$key]];
                                if($modelParents->save()) $ptrParents = 1;
                            }
                        }
                    endforeach;

                    // delete if parents de-selected/removed
                    foreach($parentsArr as $keyPk=>$parentsPk) :
                        if(!in_array($parentsPk, $_POST['proId'])) :
                            $modelParentsPk = ItemParents::model()->find(array('condition'=>'parentId=:parentId AND itemId=:itemId',
                                'params'=>array(':parentId'=>$parentsPk,':itemId'=>$model->id)));
                            $modelParentsPk->delete();
                        endif;
                    endforeach;
                    // update items status
                    if ($ptrParents==1) Items::model()->updateByPk($model->id, array('isParent'=>Items::IS_PARENTS));
                }
                else Items::model()->updateByPk($model->id,array('isParent'=>Items::IS_NOT_PARENTS));

                // item e-commerce status update by image
                $itemImageModel = ItemImages::model()->find(array('condition'=>'itemId=:itemId AND status=:status',
                    'params'=>array(':itemId'=>$model->id,':status'=>ItemImages::STATUS_ACTIVE)));
                if(empty($itemImageModel)) Items::model()->updateByPk($model->id,array('isEcommerce'=>Items::IS_NOT_ECOMMERCE));

                if($model->variation_type == 2){
                    $this->redirect(array('items/variation?item_id='.$model->id));
                }
                $msg = "Items Updated Successfully";
            }
        }
        $itemsvariation = ItemsVariation::model()->findAll(array('condition'=>'item_id=:item_id',
            'params'=>array(':item_id'=>$model->id)));

        $itemvariation = new ItemsVariation();

        $this->render('_form',array(
            'model'=>$model,'attTypeModel'=>$attTypeModel,
            'itemsvariation'=>$itemsvariation,
            'itemvariation'=>$itemvariation,
            'itemId'=>$id,'msg'=>$msg,
        ));
    }



    /**
     * import item data from xls.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionImport()
    {
        $this->pageTitle = 'Import Items';
        $msg = '';
        $ptr = 0;
        $model = new Items;
        $countStatus=1;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['Items']))
        {
            if(!empty($_FILES["Items"]["name"]["xlsFile"]) && $_POST['Items']['variation_type']==2) {
                $file_data = fopen($_FILES["Items"]["tmp_name"]["xlsFile"], 'r');
                fgetcsv($file_data);
                $incree = 0;
                $qunatity=0;
                while ($row = fgetcsv($file_data)) {
                    $itemCode=str_replace(' ', '', !empty($row[0])?$row[0]:'');
                    $color=str_replace(' ', '', !empty($row[2])?$row[2]:'');
                    $size=str_replace(' ', '', !empty($row[3])?$row[3]:'');
                    $qunatity=str_replace(' ', '', !empty($row[4])?$row[4]:'');
                    $costPrice=str_replace(' ', '', !empty($row[5])?$row[5]:'');
                    $sellPrice=str_replace(' ', '', !empty($row[6])?$row[6]:'');

                    $item = new Items();
                    $modelVariations = new ItemsVariation();
                    $items= Items::model()->find(array('condition'=>'itemCode=:itemCode', 'params'=>array(':itemCode'=>$itemCode)));
                        if(!empty($items)){

                            $color = Attributes::getNameName($color);
                            $size = Attributes::getNameName((string) $size);

                            $sizeName=$colorName='';
                            $sizeId=0;
                            $colorId=0;
                            if (!empty($size)){
                                $sizeName=$size->name;
                                $sizeId=$size->id;
                            }
                            if (!empty($color)){
                                $colorName=$color->name;
                                $colorId=$color->id;
                            }

                            $item->itemCode = $itemCode.'.'.$sizeName;
                            $item->barCode ='';
                            $item->variation_type = 2;
                            $item->itemName = $items->itemName.'/'.$colorName.'/'.$sizeName;
                            $item->description = $items->itemName.'/'.$colorName.'/'.$sizeName;
                            $item->itemName_bd = $items->itemName_bd;
                            $item->costPrice = $costPrice;
                            $item->sellPrice = $sellPrice;
                            $item->catId = $items->catId;
                            $item->brandId = $items->brandId;
                            $item->supplierId = $items->supplierId;
                            $item->taxId = $items->taxId;
                            $item->negativeStock = $items->negativeStock;
                            $item->isWeighted = $items->isWeighted;
                            $item->caseCount = $items->caseCount;
                            $item->isEcommerce = $items->isEcommerce;
                            $item->isFeatured = $items->isFeatured;
                             $item->isNew = $items->isNew;
                             $item->isSpecial = $items->isSpecial;
                            $item->isParent = $items->isParent;
                            $item->variation = $items->id;
                            $item->item_quantity = $qunatity;
                            $item->status = 1;
                            if ($item->save(false)){
                                $modelVariations->item_id = $item->id;
                                $modelVariations->item_code = $itemCode;
                                $modelVariations->name = $item->itemName;
                                $modelVariations->bar_code ='';
                                $modelVariations->color =$colorId;
                                $modelVariations->size =$sizeId;
                                $modelVariations->quantity =str_replace(' ', '', !empty($row[4])?$row[4]:'');
                                $modelVariations->cost_price =$costPrice;
                                $modelVariations->sell_price =$sellPrice;
                                if($modelVariations->save()){
                                    // barcode generations : if a item is not weighted and barcode field blank
                                    if(empty($_POST['ItemsVariation']['bar_code']))
                                    {

                                        // create directory if not exists
                                        if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com'))
                                        {
                                            mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com', 0777, true);
                                        }

                                        // barcode ean13
                                        $uiniqueId = date("ymdhis").mt_rand(0,9);
                                        $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                                        $barcode = new Barcodeean128();
                                        $barcode->draw($uiniqueId,$location,"png",true);


                                        // update barcode field
                                        $command = Yii::app()->db->createCommand();
                                        $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$item->id));
                                        $command->update('pos_items_variation', array('bar_code'=>$uiniqueId), 'item_id=:item_id', array(':item_id'=>$item->id));

                                        /*
                                                                $sql = "UPDATE `pos_items_variation` SET `bar_code`= '123465' WHERE `item_id`= ".$item->id;
                                                                $command = Yii::app()->db->createCommand($sql);
                                                                $command->execute();*/
                                    }

                                    // Purchase Order Save

                                    $purchaseOrder = new PurchaseOrder;
                                    $purchaseOrder->branchId = $item->brandId;
                                    $purchaseOrder->supplierId = $item->supplierId;
                                    $purchaseOrder->poNo = 'PO'.date("Ymdhis");
                                    $purchaseOrder->totalQty = $modelVariations->quantity;
                                    $purchaseOrder->totalPrice = $modelVariations->sell_price;
                                    $purchaseOrder->totalWeight = 0;
                                    $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                                    $purchaseOrder->save();

                                    //PoDetails Save
                                    $modelPoDetails = new PoDetails;
                                    $modelPoDetails->poId = $purchaseOrder->id;
                                    $modelPoDetails->qty = $modelVariations->quantity;
                                    $modelPoDetails->itemId = $item->id;
                                    $modelPoDetails->costPrice = $modelVariations->sell_price;
                                    $modelPoDetails->currentStock = 0;
                                    $modelPoDetails->soldQty = 0;
                                    $modelPoDetails->save();

                                    // Good Received Saved
                                    $modelGrn = new GoodReceiveNote;
                                    $modelGrn->supplierId= $item->supplierId;
                                    $modelGrn->poId = $purchaseOrder->id;
                                    $modelGrn->grnNo = 'GRN'.date("Ymdhis");
                                    $modelGrn->totalQty = $modelVariations->quantity;
                                    $modelGrn->totalWeight = 0;
                                    $modelGrn->totalPrice = $modelVariations->sell_price;
                                    $modelGrn->totalDiscount = 0;
                                    $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                                    $modelGrn->save();

                                    // Stock Saved
                                    $modelStock = new Stock;
                                    $modelStock->grnId = $modelGrn->id;
                                    $modelStock->qty = $modelVariations->quantity;
                                    $modelStock->itemId = $item->id;
                                    $modelStock->costPrice = $modelVariations->sell_price;
                                    $modelStock->discountPercent = 0;
                                    $modelStock->status = Stock::STATUS_APPROVED;
                                    $modelStock->save();
                                }
                            }
                        }

                    $countStatus=0;
                   $incree++;
                }
            }

            if(!empty($_FILES["Items"]["name"]["xlsFile"]) && $_POST['Items']['variation_type']==1) {
                $file_data = fopen($_FILES["Items"]["tmp_name"]["xlsFile"], 'r');
                fgetcsv($file_data);
                $incree = 0;
                while ($row = fgetcsv($file_data)) {
                    $itemCode=str_replace(' ', '', !empty($row[1])?$row[1]:'');
                    $checkItemCode= Items::model()->find(array('condition'=>'itemCode=:itemCode', 'params'=>array(':itemCode'=>$itemCode)));
                    //Update Informations Item
                    if (!empty($checkItemCode)){
                        if($checkItemCode->variation_type == '1'){
                            // Stock Saved
                            $modelStock = Stock::model()->find(array('condition'=>'itemId=:itemId',
                                'params'=>array(':itemId'=>$checkItemCode->id)));

                            if(!empty($modelStock)){
                                $modelStock->costPrice = $checkItemCode->sellPrice;
                                $modelStock->discountPercent = 0;
                                $modelStock->qty = $checkItemCode->item_quantity;
                                $modelStock->update();
                            }else{
                                // Purchase Order Save
                                $purchaseOrder = new PurchaseOrder;
                                $purchaseOrder->branchId = $checkItemCode->brandId;
                                $purchaseOrder->supplierId = $checkItemCode->supplierId;
                                $purchaseOrder->poNo = 'PO'.time();
                                $purchaseOrder->totalQty = $checkItemCode->item_quantity;
                                $purchaseOrder->totalPrice = $checkItemCode->sellPrice;
                                $purchaseOrder->totalWeight = 0;
                                $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;

                                if ($purchaseOrder->save()){
                                    //PoDetails Save
                                    $modelPoDetails = new PoDetails;
                                    $modelPoDetails->poId = $purchaseOrder->id;
                                    $modelPoDetails->qty = $checkItemCode->item_quantity;
                                    $modelPoDetails->itemId = $model->id;
                                    $modelPoDetails->costPrice = $checkItemCode->sellPrice;
                                    $modelPoDetails->currentStock = 0;
                                    $modelPoDetails->soldQty = 0;
                                    $modelPoDetails->save();


                                    // Good Received Saved
                                    $modelGrn = new GoodReceiveNote;
                                    $modelGrn->supplierId= $checkItemCode->supplierId;
                                    $modelGrn->poId = $purchaseOrder->id;
                                    $modelGrn->grnNo = 'GRN'.time();
                                    $modelGrn->totalQty = $checkItemCode->item_quantity;
                                    $modelGrn->totalWeight = 0;
                                    $modelGrn->totalPrice = $checkItemCode->sellPrice;
                                    $modelGrn->totalDiscount = 0;
                                    if ($modelGrn->save()){
                                        // Stock Saved
                                        $modelStock = new Stock;
                                        $modelStock->grnId = $modelGrn->id;
                                        $modelStock->qty = $checkItemCode->item_quantity;
                                        $modelStock->itemId = $checkItemCode->id;
                                        $modelStock->costPrice = $checkItemCode->sellPrice;
                                        $modelStock->discountPercent = 0;
                                        if ($modelStock->save()){
                                            // Good Received Saved
                                            $modelGrn = GoodReceiveNote::model()->find(array('condition'=>'id=:id',
                                                'params'=>array(':id'=>$modelStock->grnId)));
                                            if(!empty($modelGrn)){
                                                $modelGrn->supplierId= $checkItemCode->supplierId;
                                                $modelGrn->totalQty = $checkItemCode->item_quantity;
                                                $modelGrn->totalWeight = 0;
                                                $modelGrn->totalPrice = $checkItemCode->sellPrice;
                                                $modelGrn->totalDiscount = 0;
                                                $modelGrn->update();

                                            }

                                            // Purchase Order Save
                                            $purchaseOrder = PurchaseOrder::model()->find(array('condition'=>'id=:id',
                                                'params'=>array(':id'=>$modelGrn->poId)));

                                            if(!empty($purchaseOrder)){
                                                //$purchaseOrder->branchId = $model->brandId;
                                                $purchaseOrder->supplierId = $checkItemCode->supplierId;
                                                $purchaseOrder->totalQty = $checkItemCode->item_quantity;
                                                $purchaseOrder->totalPrice = $checkItemCode->sellPrice;
                                                $purchaseOrder->totalWeight = 0;
                                                $purchaseOrder->update();
                                            }

                                            //PoDetails Save
                                            $modelPoDetails = PoDetails::model()->find(array('condition'=>'poId=:poId',
                                                'params'=>array(':poId'=>$modelGrn->poId)));
                                            if(!empty($modelPoDetails)){
                                                $modelPoDetails->qty = $checkItemCode->item_quantity;
                                                $modelPoDetails->itemId = $checkItemCode->id;
                                                $modelPoDetails->costPrice = $checkItemCode->sellPrice;
                                                $modelPoDetails->currentStock = 0;
                                                $modelPoDetails->soldQty = 0;
                                                $modelPoDetails->update();
                                            }
                                            ItemsVariation::model()->deleteAll(array('condition'=>'item_id=:item_id','params'=>array(':item_id'=>$checkItemCode->id)));
                                        }
                                    }
                                }
                            }

                        }
                    }else{
                        //New Created
                        $model=new Items;
                        $model->barCode='';
                        $model->isParent=2;
                        $model->variation=0;
                        $model->caseCount=1;
                        $model->brandId=1;
                        $model->supplierId=2; //pluspoint
                        $model->isWeighted='no'; //pluspoint
                        $model->taxId=2; //pluspoint
                        $model->status=1; //pluspoint
                        $model->itemCode=str_replace(' ', '', !empty($row[1])?$row[1]:'');
                        $model->variation_type=str_replace(' ', '', !empty($row[2])?($row[2]=='Sample')?1:2:'');
                        $model->itemName=str_replace(' ', '', !empty($row[3])?$row[3]:'');
                        $model->itemName_bd=str_replace(' ', '', !empty($row[3])?$row[3]:'');
                        $model->costPrice=str_replace(' ', '', !empty($row[4])?$row[4]:'');
                        $model->sellPrice=str_replace(' ', '', !empty($row[5])?$row[5]:'');
                        $model->deptId=str_replace(' ', '', !empty($row[6])?$row[6]:'');
                        $model->subdeptId=str_replace(' ', '', !empty($row[7])?$row[7]:'');
                        $model->catId=str_replace(' ', '', !empty($row[8])?$row[8]:'');
                        $model->isEcommerce=str_replace(' ', '', !empty($row[9])?($row[9]=='yes')?1:2:'');
                        $model->isFeatured=str_replace(' ', '', !empty($row[10])?($row[10]=='yes')?1:2:'');
                        $model->isNew=str_replace(' ', '', !empty($row[11])?($row[11]=='yes')?1:2:'');
                        $model->isSpecial=str_replace(' ', '', !empty($row[12])?($row[12]=='yes')?1:2:'');
                        $model->isSales=str_replace(' ', '', !empty($row[13])?($row[13]=='yes')?1:2:'');
                        $model->specification=str_replace(' ', '', !empty($row[14])?$row[14]:'');
                        $model->description=str_replace(' ', '', !empty($row[15])?$row[15]:'');
                        $model->item_quantity=str_replace(' ', '', !empty($row[16])?$row[16]:'');
                        if($model->save()){
                            if($model->variation_type == '1'){
                                // Purchase Order Save

                                $purchaseOrder = new PurchaseOrder;
                                $purchaseOrder->branchId = $model->brandId;
                                $purchaseOrder->supplierId = $model->supplierId;
                                $purchaseOrder->poNo = 'PO'.date("Ymdhis");
                                $purchaseOrder->totalQty = $model->item_quantity;
                                $purchaseOrder->totalPrice = $model->sellPrice;
                                $purchaseOrder->totalWeight = 0;
                                $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                                $purchaseOrder->save();

                                //PoDetails Save
                                $modelPoDetails = new PoDetails;
                                $modelPoDetails->poId = $purchaseOrder->id;
                                $modelPoDetails->qty = $model->item_quantity;
                                $modelPoDetails->itemId = $model->id;
                                $modelPoDetails->costPrice = $model->sellPrice;
                                $modelPoDetails->currentStock = 0;
                                $modelPoDetails->soldQty = 0;
                                $modelPoDetails->save();

                                // Good Received Saved
                                $modelGrn = new GoodReceiveNote;
                                $modelGrn->supplierId= $model->supplierId;
                                $modelGrn->poId = $purchaseOrder->id;
                                $modelGrn->grnNo = 'GRN'.date("Ymdhis");
                                $modelGrn->totalQty = $model->item_quantity;
                                $modelGrn->totalWeight = 0;
                                $modelGrn->totalPrice = $model->sellPrice;
                                $modelGrn->totalDiscount = 0;
                                $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                                $modelGrn->save();

                                // Stock Saved
                                $modelStock = new Stock;
                                $modelStock->grnId = $modelGrn->id;
                                $modelStock->qty = $model->item_quantity;
                                $modelStock->itemId = $model->id;
                                $modelStock->costPrice = $model->sellPrice;
                                $modelStock->discountPercent = 0;
                                $modelStock->status = Stock::STATUS_APPROVED;
                                $modelStock->save();

                            }
                            //End Save
                            if(empty($model->barCode))
                            {
                                // create directory if not exists
                                if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId']))
                                {
                                    mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId'], 0777, true);
                                }

                                // barcode ean13
                                $uiniqueId = date("ymdhis").mt_rand(0,9);
                                $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                                $barcode = new Barcodeean128();
                                $barcode->draw($uiniqueId,$location,"png",true);

                                // update barcode field
                                $command = Yii::app()->db->createCommand();
                                $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$model->id));
                            }
                        }
                    }

                    if($incree > 0) {

                    }
                    $countStatus=0;
                    $incree++;
                }
            }
        }

        if ($countStatus==0){
            $msg = '<div class="notification note-success"><p>Import  Successfully Done !</p></div>';
        }

        $this->render('import',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    // item search
    public function actionSearchItems()
    {
        $msg = $cat = $brandId = $supplierId ='';
        $catName = $brandName = $suppName = $filter = '';
        $model=array();

        // after post data
        if(isset($_POST['yt0']))
        {
            if(empty($_POST['deptId']) && empty($_POST['subdeptId']) && empty($_POST['catId']) && empty($_POST['supplierId']) && empty($_POST['brandId']))
                $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one category/supplier/brand !</p></div>';
            else
            {
                $supplierId = $_POST['supplierId'];
                $brandId = $_POST['brandId'];
                $cat = $_POST['catId'];

                if(!empty($_POST['catId'])) $filter.= " AND catId=".$_POST['catId'];
                if(!empty($_POST['supplierId'])) $filter.= " AND supplierId=".$_POST['supplierId'];
                if(!empty($_POST['brandId'])) $filter.= " AND brandId=".$_POST['brandId'];
                $sql = "SELECT * FROM pos_items WHERE status=".Items::STATUS_ACTIVE.$filter;
                $model = Items::model()->findAllBySql($sql);


                $brandModel = Brand::model()->findByPk($_POST['brandId']);
                if(!empty($brandModel)) $brandName = $brandModel->name;

                $suppModel = Supplier::model()->findByPk($_POST['supplierId']);
                if(!empty($suppModel)) $suppName = $suppModel->name;

                $catModel = Category::model()->findByPk($_POST['catId']);
                if(!empty($catModel)) $catName = $catModel->name;
            }
        }
        $this->render('itemSearch',array(
            'model'=>$model,
            'msg'=>$msg,
            'cat'=>$cat,'catName'=>$catName,
            'brandId'=>$brandId,'brandName'=>$brandName,
            'supplierId'=>$supplierId,'suppName'=>$suppName
        ));
    }

    /**
     * change item prices.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionChangePrice()
    {
        $msg = '';
        $model=new ItemsPrice;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);

        if(isset($_POST['itemCode']))
        {
            $itemModel = Items::model()->find('itemCode=:itemCode',array(':itemCode'=>$_POST['itemCode']));
            if(!empty($itemModel))
            {
                $itemModel->costPrice=$_POST['costPrice'];
                $itemModel->sellPrice=$_POST['sellPrice'];
                if($itemModel->save()) {
                    $msg = '<div class="notification note-success"><p>Items Price Changed Successfully</p></div>';
                    $model=new ItemsPrice;

                    //Search CHild Item
                    $sql = "SELECT id FROM pos_items WHERE  variation=".$itemModel->id ."";
                    $subItemModel = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($subItemModel) {
                        foreach ($subItemModel as $key => $subitemsModelEachItem) {
                            //update child Item
                            $childItemCOde=$subitemsModelEachItem['id'];
                            if ($childItemCOde) {
                                $vartionsItemItem = Items::model()->find('id=:id', array(':id' => $childItemCOde));
                                $vartionsItemItem->costPrice = $_POST['costPrice'];
                                $vartionsItemItem->sellPrice = $_POST['sellPrice'];
                                $vartionsItemItem->save();

                                //update variations items
                                $variationsModel = ItemsVariation::model()->find('item_id=:item_id', array(':item_id' => $subitemsModelEachItem['id']));
                                $variationsModel->cost_price = $_POST['costPrice'];
                                $variationsModel->sell_price = $_POST['sellPrice'];
                                $variationsModel->save();
                            }
                        }
                    }
                }

            }
            else $msg = '<div class="notification note-error"><p>Item doesn,t exists !</p></div>';
        }

        $this->render('changePrice',array(
            'model'=>$model,
            'msg'=>$msg,
        ));
    }

    /**
     * update item prices after change.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdatePrice()
    {
        $msg = '';
        // current day grn without approval
        $icnModel = NULL;
        $criteria = new CDbCriteria;
        $criteria->condition='status LIKE :status';
        $criteria->params=array(':status' => ItemsPrice::STATUS_INACTIVE);
        $criteria->order = 'changeDate desc';
        $icnModel = ItemsPrice::model()->findAll($criteria);

        // grn approved process
        if(isset($_POST['icnId']))
        {
            foreach($_POST['icnId'] as $key=>$icnId)
            {
                $checkedAtt = "checkedid".$key;

                if(isset($_POST[$checkedAtt])!=NULL) // checked
                {
                    // update price update table
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('id='.$icnId);
                    $priceUpd = ItemsPrice::model()->updateAll(array('status' => ItemsPrice::STATUS_ACTIVE,
                        'changeDate'=>date("Y-m-d H:i:s")),$criteria);
                    if($priceUpd) :
                        // if has chield then update all chield price
                        $itemModel = Items::model()->findByPk($_POST['itemId'][$key]);
                        if(!empty($itemModel)) :

                            /* processing chield items price
                            $chieldModel = ItemParents::model()->findAll(array('condition'=>'parentId=:parentId AND status=:status',
                                                                               'params'=>array(':parentId'=>$itemModel->id,':status'=>ItemParents::STATUS_ACTIVE)));
                            if(!empty($chieldModel)) :
                                foreach($chieldModel as $cKey=>$cData) :
                                    $newPrice = $cData->item->costPrice-($cData->qty*$cData->parent->sellPrice);
                                    $costPrice = $newPrice+($cData->qty*$_POST['sellPrice'][$key]);
                                    //Items::model()->updateByPk($cData->item->id, array('costPrice'=>$newPrice));
                                    $sql = "UPDATE pos_items set costPrice=".$costPrice." WHERE id=".$cData->item->id;
                                    $command = Yii::app()->db->createCommand($sql);
                                    $command->execute();
                                endforeach;
                            endif;*/

                            // child items processing for this items
                            $childModel = ItemParents::getAllItemChildByParent($itemModel->id,ItemParents::STATUS_ACTIVE);
                            if(!empty($childModel)) :
                                foreach($childModel as $keyChild=>$dataChild) :
                                    if($_POST['costPrice'][$key]>$itemModel->costPrice) :
                                        $operation = '+';
                                        $priceDifference = round($dataChild->qty*($_POST['costPrice'][$key] - $itemModel->costPrice),2);
                                    else :
                                        $operation = '-';
                                        $priceDifference = round($dataChild->qty*($itemModel->costPrice - $_POST['costPrice'][$key]),2);
                                    endif;
                                    $sql = "UPDATE pos_items set costPrice=costPrice".$operation.$priceDifference." WHERE id=".$dataChild->item->id;
                                    $command = Yii::app()->db->createCommand($sql);
                                    $command->execute();
                                endforeach;
                            endif;

                            // update parent item price
                            $criteriaItem = new CDbCriteria;
                            $criteriaItem->addCondition('id='.$_POST['itemId'][$key]);
                            Items::model()->updateAll(array('costPrice'=>$_POST['costPrice'][$key],
                                'sellPrice'=>$_POST['sellPrice'][$key]), $criteriaItem);
                        endif;
                    endif;
                }
            }
            $msg = '<div class="notification note-success"><p>Items Price Updated Successfully</p></div>';
            $icnModel=NULL;
        }

        $this->render('updatePrice',array(
            'icnModel'=>$icnModel,
            'msg'=>$msg,
        ));
    }

    /** generate and test barcode
     * Displays and generate barcode
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionGenerateBarcode()
    {
        $msg = '';
        $totalSuccess = $totalFailure = $totalRemain = 0;

        // manually adjust barcodes
        //$sqlCount = "SELECT id,itemCode,barCode FROM pos_items WHERE barCode=0 and isWeighted='no' ORDER BY id ASC";
        $sqlCount = "SELECT id,itemCode,barCode FROM pos_items WHERE barCode<>0 and isWeighted='no' and status=".Items::STATUS_ACTIVE." ORDER BY id ASC";
        $modelCount = Items::model()->findAllBySql($sqlCount);

        //$sql = "SELECT id,itemCode,barCode FROM pos_items WHERE barCode=0 and isWeighted='no' ORDER BY id ASC Limit 0,50";
        $sql = "SELECT id,itemCode,barCode FROM pos_items WHERE barCode<>0 and isWeighted='no' and status=".Items::STATUS_ACTIVE." ORDER BY id ASC Limit 0,100";
        $model = Items::model()->findAllBySql($sql);

        if(!empty($model)) :
            // create directory if not exists
            if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId']))
            {
                mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/'.Yii::app()->session['orgId'], 0777, true);
            }
            foreach($model as $key=>$data) :
                // barcode ean13
                $uiniqueId = $data->barCode; //date("ymdhis").$data->id;
                $location = Yii::getPathOfAlias("webroot").'/media/barcode/'.Yii::app()->session['orgId'].'/'.$uiniqueId.'.png';

                $barcode = new Barcodeean128();
                $barcode->draw($uiniqueId,$location,"png",true);

                // update barcode field
                $command = Yii::app()->db->createCommand();
                //if($command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$data->id))) {
                if($command->update('pos_items', array('status'=>Items::STATUS_INACTIVE), 'id=:id', array(':id'=>$data->id))) {
                    $totalSuccess++;
                    $msg .= '<div class="notification note-success"><p>Barcode Generated Successfully for Item Code : ' . $data->itemCode . ' !</p></div>';
                }
                else
                {
                    $totalFailure++;
                    $msg.= '<div class="notification note-error"><p>Barcode Generation Failed for Item Code : '.$data->itemCode.' !</p></div>';
                }
            endforeach;
            $totalRemain = count($modelCount)-$totalSuccess;
        endif;

        $this->render('generateBarcode',array(
            'msg'=>$msg,'totalSuccess'=>$totalSuccess,
            'totalFailure'=>$totalFailure,'totalRemain'=>$totalRemain
        ));
    }

    public function actionPrintGlobalBarcode()
    {
        $msg = $dept = $subdept = $cat = $supplierId = $filter = $isEcommerce = $isParent= '';
        $deptName = $subdeptName = $catName = $itemCode = $suppName = '';
        $endDate = $itemCode = '';
        $modelStock = $modelTransfer = array();
        $PrintButtonEnable=0;

        // after post data
        if(isset($_POST['endDate']))
        {
            $PrintButtonEnable=1;
            if(empty($_POST['endDate'])) $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select end date !</p></div>';
            else if(empty($_POST['deptId']) && empty($_POST['subdeptId']) && empty($_POST['catId']) && empty($_POST['supplierId']) && empty($_POST['itemCode']))
                $msg = '<div class="notification note-error"><p style="padding-left:20px;">Please select at least one department/sub department/category/supplier/itemCode !</p></div>';
            else
            {
                $endDate = $_POST['endDate'];
                $cat = $_POST['catId'];
                $subdept = $_POST['subdeptId'];
                $dept = $_POST['deptId'];
                $supplierId = $_POST['supplierId'];
                $itemCode = $_POST['itemCode'];

                if(!empty($_POST['isEcommerce'])) {
                    $isEcommerce = $_POST['isEcommerce'];
                    $filter.= ' AND i.isEcommerce='.$isEcommerce;
                }
                if(!empty($_POST['isParent'])) {
                    $isParent = $_POST['isParent'];
                    $filter.= ' AND i.isParent='.$isParent;
                }

                // category wise
                if(!empty($_POST['catId']))
                {
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.catId IN(SELECT id FROM pos_category WHERE id=".$cat.")
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.catId IN(SELECT id FROM pos_category WHERE id=".$cat.") AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
                }
                // sub dept wise
                else if(!empty($_POST['subdeptId']))
                {
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId IN(SELECT id FROM pos_subdepartment WHERE id=".$subdept.")
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s,pos_category c WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId IN(SELECT id FROM pos_subdepartment WHERE id=".$subdept.") AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
                }
                // dept wise
                else if(!empty($_POST['deptId']))
                {
                    if($_POST['deptId']=='all') // all departments
                    {
                        $sqlStock = "SELECT s.* FROM pos_stock s,pos_items i WHERE s.itemId=i.id AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
						             AND s.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                        WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                        AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                        GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                    else
                    {
                        $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id
                                     AND d.deptId=".$dept." AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                     AND s.branchId=".Yii::app()->session['branchId']." ".$filter."  AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                        $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s,pos_category c,pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id
                                        AND d.deptId=".$dept." AND s.itemId NOT IN(SELECT itemId FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                        AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId) AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter."
                                        AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
                    }
                }
                // supplierId wise
                else if(!empty($_POST['supplierId']))
                {
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.supplierId=".$supplierId."
							     AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.branchId=".Yii::app()->session['branchId']." ".$filter."
							     AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.supplierId=".$supplierId." AND s.itemId NOT IN(SELECT itemId FROM pos_stock
                                    WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId)
                                    AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."' AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE."
                                    GROUP BY s.itemId ORDER BY s.stockDate DESC";
                }
                // itemCode wise
                else if(!empty($_POST['itemCode']))
                {
                    $itemCode = $_POST['itemCode'];
                    $sqlStock = "SELECT s.* FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
					             AND s.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";

                    $sqlTransfer = "SELECT s.* FROM pos_items i, pos_stock_transfer s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$_POST['endDate']."'
                                    AND s.itemId NOT IN (SELECT itemId FROM pos_items i, pos_stock s WHERE s.itemId=i.id AND i.itemCode='".$itemCode."' and branchId=".Yii::app()->session['branchId']."
                                    AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='2015-07-15' AND s.status=".Stock::STATUS_APPROVED." GROUP BY itemId)
					                AND s.toBranchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".StockTransfer::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
                }
                else
                {
                    $sqlStock = $sqlTransfer = "";
                }
                $modelStock = Stock::model()->findAllBySql($sqlStock);
                $modelTransfer = Stock::model()->findAllBySql($sqlTransfer);

                // finding dept,subdept,category
                if($_POST['deptId']=='all') $deptName = 'All';
                else
                {
                    $deptModel = Department::model()->findByPk($_POST['deptId']);
                    if(!empty($deptModel)) $deptName = $deptModel->name;
                }

                $subdeptModel = Subdepartment::model()->findByPk($_POST['subdeptId']);
                if(!empty($subdeptModel)) $subdeptName = $subdeptModel->name;

                $catModel = Category::model()->findByPk($_POST['catId']);
                if(!empty($catModel)) $catName = $catModel->name;

                $catModel = Category::model()->findByPk($cat);
                if(!empty($catModel)) $catName = $catModel->name;

                $suppModel = Supplier::model()->findByPk($_POST['supplierId']);
                if(!empty($suppModel)) $suppName = $suppModel->name;
            }
        }

        if (isset($_POST['globalBarcodePrint'])){
            if (!empty($_POST['globalBarcodePrint'])){
                $postArray = explode(',', $_POST['globalBarcodePrint']);
                $filename = "barCode".rand(4, 1000000).".csv";
                $f = fopen('php://memory', 'w+');
                fputcsv($f, array('Item','Color','Item Code','Size','Barcode','Price','Print'));

                foreach($postArray as $key=>$val) :
                    $itemModel = Items::model()->findByPk($val);
                    $itemVariation = ItemsVariation::model()->find('item_id=:item_id',array(':item_id'=>$val));
                    $stock=0;
                    $itemName=$barCode=$itemCode=$price='';
                    if(!empty($itemModel)){
                        $itemName=$itemModel->itemName;
                        $itemCode=$itemModel->itemCode;
                        $barCode=$itemModel->barCode;
                        $price=$itemModel->sellPrice;
                        $stock = Stock::getItemWiseStock($itemModel->id);
                    }
                    $color = '';
                    $size = '';
                    if(!empty($itemVariation)){
                        $color = ItemsVariation::variationAttributesById($itemVariation->color);
                        $size = ItemsVariation::variationAttributesById($itemVariation->size);
                    }
                    fputcsv($f, array($itemName,$color,$itemCode,$size,$barCode,$price,$stock*2));
                endforeach;
                // reset the file pointer to the start of the file
                fseek($f, 0);
                // tell the browser it's going to be a csv file
                header('Content-Type: application/csv');
                // tell the browser we want to save it instead of displaying it
                header('Content-Disposition: attachment; filename="'.$filename.'";');
                // make php send the generated csv lines to the browser
                fpassthru($f);
                exit();
            }
        }

        $this->render('printGroupBarcode',array(
            'modelStock'=>$modelStock,'modelTransfer'=>$modelTransfer,'msg'=>$msg,
            'isEcommerce'=>$isEcommerce,'isParent'=>$isParent,
            'PrintButtonEnable'=>$PrintButtonEnable,
            'dept'=>$dept,'deptName'=>$deptName,
            'subdept'=>$subdept,'subdeptName'=>$subdeptName,
            'cat'=>$cat,'catName'=>$catName,'itemCode'=>$itemCode,
            'supplierId'=>$supplierId,'suppName'=>$suppName,'endDate'=>$endDate,
        ));

    }
    
    /** print barcode
     * Displays and generate barcode
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionPrintBarcode()
    {
        $msg = '';
        $model=array();

        if(isset($_POST['qty']) && isset($_POST['type']))
        {
            // processing sales details
            foreach($_POST['qty'] as $key=>$qty) :
                if($qty>0) :
                    $model[$key] = $_POST['proId'][$_POST['pk'][$key]].'#'.$qty;
                endif;
            endforeach;
        }
        $this->render('printBarcode',array(
            'model'=>$model,
            'msg'=>$msg,
        ));

//        $msg = '';
//        $model=array();
//
//        if(isset($_POST['qty']) && isset($_POST['type']))
//        {
//            // processing sales details
//            foreach($_POST['qty'] as $key=>$qty) :
//                if($qty>0) :
//                    $model[$key] = $_POST['proId'][$_POST['pk'][$key]].'#'.$qty;
//                endif;
//            endforeach;
//
//            if ($model){
//                $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
//                $filename = "barCode".rand(4, 1000000).".csv";
//                $f = fopen('php://memory', 'w+');
//                fputcsv($f, array('Item','Color','Item Code','Size','Barcode','Price','Print'));
//                foreach($model as $key=>$val) :
//                    $itemArr = explode('#',$val);
//                    $itemModel = Items::model()->findByPk($itemArr[0]);
//                    $itemVariation = ItemsVariation::model()->find('item_id=:item_id',array(':item_id'=>$itemArr[0]));
//                    $quantity=$itemArr[1];
//                    $itemName=$barCode=$itemCode=$price='';
//                    if(!empty($itemModel)){
//                        $itemName=$itemModel->itemName;
//                        $itemCode=$itemModel->itemCode;
//                        $barCode=$itemModel->barCode;
//                        $price=$itemModel->sellPrice;
//                    }
//                    $color = '';
//                    $size = '';
//                    if(!empty($itemVariation)){
//                        $color = ItemsVariation::variationAttributesById($itemVariation->color);
//                        $size = ItemsVariation::variationAttributesById($itemVariation->size);
//                    }
//                    fputcsv($f, array($itemName,$color,$itemCode,$size,$barCode,$price,$quantity));
//                endforeach;
//                // reset the file pointer to the start of the file
//                fseek($f, 0);
//                // tell the browser it's going to be a csv file
//                header('Content-Type: application/csv');
//                // tell the browser we want to save it instead of displaying it
//                header('Content-Disposition: attachment; filename="'.$filename.'";');
//                // make php send the generated csv lines to the browser
//                fpassthru($f);
//                exit();
//            }
//        }
//        $this->render('printBarcode',array(
//            'model'=>$model,
//            'msg'=>$msg,
//        ));
    }



    /**
     * Stock Transfer Approved
     */
    public function actionStockTransferApproved(){
        $model = new StockTransfer();


        if(!empty($_REQUEST['approved']) && !empty($_REQUEST['transferNo'])){
            $command = Yii::app()->db->createCommand();
            $command->update('pos_stock_transfer', array('status'=>1), 'transferNo=:transferNo', array(':transferNo'=>$_REQUEST['transferNo']));
            $msg = 'Successfully Approved Stock Transfer.';

            $this->redirect(array('items/stockTransferApproved'));
           /* $this->render('stockApproved',array(
                'model'=>$model,
                'msg'=>$msg,
            ));*/

        }

        if(isset($_POST['StockTransfer']) && !empty($_POST['StockTransfer'])){
            $stockTransfer = StockTransfer::model()->findAll(array('condition'=>'transferNo=:transferNo',
                'params'=>array(':transferNo'=>$_POST['StockTransfer']['transferNo'])));

            if(!empty($stockTransfer)){
                $this->render('stockApproved',array(
                    'model'=>$model,
                    'stockTransfer'=>$stockTransfer,
                ));
            }

        }


        $this->render('stockApproved',array(
            'model'=>$model,
        ));
    }




    /**
     * Stock List Management
     */

    public function actionStockList(){
        $model = new Stock();

        if(isset($_POST['Stock']) && !empty($_POST['Stock'])){
            $stock = $_POST['Stock'];
            $itemsCode = $stock['itemCode'];
            $branchAll=Branch::getorg(Branch::STATUS_ACTIVE);

            $stockModel=[];
            foreach ($branchAll as $key=>$branchItems){
                $items = Items::model()->find(array('condition'=>'itemCode=:itemCode',
                    'params'=>array(':itemCode'=>$itemsCode)));
                if(!empty($items)){

                    $sql = "SELECT SUM(`qty`) as qty, `branchId`, `costPrice`, `itemId` FROM `pos_stock` WHERE `itemId` = '.$items->id.'";
                    $stockModel[$key] = Stock::model()->findAllBySql($sql);
                }
            }

            $this->render('stockList',array(
                'model'=>$model,
                'stockModel'=>$stockModel,
            ));
        }
        $this->render('stockList',array(
            'model'=>$model,
        ));


    }

    /**
     * stock transfer from store to store
     */
    public function actionStockTransfer()
    {


        $msg = $printId =$toBranchId= '';
        $model=new StockTransfer;

        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation($model);


        if(!empty($_POST['qty']) && !empty($_POST['StockTransfer']['toBranchId']) && isset($_POST['yt0']))
        {
            $toBranchId=$_POST['StockTransfer']['toBranchId'];
            $ptr = 0;
            // processing sales details
            foreach($_POST['qty'] as $key=>$qty) :
                if($qty>0) :
                    $model = new StockTransfer;
                    $model->transferNo = $_POST['StockTransfer']['transferNo'];
                    $model->toBranchId = $_POST['StockTransfer']['toBranchId'];
                    $model->itemId = $_POST['proId'][$_POST['pk'][$key]];
                    $model->qty = $qty;
                    $model->costPrice = $_POST['costPriceAmount'][$_POST['pk'][$key]];
                    //$model->status = 0;
                    $model->save();
                    $ptr = 1;
                endif;
            endforeach;

            if($ptr==1) :
                $printId = $model->transferNo;
                $msg = "Stock Transfer Successfully";
                $model=new StockTransfer;
            endif;

        }


        $this->render('stockTransfer',array(
            'model'=>$model,
            'msg'=>$msg,
            'toBranchId'=>$toBranchId,
            'printId'=>$printId
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        // find & delete item attributes & images
        ItemAttributes::model()->deleteAll(array('condition'=>'itemId=:itemId','params'=>array(':itemId'=>$id)));
        ItemImages::model()->deleteAll(array('condition'=>'itemId=:itemId','params'=>array(':itemId'=>$id)));
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Items');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /********* Items Variation Create**********/
    public function actionVariationCreate($item_id){

        $model = new ItemsVariation();

        $item = new Items();

        $this->performAjaxValidation($model);
        $items = Items::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$item_id)));

        if(isset($_POST['yt0']) && $_POST['ItemsVariation']){
            $model->attributes=$_POST['ItemsVariation'];
            if(isset($_FILES['ItemsVariation']['name'])) {
                if(is_uploaded_file($_FILES['ItemsVariation']['tmp_name']['image'])) {
                    $name = $_FILES['ItemsVariation']['name']['image'];
                    $size = $_FILES["ItemsVariation"]["size"]['image'];
                    $ext = explode(".", $name);
                    $allowed_ext = array("png", "jpg", "jpeg");
                    if(!empty($name)){
                        if(in_array($ext['1'], $allowed_ext))
                        {
                            if($size < (1024*10240))
                            {

                                $company = Company::model()->find();

                                $comapnyNameExplode = str_replace('-', '', $company->name);
                                $comapnyNameExplode = str_replace('.', '', $comapnyNameExplode);
                                $comapnyNameExplode = str_replace(' ', '', $comapnyNameExplode);
                                $comapnyNameExplode = strtolower($comapnyNameExplode);

                                $RandomAccountNumber = uniqid($comapnyNameExplode.'-'.rand().'-');
                                // 73 x 100  Small Size
                                $new_image = '';
                                $new_name = $RandomAccountNumber . '-small.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                $new_width=73;
                                $new_height = ($height/$width)*73;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);


                                // 290 x 400 Medium Size
                                $new_image = '';
                                $new_name = $RandomAccountNumber . '-medium.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                $new_width=290;
                                $new_height = ($height/$width)*290;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);


                                // 1300 x 1800 Large Size
                                $new_image = '';
                                $new_name = $RandomAccountNumber . '-large.' . $ext['1'];
                                $path = 'media/catalog/product/' . $new_name;

                                /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                if($ext['1'] == 'png')
                                {
                                    $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                {
                                    $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                }
                                $new_width=1300;
                                $new_height = ($height/$width)*1300;
                                $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                imagejpeg($tmp_image, $path, 100);
                                imagedestroy($new_image);
                                imagedestroy($tmp_image);

                                $model->image = $path;
                                //echo '<img width="80px" src="'.$path.'" />';
                            }
                            else
                            {
                                $returnValue['warning'] = 'Image File size must be less than 8 MB';
                                //echo '<p style="color: red">Image File size must be less than 8 MB</p>';
                            }
                        }
                        else
                        {
                            $returnValue['invalid'] = 'Invalid Image File';
                            //echo '<p style="color: red">Invalid Image File</p>';
                        }
                    }
                }
            }

            if(!empty($items)){
                $color = Attributes::getNameById($model->color);
                $size = Attributes::getNameById($model->size);




                $item->itemCode = $items->itemCode.$size->name;
                $item->barCode = $model->bar_code;
                $item->variation_type = 2;
                $item->itemName = $items->itemName.'/'.$color->name.'/'.$size->name;
                $item->description = $items->itemName.'/'.$color->name.'/'.$size->name;
                $item->itemName_bd = $items->itemName_bd;
                $item->costPrice = $model->cost_price;
                $item->sellPrice = $model->sell_price;
                $item->catId = $items->catId;
                $item->brandId = $items->brandId;
                $item->supplierId = $items->supplierId;
                $item->taxId = $items->taxId;
                $item->negativeStock = $items->negativeStock;
                $item->isWeighted = $items->isWeighted;
                $item->caseCount = $items->caseCount;
                $item->isEcommerce = $items->isEcommerce;
               /* $item->isFeatured = $items->isFeatured;
                $item->isNew = $items->isNew;
                $item->isSpecial = $items->isSpecial;*/
                $item->isParent = $items->isParent;
                $item->variation = $item_id;
                $item->item_quantity = $model->quantity;
                $item->status = 1;
                $item->save(false);
                $model->item_id = $item->id;

                $model->item_code = $items->itemCode.'.'.$size->name;

                if($model->save()){

                    // barcode generations : if a item is not weighted and barcode field blank
                    if(empty($_POST['ItemsVariation']['bar_code']))
                    {

                        // create directory if not exists
                        if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com'))
                        {
                            mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com', 0777, true);
                        }

                        // barcode ean13
                        $uiniqueId = date("ymdhis").mt_rand(0,9);
                        $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                        $barcode = new Barcodeean128();
                        $barcode->draw($uiniqueId,$location,"png",true);


                        // update barcode field
                        $command = Yii::app()->db->createCommand();
                        $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$item->id));
                        $command->update('pos_items_variation', array('bar_code'=>$uiniqueId), 'item_id=:item_id', array(':item_id'=>$item->id));

/*
                        $sql = "UPDATE `pos_items_variation` SET `bar_code`= '123465' WHERE `item_id`= ".$item->id;
                        $command = Yii::app()->db->createCommand($sql);
                        $command->execute();*/
                    }

                    // Purchase Order Save

                    $purchaseOrder = new PurchaseOrder;
                    $purchaseOrder->branchId = $item->brandId;
                    $purchaseOrder->supplierId = $item->supplierId;
                    $purchaseOrder->poNo = 'PO'.date("Ymdhis");
                    $purchaseOrder->totalQty = $model->quantity;
                    $purchaseOrder->totalPrice = $model->sell_price;
                    $purchaseOrder->totalWeight = 0;
                    $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                    $purchaseOrder->save();

                    //PoDetails Save
                    $modelPoDetails = new PoDetails;
                    $modelPoDetails->poId = $purchaseOrder->id;
                    $modelPoDetails->qty = $model->quantity;
                    $modelPoDetails->itemId = $item->id;
                    $modelPoDetails->costPrice = $model->sell_price;
                    $modelPoDetails->currentStock = 0;
                    $modelPoDetails->soldQty = 0;
                    $modelPoDetails->save();

                    // Good Received Saved
                    $modelGrn = new GoodReceiveNote;
                    $modelGrn->supplierId= $item->supplierId;
                    $modelGrn->poId = $purchaseOrder->id;
                    $modelGrn->grnNo = 'GRN'.date("Ymdhis");
                    $modelGrn->totalQty = $model->quantity;
                    $modelGrn->totalWeight = 0;
                    $modelGrn->totalPrice = $model->sell_price;
                    $modelGrn->totalDiscount = 0;
                    $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                    $modelGrn->save();

                    // Stock Saved
                    $modelStock = new Stock;
                    $modelStock->grnId = $modelGrn->id;
                    $modelStock->qty = $model->quantity;
                    $modelStock->itemId = $item->id;
                    $modelStock->costPrice = $model->sell_price;
                    $modelStock->discountPercent = 0;
                    $modelStock->status = Stock::STATUS_APPROVED;
                    $modelStock->save();


                    $this->redirect(array('variation','item_id'=>$item_id));
                }
            }
        }
        $this->renderPartial('variationForm',array(
            'model'=>$model,
        ));
    }

    //Duplicated Variations Created
    public function actionDuplicatedVariationCreate($item_id){
        $model = new ItemsVariation();

        $item = new Items();

        $this->performAjaxValidation($model);
        $items = Items::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$item_id)));

        if($_POST['ItemsVariation']){
            $model->attributes=$_POST['ItemsVariation'];

            if(!empty($items)){
                $color = Attributes::getNameById($model->attributes['color']);
                $size = Attributes::getNameById($model->attributes['size']);
                $barcode=substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 10);


                $model->image = $model->attributes['image'];
                $item->itemCode =$model->attributes['item_code'].rand ( 10 , 100 );
               // echo "<pre>";
               // print_r($item);exit();

                $item->barCode = '';
                $item->variation_type = 2;
                $item->itemName = $items->itemName.'/'.$color->name.'/'.$size->name;
                $item->description = $items->itemName.'/'.$color->name.'/'.$size->name;
                $item->itemName_bd = $items->itemName_bd;

                $item->costPrice = $model->attributes['cost_price'];
                $item->sellPrice = $model->attributes['sell_price'];
                $item->catId = $items->catId;
                $item->brandId = $items->brandId;
                $item->supplierId = $items->supplierId;
                $item->taxId = $items->taxId;
                $item->negativeStock = $items->negativeStock;
                $item->isWeighted = $items->isWeighted;
                $item->caseCount = $items->caseCount;
                $item->isEcommerce = $items->isEcommerce;
                /* $item->isFeatured = $items->isFeatured;
                 $item->isNew = $items->isNew;
                 $item->isSpecial = $items->isSpecial;*/
                $item->isParent = $items->isParent;
                $item->variation = $item_id;
                $item->item_quantity = $model->attributes['quantity'];
                $item->status = 1;
                $item->subdeptId = $items->subdeptId;
                $item->deptId = $items->deptId;
                $item->save(false);
                $model->item_id =$item->id;
                $model->item_code =$item->itemCode;

                if($model->save()){
                    // barcode generations : if a item is not weighted and barcode field blank
                    if(empty($_POST['ItemsVariation']['bar_code']))
                    {

                        // create directory if not exists
                        if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com'))
                        {
                            mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com', 0777, true);
                        }

                        // barcode ean13
                        $uiniqueId = date("ymdhis").mt_rand(0,9);
                        $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                        $barcode = new Barcodeean128();
                        $barcode->draw($uiniqueId,$location,"png",true);


                        // update barcode field
                        $command = Yii::app()->db->createCommand();
                        $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$item->id));
                        $command->update('pos_items_variation', array('bar_code'=>$uiniqueId), 'item_id=:item_id', array(':item_id'=>$item->id));

                        /*
                                                $sql = "UPDATE `pos_items_variation` SET `bar_code`= '123465' WHERE `item_id`= ".$item->id;
                                                $command = Yii::app()->db->createCommand($sql);
                                                $command->execute();*/
                    }

                    // Purchase Order Save



                    $purchaseOrder = new PurchaseOrder;
                    $purchaseOrder->branchId = $item->brandId;
                    $purchaseOrder->supplierId = $item->supplierId;
                    $purchaseOrder->poNo = 'PO'.date("Ymdhis");
                    $purchaseOrder->totalQty = $model->quantity;
                    $purchaseOrder->totalPrice = $model->sell_price;
                    $purchaseOrder->totalWeight = 0;
                    $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                    $purchaseOrder->save();

                    //PoDetails Save
                    $modelPoDetails = new PoDetails;
                    $modelPoDetails->poId = $purchaseOrder->id;
                    $modelPoDetails->qty = $model->quantity;
                    $modelPoDetails->itemId = $item->id;
                    $modelPoDetails->costPrice = $model->sell_price;
                    $modelPoDetails->currentStock = 0;
                    $modelPoDetails->soldQty = 0;
                    $modelPoDetails->save();

                    // Good Received Saved
                    $modelGrn = new GoodReceiveNote;
                    $modelGrn->supplierId= $item->supplierId;
                    $modelGrn->poId = $purchaseOrder->id;
                    $modelGrn->grnNo = 'GRN'.date("Ymdhis");
                    $modelGrn->totalQty = $model->quantity;
                    $modelGrn->totalWeight = 0;
                    $modelGrn->totalPrice = $model->sell_price;
                    $modelGrn->totalDiscount = 0;
                    $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                    $modelGrn->save();

                    // Stock Saved
                    $modelStock = new Stock;
                    $modelStock->grnId = $modelGrn->id;
                    $modelStock->qty = $model->quantity;
                    $modelStock->itemId = $item->id;
                    $modelStock->costPrice = $model->sell_price;
                    $modelStock->discountPercent = 0;
                    $modelStock->status = Stock::STATUS_APPROVED;
                    $modelStock->save();

                    //$url=$this->redirect(array('variation','item_id'=>$item_id));
                }
            }
        }
    }


    /********* Items Variation Update**********/
    public function actionVariationUpdate($id, $itemid, $parentitemid){

        $model=$this->loadVariationModel($id);

        $items = Items::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$itemid)));

        if(isset($_POST['yt0']) && $_POST['ItemsVariation']){
            if(!empty($items)){
                $model->attributes=$_POST['ItemsVariation'];
                if(!empty($_FILES['ItemsVariation']['name']['image'])){
                    if(isset($_FILES['ItemsVariation']['name']['image'])) {
                        if(is_uploaded_file($_FILES['ItemsVariation']['tmp_name']['image'])) {
                            $name = $_FILES['ItemsVariation']['name']['image'];
                            $size = $_FILES["ItemsVariation"]["size"]['image'];
                            $ext = explode(".", $name);
                            $allowed_ext = array("png", "jpg", "jpeg");
                            if(!empty($name)){
                                if(in_array($ext['1'], $allowed_ext))
                                {
                                    if($size < (1024*10240))
                                    {
                                        $company = Company::model()->find();

                                        $comapnyNameExplode = str_replace('-', '', $company->name);
                                        $comapnyNameExplode = str_replace('.', '', $comapnyNameExplode);
                                        $comapnyNameExplode = str_replace(' ', '', $comapnyNameExplode);
                                        $comapnyNameExplode = strtolower($comapnyNameExplode);

                                        $RandomAccountNumber = uniqid($comapnyNameExplode.'-'.rand().'-');
                                        // 73 x 100  Small Size
                                        $new_image = '';
                                        $new_name = $RandomAccountNumber . '-small.' . $ext['1'];
                                        $path = 'media/catalog/product/' . $new_name;

                                        /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                        move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                        list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        if($ext['1'] == 'png')
                                        {
                                            $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                        {
                                            $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        $new_width=73;
                                        $new_height = ($height/$width)*73;
                                        $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                        imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                        imagejpeg($tmp_image, $path, 100);
                                        imagedestroy($new_image);
                                        imagedestroy($tmp_image);


                                        // 290 x 400 Medium Size
                                        $new_image = '';
                                        $new_name = $RandomAccountNumber . '-medium.' . $ext['1'];
                                        $path = 'media/catalog/product/' . $new_name;

                                        /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                        move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                        list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        if($ext['1'] == 'png')
                                        {
                                            $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                        {
                                            $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        $new_width=290;
                                        $new_height = ($height/$width)*290;
                                        $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                        imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                        imagejpeg($tmp_image, $path, 100);
                                        imagedestroy($new_image);
                                        imagedestroy($tmp_image);


                                        // 1300 x 1800 Large Size
                                        $new_image = '';
                                        $new_name = $RandomAccountNumber . '-large.' . $ext['1'];
                                        $path = 'media/catalog/product/' . $new_name;

                                        /*$filePath = FileHelper::getUniqueFilenameWithPath($img['image'], 'catalog/product');
                                        move_uploaded_file($_FILES['ItemsVariation']['tmp_name']["$i"]['image'], $filePath);*/
                                        list($width, $height) = getimagesize($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        if($ext['1'] == 'png')
                                        {
                                            $new_image = imagecreatefrompng($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        if($ext['1'] == 'jpg' || $ext['1'] == 'jpeg')
                                        {
                                            $new_image = imagecreatefromjpeg($_FILES["ItemsVariation"]["tmp_name"]['image']);
                                        }
                                        $new_width=1300;
                                        $new_height = ($height/$width)*1300;
                                        $tmp_image = imagecreatetruecolor($new_width, $new_height);
                                        imagecopyresampled($tmp_image, $new_image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
                                        imagejpeg($tmp_image, $path, 100);
                                        imagedestroy($new_image);
                                        imagedestroy($tmp_image);

                                        $model['image'] = $path;
                                        //echo '<img width="80px" src="'.$path.'" />';
                                    }
                                    else
                                    {
                                        $returnValue['warning'] = 'Image File size must be less than 8 MB';
                                        //echo '<p style="color: red">Image File size must be less than 8 MB</p>';
                                    }
                                }
                                else
                                {
                                    $returnValue['invalid'] = 'Invalid Image File';
                                    //echo '<p style="color: red">Invalid Image File</p>';
                                }
                            }
                        }
                    }
                }else{
                    $itemsvariation = ItemsVariation::model()->findByPk($id);
                    $model->image = $itemsvariation->image;
                }

                $model->item_id = $items->id;

                $color = Attributes::getNameById($model->attributes['color']);
                $size = Attributes::getNameById($model->attributes['size']);
                $itemName=explode("/",$items->itemName);

                $newItemCode=$model->item_code;
                if ($model->item_code){
                    $pieace = explode(".", $model->item_code);
                    $newItemCode=$pieace[0].'.'.$size->name;
                }

                $checkExistVariationsId=ItemsVariation::model()->find(array('condition'=>'item_code=:item_code', 'params'=>array(':item_code'=>$newItemCode)));

                if (!empty($checkExistVariationsId) && !empty($_POST['onchangeData'])){
                    $msg = 'Duplicated Item Code ! Please change the Item Code';
                    $_SESSION['variationErrorMessage']=$msg;
                    $this->redirect(array('variation', 'item_id' => $parentitemid));
                }else{
                    unset($_SESSION['variationErrorMessage']);
                }

                $items->costPrice = $model->cost_price;
                $items->sellPrice = $model->sell_price;
                $items->itemCode = $newItemCode;
                $items->item_quantity = $model->quantity;
                $items->itemName =$itemName[0].'/'.$color->name.'/'.$size->name;
                $items->description = $itemName[0].'/'.$color->name.'/'.$size->name;
                $items->update();

                $model->item_code = $newItemCode;
                //$model->bar_code = $items->barCode;*/
                if($model->update()){

                    // barcode generations : if a item is not weighted and barcode field blank
                    if(empty($_POST['ItemsVariation']['bar_code']))
                    {

                        // create directory if not exists
                        if (!file_exists(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com'))
                        {
                            mkdir(dirname(Yii::app()->request->scriptFile).'/media/barcode/pluspoint.com', 0777, true);
                        }

                        // barcode ean13
                        $uiniqueId = date("ymdhis").mt_rand(0,9);
                        $location = Yii::getPathOfAlias("webroot").'/media/barcode/pluspoint.com/'.$uiniqueId.'.png';

                        $barcode = new Barcodeean128();
                        $barcode->draw($uiniqueId,$location,"png",true);

                        // update barcode field
                        $command = Yii::app()->db->createCommand();
                        $command->update('pos_items', array('barCode'=>$uiniqueId), 'id=:id', array(':id'=>$model->id));
                        $command->update('pos_items_variation', array('bar_code'=>$uiniqueId), 'item_id=:item_id', array(':item_id'=>$model->item_id));
                    }


                    // Stock Saved
                    $modelStock = Stock::model()->find(array('condition'=>'itemId=:itemId',
                        'params'=>array(':itemId'=>$items->id)));

                    if(!empty($modelStock)){
                        $modelStock->costPrice = $model->sell_price;
                        $modelStock->discountPercent = 0;
                        $modelStock->qty = $model->quantity;
                        $modelStock->status = Stock::STATUS_APPROVED;
                        $modelStock->update();
                    }else{
                        // Purchase Order Save
                        $purchaseOrder = new PurchaseOrder;
                        $purchaseOrder->branchId = $items->brandId;
                        $purchaseOrder->supplierId = $items->supplierId;
                        $purchaseOrder->poNo = 'PO'.time();
                        $purchaseOrder->totalQty = $model->quantity;
                        $purchaseOrder->totalPrice = $model->sell_price;
                        $purchaseOrder->totalWeight = 0;
                        $purchaseOrder->status = PurchaseOrder::STATUS_AUTO_GRN;
                        $purchaseOrder->save();

                        //PoDetails Save
                        $modelPoDetails = new PoDetails;
                        $modelPoDetails->poId = $purchaseOrder->id;
                        $modelPoDetails->qty = $model->quantity;
                        $modelPoDetails->itemId = $items->id;
                        $modelPoDetails->costPrice = $model->sell_price;
                        $modelPoDetails->currentStock = 0;
                        $modelPoDetails->soldQty = 0;
                        $modelPoDetails->save();

                        // Good Received Saved
                        $modelGrn = new GoodReceiveNote;
                        $modelGrn->supplierId= $items->supplierId;
                        $modelGrn->poId = $purchaseOrder->id;
                        $modelGrn->grnNo = 'GRN'.time();
                        $modelGrn->totalQty = $model->quantity;
                        $modelGrn->totalWeight = 0;
                        $modelGrn->totalPrice = $model->sell_price;
                        $modelGrn->totalDiscount = 0;
                        $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                        $modelGrn->save();

                        // Stock Saved
                        $modelStock = new Stock;
                        $modelStock->grnId = $modelGrn->id;
                        $modelStock->qty = $model->quantity;
                        $modelStock->itemId = $items->id;
                        $modelStock->costPrice = $model->sell_price;
                        $modelStock->discountPercent = 0;
                        $modelStock->status = Stock::STATUS_APPROVED;
                        $modelStock->save();


                    }


                    // Good Received Saved
                    $modelGrn = GoodReceiveNote::model()->find(array('condition'=>'id=:id',
                        'params'=>array(':id'=>$modelStock->grnId)));
                    if(!empty($modelGrn)){
                        $modelGrn->supplierId= $items->supplierId;
                        $modelGrn->totalQty = $model->quantity;
                        $modelGrn->totalWeight = 0;
                        $modelGrn->totalPrice = $model->sell_price;
                        $modelGrn->totalDiscount = 0;
                        $modelGrn->status = GoodReceiveNote::STATUS_APPROVED;
                        $modelGrn->update();

                    }

                    // Purchase Order Save
                    $purchaseOrder = PurchaseOrder::model()->find(array('condition'=>'id=:id',
                        'params'=>array(':id'=>$modelGrn->poId)));

                    if(!empty($purchaseOrder)){
                        //$purchaseOrder->branchId = $model->brandId;
                        $purchaseOrder->supplierId = $items->supplierId;
                        $purchaseOrder->totalQty = $model->quantity;
                        $purchaseOrder->totalPrice = $model->sell_price;
                        $purchaseOrder->totalWeight = 0;
                        $purchaseOrder->update();
                    }


                    //PoDetails Save
                    $modelPoDetails = PoDetails::model()->find(array('condition'=>'poId=:poId',
                        'params'=>array(':poId'=>$modelGrn->poId)));
                    if(!empty($modelPoDetails)){
                        $modelPoDetails->qty = $model->quantity;
                        $modelPoDetails->itemId = $items->id;
                        $modelPoDetails->costPrice = $model->sell_price;
                        $modelPoDetails->currentStock = 0;
                        $modelPoDetails->soldQty = 0;
                        $modelPoDetails->update();
                    }

                    $this->redirect(array('variation','item_id'=>$parentitemid));
                }
            }
        }
        $this->renderPartial('variationForm',array(
            'model'=>$model,
        ));
    }
    /********* Items Variation Lists By Item ID**********/
    public function actionVariation($item_id){
        $items = Items::model()->findAll('variation=:variation',array(':variation'=>$_REQUEST['item_id']));
        $variationLists = [];
        foreach ($items as $item){
            $variationLists[] = ItemsVariation::model()->find(array('condition'=>'item_id=:item_id', 'params'=>array(':item_id'=>$item->id)));
        }

        $this->render('variationLists',array(
            'variationLists'=>$variationLists,
        ));
    }

    /********* Items Variation Delete By ID**********/
    public function actionDeletevariation($id, $itemid, $parentitemid){

        ItemAttributes::model()->deleteAll(array('condition'=>'itemId=:itemId','params'=>array(':itemId'=>$itemid)));
        ItemImages::model()->deleteAll(array('condition'=>'itemId=:itemId','params'=>array(':itemId'=>$itemid)));
        Items::model()->deleteAll(array('condition'=>'id=:id','params'=>array(':id'=>$itemid)));

        $variationLists = ItemsVariation::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$id)));

        if(!empty($variationLists->image)){
            $imageExplode = explode('large.', $variationLists->image);

            $allSizeImage = [
                $imageExplode[0].'large.'.$imageExplode[1],
                $imageExplode[0].'medium.'.$imageExplode[1],
                $imageExplode[0].'small.'.$imageExplode[1],
            ];
            foreach ($allSizeImage as $image){
                $mainPath = ltrim($image, '/');
                // Check file exist or not
                if( file_exists($mainPath) ){
                    // Remove file
                    unlink($mainPath);
                }
            }
        }

        ItemsVariation::model()->deleteAll(array('condition'=>'id=:id','params'=>array(':id'=>$id)));
        $this->redirect('variation?item_id='.$parentitemid);
    }

    /********* Items Code Validation **********/
    public function actionItemCodeValidation(){

        if(!empty($_REQUEST['itemcode'])){
            $items = Items::model()->find('itemCode=:itemCode',array(':itemCode'=>$_REQUEST['itemcode']));
           if(!empty($items)){
               echo 1;
           }
        }

    }

    /********* Bar Code Validation **********/
    public function actionBarCodeValidation(){

        if(!empty($_REQUEST['barcode'])){
            $items = Items::model()->find('barCode=:barCode',array(':barCode'=>$_REQUEST['barcode']));
            if(!empty($items)){
                echo 1;
            }
        }

    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        if (isset($_GET['pageSize'])) {
            //
            // pageSize will be set on user's state
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            //
            // unset the parameter as it
            // would interfere with pager
            // and repetitive page size change
            unset($_GET['pageSize']);
        }

        $model=new Items('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Items']))
            $model->attributes=$_GET['Items'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Items the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Items::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    public function loadVariationModel($id)
    {
        $model=ItemsVariation::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Items $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='items-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrintStockTransfer()
    {
        $msg = $transferNo =$printId= $type = '';
        $toBranchId='';
        if(isset($_REQUEST['transferNo']) && !empty($_REQUEST['transferNo'])) $transferNo = $_REQUEST['transferNo'];
        if(isset($_REQUEST['type']) && !empty($_REQUEST['type'])) $type = $_REQUEST['type'];

        $model=new StockTransfer();
        if(isset($_POST['StockTransfer']))
        {
            $transferNo=$_POST['StockTransfer']['transferNo'];
            $toBranchId=$_POST['StockTransfer']['toBranchId'];
            $stockTransferModel = StockTransfer::model()->find('transferNo=:transferNo',array(':transferNo'=>$_POST['StockTransfer']['transferNo']));
            if(!empty($stockTransferModel)) $printId = $stockTransferModel->id;
            else $msg = '<div class="notification note-error"><p>Wrong Invoice No.</p></div>';
        }
        $this->render('printStockT',array(
            'model'=>$model,
            'transferNo'=>$transferNo,
            'printId'=>$printId,
            'toBranchId'=>$toBranchId,
            'msg'=>$msg,'type'=>$type,
        ));
    }
}
