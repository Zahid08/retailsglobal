/*
SQLyog Ultimate v8.5 
MySQL - 5.5.8 : Database - dbinfo
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dbinfo` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dbinfo`;

/*Table structure for table `tbl_country` */

DROP TABLE IF EXISTS `tbl_country`;

CREATE TABLE `tbl_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL,
  `crDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_country` */

insert  into `tbl_country`(`id`,`name`,`isActive`,`crDate`) values (1,'Bangladesh',1,'2011-05-24'),(2,'India',1,'2011-05-24'),(3,'China',0,'2011-05-24');

/*Table structure for table `tbl_district` */

DROP TABLE IF EXISTS `tbl_district`;

CREATE TABLE `tbl_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `couId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `isActive` int(11) NOT NULL,
  `crDate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tbl_district_cid` (`couId`),
  CONSTRAINT `FK_tbl_district_cid` FOREIGN KEY (`couId`) REFERENCES `tbl_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_district` */

insert  into `tbl_district`(`id`,`couId`,`name`,`isActive`,`crDate`) values (1,1,'Dhaka',1,'2011-05-24'),(2,1,'Gazipur',1,'2011-05-24'),(3,1,'Narayanganj',1,'2011-05-24'),(4,1,'Bhola',1,'2011-05-24'),(5,1,'Khulna',0,'2011-05-24'),(6,1,'Pabna',0,'2011-05-24'),(7,2,'Bihar',1,'2011-05-24'),(8,2,'Urishwa',1,'2011-05-24'),(9,2,'Agortala',0,'2011-05-24');

/*Table structure for table `tbl_info` */

DROP TABLE IF EXISTS `tbl_info`;

CREATE TABLE `tbl_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `distId` int(11) NOT NULL,
  `cunId` int(11) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `crDate` date NOT NULL,
  `imagePath` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tbl_info_cid` (`cunId`),
  KEY `FK_tbl_info_did` (`distId`),
  CONSTRAINT `FK_tbl_info_cid` FOREIGN KEY (`cunId`) REFERENCES `tbl_country` (`id`),
  CONSTRAINT `FK_tbl_info_did` FOREIGN KEY (`distId`) REFERENCES `tbl_district` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_info` */

insert  into `tbl_info`(`id`,`name`,`address`,`distId`,`cunId`,`mobile`,`email`,`crDate`,`imagePath`) values (1,'Hasan','',1,1,'01678128457','hassan@gmail.com','2011-05-26','admin-habib.jpg'),(2,'Hasan','',1,1,'01678128457','hassan@gmail.com','2011-05-26','admin-Email-Icon.png'),(3,'Hasan','',1,1,'35436436','fdhfdnhf','2011-05-26','admin-Email-Icon.png'),(4,'hfghgf','jtrjntfrgjfj',1,1,'01678128457','hfghgf','2011-05-26','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
