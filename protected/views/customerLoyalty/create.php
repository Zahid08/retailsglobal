<?php
/* @var $this CustomerLoyaltyController */
/* @var $model CustomerLoyalty */

$this->breadcrumbs=array(
	'Customer Loyalties'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustomerLoyalty', 'url'=>array('index')),
	array('label'=>'Manage CustomerLoyalty', 'url'=>array('admin')),
);
?>

<h1>Create CustomerLoyalty</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>