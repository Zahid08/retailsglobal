<?php
/* @var $this CustomerLoyaltyController */
/* @var $model CustomerLoyalty */

$this->breadcrumbs=array(
	'Customer Loyalties'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustomerLoyalty', 'url'=>array('index')),
	array('label'=>'Create CustomerLoyalty', 'url'=>array('create')),
	array('label'=>'View CustomerLoyalty', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustomerLoyalty', 'url'=>array('admin')),
);
?>

<h1>Update CustomerLoyalty <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>