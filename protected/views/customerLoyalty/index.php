<?php
/* @var $this CustomerLoyaltyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customer Loyalties',
);

$this->menu=array(
	array('label'=>'Create CustomerLoyalty', 'url'=>array('create')),
	array('label'=>'Manage CustomerLoyalty', 'url'=>array('admin')),
);
?>

<h1>Customer Loyalties</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
