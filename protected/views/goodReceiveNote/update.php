<script type="text/javascript" language="javascript">
$(function() 
{
	// after grnNo inputed
	$("#grnNo").blur(function() 
	{
		$("#grn_loader").show("slow");
		var grnNo  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/grnUpdateProductsByGrn/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				grnNo : grnNo,
				type : 'updateOnly'
			},
			success: function(data) 
			{
				$("#grn_loader").hide();
				$("#grn_dynamic_container").html(data);
			},
			error: function() {}
		});
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Goods Receipt</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'good-receive-note-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Good Receive Update</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'grnNo'); ?>
					<?php $this->widget('CAutoComplete',array(
								 'model'=>$model,
								 'id'=>'grnNo',
								 'attribute' => 'grnNo',
								 //name of the html field that will be generated
								 //'name'=>'poId', 
											 //replace controller/action with real ids
								 'value'=>($model->grnNo)?$model->grnNo:'',
					
								 'url'=>array('ajax/autoCompleteGrnNo'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
								 ));
							?>
					<?php echo $form->error($model,'grnNo'); ?>
					<span id="grn_loader" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
				</div>
			</div>
			
			 <div id="grn_dynamic_container">   
				 <div class="row">
					<div style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th>Sl. No</th>
								<th>Item Code</th>
								<th class="hidden-480">Description</th>
								<th class="hidden-480">Quantity</th>
								<th class="hidden-480">Cost Price</th>
								<th class="hidden-480">Sell Price</th>
								<th class="hidden-480">Received Quantity</th>
								<th class="hidden-480">Disc %</th>
								<th class="hidden-480">Disc Amount</th>
								<th>Net Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#</td>
								<td>-</td>
								<td>- </td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480"><?php echo CHtml::textField('rqty',0,array('style'=>'width:100px; height:15px;'));?></td>
								<td class="hidden-480"><?php echo CHtml::textField('discount',0,array('style'=>'width:50px; height:15px;'));?></td>
								<td>0</td>
								<td>0</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
			
			<div class="row" style="background:#eeeeee; padding:10px;">
				<div class="fileld_equal">
					<?php echo $form->labelEx($model,'remarks'); ?>
					<?php echo $form->textArea($model,'remarks', array('class'=>'m-wrap combo','style'=>'background:#fff;width:100%;height:150px;')); ?>
					<?php echo $form->error($model,'remarks'); ?>
				</div>
				<div class="fileld_equal" style="margin-left:30px;">
					<?php echo $form->labelEx($model,'message'); ?>
					<?php echo $form->textArea($model,'message', array('class'=>'m-wrap combo','style'=>'background:#fff;width:100%;height:150px;')); ?>
					<?php echo $form->error($model,'message'); ?>
				</div>
				<div class="fileld_equal_right" style="margin-left:30px; background:#fff; border:1px dashed #ddd; padding:10px;">
					<span><em>After Update :</em></span>
					<ul class="unstyled amounts" style="background:#f7f7f7;width:60%; padding:5px;margin:5px 0px;border:1px dotted #ccc;">
						<li><strong>Total Quantity :</strong> <span style="font-size:20px;" id="grandQty">0</span><?php echo $form->hiddenField($model,'totalQty', array('id'=>'totalQty','value'=>0));?></li>
						<li><strong>Total Weight :</strong> <span style="font-size:20px;" id="grandWeight">0</span><?php echo $form->hiddenField($model,'totalWeight', array('id'=>'totalWeight','value'=>0));?></li>
					</ul>
					<ul class="unstyled amounts" style="background:#f7f7f7;width:96%; padding:5px;margin-top:10px;border:1px dotted #ccc;">
						<li><strong>Total Amount :</strong> <span style="font-size:20px;" id="totalAmount">0</span><?php echo $form->hiddenField($model,'totalPrice', array('id'=>'totalPrice','value'=>0));?></li>
						<li><strong>Total Discount :</strong> <span style="font-size:20px;" id="grandDiscount">0</span><?php echo $form->hiddenField($model,'totalDiscount', array('id'=>'totalDiscount','value'=>0));?></li>
						<li style=" margin:5px 0px;border-top:1px solid #ccc;"><strong>Grand Total :</strong> <span style="font-size:20px;" id="grandTotal">0</span></li>
					</ul>
				</div>
			</div>
        </div>        
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit GRN' : 'Update', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   