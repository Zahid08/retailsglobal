<?php
	// print po
	if(!empty($printId) && is_numeric($printId))
		 $this->renderPartial('_printGrn', array('printId'=>$printId)); 
?>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Goods Receipt Note</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchase-order-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Direct Goods Receipt Note</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span5">
					<?php echo $form->labelEx($model,'supplierId'); ?>
					<?php echo $form->dropDownList($model,'supplierId', Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), 
							   array('id'=>'supplierId','class'=>'m-wrap combo combobox','prompt'=>'Select Supplier',
									 'onchange'=>'js:$("#ajax_loadersupplier").show()',
									  'ajax' => array('type'=>'POST', 
														//'dataType'=>'json', 
														'url'=>CController::createUrl('ajax/directGrnProductsBySupplier'),
														'success'=>'function(data) 
														{
															$("#ajax_loadersupplier").hide();
															$("#purchase_dynamic_container").html(data);
														}',
										)   
								 )); ?>
					<span id="ajax_loadersupplier" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
					<?php echo $form->error($model,'supplierId'); ?>
				</div>
				<div class="fileld_right">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> PO Number</th>
                                <th><i class="icon-bookmark"></i> GRN Number</th>
								<th><i class="icon-bookmark"></i> Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>PO<?php echo date("Ymdhis"); echo $form->hiddenField($model,'poNo', array('value'=>'PO'.date("Ymdhis")));?></td>
                                <td>GN<?php echo date("Ymdhis"); echo CHtml::hiddenField('grnNo','GN'.date("Ymdhis"),array());?></td>
								<td><?php echo date("Y-m-d");?></td>
							</tr>
					   </tbody>
					 </table>
				</div>
			</div>
            
            <div class="row">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th>Total Quantity</th>
                            <th>Total Weight</th>
                            <th>Grand Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="highlight">
                                <div class="success"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandQty">0</span>
								<?php echo $form->hiddenField($model,'totalQty', array('id'=>'totalQty','value'=>0));?>
                            </td>
                            <td class="highlight">
                                <div class="info"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandWeight">0</span>
								<?php echo $form->hiddenField($model,'totalWeight', array('id'=>'totalWeight','value'=>0));?>
                            </td>
                            <td class="highlight">
                                <div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandTotal">0</span>
								<?php echo $form->hiddenField($model,'totalPrice', array('id'=>'totalPrice','value'=>0));?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
			
			<div id="purchase_dynamic_container">
				<div class="row" style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th>Sl. No</th>
								<th>Item Code</th>
								<th class="hidden-480">Description</th>
								<th class="hidden-480">Quantity</th>
								<th class="hidden-480">Cost Price</th>
								<th class="hidden-480">Sell Price</th>
								<th class="hidden-480">Case Count</th>
								<th class="hidden-480">Current Stock</th>
								<th class="hidden-480">15 Days Movement</th>
								<th>Net Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#</td>
								<td>-</td>
								<td>-</td>
								<td class="hidden-480"><?php echo CHtml::textField('qty',0,array('style'=>'width:50px; height:15px;'));?></td>
								<td class="hidden-480">0<?php // echo CHtml::textField('costPrice',0,array('style'=>'width:100px; height:15px;'));?></td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td>0</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			
			<div class="row" style="background:#eeeeee; padding:10px;">
				<div class="fileld_equal">
					<?php echo $form->labelEx($model,'remarks'); ?>
					<?php echo $form->textArea($model,'remarks', array('class'=>'m-wrap combo','style'=>'background:#fff;width:100%;height:50px;')); ?>
					<?php echo $form->error($model,'remarks'); ?>
				</div>
				<div class="fileld_equal" style="margin-left:30px;">
					<?php echo $form->labelEx($model,'message'); ?>
					<?php echo $form->textArea($model,'message', array('class'=>'m-wrap combo','style'=>'background:#fff;width:100%;height:50px;')); ?>
					<?php echo $form->error($model,'message'); ?>
				</div>
			</div>
        </div>        
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit Order' : 'Update', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
		<!--<button type="button" class="btn blue" onclick="$('#purchase-order-form').get(0).submit();" style="margin:10px;">Submit Order</button>-->
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   