<?php
/* @var $this GoodReceiveNoteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Good Receive Notes',
);

$this->menu=array(
	array('label'=>'Create GoodReceiveNote', 'url'=>array('create')),
	array('label'=>'Manage GoodReceiveNote', 'url'=>array('admin')),
);
?>

<h1>Good Receive Notes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
