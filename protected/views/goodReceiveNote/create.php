<?php
/* @var $this GoodReceiveNoteController */
/* @var $model GoodReceiveNote */

$this->breadcrumbs=array(
	'Good Receive Notes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GoodReceiveNote', 'url'=>array('index')),
	array('label'=>'Manage GoodReceiveNote', 'url'=>array('admin')),
);
?>

<h1>Create GoodReceiveNote</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>