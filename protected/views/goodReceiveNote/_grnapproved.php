<script type="text/javascript" language="javascript">
$(function() 
{
	// quantity operations
	$("#checkAll").change(function() 
	{
		if((document.getElementById('checkAll').checked))
		{
			var box = document.getElementsByClassName('checkedid');
			for (var i = 0; i < box.length; i++) {
				box[i].checked = true;
			}
		}
		else
		{
			var box = document.getElementsByClassName('checkedid');
			for (var i = 0; i < box.length; i++) {
				box[i].checked = false;
			}
		}
	});
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Goods Receipt</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'good-receive-note-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model);?>

<?php if(!empty($msg)) : echo $msg;
else : ?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Goods Receipt Process (GRP)</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="fileld_right">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> Total GRN</th>
								<th><i class="icon-bookmark"></i> Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo (!empty($todayGrn))?count($todayGrn):0;?></td>
								<td><?php echo date("Y-m-d");?></td>
							</tr>
					   </tbody>
					 </table>
				</div>
			</div>

			 <div id="grnapp_dynamic_container">    
				 <div class="row">
					<div style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th><input type="checkbox" id="checkAll" class="group-checkable" data-set=".actions_table_1 .checkboxes" /></th>
								<th>Sl. No</th>
								<th>GRN No.</th>
								<th class="hidden-480">GRN Date</th>
								<th class="hidden-480">PO No.</th>
								<th class="hidden-480">Supplier</th>
								<th class="hidden-480">GRN Quantity</th>
								<th class="hidden-480">GRN Amount</th>
								<th class="hidden-480">GRN Discount</th>
								<th class="hidden-480">GRN Weight</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$srl = 0;
						if(!empty($todayGrn)) :
							foreach($todayGrn as $key=>$data) : $srl++; ?>
							<tr>
								<td><input type="checkbox" class="checkedid" data-set=".actions_table_1 .checkboxes" name="checkedid<?php echo $key;?>" value="1" /></td>
								<td><?php echo $srl;?></td>
								<td><?php echo $data->grnNo;
										  echo CHtml::hiddenField('pk['.$key.']',$key, array());
										  echo CHtml::hiddenField('grnId['.$key.']',$data->id, array());
									?>
								</td>
								<td><?php echo $data->grnDate;?></td>
								<td><?php echo $data->po->poNo;?></td>
<!--								<td class="hidden-480">--><?php //echo $data->supplier->name;?><!--</td>-->
								<td class="hidden-480"><?php echo 'Pluspoint';?></td>
								<td class="hidden-480"><?php echo $data->totalQty;?></td>
								<td class="hidden-480"><?php echo round($data->totalPrice,2);?></td>
								<td class="hidden-480"><?php echo round($data->totalDiscount,2);?></td>
								<td class="hidden-480"><?php echo $data->totalWeight;?></td>
							</tr>
							<?php endforeach; 
							else : ?>
								<tr>
									<td><input type="checkbox" class="group-checkable" data-set=".actions_table_1 .checkboxes" /></td>
									<td>#</td>
									<td>#</td>
									<td>0000-00-00 </td>
									<td>#</td>
									<td class="hidden-480">-</td>
									<td class="hidden-480">0</td>
									<td class="hidden-480">0</td>
									<td class="hidden-480">0</td>
									<td class="hidden-480">0</td>
								</tr>
						 <?php endif;?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
        </div>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Approve GRN' : 'Update', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   