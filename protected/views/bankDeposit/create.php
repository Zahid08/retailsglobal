<?php
/* @var $this BankDepositController */
/* @var $model BankDeposit */

$this->breadcrumbs=array(
	'Bank Deposits'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BankDeposit', 'url'=>array('index')),
	array('label'=>'Manage BankDeposit', 'url'=>array('admin')),
);
?>

<h1>Create BankDeposit</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>