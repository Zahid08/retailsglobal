<?php
/* @var $this BankDepositController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bank Deposits',
);

$this->menu=array(
	array('label'=>'Create BankDeposit', 'url'=>array('create')),
	array('label'=>'Manage BankDeposit', 'url'=>array('admin')),
);
?>

<h1>Bank Deposits</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
