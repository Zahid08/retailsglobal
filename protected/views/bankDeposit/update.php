<?php
/* @var $this BankDepositController */
/* @var $model BankDeposit */

$this->breadcrumbs=array(
	'Bank Deposits'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BankDeposit', 'url'=>array('index')),
	array('label'=>'Create BankDeposit', 'url'=>array('create')),
	array('label'=>'View BankDeposit', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BankDeposit', 'url'=>array('admin')),
);
?>

<h1>Update BankDeposit <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>