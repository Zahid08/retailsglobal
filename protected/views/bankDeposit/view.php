<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Finance<i class="icon-angle-right"></i></li>	
	<li>Details Bank Deposit # <?php echo $model->bank->name; ?></li>
</ul>

<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(	
		array(
			'name'=>'bankId',
			'value'=>$model->bank->name,			
		),
		array(
			'label'=>'Account No',			
			'value'=>$model->bank->accountNo,			
		),
		
		
		'amount',
		'depositDate',
		'comments',
		array(            
					'label'=>'Status',  
					'value'=>Lookup::item('Status',$model->status),
				  ),
		'crAt',
		array(            
					'label'=>'Created By',  
					'value'=>$model->crBy0->username,),
				'moAt',
		array(            
					'label'=>'Modified By',  
					'type'=>'raw',
					'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
				'rank',
			),
)); ?>
</div>
