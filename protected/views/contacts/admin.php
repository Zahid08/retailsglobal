<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Contacts<i class="icon-angle-right"></i></li>
    <li>Contact<i class="icon-angle-right"></i></li>
    <li>Manage Contact</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
        'id'=>'contents-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'type' => 'striped bordered condensed',
        'template' => '{summary}{items}{pager}',
        'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
        'columns'=>array(
            array('header'=>'Sl.',
                'value'=>'$row+1',
            ),

            array(
                'name'=>'first_name',
                'value'=>'$data->first_name',
            ),

            array(
                'name'=>'last_name',
                'value'=>'$data->last_name',
            ),
            array(
                'name'=>'email',
                'value'=>'$data->email',
            ),
            array(
                'name'=>'phone_number',
                'value'=>'$data->phone_number',
            ),
            array(
                'name'=>'message',
                'value'=>'$data->message',
            ),array(
                'name'=>'crAt',
                'value'=>'$data->crAt',
            ),
        ),
    )); ?>
</div>