<?php 	
	Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/css/thickbox.css');	
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/thickbox.js');
?>

<script type="text/javascript" language="javascript">
	//-------showing on popup ------//
	function ShowPopupEditContAction(id) {		
		window.top.tb_remove(); 
		var bseurl = "<?php echo Yii::app()->baseUrl.'/index.php/contActions/UpdateDesc/id/';?>"+id;
		var url = bseurl+"?"+"TB_iframe=true&height=200&width=500";
		tb_show(" # Update Controller Actions", url, false);	
	}
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
    <li>User Permission <i class="icon-angle-right"></i></li>
    <li>Controller Actions List</li>
</ul>
<div class="form">
	<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>

	<?php 
	$this->widget('bootstrap.widgets.TbExtendedGridView',array(
		'id'=>'cont-actions-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
	    'type' => 'striped bordered condensed',
		'template' => '{summary}{items}{pager}',
		'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
		'columns'=>array(
			array(            
				'name'=>'contId',
				'value'=>'$data->cont->name',
				'filter'=>Controllers::getAllcontrollers(),
			),
			array(            
				'name'=>'name',
				'value'=>'$data->name',
			),
			array(            
				'name'=>'description',
				'value'=>'$data->description',
			),
			array(            
				'name'=>'status',
				'value'=>'Lookup::item("Status", $data->status)',
				'filter'=>Lookup::items("Status"),
			),
			array(
				'class'=>'bootstrap.widgets.TbButtonColumn',
				'template'=>'{update}',
				'buttons' => array(
					'update' => array(					  
					  'url'=>'$data->id',
					  'visible'=>'true',
					  'options'=>array('class'=>'updatebtns'),
					  'click'=>'js: function(){ ShowPopupEditContAction($(this).attr("href")); return false; }',
					),
				 ),
				'header'=>CHtml::dropDownList('pageSize',
					$pageSize,
					array(5=>5,10=>10,20=>20,50=>50,100=>100),
					array(
					'onchange'=>
					"$.fn.yiiGridView.update('cont-actions-grid',{ data:{pageSize: $(this).val() }})",
				)),
			),	
		),
	)); ?>
</div>