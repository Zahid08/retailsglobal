<div class="form">
<?php 
$form = $this->beginWidget('CActiveForm', array(
				'id'=>'contactions-form',
				'action' => Yii::app()->createUrl('contActions/UpdateDesc'),
				'enableAjaxValidation'=>false,
	)); ?>  
    
    <?php echo $form->errorSummary($model); ?>
    
    <?php if(!empty($msg)) :?>    
    <div class="form_notify note-success">
        <p><?php echo $msg;?></p>
    </div>
    <?php endif;?>   
	
    <FIELDSET class='fieldset'>
        <LEGEND class='legend'>Update Information</LEGEND>
        
        <div class="row">
            <label for="contDesc" style="width:100%; float:left; text-align:left;">Description</label>
            <?php echo $form->textArea($model,'description', array('style'=>'width:450px')); ?>
            <?php echo $form->error($model,'description'); ?>
        </div>	
    </FIELDSET>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class'=>'btn blue')); ?>
        <input type="hidden" name="id" value="<?php echo $model->id;?>" />
    </div>
    <?php $this->endWidget(); ?>
</div>