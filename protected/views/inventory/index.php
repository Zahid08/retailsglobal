<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Inventory</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'inventory-form',
        'enableAjaxValidation'=>true,
    )); ?>
    
    <?php if(!empty($msg)) : echo $msg;
    else :?>
    <div class="notification note-error">
        <p>Please generate all department, sub department and category wise stock report before continue !</p>
    </div>
    <?php endif;?>
    <div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">Inventory Initialize</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
				<div class="row">
				<div class="span4">
					<?php echo CHtml::label('Department','');?>
					<?php echo CHtml::dropDownList('deptId','',Department::getAllDepartment(Department::STATUS_ACTIVE), 
							  array('class'=>'m-wrap combo combobox','prompt'=>'Select Department',
									'id'=>'deptId',
									//'options'=>array($dept=>array('selected'=>'selected')),
									'onchange'=>'js:$("#ajax_loaderdept").show()',
									'ajax' => array('type'=>'POST', 
													'data'=>array('deptId'=>'js:this.value'),  
													'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
													'success'=>'function(data) 
													{
														$("#ajax_loaderdept").hide();
														$("#subdeptId").html(data);
													}',
									)
							)); ?>
						 <span id="ajax_loaderdept" style="display:none;">
							<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
						 </span>
				</div>
               <div class="span8">
                   <div class="fileld_right">
                       <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                               <tr>
                                   <th><i class="icon-bookmark"></i> Inventory Number</th>
                                   <th><i class="icon-bookmark"></i> Date</th>
                               </tr>
                           </thead>
                           <tbody>
                               <tr>
                                   <td>IV<?php echo date("Ymdhis"); echo CHtml::hiddenField('invNo','IV'.date("Ymdhis"),array());?></td>
                                   <td><?php echo date("Y-m-d");?></td>
                               </tr>
                           </tbody>
                       </table>
                   </div>
               </div>
            </div>
				<div class="row">
				<div class="span4">
				 <?php echo CHtml::label('Sub Department','');?>
				 <?php echo CHtml::dropDownList('subdeptId','',Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), 
								  array('class'=>'m-wrap span','prompt'=>'Select Sub Department',
										'id'=>'subdeptId', 
					)); ?>
				
				</div>
            </div>
			</div>            
        </div>
		<div class="row buttons">
			<?php echo CHtml::submitButton('Initialize', array('class'=>'btn blue')); ?>
		</div>
    </div>
    <?php $this->endWidget(); ?>
</div>