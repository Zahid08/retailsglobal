<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Inventory</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
	<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'inventory-form',
        'enableAjaxValidation'=>true,
    )); ?>
    
    <?php if(!empty($msg)) : echo $msg;
    else :?>
    <div class="notification note-error">
        <p>Please initialize all department, sub department and category before close !</p>
    </div>
    <?php endif;?>
    <div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Inventory Close</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active span4" id="tab_1_1">
            <?php /*?><div class="row">
            	<?php 
					if($count>0)
						echo '<b><i>Reset Inventory for almost '.$count.' data and Initialize it !</i></b>';
                    else echo '<b><i>Inventory empty, Initialize it !</i></b>';
				?>
            </div><?php */?>
            <div class="row">	
                <?php echo CHtml::label('Department','');?>
                <?php echo CHtml::dropDownList('deptId','',Department::getAllDepartment(Department::STATUS_ACTIVE), 
                          array('class'=>'m-wrap combo combobox','prompt'=>'Select Department',
                                'id'=>'deptId',
                                //'options'=>array($dept=>array('selected'=>'selected')),
                                'onchange'=>'js:$("#ajax_loaderdept").show()',
                                'ajax' => array('type'=>'POST', 
                                                'data'=>array('deptId'=>'js:this.value'),  
                                                'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
                                                'success'=>'function(data) 
                                                {
                                                    $("#ajax_loaderdept").hide();
                                                    $("#subdeptId").html(data);
                                                }',
                                )
                        )); ?>
                     <span id="ajax_loaderdept" style="display:none;">
                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                     </span>
            </div>
            <div class="row">
             <?php echo CHtml::label('Sub Department','');?>
             <?php echo CHtml::dropDownList('subdeptId','',Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), 
                              array('class'=>'m-wrap span','prompt'=>'Select Sub Department',
                                    'id'=>'subdeptId', 
                            )); ?>
            
        	</div>
		</div>            
       </div>
	   <div class="row buttons">
			<?php echo CHtml::submitButton('Close', array('class'=>'btn blue')); ?>
		</div>
    </div>
    <?php $this->endWidget(); ?>
</div>