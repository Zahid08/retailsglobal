<?php
/* @var $this TemporaryWorkerWidrawController */
/* @var $model TemporaryWorkerWidraw */

$this->breadcrumbs=array(
	'Temporary Worker Widraws'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TemporaryWorkerWidraw', 'url'=>array('index')),
	array('label'=>'Create TemporaryWorkerWidraw', 'url'=>array('create')),
	array('label'=>'View TemporaryWorkerWidraw', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TemporaryWorkerWidraw', 'url'=>array('admin')),
);
?>

<h1>Update TemporaryWorkerWidraw <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>