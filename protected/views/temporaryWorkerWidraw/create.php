<?php
/* @var $this TemporaryWorkerWidrawController */
/* @var $model TemporaryWorkerWidraw */

$this->breadcrumbs=array(
	'Temporary Worker Widraws'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TemporaryWorkerWidraw', 'url'=>array('index')),
	array('label'=>'Manage TemporaryWorkerWidraw', 'url'=>array('admin')),
);
?>

<h1>Create TemporaryWorkerWidraw</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>