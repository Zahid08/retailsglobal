<?php
/* @var $this TemporaryWorkerWidrawController */
/* @var $data TemporaryWorkerWidraw */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branchId')); ?>:</b>
	<?php echo CHtml::encode($data->branchId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bankId')); ?>:</b>
	<?php echo CHtml::encode($data->bankId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('wkId')); ?>:</b>
	<?php echo CHtml::encode($data->wkId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('twNo')); ?>:</b>
	<?php echo CHtml::encode($data->twNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount')); ?>:</b>
	<?php echo CHtml::encode($data->amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	*/ ?>

</div>