<?php
/* @var $this TemporaryWorkerWidrawController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Temporary Worker Widraws',
);

$this->menu=array(
	array('label'=>'Create TemporaryWorkerWidraw', 'url'=>array('create')),
	array('label'=>'Manage TemporaryWorkerWidraw', 'url'=>array('admin')),
);
?>

<h1>Temporary Worker Widraws</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
