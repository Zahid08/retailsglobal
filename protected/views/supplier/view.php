<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Supplier <i class="icon-angle-right"></i></li>
    <li>Details Supplier # <?php echo $model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'suppId',
		'name',
		'address',
		'city',
		'email',
		'phone',
		'crType',
		'creditDay',
		'consignmentDay',
		array(            
					'label'=>'Status',  
					'value'=>Lookup::item('Status',$model->status),
				  ),
				'crAt',
		array(            
					'label'=>'Created By',  
					'value'=>$model->crBy0->username,),
				'moAt',
		array(            
					'label'=>'Modified By',  
					'type'=>'raw',
					'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
				'rank',
			),
)); ?>
</div>
