<?php
    $protocol = 'http://';
    if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https://';
?>
<style>
	table, tr,td{
		border: 0;
	}
</style>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Arial, sans-serif;">
	<tbody>
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
					<tr>
						<td bgcolor="#70bbd9" align="center" style="padding: 20px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
						   <?php if(!empty($companyModel)) : ?>
							   <img src="<?php echo $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.$companyModel->logo;?>" style="height:78px;" />	
						   <?php endif; ?>
						</td>
					</tr>
					<tr>
						<td bgcolor="#54afd3" align="center">
							<table>
								<tbody>
									<tr>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $protocol.$companyModel->domain;?>" target="_blank" title="Home">Home</a>
										</td>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $protocol.$companyModel->domain;?>" target="_blank" title="Home">Support</a>
										</td>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $protocol.$companyModel->domain;?>" target="_blank" title="Home">Blog</a>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<table cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr>
										<td bgcolor="#d0d0d0" width="4px"></td>
										<td bgcolor="#eee">
											<h6 style="margin:10px 20px; color:#555; font-weight:500;font-size: 12px;">
											You are currently active user.<br/><br/>
											Thank you for stay with us.
											</h6>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr>
										<td bgcolor="#dedede" width="4px"></td>
										<td bgcolor="#dedede">
											<h5 style="margin:10px 20px; color:#185173; font-weight:100;font-size: 16px;" >User details</h5>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<table cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr>
										<td bgcolor="#d0d0d0" width="4px"></td>
										<td bgcolor="#eee">
											<h6 style="margin:10px 20px; color:#555; font-weight:500;font-size: 14px;">
												Name : <?php echo $name;?><br/>												
												User Name : <?php echo $userName;?><br/>												
											</h6>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<?php if(!empty($socialModel)) : ?>
					<tr>
						<td align="center" bgcolor="#eaeaea" style="padding:10px 30px 10px 30px;">
							<?php foreach($socialModel as $socialKey=>$socialData) :
								echo '<a href="'.$socialData->url.'" title="'.$socialData->name.'" target="_blank" style="margin-right:5px;"><img alt="'.$socialData->name.'" src="'.$protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.$socialData->icon.'" height="30"></a>';
				 			endforeach; ?>
						</td>
					</tr>
					<?php endif;?>
					<tr>
						<td align="center" bgcolor="#dedede" style="font-size:9px;padding:10px">
							<p>&copy; <?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt));?>  <a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a> - all rights reserved | <?php if(!empty($branchModel)) echo $branchModel->addressline.','.$branchModel->city.','.$branchModel->postalcode;?></p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
	</table>