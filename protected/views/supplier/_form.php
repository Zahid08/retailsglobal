<script type="text/javascript" language="javascript">
	jQuery(document).ready(function() 
	{   
	   // user menu
	   $("#crType").change(function()
	   {
		   if($(this).val()=='credit') 
		   {
			   $("#consignmentDay").hide(); 
			   $("#creditDay").show('slow'); 
		   }
		   else if($(this).val()=='consignment') 
		   {
			   $("#creditDay").hide(); 
			   $("#consignmentDay").show('slow'); 
		   }
		   else {}
	   });
	});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Supplier <i class="icon-angle-right"></i></li>
	 <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Supplier</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
        	<ul class="nav nav-tabs">
            	<li class="active"><a href="#tab_1_1" data-toggle="tab">General</a></li>
            	<li><a href="#tab_1_2" data-toggle="tab">Contact details</a></li>
        	</ul>
       	 	<div class="tab-content">
            	<div class="tab-pane active" id="tab_1_1">
                	<?php if($model->isNewRecord) : ?>
                    <div class="row">
                    	<?php echo $form->labelEx($model,'suppId'); ?>
						<?php echo $form->textField($model,'suppId', array('id'=>'suppId','class'=>'m-wrap large','readonly'=>'true', 'value'=>'SN'.date("ymdhis"))); ?>
                        <?php echo $form->error($model,'suppId'); ?> 
                    </div>
                    <?php endif; ?>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>50,'maxlength'=>50)); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </div>

                    <div class="row">
                        <?php echo $form->labelEx($model,'name_bd'); ?>
                        <?php echo $form->textField($model,'name_bd',array('class'=>'m-wrap large','size'=>50,'maxlength'=>50)); ?>
                        <?php echo $form->error($model,'name_bd'); ?>
                    </div>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'crType'); ?>
                        <?php echo $form->dropDownList($model,'crType', UsefulFunction::enumItem($model,'crType'), array('id'=>'crType','class'=>'m-wrap large','prompt'=>'Select Credit Type')); ?>
                        <?php echo $form->error($model,'crType'); ?>
                    </div>
                    
                    <?php 
						$displayCr = 'none';
						$displayCons = 'none';
						if(!$model->isNewRecord) :
							if($model->crType=='credit') $displayCr = 'block';
							elseif($model->crType=='consignment') $displayCons = 'block';
							else {}
						  endif;
					?>
                    <div class="row" id="creditDay" style="display:<?php echo $displayCr;?>">
                        <?php echo $form->labelEx($model,'creditDay'); ?>
                        <?php echo $form->textField($model,'creditDay',array('class'=>'m-wrap large')); ?>
                        <?php echo $form->error($model,'creditDay'); ?>
                    </div>
                    <div class="row" id="consignmentDay" style="display:<?php echo $displayCons;?>">
                        <?php echo $form->labelEx($model,'consignmentDay'); ?>
                        <?php echo $form->textField($model,'consignmentDay',array('class'=>'m-wrap large')); ?>
                        <?php echo $form->error($model,'consignmentDay'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'status'); ?>
                        <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </div>  
                </div>
                <div class="tab-pane" id="tab_1_2">
                	<div class="row">
                        <?php echo $form->labelEx($model,'address'); ?>
                        <?php echo $form->textField($model,'address',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250)); ?>
                        <?php echo $form->error($model,'address'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'city'); ?>
                        <?php echo $form->textField($model,'city',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                        <?php echo $form->error($model,'city'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'email'); ?>
                        <?php echo $form->textField($model,'email',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'phone'); ?>
                        <?php echo $form->textField($model,'phone',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                        <?php echo $form->error($model,'phone'); ?>
                    </div> 
                </div>
		</div><!-- tab-content -->
     <div class="row buttons">
         <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
      </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   