<?php
/* @var $this TracerController */
/* @var $model Tracer */

$this->breadcrumbs=array(
	'Tracers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tracer', 'url'=>array('index')),
	array('label'=>'Manage Tracer', 'url'=>array('admin')),
);
?>

<h1>Create Tracer</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>