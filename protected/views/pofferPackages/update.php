<?php
/* @var $this PofferPackagesController */
/* @var $model PofferPackages */

$this->breadcrumbs=array(
	'Poffer Packages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PofferPackages', 'url'=>array('index')),
	array('label'=>'Create PofferPackages', 'url'=>array('create')),
	array('label'=>'View PofferPackages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PofferPackages', 'url'=>array('admin')),
);
?>

<h1>Update PofferPackages <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>