<?php
/* @var $this PofferPackagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Poffer Packages',
);

$this->menu=array(
	array('label'=>'Create PofferPackages', 'url'=>array('create')),
	array('label'=>'Manage PofferPackages', 'url'=>array('admin')),
);
?>

<h1>Poffer Packages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
