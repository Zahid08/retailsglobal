<?php
/* @var $this PofferPackagesController */
/* @var $model PofferPackages */

$this->breadcrumbs=array(
	'Poffer Packages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PofferPackages', 'url'=>array('index')),
	array('label'=>'Manage PofferPackages', 'url'=>array('admin')),
);
?>

<h1>Create PofferPackages</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>