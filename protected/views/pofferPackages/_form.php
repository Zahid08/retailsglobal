<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Promotional Offer
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'poffer-packages-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Packages</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>

			<div class="row">
                <div class="col-md-5" style="float: left;padding-right: 25px;">
                    <label for="PofferPackages_discount" class="required">Discount (%) <span class="required">*</span></label>
                    <?php echo $form->textField($model,'discount',array('class'=>'m-wrap large','size'=>11,'maxlength'=>11)); ?>
                    <?php echo $form->error($model,'discount'); ?>
                </div>
                <div class="col-md-2" style="float: left;padding-right: 25px;">
                <label style="position: absolute;padding: 26px 2px 2px 0px;">Or</label>
                </div>
                <div class="col-md-5" style="float: left;padding-left: 35px;">
                    <label for="PofferPackages_discount" class="required">Falt Amount<span class="required">*</span></label>
                    <?php echo $form->textField($model,'flat_amount',array('class'=>'m-wrap large','size'=>11,'maxlength'=>11)); ?>
                    <?php echo $form->error($model,'flat_amount'); ?>
                </div>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
        </div>  
    </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   