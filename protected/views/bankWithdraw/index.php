<?php
/* @var $this BankWithdrawController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bank Withdraws',
);

$this->menu=array(
	array('label'=>'Create BankWithdraw', 'url'=>array('create')),
	array('label'=>'Manage BankWithdraw', 'url'=>array('admin')),
);
?>

<h1>Bank Withdraws</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
