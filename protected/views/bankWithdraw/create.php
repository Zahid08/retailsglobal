<?php
/* @var $this BankWithdrawController */
/* @var $model BankWithdraw */

$this->breadcrumbs=array(
	'Bank Withdraws'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BankWithdraw', 'url'=>array('index')),
	array('label'=>'Manage BankWithdraw', 'url'=>array('admin')),
);
?>

<h1>Create BankWithdraw</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>