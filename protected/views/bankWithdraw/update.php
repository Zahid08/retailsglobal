<?php
/* @var $this BankWithdrawController */
/* @var $model BankWithdraw */

$this->breadcrumbs=array(
	'Bank Withdraws'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BankWithdraw', 'url'=>array('index')),
	array('label'=>'Create BankWithdraw', 'url'=>array('create')),
	array('label'=>'View BankWithdraw', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BankWithdraw', 'url'=>array('admin')),
);
?>

<h1>Update BankWithdraw <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>