<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<h3 class="page-title">
    Welcome <small><?php echo Yii::app()->user->name;?>  to HaatBazar POS</small>
</h3>
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index-2.html">Home</a> 
        <i class="icon-angle-right"></i>
    </li>
    <li><a href="#">Dashboard</a></li>
    <?php /*?><li class="pull-right no-text-shadow">
        <div id="dashboard-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive" data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
            <i class="icon-calendar"></i>
            <span></span>
            <i class="icon-angle-down"></i>
        </div>
    </li><?php */?>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<!-- BEGIN DASHBOARD  -->
<div id="dashboard">
	<p><img src="<?php echo Yii::app()->baseUrl;?>/media/images/pos-integration.jpg" /></p>
</div>
<!-- END DASHBOARD  -->