<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>GL Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

 <?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>
<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
    <div style="width:98.7%; padding:10px; border:1px dotted #ccc;">
        <div class="row">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form',
                    'action'=>Yii::app()->createUrl('report/dailyGeneralLedger'),
                    'enableAjaxValidation'=>true,				
                    
           )); ?> 
            <div class="span3">
                <?php echo CHtml::label('Start Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$model,
                       'name'=>'startDate',
                       'id'=>'startDate',					   
                       'value' =>$startDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                         'style'=>'width:100%!important;height:33px',
                       ),
                    ));
                ?>
            </div>
            <div class="span3">
                <?php echo CHtml::label('End Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$model,
                       'name'=>'endDate',
                       'id'=>'endDate',					   
                       'value' =>$endDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                         'style'=>'width:100%!important;height:33px',
                       ),
                    ));
                ?>
            </div>
            <div class="span2 button" style="padding-top:27px;">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue btn_up','id'=>'btn_up'));?>
            </div>
			<?php $this->endWidget(); ?>   
            <div class="span3 button_x" style="padding-top:12px; float:right">
				<?php //echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'dailyGeneralLedgerPdf','endDate'=>$endDate))); ?>
				<?php //echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/dailyGeneralLedger','startDate'=>$startDate,'endDate'=>$endDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
			 </div>    
        </div>
    </div>
  
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                           // $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            //$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">GL Report</td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
                <?php else : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date : <?php echo $pdate;?></td>
                </tr>
				<?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Income</th>
                        <th>Expense</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td width="50%">
                        	<?php
							
								$income = $expense = $profit = $closingStock = 0;
								
								// closing stock = total purchase+opening stock - salesCostPrice
								$closingStock = ($modelpurchase['totalPurchase']+$modelopeningstock['totalPurchase']) - $modelcostsales['totalCostPrice'];
								
								$income = ($modelsales['totalSell']-$modelsalesreturn['totalAmount'])+$modelpr['totalCost']+$modelmi['totalAmount']+$modelpurchasediscount['totalPurchase']+$closingStock;
								$expense = $modelpurchase['totalPurchase']+$modelopeningstock['totalPurchase']+$modeldam['totalAmount']+$modelwas['totalAmount']+$modelmiex['totalAmount']+$modelsalesdiscountexpense['totalSell']+$modelbc['totalCommision'];
								$profit = $income-$expense;
							?>
                        	<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Net Sales</strong> : 
                                <?php
								$netSell =	!empty($modelsales['totalSell'])?UsefulFunction::formatMoney(round(($modelsales['totalSell']-$modelsalesreturn['totalAmount']),2)):'0';
								echo $netSell;
								$data['netSell']=$netSell;
								
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Purchase Return</strong> : 
                            	<?php
								$purchesReturn = !empty($modelpr['totalCost'])?UsefulFunction::formatMoney(round($modelpr['totalCost'],2)):'0';
								echo $purchesReturn;
								$data['purchesReturn'] = $purchesReturn;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Misc. Income</strong> : 
                            	<?php 
								$miscIncome = !empty($modelmi['totalAmount']) || !empty($modelpurchasediscount['totalPurchase'])?UsefulFunction::formatMoney(round(($modelmi['totalAmount']+$modelpurchasediscount['totalPurchase']),2)):'0';
								echo $miscIncome;
								$data['miscIncome']=$miscIncome;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Transfer Out</strong> : 0</p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Closing Stock</strong> : <?php $closingStok = UsefulFunction::formatMoney(round($closingStock,2)); echo $closingStock; $data['closingstok']= $closingStock; ?></p>
                            <p style="border-bottom:1px solid #ddd;padding:0 0 3px 0;margin:3px 0px;">&nbsp;</p>
                        </td>
						<td width="50%">
                        	<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Purchase</strong> : 
                            	<?php 
								$purchase = !empty($modelpurchase['totalPurchase'])?UsefulFunction::formatMoney(round($modelpurchase['totalPurchase'],2)):'0';
								echo $purchase;
								$data['purchase']=$purchase;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Damages</strong> : 
                            	<?php 
								$damage = !empty($modeldam['totalAmount'])?UsefulFunction::formatMoney(round($modeldam['totalAmount'],2)):'0';
								echo $damage;
								$data['damage']=$damage;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Wastages</strong> :
                            	<?php 
								$wastage = !empty($modelwas['totalAmount'])?UsefulFunction::formatMoney(round($modelwas['totalAmount'],2)):'0';
								echo $wastage;
								$data['wastage']=$wastage;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Misc. Expense</strong> : 
                            	<?php 
								$miscExpense = !empty($modelmiex['totalAmount']) || !empty($modelsalesdiscountexpense['totalSell'])?UsefulFunction::formatMoney(round(($modelmiex['totalAmount']+$modelsalesdiscountexpense['totalSell']),2)):'0';
								echo $miscExpense;
								$data['miscExpense']=$miscExpense;
								?>
                            </p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Transfer In</strong> : 0</p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Opening Stock</strong> :
							<?php 
							$openingStok = UsefulFunction::formatMoney(round($modelopeningstock['totalPurchase'],2));
							echo $openingStok;
							$data['openingStok']=$openingStok;
							?>
							</p>
                            <p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Bank Commision</strong> : 
							<?php
							$bankComission = !empty($modelbc['totalCommision'])?UsefulFunction::formatMoney(round($modelbc['totalCommision'],2)):'0';
							echo $bankComission;
							$data['bankComission']=$bankComission;
							?>
                            </p>
                            <p style="padding:0 0 3px 0;margin:3px 0px;"><strong>Profit</strong> :
							<?php 
							$profit = UsefulFunction::formatMoney(round($profit,2));
							echo $profit;
							$data['profit']=$profit;
							?></p>
                        </td> 
					</tr>
                    
                	<tr style="font-weight:bold; background:#eee;">
                        <td>
						<?php $incomeTotal = UsefulFunction::formatMoney(round($income,2));
						echo $incomeTotal; $data['incomeTotal']=$incomeTotal;
						?></td>
						<td>
						<?php
						$expenseTotal = UsefulFunction::formatMoney(round($expense+$profit,2));
						echo $expenseTotal; $data['expenseTotal']=$expenseTotal;
						?>
						</td>
                    </tr>					
					<?php
					 Yii::app()->session['datlyGL'] = $data;
					/*if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel']))
					{
						unset(Yii::app()->session['reportModel']);
					}
					else Yii::app()->session['reportModel'] = $data;*/ 
				   ?>	
                
                </tbody>
            </table>
        </div>
    </div>
</div>