<?php 
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);  
?>
<div class="form">   
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
					   if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Temporary Worker Balance Summary Report</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';?>Date from <?php echo $startDate;?> to <?php echo $endDate;?>
				 </td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th>Worker</th>									                 
					<th class="hidden-480">Balance</th>                        
				</tr>
			</thead>
			<tbody>
			<?php
			if(!empty($model)) : 
				$srl = $totalQty  = $totalPrice = $rTotalQty= $costPrice= $balance=   0;
				foreach($model as $data) : 
				$srl++;
				$balance =TemporaryWorker::getWorkerBalance($data->id);
				$totalPrice+=$balance;
				?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data->username;?></td>														
					<td><?php echo $balance;?></td>						
				</tr>
			<?php endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="2" align="right">Total : </td>
					<td><?php echo $totalPrice;?></td>				
				</tr>
			 <?php
			else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>	                  
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>