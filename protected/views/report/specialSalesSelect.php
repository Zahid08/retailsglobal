<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }					
			);
		});
        
        // date difference find by submit
        $("#btn_submit").click(function()
        {
            var startDate = new Date($("#startDate").val());
            var endDate = new Date($("#endDate").val());
            var timeDiff = Math.abs(endDate.getTime() - startDate.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            
            if(diffDays>1) 
            {
                $("#dateError").show('slow');
                return false;
            }
            else 
            {
                return true;
                $("#dateError").hide();
            }
        });
        
        // after customer inputed for save
        $("#SaveAll").click(function()
        {
            // var idList  = $("input[type=checkbox]:checked").serialize();
            var invoices = $.map($(':checkbox[name=idList\\[\\]]:checked'), function(n, i){
                              return n.value;
                        }).join(',');
            if(invoices)
            {
                if(confirm("Are you sure want to Save?"))
                {
                    var startDate  = $("#startDate").val();	
                    var endDate  = $("#endDate").val();	
                    var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/specialSalesSelectSave/";
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: urlajax,
                        data: 
                        { 
                            startDate : startDate,
                            endDate : endDate,
                            invoices : invoices
                        },
                        success: function(data) 
                        {
                            window.location.href = "<?php echo Yii::app()->baseUrl;?>/report/specialSalesSelect/startDate/"+startDate+"/endDate/"+endDate;
                        },
                        error: function() {}
                    });
                }
            }
        });

        // select deselect checkbox
        jQuery(document).on('click','#selectAll',function() {
            var checked=this.checked;
            jQuery("input[name='idList\[\]']:enabled").each(function() {this.checked=checked;});
        });
        jQuery(document).on('click', "input[name='idList\[\]']", function() {
            jQuery('#selectAll').prop('checked', jQuery("input[name='idList\[\]']").length==jQuery("input[name='idList\[\]']:checked").length);
        });
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Global Sales <i class="icon-angle-right"></i></li>
    <li>Process Invoices</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
    
    <div id="dateError" style="display:none;">
        <div class="notification note-error"><p style="padding-left:20px;">Please select date between 1 day difference !</p></div>
    </div>
	
    <div style="width:98.29%; padding:10px; border:1px dotted #ccc;">
		<?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'action'=>Yii::app()->createUrl('report/specialSalesSelect'),
            'enableAjaxValidation'=>true,				        
        )); ?> 
        <div class="row">
            <div class="span4">
                <?php echo CHtml::label('Start Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$model,
                       'name'=>'startDate',
                       'id'=>'startDate',					   
                       'value' =>$startDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                       ),
                    ));
                ?>
            </div>
            <div class="span4">
                <?php echo CHtml::label('End Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$model,
                       'name'=>'endDate',
                       'id'=>'endDate',					   
                       'value' =>$endDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                       ),
                    ));
                ?>
            </div> 
            <div class="span4" style="padding-top:16px;">
                <?php echo CHtml::submitButton('Submit', array('id'=>'btn_submit','class'=>'btn blue'));?>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    
    <?php if(!empty($model)) : ?> <div class="row"><a id="SaveAll" class="btn blue">Save</a></div> 
    <?php else : echo '<div style="width:98.29%; margin:15px 0px;"></div>'; endif;?>
      
    <div id="report_dynamic_container">
        <div class="row">
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th><input type="checkbox" id="selectAll" value="" style="width:20px;"></th>
                        <th>Sl. No</th>
                        <th class="hidden-480">Invoice</th>
                        <th>Qty</th>
                        <th>Weight</th>
                        <th>Amount</th>
                        <th>VAT</th>
                        <th class="hidden-480">Date</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) :
					$srl = $totalQty = $totalWeight = $totalAmount = $totalVat = 0;
					foreach($model as $data) : 
					$srl++; 
                    $totalQty+=$data->totalQty;
                    $totalWeight+=$data->totalWeight;
					$totalAmount+=$data->totalPrice; 
                    $totalVat+=$data->totalTax; 
                    if(!empty($specialSalesArr)) :
                        if(in_array($data->id,$specialSalesArr)) $checked = 'checked';
                        else $checked = '';
                    else : $checked = '';
                    endif; ?>
					<tr>
                        <td class="checkbox-column">
                            <input type="checkbox" name="idList[]" id="trip-grid_c11_<?php echo $srl;?>" value="<?php echo $data->id;?>" style="width:20px;" <?php echo $checked;?>>
                        </td>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->invNo;?></td>
                        <td><?php echo $data->totalQty;?></td>
                        <td><?php echo $data->totalWeight;?></td>
                        <td><?php echo $data->totalPrice;?></td>
                        <td><?php echo $data->totalTax;?></td>
						<td><?php echo $data->orderDate;?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="3" align="right">Total : </td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalQty,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalWeight,2));?></td>
						<td><?php echo UsefulFunction::formatMoney(round($totalAmount,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalVat,2));?></td>
						<td></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td style="width:50px;"><input type="checkbox" id="selectAll" value="" style="width:20px;"></td>
                        <td style="width:80px;">#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>