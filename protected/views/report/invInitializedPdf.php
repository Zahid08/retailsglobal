<div class="form">
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Inventory Initialize Report</td>
			</tr>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th class="hidden-480">Sub Department</th>
					<th class="hidden-480">Status</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)) : 
				$srl = 0;
				foreach($model as $data) : 	$srl++;
					if($data->status==1) $status = "Active";
					else $status = 'Inactive';
				?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data->subDept->name;?></td>
					<td><?php echo $status;?></td>
				</tr>
			  <?php endforeach; 
			 else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div> 
</div>