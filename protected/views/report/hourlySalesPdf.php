<div class="form">
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php
					    $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif;
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php
					   $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
						if(!empty($branchModel)) echo $branchModel->addressline;
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Hourly Sales Report</td>
			</tr>
			<?php if(!empty($startDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date <?php echo $startDate;?></td>
			</tr>
			<?php endif;?>
		</table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
            <tr style="border-top:1px solid #ddd;">
                <th>Sl. No</th>
                <th>Hour</th>
                <th>Cash</th>
                <th>Card</th>
                <th>Item Discount</th>
                <th>Instant Discount</th>
                <th>Reedem Amount</th>
                <th>Coupon Amount</th>
                <th>Gross Total</th>
                <th>Avg. Busket</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($model)) :
                $srl = $cashPaid = $cardPaid = $totalDiscount = $totalInstantDiscount = $loyaltyPaid = $totalCouponAmount = $netPaid = $totalPaid = 0;
                foreach($model as $key=>$val) : $nextKey = $key+1;
                    $sql = "SELECT COUNT(id) AS totalBill,SUM(CASE WHEN spDiscount=0 THEN totalPrice+totalTax ELSE totalPrice END) AS totalPrice,SUM(cashPaid)-SUM(totalChangeAmount) AS cashPaid,SUM(cardPaid) AS cardPaid,SUM(totalDiscount) AS totalDiscount,SUM(instantDiscount) AS instantDiscount,SUM(loyaltyPaid) AS loyaltyPaid FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate>'".$_POST['startDate']." ".$key."' AND orderDate<'".$_POST['startDate']." ".$nextKey."' AND isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND status<>".SalesDetails::STATUS_INACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d %H') ORDER BY orderDate ASC"; // IF(spDiscount=0,SUM(totalPrice),SUM(totalPrice)+SUM(totalTax)) AS totalPrice
                    $data = Yii::app()->db->createCommand($sql)->queryRow();

                    if(!empty($data)) :
                        $srl++;
                        $cashPaid+=$data['cashPaid'];
                        $cardPaid+=$data['cardPaid'];
                        $totalDiscount+=$data['totalDiscount'];
                        $totalInstantDiscount+=$data['instantDiscount'];
                        $loyaltyPaid+=$data['loyaltyPaid'];
                        $totalPaid+=$data['totalPrice'];

                        // coupon Amount Processing
                        $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate>'".$_POST['startDate']." ".$key."' AND orderDate<'".$_POST['startDate']." ".$nextKey."' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY i.orderDate ASC";
                        $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                        $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
                        ?>
                        <tr>
                            <td><?php echo $srl;?></td>
                            <td><?php echo $val;//echo $data['prevhours'].'&nbsp;-&nbsp;'.$data['hours'];?></td>
                            <td><?php echo round($data['cashPaid'],2);?></td>
                            <td><?php echo round($data['cardPaid'],2);?></td>
                            <td><?php echo round($data['totalDiscount'],2);?></td>
                            <td><?php echo round($data['instantDiscount'],2);?></td>
                            <td><?php echo round($data['loyaltyPaid'],2);?></td>
                            <td><?php echo round($modelCouponAmount['totalCouponAmount'],2);?></td>
                            <td><?php echo round($data['totalPrice'],2);?></td>
                            <td><?php echo round($data['totalPrice']/$data['totalBill'],2);?></td>
                        </tr>
                    <?php
                    endif;
                endforeach; ?>
                <tr style="font-weight:bold;">
                    <td align="right" colspan="2">Total : </td>
                    <td><?php echo UsefulFunction::formatMoney(round($cashPaid,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($cardPaid,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalDiscount,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalInstantDiscount,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($loyaltyPaid,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalCouponAmount,2),true);?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalPaid,2),true);?></td>
                    <td></td>
                </tr>
            <?php
            else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
	</div>
</div>