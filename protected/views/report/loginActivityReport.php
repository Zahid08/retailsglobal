<style type="text/css">
    .invoiceTbl td{ margin:0; padding:0;}
    @media screen
    {
        .invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
    }
    @media print
    {
        .invoiceTbl td {font-size:11px;}
    }
    @media screen,print
    {
        .invoiceTbl td {font-size:11px;}
    }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    //-------print options----//
    $(function()
    {
        $("#printIcon").click(function()
        {
            $('#report_dynamic_container').printElement(
                {
                    overrideElementCSS:[
                        '<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
                        { href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
                        '<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
                        { href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
                        '<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
                        { href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
                        '<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
                        { href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
                    ],
                    printBodyOptions:{
                        styleToAdd:'margin-left:25px !important',
                        //classNameToAdd : 'printBody',
                    }
                }
            );
        });
    });
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Special Sales <i class="icon-angle-right"></i></li>
    <li>Global Details</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php
$companyName=$branchAddress='';
$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
if(!empty($companyModel)) :
    $companyName=$companyModel->name;
endif;
$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
if(!empty($branchModel)) $branchAddress=$branchModel->addressline;
?>


<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
    <div style="width:98%; padding:10px; border:1px dotted #ccc;">
        <div style=" float:left;" class="span6">
            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'action'=>Yii::app()->createUrl('report/loginActivityReport'),
                'enableAjaxValidation'=>true,

            )); ?>
            <div class="row">
                <div class="span4">
                    <?php echo CHtml::label('Date','');?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        //'model'=>$model,
                        'name'=>'startDate',
                        'id'=>'startDate',
                        'value' =>$startDate,
                        'options'=>array(
                            'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                            'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                            'changeMonth' => 'true',
                            'changeYear' => 'true',
                            'showButtonPanel' => 'true',
                            'constrainInput' => 'false',
                            'duration'=>'normal',
                        ),
                        'htmlOptions'=>array(
                            'class'=>'m-wrap large',
                        ),
                    ));
                    ?>
                </div>
                <div class="span4">
                    <?php echo CHtml::label('Branch','');?>
                    <?php echo CHtml::dropDownList('branch',$branch,Branch::getAllBranchGlobal(Branch::STATUS_ACTIVE),
                        array('class'=>'m-wrap combo combobox','prompt'=>'Select','id'=>'branch',
                        ));
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="span4 _button" style="padding-top:24px;margin-left:0;">
                    <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
        <div class="span3 button_x" style="padding-top:12px; float:right">
            <!--            	 --><?php //
            //				 	if(isset(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
            //				 ?>
            <?php
            if (isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) {
                unset(Yii::app()->session['reportModel']);
                Yii::app()->session['reportModel'] = $model;
            }
            else
                Yii::app()->session['reportModel'] = $model;
            ?>

            <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'loginActivityReportPdf','startDate'=>$startDate,'endDate'=>$endDate))); ?>
            <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/specialSalesDetails','startDate'=>$startDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress,'endDate'=>$endDate))); ?>
            <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
                <img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
            </a>
        </div>
        <div style="clear:both"></div>
    </div>

    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php
                        if(!empty($branchModel)) echo $branchModel->addressline;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Opening And Closing Shop Report</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date Time : <?php echo $startDate;?></td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php if(!empty($branchName)) echo 'Branch : '.$branchName;?>
                    </td>
                </tr>
            </table>

            <table class="table table-striped table-hover bordercolumn">
                <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th class="hidden-480">BranchName</th>
                    <th>Username</th>
                    <th>Ip Address</th>
                    <th>Opening Shop</th>
                    <th>Closing Shop</th>
                    <th>Login Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if(!empty($model)) :
                    $branchName='';
                    $srl=0;
                    foreach($model as $data) :
                        $srl++;
                        $branchModel = Branch::model()->findByPk($data->branch_id);
                        if(!empty($branchModel)) $branchName = $branchModel->name;
                        $userName=!empty($data->user_name)?$data->user_name:'';
                        $ipAddress=!empty($data->ipAddress)?$data->ipAddress:'';
                        $opening=!empty($data->lastlogin)?$data->lastlogin:'';
                        $closing=!empty($data->lastlogout)?$data->lastlogout:'';
                        $loginDate=!empty($data->crAt)?$data->crAt:'';
                        ?>
                        <tr>
                            <td><?php echo $srl;?></td>
                            <td><?php echo $branchName;?></td>
                            <td><?php echo $userName;?></td>
                            <td><?php echo $ipAddress;?></td>
                            <td><?php echo $opening;?></td>
                            <td><?php echo $closing;?></td>
                            <td><?php echo $loginDate;?></td>
                    <?php endforeach; ?>
                <?php
                else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>