<?php
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
?>
<div class="form">      
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
					   if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Temporary Worker Payment</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';?>Date from <?php echo $startDate;?> to <?php echo $endDate;?>
				 </td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th>Worker</th>
					<th>Invoice No.</th>					
					<th class="hidden-480">Amount</th>                        
					<th class="hidden-480">Payment</th>                        
					<th class="hidden-480">Created</th>                      
					<th class="hidden-480">Remarks</th>                      
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)):
				$srl=0; $totalAmount= 0;
			foreach ($model as $data) : $srl++; 
				$totalAmount+=$data->amount; ?>  
				<tr style="border-top:1px solid #ddd;">
					<td><?php echo $srl; ?></td>
					<td><?php echo $data->wk->username; ?></td>
					<td><?php echo $data->twNo; ?></td>				
					<td class="hidden-480"><?php echo $data->amount; ?></td> 
					<td class="hidden-480"><?php echo ($data->bankId)>0?'Bank':'Cash';?></td>
					<td class="hidden-480"><?php echo $data->crAt;?></td> 					
					<td class="hidden-480"><?php echo $data->remarks;  ?></td>                      
				</tr>                
		   <?php   endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="3" align="right">Total : </td>
					<td><?php echo $totalAmount;?></td>						
					<td></td>					
					<td></td>					
					<td></td>					
				</tr>
		  <?php  else: ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>					
					<td>-</td>					
					<td>-</td>					
				</tr>
		   <?php  endif;  ?>
			</tbody>
		</table>
	</div>

</div>