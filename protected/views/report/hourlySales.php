<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Hourly Sales Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>


<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	
    <div style="width:98%; padding:10px; border:1px dotted #ccc;">
	<div style="float:left;" class="span6">
		<?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'action'=>Yii::app()->createUrl('report/hourlySales'),
            'enableAjaxValidation'=>true,				        
          )); ?> 
        <div class="row">
            <div class="span6">
                <?php echo CHtml::label('Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$model,
                       'name'=>'startDate',
                       'id'=>'startDate',					   
                       'value' =>$startDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                       ),
                    ));
                ?>
            </div>
			<div class="span4 button" style="padding-top:27px;">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue btn_up','id'=>'btn_up'));?>
            </div>           
        </div>
        <?php $this->endWidget(); ?>
		</div>
		<div class="span3 button_x" style="padding-top:12px; float:right">
            	 <?php
				 	 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
                     else Yii::app()->session['reportModel'] = $model;
				 ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'hourlySalesPdf','startDate'=>$startDate))); ?>
			    <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/hourlySales','startDate'=>$startDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
		 </div>
		<div style="clear:both;"></div>
		
    </div>
      
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            //$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                           // $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Hourly Sales Report</td>
                </tr>
                <?php if(!empty($startDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date <?php echo $startDate;?></td>
                </tr>
				<?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>Hour</th>
                        <th>Cash</th>
                        <th>Card</th>
                        <th>Item Discount</th>
                        <th>Instant Discount</th>
                        <th>Reedem Amount</th>
                        <th>Coupon Amount</th>
                        <th>Gross Total</th>
                        <th>Avg. Busket</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) :
				$srl = $cashPaid = $cardPaid = $totalDiscount = $totalInstantDiscount = $loyaltyPaid = $totalCouponAmount = $netPaid = $totalPaid = 0;
				foreach($model as $key=>$val) : $nextKey = $key+1;					
					$sql = "SELECT COUNT(id) AS totalBill,SUM(CASE WHEN spDiscount=0 THEN totalPrice+totalTax ELSE totalPrice END) AS totalPrice,SUM(cashPaid)-SUM(totalChangeAmount) AS cashPaid,SUM(cardPaid) AS cardPaid,SUM(totalDiscount) AS totalDiscount,SUM(instantDiscount) AS instantDiscount,SUM(loyaltyPaid) AS loyaltyPaid FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate>'".$_POST['startDate']." ".$key."' AND orderDate<'".$_POST['startDate']." ".$nextKey."' AND isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND status<>".SalesDetails::STATUS_INACTIVE." GROUP BY DATE_FORMAT(orderDate, '%Y%m%d %H') ORDER BY orderDate ASC"; // IF(spDiscount=0,SUM(totalPrice),SUM(totalPrice)+SUM(totalTax)) AS totalPrice
					$data = Yii::app()->db->createCommand($sql)->queryRow();

					if(!empty($data)) :
						$srl++; 
                        $cashPaid+=$data['cashPaid'];
                        $cardPaid+=$data['cardPaid'];
                        $totalDiscount+=$data['totalDiscount'];
                        $totalInstantDiscount+=$data['instantDiscount'];
                        $loyaltyPaid+=$data['loyaltyPaid'];
                        $totalPaid+=$data['totalPrice'];

                        // coupon Amount Processing
                        $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate>'".$_POST['startDate']." ".$key."' AND orderDate<'".$_POST['startDate']." ".$nextKey."' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY i.orderDate ASC";
                        $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                        $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
					?>
					<tr>
						<td><?php echo $srl;?></td>
                        <td><?php echo $val;//echo $data['prevhours'].'&nbsp;-&nbsp;'.$data['hours'];?></td>
                        <td><?php echo round($data['cashPaid'],2);?></td>
                        <td><?php echo round($data['cardPaid'],2);?></td>
                        <td><?php echo round($data['totalDiscount'],2);?></td>
                        <td><?php echo round($data['instantDiscount'],2);?></td>
                        <td><?php echo round($data['loyaltyPaid'],2);?></td>
                        <td><?php echo round($modelCouponAmount['totalCouponAmount'],2);?></td>
                        <td><?php echo round($data['totalPrice'],2);?></td>
                        <td><?php echo round($data['totalPrice']/$data['totalBill'],2);?></td>
					</tr>
				<?php 
					endif;	
				endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td align="right" colspan="2">Total : </td>
						<td><?php echo UsefulFunction::formatMoney(round($cashPaid,2),true);?></td>
						<td><?php echo UsefulFunction::formatMoney(round($cardPaid,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalDiscount,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalInstantDiscount,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($loyaltyPaid,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalCouponAmount,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalPaid,2),true);?></td>
                        <td></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>