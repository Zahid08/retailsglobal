<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);   
?>
<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php
                    if(!empty($companyModel)) : ?>
                        <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                        <?php echo $companyModel->name;
                    endif;
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php
                    if(!empty($branchModel)) echo $branchModel->addressline;
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Service Charge Report</td>
            </tr>
            <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date <?php echo $startDate.' to  '.$endDate;?></td>
                </tr>
            <?php endif;?>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
            <tr style="border-top:1px solid #ddd;">
                <th>Sl. No</th>
                <th>Total Sales</th>
                <th>Total Return</th>
                <th>Net Sales</th>
                <th>Service Charge (10%)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($serviceChargeAmount)) : ?>
                <tr>
                    <td>1</td>
                    <td><?php echo UsefulFunction::formatMoney($netSales,2);?></td>
                    <td><?php echo UsefulFunction::formatMoney($netReturn,2);?></td>
                    <td><?php echo UsefulFunction::formatMoney(($netSales-$netReturn),2);?></td>
                    <td><?php echo UsefulFunction::formatMoney($serviceChargeAmount,2);?></td>
                </tr>
            <?php else: ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>