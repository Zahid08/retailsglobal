s<div class="form">    
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
					   $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline; 
						if($salesType==1) $typeMsg = ' (Online Sale)';
						else if ($salesType==2) $typeMsg = ' (Local Sale)';
						else $typeMsg = '';
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Department Wise Sales Report <?php echo $typeMsg; ?></td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th class="hidden-480">Department</th>					
                    <th class="hidden-480">Cost of Sales</th>
                    <th class="hidden-480">Net Sales</th>						
                    <th class="hidden-480">VAT</th>
                    <th class="hidden-480">Discount</th>
                    <th class="hidden-480">Gross Sales</th>
                    <th class="hidden-480">Gross Profit</th>
                    <th class="hidden-480">GP %</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            if(!empty($model) && !empty($startDate) && !empty($endDate)) : 

                // local & online sell filtering
                $filter = '';
                if(isset($salesType) && !empty($salesType))
                   $filter.=' AND v.isEcommerce='.$salesType;
                   $srl = $totalCostSales  = $totalNetSales = $totalNetSalesVat = $totalSalesDiscount = 0;
                   foreach($model as $data) : 
                    // cost of sales total
                    /*$sqlpurchase = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_stock s,pos_items i,pos_category c,pos_subdepartment d WHERE s.grnId=n.id 
AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.branchId=branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate BETWEEN '".$startDate."' AND '".$endDate."' ORDER BY n.grnDate DESC";
                    $modelpurchase = Yii::app()->db->createCommand($sqlpurchase)->queryRow();*/
                    $sqlcostsales = "SELECT ROUND(SUM(s.qty*s.costPrice),2) AS totalCostPrice FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status=".SalesDetails::STATUS_ACTIVE." ORDER BY s.salesDate DESC";
                    $modelcostsales = Yii::app()->db->createCommand($sqlcostsales)->queryRow();

                    // net sales
                    $sqlnetsales = "SELECT ROUND((SUM(s.qty*s.salesPrice)-SUM(s.discountPrice)),2) AS totalSell FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
                    $modelnetsales = Yii::app()->db->createCommand($sqlnetsales)->queryRow();

                    // net vat
                    $sqlnetsalesvat = "SELECT ROUND(SUM(s.vatPrice),2) AS totalTax FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
                    $modelnetsalesvat = Yii::app()->db->createCommand($sqlnetsalesvat)->queryRow();

                    // net discount
                    $sqlnetsalesdiscount = "SELECT ROUND(SUM(s.discountPrice),2) AS totalDiscount FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND v.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY v.orderDate DESC";
                    $modelnetsalesdiscount = Yii::app()->db->createCommand($sqlnetsalesdiscount)->queryRow();

                    $srl++; 
                    $totalCostSales+=$modelcostsales['totalCostPrice'];
                    $totalNetSales+=$modelnetsales['totalSell'];
                    $totalNetSalesVat+= $modelnetsalesvat['totalTax'];
                    $totalSalesDiscount+= $modelnetsalesdiscount['totalDiscount'];
                    ?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->name;?></td>						
                        <td><?php echo UsefulFunction::formatMoney($modelcostsales['totalCostPrice'], true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell'], true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($modelnetsalesvat['totalTax'], true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($modelnetsalesdiscount['totalDiscount'], true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell']+$modelnetsalesvat['totalTax']+$modelnetsalesdiscount['totalDiscount'], true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'], true);?></td>
                        <td><?php if($modelnetsales['totalSell']>0) echo round((($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'])*100)/$modelnetsales['totalSell'],2);?></td>
                    </tr>
              <?php endforeach; ?>
                <tr style="font-weight:bold;">
                    <td colspan="2" align="right">Total : </td>
                    <td><?php echo UsefulFunction::formatMoney($totalCostSales, true);?></td>
                    <td><?php echo UsefulFunction::formatMoney($totalNetSales, true);?></td>
                    <td><?php echo UsefulFunction::formatMoney($totalNetSalesVat, true);?></td>
                    <td><?php echo UsefulFunction::formatMoney($totalSalesDiscount, true);?></td>
                    <td><?php echo UsefulFunction::formatMoney($totalNetSales+$totalNetSalesVat+$totalSalesDiscount, true);?></td>
                    <td><?php echo UsefulFunction::formatMoney($totalNetSales-$totalCostSales, true);?></td>
                    <td><?php if($totalNetSales>0) echo round((($totalNetSales-$totalCostSales)*100)/$totalNetSales,2);?></td>
                </tr>
             <?php
             else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>  
                    <td>-</td> 
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
	</div>  
</div>