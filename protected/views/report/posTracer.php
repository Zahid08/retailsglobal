<META HTTP-EQUIV="refresh" CONTENT="120"> <!-- after every 2 min it will refresh -->
<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>POS Tracer Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>
<div class="form">
        <div class="row">
            <div class="span3 button_x" style="padding-top:17px; float:right">
			 <?php 
				 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
				 else Yii::app()->session['reportModel'] = $model;
			 ?>
			<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'posTracerPdf','startDate'=>date("Y-m-d")))); ?>
			<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/posTracer','branchAddress'=>$branchAddress,'companyName'=>$companyName,'date'=>date("Y-m-d")))); ?>
			 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
				<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
			</a>
		</div>
    	</div>
      
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            //$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            //$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">POS Tracer Report</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date : <?php echo date("Y-m-d");?></td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>POS</th>
                        <th>Cashier</th>
                        <th>Cash</th>
                        <th>Card</th>
                        <th>Item Discount</th>
                        <th>Instant Discount</th>
                        <th>Reedem Amount</th>
                        <th>Coupon Amount</th>
                        <th>Return Amount</th>
                        <th>Gross Total</th>
                        <th>Avg. Busket</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = $cashPaid = $cardPaid = $totalDiscount = $totalInstantDiscount = $loyaltyPaid = $totalCouponAmount = $totalReturnAmount = $netPaid = $totalPaid = 0;
					foreach($model as $data) : 
                        $srl++; 
                        // coupon Amount Processing
                        $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.crBy=".$data['crBy']." AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate LIKE '".date("Y-m-d")."%' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY i.orderDate DESC";
                        $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                        $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];

                        // return Amount Processing
                        $sqlReturnAmount = "SELECT SUM(r.totalPrice) AS totalReturnAmount FROM pos_sales_return r INNER JOIN pos_sales_invoice i 
WHERE r.salesId=i.id AND r.branchId=".Yii::app()->session['branchId']." AND r.returnDate LIKE '".date("Y-m-d")."%' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND r.status=".SalesReturn::STATUS_ACTIVE." AND r.crBy=".$data['crBy']." ORDER BY r.returnDate DESC";
                        $modelReturnAmount = Yii::app()->db->createCommand($sqlReturnAmount)->queryRow();
                        $totalReturnAmount+=$modelReturnAmount['totalReturnAmount'];

                        $cashPaid+=$data['cashPaid']-$modelReturnAmount['totalReturnAmount'];
                        $cardPaid+=$data['cardPaid'];
                        $totalDiscount+=$data['totalDiscount'];
                        $totalInstantDiscount+=$data['instantDiscount'];
                        $loyaltyPaid+=$data['loyaltyPaid'];
                        $totalPaid+=$data['totalPrice']-$modelReturnAmount['totalReturnAmount'];
                    ?>
					<tr>
						<td><?php echo $srl;?></td>
                        <td><?php echo $data['pos'];?></td>
						<td><?php echo $data['cashier'];?></td>
                        <td><?php echo round(($data['cashPaid']-$modelReturnAmount['totalReturnAmount']),2);?></td>
                        <td><?php echo round($data['cardPaid'],2);?></td>
                        <td><?php echo round($data['totalDiscount'],2);?></td>
                        <td><?php echo round($data['instantDiscount'],2);?></td>
                        <td><?php echo round($data['loyaltyPaid'],2);?></td>
                        <td><?php echo round($modelCouponAmount['totalCouponAmount'],2);?></td>
                        <td><?php echo round($modelReturnAmount['totalReturnAmount'],2);?></td>
                        <td><?php echo round(($data['totalPrice']-$modelReturnAmount['totalReturnAmount']),2);?></td>
                        <td><?php echo round((($data['totalPrice']-$modelReturnAmount['totalReturnAmount'])/$data['totalBill']),2);?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td align="right" colspan="3">Total : </td>
						<td><?php echo UsefulFunction::formatMoney(round($cashPaid,2));?></td>
						<td><?php echo UsefulFunction::formatMoney(round($cardPaid,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalDiscount,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalInstantDiscount,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($loyaltyPaid,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalCouponAmount,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalReturnAmount,2));?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalPaid,2));?></td>
                        <td></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>