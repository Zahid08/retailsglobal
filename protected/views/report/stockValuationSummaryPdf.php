<div class="form">
    <div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Stock valuation summary Report</td>
			</tr>
			<?php if(!empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date till <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th class="hidden-480">Department</th>
					<th class="hidden-480">Total</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model) && !empty($endDate)) : 
				$srl = $totalAmount  = 0;
				foreach($model as $data) : // current month = and s.stockDate BETWEEN '".date("Y-m")."-01' AND '".$endDate."'
					$sql = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId IN(SELECT id FROM pos_department WHERE id=".$data->id.") and s.stockDate<='".$endDate."' and s.branchId=".Yii::app()->session['branchId']." and s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";
					$stockModel = Stock::model()->findAllBySql($sql);	
					if(!empty($stockModel)) :
						$srl++; 
						$amount = 0;
						foreach($stockModel as $dataStock) :
							$amount+=Stock::getItemWiseStock($dataStock->itemId)*$dataStock->item->costPrice;
						endforeach;
						$totalAmount+=$amount;
						?>
						<tr>
							<td><?php echo $srl;?></td>
							<td><?php echo $data->name;?></td>
							<td><?php echo UsefulFunction::formatMoney($amount, true);?></td>
						</tr>
			  <?php endif;
				endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="2" align="right">Total : </td>
					<td><?php echo UsefulFunction::formatMoney($totalAmount, true);?></td>
				</tr>
			 <?php
			 else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>    
</div>