<div class="form">   
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Inventory Details Report</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
			
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php if(!empty($itemCode)) echo 'Item Code : '.$itemCode.', ';
						  if(!empty($invNo)) echo 'Inventory NO. : '.$invNo;
					?>
				</td>
			</tr>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th>IV No.</th>
					<th class="hidden-480">Item Code</th>
					<th class="hidden-480">Description</th>
					<th class="hidden-480">Quantity</th>
					<th class="hidden-480">Cost Price</th>
					<th class="hidden-480">Adjust Quantity</th>
					<th class="hidden-480">Adjust Price</th>					
					<th class="hidden-480">Ecommerce Item</th>
				    <th class="hidden-480">Child Item</th>
					<th class="hidden-480">Date</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)) : 
				$srl = 0;
				$totalQty = $totalPrice = $totaladjQty = $totaladjPrice = 0;
				foreach($model as $data) : 
					$srl++; 
					$totalQty+=$data->qty; $totalPrice+=($data->qty*$data->costPrice);
					$totaladjQty+=$data->adjustQty;
					$totaladjPrice+=($data->adjustQty*$data->costPrice); ?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data->invNo;?></td>
					<td><?php echo $data->item->itemCode;?></td>
					<td><?php echo $data->item->itemName;?></td>
					<td><?php echo $data->qty;?></td>
					<td><?php echo round(($data->qty*$data->costPrice),2);?></td>
					<td><?php echo $data->adjustQty;?></td>
					<td><?php echo round(($data->adjustQty*$data->costPrice),2);?></td>
					<td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
    				<td><?php echo $data->item->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
					<td><?php echo $data->invDate;?></td>
				</tr>
				<?php endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="4" align="right">Total : </td>
					<td><?php echo $totalQty;?></td>
					<td><?php echo round($totalPrice,2);?></td>
					<td><?php echo $totaladjQty;?></td>
					<td><?php echo round($totaladjPrice,2);?></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			 <?php else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>   
</div>