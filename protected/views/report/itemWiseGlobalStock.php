<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
        $("#itemCode").focus();

		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl;?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Item Wise Global Stock Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$bName=$bAdd='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $bName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $bAdd=$branchModel->addressline; 
?>

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	<div style="width:98%; padding:10px; border:1px dotted #ccc;">
	<div style="float:left;" class="span8">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->createUrl('report/itemWiseGlobalStock'),
				'enableAjaxValidation'=>true,				
				
	   )); ?>    
        <div class="row">
          <div class="span6">
              <?php echo CHtml::label('Branch','');?>
              <?php echo CHtml::dropDownList('branch',$branch,Branch::getAllBranchGlobal(Branch::STATUS_ACTIVE), 
                              array('class'=>'m-wrap combo combobox','id'=>'branch',
                          )); 
              ?>
          </div>
          <div class="span6">
              <?php echo CHtml::label('Ecommerce Items','');?>
              <?php echo CHtml::dropDownList('isEcommerce',$isEcommerce,array(1=>'Yes',2=>'No'), 
                              array('class'=>'m-wrap combo combobox','id'=>'isEcommerce',
                          )); 
              ?>   
          </div>
        </div> 
        <div class="row">        	
            <div class="span6">
                <?php echo CHtml::label('Item Code','');?>
            	<?php echo CHtml::textField('itemCode',$itemCode,array('class'=>'m-wrap span','size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="span6">
                <?php echo CHtml::label('End Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$modelStock,
                       'name'=>'endDate',
                       'id'=>'endDate',					   
                       'value' =>$endDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                       ),
                    ));
                ?>
            </div>             
        </div>
		<div class="row">
		    <div class="span4" style="padding-top:0px;">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
            </div> 				
		</div>
    <?php $this->endWidget(); ?>
	</div>
		<div class="span3 button_x" style="padding-top:0px; float:right">
			 <?php 
				 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
				 else Yii::app()->session['reportModel'] = $modelStock;
			 ?>																																											<?php //if(!empty($startDate) && !empty($endDate))  ?>
			 <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'itemWiseGlobalStockPdf','endDate'=>$endDate,'itemCode'=>$itemCode))); ?>
			<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/itemWiseGlobalStock','endDate'=>$endDate,'bName'=>$bName,'bAdd'=>$bAdd,'itemCode'=>$itemCode))); ?>
			 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
				<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
			</a>
		</div>	
		<div style="clear:both"></div>
		
	</div>
    
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Item Wise Global Stock Report</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php 
                              if(!empty($branchName)) echo 'Branch : '.$branchName;
							  if(!empty($itemCode)) echo ', Item Code : '.$itemCode;
                        ?>
                    </td>
                </tr>
                <?php if(!empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date till <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Ecommerce Item</th>
                        <?php if($branch=='all') : foreach(Branch::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Branch::STATUS_ACTIVE))) as $keyBranch=>$dataBranch) : ${"totalQty".$dataBranch->id} = 0; ?>
                            <th class="hidden-480"><?php echo $dataBranch->name;?></th>
                        <?php endforeach; else : ?><th class="hidden-480">Stock</th><?php endif;?>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($modelStock) || !empty($modelTransfer)) : 
					$srl = $totalQty  = 0;
					foreach($modelStock as $data) : 
					$srl++; ?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->item->itemCode;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
                        <td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
                        <?php if($branch=='all') : foreach(Branch::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Branch::STATUS_ACTIVE))) as $keyBranch=>$dataBranch) : 
                            ${"stockQty".$dataBranch->id} = Stock::getBranchWiseItemStockByDate($dataBranch->id,$data->itemId,$endDate); 
                            ${"totalQty".$dataBranch->id}+= ${"stockQty".$dataBranch->id}; ?>
                        <td><?php echo ${"stockQty".$dataBranch->id};?></td>
                        <?php endforeach; else : $stockQty = Stock::getBranchWiseItemStockByDate($branch,$data->itemId,$endDate); 
                        $totalQty+=$stockQty;?><td><?php echo $stockQty;?></td><?php endif;?>
					</tr>
				<?php endforeach;
                    foreach($modelTransfer as $data) : 
					$srl++; ?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->item->itemCode;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
                        <td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
                        <?php if($branch=='all') : foreach(Branch::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Branch::STATUS_ACTIVE))) as $keyBranch=>$dataBranch) : ?>
                            ${"stockQty".$dataBranch->id} = Stock::getBranchWiseItemStockByDate($dataBranch->id,$data->itemId,$endDate); 
                            ${"totalQty".$dataBranch->id}+= ${"stockQty".$dataBranch->id}; ?>
                        <td><?php echo ${"stockQty".$dataBranch->id};?></td>
                        <?php endforeach; else : $transferQty = Stock::getBranchWiseItemStockByDate($branch,$data->itemId,$endDate);
                        $totalQty+=$transferQty; ?><td><?php echo $transferQty;?></td><?php endif;?>
					</tr>
				<?php endforeach;?>
                	<tr style="font-weight:bold;">
                      <td colspan="4" align="right">Total : </td>
                      <td>-</td>
                      <?php if($branch=='all') : foreach(Branch::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Branch::STATUS_ACTIVE))) as $keyBranch=>$dataBranch) : ?>
                            <td><?php echo ${"totalQty".$dataBranch->id};?></td>
                        <?php endforeach; else : ?><td><?php echo $totalQty;?></td><?php endif;?>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <?php if($branch=='all') : foreach(Branch::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>Branch::STATUS_ACTIVE))) as $keyBranch=>$dataBranch) : ?>
                        <td>-</td>
                        <?php endforeach; else : ?><td>-</td><?php endif;?>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>