<?php
?>
<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php
                    $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
                    if(!empty($companyModel)) : ?>
                        <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                        <?php echo $companyModel->name;
                    endif;
                    ?>
                </td>
            </tr>
            <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
            <?php endif;?>
        </table>

        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php
                    if(!empty($branchModel)) echo $branchModel->addressline;
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Opening And Closing Shop Report</td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Date Time : <?php echo $startDate;?></td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php if(!empty($branchName)) echo 'Branch : '.$branchName;?>
                </td>
            </tr>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
            <tr style="border-top:1px solid #ddd;">
                <th>Sl. No</th>
                <th class="hidden-480">BranchName</th>
                <th>Username</th>
                <th>Ip Address</th>
                <th>Opening Shop</th>
                <th>Closing Shop</th>
                <th>Login Date</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(!empty($model)) :
                $branchName='';
                $srl=0;
                foreach($model as $data) :
                    $srl++;
                    $branchModel = Branch::model()->findByPk($data->branch_id);
                    if(!empty($branchModel)) $branchName = $branchModel->name;
                    $userName=!empty($data->user_name)?$data->user_name:'';
                    $ipAddress=!empty($data->ipAddress)?$data->ipAddress:'';
                    $opening=!empty($data->lastlogin)?$data->lastlogin:'';
                    $closing=!empty($data->lastlogout)?$data->lastlogout:'';
                    $loginDate=!empty($data->crAt)?$data->crAt:'';
                    ?>
                    <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $branchName;?></td>
                    <td><?php echo $userName;?></td>
                    <td><?php echo $ipAddress;?></td>
                    <td><?php echo $opening;?></td>
                    <td><?php echo $closing;?></td>
                    <td><?php echo $loginDate;?></td>
                <?php endforeach; ?>
            <?php
            else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>