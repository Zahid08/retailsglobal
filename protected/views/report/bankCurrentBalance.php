<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
			{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Bank Current Balance Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
      $model = $bankModel;      
?>

<div class="form">
    <div style="width:98%; padding:10px;">
        <div class="row">
            <div class="span3 button_x" style="padding-top:17px; float:right">
                <?php 
                     if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
                     else Yii::app()->session['reportModel'] = $model;
                ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'bankCurrentBalancePdf'))); ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/bankCurrentBalance','branchAddress'=>$branchAddress,'companyName'=>$companyName))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
			</div>
        </div>
    </div>
    
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Bank Current Balance Report</td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>Bank</th>
                        <th>A/C No.</th>
                        <th>Total Deposit</th>
                        <th>Total Withdraw</th>
                        <th>Supplier Payment</th>
                        <th>Misc. Expense</th>
                        <th>Balance</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                if(!empty($bankModel)) :  
                    $srl = $totalDeposit = $totalWithdraw = $totalSuppWithdraw = $totalMiscExp = $balance = $grandBalance = 0;
                    foreach($bankModel as $key=>$data) :
                        $srl++; 
                        // total deposit
                        $sqlDeposit = "SELECT SUM(amount) AS totalAmount FROM pos_bank_deposit WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".BankDeposit::STATUS_ACTIVE;
				        $modelDeposit = Yii::app()->db->createCommand($sqlDeposit)->queryRow();
                        $totalDeposit+=$modelDeposit['totalAmount'];

                        // total widraw
                        $sqlWithdraw = "SELECT SUM(amount) AS totalAmount FROM `pos_bank_withdraw` WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".BankWithdraw::STATUS_ACTIVE;
                        $modelWithdraw = Yii::app()->db->createCommand($sqlWithdraw)->queryRow();
                        $totalWithdraw+=$modelWithdraw['totalAmount'];

                        // supplier withdraw
                        $sqlSuppWithdraw = "SELECT SUM(amount) AS totalAmount FROM pos_supplier_widraw WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".SupplierWidraw::STATUS_ACTIVE;
				        $modelSuppWithdraw = Yii::app()->db->createCommand($sqlSuppWithdraw)->queryRow();
                        $totalSuppWithdraw+=$modelSuppWithdraw['totalAmount'];

                        // misc expense
                        $sqlMiscExp = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND type=".Finance::STATUS_EXPENSE." AND status=".Finance::STATUS_ACTIVE;
				        $modelMiscExp = Yii::app()->db->createCommand($sqlMiscExp)->queryRow();
                        $totalMiscExp+=$modelMiscExp['totalAmount'];
                        
                        $balance = $modelDeposit['totalAmount']-($modelWithdraw['totalAmount']+$modelSuppWithdraw['totalAmount']+$modelMiscExp['totalAmount']);
                        $grandBalance+=round($balance,2);
                    ?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->name;?></td>
                        <td><?php echo $data->accountNo;?></td>
                        <td><?php echo !empty($modelDeposit['totalAmount'])?$modelDeposit['totalAmount']:0;?></td>
                        <td><?php echo !empty($modelWithdraw['totalAmount'])?$modelWithdraw['totalAmount']:0;?></td>
                        <td><?php echo !empty($modelSuppWithdraw['totalAmount'])?$modelSuppWithdraw['totalAmount']:0;?></td>
                        <td><?php echo !empty($modelMiscExp['totalAmount'])?$modelMiscExp['totalAmount']:0;?></td>
                        <td><?php echo round($balance,2);?></td>
                    </tr>
                <?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="3" align="right">Total : </td>
                        <td><?php echo round($totalDeposit,2);?></td>
                        <td><?php echo round($totalWithdraw,2);?></td>
                        <td><?php echo round($totalSuppWithdraw,2);?></td>
                        <td><?php echo round($totalMiscExp,2);?></td>
                        <td><?php echo round($grandBalance,2);?></td>
                    </tr>
                <?php else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif;?>
                </tbody>
            </table>
        </div>
    </div>
</div>