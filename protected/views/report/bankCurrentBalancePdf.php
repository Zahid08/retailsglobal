<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Bank Current Balance Report</td>
            </tr>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th>Bank</th>
                    <th>A/C No.</th>
                    <th>Total Deposit</th>
                    <th>Supplier Payment</th>
                    <th>Misc. Expense</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            if(!empty($model)) : 
                $srl = $totalDeposit = $totalSuppWithdraw = $totalMiscExp = $balance = $grandBalance = 0;
                foreach($model as $key=>$data) : 
                    $srl++; 
                    // total deposit
                    $sqlDeposit = "SELECT SUM(amount) AS totalAmount FROM pos_bank_deposit WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".BankDeposit::STATUS_ACTIVE;
                    $modelDeposit = Yii::app()->db->createCommand($sqlDeposit)->queryRow();
                    $totalDeposit+=$modelDeposit['totalAmount'];

                    // supplier withdraw
                    $sqlSuppWithdraw = "SELECT SUM(amount) AS totalAmount FROM pos_supplier_widraw WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND status=".SupplierWidraw::STATUS_ACTIVE;
                    $modelSuppWithdraw = Yii::app()->db->createCommand($sqlSuppWithdraw)->queryRow();
                    $totalSuppWithdraw+=$modelSuppWithdraw['totalAmount'];

                    // misc expense
                    $sqlMiscExp = "SELECT SUM(amount) AS totalAmount FROM pos_finance WHERE branchId=".Yii::app()->session['branchId']." AND bankId=".$data->id." AND type=".Finance::STATUS_EXPENSE." AND status=".Finance::STATUS_ACTIVE;
                    $modelMiscExp = Yii::app()->db->createCommand($sqlMiscExp)->queryRow();
                    $totalMiscExp+=$modelMiscExp['totalAmount'];

                    $balance = $modelDeposit['totalAmount']-($modelSuppWithdraw['totalAmount']+$modelMiscExp['totalAmount']);
                    $grandBalance+=round($balance,2);
                ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->name;?></td>
                    <td><?php echo $data->accountNo;?></td>
                    <td><?php echo !empty($modelDeposit['totalAmount'])?$modelDeposit['totalAmount']:0;?></td>
                    <td><?php echo !empty($modelSuppWithdraw['totalAmount'])?$modelSuppWithdraw['totalAmount']:0;?></td>
                    <td><?php echo !empty($modelMiscExp['totalAmount'])?$modelMiscExp['totalAmount']:0;?></td>
                    <td><?php echo round($balance,2);?></td>
                </tr>
            <?php endforeach; ?>
                <tr style="font-weight:bold;">
                    <td colspan="3" align="right">Total : </td>
                    <td><?php echo round($totalDeposit,2);?></td>
                    <td><?php echo round($totalSuppWithdraw,2);?></td>
                    <td><?php echo round($totalMiscExp,2);?></td>
                    <td><?php echo round($grandBalance,2);?></td>
                </tr>
            <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif;?>
            </tbody>
        </table>
    </div>
</div>