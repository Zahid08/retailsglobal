<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
			{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			    }			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Best Sales <i class="icon-angle-right"></i></li>
    <li>Global Details</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif;
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline;
?>

<div class="form">
    <div style="width:98%; padding:10px; border:1px dotted #ccc;">
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php                             
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td>
                    <td style="width: 70px;">
                        <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php                             
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Best Sales Details Report</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date Time : <?php echo date("Y-m-d H:i:s");?></td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php if(!empty($branchName)) echo 'Branch : '.$branchName;?>
                    </td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th class="hidden-480">Item Name</th>
                    <th class="hidden-480">Item Code</th>
                    <th class="hidden-480">Total Item Sell</th>
                    <th class="hidden-480">Cost of Sales</th>
                    <th class="hidden-480">Net Sales</th>
                    <th class="hidden-480">VAT</th>
                    <th class="hidden-480">Item Discount</th>
                </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = $totalItemSell = $costPrice = $salePrice = $discountPrice = $vatPrice = 0;
					foreach($model as $data) :
                        $srl++;
					   $itemId=$data['itemId'];
                        $totalItemSell+=$data['totalItemSell'];
                        $costPrice+=$data['costPrice'];
                        $salePrice+=$data['salePrice'];
                        $discountPrice+=$data['discountPrice'];
                        $vatPrice+=$data['vatPrice'];
                        // coupon Amount Processing
                        $sqlForItems = "SELECT * FROM `pos_items` WHERE `id`=".$itemId."";
                        $sqlItems = Yii::app()->db->createCommand($sqlForItems)->queryRow();
                        $itemName=$itemCode='';
                        if ($sqlItems){
                            $itemName=!empty($sqlItems['itemName'])?$sqlItems['itemName']:'';
                            $itemCode=!empty($sqlItems['itemCode'])?$sqlItems['itemCode']:'';
                        }
					?>
					<tr>
						<td><?php echo $srl;?></td>
                        <td><?php echo $itemName;?></td>
                        <td><?php echo $itemCode;?></td>
                        <td><?php echo round(($data['totalItemSell']),2);?></td>
                        <td><?php echo round($data['costPrice'],2);?></td>
                        <td><?php echo round($data['salePrice'],2);?></td>
                        <td><?php echo round($data['discountPrice'],2);?></td>
                        <td><?php echo round($data['vatPrice'],2);?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td align="right" colspan="3">Total : </td>
						<td><?php echo UsefulFunction::formatMoney(round($totalItemSell,2),true);?></td>
						<td><?php echo UsefulFunction::formatMoney(round($costPrice,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($salePrice,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($discountPrice,2),true);?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($vatPrice,2),true);?></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>