<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
				}			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Temporary Worker <i class="icon-angle-right"></i></li>
    <li>Temporary Worker Balance Summary</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	
    <div style="width:98%; padding:10px; border:1px dotted #ccc;">
		<div style="float:left;" class="span5">
				<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'login-form',
						'action'=>Yii::app()->createUrl('report/tempWorkerBalanceSummary'),
						'enableAjaxValidation'=>true,				
						
			   )); ?> 
			<div class="row">
			   <div class="span6">
					<?php echo CHtml::label('Select Worker','');?>
					<?php echo CHtml::dropDownList('wkId',$wkId,User::getTemporaryWorkerUser(Yii::app()->session['branchId']), 
							  array('class'=>'m-wrap combo combobox','prompt'=>'Select Worker',
									'id'=>'deptId',
									'options'=>array($wkId=>array('selected'=>'selected')),
									'onchange'=>'js:$("#ajax_loaderdept").show()',
									'ajax' => array('type'=>'POST', 
													'data'=>array('deptId'=>'js:this.value'),  
													'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
													'success'=>'function(data) 
													{
														$("#ajax_loaderdept").hide();
														$("#subdeptId").html(data);
													}',
									)
							)); ?>
						 <span id="ajax_loaderdept" style="display:none;">
							<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
						 </span>
				</div>
				
				<div class="span4" style="padding-top:27px;">
					<?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
				</div>
			</div>
            <?php $this->endWidget(); ?>
			</div>
            <div class="span3 button_x" style="padding-top:12px; float:right">
				 <?php 
					 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
					 else Yii::app()->session['reportModel'] = $model;
				 ?>
				 <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'tempWorkerBalanceSummaryPdf','startDate'=>$startDate,'endDate'=>$endDate))); ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/tempWorkerBalanceSummary','startDate'=>$startDate,'endDate'=>$endDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
		    </div>
        <div style="clear:both"></div>
    </div>
    
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                           if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Temporary Worker Balance Summary Report</td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';?>Date from <?php echo $startDate;?> to <?php echo $endDate;?>
                     </td>
                </tr>
                <?php endif;?>
            </table>           
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>Worker</th>                                                             
                        <th class="hidden-480">Balance</th>                        
                    </tr>
                </thead>
                <tbody>
                <?php
				if(!empty($model)) : 
					$srl = $totalQty  = $totalPrice = $rTotalQty= $costPrice= $balance=   0;
					foreach($model as $data) : 
					$srl++;
					$balance =TemporaryWorker::getWorkerBalance($data->id);
					$totalPrice+=$balance;
					?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->username;?></td>																
						<td><?php echo $balance;?></td>						
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="2" align="right">Total : </td>
						<td><?php echo $totalPrice;?></td>				
                    </tr>
                 <?php
                else : ?>
                    <tr>
                        <td>#</td>                       
                        <td>-</td>
                        <td>-</td>                       
                        <td>-</td>                       
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>