<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Suppliers Balance Summary</td>
            </tr>
            <?php if(!empty($crType) && !empty($crType)) : ?>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Supplier Type : <?php echo $crType;?> Supplier</td>
            </tr>
            <?php endif;?>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Supplier ID</th>
                    <th>Supplier Name</th>
                    <th class="hidden-480">Type</th>
                    <th class="hidden-480">Balance</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $srl = $totalBalance = 0;
            if(!empty($model)) : 
                foreach($model as $data) : $srl++; 
                $suppBalance = ($data->crType==Supplier::CREDIT_SUPP)?Supplier::getSupplierBalance($data->id):Supplier::getSupplierBalanceConsig($data->id);
                $totalBalance+=$suppBalance; ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->suppId;?></td>
                    <td><?php echo $data->name;?></td>
                    <td><?php echo $data->crType;?></td>
                    <td><?php echo $suppBalance;?></td>
                </tr>
            <?php endforeach; ?>
                <tr style="font-weight:bold;">
                    <td colspan="4" align="right">Total Balance : </td>
                    <td><?php echo round($totalBalance,2);?></td>
                </tr>
             <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>    
    </div>
</div>