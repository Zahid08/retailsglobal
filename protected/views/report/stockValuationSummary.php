<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Stock Valuation Summary Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	<div style="width:98%; padding:10px; border:1px dotted #ccc;">
	<div style="float:left;" class="span6">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->createUrl('report/stockValuationSummary'),
				'enableAjaxValidation'=>true,				
				
	   )); ?> 

	<div class="row">
		<div class="span6">
			<?php echo CHtml::label('End Date','');?>
			<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
				   //'model'=>$model,
				   'name'=>'endDate',
				   'id'=>'endDate',					   
				   'value' =>$endDate,  
				   'options'=>array(
				   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
				   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
				   'changeMonth' => 'true',
				   'changeYear' => 'true',
				   'showButtonPanel' => 'true',
				   'constrainInput' => 'false',
				   'duration'=>'normal',
				  ),
				  'htmlOptions'=>array(
					 'class'=>'m-wrap large',
				   ),
				));
			?>
		</div>
		<div class="span4" style="padding-top:17px;">
			<?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
		</div>			
	</div>    
    <?php $this->endWidget(); ?>
	</div>
		<div class="span3 button_x" style="padding-top:0px; float:right">
			 <?php 
				 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
				 else Yii::app()->session['reportModel'] = $model;
			 ?>
			 <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'stockValuationSummaryPdf','endDate'=>$endDate))); ?>
			<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/stockValuationSummary','endDate'=>$endDate,'branchAddress'=>$branchAddress,'companyName'=>$companyName))); ?>
			 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
				<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
			</a>
		</div>
		<div style="clear:both"></div>
	</div>
			
     
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            //$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            //$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Stock Valuation Summary Report</td>
                </tr>
                <?php if(!empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date till <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th class="hidden-480">Department</th>
                        <th class="hidden-480">Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php      
                /* manual stock valuation check
                $itemStockQty = Stock::getItemWiseStockByDate(1676,'2015-07-15');
                echo 'Total Stock : '.$itemStockQty.'<br/>';
                echo 'Total Cost : '.Stock::getItemFifoCostPrice(1676,-35);
                exit();*/      

				if(!empty($model) && !empty($endDate)) : 
					$srl = $totalAmount  = 0;
					foreach($model as $data) : // current month = and s.stockDate BETWEEN '".date("Y-m")."-01' AND '".$endDate."'
                        // stock model calculations
						$sql = "SELECT s.* FROM pos_items i, pos_stock s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$endDate."' and s.branchId=".Yii::app()->session['branchId']." and s.status=".Stock::STATUS_APPROVED." GROUP BY s.itemId ORDER BY s.stockDate DESC";
						$stockModel = Stock::model()->findAllBySql($sql);	
						if(!empty($stockModel)) :
							$srl++; 
							$amountStock = 0;
							foreach($stockModel as $dataStock) :
                                $itemStockQty = Stock::getItemWiseStockByDate($dataStock->itemId,$endDate);
                                $amountStock+=Stock::getItemFifoCostPrice($dataStock->itemId,$itemStockQty);
							endforeach;
                        else : $amountStock = 0;
				        endif;
                        // stock transfer model calculations
						$sqlSt = "SELECT s.* FROM pos_items i, pos_stock_transfer s, pos_category c, pos_subdepartment d WHERE s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.itemId NOT IN(SELECT itemId FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND STATUS=".Stock::STATUS_APPROVED." GROUP BY itemId) AND DATE_FORMAT(s.stockDate, '%Y-%m-%d')<='".$endDate."' AND s.toBranchId=".Yii::app()->session['branchId']." AND s.status=".Stock::STATUS_ACTIVE." GROUP BY s.itemId ORDER BY s.stockDate DESC";
						$stockTransferModel = StockTransfer::model()->findAllBySql($sqlSt);	
						if(!empty($stockTransferModel)) :
							$amountTransfer = 0;
							foreach($stockTransferModel as $dataTransfer) :
                                $itemStockTransferQty = Stock::getItemWiseStockByDate($dataTransfer->itemId,$endDate);
                                $amountTransfer+=Stock::getItemFifoCostPrice($dataTransfer->itemId,$itemStockTransferQty);
							endforeach;
                        else : $amountTransfer = 0;
				        endif; $totalAmount+=($amountStock+$amountTransfer);?>
                        <tr>
                            <td><?php echo $srl;?></td>
                            <td><?php echo $data->name;?></td>
                            <td><?php echo UsefulFunction::formatMoney(($amountStock+$amountTransfer), true);?></td>
                        </tr>
					<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="2" align="right">Total : </td>
						<td><?php echo UsefulFunction::formatMoney($totalAmount, true);?></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>