<div class="form">    
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Bank/Card Summary Report</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php else : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date : <?php echo date("Y-m-d");?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th class="hidden-480">Bank/Card</th>
					<th>Total Collection</th>
					<th>Bank Commision(%)</th>
					<th class="hidden-480">Bank Commision</th>
					<th class="hidden-480">Date</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)) : 
				$srl = $totalnetAmount = $totalbankCommision = 0;
				foreach($model as $data) : 
				$srl++; 
				//$netAmount = ($data->totalPrice+$data->totalTax)-$data->totalDiscount;
				$netAmount = $data->cardPaid;
				$bankCommision = round((($netAmount*$data->card->commision)/100),2);
				$totalnetAmount+=$netAmount;
				$totalbankCommision+=$bankCommision; ?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data->card->name;?></td>
					<td><?php echo $netAmount;?></td>
					<td><?php echo $data->card->commision;?></td>
					<td><?php echo $bankCommision;?></td>
					<td><?php echo $data->orderDate;?></td>
				</tr>
			<?php endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="2" align="right">Total : </td>
					<td><?php echo $totalnetAmount;?></td>
					<td></td>
					<td><?php echo $totalbankCommision;?></td>
					<td></td>
				</tr>
			 <?php
			 else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>