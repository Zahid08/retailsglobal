<div class="form">    
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Suppliers Credit Payment Summary</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
		</table>

		<table class="table table-striped table-hover" cellpadding="0" cellspacing="0" style=" margin:0; padding:0;border:0px;">
		<tr>
			<td>
				<table class="table table-striped table-hover bordercolumn">
					<thead>
						<tr>
							<th>Sl. No</th>
							<th>GRN Date</th>
							<th class="hidden-480">GRN No.</th>
							<th class="hidden-480">Received</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					$srl = $totalReceipt = 0;
					if(!empty($model)) : 
						foreach($model as $data) : $srl++;
						if(!empty($data->amount)){
                            $totalReceipt+=$data->amount;
                        }
						?>
						<tr>
							<td><?php echo $srl;?></td>
							<td><?php echo $data->crAt;?></td>
							<td>
                                <?php
                                if(!empty($data->grn)){
                                    echo $data->grn->grnNo;
                                }
                                ?>
                            </td>
							<td>
                                <?php
                                if(!empty($data->amount)){
                                    echo $data->amount;
                                }
                                ?>
                            </td>
						</tr>
					<?php endforeach; ?>
						<tr style="font-weight:bold;">
							<td colspan="3">Total Received : </td>
							<td><?php echo $totalReceipt;?></td>
						</tr>
					 <?php else : ?>
						<tr>
							<td>#</td>
							<td>-</td>
							<td>-</td>
							<td>-</td>
						</tr>
					<?php endif; ?>
					</tbody>
				</table>
			 </td>
			 <td>
				<table class="table table-striped table-hover bordercolumn">
					<thead>
						<tr>
							<th class="hidden-480">Payment Date</th>
							<th class="hidden-480">PGRN No.</th>
							<th class="hidden-480">Payment</th>
							<th class="hidden-480">Remarks</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						$totalPayment = 0;
                        if(!empty($modelwidraw)) : 
                            foreach($modelwidraw as $data) :
                                if(!empty($data->amount)){
                                    $totalPayment+=$data->amount;
                                }
                            ?>
                            <tr>
                                <td><?php echo $data->crAt;?></td>
                                <td><?php echo $data->pgrnNo;?></td>
                                <td>
                                    <?php
                                    if(!empty($data->amount)){
                                        echo $data->amount;
                                    }
                                    ?>
                                </td>
                                <td><?php echo ($data->bankId)>0?'Bank':'Cash';?></td>
                                <td><?php echo $data->remarks;?></td>
                            </tr>
                         <?php endforeach; ?>
                        	<tr style="font-weight:bold;">
                                <td colspan="2">Total Payment : </td>
                                <td><?php echo $totalPayment;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                         <?php else : ?>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        <?php endif; ?>
					</tbody>
				</table>
			 </td>
		  </tr>
		  <tr>
			<td colspan="2"><strong>BALANCE : <?php echo round($totalReceipt-$totalPayment,2);?></strong></td>
		  </tr>
	  </table>
	</div>
</div>