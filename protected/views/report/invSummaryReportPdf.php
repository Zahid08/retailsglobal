<div class="form">    
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
					   $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
					    $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Inventory Summary Report</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th>IV No.</th>
					<th class="hidden-480">Total Quantity</th>
					<th class="hidden-480">Total Weight</th>
					<th class="hidden-480">Total Adjust Quantity</th>
					<th class="hidden-480">Total Price</th>
					<th class="hidden-480">Total Adjust Price</th>
					<th class="hidden-480">Date</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)) : $srl = 0;
				$totalQty = $totalWeight = $totalPrice = $totaladjQty = $totaladjPrice = 0;
				foreach($model as $data) : 
					$srl++; 
					$totalQty+=$data['tQty']; $totalPrice+=$data['tPrice'];
					$totalWeight+=$data['tWeight'];
					$totaladjQty+=$data['tAdQty'];
					$totaladjPrice+=$data['tAdPrice']; ?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data['invNo'];?></td>
					<td><?php echo $data['tQty'];?></td>
					<td><?php echo $data['tWeight'];?></td>
					<td><?php echo $data['tAdQty'];?></td>
					<td><?php echo $data['tPrice'];?></td>
					<td><?php echo $data['tAdPrice'];?></td>
					<td><?php echo $data['invDate'];?></td>
				</tr>
			<?php endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="2" align="right">Total : </td>
					<td><?php echo $totalQty;?></td>
					<td><?php echo $totalWeight;?></td>
					<td><?php echo $totaladjQty;?></td>
					<td><?php echo round($totalPrice,2);?></td>
					<td><?php echo round($totaladjPrice,2);?></td>
					<td></td>
				</tr>
			 <?php else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div> 
</div>