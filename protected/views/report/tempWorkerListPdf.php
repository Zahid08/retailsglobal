<div class="form"> 
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Temporary Worker Items Report</td>
            </tr>
            <?php if(!empty($startDate) && !empty($endDate)) : ?>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                	<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';?>Date from <?php echo $startDate;?> to <?php echo $endDate;?>
                 </td>
            </tr>
            <?php endif;?>
        </table>
        
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th>Worker</th>
                    <th>TW Invoice No</th>
                    <th class="hidden-480">Item Name</th>                        
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Receive Quantity</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Date</th>
                </tr>
            </thead>
            <tbody>
            <?php            
			if(!empty($model)) : 
				$srl = $totalQty  = $totalPrice = $rTotalQty = 0;
				foreach($model as $data) : 
				$srl++;

				if(!empty($data->qty)){
                    $totalQty+=$data->qty;
                    $rTotalQty+=$data->rqty;
                    $totalPrice+=$data->qty*$data->costPrice;
                }

				?>
				<tr>
					<td><?php echo $srl;?></td>
                    <td>
                        <?php
                        if(!empty($data->wk)){
                            echo $data->wk->username;
                        }
                        ?>
                    </td>
					<td>
                        <?php
                        if(!empty($data->twNo)){
                            echo $data->twNo;
                        }
                        ?>
                    </td>
					<td>
                        <?php
                        if(!empty($data->item)){
                            echo $data->item->itemName;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (!empty($data->qty)){
                            echo $data->qty;
                        }
                        ?>
                    </td>
					<td>
                        <?php
                        if (!empty($data->rqty)){
                            echo $data->rqty;
                        }
                        ?>
                    </td>
					<td>
                        <?php
                        if(!empty($data->qty)){
                            echo $data->qty*$data->costPrice;
                        }
                        ?>
                    </td>
					<td>
                        <?php
                        if(!empty($data->twDate)){
                            echo $data->twDate;
                        }
                        ?>
                    </td>
				</tr>
			<?php endforeach; ?>
            	<tr style="font-weight:bold;">
                    <td colspan="4" align="right">Total : </td>
                    <td><?php echo $totalQty;?></td>
					<td><?php echo $rTotalQty;?></td>
                    <td><?php echo $totalPrice;?></td>
                    <td></td>
                </tr>
             <?php
            else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    
</div>