<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Purchase Order Summary Report</td>
            </tr>			
            <?php if(!empty($startDate) && !empty($endDate) && !empty($suppName)) : ?>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="1">Supplier <?php echo $suppName;?>, Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
            </tr>
            <?php endif;?>
        </table>
        
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>PO No.</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Quantity</th>
                        <th class="hidden-480">Weight</th>
                        <th class="hidden-480">Price</th>
                        <th class="hidden-480">PO Date</th>
                        <th class="hidden-480">Remarks</th>
                    </tr>
            </thead>
            <tbody>
            <?php 
				if(!empty($model)) : 
					$srl = $totalQty = $totalWeight = $totalPrice = 0;
					foreach($model as $data) : 
					$srl++; 
					$totalQty+=$data->totalQty;
					$totalWeight+=$data->totalWeight;
					$totalPrice+=$data->totalPrice; ?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->poNo;?></td>
						<td><?php echo $data->supplier->name;?></td>
						<td><?php echo $data->totalQty;?></td>
						<td><?php echo $data->totalWeight;?></td>
						<td><?php echo $data->totalPrice;?></td>
						<td><?php echo $data->poDate;?></td>
						<td><?php echo $data->remarks;?></td>
					</tr>
				<?php endforeach;   ?>
					<tr style="font-weight:bold;">
                        <td colspan="3" align="right">Total : </td>
						<td><?php echo $totalQty;?></td>
						<td><?php echo $totalWeight;?></td>
                        <td><?php echo $totalPrice;?></td>
                        <td colspan="2"></td>
                    </tr>
				<?php else: ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>