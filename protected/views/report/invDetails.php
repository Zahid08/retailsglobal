<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl;?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:
					{                         
						styleToAdd:'margin-left:25px !important',
						//classNameToAdd : 'printBody',
					}  
				}			
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Inventory Details Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	
    <div style="width:98.7%; padding:10px; border:1px dotted #ccc;">
			<div style="float:left;" class="span6">
				<?php $form=$this->beginWidget('CActiveForm', array(
						'id'=>'login-form',
						'action'=>Yii::app()->createUrl('report/invDetailsReport'),
						'enableAjaxValidation'=>true,				
						
			   )); ?> 
			   <div class="row">
				   <div class="span6">
						<?php echo CHtml::label('Inventory No.',''); ?>
						<?php $this->widget('CAutoComplete',array(
								 //'model'=>$model,
								 'id'=>'invNo',
								 'attribute' => 'invNo',
								 //name of the html field that will be generated
								 'name'=>'invNo', 
								 //replace controller/action with real ids
								 'value'=>$invNo,
					
								 'url'=>array('ajax/autoCompleteinvNoClosed'), 
								 'max'=>100, //specifies the max number of items to display
			
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap','style'=>'width:100%;','maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
								 ));
						 ?>
					 </div>
                   <div class="span6">
                       <?php echo CHtml::label('Item Code','');?>
                       <?php echo CHtml::textField('itemCode',$itemCode,array('class'=>'m-wrap large','style'=>'width:100%;','maxlength'=>255)); ?>
                   </div>			 
				</div>
			   <div class="row">
                   <div class="span6">
                       <?php echo CHtml::label('Start Date','');?>
                       <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                             //'model'=>$model,
                             'name'=>'startDate',
                             'id'=>'startDate',					   
                             'value' =>$startDate,  
                             'options'=>array(
                                 'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                                 'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                                 'changeMonth' => 'true',
                                 'changeYear' => 'true',
                                 'showButtonPanel' => 'true',
                                 'constrainInput' => 'false',
                                 'duration'=>'normal',
                             ),
                             'htmlOptions'=>array(
                                 'class'=>'m-wrap large',
                             ),
                         ));
                       ?>
                   </div>	                
					<div class="span6">
						<?php echo CHtml::label('End Date','');?>
						<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
							   //'model'=>$model,
							   'name'=>'endDate',
							   'id'=>'endDate',					   
							   'value' =>$endDate,  
							   'options'=>array(
							   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
							   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
							   'changeMonth' => 'true',
							   'changeYear' => 'true',
							   'showButtonPanel' => 'true',
							   'constrainInput' => 'false',
							   'duration'=>'normal',
							  ),
							  'htmlOptions'=>array(
								 'class'=>'m-wrap large',
								 'style'=>'width:135px;height:20px',
							   ),
							));
						?>
					</div>
					
				</div>
				<div class="row">
					<div class="span4 button" style="padding-top:0px; margin-left:0;">
					 <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
					</div>
				</div>	
           
			<?php $this->endWidget(); ?>  
			</div>
			 <?php 
				$bName=$bAdd='';
				$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
				if(!empty($companyModel)) :
					  $bName=$companyModel->name;
				endif; 
				$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
				  if(!empty($branchModel)) $bAdd=$branchModel->addressline; 
			?>

			<div class="span3 button_x" style="padding-top:12px; float:right">
				 <?php 
					if(isset(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
					else Yii::app()->session['reportModel'] = $model;
				 ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'invDetailsPdf','startDate'=>$startDate,'endDate'=>$endDate))); ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/invDetails','startDate'=>$startDate,'endDate'=>$endDate,'bAdd'=>$bAdd,'bName'=>$bName,'itemCode'=>$itemCode,'invNo'=>$invNo))); ?>
				<a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
			 </div>	
			<div style="clear:both"></div>
        </div>
    </div>
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                             if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                             if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Inventory Details Report</td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
                
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php if(!empty($itemCode)) echo 'Item Code : '.$itemCode.', ';
							  if(!empty($invNo)) echo 'Inventory NO. : '.$invNo;
						?>
                    </td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>IV No.</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Quantity</th>
                        <th class="hidden-480">Cost Price</th>
                        <th class="hidden-480">Adjust Quantity</th>
                        <th class="hidden-480">Adjust Price</th>
                        <th class="hidden-480">Ecommerce Item</th>
    					<th class="hidden-480">Child Item</th>
                        <th class="hidden-480">Date</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = 0;
					$totalQty = $totalPrice = $totaladjQty = $totaladjPrice = 0;
					foreach($model as $data) : 
						$srl++; 
						$totalQty+=$data->qty; $totalPrice+=($data->qty*$data->costPrice);
						$totaladjQty+=$data->adjustQty;
						$totaladjPrice+=($data->adjustQty*$data->costPrice); ?>
					<tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->invNo;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->qty;?></td>
                        <td><?php echo round(($data->qty*$data->costPrice),2);?></td>
                        <td><?php echo $data->adjustQty;?></td>
                        <td><?php echo round(($data->adjustQty*$data->costPrice),2);?></td>
                        <td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
    					<td><?php echo $data->item->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
                        <td><?php echo $data->invDate;?></td>
                    </tr>
					<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="4" align="right">Total : </td>
						<td><?php echo $totalQty;?></td>
						<td><?php echo round($totalPrice,2);?></td>
                        <td><?php echo $totaladjQty;?></td>
						<td><?php echo round($totaladjPrice,2);?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                 <?php else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>