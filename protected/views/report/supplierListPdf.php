<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img width="70px" src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Suppliers List</td>
            </tr>			
            <?php if(!empty($startDate) && !empty($endDate)) : ?>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
            </tr>
            <?php endif;?>
        </table>
        
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>ID</th>
                    <th class="hidden-480">Type</th>
                    <th class="hidden-480">Name</th>
                    <th class="hidden-480">Phone</th>
                    <th class="hidden-480">Email</th>
                    <th class="hidden-480">Created</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            if(!empty($model)) : $srl = 0;
                foreach($model as $data) : $srl++; ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->suppId;?></td>
                    <td><?php echo $data->crType;?></td>
                    <td><?php echo $data->name;?></td>
                    <td><?php echo $data->phone;?></td>
                    <td><?php echo $data->email;?></td>
                    <td><?php echo $data->crAt;?></td>
                </tr>
            <?php endforeach;
             else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>