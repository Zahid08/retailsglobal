<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Suppliers Credit Payment Summary</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>
<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	<div style="width:98.7%; padding:10px; border:1px dotted #ccc;">
		<div style="width:50%; float:left;">
			<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'login-form',
					'action'=>Yii::app()->createUrl('report/suppliercreditPayment'),
					'enableAjaxValidation'=>true,				
					
		   )); ?> 
			<div class="row">
				<div class="span6">
					<?php echo CHtml::label('Start Date','');?>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						   //'model'=>$model,
						   'name'=>'startDate',
						   'id'=>'startDate',					   
						   'value' =>$startDate,  
						   'options'=>array(
						   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
						   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
						   'changeMonth' => 'true',
						   'changeYear' => 'true',
						   'showButtonPanel' => 'true',
						   'constrainInput' => 'false',
						   'duration'=>'normal',
						  ),
						  'htmlOptions'=>array(
							 'class'=>'m-wrap large',
							 
						   ),
						));
					?>
				</div>
				<div class="span6">
					<?php echo CHtml::label('End Date','');?>
					<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
						   //'model'=>$model,
						   'name'=>'endDate',
						   'id'=>'endDate',					   
						   'value' =>$endDate,  
						   'options'=>array(
						   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
						   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
						   'changeMonth' => 'true',
						   'changeYear' => 'true',
						   'showButtonPanel' => 'true',
						   'constrainInput' => 'false',
						   'duration'=>'normal',
						  ),
						  'htmlOptions'=>array(
							 'class'=>'m-wrap large',
							 
						   ),
						));
					?>
				</div>          
			</div>
			
			<div class="row">
				<div class="span6">
					<?php echo CHtml::label('Supplier',''); ?>
					<?php echo CHtml::dropDownList('supId',$supplier,Supplier::getAllSupplierByType(Supplier::CREDIT_SUPP), 
							   array('id'=>'supplierId','class'=>'m-wrap combo combobox','prompt'=>'Select Supplier',
									 'onchange'=>'js:$("#ajax_loadersupplier").show()',
									  'ajax' => array('type'=>'POST', 
														//'dataType'=>'json', 
														'url'=>CController::createUrl('ajax/paymentSupplierInfoCredit'),
														'success'=>'function(data) 
														{
															$("#ajax_loadersupplier").hide();
															$("#supplier_info_credit").html(data);
														}',
										)   
								 )); ?>
				</div>
				<div class="span4" style="padding-top:22px;">
					<?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
				</div>
			</div>        
			<?php $this->endWidget(); ?>
		</div>		
		<div class="span4 button_x" style="padding-top:12px; float:right">
			 <?php 
				if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
                else Yii::app()->session['reportModel'] = $model;
				
				if(isset(Yii::app()->session['modelwidraw']) && !empty(Yii::app()->session['modelwidraw'] ) )
					unset(Yii::app()->session['modelwidraw']);
				else Yii::app()->session['modelwidraw'] = $modelwidraw;
				
			 ?>
			 <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'suppliercreditPaymentPdf','startDate'=>$startDate,'endDate'=>$endDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress))); ?>
			<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/suppliercreditPayment','startDate'=>$startDate,'endDate'=>$endDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress))); ?>
			 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
				<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
			</a>
		</div>
		<div style="clear:both"></div>
    </div>
    
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Suppliers Credit Payment Summary</td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover" cellpadding="0" cellspacing="0" style=" margin:0; padding:0;border:0px;">
            <tr>
            	<td>
                    <table class="table table-striped table-hover bordercolumn">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>GRN Date</th>
                                <th class="hidden-480">GRN No.</th>
                                <th class="hidden-480">Received</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
						$srl = $totalReceipt = 0;
                        if(!empty($model)) : 
                            foreach($model as $data) : $srl++; 
							$totalReceipt+=$data->amount;
							?>
                            <tr>
                                <td><?php echo $srl;?></td>
                                <td><?php echo $data->crAt;?></td>
                                <td><?php echo $data->grn->grnNo;?></td>
                                <td><?php echo $data->amount;?></td>
                            </tr>
                        <?php endforeach; ?>
                        	<tr style="font-weight:bold;">
                                <td colspan="3">Total Received : </td>
                                <td><?php echo $totalReceipt;?></td>
                            </tr>
                         <?php else : ?>
                            <tr>
                                <td>#</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                 </td>
                 <td>
                    <table class="table table-striped table-hover bordercolumn">
                        <thead>
                            <tr>
                                <th class="hidden-480">Payment Date</th>
                                <th class="hidden-480">PGRN No.</th>
                                <th class="hidden-480">Amout</th>
                                <th>Payment</th>
                                <th class="hidden-480">Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
						$totalPayment = 0;
                        if(!empty($modelwidraw)) : 
                            foreach($modelwidraw as $data) : $totalPayment+=$data->amount; ?>
                            <tr>
                                <td><?php echo $data->crAt;?></td>
                                <td><?php echo $data->pgrnNo;?></td>
                                <td><?php echo $data->amount;?></td>
                                <td><?php echo ($data->bankId)>0?'Bank':'Cash';?></td>
                                <td><?php echo $data->remarks;?></td>
                            </tr>
                         <?php endforeach; ?>
                        	<tr style="font-weight:bold;">
                                <td colspan="2">Total Payment : </td>
                                <td><?php echo $totalPayment;?></td>
                                <td></td>
                                <td></td>
                            </tr>
                         <?php else : ?>
                            <tr>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                 </td>
              </tr>
              <tr>
              	<td colspan="2"><strong>BALANCE : <?php echo round($totalReceipt-$totalPayment,2);?></strong></td>
              </tr>
          </table>
        </div>
    </div>
</div>