<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl;?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Purchase Return Details Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	
    <div style="width:98.7%; padding:10px; border:1px dotted #ccc;">
        <div class="row">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form',
                    'action'=>Yii::app()->createUrl('report/purchaseReturnDetailsList'),
                    'enableAjaxValidation'=>true,				
                    
           )); ?> 
            <div class="span4">
                <?php echo CHtml::label('PR No.',''); ?>
                <?php $this->widget('CAutoComplete',array(
						 //'model'=>$model,
						 'id'=>'prNo',
						 'attribute' => 'prNo',
						 //name of the html field that will be generated
						 'name'=>'prNo', 
						 //replace controller/action with real ids
						 'value'=>$prNo,
			
						 'url'=>array('ajax/autoCompletePrNo'), 
						 'max'=>100, //specifies the max number of items to display
	
									 //specifies the number of chars that must be entered 
									 //before autocomplete initiates a lookup
						 'minChars'=>2, 
						 'delay'=>500, //number of milliseconds before lookup occurs
						 'matchCase'=>false, //match case when performing a lookup?
						 'mustMatch' => true,
									 //any additional html attributes that go inside of 
									 //the input field can be defined here
						 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
													
						 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
						 //'extraParams' => array('taskType' => 'desc'),
						 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
						 ));
				 ?>
            </div>
            <div class="span4" style="padding-top:23px;">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
            </div>
			
            <?php $this->endWidget(); ?>
			<div class="span3 button_x" style="padding-top:12px; float:right">
				 <?php 
					 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
					 else Yii::app()->session['reportModel'] = $model;
				 ?>
				 <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'purchaseReturnDetailsListPdf','prNo'=>$prNo))); ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/purchaseReturnDetailsList','prNo'=>$prNo,'branchAddress'=>$branchAddress,'companyName'=>$companyName))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
		   </div>
        </div>
    </div> 
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Purchase Return Details Report</td>
                </tr>
                <?php if(!empty($prNo)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">PR No. <?php echo $prNo;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Quantity</th>
                        <th class="hidden-480">Cost Price</th>                        
						<th class="hidden-480">Ecommerce Item</th>
					    <th class="hidden-480">Child Item</th>                    
                        <th class="hidden-480">PR Date</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = $totalQty  = $totalPrice = 0;
					foreach($model as $data) : 
					$srl++; 
					$totalQty+=$data->qty;
					$totalPrice+=$data->qty*$data->costPrice; ?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->item->itemCode;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
						<td><?php echo round($data->qty,2);?></td>
						<td><?php echo round(($data->qty*$data->costPrice),2);?></td>
						<td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
    					<td><?php echo $data->item->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
						<td><?php echo $data->prDate;?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="4" align="right">Total : </td>
						<td><?php echo $totalQty;?></td>
                        <td><?php echo UsefulFunction::formatMoney(round($totalPrice,2));?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>