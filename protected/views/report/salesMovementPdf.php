<div class="form">    
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Sales Movement Report</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';
						  if(!empty($subdeptName)) echo 'Sub Department : '.$subdeptName.', ';
						  if(!empty($suppName)) echo 'Supplier : '.$suppName.', ';
						  if(!empty($catName)) echo 'Category : '.$catName;
					?>
				</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Sl. No</th>
					<th class="hidden-480">Item Code</th>
					<th class="hidden-480">Description</th>
					<th class="hidden-480">Sold Quantity</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			if(!empty($model)) : 
				$srl = $totalQty  = 0;
				foreach($model as $data) : 
				$srl++; 
				$itemModel = Items::model()->findByPk($data['itemId']);
				$totalQty+=$data['totalSales'];?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $itemModel->itemCode;?></td>
					<td><?php echo $itemModel->itemName;?></td>
					<td><?php echo round($data['totalSales'],2);?></td>
				</tr>
			<?php endforeach; ?>
				<tr style="font-weight:bold;">
					<td colspan="3" align="right">Total : </td>
					<td><?php echo round($totalQty,2);?></td>
				</tr>
			 <?php
			 else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>
</div>