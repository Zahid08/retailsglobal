<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);   
?>


<div class="form">    
  <div class="row">
      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
          <tr>
              <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                  <?php 
                      if(!empty($companyModel)) : ?>
                          <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                          <?php echo $companyModel->name;
                      endif; 
                  ?>
              </td> 
          </tr>
          <tr>
              <td style="text-align:center;padding:3px 0px;" colspan="2">
                  <?php 
                     if(!empty($branchModel)) echo $branchModel->addressline;  
                  ?>
              </td>
          </tr>
          <tr>
              <td style="text-align:center;padding:3px 0px;" colspan="2">Coupon Wise Sales Report</td>
          </tr>
          <?php if(!empty($startDate) && !empty($endDate)) : ?>
          <tr>
              <td style="text-align:center;padding:3px 0px;" colspan="2">Date <?php echo $startDate.' to  '.$endDate;?></td>
          </tr>
	   <?php endif;?>
      </table>
      
      <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th>Invoice No.</th>
                    <th>Coupon No.</th>
                    <th>Amount</th>
                    <th>Date</th>                                           
                </tr>
            </thead>
            <tbody>
            <?php 
                if(!empty($model)) : $srl=0;$cuponAMount=0;
                    foreach ($model as $key => $value) : $srl++;
                        $cuponAMount+=$value->coupon->couponAmount;?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $value->invNo;?></td>
                    <td><?php echo $value->coupon->couponNo;?></td>
                    <td><?php echo round($value->coupon->couponAmount,2);?></td>                      
                    <td><?php echo $value->crAt;?></td>                                             
                </tr>
                <?php endforeach;?>
                  <tr style="font-weight:bold;">
                    <td colspan="3">Total Amount</td>
                    <td><?php echo round($cuponAMount,2); ?></td>
                    <td></td>  
                </tr>                            
                <?php else: ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>	              	
                  </tr>             	
            <?php endif;?>
            </tbody>
        </table>
  </div>
</div>