<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Cash in Hands Report</td>
            </tr>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th>Cash Withdraw</th>
                    <th>Supplier Payment(Cash)</th>
                    <th>Worker Payment(Cash)</th>
                    <th>Misc. Expense(Cash)</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td><?php echo !empty($modelWithdraw)?$modelWithdraw:0;?></td>
                    <td><?php echo !empty($modelSuppWithdraw)?$modelSuppWithdraw:0;?></td>
                    <td><?php echo !empty($modelWkWithdraw)?$modelWkWithdraw:0;?></td>
                    <td><?php echo !empty($modelMiscExp)?$modelMiscExp:0;?></td>
                    <td><?php echo $balance;?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>