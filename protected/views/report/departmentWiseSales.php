<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			    }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Department Wise Sales Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$companyName=$branchAddress='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $companyName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $branchAddress=$branchModel->addressline; 
?>


<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	<div style="width:98%; padding:10px; border:1px dotted #ccc;">
	 <div style=" float:left;" class="span6">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->createUrl('report/departmentWiseSales'),
				'enableAjaxValidation'=>true,				
				
	   )); ?> 
	   
		<div class="row">
			<div class="span4">
				<?php echo CHtml::label('Start Date','');?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					   //'model'=>$model,
					   'name'=>'startDate',
					   'id'=>'startDate',					   
					   'value' =>$startDate,  
					   'options'=>array(
					   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
					   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
					   'changeMonth' => 'true',
					   'changeYear' => 'true',
					   'showButtonPanel' => 'true',
					   'constrainInput' => 'false',
					   'duration'=>'normal',
					  ),
					  'htmlOptions'=>array(
						 'class'=>'m-wrap large',
					   ),
					));
				?>
			</div>
			<div class="span4">
				<?php echo CHtml::label('End Date','');?>
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					   //'model'=>$model,
					   'name'=>'endDate',
					   'id'=>'endDate',					   
					   'value' =>$endDate,  
					   'options'=>array(
					   'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
					   'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
					   'changeMonth' => 'true',
					   'changeYear' => 'true',
					   'showButtonPanel' => 'true',
					   'constrainInput' => 'false',
					   'duration'=>'normal',
					  ),
					  'htmlOptions'=>array(
						 'class'=>'m-wrap large',
					   ),
					));
				?>
			</div>
            <div class="span4" style="margin-top: 5px;">
			    <?php echo CHtml::label('&nbsp;','');?>
				<?php echo CHtml::radioButtonList('salesType',$salesType,array('1'=>'Online Sale','2'=>'Local Sale'),array('labelOptions'=>array('style'=>'display:inline'),'separator'=>'&nbsp',)); ?>
			</div>
		</div>
		<div class="row">		
			<div class="span4 _button" style="padding-top:24px;margin-left:0;">
				<?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
			</div>
		</div>
	</div>
    <?php $this->endWidget(); ?>
		<div class="span3 button_x" style="padding-top:12px; float:right">
            	 <?php 
				 	if(isset(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
				 	else Yii::app()->session['reportModel'] = $model;
				 ?>
				<?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'departmentWiseSalesPdf','startDate'=>$startDate,'endDate'=>$endDate,'salesType'=>$salesType))); ?>
			    <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/departmentWiseSales','startDate'=>$startDate,'companyName'=>$companyName,'branchAddress'=>$branchAddress,'endDate'=>$endDate,'salesType'=>$salesType))); ?>
				 <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
				</a>
		 </div>
		<div style="clear:both"></div>
    </div>
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                           if(!empty($branchModel)) echo $branchModel->addressline;  
  						   if($salesType==1) $typeMsg = ' (Online Sale)';
						   else if ($salesType==2) $typeMsg = ' (Local Sale)';
						   else $typeMsg = '';
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Department Wise Sales Report <?php echo $typeMsg; ?></td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th class="hidden-480">Department</th>					
                        <th class="hidden-480">Cost of Sales</th>
                        <th class="hidden-480">Net Sales</th>						
                        <th class="hidden-480">VAT</th>
                        <th class="hidden-480">Item Discount</th>
                        <th class="hidden-480">Gross Sales</th>
                        <th class="hidden-480">Gross Profit</th>
                        <th class="hidden-480">GP %</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                $srl = $totalCostSales  = $totalNetSales = $totalNetSalesVat = $totalSalesDiscount = 0;
				if(!empty($model) && !empty($startDate) && !empty($endDate)) : 
                    // local & online sell filtering
                    $filter = '';
                    if(isset($salesType) && !empty($salesType))
                       $filter.=' AND v.isEcommerce='.$salesType;
					   foreach($model as $data) : 

						// cost of sales total
						/*$sqlpurchase = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_stock s,pos_items i,pos_category c,pos_subdepartment d WHERE s.grnId=n.id 
AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.branchId=branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate BETWEEN '".$startDate."' AND '".$endDate."' ORDER BY n.grnDate DESC";
						$modelpurchase = Yii::app()->db->createCommand($sqlpurchase)->queryRow();*/

						$sqlcostsales = "SELECT ROUND(SUM(s.qty*s.costPrice),2) AS totalCostPrice FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." ORDER BY s.salesDate DESC";
						$modelcostsales = Yii::app()->db->createCommand($sqlcostsales)->queryRow();
						
						// net sales
						$sqlnetsales = "SELECT ROUND((SUM(s.qty*s.salesPrice)-SUM(s.discountPrice)),2) AS totalSell FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." ORDER BY s.salesDate DESC";
						$modelnetsales = Yii::app()->db->createCommand($sqlnetsales)->queryRow();
						
						// net vat
						$sqlnetsalesvat = "SELECT ROUND(SUM(s.vatPrice),2) AS totalTax FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." ORDER BY v.orderDate DESC";
						$modelnetsalesvat = Yii::app()->db->createCommand($sqlnetsalesvat)->queryRow();
                        
                        // net discount
						$sqlnetsalesdiscount = "SELECT ROUND(SUM(s.discountPrice),2) AS totalDiscount FROM pos_items i, pos_sales_invoice v, pos_sales_details s, pos_category c, pos_subdepartment d WHERE v.id=s.salesId AND s.itemId=i.id AND i.catId=c.id AND c.subdeptId=d.id AND d.deptId=".$data->id." AND s.salesDate BETWEEN '".$startDate."' AND '".$endDate."' AND v.branchId=".Yii::app()->session['branchId']." ".$filter." AND s.status<>".SalesDetails::STATUS_INACTIVE." ORDER BY v.orderDate DESC";
						$modelnetsalesdiscount = Yii::app()->db->createCommand($sqlnetsalesdiscount)->queryRow();
						
						$srl++; 
						$totalCostSales+=$modelcostsales['totalCostPrice'];
						$totalNetSales+=$modelnetsales['totalSell'];
						$totalNetSalesVat+= $modelnetsalesvat['totalTax'];
                        $totalSalesDiscount+= $modelnetsalesdiscount['totalDiscount'];
						?>
                        <tr>
                            <td><?php echo $srl;?></td>
                            <td><?php echo $data->name;?></td>						
                            <td><?php echo UsefulFunction::formatMoney($modelcostsales['totalCostPrice'], true);?></td>
                            <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell'], true);?></td>
							<td><?php echo UsefulFunction::formatMoney($modelnetsalesvat['totalTax'], true);?></td>
                            <td><?php echo UsefulFunction::formatMoney($modelnetsalesdiscount['totalDiscount'], true);?></td>
                            <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell']+$modelnetsalesvat['totalTax']+$modelnetsalesdiscount['totalDiscount'], true);?></td>
                            <td><?php echo UsefulFunction::formatMoney($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'], true);?></td>
                            <td><?php if($modelnetsales['totalSell']>0) echo round((($modelnetsales['totalSell']-$modelcostsales['totalCostPrice'])*100)/$modelnetsales['totalSell'],2);?></td>
                        </tr>
				  <?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="2" align="right">Total : </td>
						<td><?php echo UsefulFunction::formatMoney($totalCostSales, true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($totalNetSales, true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($totalNetSalesVat, true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($totalSalesDiscount, true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($totalNetSales+$totalNetSalesVat+$totalSalesDiscount, true);?></td>
                        <td><?php echo UsefulFunction::formatMoney($totalNetSales-$totalCostSales, true);?></td>
                        <td><?php if($totalNetSales>0) echo round((($totalNetSales-$totalCostSales)*100)/$totalNetSales,2);?></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>  
                        <td>-</td> 
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <div class="row">
        <div class="span3">
            <?php
                // sales return cost Amount Processing
                $sqlReturnCostAmount = "SELECT SUM(r.qty*s.costPrice) AS totalReturnCostAmount FROM pos_sales_return r, pos_sales_details s
WHERE r.salesId=s.salesId AND r.itemId=s.itemId AND r.branchId=".Yii::app()->session['branchId']." AND r.returnDate BETWEEN '".$startDate."' AND '".$endDate."' AND r.status=".SalesReturn::STATUS_ACTIVE." ORDER BY r.returnDate DESC";
                $modelReturnCostAmount = Yii::app()->db->createCommand($sqlReturnCostAmount)->queryRow();
            ?>
            <h5 style="font-weight:bold;">Cost of Sales Calculation : </h5>
            <div style="height:80px;border:1px dashed #ddd;padding:10px;">
                <p>Cost of Sales : <?php echo UsefulFunction::formatMoney($totalCostSales, true);?></p>
                <p>Cost of Sales Return : <?php echo UsefulFunction::formatMoney($modelReturnCostAmount['totalReturnCostAmount'], true);?></p>
                <p style="border-top:1px solid #ddd;font-weight:bold;">Final Cost of Sales : <?php echo UsefulFunction::formatMoney(($totalCostSales-$modelReturnCostAmount['totalReturnCostAmount']), true);?></p>
            </div>
        </div>
        <div class="span3">
            <?php
                // sales return Amount Processing
                $sqlReturnAmount = "SELECT SUM(r.totalPrice) AS totalReturnAmount FROM pos_sales_return r WHERE r.branchId=".Yii::app()->session['branchId']." AND r.returnDate BETWEEN '".$startDate."' AND '".$endDate."' AND r.status=".SalesReturn::STATUS_ACTIVE." ORDER BY r.returnDate DESC";
                $modelReturnAmount = Yii::app()->db->createCommand($sqlReturnAmount)->queryRow();

                // instant discount
                $sqlId = "SELECT SUM(instantDiscount) AS totalInstantDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate BETWEEN '".$startDate."' AND '".$endDate."' AND status<>".SalesInvoice::STATUS_INACTIVE." ORDER BY orderDate DESC";
                $resId = Yii::app()->db->createCommand($sqlId)->queryRow();
            ?>
            <h5 style="font-weight:bold;">Net Sales Calculation : </h5>
            <div style="height:110px;border:1px dashed #ddd;padding:10px;">
                <p>Net Sales : <?php echo UsefulFunction::formatMoney($totalNetSales, true);?></p>
                <p>Sales Return : <?php echo UsefulFunction::formatMoney($modelReturnAmount['totalReturnAmount'], true);?></p>
                <p>Instant Discount : <?php echo UsefulFunction::formatMoney($resId['totalInstantDiscount'], true);?></p>
                <p style="border-top:1px solid #ddd;font-weight:bold;">Final Net Sales : <?php echo UsefulFunction::formatMoney(($totalNetSales-($modelReturnAmount['totalReturnAmount']+$resId['totalInstantDiscount'])), true);?></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="span6" style="height:20px;border:1px dashed #ddd;padding:5px 10px;">
            <p style="font-weight:bold;text-align:center;">Final Profit : <?php echo UsefulFunction::formatMoney((($totalNetSales-$modelReturnAmount['totalReturnAmount'])-($totalCostSales-$modelReturnCostAmount['totalReturnCostAmount'])), true);?></p>
        </div>
    </div>
</div>

<style>
#localSale_1{
	margin-left:5px !important;
}
#onlineSale_1{
	margin-left:5px !important;
}
</style>