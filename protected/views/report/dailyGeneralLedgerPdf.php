<div class="form"> 
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
					  $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">GL Report</td>
			</tr>
			<?php if(!empty($startDate) && !empty($endDate)) : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
			</tr>
			<?php else : ?>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Date : <?php echo $pdate;?></td>
			</tr>
			<?php endif;?>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr style="border-top:1px solid #ddd;">
					<th>Income</th>
					<th>Expense</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td width="50%">
						<?php
							$income = $expense = $profit = $closingStock = 0;
							
							// closing stock = total purchase+opening stock - salesCostPrice
							$closingStock = ($modelpurchase['totalPurchase']+$modelopeningstock['totalPurchase']) - $modelcostsales['totalCostPrice'];
							
							$income = ($modelsales['totalSell']-$modelsalesreturn['totalAmount'])+$modelpr['totalCost']+$modelmi['totalAmount']+$modelpurchasediscount['totalPurchase']+$closingStock;
							$expense = $modelpurchase['totalPurchase']+$modelopeningstock['totalPurchase']+$modeldam['totalAmount']+$modelwas['totalAmount']+$modelmiex['totalAmount']+$modelsalesdiscountexpense['totalSell']+$modelbc['totalCommision'];
							$profit = $income-$expense;
						?>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Net Sales</strong> : 
							<?php echo !empty($modelsales['totalSell'])?UsefulFunction::formatMoney(round(($modelsales['totalSell']-$modelsalesreturn['totalAmount']),2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Purchase Return</strong> : 
							<?php echo !empty($modelpr['totalCost'])?UsefulFunction::formatMoney(round($modelpr['totalCost'],2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Misc. Income</strong> : 
							<?php echo !empty($modelmi['totalAmount']) || !empty($modelpurchasediscount['totalPurchase'])?UsefulFunction::formatMoney(round(($modelmi['totalAmount']+$modelpurchasediscount['totalPurchase']),2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Transfer Out</strong> : 0</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Closing Stock</strong> : <?php echo UsefulFunction::formatMoney(round($closingStock,2));?></p>
						<p style="border-bottom:1px solid #ddd;padding:0 0 3px 0;margin:3px 0px;">&nbsp;</p>
					</td>
					<td width="50%">
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Purchase</strong> : 
							<?php echo !empty($modelpurchase['totalPurchase'])?UsefulFunction::formatMoney(round($modelpurchase['totalPurchase'],2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Damages</strong> : 
							<?php echo !empty($modeldam['totalAmount'])?UsefulFunction::formatMoney(round($modeldam['totalAmount'],2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Wastages</strong> :
							<?php echo !empty($modelwas['totalAmount'])?UsefulFunction::formatMoney(round($modelwas['totalAmount'],2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Misc. Expense</strong> : 
							<?php echo !empty($modelmiex['totalAmount']) || !empty($modelsalesdiscountexpense['totalSell'])?UsefulFunction::formatMoney(round(($modelmiex['totalAmount']+$modelsalesdiscountexpense['totalSell']),2)):'0';?>
						</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0;margin:3px 0px;"><strong>Transfer In</strong> : 0</p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Opening Stock</strong> : <?php echo UsefulFunction::formatMoney(round($modelopeningstock['totalPurchase'],2));?></p>
						<p style="border-bottom:1px solid #ddd; padding:0 0 3px 0; margin:3px 0px;"><strong>Bank Commision</strong> : 
							<?php echo !empty($modelbc['totalCommision'])?UsefulFunction::formatMoney(round($modelbc['totalCommision'],2)):'0';?>
						</p>
						<p style="padding:0 0 3px 0;margin:3px 0px;"><strong>Profit</strong> : <?php echo UsefulFunction::formatMoney(round($profit,2));?></p>
					</td> 
				</tr>
				
				<tr style="font-weight:bold; background:#eee;">
					<td><?php echo UsefulFunction::formatMoney(round($income,2));?></td>
					<td><?php echo UsefulFunction::formatMoney(round($expense+$profit,2));?></td>
				</tr>
			
			</tbody>
		</table>
	</div>
   
</div>