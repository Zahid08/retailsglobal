<div class="form">
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Items List</td>
            </tr>
            <?php if(!empty($startDate) && !empty($endDate)) : ?>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
            </tr>
            <?php endif;?>
        </table>
        
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Barcode</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Category</th>
                    <th class="hidden-480">Brand</th>
                    <th class="hidden-480">Supplier</th>
                    <th class="hidden-480">Ecommerce Item</th>
                    <th class="hidden-480">Child Item</th>
                    <th class="hidden-480">Creation</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            if(!empty($model)) : $srl = 0;
                foreach($model as $data) : $srl++; ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->itemCode;?></td>
                    <td><?php echo ($data->barCode==0)?'':$data->barCode;?></td>
                    <td><?php echo $data->itemName;?></td>
                    <td><?php echo $data->cat->name;?></td>
                    <td><?php echo $data->brand->name;?></td>
                    <td><?php echo $data->supplier->name;?></td>
                    <td><?php echo $data->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
                    <td><?php echo $data->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
                    <td><?php echo $data->crAt;?></td>
                </tr>
            <?php endforeach;
             else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>