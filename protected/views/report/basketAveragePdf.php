<div class="form">  
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php                         
                            $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline; 

                            if($salesType==1) $typeMsg = ' (Online Sale)';
                            else if ($salesType==2) $typeMsg = ' (Local Sale)';
                            else $typeMsg = ''; 
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Basket Average Analysis Report <?php echo $typeMsg; ?></td>
                </tr>
                <?php if(!empty($startDate) && !empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date from <?php echo $startDate;?> to <?php echo $endDate;?></td>
                </tr>
				<?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th>Sl. No</th>
                        <th>Sales Date</th>
                        <th>Total Sales</th>
                        <th>Total Bill</th>
                        <th>Busket Average</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = $totalnetAmount = $totalBill =  0;
					foreach($model as $data) : 
					$srl++; 
					$totalnetAmount+=$data['totalAmount'];
					$totalBill+=$data['totalBill']; ?>
					<tr>
						<td><?php echo $srl;?></td>
                        <td><?php echo $data['orderDate'];?></td>
						<td><?php echo $data['totalAmount'];?></td>
                        <td><?php echo $data['totalBill'];?></td>
                        <td><?php echo round(($data['totalAmount']/$data['totalBill']),2);?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td align="right" colspan="2">Total : </td>
						<td><?php echo $totalnetAmount;?></td>
						<td><?php echo $totalBill;?></td>
                        <td><?php echo round(($totalnetAmount/$totalBill),2);?></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    
</div>