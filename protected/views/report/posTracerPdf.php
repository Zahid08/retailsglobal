<div class="form">      
    <div class="row">
        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                            <?php echo $companyModel->name;
                        endif; 
                    ?>
                </td> 
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <?php 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                        if(!empty($branchModel)) echo $branchModel->addressline;  
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">POS Tracer Report</td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">Date : <?php echo date("Y-m-d");?></td>
            </tr>
        </table>

        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr style="border-top:1px solid #ddd;">
                    <th>Sl. No</th>
                    <th>POS</th>
                    <th>Cashier</th>
                    <th>Cash</th>
                    <th>Card</th>
                    <th>Discount</th>
                    <th>Reedem Amount</th>
                    <th>Coupon Amount</th>
                    <th>Gross Total</th>
                    <th>Avg. Busket</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            if(!empty($model)) : 
                $srl = $cashPaid = $cardPaid = $totalDiscount = $loyaltyPaid = $totalCouponAmount = $netPaid = $totalPaid = 0;
                foreach($model as $data) : 
                    $srl++; 
                    $cashPaid+=$data['cashPaid'];
                    $cardPaid+=$data['cardPaid'];
                    $totalDiscount+=$data['totalDiscount'];
                    $loyaltyPaid+=$data['loyaltyPaid'];
                    $totalPaid+=$data['totalPrice'];

                    // coupon Amount Processing
                    $sqlCouponAmount = "SELECT SUM(c.couponAmount) AS totalCouponAmount FROM pos_sales_invoice i,pos_coupons c WHERE i.`couponId`=c.id AND i.crBy=".$data['crBy']." AND i.branchId=".Yii::app()->session['branchId']." AND i.orderDate LIKE '".date("Y-m-d")."%' AND i.isEcommerce=".SalesInvoice::IS_NOT_ECOMMERCE." AND i.status=".SalesInvoice::STATUS_ACTIVE." ORDER BY i.orderDate DESC";
                    $modelCouponAmount = Yii::app()->db->createCommand($sqlCouponAmount)->queryRow();
                    $totalCouponAmount+=$modelCouponAmount['totalCouponAmount'];
                ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data['pos'];?></td>
                    <td><?php echo $data['cashier'];?></td>
                    <td><?php echo round($data['cashPaid'],2);?></td>
                    <td><?php echo round($data['cardPaid'],2);?></td>
                    <td><?php echo round($data['totalDiscount'],2);?></td>
                    <td><?php echo round($data['loyaltyPaid'],2);?></td>
                    <td><?php echo round($modelCouponAmount['totalCouponAmount'],2);?></td>
                    <td><?php echo round($data['totalPrice'],2);?></td>
                    <td><?php echo $data['totalPrice']/$data['totalBill'];?></td>
                </tr>
            <?php endforeach; ?>
                <tr style="font-weight:bold;">
                    <td align="right" colspan="3">Total : </td>
                    <td><?php echo UsefulFunction::formatMoney(round($cashPaid,2));?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($cardPaid,2));?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalDiscount,2));?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($loyaltyPaid,2));?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalCouponAmount,2));?></td>
                    <td><?php echo UsefulFunction::formatMoney(round($totalPaid,2));?></td>
                    <td></td>
                </tr>
             <?php
             else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>