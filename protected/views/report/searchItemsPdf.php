<div class="form">	
	<div class="row">
		<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
			<tr>
				<td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
					<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
							<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
							<?php echo $companyModel->name;
						endif; 
					?>
				</td> 
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
				</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">Item Search Result</td>
			</tr>
			<tr>
				<td style="text-align:center;padding:3px 0px;" colspan="2">
					<?php if(!empty($brandName)) echo 'Brand : '.$brandName.', ';
						  if(!empty($suppName)) echo 'Supplier : '.$suppName.', ';
						  if(!empty($catName)) echo 'Category : '.$catName;
					?>
				</td>
			</tr>
		</table>
		
		<table class="table table-striped table-hover bordercolumn">
			<thead>
				<tr>
					<th>Sl. No</th>
					<th class="hidden-480">Brand</th>
					<th class="hidden-480">Supplier</th>
					<th class="hidden-480">Category</th>
					<th class="hidden-480">Item Code</th>
					<th class="hidden-480">Bar Code</th>
					<th class="hidden-480">Description</th>
					<th class="hidden-480">Cost Price</th>
					<th class="hidden-480">Sell Price</th>
					<th class="hidden-480">Weighted</th>
				</tr>
			</thead>
			<tbody>
			<?php 
			echo '<pre>';
			print_r($model);
			echo '</pre>';
			?>
			<?php 
			
			if(!empty($model)) : 
				$srl = 0;
				foreach($model as $data) : 
				$srl++; 
				?>
				<tr>
					<td><?php echo $srl;?></td>
					<td><?php echo $data->brand->name;?></td>
					<td><?php echo $data->supplier->name;?></td>
					<td><?php echo $data->cat->name;?></td>
					<td><?php echo $data->itemCode;?></td>
					<td><?php echo $data->barCode;?></td>
					<td><?php echo $data->itemName;?></td>
					<td><?php echo $data->costPrice;?></td>
					<td><?php echo $data->sellPrice;?></td>
					<td><?php echo $data->isWeighted;?></td>
				</tr>
			<?php endforeach; 
			else : ?>
				<tr>
					<td>#</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
	</div>

</div>