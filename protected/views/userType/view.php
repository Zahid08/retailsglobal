<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
  	<li>User Type <i class="icon-angle-right"></i></li>
    <li>Details User Type : <?php echo $model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'data'=>$model,
		'type' => 'striped bordered condensed',
        'attributes'=>array(
            'name',		
			array(               
               'label'=>'Status ',               
               'value'=>Lookup::item("Status", $model->status),
             ), 
			 'rank'
        ),
    )); ?>
</div>