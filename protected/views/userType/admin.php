<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
	 <li>User Type <i class="icon-angle-right"></i></li>
    <li>Manage User Type</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
	<?php 
     $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
    
     $this->widget('bootstrap.widgets.TbExtendedGridView',array(
        'id'=>'user-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'type' => 'striped bordered condensed',
		'template' => '{summary}{items}{pager}',
		'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
        'columns'=>array(
            'name',
            /*array(            
                    'name'=>'isType',
                    'value'=>'Lookup::item("Status", $data->isType)',
                    'filter'=>UserType::getAllUserIsType(UserType::STATUS_ACTIVE),
            ),*/
             array(            
                    'name'=>'status',
                    'value'=>'Lookup::item("Status", $data->status)',
                    'filter'=>Lookup::items("Status"),
            ),
            'rank',
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                //'template'=>'{update}',
                'header'=>CHtml::dropDownList('pageSize',
                    $pageSize,
                    array(5=>5,10=>10,20=>20,50=>50,100=>100),
                    array(
                   //
                   // change 'user-grid' to the actual id of your grid!!
                    'onchange'=>
                    "$.fn.yiiGridView.update('user-type-grid',{ data:{pageSize: $(this).val() }})",
                )),
            ),
        ),
    )); ?>
</div>
