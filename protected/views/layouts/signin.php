<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
    <?php if(isset($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
    <?php if(isset($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>
	
    <meta name="subject" content="Unlocklive" />
    <meta name="language" content="English" />
    <meta name="copyright" content="© www.unlocklive.com/">
    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Googlebot" content="all" />
    <meta name="language" content="en" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="Rating" content="General" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="coverage" content="Worldwide" />
    <meta name = "Server" content = "New" />
    <meta name="expires" content="0" />
    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
    <meta name="reply-to" content="info@unlocklive.com" />
    
    <?php // configure theme skin path
		$themeSkinPath = Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];
    ?>
   
	<!-- END GLOBAL MANDATORY STYLES -->	
    <?php 
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
	if(!empty($companyModel)) : ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>
    <?php endif; ?>
    
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link href="<?php echo $themeSkinPath; ?>/css/pages/login-soft.css" rel="stylesheet" type="text/css"/>
	<!-- END PAGE LEVEL STYLES -->
	<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
	<!-- BEGIN LOGO -->
	<div class="logo">
        <?php 
        if(!empty($companyModel)) echo CHtml::link('<img src="'.Yii::app()->baseUrl.$companyModel->logo.'" style="height:88px;" />',array('eshop/index'),array('title'=>$companyModel->name));
        else echo '<img src="'.$themeSkinPath.'/img/logo.png" alt="" /> ';?>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<?php echo $content; ?>  
	</div>
	<!-- END LOGIN -->
	<!-- BEGIN COPYRIGHT -->
	<div class="copyright">
		2014 &copy; Unlocklive.
	</div>
	<!-- END COPYRIGHT -->
    
	<!-- BEGIN CORE PLUGINS -->
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>   
  </body>
</html>    