<?php $this->beginContent('//layouts/main'); ?>
 <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar nav-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->        
            <?php $this->widget('LeftMenu');?> 
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- BEGIN PAGE -->
    <div class="page-content">			
        <!-- BEGIN PAGE CONTAINER-->
        <div class="container-fluid">				
            <!-- BEGIN PAGE CONTENT-->
            <div class="row-fluid">
                <?php echo $content; ?>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE CONTAINER--> 
    </div>
    <!-- END PAGE -->      
<?php $this->endContent(); ?>