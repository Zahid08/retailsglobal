<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo isset($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
    <?php if(isset($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
    <?php if(isset($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>
	
    <meta name="subject" content="Unlocklive" />
    <meta name="language" content="English" />
    <meta name="copyright" content="© www.unlocklive.com/">
    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Googlebot" content="all" />
    <meta name="language" content="en" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="Rating" content="General" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="coverage" content="Worldwide" />
    <meta name = "Server" content = "New" />
    <meta name="expires" content="0" />
    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
    <meta name="reply-to" content="info@unlocklive.com" />
    
    <?php // configure theme skin path
		$themeSkinPath = Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];
    ?>
   
	<!-- END GLOBAL MANDATORY STYLES -->	
    <?php 
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
	if(!empty($companyModel)) : ?>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>
    <?php endif; ?>
    
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/plugins/font-awesome/css/font-awesome-4.2.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $themeSkinPath; ?>/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<!-- END GLOBAL MANDATORY STYLES -->
    
	<!-- BEGIN PAGE LEVEL STYLES --> 
	<link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $themeSkinPath; ?>/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap/css/bootstrap-combobox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $themeSkinPath; ?>/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
  	<!-- END PAGE LEVEL STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<?php // $this->getUniqueId()=="report" ||
	if(Yii::app()->urlManager->parseUrl(Yii::app()->request)=="salesInvoice/create" || Yii::app()->urlManager->parseUrl(Yii::app()->request)=="items/admin") $bodyclass="page-header-fixed page-sidebar-closed";
	else $bodyclass = "page-header-fixed";
?>
<body class="<?php echo $bodyclass;?>"> <!-- oncontextmenu="return false" onselectstart="return false" ondragstart="return false"-->
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<!-- BEGIN TOP NAVIGATION BAR -->
		<div class="navbar-inner">
			<div class="container-fluid top_part_content">
            	<div class="page-logo">
                <?php 
					$companyModelTop = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
					if(!empty($companyModelTop)) : 
						echo '<span class="pos_company">'.$companyModelTop->name.'</span>'; 
                        echo '<span class="branch-name">&nbsp;-&nbsp;'.Yii::app()->session['branchName'].'</span>'; 
					endif; 
				?>			
    			</div>
                <div class="top-menu">
                    <!-- BEGIN TOP NAVIGATION MENU -->  
                    <div id="topMenuWidget"><?php $this->widget('TopMenu');?></div>
                    <!-- END TOP NAVIGATION MENU --> 
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                        <img src="<?php echo $themeSkinPath; ?>/img/menu-toggler.png" alt="" />
                    </a>          
                    <!-- END RESPONSIVE MENU TOGGLER -->
                </div>
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
    
	<!-- BEGIN CONTAINER -->   
	<div class="page-container">
		<?php echo $content; ?>   
	</div>
	<!-- END CONTAINER -->
    
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<div class="footer-inner">
			2014 &copy; Unlocklive.
		</div>
		<div class="footer-tools">
			<span class="go-top">
			<i class="icon-angle-up"></i>
			</span>
		</div>
	</div>
	<!-- END FOOTER -->

	<!-- BEGIN CORE PLUGINS -->     
	<?php 
		Yii::app()->clientScript->registerCoreScript('jquery'); 
		Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
	?>
	<script src="<?php echo $themeSkinPath; ?>/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
	<!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
	<script src="<?php echo $themeSkinPath; ?>/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>      
	<script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap/js/bootstrap-combobox.js" type="text/javascript"></script>
    
	<!--[if lt IE 9]>
	<script src="<?php echo $themeSkinPath; ?>/plugins/excanvas.min.js"></script>
	<script src="<?php echo $themeSkinPath; ?>/plugins/respond.min.js"></script>  
	<![endif]-->   
	
	<!-- BEGIN PAGE LEVEL PLUGINS -->
	<script src="<?php echo $themeSkinPath; ?>/plugins/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="<?php echo $themeSkinPath; ?>/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
	
	<script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap-daterangepicker/date.js" type="text/javascript"></script>
	<script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>     
	<script src="<?php echo $themeSkinPath; ?>/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
    
    <script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript" ></script>
	<script src="<?php echo $themeSkinPath; ?>/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript" ></script>
    
    
    <?php /*?><script src="<?php echo $themeSkinPath; ?>/scripts/metronic.js" type="text/javascript"></script>
    <script src="<?php echo $themeSkinPath; ?>/scripts/layout.js" type="text/javascript"></script>
	<script src="<?php echo $themeSkinPath; ?>/scripts/quick-sidebar.js" type="text/javascript">	
    </script> <?php */?> 
    
	<script src="<?php echo $themeSkinPath; ?>/scripts/app.js" type="text/javascript"></script>
	<script src="<?php echo $themeSkinPath; ?>/scripts/index.js" type="text/javascript"></script>   
    <script src="<?php echo $themeSkinPath; ?>/scripts/ui-modals.js"></script>  
    <script src="<?php echo $themeSkinPath; ?>/scripts/custom_js.js"></script>  
    <script src="<?php echo $themeSkinPath; ?>/scripts/slimscroll.min.js"></script>  
    <script>
        jQuery(function(){
            jQuery('#slimscroller').slimScroll({
                height: '250px'
            });
        });
    </script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>