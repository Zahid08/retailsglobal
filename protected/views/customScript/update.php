<?php
/* @var $this CustomscriptController */
/* @var $model Customscript */

$this->breadcrumbs=array(
	'Customscripts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Customscript', 'url'=>array('index')),
	array('label'=>'Create Customscript', 'url'=>array('create')),
	array('label'=>'View Customscript', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Customscript', 'url'=>array('admin')),
);
?>

<h1>Update Customscript <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>