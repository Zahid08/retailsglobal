<?php
/* @var $this CustomscriptController */
/* @var $model Customscript */

$this->breadcrumbs=array(
	'Customscripts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Customscript', 'url'=>array('index')),
	array('label'=>'Manage Customscript', 'url'=>array('admin')),
);
?>

<h1>Create Customscript</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>