<?php
/* @var $this CustomscriptController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customscripts',
);

$this->menu=array(
	array('label'=>'Create Customscript', 'url'=>array('create')),
	array('label'=>'Manage Customscript', 'url'=>array('admin')),
);
?>

<h1>Customscripts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
