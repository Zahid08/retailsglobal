<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Payment Method<i class="icon-angle-right"></i></li>
    <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Method</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payment-method-form',
	'enableAjaxValidation'=>true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Method</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255)); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'apiShortName'); ?>
                <?php echo $form->dropDownList($model,'apiShortName',UsefulFunction::getApiShostName(),array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'apiShortName'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'image'); ?>
                <?php echo $form->fileField($model,'image',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'image'); ?>
                <?php
                 echo CHtml::image($model->image,"",array("width"=>50))
                ?>

            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php //echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                <?php
                $this->widget('application.extensions.eckeditor.ECKEditor', array(
                    'model'=>$model,
                    'attribute'=>'description',
                    'config' => array(
                        /*'toolbar'=>array(
                            array('Source', '-', 'NewPage', 'Preview', '-','Bold', 'Italic','Underline','Styles','Paragraph','Strike' ),
                            array('-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                            array('-','Link', 'Unlink', 'Anchor' ) , //
                        ),
                        'language'=>'en',
                        'filebrowserImageBrowseUrl'=>'kcfinder/browse.php?type=files',
                        'filebrowserImageUploadUrl'=>Yii::app()->createAbsoluteUrl('site/redactorimageUp'),*/
                    ),
                ));
                ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   