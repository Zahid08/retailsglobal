<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Payment Method<i class="icon-angle-right"></i></li>
    <li>Details Payment Method # <?php echo $model->title; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'title',
		'shortKey',
        array(
            'name'=>'description',
            'value'=>strip_tags(preg_replace("/&#?[a-z0-9]+;/i","",$model->description)),
        ),
        array(
            'name' => 'image',
            'type' => 'raw',
            'value' => CHtml::image($model->image,"",array("width"=>50)),
        ),
		'isApi',
        array(
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
        array(
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
        array(
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
	    ),
)); ?>
</div>
