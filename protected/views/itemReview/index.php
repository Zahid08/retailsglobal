<?php
/* @var $this ItemReviewController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Item Reviews',
);

$this->menu=array(
	array('label'=>'Create ItemReview', 'url'=>array('create')),
	array('label'=>'Manage ItemReview', 'url'=>array('admin')),
);
?>

<h1>Item Reviews</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
