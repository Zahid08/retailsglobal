<?php
/* @var $this ItemReviewController */
/* @var $model ItemReview */

$this->breadcrumbs=array(
	'Item Reviews'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ItemReview', 'url'=>array('index')),
	array('label'=>'Create ItemReview', 'url'=>array('create')),
	array('label'=>'View ItemReview', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ItemReview', 'url'=>array('admin')),
);
?>

<h1>Update ItemReview <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>