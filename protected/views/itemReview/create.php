<?php
/* @var $this ItemReviewController */
/* @var $model ItemReview */

$this->breadcrumbs=array(
	'Item Reviews'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ItemReview', 'url'=>array('index')),
	array('label'=>'Manage ItemReview', 'url'=>array('admin')),
);
?>

<h1>Create ItemReview</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>