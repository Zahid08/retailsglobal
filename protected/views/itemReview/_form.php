<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce <i class="icon-angle-right"></i></li>
    <li>Item Review<i class="icon-angle-right"></i></li>
    <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Item Review</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'item-review-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Item Review</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">

                <div class="row">
                <?php echo $form->labelEx($model,'rating'); ?>
                <?php echo $form->textField($model,'rating',array('class'=>'m-wrap large','readonly'=>true)); ?>
                <?php echo $form->error($model,'rating'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150,'readonly'=>true)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150,'readonly'=>true)); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'comment'); ?>
                <?php
                $this->widget('application.extensions.eckeditor.ECKEditor', array(
                    'model'=>$model,
                    'attribute'=>'comment',
                    'config' => array(
                        /*'toolbar'=>array(
                            array('Source', '-', 'NewPage', 'Preview', '-','Bold', 'Italic','Underline','Styles','Paragraph','Strike' ),
                            array('-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                            array('-','Link', 'Unlink', 'Anchor' ) , //
                        ),
                        'language'=>'en',
                        'filebrowserImageBrowseUrl'=>'kcfinder/browse.php?type=files',
                        'filebrowserImageUploadUrl'=>Yii::app()->createAbsoluteUrl('site/redactorimageUp'),*/
                    ),
                ));
                ?>
                <?php echo $form->error($model,'comment'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   