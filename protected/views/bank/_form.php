<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Setting<i class="icon-angle-right"></i></li>
    <li>Bank Account</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Bank Account</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			 <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php 
                    $this->widget('CAutoComplete',array(
                         'model'=>$model,
                         'id'=>'name',
                         'attribute' => 'name',
                         //name of the html field that will be generated
                         //'name'=>'poId', 
                                     //replace controller/action with real ids
                         'value'=>($model->name)?$model->name:'',

                         'url'=>array('ajax/autoCompleteBankAccountsName'), 
                         'max'=>100, //specifies the max number of items to display

                                     //specifies the number of chars that must be entered 
                                     //before autocomplete initiates a lookup
                         'minChars'=>2, 
                         'delay'=>500, //number of milliseconds before lookup occurs
                         'matchCase'=>false, //match case when performing a lookup?
                         'mustMatch' =>false,
                                     //any additional html attributes that go inside of 
                                     //the input field can be defined here
                         'htmlOptions'=>array('class'=>'m-wrap span','style'=>'width:331px;','maxlength'=>20), 	

                         //'extraParams' => array('name' => 'js:function() { return $("#supplierId").val(); }'),
                         //'extraParams' => array('taskType' => 'desc'),
                         'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
                    ));
                ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'branch'); ?>
                <?php $this->widget('CAutoComplete',array(
                         'model'=>$model,
                         'id'=>'branchbranch',
                         'attribute' => 'branch',
                         //name of the html field that will be generated
                         //'name'=>'poId', 
                                     //replace controller/action with real ids
                         'value'=>($model->branch)?$model->branch:'',

                         'url'=>array('ajax/autoCompleteBankBranchName'), 
                         'max'=>100, //specifies the max number of items to display

                                     //specifies the number of chars that must be entered 
                                     //before autocomplete initiates a lookup
                         'minChars'=>2, 
                         'delay'=>500, //number of milliseconds before lookup occurs
                         'matchCase'=>false, //match case when performing a lookup?
                         'mustMatch' => false,
                                     //any additional html attributes that go inside of 
                                     //the input field can be defined here
                         'htmlOptions'=>array('class'=>'m-wrap span','style'=>'width:331px;','maxlength'=>20), 	

                         //'extraParams' => array('name' => 'js:function() { return $("#supplierId").val(); }'),
                         //'extraParams' => array('taskType' => 'desc'),
                         'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
                    ));
                ?>
                <?php echo $form->error($model,'branch'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'accountNo'); ?>
                <?php echo $form->textField($model,'accountNo',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                <?php echo $form->error($model,'accountNo'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   