<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-deposit-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="portlet box blue"><!--light-grey-->
	<div class="portlet-title">
		<div class="caption">&loz;&nbsp;<?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;SupplierDeposit</div>
		<div class="tools">
			<a href="javascript:;" class="collapse"></a>
			<a href="javascript:;" class="reload"></a>
			<a href="javascript:;" class="remove"></a>
		</div>
	</div>
	<div class="portlet-body">
		<div class="row">
            <?php echo $form->labelEx($model,'supId'); ?>
            <?php echo $form->textField($model,'supId',array('class'=>'m-wrap large','size'=>10,'maxlength'=>10)); ?>
            <?php echo $form->error($model,'supId'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'grnId'); ?>
            <?php echo $form->textField($model,'grnId',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
            <?php echo $form->error($model,'grnId'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'amount'); ?>
            <?php echo $form->textField($model,'amount',array('class'=>'m-wrap large')); ?>
            <?php echo $form->error($model,'amount'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'remarks'); ?>
            <?php echo $form->textField($model,'remarks',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250)); ?>
            <?php echo $form->error($model,'remarks'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'status'); ?>
            <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
            <?php echo $form->error($model,'status'); ?>
        </div>
            
        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
        </div>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   