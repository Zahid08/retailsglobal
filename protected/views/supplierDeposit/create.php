<?php
/* @var $this SupplierDepositController */
/* @var $model SupplierDeposit */

$this->breadcrumbs=array(
	'Supplier Deposits'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SupplierDeposit', 'url'=>array('index')),
	array('label'=>'Manage SupplierDeposit', 'url'=>array('admin')),
);
?>

<h1>Create SupplierDeposit</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>