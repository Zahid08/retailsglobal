<?php
/* @var $this SupplierDepositController */
/* @var $model SupplierDeposit */

$this->breadcrumbs=array(
	'Supplier Deposits'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SupplierDeposit', 'url'=>array('index')),
	array('label'=>'Create SupplierDeposit', 'url'=>array('create')),
	array('label'=>'View SupplierDeposit', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SupplierDeposit', 'url'=>array('admin')),
);
?>

<h1>Update SupplierDeposit <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>