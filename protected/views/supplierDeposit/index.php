<?php
/* @var $this SupplierDepositController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Supplier Deposits',
);

$this->menu=array(
	array('label'=>'Create SupplierDeposit', 'url'=>array('create')),
	array('label'=>'Manage SupplierDeposit', 'url'=>array('admin')),
);
?>

<h1>Supplier Deposits</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
