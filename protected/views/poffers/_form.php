<script type="text/javascript" language="javascript">
$(function() 
{
	// Add new item rows after last item
	var id = 0;
	$(".codeblur").blur(function() 
	{
		var inputId = $(this).attr("id");
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		$("#ajax_loaderCode"+inputId).show("slow");
		
		var input = $(this);
		var code  = input.val();
		var master = $("table.bordercolumn");				
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/productByCode/";
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: urlajax,
			data: 
			{ 
				code : code,
			},
			success: function(data) 
			{
				$("#ajax_loaderCode"+inputId).hide();
				
				if(data=="null") {}
				else if(data=="invalid") 
				{
					input.val("");
					input.focus();
					$("#ajax_errorCode"+inputId).html("Invalid Item Code !");	
				}
				else 
				{
					$("#ajax_errorCode"+inputId).html("");
					
					// create dynamic Row for current row input
					if(inputId==lastRowId)
					{
						id++;
						// Get a new row based on the prototype row
						var prot = master.find(".parentRow").clone(true);			
						prot.attr("id", id)
						prot.attr("class", "chieldRow" + id)
						
						// loader error and srl processing
						prot.find(".ajax_loaderCode").attr("id", "ajax_loaderCode"+id);
						prot.find(".ajax_errorCode").attr("id", "ajax_errorCode"+id);
						prot.find(".codeblur").attr("id",id);
						prot.find(".srlNo").html(id+1);
						prot.find(".codeblur").attr("id",id);
						
						// item processing
						prot.find(".pk").attr("name", "pk[" + id +"]"); prot.find(".pk").attr("id", "pk" + id); $("#pk"+inputId).val(inputId);
						prot.find(".proId").attr("name", "proId[" + id +"]"); prot.find(".proId").attr("id", "proId" + id); $("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						prot.find(".itemName").attr("id", "itemName" + id); $("#itemName"+inputId).html(data.itemName);
						prot.find(".sellPrice").attr("id", "sellPrice" + id); $("#sellPrice"+inputId).html(data.sellPrice); 
						prot.find(".sellPriceAmount").attr("id", "sellPriceAmount" + id); prot.find(".sellPriceAmount").attr("name", "sellPriceAmount[" + id +"]"); $("#sellPriceAmount"+inputId).val(data.sellPrice);
						
						// final binding and clone with reset values
						prot.find(".codeblur").attr("value", "");
						prot.find(".itemName").html("");
						prot.find(".sellPrice").html("0.00");
						master.find("tbody").append(prot);
						$("table.bordercolumn tr:last .codeblur").focus();
					}
					else  // last blank row
					{
						// item processing
						$("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						$("#itemName"+inputId).html(data.itemName);
						$("#sellPrice"+inputId).html(data.sellPrice); 
						$("#sellPriceAmount"+inputId).val(data.sellPrice);
					}
				}
			},
			error: function() {
				$("#ajax_errorCode"+inputId).html("Invalid Item Code !");
			}
		});
	});	
	
	// Remove items functionality
	$("table.bordercolumn img.removeRow").live("click", function() 
	{
		id--;
		$(this).parents("tr").remove();  
	});

    $('.child-sub-item').on('change', function(event){
        event.preventDefault();
        var value=$(this).val();
        if (value==''){
           $('.row.table-area-hide-show').show();
        }else {
            $('.row.table-area-hide-show').hide();
        }
    });
	// use item from search fill with item code
	$("#useItems").click(function() 
	{
		var itemName  = $("#itemId").val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemCodeByTitle/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemName : itemName,
			},
			success: function(data) 
			{
				$("table.bordercolumn tr:last .codeblur").val(data);
				$("table.bordercolumn tr:last .codeblur").focus();
				$("#itemId").val("");
			},
			error: function() {}
		});
			
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Promotional Offer
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'poffers-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Promotional offers</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
            <div class="row">
                <div class="span6">
                    <div class="span6">
                        <?php echo $form->labelEx($model,'packageId'); ?>
                        <?php echo $form->dropDownList($model,'packageId', PofferPackages::getAllPofferPackages(PofferPackages::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Package')); ?>
                        <?php echo $form->error($model,'packageId'); ?>
                    </div>
                    <div class="span6">
                        <?php $branch=''; echo CHtml::label('Branch','');?>
                        <?php echo CHtml::dropDownList('branch',$branch,Branch::getAllBranchGlobal(Branch::STATUS_ACTIVE),
                            array('class'=>'m-wrap combo combobox','prompt'=>'Select','id'=>'branch',
                            ));
                        ?>
                    </div>

                    <div class="row">              
                <div class="span6">
                    <?php echo $form->labelEx($model,'startDate'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'name'=>'Poffers[startDate]',
                        'id'=>'startDate',					   
                        'value' =>$model->startDate,  
                        'options'=>array(
                            'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                            'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                            'changeMonth' => 'true',
                            'changeYear' => 'true',
                            'showButtonPanel' => 'true',
                            'constrainInput' => 'false',
                            'duration'=>'normal',
                        ),
                        'htmlOptions'=>array(
                            'class'=>'m-wrap large',
                        ),
                    ));
                                        ?>
                    <?php echo $form->error($model,'startDate'); ?>
                </div>
                <div class="span6 fileld_right" style="padding:0;">
                    <?php echo $form->labelEx($model,'endDate'); ?>
                    <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model'=>$model,
                        'name'=>'Poffers[endDate]',
                        'id'=>'endDate',					   
                        'value' =>$model->endDate,  
                        'options'=>array(
                            'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                            'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                            'changeMonth' => 'true',
                            'changeYear' => 'true',
                            'showButtonPanel' => 'true',
                            'constrainInput' => 'false',
                            'duration'=>'normal',
                        ),
                        'htmlOptions'=>array(
                            'class'=>'m-wrap large',
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model,'endDate'); ?>
                    
                </div>
                        <div class="row">
                        <div class="span6">
                            <?php echo $form->labelEx($model,'catId'); ?>
                            <?php echo $form->dropDownList($model,'catId', Category::getAllCategory(Category::STATUS_ACTIVE), array('class'=>'m-wrap span child-sub-item','prompt'=>'Select Child-Sub Category','id'=>'catId',)); ?>
                            <?php echo $form->error($model,'catId'); ?>
                        </div>
                        </div>
            </div> 
                    
                </div>
                <div class="span6">
                    <div class="fileld_right" style="width:442px;">
                        <a class="btn btn-primary" data-toggle="modal" href="#search_info" style="margin:15px 0; font-size:12px;"><i class="icon-search"></i> Search Items</a>
                        <table class="table table-striped table-bordered table-advance table-hover">
                            <thead>
                                <tr>
                                    <th><i class="icon-bookmark"></i> Offer Number</th>
                                    <th><i class="icon-bookmark"></i> Offer Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>ON<?php echo date("Ymdhis"); echo $form->hiddenField($model,'offerNo', array('value'=>'ON'.date("Ymdhis")));?></td>
                                    <td><?php echo date("Y-m-d");?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
			
			<div id="search_info" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4>Search by item title : </h4>
				</div>
				<div class="modal-body">
					<?php $this->widget('CAutoComplete',array(
								 //'model'=>$model,
								 'id'=>'itemId',
								 //'attribute' => 'custId',
								 //name of the html field that will be generated
								 'name'=>'itemId', 
								 //replace controller/action with real ids
								 //'value'=>($model->custId)?$model->cust->name:'',
					
								 'url'=>array('ajax/autoCompleteItems'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",
								 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
							));	
						?>
						<button type="button" data-dismiss="modal" class="btn" id="useItems" style="margin-left:10px;">Use Items</button>
				</div>
			</div>
			
			<div class="row table-area-hide-show">
				<div style="height:400px; overflow:scroll;">
				<table class="table table-striped table-hover bordercolumn">
					<thead>
						<tr>
							<th>Sl. No</th>
							<th>Item Code</th>
							<th class="hidden-480">Description</th>
							<th class="hidden-480">Sell Price</th>
							<th>Remove</th>
						</tr>
					</thead>
					<tbody>
						<tr class="parentRow" id="0">
							<td><span class="srlNo">1</span></td>
							<td class="hidden-480">
								<?php 
									 echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:100px; height:15px;'));
									 echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
									 echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
								?>
								<span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
									<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
								</span>
								<div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
							</td>
							<td><span id="itemName0" class="itemName">-</span></td>
							<td class="hidden-480">
								<span id="sellPrice0" class="sellPrice">0</span>
							</td>
							<td>
								<img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
							</td>
						</tr>  
					</tbody>
				</table>
				</div>
			</div>
		</div>          
    </div>
	<div class="row buttons">
          <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   