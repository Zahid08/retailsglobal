<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings <i class="icon-angle-right"></i></li>
    <li>
        Promotional Offer
        <i class="icon-angle-right"></i>
    </li>
    <li>Manage Offer</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php CHtml::link('Add Poffers', array('poffers/create'), array('class'=>'btn light', 'style'=>'float:right;margin-left:10px;'));?>

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'poffers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		'offerNo',
		array('name'=>'packageId',
             'value'=>'$data->package?$data->package->name:""',
			 'filter'=>PofferPackages::getAllPofferPackages(PofferPackages::STATUS_ACTIVE),
		),
		array('name'=>'itemId',
			 'value'=>'$data->item?$data->item->itemCode:""',
			 'filter'=>Items::getAllItems(Items::STATUS_ACTIVE),
		),
		'startDate',
		'endDate',
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('poffers-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>