<?php
/* @var $this PoffersController */
/* @var $model Poffers */

$this->breadcrumbs=array(
	'Poffers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Poffers', 'url'=>array('index')),
	array('label'=>'Manage Poffers', 'url'=>array('admin')),
);
?>

<h1>Create Poffers</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>