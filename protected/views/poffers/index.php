<?php
/* @var $this PoffersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Poffers',
);

$this->menu=array(
	array('label'=>'Create Poffers', 'url'=>array('create')),
	array('label'=>'Manage Poffers', 'url'=>array('admin')),
);
?>

<h1>Poffers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
