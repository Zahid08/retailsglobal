<script type="text/javascript" language="javascript">
$(function() 
{
	// after grnNo inputed
	$("#grnNo").blur(function() 
	{
		$("#grn_loader").show("slow");
		var grnNo  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/grnUpdateProductsByGrn/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				grnNo : grnNo,
			},
			success: function(data) 
			{
				$("#grn_loader").hide();
				$("#grn_dynamic_container").html(data);
			},
			error: function() {}
		});
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'poffers-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="portlet box blue"><!--light-grey-->
	<div class="portlet-title">
		<div class="caption">Offers Update</div>
		<div class="tools">
			<a href="javascript:;" class="collapse"></a>
			<a href="javascript:;" class="reload"></a>
			<a href="javascript:;" class="remove"></a>
		</div>
	</div>
	<div class="portlet-body">
        <div class="row">
        	<div class="span4">
            	<?php echo $form->labelEx($model,'offerNo'); ?>
                <?php $this->widget('CAutoComplete',array(
							 'model'=>$model,
							 'id'=>'offerNo',
							 'attribute' => 'offerNo',
							 //name of the html field that will be generated
							 //'name'=>'poId', 
										 //replace controller/action with real ids
							 'value'=>($model->offerNo)?$model->offerNo:'',
				
							 'url'=>array('ajax/autoCompleteofferNo'), 
							 'max'=>100, //specifies the max number of items to display
 
										 //specifies the number of chars that must be entered 
										 //before autocomplete initiates a lookup
							 'minChars'=>2, 
							 'delay'=>500, //number of milliseconds before lookup occurs
							 'matchCase'=>false, //match case when performing a lookup?
							 'mustMatch' => true,
										 //any additional html attributes that go inside of 
										 //the input field can be defined here
							 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
							 							
							 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
							 //'extraParams' => array('taskType' => 'desc'),
							 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
							 ));
						?>
                <?php echo $form->error($model,'offerNo'); ?>
                <span id="grn_loader" style="display:none;">
                    <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                </span>
            </div>
        </div>
        
         <div id="grn_dynamic_container">   
         	 <div class="row">
                <div style="height:600px; overflow:scroll;">
                <table class="table table-striped table-hover bordercolumn">
                    <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Item Code</th>
                            <th class="hidden-480">Description</th>
                            <th class="hidden-480">Sell Price</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><span class="srlNo">1</span></td>
                            <td class="hidden-480">
                                <?php 
                                     echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:100px; height:15px;'));
                                     echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
                                     echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
                                ?>
                                <span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
                                    <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                </span>
                                <div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
                            </td>
                            <td><span id="itemName0" class="itemName">-</span></td>
                            <td class="hidden-480">
                                <span id="sellPrice0" class="sellPrice">0</span>
                            </td>
                            <td>
                                <img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        
        <div class="row buttons">
            <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
        </div>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   