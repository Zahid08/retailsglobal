<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings <i class="icon-angle-right"></i></li>
    <li>Promotional Offer <i class="icon-angle-right"></i></li>
    <li>Details Offers # <?php echo $model->package->name; ?> # <?php echo $model->item->itemCode; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'offerNo',
		'packageId',
		array(            
			'label'=>'Package',  
			'value'=>$model->package->name.'('.$model->package->discount.'%)'
		),
		array(            
			'label'=>'Item',  
			'value'=>$model->item->itemCode.'('.$model->item->itemName.')'
		),
		'startDate',
		'endDate',
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username
		),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
	),
)); ?>
</div>
