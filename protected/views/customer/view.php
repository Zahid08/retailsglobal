<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Customer <i class="icon-angle-right"></i></li>
    <li>Details Customer # <?php echo $model->title.'&nbsp;'.$model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'custId',
		'title',
		'name',
		'gender',
		'addressline',
		'city',
		'phone',
		'email',
		array(            
			'label'=>'Customer Type',  
			'value'=>$model->custType0->name,
		  ),
		//'crLimit',
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
	),
)); ?>
</div>
