<script type="text/javascript" language="javascript">
	/*jQuery(document).ready(function() 
	{   
	   // user menu
	   $("#custType").change(function()
	   {
		   if($(this).val()==2) 
		   {
			   $("#crLimit").show('slow'); 
		   }
		   else $("#crLimit").hide();
	   });
	});*/
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Customer<i class="icon-angle-right"></i></li>
	<li>Customer<i class="icon-angle-right"></i></li>
	<li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Customer</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="portlet-body"> 
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1_1" data-toggle="tab">General</a></li>
            <li><a href="#tab_1_2" data-toggle="tab">Contact details</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                	<?php if($model->isNewRecord) : ?>
                	<div class="row">
                    	<?php echo $form->labelEx($model,'custId'); ?>
						<?php echo $form->textField($model,'custId', array('id'=>'custId','class'=>'m-wrap large','readonly'=>'true', 'value'=>'CN'.date("Ymdhis"))); ?>
                        <?php echo $form->error($model,'custId'); ?> 
                    </div>
                    <?php endif; ?>
                    
                    <div class="row">
						<?php echo $form->labelEx($model,'title'); ?>
                        <?php echo $form->dropDownList($model,'title', UsefulFunction::enumItem($model,'title'), array('class'=>'m-wrap large','prompt'=>'Select Title')); ?>
                        <?php echo $form->error($model,'title'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'name'); ?>
                        <?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'gender'); ?>
                        <?php echo $form->dropDownList($model,'gender', UsefulFunction::enumItem($model,'gender'), array('class'=>'m-wrap large','prompt'=>'Select Gender')); ?>
                        <?php echo $form->error($model,'gender'); ?>
                    </div>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'custType'); ?>
                        <?php echo $form->dropDownList($model,'custType', CustomerType::getAllCustomerType(CustomerType::STATUS_ACTIVE), array('id'=>'custType','class'=>'m-wrap large','prompt'=>'Select Customer Type')); ?>
                        <?php echo $form->error($model,'custType'); ?>
                    </div>
                    
                    <?php /*?><?php 
						$display = 'none';
						if(!$model->isNewRecord) :
							if($model->custType==Customer::CUST_INSTITUTIONAL) $display = 'block';
							else {}
						endif;
					?>
                    
                    <div class="row" style="display:<?php echo $display;?>" id="crLimit">
						<?php echo $form->labelEx($model,'crLimit'); ?>
                        <?php echo $form->textField($model,'crLimit',array('class'=>'m-wrap large','size'=>10,'maxlength'=>10)); ?>
                        <?php echo $form->error($model,'crLimit'); ?>
                    </div><?php */?>
                    
                    <div class="row">
                        <?php echo $form->labelEx($model,'status'); ?>
                        <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </div>
                </div>
                <div class="tab-pane" id="tab_1_2">
                	<div class="row">
						<?php echo $form->labelEx($model,'addressline'); ?>
                        <?php echo $form->textField($model,'addressline',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'addressline'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'city'); ?>
                        <?php echo $form->textField($model,'city',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                        <?php echo $form->error($model,'city'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'phone'); ?>
                        <?php echo $form->textField($model,'phone',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155)); ?>
                        <?php echo $form->error($model,'phone'); ?>
                    </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'email'); ?>
                        <?php echo $form->textField($model,'email',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155)); ?>
                        <?php echo $form->error($model,'email'); ?>
                    </div>
                </div>                
                
        	</div><!-- tab-content -->
       <div class="row buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
		</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   