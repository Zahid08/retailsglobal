<script>
 $(function() {
    var orderId;
	// change status
	$(".changeStatus").click(function() {
		orderId = $(this).attr('id');
	});

    $("#updateStatus").click(function() {
		var orderStatus = $("#orderStatus").val();
        $.ajax({
			type: "POST",
			dataType: "json",
			beforeSend : function() {
				$("#status_loader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/updateOrderStatusById/",
			data: {
				orderId: orderId,
				orderStatus : orderStatus
			},
			success: function(data) {
				if(data=='success')
                {
                    $("#status_loader").hide();
                    $.fn.yiiGridView.update('sales-invoice-grid');
                }
			},
			error: function() {}
		});
	});

});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Sales Invoice <i class="icon-angle-right"></i></li>
    <li>Invoice List</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<span id="status_loader" style="display:none;">
    <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
</span>

<div class="form" style="overflow:scroll;">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'sales-invoice-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
        array('header'=>'Sl.',
			 'value'=>'$row+1',
	    ),
		'invNo',
         array('name'=>'custId',
			 'value'=>'!empty($data->cust)?$data->cust->name:"Guest"',
		),
		'orderDate',
		'totalQty',
		'totalPrice',
        'totalTax',
        'totalShipping',
        'totalDiscount',
        array('name'  => 'status',
              'value' => 'CHtml::link(Lookup::item("Status", $data->status), "#update_status",array("class"=>"changeStatus","id"=>$data->id,"data-toggle"=>"modal","style"=>"color:#00a8e6;"))',
              'type'  => 'raw',
              'filter'=>Lookup::items('Status'),
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view} {print} {delete}',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			//'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'buttons'=>array(
			   'print'=>array(
			   		'label' => 'Print',
					 'url' => 'CHtml::normalizeUrl(array("salesInvoice/invoicePreview/id/".rawurlencode($data->id)))',
					 'imageUrl' => Yii::app()->request->baseUrl.'/media/icons/print_small_icon.jpg',
				),
			),
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(20=>20,50=>50,100=>100,500=>500),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('sales-invoice-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>
<div id="update_status" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4>Update Status : </h4>
    </div>
    <div class="modal-body">
        <?php echo CHtml::dropDownList('status','',Lookup::items('Status'), array('id'=>'orderStatus','class'=>'m-wrap large','prompt'=>'Select Status')); ?>
        <button style="margin-left:10px;margin-bottom:10px;" type="button" data-dismiss="modal" class="btn" id="updateStatus">Update</button>
    </div>
</div>