<?php
	// print invoice
	if(!empty($invoice) && is_numeric($invoice))
		 $this->renderPartial('_printSalesReturn', array('invoice'=>$invoice,'itemArr'=>$itemArr)); 
?>
<script type="text/javascript" language="javascript">
$(function() 
{
	// after grnNo inputed
	$("#invNo").blur(function() 
	{
		$("#inv_loader").show("slow");
		$("#grandQty").html(0); 
		$("#totalWeight").html(0.0);
		$("#grandTotal").html(0);
		var invNo  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/salesReturnByInvoice/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				invNo : invNo,
			},
			success: function(data) 
			{
				$("#inv_loader").hide();
				$("#inv_dynamic_container").html(data);
			},
			error: function() {}
		});
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Sales</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sales-invoice-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Sales Return</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4" style="width:500px;">
					<?php echo $form->labelEx($model,'invNo'); ?>
					<?php $this->widget('CAutoComplete',array(
								 'model'=>$model,
								 'id'=>'invNo',
								 'attribute' => 'invNo',
								 //name of the html field that will be generated
								 //'name'=>'custId', 
								 //replace controller/action with real ids
								 'value'=>($model->custId)?$model->cust->name:'',
								 'url'=>array('ajax/autoCompleteSalesInvoice'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",
								 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
							));
					?>
					<?php echo $form->error($model,'invNo'); ?>
					<span id="inv_loader" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
				</div>
			</div>
            
            <div class="row">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th>Total Return Quantity</th>
                            <th>Total Return Weight</th>
                            <th>Total Return Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="highlight">
                                <div class="success"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandQty">0</span>
                            </td>
                            <td class="highlight">
                                <div class="info"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="totalWeight">0.0</span>
                            </td>
                            <td class="highlight">
                                <div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandTotal">0</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
			
			<div id="inv_dynamic_container">   
				 <div class="row">
					<div style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th>Sl. No</th>
								<th>Item Code</th>
								<th class="hidden-480">Description</th>
								<th class="hidden-480">Return Quantity</th>
								<th class="hidden-480">Sold Quantity</th>
								<th class="hidden-480">Sold Price</th>
                                <th class="hidden-480">Item Tax</th>
								<th class="hidden-480">Item Discount</th>
                                <th class="hidden-480">Special Discount</th>
                                <th class="hidden-480">Instant Discount</th>
								<th>Net Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#</td>
								<td>-</td>
								<td>-</td>
								<td class="hidden-480"><?php echo CHtml::textField('qty','',array('id'=>'qty', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?></td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
                                <td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td>0</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
        </div>         
    </div>
    <div class="row buttons">
		<?php echo CHtml::submitButton('Submit', array('id'=>'submitInvoice', 'class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   