<style type="text/css">
    .customer-invoice{
        font-size: 15px;
        line-height: 24px;
        font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    }
    table#body tbody tr th{
        border: 1px solid #ddd;
        padding: 10px;
        background: #353535;
        color: #fff;
    }
    table#body tbody tr td{
        border:1px solid #ddd;
        padding:5px;
    }
    table#head{
        border:1px solid #ddd;
        max-width: 800px;
        width: 100%;
    }
    table#body{
        max-width: 800px;
        width: 100%;
    }

    .invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>

<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Sales Invoice <i class="icon-angle-right"></i></li>
    <li>Print Order Invoice </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="span3 pull-right">	
     <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
        <img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
    </a>
</div>
<div class="clearfix"></div>
<style>
    .invoice-print table{
        border: 1px solid #ddd;
        max-width: 800px;
        width: 100%;
        margin: 0px auto;
    }
</style>

<div id="report_dynamic_container" class="form">
    <div class="invoice-print">
        <table width="100%" style="border:1px solid #eee; border-collapse:collapse;padding: 10px">
            <tr>
                <td>
                    <?php if(!empty($companyModel)) { ?>
                        <img src="<?php echo Yii::app()->request->baseUrl.$companyModel->logo;?>" style="" />
                    <?php } ?>
                </td>
                <td>
                    <strong>Invoice No:</strong> <?php echo $orderModel->invNo; ?> <br>
                    <strong>Date:</strong> <?php echo date("F j Y, g:i a",strtotime($orderModel->orderDate)); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <h3>Billing Information</h3>
                    <p><strong>Name: </strong><?php echo $customerModel->name;?></p>
                    <p><strong>Address: </strong><?php echo $customerModel->addressline.', '.$customerModel->city;?></p>
                    <p><strong>Phone: </strong><?php echo $customerModel->phone;?></p>
                    <p><strong>Email: </strong><?php echo $customerModel->email;?></p>
                </td>
                <td>
                    <h3>Shipping Information</h3>
                    <p><strong>Name: </strong><?php echo $customerModel->name;?></p>
                    <p><strong>Address: </strong><?php echo $customerModel->addressline.', '.$customerModel->city;?></p>
                    <p><strong>Phone: </strong><?php echo $customerModel->phone;?></p>
                    <p><strong>Email: </strong><?php echo $customerModel->email;?></p>
                </td>
            </tr>
        </table>
        <br>
        <?php if(!empty($orderDetailsModel)) : ?>
            <table width="100%" style="border:1px solid #eee; border-collapse:collapse;">
                <tbody>
                <tr style="border:1px solid #eee;">
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px;background: #353535;color: #fff;text-align: center;font-size: 16px;">
                        Item Description
                    </td>
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px;background: #353535;color: #fff;text-align: center;font-size: 16px;">
                        Image
                    </td>
                  <!--  <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
                        Color
                    </td>
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
                        Size
                    </td>-->
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px;background: #353535;color: #fff;text-align: center;font-size: 16px;">
                        Amount
                    </td>
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px;background: #353535;color: #fff;text-align: center;font-size: 16px;">
                        Quantity
                    </td>
                    <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px;background: #353535;color: #fff;text-align: center;font-size: 16px;">
                        Total Price
                    </td>
                </tr>
                <?php

                $totalItemsPriceArray = array();
                ?>
                <?php foreach($orderDetailsModel as $key=>$data) : ?>
                    <?php
                    $totalItemsPriceArray[] = $data->salesPrice*$data->qty;


                    $variation = ItemsVariation::model()->find(array('condition'=>'id=:id',
                        'params'=>array(':id'=>$data->variation_id)));
                    $small = str_replace('large','small', $variation->image);
                    //$productImage = Yii::getAlias('@backendUrl').'/'.$small;

                    $color = Attributes::model()->find(
                            array(
                                'condition'=>'id=:id AND attTypeId=:attTypeId',
                                'params'=>array(':id'=>$variation->color, ':attTypeId'=>2)
                            )
                    );
                    $size = Attributes::model()->find(
                            array(
                                'condition'=>'id=:id AND attTypeId=:attTypeId',
                                'params'=>array(':id'=>$variation->size, ':attTypeId'=>1)
                            )
                    );

                    $colorValue = $sizeValue = '';
                    if(!empty($color)){
                        $colorValue = $color->name;
                    } if(!empty($size)){
                        $sizeValue = $size->name;
                    }
                    ?>
                    <tr style="border:1px solid #eee;text-align: center">
                        <td align="center" style="border:1px solid #eee;">
                            <?php echo $data->item->itemName;?>
                            <p style="margin: 0px;"><strong>Color:</strong> <?php echo $colorValue;?></p>
                            <p><strong>Size:</strong> <?php echo $sizeValue;?></p>
                        </td>
                        </td>
                        <td align="center" style="border:1px solid #eee;text-align: center;">
                            <img src="/<?php echo $small; ?>" alt="<?php echo $data->item->itemName;?>">
                        </td>
                      <!--  <td align="center" style="border:1px solid #eee;"><?php /*echo $colorValue;*/?></td>
                        <td align="center" style="border:1px solid #eee;"><?php /*echo $sizeValue;*/?></td>-->
                        <td align="center" style="border:1px solid #eee;text-align: center;"><?php echo $data->salesPrice;?></td>
                        <td align="center" style="border:1px solid #eee;text-align: center;"><?php echo $data->qty;?></td>
                        <td align="center" style="border:1px solid #eee;text-align: center;"><?php echo Company::getCurrency().'&nbsp;'.((($data->salesPrice*$data->qty)+$data->vatPrice)-$data->discountPrice);?></td>
                    </tr>
                <?php endforeach; ?>
                <?php
                if(!empty($totalItemsPriceArray)) {
                    $totalItemsPrice = array_sum($totalItemsPriceArray);
                }else{
                    $totalItemsPrice = 0;
                }
                ?>
                </tbody>
            </table>
        <?php endif;?>

        <table width="100%" style="border:1px solid #eee;border-collapse:collapse;margin-top:10px " >
            <tbody>
            <tr>
                <td width="370px" style="border:1px solid #eee;padding:5px;">
                    Cart Subtotal :
                </td>
                <td style="border:1px solid #eee;padding:5px;text-align:right;">
                    <?php echo Company::getCurrency().'&nbsp;'.($totalItemsPrice+$orderModel->totalTax);?>
                </td>
            </tr>
            <tr>
                <td width="370px" style="border:1px solid #eee;padding:5px;">
                    Shipping :
                </td>
                <td style="border:1px solid #eee;padding:5px;text-align:right;">
                    <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalShipping);?>
                </td>
            </tr>
            <tr>
                <td width="370px" style="border:1px solid #eee;padding:5px;">
                    Total :
                </td>
                <td style="border:1px solid #eee;padding:5px;text-align:right;">
                    <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($totalItemsPrice+$orderModel->totalTax+$orderModel->totalShipping);?>
                </td>
            </tr>
            <tr>
                <td width="370px" style="border:1px solid #eee;padding:5px;">
                    Discount :
                </td>
                <td style="border:1px solid #eee;padding:5px;text-align:right;">
                    <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalDiscount);?>
                </td>
            </tr>
            <tr>
                <td width="370px" style="border:1px solid #eee;padding:5px;">
                    Grand Total :
                </td>
                <td style="border:1px solid #eee;padding:5px;text-align:right;">
                    <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($totalItemsPrice+$orderModel->totalTax+$orderModel->totalShipping)-$orderModel->totalDiscount);?>
                </td>
            </tr>
            </tbody>
        </table>
        <br><br>
        <p style="text-align: center">&copy; <?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt));?>  <a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a> - all rights reserved | <?php if(!empty($branchModel)) echo $branchModel->addressline.','.$branchModel->city.','.$branchModel->postalcode;?></p>
    </div>
    <?php /*
    <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Arial, sans-serif;">
	<tbody>
		<tr>
            <td style="padding: 10px 30px;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td width="70%" style="padding: 20px 0 30px 0; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
						   <?php if(!empty($companyModel)) : ?>
							   <img src="<?php echo Yii::app()->request->baseUrl.$companyModel->logo;?>" style="height:78px;" /> <br>
                            <h4 style="font-weight:bold;"><?php echo $companyModel->name; endif; ?></h4>
						</td>
                        <td>
                          <?php if(!empty($branchModel)) echo $branchModel->addressline.','.$branchModel->city.','.$branchModel->postalcode;?>  
                        </td>
					</tr>
					<tr>
                        <td align="center" style="font-weight: bold; font-family: Arial, sans-serif;" colspan="2">
                            <h4 style="margin:10px 20px; text-align:center; font-size: 28px; color:#555; font-weight:100;text-decoration: underline;">Invoice</h4>
						</td>
					</tr>
					<tr>
                        <td align="center" style="padding:40px 30px 40px 30px" colspan="2">
                            <table width="90%" cellspacing="0" cellpadding="0" border="0" align="center">
							   <tbody>
								   <tr>
									   <td align="center" style="font-size: 16px;padding:0px 30px 40px 0;">
                                           <strong>Invoice No:</strong> <?php echo $orderModel->invNo; ?> <br>
                                           <strong>Date:</strong> <?php echo date("F j Y, g:i a",strtotime($orderModel->orderDate)); ?>
									   </td>
								   </tr>
                                   <?php if(!empty($customerModel)){ ?>
								   <tr>
								       <td>
                                           <table width="100%">
                                               <tbody>
                                                   <tr width="100%">
                                                       <td style="padding-right:10px; width:50%;">
                                                           <table>
                                                               <tbody>
                                                                   <tr>
                                                                       <td>
                                                                           <h3 style="margin:0;">Billing Information</h3>
                                                                       </td>
                                                                   </tr>
                                                                   <tr>
                                                                       <td class="billling_shipping">
                                                                            <p><strong>Name: </strong><?php echo $customerModel->name;?></p>
                                                                            <p><strong>Address: </strong><?php echo $customerModel->addressline.', '.$customerModel->city;?></p>
                                                                            <p><strong>Phone: </strong><?php echo $customerModel->phone;?></p>
                                                                            <p><strong>Email: </strong><?php echo $customerModel->email;?></p>
                                                                       </td>
                                                                   </tr>
                                                               </tbody>
                                                           </table>
                                                       </td>
                                                       <td style="padding-left:10px; width:50%;">
                                                           <table>
                                                               <tbody>
                                                                   <tr>
                                                                       <td>
                                                                           <h3 style="margin:0;">Shipping Information</h3>
                                                                       </td>
                                                                   </tr>
                                                                   <tr>
                                                                       <td class="billling_shipping">
                                                                            <p><strong>Name: </strong><?php echo $customerModel->name;?></p>
                                                                            <p><strong>Address: </strong><?php echo $customerModel->addressline.', '.$customerModel->city;?></p>
                                                                            <p><strong>Phone: </strong><?php echo $customerModel->phone;?></p>
                                                                            <p><strong>Email: </strong><?php echo $customerModel->email;?></p>
                                                                       </td>
                                                                   </tr>
                                                               </tbody>
                                                           </table>
                                                       </td>
                                                   </tr>
                                               </tbody>
                                           </table>
								       </td>
								   </tr>
                                   <?php } ?>
								   <tr>
									   <td>
									   	   <?php if(!empty($orderDetailsModel)) : ?>
										   <table width="100%" style="border:1px solid #eee; border-collapse:collapse;">
											   <tbody>
												   <tr style="border:1px solid #eee;">
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Product
													   </td>
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Color
													   </td>
                                                       <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
                                                           Size
                                                       </td>
                                                       <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Quantity
													   </td>
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Price
													   </td>
												   </tr>
												   <?php

												        $totalItemsPriceArray = array();
												   ?>
												   <?php foreach($orderDetailsModel as $key=>$data) : ?>
												   <?php
												        $totalItemsPriceArray[] = $data->salesPrice*$data->qty;


                                                       $variation = ItemsVariation::model()->find(array('condition'=>'id=:id',
                                                           'params'=>array(':id'=>$data->variation_id)));
												   ?>
												   <tr style="border:1px solid #eee;">
													   <td align="center" style="border:1px solid #eee;">
														   <table width="100%">
															   <tbody>
																   <tr><td><?php echo $data->item->itemName;?></td></tr>
															   </tbody>
													   		</table>
													   </td>
													   <td align="center" style="border:1px solid #eee;"><?php echo $variation->color;?></td>
													   <td align="center" style="border:1px solid #eee;"><?php echo $variation->size;?></td>
													   <td align="center" style="border:1px solid #eee;"><?php echo $data->qty;?></td>
													   <td align="center" style="border:1px solid #eee;"><?php echo Company::getCurrency().'&nbsp;'.((($data->salesPrice*$data->qty)+$data->vatPrice)-$data->discountPrice);?></td>
												   </tr>
												   <?php endforeach; ?>
												   <?php
                                                   if(!empty($totalItemsPriceArray)) {
                                                       $totalItemsPrice = array_sum($totalItemsPriceArray);
                                                   }else{
                                                       $totalItemsPrice = 0;
                                                   }
												   ?>
											   </tbody>
										   </table>
										   <?php endif;?>  
										   
										   <table width="100%" style="border:1px solid #eee;border-collapse:collapse;margin-top:10px " >
											   <tbody>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Cart Subtotal :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.($totalItemsPrice+$orderModel->totalTax);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Shipping :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalShipping);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Total :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($totalItemsPrice+$orderModel->totalTax+$orderModel->totalShipping);?>
													   </td>
												   </tr>
                                                   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Discount :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalDiscount);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Grand Total :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($totalItemsPrice+$orderModel->totalTax+$orderModel->totalShipping)-$orderModel->totalDiscount);?>
													   </td>
												   </tr>
											   </tbody>
										   </table>
									   </td>
								   </tr>
							   </tbody>
						   </table>
						</td>
					</tr>
					<?php if(!empty($socialModel)) : ?>
					<tr>
                        <td align="center" bgcolor="#eaeaea" style="padding:10px 30px 10px 30px;" colspan="0">
							<?php foreach($socialModel as $socialKey=>$socialData) :
								echo '<a href="'.$socialData->url.'" title="'.$socialData->name.'" target="_blank" style="margin-right:5px;"><img alt="'.$socialData->name.'" src="'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.$socialData->icon.'" height="30"></a>';
				 			endforeach; ?>
						</td>
					</tr>
					<?php endif;?>
					<tr>
                        <td align="center" style="font-size:9px;padding:10px; text-align:center;" colspan="0">
							<p>&copy; <?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt));?>  <a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a> - all rights reserved | <?php if(!empty($branchModel)) echo $branchModel->addressline.','.$branchModel->city.','.$branchModel->postalcode;?></p>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
	</table>   */?>
</div>
