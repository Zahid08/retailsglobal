
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Sales</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="row">
	<div class="col-md-6" style="width: 50%;margin-left: 30px;">
		<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sales-invoice-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) : echo $msg; else : ?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Print Sales Invoice</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<?php if(!empty($invoice) && is_numeric($invoice) && Branch::getInvoiceLayoutValue(Yii::app()->session['branchId'])==Branch::IS_FULL_INVOICE_LAYOUT) : ?>
				<div class="row">
					<ol class="breadcrumb">
						<li><?php echo CHtml::link('<i class="fa fa-print"></i> Customer Copy',array('salesInvoice/printInvoice','invoice'=>$invoice,'type'=>'invoice'),array('class'=>'btn btn-outline btn-primary'));?></li>
						<li><?php echo CHtml::link('<i class="fa fa-print"></i> Challan Copy',array('salesInvoice/printInvoice','invoice'=>$invoice,'type'=>'challan'),array('class'=>'btn btn-outline btn-primary'));?></li>
					</ol>
				</div>
			<?php else : ?>
			<div class="row">
				<div class="span4" style="width:500px;">
					<?php echo $form->labelEx($model,'invNo'); ?>
					<?php $this->widget('CAutoComplete',array(
								 'model'=>$model,
								 'id'=>'invNo',
								 'attribute' => 'invNo',
								 //name of the html field that will be generated
								 //'name'=>'custId', 
								 //replace controller/action with real ids
								 'value'=>($model->custId)?$model->cust->name:'',
					
								 'url'=>array('ajax/autoCompleteSalesInvoice'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",
								 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
							));
					?>
					<?php echo $form->error($model,'invNo'); ?>
				</div>
			</div>
			<?php endif;?>
        </div>          
    </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Print Preview', array('id'=>'submitInvoice', 'class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form --> 
	</div>
	<div class="col-md-6" style="width: 54%;margin-left: 30px;float: right;margin-top: -265px;">
		<?php
			// print invoice
			if(!empty($invoice) && is_numeric($invoice))
			{
				if(Branch::getInvoiceLayoutValue(Yii::app()->session['branchId'])==Branch::IS_FULL_INVOICE_LAYOUT)
		        {
		            if($type=='challan') $this->renderPartial('_printInvoiceChalan', array('invoice'=>$invoice));
		            else $this->renderPartial('_printInvoiceFull', array('invoice'=>$invoice));
		        }
				else $this->renderPartial('_printInvoice', array('invoice'=>$invoice));
			}
		?>	
</div>
</div>  
