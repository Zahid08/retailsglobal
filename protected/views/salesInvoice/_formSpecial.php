<?php
    /* print invoice
    if(!empty($invoice) && is_numeric($invoice))
    {
        if(Branch::getInvoiceLayoutValue(Yii::app()->session['branchId'])==Branch::IS_FULL_INVOICE_LAYOUT)
        {
            if($type=='challan') $this->renderPartial('_printInvoiceChalan', array('invoice'=>$invoice));
            else $this->renderPartial('_printInvoiceFull', array('invoice'=>$invoice));
        }
        else $this->renderPartial('_printInvoice', array('invoice'=>$invoice));
    }*/
?>
<script type="text/javascript" language="javascript">
$(function() 
{
    /******************************************* FORMS INITIALS STARTS  **************************************************/
    /*********************************************************************************************************************/
    
	// focus to first item 
	$(".codeblur").focus();
	
	// after customer inputed on keyup

	$("#custId").keyup(function()
	{
		$("#custInfo_loader").show("slow");
		var custId  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/getCustomerInfo/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{
				type : 'special',
				custId : custId,
			},
			success: function(data) 
			{
				$("#custInfo_loader").hide();
                $("#cust_dynamic_container").show("slow");
				$("#cust_dynamic_container").html(data);
				if($("#institutionalCust").val()==2) $("#showCustBalancePanel").show("slow");
			},
			error: function() {}
		});
	});
    
    // after customer inputed on blur
	$("#custId").blur(function() 
	{
		$("#custInfo_loader").show("slow");
		var custId  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/getCustomerInfo/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{
                type : 'special',
				custId : custId,
			},
			success: function(data) 
			{
				$("#custInfo_loader").hide();
                $("#cust_dynamic_container").show("slow");
				$("#cust_dynamic_container").html(data);
				if($("#institutionalCust").val()==2) $("#showCustBalancePanel").show("slow");
			},
			error: function() {}
		});
	});

	var isBarcode = 0;
	$('.codeblur').on({
		keypress: function() { typed_into = true; },
		change: function() {
			if(typed_into) isBarcode = 1;
		}
	});
	
	// Add new item rows after last item
	var id = 0;
	$(".codeblur").blur(function() 
	{
		var inputId = $(this).attr("id");
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		var input = $(this);
    	var code  = input.val();
		var weight = 0.0;
		
		// finding item codes by weighted/not
		var startTwoDigit = parseInt(code.substring(0,2));
		if(startTwoDigit==99)
		{
			// weight calculations - 3rd to next 5 digit
			var weightKg = code.substring(7,9);	
			var weightGm = code.substring(9,12);	
			code = code.substring(2,7);	
			weight = parseFloat(weightKg+"."+weightGm);
		}

		var master = $("table.bordercolumn");				
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/productByCode/";
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: urlajax,
			data: 
			{ 
				code : code,
				weight:weight,
			},
			success: function(data) 
			{	
				if(data=="null") {}
				else if(data=="invalid") 
				{
					input.val("");
					input.focus();
					$("#ajax_errorCode"+inputId).html("Invalid Item Code !");	
				}
				else 
				{
					$("#ajax_errorCode"+inputId).html("");
					
					// quantity operations
					if(data.isWeighted=="yes") {
						var qtyId = "qtywt"; var qtyPlaceholder = "0.0";
						var type = 2;
					}
					else {
						var qtyId = "qty"; var qtyPlaceholder = "0";
						var type = 1;	
					}	
			
					// create dynamic Row for current row input / current row = last Row
					if(inputId==lastRowId)
					{
						id = parseInt(lastRowId)+1;
						// Get a new row based on the prototype row
						var prot = master.find(".parentRow").clone(true);			
						prot.attr("id", id)
						prot.attr("class", "chieldRow" + id)
						
						// loader error and srl processing
						prot.find(".ajax_loaderCode").attr("id", "ajax_loaderCode"+id);
						prot.find(".ajax_errorCode").attr("id", "ajax_errorCode"+id);
						prot.find(".codeblur").attr("id",id);
						prot.find(".srlNo").html(id+1);
						prot.find(".codeblur").attr("id",id);
						
						// item processing
						prot.find(".pk").attr("name", "pk[" + id +"]"); prot.find(".pk").attr("id", "pk" + id); $("#pk"+inputId).val(inputId);
						prot.find(".proId").attr("name", "proId[" + id +"]"); prot.find(".proId").attr("id", "proId" + id); 
						$("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						prot.find(".itemName").attr("id", "itemName" + id); $("#itemName"+inputId).html(data.itemName);
						prot.find(".sellPrice").attr("id", "sellPrice" + id); $("#sellPrice"+inputId).html(data.sellPrice); 
						prot.find(".sellPriceAmount").attr("id", "sellPriceAmount" + id); 
						prot.find(".sellPriceAmount").attr("name", "sellPriceAmount[" + id +"]"); $("#sellPriceAmount"+inputId).val(data.sellPrice);
						prot.find(".costPriceAmount").attr("id", "costPriceAmount" + id); 
						prot.find(".costPriceAmount").attr("name", "costPriceAmount[" + id +"]"); $("#costPriceAmount"+inputId).val(data.costPrice);
						
						// item quantity next row		
						prot.find(".qtyblur").attr("name", "qty[" + id +"]"); 
						prot.find(".qtyblur").attr("id", "qty" + id); 
						prot.find(".qtyblur").attr("placeholder", 0); 
						input.val(data.itemCode); 
						
						// quantity current weighted or not
						$("#qty"+inputId).val(""); $("#qty"+inputId).attr("placeholder", qtyPlaceholder); 
						$("#qty"+inputId).attr("id", qtyId + inputId); 
						prot.find(".typeblur").attr("id", "type" + id); prot.find(".typeblur").attr("name", "type[" + id +"]"); $("#type"+inputId).val(type); 
						/*if(isBarcode==1 && data.isWeighted=="no") $("#"+qtyId + inputId).val(1);
						else $("#"+qtyId + inputId).val(data.weight);*/
						
						// qty * sellprice calc
						if(isBarcode==1 && data.isWeighted=="no") var netPriceIni = parseFloat(data.sellPrice); 
						else var netPriceIni = parseFloat(data.weight);
						
						// discount will process later
						prot.find(".discount").attr("id", "discount" + id); $("#discount"+inputId).html(data.discountPercent); 
						prot.find(".discountPercent").attr("id", "discountPercent" + id); 
						prot.find(".discountPercent").attr("name", "discountPercent[" + id +"]"); $("#discountPercent"+inputId).val(data.discountPercent);
						prot.find(".discountTotal").attr("id", "discountTotal" + id);
						prot.find(".discountAmount").attr("id", "discountAmount" + id); 
						prot.find(".discountAmount").attr("name", "discountAmount[" + id +"]");
						var disPriceIni = (netPriceIni*parseFloat(data.discountPercent))/100;
						$("#discountTotal"+inputId).html(disPriceIni.toFixed(2)); $("#discountAmount"+inputId).val(disPriceIni.toFixed(2));
						
						// tax process 
						prot.find(".tax").attr("id", "tax" + id); $("#tax"+inputId).html(data.taxPercent); 
						prot.find(".taxTotal").attr("id", "taxTotal" + id);
						prot.find(".taxAmount").attr("id", "taxAmount" + id); 
						prot.find(".taxAmount").attr("name", "taxAmount[" + id +"]");
						var taxPriceIni = (netPriceIni*parseFloat(data.taxPercent))/100;
						$("#taxTotal"+inputId).html(taxPriceIni.toFixed(2)); $("#taxAmount"+inputId).val(taxPriceIni.toFixed(2));
						
						// net amount set
						prot.find(".netTotal").attr("id", "netTotal" + id); 
						var netTotalIni = (netPriceIni + taxPriceIni) - disPriceIni;
						$("#netTotal"+inputId).html(netTotalIni.toFixed(2));
						
						// sum calculations
						var sumQty = 0;
						var sumWeight = 0;
						var sumPrice = 0;
						var sumDisount = 0;
						var sumTax = 0;
						
						for(var i=0;i<=lastRowId;i++)
						{
							var pk = $("#pk"+i).val();
							if(i==pk) 
							{
								if($("#qty"+i).val()=="") var sumQtyId = 0;
								else if(typeof($("#qty"+i).val())==="undefined") {
									var sumQtyId = 0;
									var sumQtyWtId = $("#qtywt"+i).val();
								}
								else var sumQtyId = $("#qty"+i).val();
								
								if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
								else if(typeof($("#qtywt"+i).val())==="undefined") {
									var sumQtyId = $("#qty"+i).val();
									var sumQtyWtId = 0;
								}
								else var sumQtyWtId = $("#qtywt"+i).val();
							
								if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
								if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
								var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
								if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
								
								sumQty+=parseFloat(sumQtyId); 
								sumWeight+=parseFloat(sumQtyWtId);
								sumPrice+=totalQtyId*parseFloat($("#sellPriceAmount"+i).val());
								
								sumDisount+=parseFloat($("#discountAmount"+i).val());
								sumTax+=parseFloat($("#taxAmount"+i).val());
							}
						}
						// value placement/update
						$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
						$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
						$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
						$("#totalTaxAmnt").html(sumTax.toFixed(2)); $("#totalTax").val(sumTax.toFixed(2)); $("#spDiscount").val(sumTax.toFixed(2));
						
						var subGrandAmnt = sumPrice+sumTax;
						$("#subGrandAmount").html(subGrandAmnt.toFixed(2));
						$("#grandDiscount").html(sumDisount.toFixed(2)); $("#totalDiscount").val(sumDisount.toFixed(2));
						var grandPrice = subGrandAmnt - sumDisount;
						var duePrice = sumPrice - sumDisount;
						$("#grandTotal").html(grandPrice.toFixed(2)); 
                        
                        // special discount settings
                        <?php if(Branch::getSpecialDiscountValue(Yii::app()->session['branchId'])==Branch::IS_SPECIAL_DISCOUNT) : ?>
                                 $("#totalDueAmount").val(duePrice.toFixed(2));
                        <?php else : ?> $("#totalDueAmount").val(grandPrice.toFixed(2)); <?php endif;?>
						
						// final binding and clone with reset values
						prot.find(".codeblur").attr("value", "");
						prot.find(".itemName").html("");
						prot.find(".qtyblur").val("0");
						prot.find(".sellPrice").html("0.00");
						prot.find(".discount").html("0.00");
						prot.find(".discountTotal").html("0.00");
						prot.find(".tax").html("0.00");
						prot.find(".taxTotal").html("0.00");
						prot.find(".netTotal").html("0.00");
						master.find("tbody").append(prot);
						// if(data.isWeighted=="no") $("table.bordercolumn tr:last .codeblur").focus();
					}
					else  // when we changes previous inputted item code
					{
						// item processing
						$("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						$("#itemName"+inputId).html(data.itemName);
						$("#sellPrice"+inputId).html(data.sellPrice); 
						$("#sellPriceAmount"+inputId).val(data.sellPrice);
						$("#costPriceAmount"+inputId).val(data.costPrice);
									
						// quantity current weighted or not
						var qtyCurrentId = ( $("#qty"+inputId).attr("id") || $("#qtywt"+inputId).attr("id") );
						$("#"+qtyCurrentId).val(""); $("#"+qtyCurrentId).attr("placeholder", qtyPlaceholder); 
						$("#"+qtyCurrentId).attr("id", qtyId + inputId); 
						$("#type"+inputId).val(type);

						// if(isBarcode==1 && data.isWeighted=="no") $("#"+qtyId + inputId).val(1);
						// else $("#"+qtyId + inputId).val(data.weight);

						// sell price
						var qty   = $("#qty"+inputId).val(); if(qty=="") qty = 0;
						var sellPr = $("#sellPrice"+inputId).html();
						var netPrice = qty*parseFloat(sellPr);
						
						// discount later 
						$("#discount"+inputId).html(data.discountPercent); 
						$("#discountPercent"+inputId).val(data.discountPercent);
						var disPr = $("#discount"+inputId).html();
						var disPrice = (netPrice*parseFloat(disPr))/100;
						$("#discountTotal"+inputId).html(disPrice.toFixed(2)); $("#discountAmount"+inputId).val(disPrice.toFixed(2));
				
						// tax & total
						$("#tax"+inputId).html(data.taxPercent); 
						var txPr = $("#tax"+inputId).html();
						var taxPrice = (netPrice*parseFloat(txPr))/100;
						$("#taxTotal"+inputId).html(taxPrice.toFixed(2)); $("#taxAmount"+inputId).val(taxPrice.toFixed(2));
						var netTotal = (netPrice + taxPrice) - disPrice;
						$("#netTotal"+inputId).html(netTotal.toFixed(2));
						
						// sum calculations
						var sumQty = 0;
						var sumWeight = 0;
						var sumPrice = 0;
						var sumDisount = 0;
						var sumTax = 0;
						
						for(var i=0;i<=lastRowId;i++)
						{
							var pk = $("#pk"+i).val();
							if(i==pk) 
							{
								if($("#qty"+i).val()=="") var sumQtyId = 0;
								else if(typeof($("#qty"+i).val())==="undefined") {
									var sumQtyId = 0;
									var sumQtyWtId = $("#qtywt"+i).val();
								}
								else var sumQtyId = $("#qty"+i).val();
								
								if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
								else if(typeof($("#qtywt"+i).val())==="undefined") {
									var sumQtyId = $("#qty"+i).val();
									var sumQtyWtId = 0;
								}
								else var sumQtyWtId = $("#qtywt"+i).val();
							
								if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
								if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
								var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
								if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
								
								sumQty+=parseFloat(sumQtyId); 
								sumWeight+=parseFloat(sumQtyWtId);
								sumPrice+=totalQtyId*parseFloat($("#sellPriceAmount"+i).val());
								
								sumDisount+=parseFloat($("#discountAmount"+i).val());
								sumTax+=parseFloat($("#taxAmount"+i).val());
							}
						}
						// value placement/update
						$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
						$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
						$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
						$("#totalTaxAmnt").html(sumTax.toFixed(2)); $("#totalTax").val(sumTax.toFixed(2)); $("#spDiscount").val(sumTax.toFixed(2));
						
						var subGrandAmnt = sumPrice+sumTax;
						$("#subGrandAmount").html(subGrandAmnt.toFixed(2));
						$("#grandDiscount").html(sumDisount.toFixed(2)); $("#totalDiscount").val(sumDisount.toFixed(2));
						var grandPrice = subGrandAmnt - sumDisount;
						var duePrice = sumPrice - sumDisount;
						$("#grandTotal").html(grandPrice.toFixed(2)); 
                        
                        // special discount settings
                        <?php if(Branch::getSpecialDiscountValue(Yii::app()->session['branchId'])==Branch::IS_SPECIAL_DISCOUNT) : ?>
                                 $("#totalDueAmount").val(duePrice.toFixed(2));
                        <?php else : ?> $("#totalDueAmount").val(grandPrice.toFixed(2)); <?php endif;?>
					}
				}
			},
			error: function() {
				$("#ajax_errorCode"+inputId).html("Invalid Item Code !");
			}
		});
	});
    
    // Remove items functionality
	$("table.bordercolumn img.removeRow").live("click", function() 
	{
		var parentRow = $(this).parents("tr").attr('id');
		if(parentRow>0) // not parentRow
		{
			$(this).parents("tr").remove();  
			// sum calculations
			var sumQty = 0;
			var sumWeight = 0;
			var sumPrice = 0;
			var sumDisount = 0;
			var sumTax = 0;
			var lastRowId = $('table.bordercolumn tr:last').attr('id');
			id = lastRowId;
			
			for(var i=0;i<=lastRowId;i++)
			{
				var pk = $("#pk"+i).val();
				if(i==pk) 
				{
					if($("#qty"+i).val()=="") var sumQtyId = 0;
					else if(typeof($("#qty"+i).val())==="undefined") {
						var sumQtyId = 0;
						var sumQtyWtId = $("#qtywt"+i).val();
					}
					else var sumQtyId = $("#qty"+i).val();
					
					if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
					else if(typeof($("#qtywt"+i).val())==="undefined") {
						var sumQtyId = $("#qty"+i).val();
						var sumQtyWtId = 0;
					}
					else var sumQtyWtId = $("#qtywt"+i).val();
				
					if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
					if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
					var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
					if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
					
					sumQty+=parseFloat(sumQtyId); 
					sumWeight+=parseFloat(sumQtyWtId);
					sumPrice+=totalQtyId*parseFloat($("#sellPriceAmount"+i).val());
					
					sumDisount+=parseFloat($("#discountAmount"+i).val());
					sumTax+=parseFloat($("#taxAmount"+i).val());
				}
			}
			
			// value placement/update
			$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
			$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
			$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
			$("#totalTaxAmnt").html(sumTax.toFixed(2)); $("#totalTax").val(sumTax.toFixed(2)); $("#spDiscount").val(sumTax.toFixed(2));
			
			var subGrandAmnt = sumPrice+sumTax;
			$("#subGrandAmount").html(subGrandAmnt.toFixed(2));
			$("#grandDiscount").html(sumDisount.toFixed(2)); $("#totalDiscount").val(sumDisount.toFixed(2));
			var grandPrice = subGrandAmnt - sumDisount;
			var duePrice = sumPrice - sumDisount;
			$("#grandTotal").html(grandPrice.toFixed(2)); 
            
            // special discount settings
            <?php if(Branch::getSpecialDiscountValue(Yii::app()->session['branchId'])==Branch::IS_SPECIAL_DISCOUNT) : ?>
                     $("#totalDueAmount").val(duePrice.toFixed(2));
            <?php else : ?> $("#totalDueAmount").val(grandPrice.toFixed(2)); <?php endif;?>
            
			$("table.bordercolumn tr:last .codeblur").focus();
		}
		else alert("First row can,t be removed !");	
	});
	
	// use item from search fill with item code
	$("#useItems").click(function() 
	{
		var itemName  = $("#itemId").val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemCodeByTitle/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemName : itemName,
			},
			success: function(data) 
			{
				$("table.bordercolumn tr:last .codeblur").val(data);
				$("table.bordercolumn tr:last .codeblur").focus();
				$("#itemId").val("");
			},
			error: function() {}
		});
			
	});
	
	// go to cash after press [+] key = 43 key
	$(document).on('keypress', function(ev) 
	{
		var keyCode = window.event ? ev.keyCode : ev.which;
		if(keyCode == 43) $("#cashPaid").focus();	
	});
    
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
    
    /******************************************* FORMS INITIALS ENDS  ****************************************************/
    /*********************************************************************************************************************/
    
    /******************************************* ON KEYUP EVENT STARTS  **************************************************/
    /*********************************************************************************************************************/
    
    // quantity operations on keyup	
	$('.qtyblur').keyup(function() 
	{
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		var sumDisount = 0;
		var sumTax = 0;
		
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		// sell price
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val(); if(qty=="") qty = 0;
		var sellPr = $("#sellPrice"+qtyId).html();
		var netPrice = qty*parseFloat(sellPr);
        
        // cost price fifo order by quantity
        var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemFifoCostPrice/";
		$.ajax({
			type: "POST",
            dataType: "json",
			url: urlajax,
			data: 
			{ 
                salesQty : qty,
				itemId : $("#proId"+qtyId).val(),
			},
			success: function(costPrice) 
			{
				$("#costPriceAmount"+qtyId).val(costPrice);
			},
			error: function() {}
		});
		
		// discount later 
		var disPr = $("#discount"+qtyId).html();
		var disPrice = (netPrice*parseFloat(disPr))/100;
		$("#discountTotal"+qtyId).html(disPrice.toFixed(2)); $("#discountAmount"+qtyId).val(disPrice.toFixed(2));

		// tax 
		var txPr = $("#tax"+qtyId).html();
		var taxPrice = (netPrice*parseFloat(txPr))/100;
		$("#taxTotal"+qtyId).html(taxPrice.toFixed(2)); $("#taxAmount"+qtyId).val(taxPrice.toFixed(2));
		
		var netTotal = (netPrice + taxPrice) - disPrice;
		$("#netTotal"+qtyId).html(netTotal.toFixed(2));
		
		// sum calculations
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		for(var i=0;i<=lastRowId;i++)
		{
			var pk = $("#pk"+i).val();
			if(i==pk) 
			{
				if($("#qty"+i).val()=="") var sumQtyId = 0;
				else if(typeof($("#qty"+i).val())==="undefined") {
					var sumQtyId = 0;
					var sumQtyWtId = $("#qtywt"+i).val();
				}
				else var sumQtyId = $("#qty"+i).val();
				
				if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
				else if(typeof($("#qtywt"+i).val())==="undefined") {
					var sumQtyId = $("#qty"+i).val();
					var sumQtyWtId = 0;
				}
				else var sumQtyWtId = $("#qtywt"+i).val();
	
				if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
				if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
				var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
				if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
				
				sumQty+=parseFloat(sumQtyId); 
				sumWeight+=parseFloat(sumQtyWtId);
				sumPrice+=totalQtyId*parseFloat($("#sellPriceAmount"+i).val());
				
				sumDisount+=parseFloat($("#discountAmount"+i).val());
				sumTax+=parseFloat($("#taxAmount"+i).val());
			}
		}
		
		// value placement/update
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
		$("#totalTaxAmnt").html(sumTax.toFixed(2)); $("#totalTax").val(sumTax.toFixed(2)); $("#spDiscount").val(sumTax.toFixed(2));
		
		var subGrandAmnt = sumPrice+sumTax;
		$("#subGrandAmount").html(subGrandAmnt.toFixed(2));
		$("#grandDiscount").html(sumDisount.toFixed(2)); $("#totalDiscount").val(sumDisount.toFixed(2));
		var grandPrice = subGrandAmnt - sumDisount;
		var duePrice = sumPrice - sumDisount;
		$("#grandTotal").html(grandPrice.toFixed(2));
        
        // special discount settings
        <?php if(Branch::getSpecialDiscountValue(Yii::app()->session['branchId'])==Branch::IS_SPECIAL_DISCOUNT) : ?>
                 $("#totalDueAmount").val(duePrice.toFixed(2));
        <?php else : ?> $("#totalDueAmount").val(grandPrice.toFixed(2)); <?php endif;?>
	});

    /******************************************* ON KEYUP EVENT ENDS **************************************************/
    /******************************************************************************************************************/

    /******************************************* ON BLUR EVENT STSRTS **************************************************/
    /******************************************************************************************************************/

	// quantity operations on blur
	$(".qtyblur").blur(function()
	{
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		var sumDisount = 0;
		var sumTax = 0;

		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');
		else var qtyIdArr = $(this).attr("id").split('qty');

		// sell price
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val(); if(qty=="") qty = 0;
		var sellPr = $("#sellPrice"+qtyId).html();
		var netPrice = qty*parseFloat(sellPr);

        // cost price fifo order by quantity
        var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemFifoCostPrice/";
		$.ajax({
			type: "POST",
            dataType: "json",
			url: urlajax,
			data:
			{
                salesQty : qty,
				itemId : $("#proId"+qtyId).val(),
			},
			success: function(costPrice)
			{
				$("#costPriceAmount"+qtyId).val(costPrice);
			},
			error: function() {}
		});

		// discount later
		var disPr = $("#discount"+qtyId).html();
		var disPrice = (netPrice*parseFloat(disPr))/100;
		$("#discountTotal"+qtyId).html(disPrice.toFixed(2)); $("#discountAmount"+qtyId).val(disPrice.toFixed(2));

		// tax
		var txPr = $("#tax"+qtyId).html();
		var taxPrice = (netPrice*parseFloat(txPr))/100;
		$("#taxTotal"+qtyId).html(taxPrice.toFixed(2)); $("#taxAmount"+qtyId).val(taxPrice.toFixed(2));

		var netTotal = (netPrice + taxPrice) - disPrice;
		$("#netTotal"+qtyId).html(netTotal.toFixed(2));

		// sum calculations
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		for(var i=0;i<=lastRowId;i++)
		{
			var pk = $("#pk"+i).val();
			if(i==pk)
			{
				if($("#qty"+i).val()=="") var sumQtyId = 0;
				else if(typeof($("#qty"+i).val())==="undefined") {
					var sumQtyId = 0;
					var sumQtyWtId = $("#qtywt"+i).val();
				}
				else var sumQtyId = $("#qty"+i).val();

				if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
				else if(typeof($("#qtywt"+i).val())==="undefined") {
					var sumQtyId = $("#qty"+i).val();
					var sumQtyWtId = 0;
				}
				else var sumQtyWtId = $("#qtywt"+i).val();

				if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
				if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
				var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
				if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;

				sumQty+=parseFloat(sumQtyId);
				sumWeight+=parseFloat(sumQtyWtId);
				sumPrice+=totalQtyId*parseFloat($("#sellPriceAmount"+i).val());

				sumDisount+=parseFloat($("#discountAmount"+i).val());
				sumTax+=parseFloat($("#taxAmount"+i).val());
			}
		}

		// value placement/update
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
		$("#totalTaxAmnt").html(sumTax.toFixed(2)); $("#totalTax").val(sumTax.toFixed(2)); $("#spDiscount").val(sumTax.toFixed(2));

		var subGrandAmnt = sumPrice+sumTax;
		$("#subGrandAmount").html(subGrandAmnt.toFixed(2));
		$("#grandDiscount").html(sumDisount.toFixed(2)); $("#totalDiscount").val(sumDisount.toFixed(2));
		var grandPrice = subGrandAmnt - sumDisount;
		var duePrice = sumPrice - sumDisount;
		$("#grandTotal").html(grandPrice.toFixed(2));

        // special discount settings
        <?php if(Branch::getSpecialDiscountValue(Yii::app()->session['branchId'])==Branch::IS_SPECIAL_DISCOUNT) : ?>
                 $("#totalDueAmount").val(duePrice.toFixed(2));
        <?php else : ?> $("#totalDueAmount").val(grandPrice.toFixed(2)); <?php endif;?>
	});

    /******************************************* ON BLUR EVENT ENDS **************************************************/
    /******************************************************************************************************************/
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Sales</li>

    <li style="float:right; margin:1px 10px 0px 0px;">
        <i class="icon-bookmark"></i> Invoice Number : <span style="color:#e332ef;">IN<?php echo date( "Ymdhis");?></span>
    </li>
    <li style="float:right; margin:1px 10px 0px 0px;">
        <i class="icon-bookmark"></i> Invoice Date : <span style="color:#e332ef;"><?php echo date( "Y-m-d");?></span>
    </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sales-invoice-form',
	'enableAjaxValidation'=>true,
));
echo $form->hiddenField($model,'invNo', array('value'=>'IN'.date("Ymdhis")));?>
<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php endif;?>

<div style="width:74.3%;position:absolute;left:0px;top:0px">
    <?php $this->widget('CAutoComplete',array(
                 'model'=>$model,
                 'id'=>'custId',
                 'attribute' => 'custId',
                 //name of the html field that will be generated
                 //'name'=>'custId',
                 //replace controller/action with real ids
                 'value'=>!empty($model->custId)?$model->cust->name:'',
                 'url'=>array('ajax/autoCompleteCustomerSpecial'),
                 'max'=>100, //specifies the max number of items to display

                             //specifies the number of chars that must be entered
                             //before autocomplete initiates a lookup
                 'minChars'=>2,
                 'delay'=>500, //number of milliseconds before lookup occurs
                 'matchCase'=>false, //match case when performing a lookup?
                 'mustMatch' => true,
                             //any additional html attributes that go inside of
                             //the input field can be defined here
                 'htmlOptions'=>array('placeholder'=>'Customer Name',
                                      'class'=>'m-wrap large pull-right','style'=>'color:#000;border-bottom:0;'), // 'size'=>20,'maxlength'=>20,

                 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
                 //'extraParams' => array('taskType' => 'desc'),
                 'methodChain'=>".result(function(event,item){})",
                 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
            ));
    ?>
    <?php echo $form->error($model,'custId'); ?>
    <span id="custInfo_loader" style="display:none;">
        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
    </span>
    <a data-toggle="modal" href="#search_info" style="margin-right:20px;padding-top:7px;font-size:12px;float:right;"><i class="icon-bookmark"></i> Search Items</a>
</div>

<div class="tabbable tabbable-custom" style="width:74.3%;float:left;">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">Create Invoice</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
            <div id="search_info" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4>Search by item title : </h4>
                </div>
                <div class="modal-body">
                    <?php $this->widget('CAutoComplete',array(
                                 //'model'=>$model,
                                 'id'=>'itemId',
                                 //'attribute' => 'custId',
                                 //name of the html field that will be generated
                                 'name'=>'itemId',
                                 //replace controller/action with real ids
                                 //'value'=>($model->custId)?$model->cust->name:'',

                                 'url'=>array('ajax/autoCompleteItems'),
                                 'max'=>100, //specifies the max number of items to display

                                             //specifies the number of chars that must be entered
                                             //before autocomplete initiates a lookup
                                 'minChars'=>2,
                                 'delay'=>500, //number of milliseconds before lookup occurs
                                 'matchCase'=>false, //match case when performing a lookup?
                                 'mustMatch' => true,
                                             //any additional html attributes that go inside of
                                             //the input field can be defined here
                                 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20),

                                 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
                                 //'extraParams' => array('taskType' => 'desc'),
                                 'methodChain'=>".result(function(event,item){})",
                                 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
                            ));
                        ?>
                        <button style="margin-left:10px;" type="button" data-dismiss="modal" class="btn" id="useItems">Use Items</button>
                </div>
            </div>
            <div class="row" id="cust_dynamic_container" style="display:none;"></div>

            <div class="row" id="sales_dynamic_container">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th>Total Quantity</th>
                            <th>Total Weight</th>
                            <th>Total Amount</th>
                            <th>Total Tax</th>
                            <th>Sub Total</th>
                            <th>Total Discount</th>
                            <th>Grand Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="highlight">
                            	<div class="success"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandQty">0</span><?php echo $form->hiddenField($model,'totalQty', array('id'=>'totalQty'));?>
                            </td>
                            <td class="highlight">
                            	<div class="info"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandWeight">0</span><?php echo $form->hiddenField($model,'totalWeight', array('id'=>'totalWeight','value'=>0));?>
                            </td>
                            <td class="highlight">
                            	<div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="totalAmount">0.00</span><?php echo $form->hiddenField($model,'totalPrice', array('id'=>'totalPrice'));?>

                            </td>
                            <td class="highlight">
                            	<div class="warning"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="totalTaxAmnt">0.00</span><?php echo $form->hiddenField($model,'totalTax', array('id'=>'totalTax','value'=>0));?>
                            </td>
                            <td class="highlight">
                            	<div class="important"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="subGrandAmount">0.00</span>
                            </td>
                            <td class="highlight">
                            	<div class="warning" style="border-color:#0ef2f2;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandDiscount">0.00</span><?php echo $form->hiddenField($model,'totalDiscount', array('id'=>'totalDiscount','value'=>0));?>
                            </td>
                            <td class="highlight">
                            	<div class="important" style="border-color:#c14711;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandTotal">0.00</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
             </div>

             <div class="row" style="<?php echo (Branch::getInstantDiscountValue(Yii::app()->session['branchId'])==Branch::IS_INSTANT_DISCOUNT)?'height:564px;':'height:474px;';?>overflow:scroll;border-left:1px solid #ddd;border-top:1px solid #ddd; padding-left:1px;">
                <table class="table table-striped table-hover bordercolumn">
                    <thead>
                        <tr>
                            <th>Sl. No</th>
                            <th>Item Code</th>
                            <th class="hidden-480">Description</th>
                            <th class="hidden-480">Quantity</th>
                            <th class="hidden-480">Sell Price</th>
                            <th class="hidden-480">Disc %</th>
                            <th class="hidden-480">Disc Amount</th>
                            <th class="hidden-480">Tax %</th>
                            <th class="hidden-480">Tax Amount</th>
                            <th>Net Amount</th>
                            <th>Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<tr class="parentRow" id="0">
                            <td><span class="srlNo">1</span></td>
                            <td class="hidden-480">
                                <?php 
                                     echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:90px; height:15px;'));
                                     echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
                                     echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
                                ?>
                                <span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
                                    <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                </span>
                                <div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
                            </td>
                            <td><span id="itemName0" class="itemName">-</span></td>
                            <td class="hidden-480">
                                <?php echo CHtml::textField('qty[0]','',array('id'=>'qty0', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?>
                                <?php echo CHtml::hiddenField('type[0]','',array('id'=>'type0', 'class'=>'typeblur'));?>
                            </td>
                            <td class="hidden-480">
                                <span id="sellPrice0" class="sellPrice">0</span>
                                <?php echo CHtml::hiddenField('costPriceAmount[0]',0, array('id'=>'costPriceAmount0','class'=>'costPriceAmount'));?>
                                <?php echo CHtml::hiddenField('sellPriceAmount[0]',0, array('id'=>'sellPriceAmount0','class'=>'sellPriceAmount'));?>
                            </td>
                            <td class="hidden-480"><span id="discount0" class="discount">0</span><?php echo CHtml::hiddenField('discountPercent[0]','0',array('id'=>'discountPercent0', 'class'=>'discountPercent'));?></td>
                            <td><span id="discountTotal0" class="discountTotal">0</span><?php echo CHtml::hiddenField('discountAmount[0]','0',array('id'=>'discountAmount0', 'class'=>'discountAmount'));?></td>
                            <td class="hidden-480"><span id="tax0" class="tax">0</span></td>
                            <td><span id="taxTotal0" class="taxTotal">0</span><?php echo CHtml::hiddenField('taxAmount[0]','0',array('id'=>'taxAmount0', 'class'=>'taxAmount'));?></td>
                            <td><span id="netTotal0" class="netTotal">0</span></td>
                            <td>
                                <img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
                            </td>
                        </tr>  
                    </tbody>
                </table>
            </div>
         </div>
	</div>
                                            
    <div class="row buttons pull-right">
        <button type="submit" data-dismiss="modal" id="submitInvoice" class="btn blue" style="margin:10px 0px 7px 0px;">Submit Invoice</button>
    </div>
</div>

<div class="pull-right" style="width:22.5%;float:right">
    <div class="pull-right" style="clear:right; width:100%; background:#fff; border:1px solid #ddd;padding:0px 10px;">
        <div class="row">
            <?php echo CHtml::label('<strong>Total Amount</strong>', ''); ?>
            <?php echo CHtml::textField('totalDueAmount','', array('id'=>'totalDueAmount','class'=>'span12 m-wrap','readonly'=>'true','style'=>'font-size:18px;','placeholder'=>'0.00')); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model,'totalShipping', array('style'=>'font-weight:bold;')); ?>
            <?php echo $form->textField($model,'totalShipping', array('id'=>'totalShipping','class'=>'span12 m-wrap','style'=>'color:#999;font-size:18px;','placeholder'=>'0.00')); ?>
            <?php echo $form->error($model,'totalShipping'); ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   
