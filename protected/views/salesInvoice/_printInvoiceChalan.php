<style type="text/css">
    @media screen
    {
        .invoiceTbl td {font-family:verdana,sans-serif;font-size:10px;}
    }
    @media print
    {
        .invoiceTbl td {font-size:10px;}
    }
    @media screen,print
    {
        .invoiceTbl td {font-size:10px;}
    }
    .bordercolumn tbody tr td {
        padding: 0px !important;
        border:1px solid #000;

    }
    table tr td {
        font-size: 10px !important;
    }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    //-------print options----//
    $(function()
    {
        $('#report_dynamic_container').printElement(
            {
                overrideElementCSS:[
                    '<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
                    { href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
                ],
                printBodyOptions:{
                    styleToAdd:'margin-left:25px !important',
                    //classNameToAdd : 'printBody',
                }
            }
        );
    });
</script>
<div style="display:none;">
    <div class="form" id="report_dynamic_container" style="padding-top: 120px;">
        <div class="row">
            <?php
            $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
            $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
            $salesModel = SalesInvoice::model()->findByPk($invoice);
            $date = date("d-m-Y", strtotime($salesModel->orderDate));
            $date = date("j F Y", strtotime($date));
            ?>
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td style="text-align:left; font-size: 12px;" colspan="2">
                        <div style="text-align: left; width: 200px; float: left;">
                            <?php
                            echo '<span style="margin: 0; font-size: 20px;">Challan</span>';
                            ?><br />
                            <strong>#<?php echo $salesModel->invNo; ?></strong><br />
                            <span style="font-weight: normal; font-size: 14px;">Date: <?php echo $date; ?> <br /></span>
                            <?php if($salesModel->custId!=0) : ?>
                            <span style="font-weight: normal; margin-top: 15px; font-size: 14px;">To: </span><span style="font-weight: bold; font-size: 15px; margin-top: 15px;"><?php echo $salesModel->cust->name; ?><br />
                                <span style="font-weight: normal; font-size: 12px; margin-left: 25px; margin-top: 10px;"><?php echo $salesModel->cust->addressline . ','. $salesModel->cust->city; ?></span>
                                <?php endif; ?>
                        </div>
                    </td>
                </tr>
            </table >
            <br />
            <?php if(!empty($salesModel)) : ?>
                <table class="table table-striped table-hover bordercolumn">
                    <tr style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; ">
                        <th style="width: 50px; text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">SL No.</th>
                        <th style="text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Product</th>
                        <th style="text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Description</th>
                        <th style="width: 80px; text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Quantity</th>
                    </tr>
                    <?php $totalQty = 0;
                    $i=1;
                    foreach($salesModel->salesDetails as $key=>$sales) :
                        $totalQty+=$sales->qty; ?>
                        <tr>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $i. '.'; ?></td>
                            <td style="padding: 0px 3px; font-size: 12px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $sales->item->cat->name;?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $sales->item->itemName;?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: center; border: 1px solid #000;"><?php echo $sales->qty;?></td>
                        </tr>
                        <?php $i++; endforeach;  ?>
                    <tr>
                        <td colspan="3" rowspan="4" style="border: 1px solid #000; padding: 0px 3px;text-align: right;">Total</td>
                        <td style="padding: 0px 3px; border: 1px solid #000;text-align: center;font-size: 12px;"><?php echo $totalQty;?></td>
                    </tr>
                </table>
                <?php
            endif; ?>
        </div>
    </div>
</div>