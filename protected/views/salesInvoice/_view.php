<?php
/* @var $this SalesInvoiceController */
/* @var $data SalesInvoice */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invNo')); ?>:</b>
	<?php echo CHtml::encode($data->invNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('branchId')); ?>:</b>
	<?php echo CHtml::encode($data->branchId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custId')); ?>:</b>
	<?php echo CHtml::encode($data->custId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orderDate')); ?>:</b>
	<?php echo CHtml::encode($data->orderDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delivaryDate')); ?>:</b>
	<?php echo CHtml::encode($data->delivaryDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalQty')); ?>:</b>
	<?php echo CHtml::encode($data->totalQty); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalWeight')); ?>:</b>
	<?php echo CHtml::encode($data->totalWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalPrice')); ?>:</b>
	<?php echo CHtml::encode($data->totalPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalDiscount')); ?>:</b>
	<?php echo CHtml::encode($data->totalDiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalTax')); ?>:</b>
	<?php echo CHtml::encode($data->totalTax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cashPaid')); ?>:</b>
	<?php echo CHtml::encode($data->cashPaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardPaid')); ?>:</b>
	<?php echo CHtml::encode($data->cardPaid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardId')); ?>:</b>
	<?php echo CHtml::encode($data->cardId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cardNo')); ?>:</b>
	<?php echo CHtml::encode($data->cardNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>