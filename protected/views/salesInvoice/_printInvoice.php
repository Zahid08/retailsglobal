<style type="text/css">
    .invoiceTbl td{ margin:0; padding:0;}
    @media screen
      {
        .invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
      }
    @media print
      {
        .invoiceTbl td {font-size:11px;}
      }
    @media screen,print
      {
        .invoiceTbl td {font-size:11px;}
      }
#invoice-POS{
  box-shadow: 0 0 1in -0.25in rgba(0, 0, 0, 0.5);
  padding:2mm;
  margin: 0 auto;
  width: 100mm;
  background: #FFF;
::selection {background: #f31544; color: #FFF;}
::moz-selection {background: #f31544; color: #FFF;}
h1{
  font-size: 1.5em;
  color: #222;
}
h2{font-size: .9em;}
h3{
  font-size: 1.2em;
  font-weight: 300;
  line-height: 2em;
}
p{
  font-size: .7em;
  color: #666;
  line-height: 1.2em;
}
 
#top, #mid,#bot{ /* Targets all id with 'col-' */
  border-bottom: 1px solid #EEE;
}

#top{min-height: 100px;}
#mid{min-height: 80px;}
#bot{ min-height: 50px;}

#top .logo{
  //float: left;
height: 60px;
width: 60px;
background: url(http://michaeltruong.ca/images/logo1.png) no-repeat;
background-size: 60px 60px;
}
.clientlogo{
  float: left;
height: 60px;
width: 60px;
background: url(http://michaeltruong.ca/images/client.jpg) no-repeat;
background-size: 60px 60px;
  border-radius: 50px;
}
.info{
  display: block;
  //float:left;
  margin-left: 0;
}
.title{
  float: right;
}
.title p{text-align: right;}


table{
  width: 100%;
  border-collapse: collapse;
}
table, td, th {
  border: 1px solid black;
}
td{
  //padding: 5px 0 5px 15px;
  //border: 1px solid #EEE
}
.tabletitle{
  //padding: 5px;
  font-size: .5em;
  background: #EEE;
}
.service{border-bottom: 1px solid #EEE;}
.item{width: 24mm;}
.itemtext{font-size: .5em;}

#legalcopy{
  margin-top: 5mm;
}

</style>

<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="<?php echo Yii::app()->baseUrl;?>/js/printPreview.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    // print options
    $(function()
    {
        $('#invoice-POS').printElement(
        {
            overrideElementCSS:[
                '<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
                { href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
            ],
            printBodyOptions:{
            }
        });
    });
</script>

<?php
$returnModel = array();
if (!empty($salesReturn) && isset($salesReturn)){
    $salesInvoiceId=json_decode($salesReturn);
    // $salesReturnId=$salesInvoiceId[0];
    // $returnModel= SalesReturn::model()->findAll('salesId=:salesId',array(':salesId'=>$salesReturnId));
    $returnModel = SalesReturn::model()->findAllByAttributes(array("salesId"=>$salesInvoiceId));
}
?>

<div style="float: right;">
  <input type="button" class="btn green" onclick="printDiv('masterContent')" value="Print Invoice!" />
  <input type="button" class="btn red" value="Close" onclick="window.top.location = window.top.location" />
</div>
<div class="form" id="masterContent">
    <style>
        table td, table th{
            padding: 0px 10px!important;
        }
        p.itemtext {
            margin: 3px 0px 3px!important;
        }
        h5.itemcodeArea {
            padding: 0px!important;
            margin: 3px 0px 3px!important;
            font-size: 13px;
        }
        p.legal {
            margin: 1px;
        }
        td.underconditions {
            padding: 1px!important;
            margin: 0px!important;
        }
        h5.transaction_block {
            padding: 0px!important;
            margin: 5px 0px 3px!important;
        }
    </style>
  <div id="invoice-POS" style="width:292px; margin: 0 auto; height:auto; background:#fff; padding:10px;border:0px dashed #3d3d3d;">
   
    <center id="top">
      <div class="logo"></div>
      <div class="info">
                    <?php 
                        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                        $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
                        if(!empty($companyModel)) : ?>
        <h3><img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:100%;height: 60px;" /></h3>
        <h3 style="margin-bottom: -10px;margin-top: -5px;"></h3>
        <h5 style="margin-bottom: -1px;font-size: 12px;"><?php
            endif; 
            if(!empty($branchModel)) echo $branchModel->name;
        ?></h5>
      </div><!--End Info-->
       <div class="info" style="border-bottom: 1px dashed;border-top: 1px dashed;">
        <h5  style="margin-bottom: -33px;margin-top: 5px;font-size: 12px;"><span>53/2, New Elephant Road, Dhaka-1205,</span></h5>
        <br>
        <h5 style="margin-bottom: -35px;font-size: 12px;"><span><?php if(!empty($branchModel)) echo $branchModel->addressline.', Phone : '.$branchModel->phone;?></span></h5>
        <br>
        <h5 style="margin-bottom: 5px;margin-top: 12px;font-weight: bold;font-size: 12px;"><span>BIN : <?php echo $branchModel->vatrn;?></span></h5>
        <?php
          if(!empty($custModel)){
        ?>

        <h5  style="margin: 0 !important;font-size: 12px;">Customer: <?php echo $custModel->name; ?></h5>
        <h5  style="margin: 0 !important;font-size: 12px;">Phone: <?php echo $custModel->phone; ?></h5>


        <?php } ?>
      </div><!--End Info-->

    </center><!--End InvoiceTop-->
   
    <div id="bot">

    <div id="table" style="width: 100%;">
        <table style="width: 100%; font-size: 12px;">
        <tr class="tabletitle" style="border-bottom: 1px dashed;">
          <td class="" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">Item Code</h5></td>
          <td class="" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">RPU</h5></td>
          <td class="Hours" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">Qty</h5></td>
          <td class="Rate" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">Total</h5></td>
        </tr>
                <?php
                $salesModel = SalesInvoice::model()->findByPk($invoice);
                $totalSales = 0;
                $totalQty = 0;
                foreach($salesModel->salesDetails as $key=>$sales) : 
                    $netSalesPrice = $sales->qty * $sales->salesPrice;
                    $totalSales+=$netSalesPrice;
                    $totalQty+=$sales->qty;
                ?>
        <tr class="service" style="border-bottom: 1px dashed;">
          <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $sales->item->itemCode;?></p></td>
          <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php
        
          echo $sales->salesPrice;?></p></td>
          <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $sales->qty;?></p></td>
          <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $netSalesPrice;?></p></td>
        </tr>
            <?php endforeach; ?>

        <tr class="tabletitle">
          <td>Net Total</td>
          <td></td>
          <td class="Rate" style="border-bottom: 1px dashed;"><?php echo $totalQty;?></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;"><?php echo UsefulFunction::formatMoney($totalSales);?></h4></td>
        </tr>

        <!-- tax part -->
                <?php 
                $vatArr = array();
                $totalVat = 0;
                foreach($salesModel->salesDetails as $key=>$sales) : 
                    //$taxRate = $sales->item->tax->taxRate;
                    $taxRate = Tax::getTaxByTaxId($sales->item->taxId);
                    if($taxRate>0) :
                        //if(!in_array($taxRate, $vatArr)) :
                            //$netPrice = SalesDetails::getuniqueVatWiseSells($sales->salesId,$taxRate);
                            $netPrice = $sales->qty * $sales->salesPrice;
                            $vat = ($netPrice*$taxRate)/100;
                ?>
                        
        <tr class="tabletitle">
          <td>Tax</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;"><?php echo UsefulFunction::formatMoney($totalSales+$totalVat);?></h4></td>
        </tr>
                <?php //endif;
                      //$vatArr[] = $taxRate;
                    endif;
                endforeach; ?>

        <tr class="tabletitle">
          <td>Discount</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;    font-size: 15px;"><?php echo $salesModel->totalDiscount;?></h4></td>
        </tr>        
        
        <!-- special Discount -->
        <?php if(!empty($salesModel->spDiscount) && $salesModel->spDiscount>0) :?> 
        <tr class="tabletitle">
          <td>Discount</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;"><?php echo $salesModel->spDiscount;?></h4></td>
        </tr>
         <?php endif; ?>

        <!-- Instant Discount -->
        <?php if(!empty($salesModel->instantDiscount) && $salesModel->instantDiscount>0) :?> 
        <tr class="tabletitle">
          <td>Instant Discount</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;"><?php echo $salesModel->instantDiscount;?></h4></td>
        </tr>
        <?php endif; ?>

        <!-- Instant Discount cupon-->
        <?php if(!empty($salesModel->couponId) && $salesModel->couponId>0) :?> 
        <tr class="tabletitle">
          <td>Instant Discount(cupon)</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;"><?php echo $salesModel->coupon->couponAmount;?></h4></td>
        </tr>
        <?php endif; ?>

        <!-- Grand Total-->
        <?php if(!empty($salesModel->couponId) && $salesModel->couponId>0) :?> 
        <tr class="tabletitle">
          <td>Grand Total</td>
          <td class="Rate"></td>
          <td class="Rate"></td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;">
            <?php 
                // considering instant & special discount
                $grandTotal = ($totalSales+$totalVat) - ($salesModel->totalDiscount+$salesModel->spDiscount+$salesModel->instantDiscount);
                
                // coupon consideration
                if(!empty($salesModel->couponId) && $salesModel->couponId>0) $grandTotal = ($grandTotal - $salesModel->coupon->couponAmount);

                if($salesModel->cardId!=0) echo UsefulFunction::formatMoney($grandTotal);
                else 
                {
                    $str_count = substr_count($grandTotal, '.');
                    if($str_count >0) 
                    {
                        $roundoffArr = explode('.',$grandTotal);
                        if(!empty($roundoffArr)) echo UsefulFunction::formatMoney(round($grandTotal)).'&nbsp;(round of 0.'.$roundoffArr[1].')';
                    }
                    else echo UsefulFunction::formatMoney($grandTotal);
                }
            ?>              
          </h4></td>
        </tr>
        <?php endif; ?>

        <!-- cash paid -->
        
        <?php if(!empty($salesModel->cashPaid)) :?>
        <tr class="tabletitle">
          <td><span style="color:red;">Cash Paid</span>:</td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;">
              <?php echo $salesModel->cashPaid;?>
          </h4></td>
        </tr>
        <?php endif; ?>

        <!-- cash/card -->
        <?php if($salesModel->cardId!=0) : ?>
        <tr class="tabletitle">
          <td><span style="color:red;">Cash</span>:</td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;">
             BDT <?php echo UsefulFunction::formatMoney($salesModel->cashPaid); //echo UsefulFunction::formatMoney((($totalSales+$totalVat) - $salesModel->totalDiscount) - $salesModel->cardPaid);?>
          </h4></td>
        </tr>        
        <tr class="tabletitle">
          <td><span style="color:red;"><?php echo $salesModel->card->name;?></span>:</td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment"><h4 style="margin-bottom: 1px;margin-top: 1px;font-size: 15px;">
             BDT <?php echo UsefulFunction::formatMoney($salesModel->cardPaid);?>
          </h4></td>
        </tr>
    <?php endif;?>


        <!-- change amount -->
        <?php if(!empty($salesModel->totalChangeAmount)) :?> 
        <tr class="tabletitle">
          <td>Refund Amount </td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;">
            <?php echo $salesModel->totalChangeAmount;?>
            </h4>
          </td>
        </tr>
        <?php endif; ?>

        <!-- Points Awarded -->
        <?php if($salesModel->custId!=0) :?> 
        <tr class="tabletitle">
          <td>Points Awarded </td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;">
            <?php echo Customer::getCustomerLoyaltyPointBySalesId($salesModel->id);?>
            </h4>
          </td>
        </tr>        

        <tr class="tabletitle">
          <td>Points Redeemed </td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;">
                <?php 
                    echo $salesModel->loyaltyPaid;
                    // echo Customer::getCustomerLoyaltyInvoice($salesModel->cust->id);
                ?>
            </h4>
          </td>
        </tr>
        <tr class="tabletitle">
          <td>Balance Points </td>
          <td class="Rate"></td>
          <td class="Rate">:</td>
          <td class="payment" style="border-bottom: 1px dashed;"><h4 style="margin-bottom: 1px;margin-top: 1px;">
                <?php echo Customer::getCustomerLoyaltyBalance($salesModel->cust->id);?>
            </h4>
          </td>
        </tr>
        <?php endif; ?>
        </table>

        <?php
            if(count($returnModel) > 0){
        ?>

        <hr style="margin: 4px 0 !important;">
        <h5 style="text-align: center;font-size: 1em;font-weight: bold;margin: 0 !important;">Return</h5>
        <hr style="margin: 4px 0 !important;">

        <table style="width: 100%; font-size: 12px;">
            <tr class="tabletitle" style="border-bottom: 1px dashed;">
                <td class="Hours" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">ItemCode</h5></td>
                <td class="Hours" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">Qty</h5></td>
                <td class="Rate" style="border-bottom: 1px dashed;"><h5 class="itemcodeArea">Amount</h5></td>
            </tr>
            <?php
                $intotalPrice = 0;
                $totalQty = 0;
                foreach ($returnModel as $key=>$item){
                    $qry=$item->qty;
                    $intotalPrice +=$item->totalPrice;
                    $totalQty +=$item->qty;
                    $modelSale= SalesInvoice::model()->find('id=:id',array(':id'=>$item->salesId));
                    $InvoiceId='';
                    if ($modelSale){
                        $InvoiceId= $modelSale->invNo;
                    }
                    $itemModel = Items::model()->find('id=:id',array(':id'=>$item->itemId));
                    $itemCode='';
                    if ($itemModel){
                        $itemCode=$itemModel->itemCode;
                    }
            ?>

                <tr class="service" style="border-bottom: 1px dashed;">
                    <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $itemCode;?></p></td>
                    <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $qry;?></p></td>
                    <td class="tableitem" style="border-bottom: 1px dashed;"><p class="itemtext"><?php echo $item->totalPrice;?></p></td>
                </tr>

            <?php } ?>

                  <tr>
                    <td>Total</td>
                    <td> <?php echo $totalQty; ?> </td>
                    <td><?php echo $intotalPrice; ?></td>
                  </tr>

        </table>

        <?php } ?>



    </div><!--End Table-->
  <br>
    <div class="info" style="border: 1px dashed;width: 74%;margin: 0 auto;">
       <center><h4 style=" margin-bottom: 1px;margin-top: 1px;">Terms & Conditions</h4></center>
    </div><!--End Info-->
  <br>
  <div id="legalcopy" style="margin-left: 15px; margin-left: 15px;border-bottom: 1px dashed;font-size: 12px;">
      <p class="legal">• No Return/Exchange is possible without its Full Invoice.</p>
      <p class="legal">• Bought product can only be returned/exchanged within 10
days of invoice issue date.
      </p>
      <p class="legal">• No Return is possible if there have no production fault
founds, and it will decided by Plus Point authority only.</p>
      <p class="legal">• Refund is not applicable, customers can only return &
exchange the product/s if it fulfil return and exchange
criteria set by Plus Point Authority.</p>      
      <p class="legal">• Decision taken by Authority should be granted by
      customer in positive.</p>
      <p class="legal">• Discount Products are not applicable for any Exchange or
      Return.</p>
  </div>
      <table style="width: 100%;margin-top: -6px;">
        <tr class="tabletitle" style="border-bottom: 1px dashed;">
          <td class="underconditions" style="text-align: right;"><h5>Date :</h5></td>
          <td class="underconditions"><h5><?php echo date("d.m.Y");?></h5></td>
          <td class="Hours underconditions"><h5></h5></td>
          <td class="Hours underconditions"><h5>Time:</h5></td>
          <td class="Hours underconditions"><h5><?php echo date("g:i:s");?></h5></td>
        </tr>
      </table>
        <table style="width: 100%;margin-top: 0px;margin-bottom: -20px;">
            <tr class="tabletitle" style="border-bottom: 1px dashed;">
              <td class="" style="text-align: right;"><h5 class="transaction_block">Transaction ID :</h5></td>
              <td class=""></td>
              <td class="Hours"><h5 class="transaction_block"><?php echo $salesModel->invNo;?></h5></td>
            </tr>
        </table>
      <table style="width: 100%;margin-top: 14px;">
        <tr class="tabletitle" style="border-bottom: 1px dashed;">
          <td class="" style="text-align: right;"><h5>Biller</h5></td>
          <td class=""><h5>:</h5></td>
          <td class="Hours"><h5><?php echo Yii::app()->user->name;?></h5></td>
        </tr>
      </table>
    <div class="info" style="border: 1px dashed;width: 107%;margin: 12px -10px auto;">
       <center>

       <?php  
              $baseUrl = Yii::app()->baseUrl;
              $invNo = $salesModel->invNo;
              echo "<img style='width:100%;' alt='testing' src='".$baseUrl."/barcode/barcode.php?codetype=Code39&size=40&text=".$invNo."&print=true'/>";
       ?>
        </center>
    </div><!--End Info-->
  <br>
        <?php
            if ($salesModel->message){
        ?>
  <div id="legalcopy" style="margin-left: 15px; margin-right: 15px; margin-left: 15px;text-align: justify;font-size: 12px;">
      <p class="legal"><?php echo $salesModel->message?></p>
  </div>
        <?php } ?>        

        <?php
            if ($salesModel->remarks){
        ?>
  <div id="legalcopy" style="margin-left: 15px; margin-right: 15px; margin-left: 15px;text-align: justify;font-size: 12px;">
      <p class="legal"><?php echo $salesModel->remarks?></p>
  </div>
        <?php } ?>
</div><!--End InvoiceBot-->

  </div><!--End Invoice-->      
</div>

<script type="text/javascript">
  function printDiv(masterContent) {
     var printContents = document.getElementById(masterContent).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
