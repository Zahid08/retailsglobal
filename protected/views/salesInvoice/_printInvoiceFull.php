<style type="text/css">
    @media screen
    {
        .invoiceTbl td {font-family:verdana,sans-serif;font-size:10px;}
    }
    @media print
    {
        .invoiceTbl td {font-size:10px;}
    }
    @media screen,print
    {
        .invoiceTbl td {font-size:10px;}
    }
    .bordercolumn tbody tr td {
        padding: 0px !important;
        border:1px solid #000;

    }
    table tr td {
        font-size: 10px !important;
    }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    //-------print options----//
    $(function()
    {
        $('#report_dynamic_container').printElement(
        {
            overrideElementCSS:[
                '<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
                { href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
            ],
            printBodyOptions:{
                styleToAdd:'margin-left:25px !important',
                //classNameToAdd : 'printBody',
            }
        });
    });
</script>
<div style="display:none;">
    <div class="form" id="report_dynamic_container" style="padding-top: 120px;">
        <div class="row">
            <?php
                $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
                $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']);
                $salesModel = SalesInvoice::model()->findByPk($invoice);
                $date = date("d-m-Y", strtotime($salesModel->orderDate));
                $date = date("j F Y", strtotime($date));
            ?>
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td style="text-align:left; font-size: 12px;" colspan="2">
                        <div style="text-align: left; width: 200px; float: left;">
                            <?php
                            echo '<span style="margin: 0; font-size: 20px;">Invoice/ Bill</span>';
                            ?><br />
                            <strong>#<?php echo $salesModel->invNo; ?></strong><br />
                            <span style="font-weight: normal; font-size: 14px;">Date: <?php echo $date; ?> <br /></span>
                            <?php if($salesModel->custId!=0) : ?>
                            <span style="font-weight: normal; margin-top: 15px; font-size: 14px;">To: </span><span style="font-weight: bold; font-size: 15px; margin-top: 15px;"><?php echo $salesModel->cust->name; ?><br />
                                <span style="font-weight: normal; font-size: 12px; margin-left: 25px; margin-top: 10px;"><?php echo $salesModel->cust->addressline . ','. $salesModel->cust->city; ?></span>
                                <?php endif; ?>
                        </div>
                    </td>
                </tr>
            </table >
            <br />
            <?php if(!empty($salesModel)) : ?>
                <table class="table table-striped table-hover bordercolumn">
                    <tr style="border-top:1px solid #000;border-left:1px solid #000;border-right:1px solid #000; ">
                        <th style="width: 50px; text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">SL No.</th>
                        <th style="text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Product</th>
                        <th style="text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Description</th>
                        <th style="width: 80px; text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Quantity</th>
                        <th style="width: 80px; text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Price Per Unit</th>
                        <th style="text-align: center; border: 1px solid #000; font-size: 12px; padding: 1px;">Value (TK)</th>
                    </tr>
                    <?php $totalSales = 0;
                    $i=1;
                    foreach($salesModel->salesDetails as $key=>$sales) :
                        $netSalesPrice = $sales->qty * $sales->salesPrice;
                        $totalSales+=$netSalesPrice; ?>
                        <tr>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $i. '.'; ?></td>
                            <td style="padding: 0px 3px; font-size: 12px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $sales->item->cat->name;?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $sales->item->itemName;?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: left; border: 1px solid #000;"><?php echo $sales->qty;?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: right; border: 1px solid #000;"><?php echo UsefulFunction::formatMoney($sales->salesPrice,2);?></td>
                            <td style="padding: 0px 3px; font-size: 12px; text-align: right; border: 1px solid #000;"><?php echo UsefulFunction::formatMoney($netSalesPrice,2);?></td>
                        </tr>
                        <?php $i++; endforeach;  ?>
                    <tr>
                        <td colspan="4" rowspan="4" style="border: 1px solid #000; padding: 0px 3px;">&nbsp;</td>
                        <td style="border: 1px solid #000; padding: 0px 3px; font-size: 12px;">
                            <div style="width: 90px; text-align: left;">Total</div>
                        </td>
                        <td style="padding: 0px 3px; border: 1px solid #000;text-align: right;font-size: 12px;"><?php echo UsefulFunction::formatMoney($totalSales,2);?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 3px; border: 1px solid #000; border: 1px solid #000; font-size: 12px;">
                            <div style="width: 90px; text-align: left;">Delivery Charge</div>
                        </td>
                        <td style="padding: 0px 3px; border: 1px solid #000; font-size: 12px;text-align: right;"><?php echo UsefulFunction::formatMoney($salesModel->totalShipping,2);?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 3px; border: 1px solid #000; border: 1px solid #000; font-size: 12px;">
                            <div style="width: 90px; text-align: left;">VAT Charge</div>
                        </td>
                        <td style="padding: 0px 3px; border: 1px solid #000; font-size: 12px;text-align: right;"><?php echo UsefulFunction::formatMoney($salesModel->totalTax,2); ?></td>
                    </tr>
                    <tr>
                        <td style="padding: 0px 3px; border: 1px solid #000; font-size: 12px;"><div style="width: 90px; text-align: left;">Grand Total</div></td>
                        <td style="padding: 0px 3px; border: 1px solid #000; font-size: 12px;text-align: right; font-weight: bold;"><?php echo UsefulFunction::formatMoney(($totalSales+$salesModel->totalShipping),2);?></td>
                    </tr>
                </table>
                <?php
            endif; ?>
        </div>
    </div>
</div>