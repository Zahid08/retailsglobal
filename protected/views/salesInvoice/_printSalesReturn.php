<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script language="VBScript" type="text/javascript">
	function offprintDialougeVb()
	{
		// THIS VB SCRIP REMOVES THE PRINT DIALOG BOX AND PRINTS TO YOUR DEFAULT PRINTER
		Sub window_onunload()
		On Error Resume Next
		Set WB = nothing
		On Error Goto 0
		End Sub
		
		Sub Print()
		OLECMDID_PRINT = 6
		OLECMDEXECOPT_DONTPROMPTUSER = 2
		OLECMDEXECOPT_PROMPTUSER = 1
		
		
		On Error Resume Next
		
		If DA Then
		call WB.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,1)
		
		Else
		call WB.IOleCommandTarget.Exec(OLECMDID_PRINT ,OLECMDEXECOPT_DONTPROMPTUSER,"","","")
		
		End If
		
		If Err.Number <> 0 Then
		If DA Then 
		Alert("Nothing Printed :" & err.number & " : " & err.description)
		Else
		HandleError()
		End if
		End If
		On Error Goto 0
		End Sub
		
		If DA Then
		wbvers="8856F961-340A-11D0-A96B-00C04FD705A2"
		Else
		wbvers="EAB22AC3-30C1-11CF-A7EB-0000C05BAE0B"
		End If
		
		document.write "<object ID=""WB"" WIDTH=0 HEIGHT=0 CLASSID=""CLSID:"
		document.write wbvers & """> </object>"
	}
</script>
<script type="text/javascript" language="javascript">
	function offprintDialouge() 
	{
		var WebBrowser = '<object id="WebBrowser1" width=0 height=0 classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
		document.body.insertAdjacentHTML('beforeEnd', WebBrowser);
		WebBrowser1.ExecWB(1, 2); // use a 1 vs. a 2 for a prompting dialog box 
		WebBrowser1.outerHTML = ""; 
	}
	
	//-------print options----//
	$(function()
	{
		 //offprintDialouge();
		 $('#content-table-inner').printElement(
		 {
				overrideElementCSS:[
					'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
					{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
				],            
				printBodyOptions:{                         	
				}
			}			
		 );
	});
</script>  
		
<div class="form" style="display:none;">
	<div class="row" style="width:292px; height:auto; background:#fff; padding:10px;border:0px dashed #3d3d3d;" id="content-table-inner">
    	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl">
            <tr>
                <td align="center" style="text-align:center; border-bottom:1px dashed #000; padding:3px 0px;" colspan="2" class="marpad">
                	<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
						if(!empty($companyModel)) : ?>
                			<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                   			<?php echo $companyModel->name; echo "<br/>";
                   			echo "Sales Return Invoice";
						endif; 
					?>
                </td> 
            </tr>
            <tr>
            	<td style="text-align:center; border-bottom:1px dashed #000;padding:3px 0px;" colspan="2">
            		<?php 
						$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
						if(!empty($branchModel)) echo $branchModel->addressline;  
					?>
            	</td>
            </tr>
            
            <?php 
			$salesModel = SalesInvoice::model()->findByPk($invoice); 
			if(!empty($salesModel)) : 
				if($salesModel->custId!=0) : ?>
					<tr>
						<td style="text-align:left;padding:3px 0px 10px 0px;" colspan="2">
							<span>Customer :</span><br />
							No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <?php echo $salesModel->cust->custId;?><br />
							Name : <?php echo $salesModel->cust->title.'&nbsp;'.$salesModel->cust->name;?>
						</td>
					</tr>
				<?php else : ?>
					<tr>
						<td colspan="2" style="height:10px;"></td>
					</tr>
				<?php endif;
                $totalSales = 0;
				$criteria = new CDbCriteria;					
				$criteria->condition='salesId=:salesId and branchId=:branchId';
				$criteria->params=array(':salesId'=>$salesModel->id,':branchId'=>Yii::app()->SESSION['branchId']);
				$criteria->addInCondition('itemId',$itemArr);	
				$criteria->order = 'returnDate desc';
				$salesDetaulsModel = SalesReturn::model()->findAll($criteria);
				
				foreach($salesDetaulsModel as $key=>$sales) : 
					$totalSales+=$sales->totalPrice; ?>
                    <tr>
                        <td style="text-align:left;"><?php echo $sales->item->itemCode;?><br />&nbsp;&nbsp;<?php echo $sales->qty;?> *</td>
                        <td style="text-align:right;">
							<?php echo $sales->item->itemName;?><br /><?php echo round(($sales->totalPrice/$sales->qty),2);?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $sales->totalPrice;?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ($sales->item->tax->taxRate)>0?$sales->item->tax->taxRate:'N';?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td style="text-align:left;">Grand Total :</td>
                    <td style="text-align:right;border-top:1px dashed #000;"><?php echo $totalSales;?></td>
                </tr>
                <tr>
                    <td colspan="2" style="height:5px;"></td>
                </tr>
            <?php endif; ?>
            
            <tr>
            	<td style="text-align:left;">Date&nbsp;&nbsp;&nbsp;&nbsp;Time<br /><?php echo gmdate("d.m.Y g:i");?></td>
                <td style="text-align:right;">Transaction ID<br /><?php echo $salesModel->invNo;?></td>
            </tr>
            <tr>
            	<td style="text-align:left;" colspan="2">Cashier Name :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo Yii::app()->user->name;?></td>
            </tr>
            <tr>
            	<td style="text-align:left;" colspan="2">Vat Registration No :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $branchModel->vatrn;?></td>
            </tr>
            <tr>
            	<td colspan="2" style="height:10px;"></td>
            </tr>
            <tr>
            	<td style="text-align:left;padding:5px 0px;border-top:1px dashed #000;" colspan="2"><?php echo $companyModel->detailsinfo;?></td>
            </tr>
        </table>
    </div>        
</div>
