<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Slider<i class="icon-angle-right"></i></li>
    <li>Details Advertisement # <?php echo $model->position; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(		
		'position',
		'title',
        'shortDesc',
		'url',
		array(
            'name' => 'image',
            'type' => 'raw',
            'value' => CHtml::image(Yii::app()->getBaseUrl(true).'/media/catalog/sliders/'.$model->image,"",array("width"=>50)),
        ),
array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
		'rank',
	),
)); ?>
</div>
