<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Slider<i class="icon-angle-right"></i></li>
    <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Slider</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sliders-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Slider</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			 <!--<div class="row">
                <?php /*echo $form->labelEx($model,'position'); */?>
                <?php /*echo $form->dropDownList($model,'position', UsefulFunction::eshopAdvertisementposition(), array('class'=>'m-wrap large','prompt'=>'Select Position')); */?>
                <?php /*echo $form->error($model,'position'); */?>
            </div>-->
            <div class="row">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('class'=>'m-wrap large','size'=>60,'maxlength'=>200)); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'secondTitle'); ?>
                <?php echo $form->textField($model,'secondTitle',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'secondTitle'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'shortDesc'); ?>
                <?php echo $form->textArea($model,'shortDesc',array('class'=>'m-wrap larg','rows'=>"3",'size'=>60,'maxlength'=>255,'style'=>"width: 332px;")); ?>
                <?php echo $form->error($model,'shortDesc'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'url'); ?>
                <?php echo $form->textField($model,'url',array('class'=>'m-wrap large','size'=>60,'maxlength'=>700)); ?>
                <?php echo $form->error($model,'url'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'image'); ?>
				<?php echo $form->fileField($model,'image',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'image'); ?>
				<?php echo CHtml::image(Yii::app()->getBaseUrl(true).'/media/catalog/sliders/'.$model->image,"",array("width"=>50));?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   