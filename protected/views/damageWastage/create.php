<?php
/* @var $this DamageWastageController */
/* @var $model DamageWastage */

$this->breadcrumbs=array(
	'Damage Wastages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DamageWastage', 'url'=>array('index')),
	array('label'=>'Manage DamageWastage', 'url'=>array('admin')),
);
?>

<h1>Create DamageWastage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>