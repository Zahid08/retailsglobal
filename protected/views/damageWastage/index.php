<?php
/* @var $this DamageWastageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Damage Wastages',
);

$this->menu=array(
	array('label'=>'Create DamageWastage', 'url'=>array('create')),
	array('label'=>'Manage DamageWastage', 'url'=>array('admin')),
);
?>

<h1>Damage Wastages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
