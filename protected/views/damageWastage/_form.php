<script type="text/javascript" language="javascript">
$(function() 
{
	// focus to first item 
	$(".codeblur").focus();
	
	// Add new item rows after last item
	var id = 0;
	$(".codeblur").blur(function() 
	{
		var inputId = $(this).attr("id");
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		$("#ajax_loaderCode"+inputId).show("slow");
		
		var input = $(this);
		var code  = input.val();
		var master = $("table.bordercolumn");				
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/productByCode/";
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: urlajax,
			data: 
			{ 
				code : code,
			},
			success: function(data) 
			{
				$("#ajax_loaderCode"+inputId).hide();
				
				if(data=="null") {}
				else if(data=="invalid") 
				{
					input.val("");
					input.focus();
					$("#ajax_errorCode"+inputId).html("Invalid Item Code !");	
				}
				else 
				{
					$("#ajax_errorCode"+inputId).html("");
								
					// quantity operations
					if(data.isWeighted=="yes") {
						var qtyId = "qtywt"; var qtyPlaceholder = "0.0";
						var type = 2;
					}
					else {
						var qtyId = "qty"; var qtyPlaceholder = "0";
						var type = 1;	
					}	
					
					// create dynamic Row for current row input
					if(inputId==lastRowId)
					{
						id++;
						// Get a new row based on the prototype row
						var prot = master.find(".parentRow").clone(true);			
						prot.attr("id", id)
						prot.attr("class", "chieldRow" + id)
						
						// loader error and srl processing
						prot.find(".ajax_loaderCode").attr("id", "ajax_loaderCode"+id);
						prot.find(".ajax_errorCode").attr("id", "ajax_errorCode"+id);
						
						prot.find(".ajax_loaderQty").attr("id", "ajax_loaderQty"+id);
						prot.find(".ajax_errorQty").attr("id", "ajax_errorQty"+id);
						
						prot.find(".codeblur").attr("id",id);
						prot.find(".srlNo").html(id+1);
						prot.find(".codeblur").attr("id",id);
						
						// item quantity next row		
						prot.find(".qtyblur").attr("name", "qty[" + id +"]"); 
						prot.find(".qtyblur").attr("id", "qty" + id); 
						prot.find(".qtyblur").attr("placeholder", 0); 
						input.val(data.itemCode); 
						
						// quantity current weighted or not
						$("#qty"+inputId).val(""); $("#qty"+inputId).attr("placeholder", qtyPlaceholder); 
						$("#qty"+inputId).attr("id", qtyId + inputId);  
						prot.find(".typeblur").attr("id", "type" + id); prot.find(".typeblur").attr("name", "type[" + id +"]"); $("#type"+inputId).val(type); 
						
						// item processing
						prot.find(".pk").attr("name", "pk[" + id +"]"); prot.find(".pk").attr("id", "pk" + id); $("#pk"+inputId).val(inputId);
						prot.find(".proId").attr("name", "proId[" + id +"]"); prot.find(".proId").attr("id", "proId" + id); $("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						prot.find(".itemName").attr("id", "itemName" + id); $("#itemName"+inputId).html(data.itemName);
                        prot.find(".currentStock").attr("id", "currentStock" + id); $("#currentStock"+inputId).html(data.currentStock);
						prot.find(".costPrice").attr("id", "costPrice" + id); $("#costPrice"+inputId).html(data.costPrice); 
						prot.find(".costPriceAmount").attr("id", "costPriceAmount" + id); prot.find(".costPriceAmount").attr("name", "costPriceAmount[" + id +"]"); $("#costPriceAmount"+inputId).val(data.costPrice);
						prot.find(".netTotal").attr("id", "netTotal" + id); $("#netTotal"+inputId).html(0.00);
						
						// final binding and clone with reset values
						prot.find(".codeblur").attr("value", "");
						prot.find(".itemName").html("");
						prot.find(".qtyblur").val("0");
						prot.find(".costPrice").html("0.00");
						prot.find(".netTotal").html("0.00");
						master.find("tbody").append(prot);
					}
					else  // last blank row
					{
						// item processing
						$("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						$("#itemName"+inputId).html(data.itemName);
						$("#costPrice"+inputId).html(data.costPrice); 
						$("#costPriceAmount"+inputId).val(data.costPrice);
						
						// quantity current weighted or not
						var qtyCurrentId = ( $("#qty"+inputId).attr("id") || $("#qtywt"+inputId).attr("id") );
						$("#"+qtyCurrentId).val(""); $("#"+qtyCurrentId).attr("placeholder", qtyPlaceholder); 
						$("#"+qtyCurrentId).attr("id", qtyId + inputId); 
						$("#type"+inputId).val(type); 
						
						// sum calculations
						var sumQty = 0;wq
						var sumWeight = 0;
						var sumPrice = 0;
						
						for(var i=0;i<=lastRowId;i++)
						{
							var pk = $("#pk"+i).val();
							if(i==pk) 
							{
								if($("#qty"+i).val()=="") var sumQtyId = 0;
								else if(typeof($("#qty"+i).val())==="undefined") {
									var sumQtyId = 0;
									var sumQtyWtId = $("#qtywt"+i).val();
								}
								else var sumQtyId = $("#qty"+i).val();
								
								if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
								else if(typeof($("#qtywt"+i).val())==="undefined") {
									var sumQtyId = $("#qty"+i).val();
									var sumQtyWtId = 0;
								}
								else var sumQtyWtId = $("#qtywt"+i).val();
					
								if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
								if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
								var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
								if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
								
								sumQty+=parseFloat(sumQtyId); 
								sumWeight+=parseFloat(sumQtyWtId);
								sumPrice+=totalQtyId*parseFloat($("#costPrice"+i).html());
							}
						}
						
						// value placement/update
						$("#grandQty").html(sumQty);
						$("#grandWeight").html(sumWeight);
						$("#totalAmount").html(sumPrice.toFixed(2));
	
					}
				}
			},
			error: function() {
				$("#ajax_errorCode"+inputId).html("Invalid Item Code !");
			}
		});
	});	
	
	// quantity operations		
	$(".qtyblur").keyup(function() 
	{
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		// sell price
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val(); if(qty=="") qty = 0;
		var sellPr = $("#costPrice"+qtyId).html();
		var netPrice = qty*sellPr;
		
		$("#ajax_loaderQty"+qtyId).show("slow");
		var input = $(this);
		
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/checkStockByItemId/";
		var itemId  = $("#proId"+qtyId).val();
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemId : itemId
			},
			success: function(data) 
			{
				$("#ajax_loaderQty"+qtyId).hide();
				
				// stock available
				if(parseFloat(data)>=parseFloat(qty))
				{
					$("#ajax_errorQty"+qtyId).html("");
					$("#netTotal"+qtyId).html(netPrice.toFixed(2));
			
					// sum calculations
					var lastRowId = $('table.bordercolumn tr:last').attr('id');
					for(var i=0;i<=lastRowId;i++)
					{
						var pk = $("#pk"+i).val();
						if(i==pk) 
						{
							if($("#qty"+i).val()=="") var sumQtyId = 0;
							else if(typeof($("#qty"+i).val())==="undefined") {
								var sumQtyId = 0;
								var sumQtyWtId = $("#qtywt"+i).val();
							}
							else var sumQtyId = $("#qty"+i).val();
							
							if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
							else if(typeof($("#qtywt"+i).val())==="undefined") {
								var sumQtyId = $("#qty"+i).val();
								var sumQtyWtId = 0;
							}
							else var sumQtyWtId = $("#qtywt"+i).val();
				
							if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
							if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
							var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
							if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
							
							sumQty+=parseFloat(sumQtyId); 
							sumWeight+=parseFloat(sumQtyWtId);
							sumPrice+=totalQtyId*parseFloat($("#costPrice"+i).html());
						}
					}
					
					// value placement/update
					$("#grandQty").html(sumQty);
					$("#grandWeight").html(sumWeight);
					$("#totalAmount").html(sumPrice.toFixed(2));
				}
				else {
					input.val("");
					input.focus();
					$("#ajax_errorQty"+qtyId).html("Stock is less than Quantity !");		
				}
			},
			error: function() {
				$("#ajax_errorQty"+qtyId).html("Stock is less than Quantity !");
			}
		});
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
	// Remove items functionality
	$("table.bordercolumn img.removeRow").live("click", function() 
	{
		var parentRow = $(this).parents("tr").attr('id');
		if(parentRow>0) // not parentRow
		{
			id--;
			$(this).parents("tr").remove();  
			
			// sum calculations
			var sumQty = 0;
			var sumWeight = 0;
			var sumPrice = 0;
			var lastRowId = $('table.bordercolumn tr:last').attr('id');
			
			for(var i=0;i<=lastRowId;i++)
			{
				var pk = $("#pk"+i).val();
				if(i==pk) 
				{
					if($("#qty"+i).val()=="") var sumQtyId = 0;
					else if(typeof($("#qty"+i).val())==="undefined") {
						var sumQtyId = 0;
						var sumQtyWtId = $("#qtywt"+i).val();
					}
					else var sumQtyId = $("#qty"+i).val();
					
					if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
					else if(typeof($("#qtywt"+i).val())==="undefined") {
						var sumQtyId = $("#qty"+i).val();
						var sumQtyWtId = 0;
					}
					else var sumQtyWtId = $("#qtywt"+i).val();
		
					if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
					if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
					var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
					if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
					
					sumQty+=parseFloat(sumQtyId); 
					sumWeight+=parseFloat(sumQtyWtId);
					sumPrice+=totalQtyId*parseFloat($("#costPrice"+i).html());
				}
			}
			
			// value placement/update
			$("#grandQty").html(sumQty);
			$("#grandWeight").html(sumWeight);
			$("#totalAmount").html(sumPrice.toFixed(2));
		}
		else
		{
			alert("First row can,t be removed !");	
		}
	});
	
	// use item from search fill with item code
	$("#useItems").click(function() 
	{
		var itemName  = $("#itemId").val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemCodeByTitle/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemName : itemName,
			},
			success: function(data) 
			{
				$("table.bordercolumn tr:last .codeblur").val(data);
				$("table.bordercolumn tr:last .codeblur").focus();
				$("#itemId").val("");
			},
			error: function() {}
		});
			
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Damage/Wastage</li>	
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'damage-wastage-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Damage/Wastage</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
                <div class="fileld_right" style="padding-top:0;">
                    <a class="btn btn-primary" data-toggle="modal" href="#search_info" style="margin-bottom:10px; font-size:12px;"><i class="icon-search"></i> Search Items</a>
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> Damage/Wastage Number</th>
								<th><i class="icon-bookmark"></i> Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>DW<?php echo date("Ymdhis"); echo $form->hiddenField($model,'dwNo', array('value'=>'DW'.date("Ymdhis")));?></td>
								<td><?php echo date("Y-m-d");?></td>
							</tr>
					   </tbody>
					 </table>
				</div><br />
				
			</div>
			
			<div id="search_info" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4>Search by item title : </h4>
				</div>
				<div class="modal-body">
					<?php $this->widget('CAutoComplete',array(
								 //'model'=>$model,
								 'id'=>'itemId',
								 //'attribute' => 'custId',
								 //name of the html field that will be generated
								 'name'=>'itemId', 
								 //replace controller/action with real ids
								 //'value'=>($model->custId)?$model->cust->name:'',
					
								 'url'=>array('ajax/autoCompleteItems'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",
								 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
							));	
						?>
						<button type="button" data-dismiss="modal" class="btn" id="useItems" style="margin-left:10px;">Use Items</button>
				</div>
			</div>
			
            <div class="row">
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th>Total Quantity</th>
                            <th>Total Weight</th>
                            <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="highlight">
                            	<div class="success"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandQty">0</span>
                            </td>
                            <td class="highlight">
                            	<div class="info"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="grandWeight">0</span>
                            </td>
                            <td class="highlight">
                            	<div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;
                                <span style="font-size:20px;" id="totalAmount">0.00</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
             
			<div class="row">
				<div style="height:400px; overflow:scroll;">
				<table class="table table-striped table-hover bordercolumn">
					<thead>
						<tr>
							<th>Sl. No</th>
							<th>Item Code</th>
							<th class="hidden-480">Description</th>
							<th class="hidden-480">Quantity</th>
                            <th class="hidden-480">Current Stock</th>
							<th class="hidden-480">Cost Price</th>
							<th>Net Amount</th>
							<th>Remove</th>
						</tr>
					</thead>
					<tbody>
						<tr class="parentRow" id="0">
							<td><span class="srlNo">1</span></td>
							<td class="hidden-480">
								<?php 
									 echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:100px; height:15px;'));
									 echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
									 echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
								?>
								<span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
									<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
								</span>
								<div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
							</td>
							<td><span id="itemName0" class="itemName">-</span></td>
							<td class="hidden-480">
								<?php echo CHtml::textField('qty[0]','',array('id'=>'qty0', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?>
								<?php echo CHtml::hiddenField('type[0]','',array('id'=>'type0', 'class'=>'typeblur'));?>
								<span id="ajax_loaderQty0" style="display:none;" class="ajax_loaderQty">
									<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
								</span>
								<div style="width:100%; color:red; font-size:11px;" id="ajax_errorQty0" class="ajax_errorQty"></div>
							</td>
                            <td class="hidden-480"><span id="currentStock0" class="currentStock">0</span></td>
							<td class="hidden-480">
								<span id="costPrice0" class="costPrice">0</span>
								<?php echo CHtml::hiddenField('costPriceAmount[0]',0, array('id'=>'costPriceAmount0','class'=>'costPriceAmount'));?>
							</td>
							<td><span id="netTotal0" class="netTotal">0</span></td>
							<td>
								<img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
							</td>
						</tr>  
					</tbody>
				</table>
				</div>
			</div>
        </div> 
    </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   