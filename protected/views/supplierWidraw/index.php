<?php
/* @var $this SupplierWidrawController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Supplier Widraws',
);

$this->menu=array(
	array('label'=>'Create SupplierWidraw', 'url'=>array('create')),
	array('label'=>'Manage SupplierWidraw', 'url'=>array('admin')),
);
?>

<h1>Supplier Widraws</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
