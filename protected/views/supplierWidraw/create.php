<?php
/* @var $this SupplierWidrawController */
/* @var $model SupplierWidraw */

$this->breadcrumbs=array(
	'Supplier Widraws'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SupplierWidraw', 'url'=>array('index')),
	array('label'=>'Manage SupplierWidraw', 'url'=>array('admin')),
);
?>

<h1>Create SupplierWidraw</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>