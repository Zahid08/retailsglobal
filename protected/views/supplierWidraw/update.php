<?php
/* @var $this SupplierWidrawController */
/* @var $model SupplierWidraw */

$this->breadcrumbs=array(
	'Supplier Widraws'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SupplierWidraw', 'url'=>array('index')),
	array('label'=>'Create SupplierWidraw', 'url'=>array('create')),
	array('label'=>'View SupplierWidraw', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SupplierWidraw', 'url'=>array('admin')),
);
?>

<h1>Update SupplierWidraw <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>