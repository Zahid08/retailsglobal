<script type="text/javascript" language="javascript">	
	// JQUERY STARTED
	$(document).ready(function() 
	{
		// switch deo types
		$(".isBank").click(function() 
		{
			var type = $(this).val();
			if(type==1) $("#bankRow").show("slow");
			else $("#bankRow").hide("slow");
		});
  }); 
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Finance <i class="icon-angle-right"></i></li>
	<li>Payment</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'supplier-widraw-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) : echo $msg; else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">Consigment Supplier Payment</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'supId'); ?>
					<?php echo $form->dropDownList($model,'supId', Supplier::getAllSupplierByType(Supplier::CONSIG_SUPP), 
							   array('id'=>'supplierId','class'=>'m-wrap combo combobox','prompt'=>'Select Supplier',
									 'onchange'=>'js:$("#ajax_loadersupplier").show()',
									  'ajax' => array('type'=>'POST', 
														//'dataType'=>'json', 
														'url'=>CController::createUrl('ajax/paymentSupplierInfoConSig'),
														'success'=>'function(data) 
														{
															$("#ajax_loadersupplier").hide();
															$("#supplier_info_credit").html(data);
														}',
										)   
								 )); ?>
					<span id="ajax_loadersupplier" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
					<?php echo $form->error($model,'supId'); ?>
				</div>
				<div class="fileld_right" id="supplier_info_credit">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> Payment Date</th>
								<th><i class="icon-bookmark"></i> Consig. Supplier Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo date("Y-m-d");?></td>
								<td>0</td>
							</tr>
					   </tbody>
					 </table>
				</div>
			</div>
            <div class="row">
                <?php echo $form->radioButtonList($model,'isBank',array(2=>'Cash',1=>'Bank'),array('class'=>'isBank','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
            </div>
            <div id="bankRow" class="row" style="display:none;">
                <div class="row">
                    <?php echo $form->labelEx($model,'bankId'); ?>
                    <?php echo $form->dropDownList($model,'bankId',Bank::getAllBankName(Bank::STATUS_ACTIVE),array('class'=>'m-wrap large','style'=>'width:417px !important;','empty'=>'Select Bank')); ?>
                    <?php echo $form->error($model,'bankId'); ?>
                </div> 
                <div class="row">
                    <?php echo $form->labelEx($model,'bankAcNo'); ?>
                    <?php 
                        $this->widget('CAutoComplete',array(
                             'model'=>$model,
                             'id'=>'bankAcNo',
                             'attribute' => 'bankAcNo',
                             //name of the html field that will be generated
                             //'name'=>'poId', 
                                         //replace controller/action with real ids
                             //'value'=>'',

                             'url'=>array('ajax/autoCompleteBankAccountsNo'), 
                             'max'=>100, //specifies the max number of items to display

                                         //specifies the number of chars that must be entered 
                                         //before autocomplete initiates a lookup
                             'minChars'=>2, 
                             'delay'=>500, //number of milliseconds before lookup occurs
                             'matchCase'=>false, //match case when performing a lookup?
                             'mustMatch' =>false,
                                         //any additional html attributes that go inside of 
                                         //the input field can be defined here
                             'htmlOptions'=>array('class'=>'m-wrap span','style'=>'width:417px;','maxlength'=>20),  

                             'extraParams' => array('bankId' => 'js:function() { return $("#SupplierWidraw_bankId").val(); }'),
                             //'extraParams' => array('taskType' => 'desc'),
                             'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
                        ));
                    ?>
                    <?php echo $form->error($model,'bankAcNo'); ?>
                </div> 
            </div>
			<div class="row">
				<?php echo $form->labelEx($model,'amount'); ?>
				<?php echo $form->textField($model,'amount',array('style'=>'width:417px;','class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'amount'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'remarks'); ?>
				<?php echo $form->textArea($model,'remarks', array('class'=>'m-wrap large','style'=>'background:#fff;width:417px;height:150px;')); ?>
				<?php echo $form->error($model,'remarks'); ?>
			</div>
		</div>        
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   