<?php
$this->breadcrumbs=array(
	'Controllers'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Controllers', 'url'=>array('index')),
	array('label'=>'Create Controllers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('controllers-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Controllers</h1>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'controllers-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(            
            'name'=>'name',
            'value'=>'$data->name',
        ),
		array(            
            'name'=>'status',
            'value'=>'Lookup::item("Status", $data->status)',
			'filter'=>Lookup::items("Status"),
        ),
		array(
			'class'=>'CButtonColumn',
			'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('controllers-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
