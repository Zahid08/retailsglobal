<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce <i class="icon-angle-right"></i></li>
    <li>SEO Meta Data <i class="icon-angle-right"></i></li>
    <li>Details SEO Meta Data # <?php echo $model->tagName; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(		
		'tagName',
		'description',
		'crAt',
        array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
        array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
        array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
	),
)); ?>
</div>
