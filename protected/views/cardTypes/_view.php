<?php
/* @var $this CardTypesController */
/* @var $data CardTypes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	*/ ?>

</div>