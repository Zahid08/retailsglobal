<?php
/* @var $this CardTypesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payment Methods',
);

$this->menu=array(
	array('label'=>'Create CardTypes', 'url'=>array('create')),
	array('label'=>'Manage CardTypes', 'url'=>array('admin')),
);
?>

<h1>Payment Methods</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
