<?php
/* @var $this CardTypesController */
/* @var $model CardTypes */

$this->breadcrumbs=array(
	'Payment Methods'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CardTypes', 'url'=>array('index')),
	array('label'=>'Manage CardTypes', 'url'=>array('admin')),
);
?>

<h1>Create CardTypes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>