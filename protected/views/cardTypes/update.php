<?php
/* @var $this CardTypesController */
/* @var $model CardTypes */

$this->breadcrumbs=array(
	'Payment Methods'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CardTypes', 'url'=>array('index')),
	array('label'=>'Create CardTypes', 'url'=>array('create')),
	array('label'=>'View CardTypes', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CardTypes', 'url'=>array('admin')),
);
?>

<h1>Update CardTypes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>