<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Advertisement<i class="icon-angle-right"></i></li>
    <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Advertisement</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'advertisement-form',
	'enableAjaxValidation'=>true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Advertisement</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			    <div class="row">
                <?php echo $form->labelEx($model,'position'); ?>
                <?php echo $form->dropDownList($model,'position', UsefulFunction::eshopAdvertisementposition(), array('class'=>'m-wrap large','prompt'=>'Select Position')); ?>
                <?php echo $form->error($model,'position'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'title'); ?>
                <?php echo $form->textField($model,'title',array('class'=>'m-wrap large','size'=>60,'maxlength'=>200)); ?>
                <?php echo $form->error($model,'title'); ?>
            </div>
              <div class="row">
                <?php echo $form->labelEx($model,'secondTitle'); ?>
                <?php echo $form->textField($model,'secondTitle',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'secondTitle'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textarea($model,'description',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'description'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'url'); ?>
                <?php echo $form->textField($model,'url',array('class'=>'m-wrap large','size'=>60,'maxlength'=>200)); ?>
                <?php echo $form->error($model,'url'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'button_name'); ?>
                <?php echo $form->textField($model,'button_name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>200)); ?>
                <?php echo $form->error($model,'button_name'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'startDate'); ?>
                <?php //echo $form->textField($model,'startDate',array('class'=>'m-wrap large')); ?>
                <?php
                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                        'model' => $model,
                        'attribute' => 'startDate',
                        'language' => 'en',
                        'theme' => 'startDate',
                        'options' => array(
                           // 'showOn' => 'both',             // also opens with a button
                            'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                            'showOtherMonths' => true,      // show dates in other months
                            'selectOtherMonths' => true,    // can seelect dates in other months
                            'changeYear' => true,           // can change year
                            'changeMonth' => true,          // can change month
                            'yearRange' => '2000:2099',     // range of year
                            'minDate' => '2000-01-01',      // minimum date
                            'maxDate' => '2099-12-31',      // maximum date
                            //'showButtonPanel' => true,      // show button panel
                        ),
                        'htmlOptions' => array(
                            'size' => '10',
                            'maxlength' => '10',
                            'class'=>'m-wrap large',
                            'style'=>'width: 333px !important',
                        ),
                    ));
                ?>
                <?php echo $form->error($model,'startDate'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'endDate'); ?>
                <?php //echo $form->textField($model,'endDate',array('class'=>'m-wrap large')); ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'endDate',
                    'language' => 'en',
                    'theme' => 'endDate',
                    'options' => array(
                        //'showOn' => 'both',             // also opens with a button
                        'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                        'showOtherMonths' => true,      // show dates in other months
                        'selectOtherMonths' => true,    // can seelect dates in other months
                        'changeYear' => true,           // can change year
                        'changeMonth' => true,          // can change month
                        'yearRange' => '2000:2099',     // range of year
                        'minDate' => '2000-01-01',      // minimum date
                        'maxDate' => '2099-12-31',      // maximum date
                        //'showButtonPanel' => true,      // show button panel
                    ),
                    'htmlOptions' => array(
                        'size' => '10',
                        'maxlength' => '10',
                        'class'=>'m-wrap large',
                        'style'=>'width: 333px !important',
                    ),
                ));
                ?>
                <?php echo $form->error($model,'endDate'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'image'); ?>
                <?php echo $form->fileField($model,'image',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'image'); ?>
                <?php
                     echo CHtml::image(Yii::app()->getBaseUrl(true).'/media/catalog/advertisement/'.$model->image,"",array("width"=>50))
                ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   