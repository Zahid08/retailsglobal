<?php
if(!empty($custModel->id)){
    ?>
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th>Customer</th>
            <th>Address</th>
            <th>Mobile</th>
            <th>Total Point</th>
            <?php if($custModel->custType==Customer::CUST_INSTITUTIONAL) echo '<th>Balance</th>';?>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo $custModel->title.'&nbsp;'.$custModel->name;?></td>
            <td class="highlight"><div class="info"></div>&nbsp;&nbsp;<?php echo $custModel->addressline.', '.$custModel->city;?></td>
            <td class="highlight"><div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;<?php echo $custModel->phone;?></td>
            <td class="highlight"><div class="warning"></div>&nbsp;&nbsp;<span id="loyalityBalance"><?php echo Customer::getCustomerLoyaltyBalance($custModel->id);?></span></td>
            <?php
            if($custModel->custType==Customer::CUST_INSTITUTIONAL) :
                echo CHtml::hiddenField('institutionalCust',Customer::CUST_INSTITUTIONAL, array('id'=>'institutionalCust')); ?>
                <td class="highlight"><div class="important"></div>&nbsp;&nbsp;<span id="customerBalance"><?php echo Customer::getCustomerBalance($custModel->id);?></span></td>
            <?php endif; echo CHtml::hiddenField('loyaltyRate',$custModel->custType0->loyaltyRate, array('id'=>'loyaltyRate'));?>

        </tr>
        </tbody>
    </table>
    <?php
}else{

   ?>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'customer-form',
        'enableAjaxValidation'=>true,
    )); ?>
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
        <tr>
            <th>Customer Name</th>
            <th>Address</th>
            <th>Mobile</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="highlight">
                <?php echo $form->hiddenField($custModel,'custId', array('id'=>'custId','class'=>'m-wrap large','readonly'=>'true', 'value'=>'CN'.date("Ymdhis"))); ?>
                <?php echo $form->textField($custModel,'name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255, 'placeholder'=>'Name')); ?>
            </td>
            <td class="highlight">
                <?php echo $form->textField($custModel,'addressline',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255, 'placeholder'=>'Address')); ?>
            </td>
            <td class="highlight">
                <?php echo $form->textField($custModel,'phone',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155, 'placeholder'=>'Mobile')); ?>
            </td>
        </tr>
        </tbody>
    </table>
    <?php $this->endWidget(); ?>
    <?php
}

?>
