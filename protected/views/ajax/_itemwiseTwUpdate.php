<script type="text/javascript" language="javascript">
$(function() 
{
    // quantity operations		
	$(".qtyblur").keyup(function() 
	{
		$("#showHideGrnTotal").hide();
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val(); if(qty=="") qty = 0;	
        var rQuantity = $("#rQuantity"+qtyId).html();
        var aQuantity = $("#aQuantity"+qtyId).html();
		var costPr = $("#costPrice"+qtyId).html();
		var netPrice = qty*costPr;

		$("#ajax_loaderQty"+qtyId).show("slow");
		var input = $(this);
		
        
        // stock not available
        if(parseFloat(qty)+parseFloat(rQuantity)>parseFloat(aQuantity))
        {	
            $("#ajax_loaderQty"+qtyId).hide();
            $("#netTotal"+qtyId).html(0);

            input.val("");
            input.focus();
            $("#ajax_errorQty"+qtyId).html("Quantity should be less or equal assigned !");
        }
        else 
        {
            $("#ajax_loaderQty"+qtyId).hide();
            $("#ajax_errorQty"+qtyId).html("");
            $("#ajax_errorQty"+qtyId).html("");
            $("#netTotal"+qtyId).html(netPrice);
        }
			
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
});
</script>
<?php if(!empty($twNoModel)) : ?>
<div class="row">
    <div class="fileld_right" style="padding-top:0;">
        <table class="table table-striped table-bordered table-advance table-hover">
            <thead>
                <tr>
                    <th><i class="icon-bookmark"></i> Temporary Worker</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo $twNoModel->wk->username;?></td>
                </tr>
           </tbody>
         </table>
    </div>
</div>
<?php endif;?>

<div class="row">
    <div style="height:400px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
        	<thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Qty</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $srl = $totalQty = 0;
			$totalAmount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++; 
                    $totalQty+=$data->qty;	
					$totalAmount+=$data->qty*$data->costPrice;
				?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                            <?php echo CHtml::hiddenField('costPrice['.$key.']',$data->costPrice, array());?>
                        </td>
                        <td class="hidden-480">
							<?php 
                                echo CHtml::hiddenField('id['.$key.']',$data->id, array('id'=>'id'.$key));
								echo CHtml::hiddenField('pk['.$key.']',$key, array());
                                echo CHtml::hiddenField('proId['.$key.']',$data->item->id, array('id'=>'proId'.$key));
								 
								if($data->item->isWeighted=='no') :
									echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
								else :
									 echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
								endif;
							?>
                            <span id="ajax_loaderQty<?php echo $key;?>" style="display:none;" class="ajax_loaderQty">
                                <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                            </span>
                            <div style="width:100%; color:red; font-size:11px;" id="ajax_errorQty<?php echo $key;?>" class="ajax_errorQty"></div>
                        </td>
                        <td><span id="netTotal<?php echo $key;?>"><?php echo $data->qty*$data->costPrice;?></span></td>
                    </tr>
                <?php endforeach; ?>
				<tr id="showHideGrnTotal">
                    <td colspan="4"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td><strong><?php echo $totalAmount;?></strong></td>
                </tr>
           <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>- </td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('rqty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td>0 </td>
                    <td>0 </td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    