<script type="text/javascript" language="javascript">
$(function() 
{
	// quantity operations
	$(".qtyblur").keyup(function() 
	{

		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');

		var qtyId = qtyIdArr[1];
		var qty   = $(this).val();

		var costPr = $("#costPrice"+qtyId).html();
		var netPrice = qty*costPr;
		$("#netTotal"+qtyId).html(netPrice);
		
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				// quantity
				var qtyval = $("#qty<?php echo $key;?>").val();
				if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
				else var qty = 0;

				sumQty+=parseFloat(qty);

				// weight
				var qtywtval = $("#qtywt<?php echo $key;?>").val();
				if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
				else var qtywt = 0;
				sumWeight+=parseFloat(qtywt);

				
				sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>

		$("#grandQty").html(sumQty);
		$("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight);
		$("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice);
		$("#totalPrice").val(sumPrice);
		var grandPrice = sumPrice - parseFloat($("#totalDiscount").val());
		$("#grandTotal").html(grandPrice);
	});
	
	// discount operations
	$(".discountblur").keyup(function() 
	{
		var discountPrArr = $(this).attr("id").split('discount');	
		var discountPrId = discountPrArr[1];
		var discountPr  = $(this).val();	
		
		var qty = ( $("#qty"+discountPrId).val() || $("#qtywt"+discountPrId).val() );
		var costPr = $("#costPrice"+discountPrId).html();
		
		var netPrice = (qty*costPr)*discountPr/100;
		$("#discountTotal"+discountPrId).html(netPrice.toFixed(2));
		
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				sumPrice+= parseFloat($("#discountTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>
		$("#grandDiscount").html(sumPrice.toFixed(2)); $("#totalDiscount").val(sumPrice.toFixed(2));
		
		var grandPrice = parseFloat($("#totalPrice").val()) - sumPrice;
		$("#grandTotal").html(grandPrice);
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
});
</script>
<div class="row">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
            <tr>
                <th>Supplier</th>
                <th>Address</th>
                <th>Payment Term</th>
                <th>Payment Mode</th>
                <th>Credit Days</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            	<td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo CHtml::hiddenField('supplierId',$poModel->supplier->id,array('id'=>'supplierId'));?><?php echo $poModel->supplier->name;?></td>
                <td class="highlight"><div class="info"></div>&nbsp;&nbsp;<?php echo $poModel->supplier->address;?></td>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo $poModel->supplier->crType;?></td>
                <td class="highlight"><div class="warning"></div>&nbsp;&nbsp;<?php echo $poModel->supplier->crType;?></td>
                <td class="highlight"><div class="important"></div>&nbsp;&nbsp;<?php echo $poModel->supplier->creditDay;?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Sell Price</th>
                    <th class="hidden-480">Received Quantity</th>
                    <th class="hidden-480">Disc %</th>
                    <th class="hidden-480">Disc Amount</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $srl = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) : $srl++; ?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480">
                            <?php 
								 echo $data->qty; 
                                 echo CHtml::hiddenField('pk['.$key.']',$key, array());
                                 echo CHtml::hiddenField('proId['.$key.']',$data->item->id, array());
                            ?>
                        </td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                            <?php echo CHtml::hiddenField('costPrice['.$key.']',$data->costPrice, array());?>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->sellPrice;?></td>
                        <td class="hidden-480">
							<?php 
								if($data->item->isWeighted=='no') :
									echo CHtml::textField('qty['.$key.']','',array('id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
								else :
									 echo CHtml::textField('qty['.$key.']','',array('id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
								endif;
							?>
                        </td>
                        <td class="hidden-480"><?php echo CHtml::textField('discount['.$key.']','',array('id'=>'discount'.$key, 'class'=>'discountblur','style'=>'width:100px; height:15px;','placeholder'=>0));?></td>
                        <td><span id="discountTotal<?php echo $key;?>">0</span></td>
                        <td><span id="netTotal<?php echo $key;?>">0</span></td>
                    </tr>
                <?php endforeach; 
            else : ?>
                <tr>
                    <td>#</td>
                    <td>Code here</td>
                    <td>Description here </td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('qty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480"><?php echo CHtml::textField('discount',0,array('style'=>'width:50px; height:15px;'));?></td>
                    <td>0</td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    