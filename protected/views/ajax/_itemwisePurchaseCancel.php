<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Sell Price</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Case Count</th>
                    <th class="hidden-480">Current Stock</th>
                    <th class="hidden-480">15 Days Movement</th>
                    <th>Net Amount</th>
                 </tr>
            </thead>
            <tbody>
            <?php
            $srl = 0;
			$totalQty = 0;
			$totalAmount = 0;
			$totalDiscount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++; 
					$totalQty+=$data->qty;	
					$totalAmount+=$data->qty*$data->costPrice;
				?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->sellPrice;?></td>
                        <td class="hidden-480">
							<?php echo $data->qty;?>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->caseCount;?></td>
                        <td class="hidden-480">
                            <?php echo Stock::getItemWiseStock($data->itemId); ?>
                        </td>
                        <td class="hidden-480">
                            <?php echo SalesDetails::get15DaysMovement($data->itemId);?>
                        </td>
                        <td><span id="netTotal<?php echo $key;?>"><?php echo $data->qty*$data->costPrice;?></span></td>
                    </tr>
                <?php endforeach; ?>
				<tr id="showHideGrnTotal">
                    <td colspan="5"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td class="hidden-480" colspan="3"><strong></strong></td>
                    <td><strong><?php echo $totalAmount;?></strong></td>
                </tr>
           <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>- </td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('rqty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480">0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    