<div class="row">
	<div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                  	<th class="hidden-480">Return Quantity</th>
                    <th class="hidden-480">Sold Quantity</th>
                    <th class="hidden-480">Sold Price</th>
                    <th class="hidden-480">Item Tax</th>
                    <th class="hidden-480">Item Discount</th>
                    <th class="hidden-480">Special Discount</th>
                    <th class="hidden-480">Instant Discount</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="hidden-480"><?php echo CHtml::textField('qty','',array('id'=>'qty', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?></td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td>0</td>
                </tr>
            </tbody>
        </table>
     </div>
</div>