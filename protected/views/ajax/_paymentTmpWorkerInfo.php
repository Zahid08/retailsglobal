<table class="table table-striped table-bordered table-advance table-hover">
    <thead>
        <tr>
            <th><i class="icon-bookmark"></i> Payment Date</th>
            <th><i class="icon-bookmark"></i> Balance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo date("Y-m-d");?></td>
            <td><?php echo TemporaryWorker::getWorkerBalance($wkId);?></td>
        </tr>
   </tbody>
 </table>