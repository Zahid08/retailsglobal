<div class="row">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Bar Code</th>
                <th>Brand</th>
                <th>Supplier</th>
                <th>Tax/vat</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo $itemModel->itemName;?></td>
                <td class="highlight"><div class="info"></div>&nbsp;&nbsp;<?php echo $itemModel->barCode;?></td>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo $itemModel->brand->name;?></td>
                <td class="highlight"><div class="warning"></div>&nbsp;&nbsp;<?php echo $itemModel->supplier->name;?></td>
                <td class="highlight"><div class="important"></div>&nbsp;&nbsp;<?php echo $itemModel->tax->taxRate;?> %</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="row">	
    <div class="span4">
        <?php echo CHtml::label('Cost Price',''); ?>
        <?php 
        if($itemModel->isParent==Items::IS_PARENTS)
            echo CHtml::textField('costPrice',$totalCostPrice,array('readonly'=>'readonly','class'=>'m-wrap large'));
        else echo CHtml::textField('costPrice',$totalCostPrice,array('class'=>'m-wrap large'));
        ?>
    </div>
    <div class="span4">
        <?php echo CHtml::label('Sell Price',''); ?>
        <?php echo CHtml::textField('sellPrice',$itemModel->sellPrice,array('class'=>'m-wrap large')); ?>
    </div>
</div>