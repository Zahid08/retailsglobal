<div class="row">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
            <tr>
                <th>Address</th>
                <th>Payment Term</th>
                <th>Payment Mode</th>
                <th>Credit Days</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="highlight"><div class="info"></div>&nbsp;-</td>
                <td class="highlight"><div class="success"></div>&nbsp;-</td>
                <td class="highlight"><div class="warning"></div>&nbsp;-</td>
                <td class="highlight"><div class="important"></div>&nbsp;-</td>
            </tr>
        </tbody>
    </table>
    
    <div style="height:600px; overflow:scroll;">
    <table class="table table-striped table-hover bordercolumn">
        <thead>
            <tr>
                <th>Sl. No</th>
                <th>Item Code</th>
                <th class="hidden-480">Description</th>
                <th class="hidden-480">Quantity</th>
                <th class="hidden-480">Cost Price</th>
                <th class="hidden-480">Sell Price</th>
                <th class="hidden-480">Case Count</th>
                <th class="hidden-480">Current Stock</th>
                <th class="hidden-480">15 Days Movement</th>
                <th>Net Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>#</td>
                <td>-</td>
                <td>-</td>
                <td class="hidden-480"><?php echo CHtml::textField('qty',0,array('style'=>'width:50px; height:15px;'));?></td>
                <td class="hidden-480">0<?php // echo CHtml::textField('costPrice',0,array('style'=>'width:100px; height:15px;'));?></td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td>0</td>
            </tr>
        </tbody>
    </table>
</div>   