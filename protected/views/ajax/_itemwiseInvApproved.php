<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
        	<thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Current Stock</th>
                    <th class="hidden-480">Cost Price</th>
                    <th>Net Amount</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $srl = 0;
			$totalQty = 0;
			$totalAmount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++; 
					$totalQty+=$data->qty;	
					$totalAmount+=$data->qty*$data->costPrice;
				?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480"><?php echo $data->qty;?>
                        </td>
                        <td><?php echo Stock::getItemWiseStock($data->item->id);?></td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                        </td>
                        <td><span id="netTotal<?php echo $key;?>"><?php echo $data->qty*$data->costPrice;?></span></td>
                        <td><?php echo $data->invDate;?></td>
                    </tr>
                <?php endforeach; ?>
				<tr id="showHideGrnTotal">
                    <td colspan="3"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td colspan="2"></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalAmount,2));?></strong></td>
                    <td></td>
                </tr>
           <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>- </td>
                    <td class="hidden-480"><?php echo CHtml::textField('qty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td>- </td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    