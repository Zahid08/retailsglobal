<script type="text/javascript" language="javascript">
$(function() 
{
	// quantity operations
	$(".qtyblur").keyup(function() 
	{

		$("#showHideInvTotal").hide();
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val();



		//$("#ajax_loaderQty"+qtyId).show("slow");
		var input = $(this);
		var sold  =$(this).data('quantity');
        
		// stock not available
		if(parseFloat(qty)>sold)
		{	
			$("#ajax_loaderQty"+qtyId).hide();
			$("#netTotal"+qtyId).html(0);
			
			input.val("");
			input.focus();
			$("#ajax_errorQty"+qtyId).html("Sold Qty is less than Quantity !");
		}
		else 
		{
			//$("#ajax_loaderQty"+qtyId).hide();
			$("#ajax_errorQty"+qtyId).html("");
			if(qty>0)
			{
                // get return qty wise value from form
				var salesPr = $("#salesPrice"+qtyId).html();
                var vatPr = qty*$("#vatInd"+qtyId).val();
				var disPr = qty*$("#discountInd"+qtyId).val();
                var spdisPr = qty*$("#spdiscountInd"+qtyId).val();
                var insdisPr = qty*$("#insdiscountInd"+qtyId).val();
                
                // set return qty wise value to form
                $("#vat"+qtyId).html(vatPr.toFixed(2));
				$("#discount"+qtyId).html(disPr.toFixed(2));
                $("#spdiscount"+qtyId).html(spdisPr.toFixed(2));
                $("#insdiscount"+qtyId).html(insdisPr.toFixed(2));
				
                // set return qty wise value to hiden field for database
                $("#vatPrice"+qtyId).val(vatPr.toFixed(2));
                $("#discountPrice"+qtyId).val(disPr.toFixed(2));
                $("#totalSpDiscount"+qtyId).val(spdisPr.toFixed(2));
                $("#totalInsDiscount"+qtyId).val(insdisPr.toFixed(2));
                
				var netPrice = (parseFloat(qty*salesPr)+vatPr) - (parseFloat(disPr)+parseFloat(spdisPr)+parseFloat(insdisPr));
				$("#netTotal"+qtyId).html(netPrice.toFixed(2));
				$("#netPrice"+qtyId).val(netPrice.toFixed(2));
			}
			
			var sumQty = 0;
			var sumWeight = 0;
			var sumPrice = 0;
			<?php foreach($itemModel as $key=>$data) : ?>
					// quantity
					var qtyval = $("#qty<?php echo $key;?>").val();
					if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
					else var qty = 0;
					sumQty+=parseFloat(qty);	
					
					// weight
					var qtywtval = $("#qtywt<?php echo $key;?>").val();
					if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
					else var qtywt = 0;
					sumWeight+=parseFloat(qtywt);	
					if(qtyval>0 || qtywtval>0)
						sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());		
			<?php endforeach; ?>
			$("#grandQty").html(sumQty); 
			$("#totalWeight").html(sumWeight);
			$("#grandTotal").html(sumPrice.toFixed(2));
		}
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
});
</script>
<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                  	<th class="hidden-480">Return Quantity</th>
                    <th class="hidden-480">Sold Quantity</th>
                    <th class="hidden-480">Sold Price</th>
                    <th class="hidden-480">Item Tax</th>
                    <th class="hidden-480">Item Discount</th>
                    <th class="hidden-480">Special Discount</th>
                    <th class="hidden-480">Instant Discount</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
			echo CHtml::hiddenField('invId', $invModel->id,array());
            $srl = $totalQty = $totalAmount = $netAmount = $totalVat = 0;
			$totalDiscount = $totalSpDiscount = $totalInsDiscount = $totalNetAmount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++;
					$totalQty+=$data->qty;
					$totalAmount+=$data->qty*$data->salesPrice;
                    $totalVat+=$data->vatPrice;
					$totalDiscount+=$data->discountPrice;
                    
                    // get special discount and instant discount
                    $spDiscount = ($invModel->spDiscount>0)?$data->vatPrice:0;
                    $insDiscount = SalesDetails::getInvoiceItemWiseInstantDiscount($invModel->id,$data->qty);
                    $totalSpDiscount+=$spDiscount;
                    $totalInsDiscount+=$insDiscount;

					$netAmount = (($data->qty*$data->salesPrice)+$data->vatPrice) - ($data->discountPrice+$spDiscount+$insDiscount);
					$totalNetAmount+=$netAmount;?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->item->itemCode;?></td>
                    <td><?php echo $data->item->itemName;?></td>
                    <td class="hidden-480">
                        <?php 
                             echo CHtml::hiddenField('pk['.$key.']',$key, array());
                             echo CHtml::hiddenField('proId['.$key.']',$data->item->id, array());

                             if($data->item->isWeighted=='no') :
                                echo CHtml::textField('qty['.$key.']','',array('id'=>'qty'.$key,'data-quantity'=>$data->qty,'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
                             else :
                                 echo CHtml::textField('qty['.$key.']','',array('id'=>'qtywt'.$key,'data-quantity'=>$data->qty, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
                             endif;

                        echo CHtml::checkBox('is_defect['.$key.']',false,array('style'=>'margin-left:10px;','value' => '1', 'uncheckValue'=>'0'));echo 'Is Defect ?'; //new line
                        echo CHtml::hiddenField('costPrice['.$key.']', $data->item->costPrice, array('id'=>'sold'.$key));  //new line
                        ?>
                        <span id="ajax_loaderQty<?php echo $key;?>" style="display:none;" class="ajax_loaderQty">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                        </span>
                        <div style="width:100%; color:red; font-size:11px;" id="ajax_errorQty<?php echo $key;?>" class="ajax_errorQty"></div>
                    </td>
                    <td>
                        <?php 
                            echo $data->qty;
                            echo CHtml::hiddenField('sold['.$key.']', $data->qty, array('id'=>'sold'.$key));
                        ?>
                    </td>
                    <td class="hidden-480"> 
                        <span id="salesPrice<?php echo $key;?>"><?php echo $data->salesPrice;?></span> 
                    </td>
                    <td class="hidden-480"><span id="vat<?php echo $key;?>"><?php echo $data->vatPrice;?></span><?php echo CHtml::hiddenField('vat['.$key.']',$data->vatPrice/$data->qty, array('id'=>'vatInd'.$key));?></td>
                    <td class="hidden-480"><span id="discount<?php echo $key;?>"><?php echo $data->discountPrice;?></span><?php echo CHtml::hiddenField('discount['.$key.']',$data->discountPrice/$data->qty, array('id'=>'discountInd'.$key));?></td>  

                    <td class="hidden-480"><span id="spdiscount<?php echo $key;?>"><?php echo $spDiscount;?></span><?php echo CHtml::hiddenField('discount['.$key.']',$spDiscount/$data->qty, array('id'=>'spdiscountInd'.$key));?></td>  
                    <td class="hidden-480"><span id="insdiscount<?php echo $key;?>"><?php echo $insDiscount;?></span><?php echo CHtml::hiddenField('discount['.$key.']',$insDiscount/$data->qty, array('id'=>'insdiscountInd'.$key));?></td>

                    <td>
                        <span id="netTotal<?php echo $key;?>"><?php echo $netAmount;?></span>
                        <?php echo CHtml::hiddenField('netPrice['.$key.']',$netAmount, array('id'=>'netPrice'.$key));?>
                        <?php echo CHtml::hiddenField('vatPrice['.$key.']',$data->vatPrice, array('id'=>'vatPrice'.$key));?>
                        <?php echo CHtml::hiddenField('discountPrice['.$key.']',$data->discountPrice, array('id'=>'discountPrice'.$key));?>
                        <?php echo CHtml::hiddenField('totalSpDiscount['.$key.']',$spDiscount, array('id'=>'totalSpDiscount'.$key));?>
                        <?php echo CHtml::hiddenField('totalInsDiscount['.$key.']',$insDiscount, array('id'=>'totalInsDiscount'.$key));?>
                    </td>
                </tr>
                <?php endforeach; ?>
				<tr id="showHideInvTotal">
                    <td colspan="4"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td class="hidden-480"><strong><?php echo UsefulFunction::formatMoney(round($totalAmount,2),true);?></strong></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalVat,2),true);?></strong></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalDiscount,2),true);?></strong></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalSpDiscount,2),true);?></strong></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalInsDiscount,2),true);?></strong></td>
                    <td><strong><?php echo UsefulFunction::formatMoney(round($totalNetAmount,2),true);?></strong></td>
                </tr>
           <?php else : ?>
                 <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="hidden-480"><?php echo CHtml::textField('qty','',array('id'=>'qty', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?></td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    