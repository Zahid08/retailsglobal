<script type="text/javascript" language="javascript">
$(function() 
{
    // default qty,weight,amount set
    $("#grandQty").html(<?php echo $grnNoModel->totalQty;?>); $("#totalQty").val(<?php echo $grnNoModel->totalQty;?>);
    $("#grandWeight").html(<?php echo $grnNoModel->totalWeight;?>); $("#totalWeight").val(<?php echo $grnNoModel->totalWeight;?>);
    $("#totalAmount").html(<?php echo round($grnNoModel->totalPrice,2);?>); $("#totalPrice").val(<?php echo round($grnNoModel->totalPrice,2);?>);
    $("#grandDiscount").html(<?php echo round($grnNoModel->totalDiscount,2);?>); $("#totalDiscount").val(<?php echo round($grnNoModel->totalDiscount,2);?>);
    $("#grandTotal").html(<?php echo round(($grnNoModel->totalPrice-$grnNoModel->totalDiscount),2);?>);

	// quantity operations blur
	$(".qtyblur").blur(function() 
	{
		$("#showHideGrnTotal").hide();
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val();	
		var costPr = $("#costPrice"+qtyId).html();
		var netPrice = qty*costPr;
		$("#netTotal"+qtyId).html(netPrice);
		
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				// quantity
				var qtyval = $("#qty<?php echo $key;?>").val();
				if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
				else var qty = 0;
				sumQty+=parseFloat(qty);	
				
				// weight
				var qtywtval = $("#qtywt<?php echo $key;?>").val();
				if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
				else var qtywt = 0;
				sumWeight+=parseFloat(qtywt);	
				
				sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
		var grandPrice = sumPrice - parseFloat($("#totalDiscount").val());
		$("#grandTotal").html(grandPrice.toFixed(2));
	});

    // quantity operations keyup
    $(".qtyblur").keyup(function()
    {
        $("#showHideGrnTotal").hide();
        var isWeight = $(this).attr('placeholder');
        if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');
        else var qtyIdArr = $(this).attr("id").split('qty');

        var qtyId = qtyIdArr[1];
        var qty   = $(this).val();
        var costPr = $("#costPrice"+qtyId).html();
        var netPrice = qty*costPr;
        $("#netTotal"+qtyId).html(netPrice);

        var sumQty = 0;
        var sumWeight = 0;
        var sumPrice = 0;
        <?php foreach($itemModel as $key=>$data) : ?>
        // quantity
        var qtyval = $("#qty<?php echo $key;?>").val();
        if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
        else var qty = 0;
        sumQty+=parseFloat(qty);

        // weight
        var qtywtval = $("#qtywt<?php echo $key;?>").val();
        if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
        else var qtywt = 0;
        sumWeight+=parseFloat(qtywt);

        sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());
        <?php endforeach; ?>
        $("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
        $("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
        $("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
        var grandPrice = sumPrice - parseFloat($("#totalDiscount").val());
        $("#grandTotal").html(grandPrice.toFixed(2));
    });
	
	// discount operations blur
	$(".discountblur").blur(function() 
	{
		$("#showHideGrnTotal").hide();
		
		var discountPrArr = $(this).attr("id").split('discount');	
		var discountPrId = discountPrArr[1];
		var discountPr  = $(this).val();

		var isWeight = $("#qty"+discountPrId).attr('placeholder') || $("#qtywt"+discountPrId).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $("#qtywt"+discountPrId).attr("id").split('qtywt');	
		else var qtyIdArr = $("#qty"+discountPrId).attr("id").split('qty');
		
		var qty = ( $("#qty"+discountPrId).val() || $("#qtywt"+discountPrId).val() );
		var costPr = $("#costPrice"+discountPrId).html();
		var netDiscountPrice = (qty*costPr)*discountPr/100;
		$("#discountTotal"+discountPrId).html(netDiscountPrice);
		
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		var sumDiscount = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				// quantity
				var qtyval = $("#qty<?php echo $key;?>").val();
				if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
				else var qty = 0;
				sumQty+=parseFloat(qty);	
				
				// weight
				var qtywtval = $("#qtywt<?php echo $key;?>").val();
				if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
				else var qtywt = 0;
				sumWeight+=parseFloat(qtywt);	
				
				sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());	
				sumDiscount+= parseFloat($("#discountTotal<?php echo $key;?>").html());
				
		<?php endforeach; ?>
		
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
		
		$("#grandDiscount").html(sumDiscount.toFixed(2)); $("#totalDiscount").val(sumDiscount.toFixed(2));
		var grandPrice = sumPrice - sumDiscount;
		$("#grandTotal").html(grandPrice.toFixed(2));
	});

    // discount operations keyup
    $(".discountblur").keyup(function()
    {
        $("#showHideGrnTotal").hide();

        var discountPrArr = $(this).attr("id").split('discount');
        var discountPrId = discountPrArr[1];
        var discountPr  = $(this).val();

        var isWeight = $("#qty"+discountPrId).attr('placeholder') || $("#qtywt"+discountPrId).attr('placeholder');
        if(isWeight=='0.0') var qtyIdArr = $("#qtywt"+discountPrId).attr("id").split('qtywt');
        else var qtyIdArr = $("#qty"+discountPrId).attr("id").split('qty');

        var qty = ( $("#qty"+discountPrId).val() || $("#qtywt"+discountPrId).val() );
        var costPr = $("#costPrice"+discountPrId).html();
        var netDiscountPrice = (qty*costPr)*discountPr/100;
        $("#discountTotal"+discountPrId).html(netDiscountPrice);

        var sumQty = 0;
        var sumWeight = 0;
        var sumPrice = 0;
        var sumDiscount = 0;
        <?php foreach($itemModel as $key=>$data) : ?>
        // quantity
        var qtyval = $("#qty<?php echo $key;?>").val();
        if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
        else var qty = 0;
        sumQty+=parseFloat(qty);

        // weight
        var qtywtval = $("#qtywt<?php echo $key;?>").val();
        if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
        else var qtywt = 0;
        sumWeight+=parseFloat(qtywt);

        sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());
        sumDiscount+= parseFloat($("#discountTotal<?php echo $key;?>").html());

        <?php endforeach; ?>

        $("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
        $("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
        $("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));

        $("#grandDiscount").html(sumDiscount.toFixed(2)); $("#totalDiscount").val(sumDiscount.toFixed(2));
        var grandPrice = sumPrice - sumDiscount;
        $("#grandTotal").html(grandPrice.toFixed(2));
    });
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
});
</script>
<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Sell Price</th>
                    <th class="hidden-480">Received Quantity</th>
                    <th class="hidden-480">Disc %</th>
                    <th class="hidden-480">Disc Amount</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
            $srl = 0;
			$totalQty = 0;
			$totalAmount = 0;
			$totalDiscount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++; 
					$discount = ($data->qty*$data->costPrice)*$data->discountPercent/100;
					$totalQty+=$data->qty;	
					$totalDiscount+=$discount;
					$totalAmount+=$data->qty*$data->costPrice;
				?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480">
                            <?php 
								 echo $data->qty; 
                                 echo CHtml::hiddenField('pk['.$key.']',$key, array());
                                 echo CHtml::hiddenField('proId['.$key.']',$data->item->id, array());
                            ?>
                        </td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                            <?php echo CHtml::hiddenField('costPrice['.$key.']',$data->costPrice, array());?>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->sellPrice;?></td>
                        <td class="hidden-480">
				        <?php 
                            /*if($data->item->isParent==Items::IS_PARENTS) : // parent has
								if($data->item->isWeighted=='no') :
									echo CHtml::textField('qty['.$key.']',$data->qty,array('readonly'=>'readonly','id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
								else :
									 echo CHtml::textField('qty['.$key.']',$data->qty,array('readonly'=>'readonly','id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
								endif;
                            else :*/
                            if($data->item->isWeighted=='no') :
                                echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
                            else :
                                 echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
                            endif;
                            //endif;
				        ?>
                        </td>
                        <td class="hidden-480">
                        <?php 
                        /*if($data->item->isParent==Items::IS_PARENTS)  // parent has
                            echo CHtml::textField('discount['.$key.']',$data->discountPercent,array('readonly'=>'readonly','id'=>'discount'.$key, 'class'=>'discountblur','style'=>'width:100px; height:15px;','placeholder'=>0));
                        else*/
                            echo CHtml::textField('discount['.$key.']',$data->discountPercent,array('id'=>'discount'.$key, 'class'=>'discountblur','style'=>'width:100px; height:15px;','placeholder'=>0));?>
                        </td>
                        <td><span id="discountTotal<?php echo $key;?>"><?php echo $discount;?></span></td>
                        <td><span id="netTotal<?php echo $key;?>"><?php echo $data->qty*$data->costPrice;?></span></td>
                    </tr>
                <?php endforeach; ?>
				<tr id="showHideGrnTotal">
                    <td colspan="6"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td class="hidden-480"><strong></strong></td>
                    <td><strong><?php echo $totalDiscount;?></strong></td>
                    <td><strong><?php echo $totalAmount;?></strong></td>
                </tr>
           <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>Code here</td>
                    <td>Description here </td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('qty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480"><?php echo CHtml::textField('discount',0,array('style'=>'width:50px; height:15px;'));?></td>
                    <td>0</td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    