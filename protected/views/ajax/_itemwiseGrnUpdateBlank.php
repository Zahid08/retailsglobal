<div class="row">
	<div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Sell Price</th>
                    <th class="hidden-480">Received Quantity</th>
                    <th class="hidden-480">Disc %</th>
                    <th class="hidden-480">Disc Amount</th>
                    <th>Net Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>-</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('rqty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480"><?php echo CHtml::textField('discount',0,array('style'=>'width:50px; height:15px;'));?></td>
                    <td>0</td>
                    <td>0</td>
                </tr>
            </tbody>
        </table>
     </div>
</div>