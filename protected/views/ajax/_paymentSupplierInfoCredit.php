<table class="table table-striped table-bordered table-advance table-hover">
    <thead>
        <tr>
            <th><i class="icon-bookmark"></i> Payment GRN Date</th>
            <th><i class="icon-bookmark"></i> Supplier Credit Balance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo date("Y-m-d");?></td>
            <td><?php echo Supplier::getSupplierBalance($suppModel->id);?></td>
        </tr>
   </tbody>
 </table>