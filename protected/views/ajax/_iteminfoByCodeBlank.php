<div class="row">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
            <tr>
                <th>Item Name</th>
                <th>Bar Code</th>
                <th>Brand</th>
                <th>Supplier</th>
                <th>Tax/vat</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;-</td>
                <td class="highlight"><div class="info"></div>&nbsp;&nbsp;-</td>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;-</td>
                <td class="highlight"><div class="warning"></div>&nbsp;&nbsp;-</td>
                <td class="highlight"><div class="important"></div>&nbsp;&nbsp;0 %</td>
            </tr>
        </tbody>
    </table>
</div>

<div class="row">	
    <div class="span4">
        <?php echo CHtml::label('Cost Price',''); ?>
        <?php echo CHtml::textField('costPrice','',array('class'=>'m-wrap large')); ?>
    </div>
    <div class="span4">
        <?php echo CHtml::label('Sell Price',''); ?>
        <?php echo CHtml::textField('sellPrice','',array('class'=>'m-wrap large')); ?>
    </div>
</div>