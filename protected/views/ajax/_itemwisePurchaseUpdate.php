<script type="text/javascript" language="javascript">
$(function() 
{
    // default qty,weight,amount set
    $("#grandQty").html(<?php echo $poNoModel->totalQty;?>); $("#totalQty").val(<?php echo $poNoModel->totalQty;?>);
    $("#grandWeight").html(<?php echo $poNoModel->totalWeight;?>); $("#totalWeight").val(<?php echo $poNoModel->totalWeight;?>);
    $("#totalAmount").html(<?php echo round($poNoModel->totalPrice,2);?>); $("#totalPrice").val(<?php echo round($poNoModel->totalPrice,2);?>);

	// quantity operations blur
	$(".qtyblur").blur(function() 
	{
		$("#showHideGrnTotal").hide();
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val();	
		var costPr = $("#costPrice"+qtyId).html();
		var netPrice = qty*costPr;
		$("#netTotal"+qtyId).html(netPrice);
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				// quantity
				var qtyval = $("#qty<?php echo $key;?>").val();
				if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
				else var qty = 0;
				sumQty+=parseFloat(qty);	
				
				// weight
				var qtywtval = $("#qtywt<?php echo $key;?>").val();
				if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
				else var qtywt = 0;
				sumWeight+=parseFloat(qtywt);	
				
				sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
	});

    // quantity operations keyup
    $(".qtyblur").keyup(function()
    {
        $("#showHideGrnTotal").hide();
        var isWeight = $(this).attr('placeholder');
        if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');
        else var qtyIdArr = $(this).attr("id").split('qty');

        var qtyId = qtyIdArr[1];
        var qty   = $(this).val();
        var costPr = $("#costPrice"+qtyId).html();
        var netPrice = qty*costPr;
        $("#netTotal"+qtyId).html(netPrice);
        var sumQty = 0;
        var sumWeight = 0;
        var sumPrice = 0;
        <?php foreach($itemModel as $key=>$data) : ?>
        // quantity
        var qtyval = $("#qty<?php echo $key;?>").val();
        if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
        else var qty = 0;
        sumQty+=parseFloat(qty);

        // weight
        var qtywtval = $("#qtywt<?php echo $key;?>").val();
        if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
        else var qtywt = 0;
        sumWeight+=parseFloat(qtywt);

        sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());
        <?php endforeach; ?>
        $("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
        $("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
        $("#totalAmount").html(sumPrice.toFixed(2)); $("#totalPrice").val(sumPrice.toFixed(2));
    });
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	
});
</script>
<div class="row">
    <div style="height:600px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th class="hidden-480">Description</th>
                    <th class="hidden-480">Cost Price</th>
                    <th class="hidden-480">Sell Price</th>
                    <th class="hidden-480">Quantity</th>
                    <th class="hidden-480">Case Count</th>
                    <th class="hidden-480">Current Stock</th>
                    <th class="hidden-480">15 Days Movement</th>
                    <th>Net Amount</th>
                 </tr>
            </thead>
            <tbody>
            <?php
            $srl = 0;
			$totalQty = 0;
			$totalAmount = 0;
            if(!empty($itemModel)) :
                foreach($itemModel as $key=>$data) :
				 	$srl++; 
					$totalQty+=$data->qty;	
					$totalAmount+=$data->qty*$data->costPrice;
				?>
                    <tr>
                        <td><?php echo $srl;?></td>
                        <td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $data->item->itemName;?></td>
                        <td class="hidden-480"> 
                            <span id="costPrice<?php echo $key;?>"><?php echo $data->costPrice;?></span>
                            <?php echo CHtml::hiddenField('costPrice['.$key.']',$data->costPrice, array());?>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->sellPrice;?></td>
                        <td class="hidden-480">
							<?php 
								echo CHtml::hiddenField('pk['.$key.']',$key, array());
                                echo CHtml::hiddenField('proId['.$key.']',$data->item->id, array());
								 
								if($data->item->isWeighted=='no') :
									echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0'));
								else :
									 echo CHtml::textField('qty['.$key.']',$data->qty,array('id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:100px; height:15px;','placeholder'=>'0.0'));
								endif;
							?>
                        </td>
                        <td class="hidden-480"><?php echo $data->item->caseCount;?></td>
                        <td class="hidden-480">
                            <?php echo Stock::getItemWiseStock($data->itemId); ?>
                        </td>
                        <td class="hidden-480">
                            <?php echo SalesDetails::get15DaysMovement($data->itemId);?>
                        </td>
                        <td><span id="netTotal<?php echo $key;?>"><?php echo $data->qty*$data->costPrice;?></span></td>
                    </tr>
                <?php endforeach; ?>
				<tr id="showHideGrnTotal">
                    <td colspan="5"><strong>Total : </strong></td>
                    <td class="hidden-480"><strong><?php echo $totalQty;?></strong></td>
                    <td class="hidden-480" colspan="3"><strong></strong></td>
                    <td><strong><?php echo $totalAmount;?></strong></td>
                </tr>
           <?php else : ?>
                <tr>
                    <td>#</td>
                    <td>-</td>
                    <td>- </td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480">0</td>
                    <td class="hidden-480"><?php echo CHtml::textField('rqty',0,array('style'=>'width:100px; height:15px;'));?></td>
                    <td class="hidden-480">0</td>
                    <td>0</td>
                    <td>0</td>
                    <td>0</td>
                </tr>
        <?php endif;?>
             </tbody>
        </table>
	</div> 
</div>    