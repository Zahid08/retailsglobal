<script type="text/javascript" language="javascript">
$(function() 
{
	// quantity operations
	$(".qtyblur").keyup(function() 
	{
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val();	
		var costPr = $("#costPrice"+qtyId).val();
		var netPrice = qty*costPr;
		$("#netTotal"+qtyId).html(netPrice);
		
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>
				// quantity
				var qtyval = $("#qty<?php echo $key;?>").val();
				if($.isNumeric(qtyval)) var qty = $("#qty<?php echo $key;?>").val();
				else var qty = 0;
				sumQty+=parseFloat(qty);	
				
				// weight
				var qtywtval = $("#qtywt<?php echo $key;?>").val();
				if($.isNumeric(qtywtval)) var qtywt = $("#qtywt<?php echo $key;?>").val();
				else var qtywt = 0;
				sumWeight+=parseFloat(qtywt);	
				
				sumPrice+=parseFloat($("#netTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>
		$("#grandQty").html(sumQty); $("#totalQty").val(sumQty);
		$("#grandWeight").html(sumWeight); $("#totalWeight").val(sumWeight);
		$("#grandTotal").html(sumPrice); $("#totalPrice").val(sumPrice);
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});

	// costPrice operations
	<?php /*?>$(".costPriceblur").blur(function() 
	{
		var costPrArr = $(this).attr("id").split('costPrice');	
		var costPrId = costPrArr[1];
		var costPr  = $(this).val();	
		var qty  	= $("#qty"+costPrId).val();
		var netPrice = qty*costPr;
		$("#netTotal"+costPrId).html(netPrice);
		
		var sumPrice = 0;
		<?php foreach($itemModel as $key=>$data) : ?>	
				sumPrice+= parseFloat($("#netTotal<?php echo $key;?>").html());	
		<?php endforeach; ?>
		$("#grandTotal").html(sumPrice); $("#totalPrice").val(sumPrice);
	});<?php */?>
	
});
</script>
<div class="row">
    <table class="table table-striped table-bordered table-advance table-hover">
        <thead>
            <tr>
                <th>Address</th>
                <th>Payment Term</th>
                <th>Payment Mode</th>
                <th>Credit Days</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="highlight"><div class="info"></div>&nbsp;&nbsp;<?php echo $suppModel->address;?></td>
                <td class="highlight"><div class="success"></div>&nbsp;&nbsp;<?php echo $suppModel->crType;?></td>
                <td class="highlight"><div class="warning"></div>&nbsp;&nbsp;<?php echo $suppModel->crType;?></td>
                <td class="highlight"><div class="important"></div>&nbsp;&nbsp;<?php echo $suppModel->creditDay;?></td>
            </tr>
        </tbody>
    </table>
    
    <div style="height:600px; overflow:scroll;">
    <table class="table table-striped table-hover bordercolumn">
        <thead>
            <tr>
                <th>Sl. No</th>
                <th>Item Code</th>
                <th class="hidden-480">Description</th>
                <th class="hidden-480">Quantity</th>
                <th class="hidden-480">Cost Price</th>
                <th class="hidden-480">Sell Price</th>
                <th class="hidden-480">Case Count</th>
                <th class="hidden-480">Current Stock</th>
                <th class="hidden-480">15 Days Movement</th>
                <th>Net Amount</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $srl = 0;
        if(!empty($itemModel)) :
            foreach($itemModel as $key=>$data) : $srl++; ?>
                <tr>
                    <td><?php echo $srl;?></td>
                    <td><?php echo $data->itemCode;?></td>
                    <td><?php echo $data->itemName;?></td>
                    <td class="hidden-480">
                        <?php 
							 echo CHtml::hiddenField('pk['.$key.']',$key, array());
							 echo CHtml::hiddenField('proId['.$key.']',$data->id, array());
							 if($data->isWeighted=='no') :
                             	 echo CHtml::textField('qty['.$key.']','',array('id'=>'qty'.$key, 'class'=>'qtyblur','style'=>'width:30px; height:15px;','placeholder'=>'0'));
							 else :
							 	 echo CHtml::textField('qty['.$key.']','',array('id'=>'qtywt'.$key, 'class'=>'qtyblur','style'=>'width:30px; height:15px;','placeholder'=>'0.0'));
							 endif;
                        ?>
                    </td>
                    <td class="hidden-480">
                        <?php // echo CHtml::textField('costPrice['.$key.']',$data->costPrice, array('id'=>'costPrice'.$key, 'class'=>'costPriceblur','style'=>'width:100px; height:15px;'));
							  echo CHtml::hiddenField('costPrice['.$key.']', $data->costPrice, array('id'=>'costPrice'.$key, 'class'=>'costPriceblur','style'=>'width:100px; height:15px;'));
							  echo $data->costPrice;
						?>
                    </td>
                    <td class="hidden-480"><?php echo $data->sellPrice;?></td>
                    <td class="hidden-480"><?php echo $data->caseCount;?></td>
                    <td class="hidden-480">
						<?php
							echo Stock::getItemWiseStock($data->id);
							echo CHtml::hiddenField('currentStock['.$key.']', Stock::getItemWiseStock($data->id), array('id'=>'currentStock'.$key, 'class'=>'currentStockblur','style'=>'width:100px; height:15px;'));
						?>
                    </td>
                    <td class="hidden-480">
						<?php 
							echo SalesDetails::get15DaysMovement($data->id); 
							echo CHtml::hiddenField('soldQty['.$key.']', SalesDetails::get15DaysMovement($data->id), array('id'=>'soldQty'.$key, 'class'=>'soldQtyblur','style'=>'width:100px; height:15px;'));
						?>
					</td>
                    <td><span id="netTotal<?php echo $key;?>">0</span></td>
                </tr>
            <?php endforeach; 
        else : ?>
            <tr>
                <td>#</td>
                <td>Code here</td>
                <td>Description here </td>
                <td class="hidden-480"><?php echo CHtml::textField('qty[]',0,array('style'=>'width:50px; height:10px;'));?></td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td class="hidden-480">0</td>
                <td>0</td>
            </tr>
    <?php endif;?>
         </tbody>
    </table>
   </div>
</div>   