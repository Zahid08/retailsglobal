<table class="table table-striped table-bordered table-advance table-hover">
    <thead>
        <tr>
            <th><i class="icon-bookmark"></i> Payment Date</th>
            <th><i class="icon-bookmark"></i> Consig. Supplier Balance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo date("Y-m-d");?></td>
            <td><?php echo Supplier::getSupplierBalanceConsig($suppModel->id);?></td>
        </tr>
   </tbody>
 </table>