<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Company <i class="icon-angle-right"></i></li>
    <li>Details Company # <?php echo $model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'orgIdPrefix',
		'name',
		'domain',
		'email',
		array(            
			'label'=>'favicon',  
			'type'=>'raw',
			'value'=>CHtml::image(Yii::app()->baseUrl.$model->favicon),
		  ),
		  array(            
			'label'=>'Logo',  
			'type'=>'raw',
            'value'=>CHtml::image(Yii::app()->baseUrl.$model->logo),
		  ),
		'detailsinfo',
		'theme',
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
	),
)); ?>
</div>
