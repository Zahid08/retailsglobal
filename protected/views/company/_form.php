<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Company
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'company-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Company</a></li>
    </ul>

	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
            <div class="row">
                <div class="span5">
                    <?php echo $form->labelEx($model,'orgIdPrefix'); ?>
                    <?php echo $form->textField($model,'orgIdPrefix',array('class'=>'m-wrap large','size'=>5,'maxlength'=>5)); ?>
                    <?php echo $form->error($model,'orgIdPrefix'); ?>                    
                </div>  
                <?php if(isset(Yii::app()->user->isSuperAdmin) && !empty(Yii::app()->user->isSuperAdmin)) : ?>
                <div class="span4">
                    <?php echo $form->labelEx($model,'theme'); ?>
                      <?php echo $form->dropDownList($model,'theme', Company::getThemes(), array('class'=>'m-wrap large','prompt'=>'Select Theme')); ?>
                    <?php echo $form->error($model,'theme'); ?>                  
                </div>  
                <?php endif;?>
            </div>
            <div class="row">
                <div class="span5">
                   <?php echo $form->labelEx($model,'name'); ?>
                   <?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                   <?php echo $form->error($model,'name'); ?>   
                </div>  
                <div class="span4">
                    <?php echo $form->labelEx($model,'logo'); ?>
                    <?php echo $form->fileField($model,'logo'); ?><br /><span style="color:#BF0000;"> [ Only Support IMAGE (jpg,gif,png) ]</span>
                    <?php echo $form->error($model,'logo'); ?>
                    <?php echo CHtml::image(Yii::app()->baseUrl.$model->logo,"",array("width"=>50)); ?>     
                </div>    
            </div>
           
            <div class="row">
                <div class="span5">
    				<?php echo $form->labelEx($model,'domain'); ?>
                    <?php echo $form->textField($model,'domain',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                    <?php echo $form->error($model,'domain'); ?>
                </div>
                <div class="span4">
                    <?php echo $form->labelEx($model,'favicon'); ?>
                    <?php echo $form->fileField($model,'favicon'); ?><br /><span style="color:#BF0000;"> [ Only Support (.ico) ]</span>
                    <?php echo $form->error($model,'favicon'); ?>
                    <?php  echo CHtml::image(Yii::app()->baseUrl.$model->favicon,"",array("width"=>50)); ?>
                </div>
            </div>
            <div class="row">
                 <div class="span5">     
    				<?php echo $form->labelEx($model,'email'); ?>
                    <?php echo $form->textField($model,'email',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                    <?php echo $form->error($model,'email'); ?>
                </div>
                <div class="span4">

                    <?php echo $form->labelEx($model,'sms_api_key'); ?>
                    <?php echo $form->textField($model,'sms_api_key',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250)); ?>
                    <?php echo $form->error($model,'sms_api_key'); ?>

                </div>
            </div>          
            <div class="row">
                <div class="span5">
                    <?php echo $form->labelEx($model,'detailsinfo'); ?>
                    <?php echo $form->textArea($model,'detailsinfo',array('rows'=>6, 'cols'=>50 ,'style'=>'width:320px !important;')); ?>
                    <?php echo $form->error($model,'detailsinfo'); ?>
                </div>
                <div class="span4">

                    <?php echo $form->labelEx($model,'sms_text'); ?>
                    <?php echo $form->textArea($model,'sms_text',array('rows'=>6, 'cols'=>50 ,'style'=>'width:320px !important;')); ?>
                    <?php echo $form->error($model,'sms_text'); ?>

                    <?php echo $form->labelEx($model,'status'); ?>
                    <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                    <?php echo $form->error($model,'status'); ?>
                </div>

            </div>
		</div>
    </div>
                
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>    
<?php $this->endWidget(); ?>
</div> <!-- form -->   