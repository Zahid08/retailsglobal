<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>
        Settings
        <i class="icon-angle-right"></i>
    </li>
    <li>Company <i class="icon-angle-right"></i></li>
    <li>Manage Company</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php CHtml::link('Add Company', array('company/create'), array('class'=>'btn light', 'style'=>'float:right;margin-left:10px;'));?>

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'company-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		'orgIdPrefix',
		'name',
		'domain',
		'email',
        array(
            'name' => 'favicon',
            'type' => 'raw',
            'value'=>'CHtml::image(Yii::app()->baseUrl.$data->favicon,"",array("style"=>"width:50px;"))',
        ),
		array('name'=>'logo',
			 'type'=>'raw',
			 'value'=>'CHtml::image(Yii::app()->baseUrl.$data->logo,"",array("style"=>"width:50px;"))',
		),	
		array('name'=>'theme',
			 'value'=>'$data->theme',
			 'filter'=>Company::getThemes(),
		),

		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=> empty(Yii::app()->user->isSuperAdmin)?'{update}':'{view} {update} {delete}',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('company-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>