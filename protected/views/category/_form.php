<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<style>
    .row.checkboxLabel label {
        margin-top: -1.5em;
        margin-left: 23px;
    }
</style>

<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Category
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'category-form',
	'enableAjaxValidation'=>true,
	 'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Category</a></li>
    </ul>
	<div class="tab-content">
        <div class="tab-pane active span4" id="tab_1_1">
			<div class="row">
				 <?php echo $form->labelEx($model,'deptId'); ?>
				 <?php echo $form->dropDownList($model,'deptId', Department::getAllDepartment(Department::STATUS_ACTIVE), 
							  array('class'=>'m-wrap combo combobox','prompt'=>'Select Department',
									'id'=>'deptId',
									'options'=>($model->isNewRecord) ? '' : array($model->subdept->dept->id=>array('selected'=>'selected')),
									'onchange'=>'js:$("#ajax_loaderdept").show()',
									'ajax' => array('type'=>'POST', 
													'data'=>array('deptId'=>'js:this.value'),  
													'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
													'success'=>'function(data) 
													{
														$("#ajax_loaderdept").hide();
														$("#subdeptId").html(data);
													}',
									)
							)); ?>
				 <span id="ajax_loaderdept" style="display:none;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
				 </span>
				 <?php echo $form->error($model,'deptId'); ?>
			</div>
			<div class="row">
				 <?php echo $form->labelEx($model,'subdeptId'); ?>
				 <?php echo $form->dropDownList($model,'subdeptId', Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), array('id'=>'subdeptId','class'=>'m-wrap span','prompt'=>'Select Sub Department')); ?>
				 <?php echo $form->error($model,'subdeptId'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
            <div class="row">
                <?php echo $form->labelEx($model,'name_bd'); ?>
                <?php echo $form->textField($model,'name_bd',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'name_bd'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'image'); ?>
                <?php echo $form->fileField($model,'image',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'image'); ?>
                <?php echo CHtml::image(Yii::app()->getBaseUrl(true).'/media/catalog/category/'.$model->image,"",array());?>
            </div>
            <?php
            if (!$model->isNewRecord ){
                ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'price'); ?>
                    <?php echo $form->textField($model,'price',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
                    <?php echo $form->error($model,'price'); ?>
                    <p style="color: red">Note :If need to update price then  first retrieve it</p>
                </div>

                <div class="row checkboxLabel" style="font-size: 16px;margin-top: 17px;width: 500px;">
                    <?php echo $form->checkBox($model,'retrieve_previous_price'); ?>
                    <?php echo $form->labelEx($model,'retrieve_previous_price'); ?>
                    <?php echo $form->error($model,'retrieve_previous_price'); ?>
                </div>
            <?php }?>

			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Status')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
        </div>   
    </div>
	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   