<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Category <i class="icon-angle-right"></i></li>
    <li>Manage Category</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php CHtml::link('Add Category', array('category/create'), array('class'=>'btn light', 'style'=>'float:right;margin-left:10px;'));?>

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		array('name'=>'subdeptId',
			 'value'=>'$data->subdept->name',
			 'filter'=> Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE),
		),
		'name',
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		 array(
            'name' => 'image',
            'type' => 'raw',
            'value' => 'CHtml::image(Yii::app()->getBaseUrl(true)."/media/catalog/category/".$data->image,"",array("style"=>"height:50px;"))',
        ),
		'rank',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('category-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>