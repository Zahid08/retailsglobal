<?php
/* @var $this BankController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vat',
);

$this->menu=array(
	array('label'=>'Create Vat', 'url'=>array('create')),
	array('label'=>'Manage Vat', 'url'=>array('admin')),
);
?>

<h1>Vat</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
