<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Setting<i class="icon-angle-right"></i></li>
    <li>Vat Account</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bank-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Vat Percentage</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
            <div class="row">
                <div class="col-md-6" style="width: 332px">
                <?php echo CHtml::label('Branch','');?>
                <?php echo CHtml::dropDownList('branch',$model->branch,Branch::getAllBranchGlobal(Branch::STATUS_ACTIVE),
                    array('class'=>'m-wrap combo combobox','prompt'=>'Select','id'=>'branch',
                    ));
                ?>
                </div>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'percentage'); ?>
                <?php echo $form->textField($model,'percentage',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                <?php echo $form->error($model,'percentage'); ?>
            </div>
            <div class="row">
                <?php echo $form->labelEx($model,'vat_date'); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model'=>$model,
                    'attribute'=>'vat_date',
                    'id'=>'vat_date',
                    //'value' =>'couponStartDate',
                    'options'=>array(
                        'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                        'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                        'changeMonth' => 'true',
                        'changeYear' => 'true',
                        'showButtonPanel' => 'true',
                        'constrainInput' => 'false',
                        'duration'=>'normal',
                    ),
                    'htmlOptions'=>array(
                        'class'=>'m-wrap large',
                        'style'=>'width: 333px !important',
                    ),
                ));
                ?>
                <?php echo $form->error($model,'vat_date'); ?>
            </div>

                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   