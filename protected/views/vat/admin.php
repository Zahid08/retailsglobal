<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Setting<i class="icon-angle-right"></i></li>
    <li>Manage Vat Percentage</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'bank-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
	array('header'=>'Sl.',
			 'value'=>'$row+1',
	),
        array('name'=>'branch',
            'value'=>'Branch::getBranchNameValue($data->branch)',
            'filter'=>Branch::getAllBranch(Brand::STATUS_ACTIVE)
        ),

		'percentage',

        array('name'=>'vat_date',
            'value'=>'date("m-d-Y", strtotime($data->vat_date))',
            'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model' =>$model,'attribute' =>'vat_date',
                'htmlOptions' =>array('id' =>'vat_date','style'=>'height: auto !important;'),
                'options' =>array('dateFormat'=>'yy-mm-dd')),true)
        ),

	       array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
	),
	//	'rank',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('bank-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>