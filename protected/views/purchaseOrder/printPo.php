<?php
	// print po
	if(!empty($printId) && is_numeric($printId))
		 $this->renderPartial('_printPo', array('printId'=>$printId)); 
?>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Purchase</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchase-order-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Print Purchase Order</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4" style="width:500px;">
					<?php echo $form->labelEx($model,'poNo'); ?>
					<?php $this->widget('CAutoComplete',array(
								 'model'=>$model,
								 'id'=>'poId',
								 'attribute' => 'poNo',
								 //name of the html field that will be generated
								 //'name'=>'poId', 
											 //replace controller/action with real ids
								 'value'=>$model->poNo,
					
								 'url'=>array('ajax/autoCompletePoNo'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
								 ));
							?>
					<?php echo $form->error($model,'poNo'); ?>
				</div>
			</div>
        </div>         
    </div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Print PO', array('id'=>'submitInvoice', 'class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   