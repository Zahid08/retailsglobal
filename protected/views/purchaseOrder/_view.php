<?php
/* @var $this PurchaseOrderController */
/* @var $data PurchaseOrder */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplierId')); ?>:</b>
	<?php echo CHtml::encode($data->supplierId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poNo')); ?>:</b>
	<?php echo CHtml::encode($data->poNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poDate')); ?>:</b>
	<?php echo CHtml::encode($data->poDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('poValidDate')); ?>:</b>
	<?php echo CHtml::encode($data->poValidDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalQty')); ?>:</b>
	<?php echo CHtml::encode($data->totalQty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalPrice')); ?>:</b>
	<?php echo CHtml::encode($data->totalPrice); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalWeight')); ?>:</b>
	<?php echo CHtml::encode($data->totalWeight); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	*/ ?>

</div>