<script type="text/javascript" language="javascript">
$(function() 
{
	// after grnNo inputed
	$("#poNo").blur(function() 
	{
		$("#po_loader").show("slow");
		var poNo  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/cancelProductsByPo/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				poNo : poNo,
			},
			success: function(data) 
			{
				$("#po_loader").hide();
				$("#po_dynamic_container").html(data);
			},
			error: function() {}
		});
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Purchase</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'purchase-order-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Purchase Order Cancel</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'poNo'); ?>
					<?php $this->widget('CAutoComplete',array(
								 'model'=>$model,
								 'id'=>'poNo',
								 'attribute' => 'poNo',
								 //name of the html field that will be generated
								 //'name'=>'poId', 
											 //replace controller/action with real ids
								 'value'=>($model->poNo)?$model->poNo:'',
					
								 'url'=>array('ajax/autoCompletePoNo'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
								 ));
							?>
					<?php echo $form->error($model,'poNo'); ?>
					<span id="po_loader" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
				</div>
			</div>
			
			 <div id="po_dynamic_container">   
				 <div class="row">
					<div style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th>Sl. No</th>
								<th>Item Code</th>
								<th class="hidden-480">Description</th>
								<th class="hidden-480">Cost Price</th>
								<th class="hidden-480">Sell Price</th>
								<th class="hidden-480">Quantity</th>
								<th class="hidden-480">Case Count</th>
								<th class="hidden-480">Current Stock</th>
								<th class="hidden-480">15 Days Movement</th>
								<th>Net Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>#</td>
								<td>-</td>
								<td>- </td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td class="hidden-480">0</td>
								<td>0</td>
								<td>0</td>
								<td>0</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
			</div>
		</div>       
	</div>
	 <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Cancel PO' : 'Cancel PO', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   