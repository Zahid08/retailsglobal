<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$('#report_dynamic_container').printElement(
		{
				overrideElementCSS:[
					'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
					{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
					'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
					{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
					'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
					{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
					'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
					{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
				],            
				printBodyOptions:{                         
						styleToAdd:'margin-left:25px !important',
						//classNameToAdd : 'printBody',
				}
		   }			
		);
	});
</script>  

<div class="form" style="display:none;">
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Purchase Order Details Report</td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th>PO No.</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Quantity</th>
                        <th class="hidden-480">Cost Price</th>
                        <th class="hidden-480">PO Date</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				$model = PoDetails::model()->findAll('poId=:poId',array(':poId'=>$printId));
				if(!empty($model)) : 
					$srl = $totalQty  = $totalPrice = 0;
					foreach($model as $data) : 
					$srl++; 
					$totalQty+=$data->qty;
					$totalPrice+=($data->qty*$data->costPrice);?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->po->poNo;?></td>
						<td><?php echo $data->item->itemCode;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
						<td><?php echo $data->qty;?></td>
						<td><?php echo round(($data->qty*$data->costPrice),2);?></td>
						<td><?php echo $data->crAt;?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                        <td colspan="5" align="right">Total : </td>
						<td><?php echo $totalQty;?></td>
                        <td><?php echo round($totalPrice,2);?></td>
                        <td></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>