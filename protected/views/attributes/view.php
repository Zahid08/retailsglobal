<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Attributes<i class="icon-angle-right"></i></li>
    <li>Attributes<i class="icon-angle-right"></i></li>
    <li>Details Attributes # <?php echo $model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		array(            
			'name'=>'attTypeId',  
			'value'=>$model->attType->name,
		  ),
		'name',
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
		'rank',
	),
)); ?>
</div>
