<?php
/* @var $this AttributesTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Attributes Types',
);

$this->menu=array(
	array('label'=>'Create AttributesType', 'url'=>array('create')),
	array('label'=>'Manage AttributesType', 'url'=>array('admin')),
);
?>

<h1>Attributes Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
