<?php
/* @var $this AttributesTypeController */
/* @var $model AttributesType */

$this->breadcrumbs=array(
	'Attributes Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AttributesType', 'url'=>array('index')),
	array('label'=>'Create AttributesType', 'url'=>array('create')),
	array('label'=>'View AttributesType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AttributesType', 'url'=>array('admin')),
);
?>

<h1>Update AttributesType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>