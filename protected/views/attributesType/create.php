<?php
/* @var $this AttributesTypeController */
/* @var $model AttributesType */

$this->breadcrumbs=array(
	'Attributes Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AttributesType', 'url'=>array('index')),
	array('label'=>'Manage AttributesType', 'url'=>array('admin')),
);
?>

<h1>Create AttributesType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>