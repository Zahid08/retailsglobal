<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store
	 <i class="icon-angle-right"></i>
	</li>
	<li>Items	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
    <?php echo $form->errorSummary($model); ?>
	<?php if(!empty($msg)) :?>
    <div class="notification note-success">
        <p><?php echo $msg;?></p>
    </div>
    <?php else :?>
    <div class="notification note-error">
        <p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
    </div>
    <?php endif;?>
 <div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;<?php echo 'Import';?>&nbsp;Items</a></li>
    </ul>
	<div class="tab-content">

            <div class="row">
                <div class="span4">
                    <label for="Items_variation_type" class="required">Type <span class="required">*</span></label>
                    <select name="Items[variation_type]" id="Items_variation_type">
                        <option value="">Select Option</option>
                        <option value="1">Simple</option>
                        <option value="2">Variation</option>
                    </select>
                    <div class="errorMessage" id="Items_variation_type_em_" style="display:none"></div>
                </div>
                <div class="span4">
                    <?php echo $form->labelEx($model,'xlsFile'); ?>
                    <?php echo $form->fileField($model,'xlsFile',array('id'=>'upload')); ?>

                    <?php echo $form->error($model,'xlsFile'); ?>
                </div>
            </div>

<!--			<div class="tab-pane active" id="progressbar">-->
<!--				<div class="row">-->
<!--                    <div class="progress">-->
<!--                        <div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>-->
<!--                    </div>-->
<!--				</div>-->
<!--			</div>-->

		</div><!-- portlet body -->
		<div class="row buttons">
             <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue')); ?>
        </div>
	</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   