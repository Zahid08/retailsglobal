<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store
	 <i class="icon-angle-right"></i>
	</li>
	<li>Items <i class="icon-angle-right"></i></li>
    <li>Generate Barcode</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="notification note-warning">
    <p></b>Total Success : <?php echo $totalSuccess;?>, Total Failure : <?php echo $totalFailure;?>, Total Remain : <?php echo $totalRemain;?></p>
</div>
<?php if(!empty($msg)) echo $msg;?>