<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
	<li>Items <i class="icon-angle-right"></i></li>
    <li>Manage Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form" style="overflow:scroll;">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'items-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		array('name'=>'catId',
			  'value'=>'$data->cat->name',
			  'filter'=>Category::getAllCategory(Category::STATUS_ACTIVE)
		),
		array('name'=>'brandId',
			  'value'=>'$data->brand->name',
			  'filter'=>Brand::getAllBrand(Brand::STATUS_ACTIVE)
		),
		array('name'=>'supplierId',
			  'value'=>'$data->supplier->name',
			  //'filter'=>Supplier::getAllSupplier(Supplier::STATUS_ACTIVE),
			  'filter'=>CHtml::activeDropDownList($model, 'supplierId',Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Supplier')),
		),
		'itemCode',
		'barCode',
		'itemName',
		'costPrice',
		'sellPrice',
		array(            
			'name'=>'isEcommerce',  
			'value'=>'(($data->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No"))',
			'filter'=>array(1=>'Yes',2=>'No'),
		),
		array(            
			'name'=>'isFeatured',  
			'value'=>'(($data->isFeatured==Items::IS_FEATURED?"Yes":"No"))',
			'filter'=>array(1=>'Yes',2=>'No'),
		),
		array(            
			'name'=>'isSales',  
			'value'=>'(($data->isSales==Items::IS_SALES?"Yes":"No"))',
			'filter'=>array(1=>'Yes',2=>'No'),
		),
        array(            
			'name'=>'isParent',  
			'value'=>'(($data->isParent==Items::IS_PARENTS?"Yes":"No"))',
			'filter'=>array(1=>'Yes',2=>'No'),
		),
		array('name'=>'variation_type',
            'value'=>'ItemsVariation::type($data->id, $data->variation_type)',

            //'filter'=>UsefulFunction::enumItem($model,'variation_type'),
		),
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('items-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>