<style>
    .items-variation-lists{
        margin-top: 15px;
    }
    .items-variation-lists .btn_up{
        margin-top: 8px !important;
    }
    a.variation-edit-btn {
        background: #000;
        color: #fff;
        padding: 3px 15px;
        display: inline-block;
        margin: 5px 5px 7px 5px;
        text-decoration: none;
        border-radius: 8%!important;
        text-transform: uppercase;
    }
    a.variation-duplicated-btn {
        background: #0b94ea;
        border-radius: 8%!important;
        color: #fff;
        padding: 3px 5px;
        display: inline-block;
        margin: 5px 5px 7px 5px;
        text-decoration: none;
        text-transform: uppercase;
    }
    a.variation-delete-btn {
        background: red;
        border-radius: 8%!important;
        color: #fff;
        padding: 3px 5px;
        display: inline-block;
        margin: 5px 5px 7px 5px;
        text-decoration: none;
        text-transform: uppercase;
    }

    a.variation-edit-btn:hover{
        background: #000020;
    }
    .modal.fade.in{
        top: 35% !important;
    }
    button.close {
        top: -22px;
        position: relative;
    }
    .bordercolumn tbody tr td{
        vertical-align: middle;
    }
    .alert.alert-success.disabled {
        margin-top: 13px;
        display: none;
    }
    .alert.alert-success {
        margin-top: 13px;
    }

    a.variation-duplicated-btn.disbaled {
        pointer-events: none;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="variationCreateModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Variation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">

            </div>
           <!-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>-->
        </div>
    </div>
</div>

<?php
if (isset($_SESSION['variationErrorMessage'])){
    ?>
    <div class="alert alert-danger" style="margin-top: 12px;">
        <strong>Error!</strong> Duplicated Item Code Generated.
    </div>
<?php
}else{
    unset($_SESSION['variationErrorMessage']);
}
?>

<div class="alert alert-success disabled">
    <strong>Success!</strong> Duplicated Variations Created.Wait 2 Second And See Below For New Creations Record.
</div>

<div class="single-product-information">
    <?php
        $item = Items::model()->find('id=:id',array(':id'=>$_REQUEST['item_id']));
    ?>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-top:20px;">
        <tbody>
            <tr>
                <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                    <strong>Item Code:</strong> <?php echo $item->itemCode; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <strong>Item Name:</strong> <?php echo $item->itemName; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <strong>Cost Price:</strong> <?php echo $item->costPrice; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <strong>Sell Price:</strong> <?php echo $item->sellPrice; ?>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;padding:3px 0px;" colspan="2">
                    <strong>Item:</strong>  <a style="margin-left: 15px;background: #48c3e9;color: #fff;padding: 2px 8px;" href="<?php echo Yii::app()->baseUrl . '/items/update/' . $item->id; ?>">Edit</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<div class="items-variation-lists">
    <div class="span6">
        <h3>Items Variation List:</h3>
    </div>
    <div style="text-align: right;" class="span6">
        <?php
            $createUrl = Yii::app()->baseUrl.'/items/variationCreate?item_id='.$_REQUEST['item_id'];
        ?>
        <a data-toggle="modal" data-target="#variationCreateModal" class="btn blue btn_up" href="<?php echo $createUrl;?>">Create Variation</a>
    </div>
    <table class="table table-striped table-hover bordercolumn">
        <thead>
        <tr style="border-top:1px solid #ddd;">
            <th style="width:60px">Sl. No</th>
            <th style="width:160px" class="hidden-480">Item Code</th>
            <th style="width:160px" class="hidden-480">Bar Code</th>
            <th style="width:200px" class="hidden-480">Image</th>
            <th style="width:160px" class="hidden-480">Color</th>
            <th style="width:100px" class="hidden-480">Size</th>
            <th style="width:160px" class="hidden-480">Total Quantity</th>
            <th style="width:160px" class="hidden-480">Total Sale</th>
            <th style="width:160px" class="hidden-480">Current Stock</th>
            <th style="width:200px" class="hidden-480">Cost Price</th>
            <th style="width:200px" class="hidden-480">Sell Price</th>
            <th style="width:100px" class="hidden-480">Status</th>
            <th style="width:502px" class="hidden-480">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
            if(!empty($variationLists)){
                $sl = 1;
                $sumqty = '';
                foreach ($variationLists as $variationList){
                  if(!empty($variationList)){
                      $color = Attributes::getNameById($variationList->color);
                      $size = Attributes::getNameById($variationList->size);
                      $stock = Stock::getItemWiseStock($variationList->item_id);
                    ?>
                    <tr>
                        <td><?php echo $sl;?></td>
                        <td><?php echo $variationList->item_code; ?></td>
                        <td><?php echo $variationList->bar_code; ?></td>
                        <td>
                            <?php
                                if(!empty($variationList->image)){
                                    ?>
                                    <img style="width: 70px;margin: 5px" src="<?php echo Yii::app()->baseUrl.'/'.$variationList->image?>" alt="">
                                    <?php
                                }else{
                                    ?>
                                    <img style="width: 70px;margin: 5px" src="https://via.placeholder.com/150" alt="<?php echo $variationList->id;?>">
                                    <?php
                                }
                            ?>
                        </td>
                        <td>
                            <?php
                                if(!empty($color)){
                                    echo $color->name;
                                }

                            ?>
                        </td>
                        <td>
                            <?php
                            if(!empty($size)){
                                echo $size->name;
                            }

                            ?>
                        </td>
                        <td>
                            <?php
                             echo $stock;
                               /* $stockQuantity = Stock::model()->find(
                                    array(
                                        'select'=>'itemId, SUM(qty) as qty',
                                        'condition'=>'itemId=:itemId',
                                        'params'=>array(':itemId'=>$variationList->item_id)
                                    )

                                );
                                if(!empty($stockQuantity)){
                                    echo $stockQuantity->qty;
                                }*/
                            ?>
                        </td>
                        <td>
                            <?php
                            $saleQuantityss = SalesDetails::model()->find(
                                    array(
                                        'select'=>'itemId, SUM(qty) as qty',
                                        'condition'=>'itemId=:itemId',
                                        'params'=>array(':itemId'=>$variationList->item_id)
                                    )

                            );

                            echo $saleQuantityss->qty;
                            ?>
                        </td>
                        <td>
                            <?php
                          $currentStock = $stock - $saleQuantityss->qty;
                            if($currentStock == 0 || empty($currentStock)){
                                echo '<p style="color: red">Stock Out</p>';
                            }else{
                                echo $currentStock;
                            }
                            ?>
                        </td>
                        <td><?php echo $variationList->cost_price; ?></td>
                        <td><?php echo $variationList->sell_price; ?></td>
                        <td>
                            <?php
                            echo Lookup::item("Status", $variationList->status)
                            ?>
                        </td>
                        <td>
                            <?php
                                $updateUrl = Yii::app()->baseUrl.'/items/variationUpdate?id='.$variationList->id.'&itemid='.$variationList->item_id.'&parentitemid='.$_REQUEST['item_id'];
                                $deleteUrl = Yii::app()->baseUrl.'/items/deletevariation?id='.$variationList->id.'&itemid='.$variationList->item_id.'&parentitemid='.$_REQUEST['item_id'];
                            ?>
                            <a data-toggle="modal" data-target="#variationCreateModal"  class="variation-edit-btn" href="<?php echo $updateUrl; ?>">Edit</a>
                            <a onclick="return confirm(' you want to delete?');" class="variation-delete-btn" href="<?php echo $deleteUrl; ?>">Delete</a>
                            <a   data-item_code="<?php echo $variationList->item_code; ?>"
                                 data-bar_code="<?php echo $variationList->bar_code; ?>"
                                 data-image="<?php echo !empty($variationList->image)?$variationList->image:'' ?>"
                                 data-color="<?php echo !empty($variationList->color)?$variationList->color:'';?>"
                                 data-size="<?php echo !empty($variationList->size)?$variationList->size:'';?>"
                                 data-stock="<?php echo !empty($stock)?$stock:'';?>"
                                 data-total_sale="<?php echo !empty($saleQuantityss->qty)?$saleQuantityss->qty:'';?>"
                                 data-current_stock="<?php echo !empty($currentStock->qty)?$currentStock:'';?>"
                                 data-cost_price="<?php echo !empty($variationList->cost_price)?$variationList->cost_price:'';?>"
                                 data-sell_price="<?php echo !empty($variationList->sell_price)?$variationList->sell_price:'';?>"
                                 data-variations_status="<?php echo !empty($variationList->status)?$variationList->status:'';?>"
                                 data-name="<?php echo !empty($item->itemName)?$item->itemName:'';?>"
                                 class="variation-duplicated-btn" href="javascript:void(0)">Duplicated</a>
                        </td>
                    </tr>
                    <?php
                    $sl++;
                }
            }}else{
                ?>
                <tr>
                    <td colspan="12">
                        <h4> Not Found Variation Items.</h4>
                    </td>
                </tr>
                <?php
                //echo "Not Found Variation Items.";
            }

        ?>

        </tbody>
    </table>
</div>

<script>
    $('.variation-duplicated-btn').on('click', function (event) {
        event.stopImmediatePropagation();
        $(this).addClass("disbaled");
        $(this).text('DUPLICATING...');

        var itemId='<?php echo $_REQUEST['item_id']?>';
        var item_code=$(this).data('item_code');
        var bar_code='';
        var image=$(this).data('image');
        var color=$(this).data('color');
        var size=$(this).data('size');
        var stock=$(this).data('stock');
        var total_sale=$(this).data('total_sale');
        var current_stock=$(this).data('current_stock');
        var cost_price=$(this).data('cost_price');
        var sell_price=$(this).data('sell_price');
        var variations_status=$(this).data('variations_status');
        var name=$(this).data('name');


        var url='<?php echo Yii::app()->request->baseUrl. '/items/duplicatedVariationCreate?item_id='?>'+itemId;
        var redirectUrl='<?php echo Yii::app()->request->baseUrl . '/items/variation?item_id='?>'+itemId;

        $.ajax({
            type: "POST",
            url: url,
            data:
                {
                    'ItemsVariation[item_id]': itemId,
                    'ItemsVariation[item_code]': item_code,
                    'ItemsVariation[bar_code]': bar_code,
                    'ItemsVariation[image]': image,
                    'ItemsVariation[color]': color,
                    'ItemsVariation[size]': size,
                    'ItemsVariation[quantity]': stock,
                    'ItemsVariation[total_sale]': total_sale,
                    'ItemsVariation[current_stock]': current_stock,
                    'ItemsVariation[cost_price]': cost_price,
                    'ItemsVariation[sell_price]': sell_price,
                    'ItemsVariation[variations_status]': variations_status,
                },
            success: function(data)
            {
                $('.alert.alert-success').removeClass('disabled');

                var timer = setTimeout(function() {
                   window.location = redirectUrl;
                }, 2000);
            },
            error: function() {
                //$("#ajax_errorQty"+qtyId).html("Stock is less than Quantity !");
            }
        });

        //var rowHtml = prepareProductRow(indexProduct);
    });

    function prepareProductRow(indexProduct) {
        var html = '';

        html += '<tr class="new-product-item">';

        html += '</tr>';

        return html;
    }

</script>