<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Items <i class="icon-angle-right"></i></li>
    <li>Search Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$bName=$bAdd='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $bName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $bAdd=$branchModel->addressline; 
?>
<div class="form">
	<div style="width:98%; padding:10px; border:1px dotted #ccc;">
		<div class="span5">	
			<?php if(!empty($msg)) echo $msg;?>
			<?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'login-form item_form',
                    'action'=>Yii::app()->createUrl('items/searchItems'),
                    'enableAjaxValidation'=>true,				
                    
           )); ?> 
		   <div class="row">
				<div class="span6">
					 <?php echo CHtml::label('Category','');?>
					 <?php echo CHtml::dropDownList('catId',$cat,Category::getAllCategory(Category::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Category','id'=>'catId',)); ?>
				 </div>
				<div class="span6">
					 <?php echo CHtml::label('Supplier','');?>
					 <?php echo CHtml::dropDownList('supplierId',$supplierId, Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Supplier')); ?>
				</div>          
				
			</div> 
			<div class="row">
				<div class="span12">
					 <?php echo CHtml::label('Brand','');?>
					 <?php echo CHtml::dropDownList('brandId',$brandId,Brand::getAllBrand(Brand::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Brand')); ?> 
				 </div>				 
			</div>
			<div class="row">
                <div class="span4" style="padding-top:21px;">
                    <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
                </div> 
			</div>
			<?php $this->endWidget(); ?>
	    </div>
        <div class="span3" style="padding-top:12px; float:right">	
			 <?php 
                 if(isset(Yii::app()->session['reportModel']) && !empty(Yii::app()->session['reportModel'])) unset(Yii::app()->session['reportModel']);
                 else Yii::app()->session['reportModel'] = $model;
             ?>																																																							             <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/pdf_icon.png', array('id'=>'pdf_icon','class'=>'pdf_btn_up','value'=>false,'submit'=>array('report/pdfReportGenerate','pdfFile'=>'searchItemsPdf','brandName'=>$brandName,'suppName'=>$suppName,'catName'=>$catName))); ?>
            <?php echo CHtml::imageButton(Yii::app()->baseUrl.'/media/icons/xls_icon.png', array('id'=>'xls_icon','class'=>'xls_btn_up','value'=>false,'submit'=>array('reportXls/searchItems','brandName'=>$brandName,'suppName'=>$suppName,'catName'=>$catName,'bName'=>$bName,'bAdd'=>$bAdd))); ?>
             <a href="javascript:void(0);" id="printIcon" style="float:left; margin:5px;">
                <img src="<?php echo Yii::app()->baseUrl;?>/media/icons/print_icon.png" />
            </a>
		</div>
        <div class="clearfix"></div>
	</div>
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            //$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                            //$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Item Search Result</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php if(!empty($brandName)) echo 'Brand : '.$brandName.', ';
							  if(!empty($suppName)) echo 'Supplier : '.$suppName.', ';
							  if(!empty($catName)) echo 'Category : '.$catName;
						?>
                    </td>
                </tr>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr>
                        <th>Sl. No</th>
                        <th class="hidden-480">Brand</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Category</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Bar Code</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Cost Price</th>
                        <th class="hidden-480">Sell Price</th>
                        <th class="hidden-480">Weighted</th>
                        <th class="hidden-480">GP(%)</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($model)) : 
					$srl = 0;
					foreach($model as $data) : 
					$srl++; 
					?>
					<tr>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->brand->name;?></td>
						<td><?php echo $data->supplier->name;?></td>
						<td><?php echo $data->cat->name;?></td>
                        <td><?php echo $data->itemCode;?></td>
                        <td><?php echo $data->barCode;?></td>
						<td><?php echo $data->itemName;?></td>
						<td><?php echo $data->costPrice;?></td>
                        <td><?php echo $data->sellPrice;?></td>
                        <td><?php echo $data->isWeighted;?></td>
                        <td><?php echo round(((($data->sellPrice-$data->costPrice)*100)/$data->sellPrice),2);?></td>
					</tr>
				<?php endforeach; 
                else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>