<?php
/* @var $this ItemsController */
/* @var $data Items */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itemCode')); ?>:</b>
	<?php echo CHtml::encode($data->itemCode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itemName')); ?>:</b>
	<?php echo CHtml::encode($data->itemName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costPrice')); ?>:</b>
	<?php echo CHtml::encode($data->costPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sellPrice')); ?>:</b>
	<?php echo CHtml::encode($data->sellPrice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catId')); ?>:</b>
	<?php echo CHtml::encode($data->catId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brandId')); ?>:</b>
	<?php echo CHtml::encode($data->brandId); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('supplierId')); ?>:</b>
	<?php echo CHtml::encode($data->supplierId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taxInclusive')); ?>:</b>
	<?php echo CHtml::encode($data->taxInclusive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('taxId')); ?>:</b>
	<?php echo CHtml::encode($data->taxId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('negativeStock')); ?>:</b>
	<?php echo CHtml::encode($data->negativeStock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	*/ ?>

</div>