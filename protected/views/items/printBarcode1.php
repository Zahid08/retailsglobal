<style type="text/css">
    @media screen
    {
        .invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
    }
    @media print
    {
        #pageBreak{page-break-after:always;}
    }
    @media screen,print
    {
        .invoiceTbl td {font-size:11px;}
        #barcodePrint{page-break-inside: avoid;page-break-after:always;}
    }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    //-------print options----//
    $(function()
    {
        $("#printIcon").click(function()
        {
            $('#pageBreak').printElement();
        });
    });
</script>
<script type="text/javascript" language="javascript">
    $(function()
    {
        // focus to first item
        $(".codeblur").focus();

        // Add new item rows after last item
        var id = 0;
        $(".codeblur").blur(function()
        {
            var inputId = $(this).attr("id");
            var lastRowId = $('table.bordercolumn tr:last').attr('id');
            $("#ajax_loaderCode"+inputId).show("slow");

            var input = $(this);
            var code  = input.val();
            var master = $("table.bordercolumn");
            var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/productByCodeBarcode/";

            $.ajax({
                type: "POST",
                dataType: "json",
                url: urlajax,
                data:
                    {
                        code : code,
                    },
                success: function(data)
                {
                    $("#ajax_loaderCode"+inputId).hide();

                    if(data=="null") {}
                    else if(data=="invalid")
                    {
                        input.val("");
                        input.focus();
                        $("#ajax_errorCode"+inputId).html("Invalid Item Code for Barcode!");
                    }
                    else
                    {
                        $("#ajax_errorCode"+inputId).html("");

                        // create dynamic Row for current row input
                        if(inputId==lastRowId)
                        {
                            id++;
                            var type = 1;

                            // Get a new row based on the prototype row
                            var prot = master.find(".parentRow").clone(true);
                            prot.attr("id", id)
                            prot.attr("class", "chieldRow" + id)

                            // loader error and srl processing
                            prot.find(".ajax_loaderCode").attr("id", "ajax_loaderCode"+id);
                            prot.find(".ajax_errorCode").attr("id", "ajax_errorCode"+id);

                            prot.find(".ajax_loaderQty").attr("id", "ajax_loaderQty"+id);
                            prot.find(".ajax_errorQty").attr("id", "ajax_errorQty"+id);

                            prot.find(".codeblur").attr("id",id);
                            prot.find(".srlNo").html(id+1);
                            prot.find(".codeblur").attr("id",id);

                            // item quantity next row
                            prot.find(".qtyblur").attr("name", "qty[" + id +"]");
                            prot.find(".qtyblur").attr("id", "qty" + id);
                            prot.find(".qtyblur").attr("placeholder", 0);
                            input.val(data.itemCode);

                            // quantity current weighted or not
                            $("#qty"+inputId).val(""); $("#qty"+inputId).attr("placeholder", 0);
                            $("#qty"+inputId).attr("id", "#qty"+ inputId);
                            prot.find(".typeblur").attr("id", "type" + id); prot.find(".typeblur").attr("name", "type[" + id +"]"); $("#type"+inputId).val(type);

                            // item processing
                            prot.find(".pk").attr("name", "pk[" + id +"]"); prot.find(".pk").attr("id", "pk" + id); $("#pk"+inputId).val(inputId);
                            prot.find(".proId").attr("name", "proId[" + id +"]"); prot.find(".proId").attr("id", "proId" + id); $("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
                            prot.find(".itemName").attr("id", "itemName" + id); $("#itemName"+inputId).html(data.itemName);
                            prot.find(".costPrice").attr("id", "costPrice" + id); $("#costPrice"+inputId).html(data.costPrice);
                            prot.find(".sellPrice").attr("id", "sellPrice" + id); $("#sellPrice"+inputId).html(data.sellPrice);

                            // final binding and clone with reset values
                            prot.find(".codeblur").attr("value", "");
                            prot.find(".itemName").html("");
                            prot.find(".qtyblur").val("0");
                            prot.find(".costPrice").html("0.00");
                            prot.find(".sellPrice").html("0.00");
                            master.find("tbody").append(prot);
                        }
                        else  // last blank row
                        {
                            // item processing
                            $("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
                            $("#itemName"+inputId).html(data.itemName);
                            $("#costPrice"+inputId).html(data.costPrice);
                            $("#sellPrice"+inputId).html(data.costPrice);

                            // quantity current weighted or not
                            var qtyCurrentId = ( $("#qty"+inputId).attr("id") || $("#qtywt"+inputId).attr("id") );
                            $("#"+qtyCurrentId).val(""); $("#"+qtyCurrentId).attr("placeholder", 0);
                            $("#"+qtyCurrentId).attr("id", "#qty" + inputId);
                            $("#type"+inputId).val(type);
                        }
                    }
                },
                error: function() {
                    $("#ajax_errorCode"+inputId).html("Invalid Item Code !");
                }
            });
        });


        // Remove items functionality
        $("table.bordercolumn img.removeRow").live("click", function()
        {
            id--;
            $(this).parents("tr").remove();
        });

        // use item from search fill with item code
        $("#useItems").click(function()
        {
            var itemName  = $("#itemId").val();
            var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemCodeByTitle/";
            $.ajax({
                type: "POST",
                url: urlajax,
                data:
                    {
                        itemName : itemName,
                    },
                success: function(data)
                {
                    $("table.bordercolumn tr:last .codeblur").val(data);
                    $("table.bordercolumn tr:last .codeblur").focus();
                    $("#itemId").val("");
                },
                error: function() {}
            });

        });

    });
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Items</li>
    <li style="float:right;"><?php echo '<a href="javascript:void(0);" id="printIcon" style="float:right; margin-right:5px;"><img src="'.Yii::app()->baseUrl.'/media/icons/print_icon.png" style="width:25px;" /></a>';?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php
    if(empty($model)) :

        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'items-form',
            'enableAjaxValidation'=>true,
        )); ?>

        <?php if(!empty($msg)) :?>
        <div class="notification note-success">
            <p><?php echo $msg;?></p>
        </div>
    <?php else :?>
        <div class="notification note-error">
            <p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
        </div>
    <?php endif;?>
        <div class="tabbable tabbable-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1_1" data-toggle="tab">Print Items Barcode</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                    <div class="row">
                        <a class="btn btn-primary" data-toggle="modal" href="#search_info" style=" float:right;margin:10px; font-size:12px;"><i class="icon-search"></i> Search Items</a>
                    </div>

                    <div id="search_info" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4>Search by item title : </h4>
                        </div>
                        <div class="modal-body">
                            <?php $this->widget('CAutoComplete',array(
                                //'model'=>$model,
                                'id'=>'itemId',
                                //'attribute' => 'custId',
                                //name of the html field that will be generated
                                'name'=>'itemId',
                                //replace controller/action with real ids
                                //'value'=>($model->custId)?$model->cust->name:'',

                                'url'=>array('ajax/autoCompleteItems'),
                                'max'=>100, //specifies the max number of items to display

                                //specifies the number of chars that must be entered
                                //before autocomplete initiates a lookup
                                'minChars'=>2,
                                'delay'=>500, //number of milliseconds before lookup occurs
                                'matchCase'=>false, //match case when performing a lookup?
                                'mustMatch' => true,
                                //any additional html attributes that go inside of
                                //the input field can be defined here
                                'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20),

                                //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
                                //'extraParams' => array('taskType' => 'desc'),
                                'methodChain'=>".result(function(event,item){})",
                                //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
                            ));
                            ?>
                            <button type="button" data-dismiss="modal" class="btn" id="useItems" style="margin-left:10px;">Use Items</button>
                        </div>
                    </div>

                    <div class="row">
                        <div style="height:400px; overflow:scroll;">
                            <table class="table table-striped table-hover bordercolumn">
                                <thead>
                                <tr>
                                    <th>Sl. No</th>
                                    <th>Item Code</th>
                                    <th class="hidden-480">Description</th>
                                    <th class="hidden-480">Quantity</th>
                                    <th class="hidden-480">Cost Price</th>
                                    <th>Sell Price</th>
                                    <th>Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="parentRow" id="0">
                                    <td><span class="srlNo">1</span></td>
                                    <td class="hidden-480">
                                        <?php
                                        echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:100px; height:15px;'));
                                        echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
                                        echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
                                        ?>
                                        <span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
									<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
								</span>
                                        <div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
                                    </td>
                                    <td><span id="itemName0" class="itemName">-</span></td>
                                    <td class="hidden-480">
                                        <?php echo CHtml::textField('qty[0]','',array('id'=>'qty0', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?>
                                        <?php echo CHtml::hiddenField('type[0]','',array('id'=>'type0', 'class'=>'typeblur'));?>
                                    </td>
                                    <td class="hidden-480">
                                        <span id="costPrice0" class="costPrice">0</span>
                                    </td>
                                    <td class="hidden-480">
                                        <span id="sellPrice0" class="sellPrice">0</span>
                                    </td>
                                    <td>
                                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row buttons">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue')); ?>
            </div>
        </div>
        <?php $this->endWidget();

    else :
        echo '<div style="height:842px;" id="pageBreak">';
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));

        foreach($model as $key=>$val) :
            $itemArr = explode('#',$val);
            $itemModel = Items::model()->findByPk($itemArr[0]);
            $itemVariation = ItemsVariation::model()->find('item_id=:item_id',array(':item_id'=>$itemArr[0]));

            $color = '';
            $size = '';
            if(!empty($itemVariation)){
                $color = ItemsVariation::variationAttributesById($itemVariation->color);
                $size = ItemsVariation::variationAttributesById($itemVariation->size);
            }


            if(!empty($itemModel)) : ?>
                <div id="barcodePrint" style="width:332.5984252px;border:1px solid #ddd;">
                    <?php
                    // http://www.a4papersize.org/a4-paper-size-in-pixels.php
                    $pathshow = Yii::app()->baseUrl.'/media/barcode/pluspoint.com/'.$itemModel->barCode;
                    $vat = (Items::getTaxRateById($itemModel->taxId))>0?'+VAT':'';

                    for($i=0;$i<$itemArr[1];$i++)
                    {
                        echo '<div style="width:143.62204724px;height:113.38582677px;float:left;clear:right; margin-right: 12px;border:1px dotted #ccc;" />';
                        echo '<div style="font-weight: 800;margin:0;padding:0;font-size:8px; text-align:center;line-height:13px;">'.$itemModel->itemName.'</div>';
                        if(!empty($itemVariation)){
                            echo '<div style="float:left;font-size:8px;padding-left:7px">Color: '.$color.'</div> <div style="float:right;font-size:8px;padding-right:7px">Size: '.$size.'</div>';
                        }

                        echo '<img src="'.$pathshow.'.png" style="margin-left:0px;width:137px;height:40px;"/>';
                        echo '<div style="font-weight: 800;margin:0;padding:0;font-size:9px; text-align:center;line-height:11px;">Item Code: '.$itemModel->itemCode.'</div>';
                        echo '<div style="font-weight: 800;margin-top: 5px;padding:0;font-size:12px; text-align:center;line-height:10px;">Amount: &nbsp;Tk.&nbsp;'.$itemModel->sellPrice.$vat.'</div>';
                        echo '</div>';
                    }

                    ?>
                    <div style="clear:both;"></div>
                </div>
            <?php endif;
        endforeach;
        echo '</div>';
    endif;
    ?>
    <div style="clear:both;">&nbsp;</div>
</div> <!-- form -->