<?php

// Download Stock Transfer PDF

  $company = Company::model()->find();


//$stockTransfer = StockTransfer::model()->findAll('transferNo=:transferNo',
//    array(':transferNo'=>$printId));

$sql = "SELECT * FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']."
                      AND transferNo='".$printId."'  GROUP  BY itemId DESC";

$stockTransfer = StockTransfer::model()->findAllBySql($sql);

if(!empty($stockTransfer)) {
    $transferNo = $_POST['StockTransfer']['transferNo'];
    $crAt = "";
    foreach ($stockTransfer as $stValue) {
        $crAt = $stValue['crAt'];
    }
    $formBranch = Branch::model()->find(array('condition' => 'id=:id', 'params' => array('id' => Yii::app()->session['branchId'])));
    $toBranch = Branch::model()->find(array('condition' => 'id=:id', 'params' => array('id' => $_POST['StockTransfer']['toBranchId'])));

    $html = '';
    $html .= '<div style="width:100%;">
            <caption><img src="' . Yii::app()->request->baseUrl . $company->logo . '" align="center" /></caption>
        </div>';
    $html .= '<table autosize="1" cellpadding="5" border="1" style="page-break-after: always;border-collapse:collapse;font-size: 8px;overflow: wrap" width="100%">
            <tr>
                <td><b>Transfer ID:</b> ' . $_POST['StockTransfer']['transferNo'] . '</td>
                <td><b>Issue Form:</b> ' . $formBranch->name . '</td>
            </tr>
            <tr>
                <td><b>Issue Date:</b> ' . date("d-M-Y", strtotime($crAt)) . '</td>
                <td><b>Print Date:</b> ' . date('d-M-Y') . '</td>
            </tr>
            <tr>
                <td><b>Issue To:</b> ' . $toBranch->name . '' . $toBranch->name . '</td>
                <td><b>Print Time:</b> ' . date("h:i:sa") . '</td>
            </tr>
        </table>';
    $html .= '<div style="margin: 10px"></div>';
    $html .= '<table autosize="1" cellpadding="5" border="1" style="border-collapse:collapse;font-size: 8px;overflow: wrap" width="100%">';


    $html .= '
            <tr>
                
                
                <th>Item Code</th>
                
                <th>RPU</th>
                <th width="50">QTY</th>
                <th>Total</th>
            </tr>';
    ?>

    <?php
    $baseUrl = Yii::app()->baseUrl;
    $transferId = $transferNo;
    $barCode = "<img style='width:100%;' alt='testing' src='".$baseUrl."/barcode/barcode.php?codetype=Code39&size=40&text=".$transferId."&print=true'/>";
    $grandTotalAmount = 0;
    $grandTotalQuantity = 0;
    foreach ($stockTransfer as $stValue) {
        $itemModel = Items::model()->find('id=:id', array(':id' => $stValue['itemId']));
        $itemVariationModel = ItemsVariation::model()->find('item_id=:item_id', array(':item_id' => $stValue['itemId']));
        $size = '';
        if (!empty($itemVariationModel)) {
            $size = $itemVariationModel->size;
        }
        $totalAmount = ($stValue['costPrice'] * $stValue['qty']);
        $html .= '<tr>
                        
                        
                        <td align="center">' . $itemModel->itemCode . '</td>
                        
                        <td align="right">' . number_format($stValue['costPrice']) . '</td>
                        <td align="right">' . $stValue['qty'] . '</td>
                        <td align="right">' . number_format($totalAmount, 2) . '</td>';
        if (is_numeric($stValue['costPrice']) && is_numeric($stValue['qty'])) {
            $grandTotalAmount += ($stValue['costPrice'] * $stValue['qty']);
        }
        $grandTotalQuantity += (int)$stValue['qty'];
    }
    $html .= '<tr align="right"><td colspan="2"><b>Grand Total :</b></td><td align="right"><b>' . $grandTotalQuantity . '</b></td><td align="right"><b>' . number_format($grandTotalAmount, 2) . '</td></b></td></tr></tbody>';

    $html .= '</table>';

    $html .= '<table autosize="1" cellpadding="20" style="border-collapse:collapse;font-size: 8px;overflow: wrap" width="100%">
                    <tr>
                        <td style="margin-top: 15px" align="center"><hr>Issue By</td>
                        <td style="margin-top: 15px" align="center"><hr>Marketing Manager</td>
                        <td style="margin-top: 15px" align="center"><hr>Receive By</td>
                        <td style="margin-top: 15px" align="center"><hr>Approve By</td>
                    </tr>

        </table>

        <table>
            <tr>
                <td style="width:100%;">'.$barCode.'</td>
            </tr>
        </table>';

    //$mPdf = Yii::app()->ePdf->mpdf('', 'c8');
    $mPdf = Yii::app()->ePdf->mPDF('utf-8', 'A8', 0, '', 0, 0, 0, 0, 0, 0);
    $mPdf->SetDisplayMode('fullpage');
    $mPdf->WriteHTML($html);


    $mPdf->Output('StockTransfer-' . $_POST['StockTransfer']['transferNo'] . '.pdf', 'D');

    exit();
    //$this->redirect('stockTransfer');

}
?>