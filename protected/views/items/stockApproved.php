<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Items<i class="icon-angle-right"></i></li>
    <li>Stock Approved</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'stock-transfer-approved-form',
        'enableAjaxValidation'=>true,
    )); ?>

    <div class="row">
        <?php echo $form->errorSummary($model); ?>
        <div style="margin-left: 0" class="span3">
            <?php echo $form->labelEx($model,'transferNo'); ?>
            <?php echo $form->textField($model,'transferNo',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200)); ?>
            <?php echo $form->error($model,'transferNo'); ?>

        </div>
        <div class="span3">
            <div style="margin-top: 16px;" class="row buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue')); ?>
            </div>
        </div>
    </div>

    <br>


    <div class="row">
        <div style="height:400px; overflow:scroll;">
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                <tr>
                    <th>Sl. No</th>
                    <th>Item Code</th>
                    <th>Bar Code</th>
                    <th>Item Name</th>
                    <th>Size</th>
                    <th>RPU</th>
                    <th>Qty</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if(!empty($stockTransfer)){
                        $i = 1;
                        $grandTotalAmount = 0;
                        $grandTotalQuantity = 0;
                       foreach ($stockTransfer as $value){
                           $item = Items::model()->find('id=:id',array(':id'=>$value['itemId']));
                           $itemVariationModel = ItemsVariation::model()->find('item_id=:item_id',array(':item_id'=>$value['itemId']));
                           $size = '';
                           if(!empty($itemVariationModel)){
                                $size = $itemVariationModel->size;
                           }
                           $totalAmount = ($value['costPrice'] * $value['qty']);
                           ?>
                            <tr>
                                <td><?php echo $value['transferNo'];?></td>
                                <td><?php echo $item->itemCode;?></td>
                                <td><?php echo $item->barCode;?></td>
                                <td><?php echo $item->itemName;?></td>
                                <td><?php echo ItemsVariation::variationAttributesById($size);?></td>
                                <td><?php echo number_format($value['costPrice']);?></td>
                                <td><?php echo $value['qty'];?></td>
                                <td><?php echo $totalAmount;?></td>
                            </tr>
                           <?php
                           if (is_numeric($value['costPrice']) && is_numeric($value['qty'])) {
                               $grandTotalAmount += ($value['costPrice'] * $value['qty']);
                           }
                           $grandTotalQuantity += (int)$value['qty'];
                       }
                       ?>
                            <tr align="right">
                                <td align="right" colspan="6"><b>Grand Total :</b></td>
                                <td align="right"><b><?php echo $grandTotalQuantity;?></b></td>
                                <td align="right"><b><?php echo number_format($grandTotalAmount,2);?></b></td>
                            </tr>
                        <?php
                    }else{
                        ?>
                        <td colspan="8">
                            <?php
                                if(!empty($msg)){
                                    echo '<h3>'.$msg.'</h3>';
                                }else{
                                    echo '<h3>Not Found</h3>';
                                }
                            ?>

                        </td>
                        <?php
                    }

                ?>

                </tbody>
            </table>
        </div>

        <?php
            if(!empty($stockTransfer)){
                ?>
                <a class="btn blue" href="<?php echo Yii::app()->baseUrl . '/items/stockTransferApproved?approved=1&transferNo='.$stockTransfer[0]->transferNo ?>">Approved Stock</a>
                <?php
            }
        ?>
        <?php $this->endWidget(); ?>
    </div>