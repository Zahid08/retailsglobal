<script type="text/javascript" language="javascript">
$(function() 
{
	// quantity operations
	$("#checkAll").change(function() 
	{
		if((document.getElementById('checkAll').checked))
		{
			var box = document.getElementsByClassName('checkedid');
			for (var i = 0; i < box.length; i++) {
				box[i].checked = true;
			}
		}
		else
		{
			var box = document.getElementsByClassName('checkedid');
			for (var i = 0; i < box.length; i++) {
				box[i].checked = false;
			}
		}
	});
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php if(!empty($msg)) :?>
	<?php echo $msg;?>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">Update Price</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="fileld_right">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> Total Pending</th>
								<th><i class="icon-bookmark"></i> Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo (!empty($icnModel))?count($icnModel):0;?></td>
								<td><?php echo date("Y-m-d");?></td>
							</tr>
					   </tbody>
					 </table>
				</div>
			</div>

			 <div id="grnapp_dynamic_container">    
				 <div class="row">
					<div style="height:600px; overflow:scroll;">
					<table class="table table-striped table-hover bordercolumn">
						<thead>
							<tr>
								<th><input type="checkbox" id="checkAll" class="group-checkable" data-set=".actions_table_1 .checkboxes" /></th>
								<th>Sl. No</th>
								<th>ICN No.</th>
								<th class="hidden-480">Item</th>
								<th class="hidden-480">Supplier</th>
								<th class="hidden-480">Change Date</th>
								<th class="hidden-480">Changed Cost Price</th>
								<th class="hidden-480">Changed Sell Price</th>
								<th class="hidden-480">Changed By</th>
								<!--<th class="hidden-480">Actions</th>-->
							</tr>
						</thead>
						<tbody>
						<?php
						$srl = 0;
						if(!empty($icnModel)) :
							foreach($icnModel as $key=>$data) :  $srl++; ?>
									<tr>
										<td><input type="checkbox" class="checkedid" data-set=".actions_table_1 .checkboxes" name="checkedid<?php echo $key;?>" value="1" /></td>
										<td><?php echo $srl;?></td>
										<td><?php echo $data->icnNo;
												  echo CHtml::hiddenField('pk['.$key.']',$key, array());
												  echo CHtml::hiddenField('icnId['.$key.']',$data->id, array());
												  echo CHtml::hiddenField('itemId['.$key.']',$data->itemId, array());
												  echo CHtml::hiddenField('costPrice['.$key.']',$data->costPrice, array());
												  echo CHtml::hiddenField('sellPrice['.$key.']',$data->sellPrice, array());
											?>
										</td>
										<td><?php echo $data->item->itemName;?></td>
										<td class="hidden-480"><?php echo $data->item->supplier->name;?></td>
										<td><?php echo $data->changeDate;?></td>
										<td class="hidden-480"><?php echo $data->costPrice;?></td>
										<td class="hidden-480"><?php echo $data->sellPrice;?></td>
										<td class="hidden-480"><?php echo $data->crBy0->username;?></td>
										<?php /*?><td class="hidden-480">
											<img id="<?php echo $data->id;?>" src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" data-toggle="tooltip" title="" data-original-title="Remove" />
											<img id="<?php echo $data->id;?>" src="<?php echo Yii::app()->baseUrl;?>/media/images/update.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="updateRow" data-toggle="tooltip" title="" data-original-title="Update" />
										</td><?php */?>
									</tr>
							<?php endforeach; 
							else : ?>
								<tr>
									<td><input type="checkbox" class="group-checkable" data-set=".actions_table_1 .checkboxes" /></td>
									<td>#</td>
									<td>#</td>
									<td>-</td>
									<td class="hidden-480">-</td>
									<td>0000-00-00 </td>
									<td class="hidden-480">0</td>
									<td class="hidden-480">0</td>
									<td class="hidden-480">-</td>
									<!--<td class="hidden-480"></td>-->
								</tr>
						 <?php endif;?>
						</tbody>
					</table>
					</div>
				</div>
			</div>
        </div>        
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Update Prices', array('class'=>'btn blue','style'=>'margin-top:10px;')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   