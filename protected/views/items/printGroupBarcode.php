<style type="text/css">
	.invoiceTbl td{ margin:0; padding:0;}
	@media screen
	  {
	  	.invoiceTbl td {font-family:verdana,sans-serif;font-size:11px;}
	  }
	@media print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
	@media screen,print
	  {
	  	.invoiceTbl td {font-size:11px;}
	  }
</style>
<script src="<?php echo Yii::app()->baseUrl;?>/js/jquery.printElement.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
	//-------print options----//
	$(function()
	{
		$("#printIcon").click(function() 
		{
		 	$('#report_dynamic_container').printElement(
				{
					overrideElementCSS:[
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/css/style-metro.css',media:'print'},
						'<?php echo Yii::app()->theme->baseUrl.'/'.Yii::app()->params->skinDefault;?>/plugins/bootstrap/css/bootstrap.min.css',
						{ href:'<?php echo Yii::app()->theme->baseUrl;?>/plugins/bootstrap/css/bootstrap.min.css',media:'print'},
						'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',
						{ href:'<?php echo Yii::app()->request->baseUrl;?>/css/form.css',media:'print'},
					],            
					printBodyOptions:{                         
							styleToAdd:'margin-left:25px !important',
							//classNameToAdd : 'printBody',
					}
			   }
			);
		});
	});
</script>  
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Reports <i class="icon-angle-right"></i></li>
    <li>Item Wise Stock Report</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
 <?php 
	$bName=$bAdd='';
	$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
	if(!empty($companyModel)) :
	      $bName=$companyModel->name;
	endif; 
	$branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
      if(!empty($branchModel)) $bAdd=$branchModel->addressline; 
?>

<div class="form">
    <?php if(!empty($msg)) echo $msg;?>
	<div style="width:98%; padding:10px; border:1px dotted #ccc;">
	<div style="float:left;" class="span8">
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'action'=>Yii::app()->createUrl('items/printGlobalBarcode'),
				'enableAjaxValidation'=>true,				
				
	   )); ?>
        <input type="hidden" name="globalBarcodePrint" value="" id="textGlobalBarcode">
       <div class="row">
            <div class="span6">
                <?php echo CHtml::label('Department','');?>
                <?php echo CHtml::dropDownList('deptId',$dept,Department::getAllDepartment(Department::STATUS_ACTIVE), 
                          array('class'=>'m-wrap combo combobox','prompt'=>'Select Department',
                                'id'=>'deptId',
                                'options'=>array($dept=>array('selected'=>'selected')),
                                'onchange'=>'js:$("#ajax_loaderdept").show()',
                                'ajax' => array('type'=>'POST', 
                                                'data'=>array('deptId'=>'js:this.value'),  
                                                'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
                                                'success'=>'function(data) 
                                                {
                                                    $("#ajax_loaderdept").hide();
                                                    $("#subdeptId").html(data);
                                                }',
                                )
                        )); ?>
                     <span id="ajax_loaderdept" style="display:none;">
                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                     </span>
            </div>
            <div class="span6">
             <?php echo CHtml::label('Sub Department','');?>
             <?php echo CHtml::dropDownList('subdeptId',$subdept,Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), 
                              array('class'=>'m-wrap span','prompt'=>'Select Sub Department',
                                    'id'=>'subdeptId',
                                    'options'=>array($subdept=>array('selected'=>'selected')),
                                    'onchange'=>'js:$("#ajax_loadersubdept").show()',
                                    'ajax' => array('type'=>'POST', 
                                                    'data'=>array('subdeptId'=>'js:this.value'),  
                                                    'url'=>CController::createUrl('ajax/dynamicCategory'),
                                                    'success'=>'function(data) 
                                                    {
                                                        $("#ajax_loadersubdept").hide();
                                                        $("#catId").html(data);
                                                    }',
                                    )
                            )); ?>
                 <span id="ajax_loadersubdept" style="display:none;">
                    <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                 </span>
            </div>           
        </div>
        <div class="row">
              <div class="span6">
                  <?php echo CHtml::label('Ecommerce Items','');?>
                  <?php echo CHtml::dropDownList('isEcommerce',$isEcommerce,array(1=>'Yes',2=>'No'), 
                                  array('class'=>'m-wrap combo combobox','prompt'=>'Select',
                                  'id'=>'bank',
                              )); 
                  ?>
              </div>
              <div class="span6">
                  <?php echo CHtml::label('Child Items','');?>
                  <?php echo CHtml::dropDownList('isParent',$isParent,array(1=>'Yes',2=>'No'), 
                                  array('class'=>'m-wrap combo combobox','prompt'=>'Select',
                                  'id'=>'bank',
                              )); 
                  ?>
              </div>
          </div>
		 <div class="row"> 
			 <div class="span6">
                 <?php echo CHtml::label('Category','');?>
                 <?php echo CHtml::dropDownList('catId',$cat,Category::getAllCategory(Category::STATUS_ACTIVE), array('class'=>'m-wrap span','prompt'=>'Select Category','id'=>'catId',)); ?>
            </div>
			<div class="span6">
            	 <?php echo CHtml::label('Supplier','');?>
				 <?php echo CHtml::dropDownList('supplierId',$supplierId, Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Supplier')); ?>
            </div>		 
		 </div>   
   
        <div class="row">        	
            
            <div class="span6">
                <?php echo CHtml::label('Item Code','');?>
            	<?php echo CHtml::textField('itemCode',$itemCode,array('class'=>'m-wrap span','size'=>60,'maxlength'=>255)); ?>
            </div>
            <div class="span6">
                <?php echo CHtml::label('End Date','');?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       //'model'=>$modelStock,
                       'name'=>'endDate',
                       'id'=>'endDate',					   
                       'value' =>$endDate,  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                       ),
                    ));
                ?>
            </div>             
        </div>
		<div class="row">
		    <div class="span4" style="padding-top:0px;width: 15.623932%;">
                <?php echo CHtml::submitButton('Submit', array('class'=>'btn blue'));?>
            </div>
            <?php
            if ($PrintButtonEnable==1){
            ?>
            <div style="padding-top:0px;" id="collectedBarcode">
            <div style="padding-top:0px;">
                <input class="btn blue" id="GlobalPrintBarcode" type="button" name="yt0" value="Get All Barcode">
            </div>
            </div>
            <?php }?>
        </div>
    <?php $this->endWidget(); ?>
	</div>
		<div class="span3 button_x" style="padding-top:0px; float:right">
		</div>
		<div style="clear:both"></div>

	</div>
    
    <div id="report_dynamic_container">
        <div class="row">
            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="invoiceTbl" style="margin-bottom:10px; font-weight:bold;">
                <tr>
                    <td align="center" style="text-align:center; padding:3px 0px;" colspan="2" class="marpad">
                        <?php 
                            //$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE)); 
                            if(!empty($companyModel)) : ?>
                                <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="width:70px;" />	<br />
                                <?php echo $companyModel->name;
                            endif; 
                        ?>
                    </td> 
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                        <?php 
                           // $branchModel = Branch::model()->findByPk(Yii::app()->session['branchId']); 
                            if(!empty($branchModel)) echo $branchModel->addressline;  
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Item Wise Global Barcode Print</td>
                </tr>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">
                    	<?php if(!empty($deptName)) echo 'Department : '.$deptName.', ';
							  if(!empty($subdeptName)) echo 'Sub Department : '.$subdeptName.', ';
							  if(!empty($suppName)) echo 'Supplier : '.$suppName.', ';
							  if(!empty($itemCode)) echo 'Item Code : '.$itemCode.', ';
							  if(!empty($catName)) echo 'Category : '.$catName; ?>
                    </td>
                </tr>
                <?php if(!empty($endDate)) : ?>
                <tr>
                    <td style="text-align:center;padding:3px 0px;" colspan="2">Date till <?php echo $endDate;?></td>
                </tr>
                <?php endif;?>
            </table>
            
            <table class="table table-striped table-hover bordercolumn">
                <thead>
                    <tr style="border-top:1px solid #ddd;">
                        <th style="text-align: center;vertical-align: middle;"><input type="checkbox" name="all" id="checkall" /></th>
                        <th>Sl. No</th>
                        <th class="hidden-480">Item Code</th>
                        <th class="hidden-480">Print Stock</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Supplier</th>
                        <th class="hidden-480">Ecommerce Item</th>
                        <th class="hidden-480">Child Item</th>
                        <th class="hidden-480">Stock</th>
                        <th class="hidden-480">Total</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
				if(!empty($modelStock) || !empty($modelTransfer)) : 
					$srl = $totalQty  =  $totalAmountStock = $totalAmountTransfer = 0;
					foreach($modelStock as $data) : 
					$srl++; 
                    $stockQty = Stock::getItemWiseStockByDate($data->itemId,$endDate);
                    $amountStock=Stock::getItemFifoCostPrice($data->itemId,$stockQty);
					$totalQty+=$stockQty;
                    $totalAmountStock+=$amountStock;
                        $stock = Stock::getItemWiseStock($data->item->id);
                    ?>
					<tr>
						<td><input type="checkbox" name="type" class="cb-element" value="<?=$data->item->id?>" /></td>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->item->itemCode;?></td>
                        <td><?php echo $stock*2;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
                        <td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
                        <td><?php echo $data->item->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
						<td><?php echo $stockQty;?></td>
                        <td><?php echo UsefulFunction::formatMoney($amountStock, true);?></td>
					</tr>
				<?php endforeach; 
                    foreach($modelTransfer as $data) : 
					$srl++; 
                    $transferQty = Stock::getItemWiseStockByDate($data->itemId,$endDate);
                    $amountTransfer=Stock::getItemFifoCostPrice($data->itemId,$transferQty);
					$totalQty+=$transferQty;
                    $totalAmountTransfer+=$amountTransfer; ?>
					<tr>
                        <td><input type="checkbox" name="type" class="cb-element" value="<?=$data->item->id?>" /></td>
						<td><?php echo $srl;?></td>
						<td><?php echo $data->item->itemCode;?></td>
						<td><?php echo $data->item->itemName;?></td>
                        <td><?php echo $data->item->supplier->name;?></td>
                        <td><?php echo $data->item->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No";?></td>
                        <td><?php echo $data->item->isParent==Items::IS_PARENTS?"Yes":"No";?></td>
						<td><?php echo $transferQty;?></td>
                        <td><?php echo UsefulFunction::formatMoney($amountTransfer, true);?></td>
					</tr>
				<?php endforeach; ?>
                	<tr style="font-weight:bold;">
                      <td colspan="4" align="right">Total : </td>
                      <td>-</td>
                      <td>-</td>
                      <td><?php echo $totalQty;?></td>
                      <td><?php echo UsefulFunction::formatMoney($totalAmountStock+$totalAmountTransfer, true);?></td>
                    </tr>
                 <?php
                 else : ?>
                    <tr>
                        <td>#</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $('#checkall').change(function () {
        $('.cb-element').prop('checked',this.checked);
    });

    $('.cb-element').change(function () {
        if ($('.cb-element:checked').length == $('.cb-element').length){
            $('#checkall').prop('checked',true);
        }
        else {
            $('#checkall').prop('checked',false);
        }
    });

    $('#textGlobalBarcode').show();
    $('#GlobalPrintBarcode').click(function () {
        var ItemList = [];
        //Checked Items Push Into List
        $("input:checkbox[name=type]:checked").each(function(){
            ItemList.push($(this).val());
        });
        
            if (ItemList.length>0) {
                $('#textGlobalBarcode').val(ItemList);
                $('#collectedBarcode').html(' <div style="padding-top:18px;"><lable style="color: green;font-size: 16px;" >Collected Done.For Geting Csv Click Submit !</lable></div>');
            }
        
    });

</script>