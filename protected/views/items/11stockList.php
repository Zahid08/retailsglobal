<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Items<i class="icon-angle-right"></i></li>
    <li>Stock List</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'stock-list-form',
        'enableAjaxValidation'=>true,
    )); ?>

    <div class="row">
        <?php echo $form->errorSummary($model); ?>
        <div style="margin-left: 0" class="span3">
            <?php echo $form->labelEx($model,'itemCode'); ?>
            <?php echo $form->textField($model,'itemCode',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200)); ?>
            <?php echo $form->error($model,'itemCode'); ?>

        </div>
        <div class="span3">
            <div style="margin-top: 16px;" class="row buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array('class'=>'btn blue')); ?>
            </div>
        </div>
    </div>

    <br>

<?php $this->endWidget(); ?>
<div class="row">
    <div style="height:400px; overflow:scroll;">
        <table class="table table-striped table-hover bordercolumn">
            <thead>
            <tr>
                <th>Sl. No</th>
                <th>Branch Name</th>
                <th>Item Code</th>
                <th>Item Name</th>
                <th class="hidden-480">Total Current Quantity </th>
                <th class="hidden-480">Cost Price</th>

            </tr>
            </thead>
            <tbody>
            <?php

           if (!empty($stock)){
               $i = 1;
               foreach ($stock as $value){
                  if(!empty($value['branchId'])) {
                      $items = Items::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $value['itemId'])));
                      ?>
                      <tr class="parentRow" id="0">
                          <td><span class="srlNo"><?php echo $i; ?></span></td>
                          <td>
                              <?php
                              $branch = Branch::model()->find(array('condition' => 'id=:id', 'params' => array(':id' => $value['branchId'])));
                              if (!empty($branch)) {
                                  echo $branch['name'];
                              }
                              ?>
                          </td>
                          <td>
                              <?php
                              if (!empty($items)) {
                                  echo $items->itemCode;
                              }
                              ?>
                          </td>
                          <td>
                              <?php
                              if (!empty($items)) {
                                  echo $items->itemName;
                              }
                              ?>
                          </td>
                          <td>
                              <?php


                              $sql = "SELECT SUM(`qty`) as qty FROM `pos_sales_details` WHERE `itemId` = " . $value['itemId'] . "";
                              $salesDetails = SalesDetails::model()->findBySql($sql);

                              echo $value['qty'] - $salesDetails['qty'];
                              ?>
                          </td>
                          <td><?php echo $value['costPrice']; ?></td>
                      </tr>
                      <?php
                  }else{
                      ?>
                      <td colspan="6"><h3>Not Found</h3></td>
                        <?php
                  }
                   $i++;
               }
           }else{
               ?>
               <td colspan="6"><h3>Not Found</h3></td>
               <?php
           }
            ?>

            </tbody>
        </table>
    </div>
</div>