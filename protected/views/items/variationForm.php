<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });

    // Item Code Check Exits
	$("#ItemsVariation_item_code").change(function() {
		var itemCode = $("#ItemsVariation_item_code").val();

        $.ajax({
			url: "<?php echo Yii::app()->request->baseUrl; ?>/items/itemCodeValidation?itemcode="+itemCode+"",
			type: 'GET',
			dataType: 'json', // added data type
			success: function(res) {
				if(res == 1){
                    $("#ItemsVariation_item_code_em_").show();
                    $("#ItemsVariation_item_code_em_").html('Already Exist.');
                }else{
                    $("#ItemsVariation_item_code_em_").hide();
                }
			}
		});
    });

    // Bar Code Check Exits
    $("#ItemsVariation_bar_code").change(function() {
        var barCode = $("#ItemsVariation_bar_code").val();
        $.ajax({
            url: "<?php echo Yii::app()->request->baseUrl; ?>/items/barCodeValidation?barcode="+barCode+"",
            type: 'GET',
            dataType: 'json', // added data type
            success: function(res) {
                if(res == 1){
                    $("#ItemsVariation_bar_code_em_").show();
                    $("#ItemsVariation_bar_code_em_").html('Already Exist.');
                }else{
                    $("#ItemsVariation_bar_code_em_").hide();
                }
            }
        });
    });
</script>
<style>

    .nwrapper {
        clear: both;
        overflow: hidden;
        display: block;
    }
    .nwrapper .nwrapper-12{
        clear: both;
        overflow: hidden;
    }
    .nwrapper .nwrapper-12 .nwrapper-6 {
        width: 250px;
        float: left;
        margin: 5px;

    }
    .nwrapper .nwrapper-12 .nwrapper-3 {
        width: 23%;
        float: left;
        margin: 5px;
    }
    .nwrapper .nwrapper-12 .nwrapper-6 input, .nwrapper .nwrapper-12 .nwrapper-6 select {
        width: 100%;
    }
 .nwrapper .nwrapper-12 .nwrapper-3 input, .nwrapper .nwrapper-12 .nwrapper-3 select {
        width: 100%;
    }

    .avatar-upload {
        position: relative;
        max-width: 205px;
        margin: 15px auto;
    }

    .avatar-upload .avatar-edit {
        position: absolute;
        right: 12px;
        z-index: 1;
        top: 10px;
    }

    .avatar-upload .avatar-edit input {
        display: none;
    }

    .avatar-upload .avatar-edit input + label {
        display: inline-block;
        width: 34px;
        height: 34px;
        margin-bottom: 0;
        border-radius: 100%;
        background: #FFFFFF;
        border: 1px solid transparent;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
        cursor: pointer;
        font-weight: normal;
        transition: all 0.2s ease-in-out;
    }

    .avatar-upload .avatar-edit input + label:hover {
        background: #f1f1f1;
        border-color: #d6d6d6;
    }

    .avatar-upload .avatar-edit input + label:after {
        content: "\f040";
        font-family: 'FontAwesome';
        color: #757575;
        position: absolute;
        top: 10px;
        left: 0;
        right: 0;
        text-align: center;
        margin: auto;
    }

    .avatar-upload .avatar-preview {
        width: 192px;
        height: 192px;
        position: relative;
        border-radius: 100%;
        border: 6px solid #F8F8F8;
        box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }

    .avatar-upload .avatar-preview > div {
        width: 100%;
        height: 100%;
        border-radius: 100%;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }
    input#ItemsVariation_item_code {
        pointer-events: none;
        background-color: #f9f9f9;
    }
</style>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-variation-form',
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

    <div class="avatar-upload">
        <div class="avatar-edit">
            <?php echo $form->fileField($model, 'image', array('id'=>'imageUpload')); ?>
            <label for="imageUpload"></label>
        </div>
        <div class="avatar-preview">
            <?php
            $imageUrl = "https://via.placeholder.com/150";
            if(!$model->isNewRecord){
                $imageUrl = Yii::app()->baseUrl.'/'.$model->image;
            }else{
                $imageUrl = 'https://via.placeholder.com/150';
            }
            ?>
            <div id="imagePreview" style="background-image: url(<?php echo $imageUrl;?>);">
            </div>
        </div>
    </div>

    <!--<div class="imagewrap">
        <?php /*echo $form->labelEx($model, 'image'); */?>
        <?php /*echo $form->fileField($model, 'image', array('class'=>'variation-image')); */?>
        <?php /*echo $form->error($model, 'image'); */?>
        <?php
/*            if(!$model->isNewRecord){
                */?>
                <div class="variation-image-preview">
                    <img style="width: 70px;margin: 5px" src="<?php /*echo Yii::app()->baseUrl.'/'.$model->image*/?>" alt="">
                </div>
                <?php
/*            }
        */?>

    </div>-->

    <input type="hidden" id="onchangeDataGet" value="" name="onchangeData">

    <div class="nwrapper">
        <div class="nwrapper-12">
            <div class="nwrapper-3">
                <?php echo $form->labelEx($model, 'item_code'); ?>
                <?php echo $form->textField($model, 'item_code', array('class' => 'm-wrap span')); ?>
                <?php echo $form->error($model, 'item_code'); ?>
            </div>
            <div class="nwrapper-3">
                <?php echo $form->labelEx($model, 'bar_code'); ?>
                <?php echo $form->textField($model, 'bar_code', array('class' => 'm-wrap span','readonly'=>true)); ?>
                <?php echo $form->error($model, 'bar_code'); ?>
            </div>
            <div class="nwrapper-3">
                <?php echo $form->labelEx($model, 'cost_price'); ?>
                <?php echo $form->textField($model, 'cost_price', array('class' => 'm-wrap span', 'required' => true)); ?>
                <?php echo $form->error($model, 'cost_price'); ?>
            </div>
            <div class="nwrapper-3">
                <?php echo $form->labelEx($model, 'sell_price'); ?>
                <?php echo $form->textField($model, 'sell_price', array('class' => 'm-wrap span', 'required' => true)); ?>
                <?php echo $form->error($model, 'sell_price'); ?>
            </div>
        </div>
        <div class="nwrapper-12">
            <?php
          /*  $attTypeModel = AttributesType::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>AttributesType::STATUS_ACTIVE)));
            foreach ($attTypeModel as $attType){
                $attributes = Attributes::getAllAttributesByTypes($attType->id,Attributes::STATUS_ACTIVE);
                echo "<pre>";
                print_r($attributes);
                echo "</pre>";
            }*/
            ?>
            <div class="nwrapper-6">
                <?php echo $form->labelEx($model, 'color'); ?>
                <?php echo $form->dropDownList($model,'color', Attributes::getAllAttributesByTypes(2,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Color', 'required' => true)); ?>
                <?php echo $form->error($model, 'color'); ?>
            </div>
            <div class="nwrapper-6">
                <?php echo $form->labelEx($model, 'size'); ?>
                <?php echo $form->dropDownList($model,'size', Attributes::getAllAttributesByTypes(1,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Size', 'required' => true)); ?>
                <?php echo $form->error($model, 'size'); ?>
            </div>
        </div>
        <div class="nwrapper-12">
            <div class="nwrapper-6">
                <?php
                /*$stockQuantity = Stock::model()->find(
                    array(
                        'select'=>'itemId, SUM(qty) as qty',
                        'condition'=>'itemId=:itemId',
                        'params'=>array(':itemId'=>$model->item_id)
                    )

                );*/

                if(!$model->isNewRecord){
                    $stock = Stock::getItemWiseStock($model->item_id);
                    if(!empty($stock)){
                        $model->quantity = $stock;
                    }
                }

                ?>
                <?php echo $form->labelEx($model, 'quantity'); ?>
                <?php echo $form->textField($model, 'quantity', array('class' => 'm-wrap span', 'required' => true)); ?>
                <?php echo $form->error($model, 'quantity'); ?>
            </div>
            <div class="nwrapper-6">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(Attributes::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Status','required' => true)); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
        </div>
    </div>

    <div class="buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
    <?php $this->endWidget(); ?>
</div>


<script>

    $('#ItemsVariation_color, #ItemsVariation_size').on('change', function(){
       $('#onchangeDataGet').val(1);
    });
</script>