<script type="text/javascript" language="javascript">
$(function() 
{
	// after customer inputed
	$("#itemCode").blur(function() 
	{
		$("#item_loader").show("slow");
		var itemCode  = $(this).val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/getItemInfoByCode/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemCode : itemCode,
			},
			success: function(data) 
			{
				$("#item_loader").hide();
				$("#item_dynamic_container").html(data);
			},
			error: function() {}
		});
	});
	
	// use item from search fill with item code
	$("#useItems").click(function() 
	{
		var itemName  = $("#itemId").val();	
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/itemCodeByTitle/";
		$.ajax({
			type: "POST",
			url: urlajax,
			data: 
			{ 
				itemName : itemName,
			},
			success: function(data) 
			{
				$("#itemCode").val(data);
				
				$("#item_loader").show("slow");	
				var urlajaxitems = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/getItemInfoByCode/";
				$.ajax({
					type: "POST",
					url: urlajaxitems,
					data: 
					{ 
						itemCode : data,
					},
					success: function(dataitems) 
					{
						$("#item_loader").hide();
						$("#item_dynamic_container").html(dataitems);
						$("#itemId").val("");
					},
					error: function() {}
				});
			},
			error: function() {}
		});
			
	});
	
});
</script>

<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store<i class="icon-angle-right"></i></li>
    <li>Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'itemsprice-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
	<?php echo $msg;?>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">Change Price</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span6" style="width:500px;">
					<?php echo CHtml::label('Item Code',''); ?>
					<?php echo CHtml::textField('itemCode','',array('id'=>'itemCode','class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
					<span id="item_loader" style="display:none;">
						<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
					</span>
					<a class="btn btn-primary" data-toggle="modal" href="#search_info" style="margin:0 15px; font-size:12px;"><i class="icon-search"></i> Search Items</a>
				</div>
				<div class="fileld_right">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th><i class="icon-bookmark"></i> Item Change Number</th>
								<th><i class="icon-bookmark"></i> Item Change Date</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>ICN<?php echo date("Ymdhis"); echo CHtml::hiddenField('icnNo','ICN'.date("Ymdhis"),array());?></td>
								<td><?php echo date("Y-m-d");?></td>
							</tr>
					   </tbody>
					 </table>
				</div>
			</div>
			
			<div id="search_info" class="modal hide fade" tabindex="-1" data-width="470" role="dialog" aria-labelledby="klarnaSubmitModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4>Search by item title : </h4>
				</div>
				<div class="modal-body">
					<?php $this->widget('CAutoComplete',array(
								 //'model'=>$model,
								 'id'=>'itemId',
								 //'attribute' => 'custId',
								 //name of the html field that will be generated
								 'name'=>'itemId', 
								 //replace controller/action with real ids
								 //'value'=>($model->custId)?$model->cust->name:'',
					
								 'url'=>array('ajax/autoCompleteItems'), 
								 'max'=>100, //specifies the max number of items to display
	 
											 //specifies the number of chars that must be entered 
											 //before autocomplete initiates a lookup
								 'minChars'=>2, 
								 'delay'=>500, //number of milliseconds before lookup occurs
								 'matchCase'=>false, //match case when performing a lookup?
								 'mustMatch' => true,
											 //any additional html attributes that go inside of 
											 //the input field can be defined here
								 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
															
								 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
								 //'extraParams' => array('taskType' => 'desc'),
								 'methodChain'=>".result(function(event,item){})",
								 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
							));	
						?>
					    <button style="margin-left:10px;" type="button" data-dismiss="modal" class="btn" id="useItems">Use Items</button>
				</div>
			</div>
			
			<div id="item_dynamic_container">
				<div class="row">
					<table class="table table-striped table-bordered table-advance table-hover">
						<thead>
							<tr>
								<th>Item Name</th>
								<th>Bar Code</th>
								<th>Brand</th>
								<th>Supplier</th>
								<th>Tax/vat</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="highlight"><div class="success"></div>&nbsp;&nbsp;-</td>
								<td class="highlight"><div class="info"></div>&nbsp;&nbsp;-</td>
								<td class="highlight"><div class="success"></div>&nbsp;&nbsp;-</td>
								<td class="highlight"><div class="warning"></div>&nbsp;&nbsp;-</td>
								<td class="highlight"><div class="important"></div>&nbsp;&nbsp;0 %</td>
							</tr>
						</tbody>
					</table>
				 </div>
				 
				 <div class="row">	
					<div class="span4">
						<?php echo CHtml::label('Cost Price',''); ?>
						<?php echo CHtml::textField('costPrice',0,array('class'=>'m-wrap large')); ?>
					</div>
					<div class="span4">
						<?php echo CHtml::label('Sell Price',''); ?>
						<?php echo CHtml::textField('sellPrice',0,array('class'=>'m-wrap large')); ?>
					</div>
				</div>
			</div>
        </div>
    </div><!-- portlet body -->
	<div class="row buttons">
		<?php echo CHtml::submitButton('Change Price', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   