<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Items <i class="icon-angle-right"></i></li>
    <li>Details Items # <?php echo $model->id; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php if($model->isEcommerce==Items::IS_ECOMMERCE) : ?>
<div class="form">
	<div class="span6">
    <h4>Attributes</h4>
	<?php 
		if(!empty($model->itemAttributes)) : 
			foreach($model->itemAttributes as $key=>$data) : 
				if($data->attType->isHtml==AttributesType::STATUS_ACTIVE)
					echo '<div class="row"><label style="float:left;">'.$data->attType->name.'&nbsp;:&nbsp;</label><label style="float:left;width:20px;height:20px;background:'.$data->att->name.'"></label></div>';
				else echo '<div class="row">'.$data->attType->name.'&nbsp;:&nbsp;'.$data->att->name.'</div>';
			endforeach;
		endif; 
	?> 
    </div>	
    <div class="span6">
    <h4>Images</h4>
	<?php 
		if(!empty($model->itemImages)) : 
			foreach($model->itemImages as $iKey=>$iData) : 
				echo '<div class="span3"><img src="'.$iData->image.'" style="width:100%;" /></div>';
			endforeach;
		endif; 
	?> 
    </div>	
</div>
<div class="clearfix">&nbsp;</div>
<?php endif;?>

<?php if($model->isParent==Items::IS_PARENTS) : ?>
<div class="form">
    <h4>Parent Items</h4>
    <table class="table table-striped table-hover bordercolumn">
        <thead>
            <tr>
                <th>Sl. No</th>
                <th>Item Code</th>
                <th class="hidden-480">Description</th>
                <th class="hidden-480">Quantity</th>
                <th class="hidden-480">Cost Price</th>
                <th class="hidden-480">Sell Price</th>
                <th>Net Price</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $totalQty = $totalnetAmount = 0;
            foreach(ItemParents::getAllItemParentsByChield($model->id,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)  :
                $totalQty+= $dataParents->qty;
                $totalnetAmount+= $dataParents->qty*$dataParents->parent->costPrice;
            ?>
            <tr class="parentRow" id="<?php echo $keyParents;?>">
                <td><span class="srlNo"><?php echo $keyParents+1;?></span></td>
                <td class="hidden-480"><?php echo $dataParents->parent->itemCode;?></td>
                <td><span class="itemName"><?php echo $dataParents->parent->itemName;?></span></td>
                <td class="hidden-480"><?php echo $dataParents->qty;?></td>
                <td class="hidden-480"><span><?php echo round($dataParents->parent->costPrice,2);?></span></td>
                <td class="hidden-480"><span><?php echo round($dataParents->parent->sellPrice,2);?></span></td>
                <td><span><?php echo round($dataParents->qty*$dataParents->parent->costPrice,2);?></span></td>
            </tr>    
            <?php endforeach; ?>
            <tr style="font-weight:bold;">
                <td align="right" colspan="3">Total : </td>
                <td><?php echo $totalQty;?></td>
                <td></td>
                <td></td>
                <td><?php echo round($totalnetAmount,2);?></td>
            </tr>
        </tbody>
    </table>
</div> 
<?php endif;?>

<div class="form">
<h4>General Informations</h4>    
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'itemCode',
		'barCode',
		'itemName',
		'costPrice',
		'sellPrice',
		array(            
			'label'=>'Department',  
			'value'=>$model->cat->subdept->dept->name,
		  ),
		 array(            
			'label'=>'Sub Department',  
			'value'=>$model->cat->subdept->name,
		  ),
		  array(            
			'label'=>'Category',  
			'value'=>$model->cat->name,
		  ),
		array(            
			'label'=>'Brand',  
			'value'=>$model->brand->name,
		),
		array(            
			'label'=>'Supplier',  
			'value'=>$model->supplier->name,
		),
		array(            
			'label'=>'Tax',  
			'type'=>'raw',
			'value'=>$model->tax->taxRate.'%&nbsp;'.Lookup::item('Tax',$model->tax->taxType),
		),
		'negativeStock',
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		array(            
			'name'=>'isEcommerce',  
			'value'=>$model->isEcommerce==Items::IS_ECOMMERCE?"Yes":"No",
		  ),
		array(            
			'name'=>'isFeatured',  
			'value'=>$model->isFeatured==Items::IS_ECOMMERCE?"Yes":"No",
		  ),
		array(            
			'name'=>'isSales',  
			'value'=>$model->isSales==Items::IS_ECOMMERCE?"Yes":"No",
		  ),
        array(            
			'name'=>'isParent',  
			'value'=>$model->isParent==Items::IS_PARENTS?"Yes":"No",
		  ),
		array(            
			'label'=>'Weighted/Not',  
			'value'=>$model->isWeighted,
		  ),
		array(            
			'label'=>'Current Stock',  
			'value'=>Stock::getItemWiseStock($model->id),
		),
		array(            
			'label'=>'Current GP(%)',  // current price
			'value'=>round(((($model->sellPrice-$model->costPrice)*100)/$model->sellPrice),2), 
			// gp = (profit/sells)% , // cp = (profit/cost)%
		),
		array(            
			'label'=>'Standard GP(%)',  // ist time price
			'value'=>round(Items::getStandardProfit($model->id),2),
		), 
		array(            
			'name'=>'specification',  
			'type'=>'raw',
			'value'=>$model->specification,
		),
		array(            
			'name'=>'description',  
			'type'=>'raw',
			'value'=>$model->description,
		),
		array(            
			'name'=>'shipping_payment',  
			'type'=>'raw',
			'value'=>$model->shipping_payment,
		),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
	),
)); ?>
</div>
