<?php
// print invoice
if(!empty($printId) && is_numeric($printId))
{
    $this->renderPartial('rePrintStockTransfer', array('printId'=>$printId,'toBranchId'=>$toBranchId,'transferNo'=>$transferNo));
}
?>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'sales-invoice-form',
        'enableAjaxValidation'=>true,
    )); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if(!empty($msg)) : echo $msg; else : ?>
        <div class="notification note-error">
            <p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
        </div>
    <?php endif;?>
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;Print Stock Transfer</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <?php if(!empty($invoice) && is_numeric($invoice) && Branch::getInvoiceLayoutValue(Yii::app()->session['branchId'])==Branch::IS_FULL_INVOICE_LAYOUT) : ?>
                    <div class="row">
                        <ol class="breadcrumb">
                            <li><?php echo CHtml::link('<i class="fa fa-print"></i> Customer Copy',array('salesInvoice/printInvoice','invoice'=>$invoice,'type'=>'invoice'),array('class'=>'btn btn-outline btn-primary'));?></li>
                            <li><?php echo CHtml::link('<i class="fa fa-print"></i> Challan Copy',array('salesInvoice/printInvoice','invoice'=>$invoice,'type'=>'challan'),array('class'=>'btn btn-outline btn-primary'));?></li>
                        </ol>
                    </div>
                <?php else : ?>
                    <div class="row">

                        <div class="span3">
                            <?php echo $form->labelEx($model,'fromBranchId'); ?>
                            <?php echo Yii::app()->session['branchName']; ?>
                        </div>
                        <div class="span3">
                            <?php echo $form->labelEx($model,'toBranchId'); ?>
                            <?php echo $form->dropDownList($model,'toBranchId',Branch::getBranchExceptSigned(Branch::STATUS_ACTIVE),array('class'=>"large m-wrap",'empty'=>'Please Select a Store','required'=>true)); ?>
                            <?php echo $form->error($model,'toBranchId'); ?>
                        </div>

                        <div class="span4" style="margin-left: 142px;">
                            <?php echo $form->labelEx($model,'transferNo'); ?>
                            <?php $this->widget('CAutoComplete',array(
                                'model'=>$model,
                                'id'=>'transferNo',
                                'attribute' => 'transferNo',
                                //name of the html field that will be generated
                                //'name'=>'custId',
                                //replace controller/action with real ids
                                'value'=>($model->transferNo)?$model->transferNo:'',

                                'url'=>array('ajax/autoCompleteStockTransfer'),
                                'max'=>100, //specifies the max number of items to display

                                //specifies the number of chars that must be entered
                                //before autocomplete initiates a lookup
                                'minChars'=>2,
                                'delay'=>500, //number of milliseconds before lookup occurs
                                'matchCase'=>false, //match case when performing a lookup?
                                'mustMatch' => true,
                                //any additional html attributes that go inside of
                                //the input field can be defined here
                                'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20),
                                'extraParams' => array('branchTo' => 'js:function() { return $("#StockTransfer_toBranchId").val(); }'),
                                //'extraParams' => array('taskType' => 'desc'),
                                'methodChain'=>".result(function(event,item){})",
                                //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
                            ));
                            ?>
                            <?php echo $form->error($model,'transferNo'); ?>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Print Stock Transfer', array('id'=>'submitInvoice', 'class'=>'btn blue','style'=>'margin-top:10px;')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div> <!-- form -->