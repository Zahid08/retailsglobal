<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript" language="javascript">
$(function() 
{

    $('.select2').select2();

    /*$.fn.appender = function (settings) {
        let appendArea = this;
        let rowHtml = $(settings.rowSection)[0].outerHTML;



            if (!$model->isNewRecord){
                ?>
                settings.hideSection ? $(settings.rowSection).remove() : "";

            }


        //settings.hideSection ? $(settings.rowSection).remove() : "";
        var numItems = $('.variable-item .varition-single-item').length;

        let rowCounter = numItems;

        if (settings.rowNumberStart) {
            rowCounter = Number(settings.rowNumberStart);
        }

        $(document).on('click', settings.addBtn, function () {

            $(appendArea).append(rowHtml);

            $('.variable-item').on('change', function(event){
                event.preventDefault();
                var form = $('form')[0]; // You need to use standard javascript object here
                var formData = new FormData(form);

                $.ajax({
                    url: " //echo Yii::app()->request->baseUrl; ?>/index.php/ajax/uploadVariationImage/",
                    method:"POST",
                    data: formData,
                    async: true,
                   dataType: 'json',
                    contentType: false,
                    cache:false,
                    processData:false,
                    beforeSend: function (xhr) {
                        $('.upload-error').show();
                        //$('.upload-error').html('<p style="color:green">Image Uploading.....</p>');

                        var new_text = event.target.id.replace("#", ".");
                        $('.'+new_text).attr("src", " //echo Yii::app()->request->baseUrl; ?>/media/icons/loader.gif");
                    },
                    success:function(data){
                        var idspilt = event.target.id.split('_');
                        var new_text = event.target.id.replace("#", ".");
                        $('.delete-image_'+idspilt[1]).show();
                        $('.'+new_text).attr("src", data.image[event.target.files[0].name]);
                        /!*console.log(event);*!/
                        $('.upload-error').html('<p style="color:green">Successfully Upload Image.</p>');
                        setTimeout(function(){
                            $(".upload-error").attr("style", "display:none");
                        }, 5000);
                    },
                    error: function () {
                        alert("Something went wrong")
                    },
                });
            });
            $('.delete-image').on('click', function (event) {
                event.preventDefault();
                console.log(event);
                var deleteClassId = event.target.className.split('_');
                var imagePath = $('.ItemsVariation_'+deleteClassId[1]+'_image').attr("src")
                var deleteFile = confirm("Do you really want to Delete?");
                if (deleteFile == true) {
                    // AJAX request
                    $.ajax({
                        url: "//echo Yii::app()->request->baseUrl; ?>/index.php/ajax/uploadVariationImage/",
                        type: 'post',
                        data: {path: imagePath,request: 2},
                        success: function(response){

                            // Remove <div >
                            if(response == 1){
                                $('.ItemsVariation_'+deleteClassId[1]+'_image').attr("src", "https://via.placeholder.com/150");
                                $('#ItemsVariation_'+deleteClassId[1]+'_image').val('');
                            }

                        }
                    });
                }

            });
            $(settings.rowSection).last().addClass(settings.addClass);

            $(settings.rowSection).last().find('.varition-single-item').attr('data-id', rowCounter);
            $(settings.rowSection).last().find('.preview-image').attr('class', 'ItemsVariation_'+rowCounter+'_image');
            $(settings.rowSection).last().find('#ytItemsVariation_0_image').attr('name', 'ItemsVariation['+rowCounter+'][image]');
            $(settings.rowSection).last().find('#ItemsVariation_0_image').attr('name', 'ItemsVariation['+rowCounter+'][image]');
            $(settings.rowSection).last().find('#ItemsVariation_0_color').attr('name', 'ItemsVariation['+rowCounter+'][color]');
            $(settings.rowSection).last().find('#ItemsVariation_0_size').attr('name', 'ItemsVariation['+rowCounter+'][size]');
            $(settings.rowSection).last().find('#ItemsVariation_0_quantity').attr('name', 'ItemsVariation['+rowCounter+'][quantity]');
            $(settings.rowSection).last().find('#ItemsVariation_0_image').attr('id', 'ItemsVariation_'+rowCounter+'_image');
            $(settings.rowSection).last().find('.ItemsVariation_0_image').attr('class', 'ItemsVariation_'+rowCounter+'_image');
            $(settings.rowSection).last().find('#delete-image-id').attr('class', 'delete-image_'+rowCounter+'');

            $(settings.rowSection).last().find("#ItemsVariation_0_color option:first").attr('selected','selected');
            $(settings.rowSection).last().find("#ItemsVariation_0_size option:first").attr('selected','selected');

            $(settings.rowNumber).last().text(rowCounter);
            rowCounter++;
        });


        if(settings.deleteBtn) {
            $(document).on('click', settings.deleteBtn, function (e) {
                e.target.closest(settings.rowSection).remove();
            })
        }
    };*/

    $('.delete-image').on('click', function (event) {
        event.preventDefault();
        console.log(event);
        var deleteClassId = event.target.className.split('_');
        var imagePath = $('.ItemsVariation_'+deleteClassId[1]+'_image').attr("src")
        var deleteFile = confirm("Do you really want to Delete?");
        if (deleteFile == true) {
            // AJAX request
            $.ajax({
                url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/uploadVariationImage/",
                type: 'post',
                data: {path: imagePath,request: 2},
                success: function(response){

                    // Remove <div >
                    if(response == 1){
                        $('.ItemsVariation_'+deleteClassId[1]+'_image').attr("src", "https://via.placeholder.com/150");
                        $('#ItemsVariation_'+deleteClassId[1]+'_image').val('');
                    }

                }
            });
        }

    });


    $('.variable-item').on('change', function(event){
        event.preventDefault();
        var form = $('form')[0]; // You need to use standard javascript object here
        var formData = new FormData(form);

        $.ajax({
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/uploadVariationImage/",
            method:"POST",
            data: formData,
            async: true,
            dataType: 'json',
            contentType: false,
            cache:false,
            processData:false,
            beforeSend: function (xhr) {
                $('.upload-error').show();
                //$('.upload-error').html('<p style="color:green">Image Uploading.....</p>');

                var new_text = event.target.id.replace("#", ".");
                $('.'+new_text).attr("src", "<?php echo Yii::app()->request->baseUrl; ?>/media/icons/loader.gif");
            },
            success:function(data){
                var idspilt = event.target.id.split('_');
                var new_text = event.target.id.replace("#", ".");
                $('.delete-image_'+idspilt[1]).show();
                $('.'+new_text).attr("src", data.image[event.target.files[0].name]);

                /*console.log(event);
                console.log(event.target.files[0].name);
*/
               /* console.log(data);
                console.log(event);*/
                if(data.warning){
                    $('.upload-error').html('<p style="color:red">'+data.warning+'</p>');
                }else if(data.invalid){
                    $('.upload-error').html('<p style="color:red">'+data.invalid+'</p>');
                }else if(data.image){
                    $('.upload-error').html('<p style="color:green">Successfully Upload Image.</p>');
                }

                setTimeout(function(){
                    $(".upload-error").attr("style", "display:none");
                }, 6000);
            },
            error: function () {
                alert("Something went wrong")
            },
        });
    });

	// delete individual pictures
	$(".remove_img").click(function()
	{
		if(confirm("Are you sure want to remove?"))
		{
			var id  = $(this).attr('id');	
			var url = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/deleteItemImageById/";
			$.ajax({
				type: "POST",
				url: url,
				beforeSend: function(data) 
				{
					$("#"+id).find('i').removeClass('fa fa-times').addClass('fa fa-circle-o-notch');
				},
				data: 
				{ 
					id : id,
				},
				success: function(id) 
				{
					$("#"+id).parent().parent().remove();
				},
				error: function() {}
			});
		}
	});
	
	// Add new item rows after last item
    $(".parentRow .codeblur").focus();
    
    <?php if($model->isParent!=Items::IS_PARENTS) : ?> var id = 0;
    <?php else : ?> var id = $('table.bordercolumn tr:last').attr('id'); <?php endif;?>
    
	$(".codeblur").blur(function() 
	{
		var inputId = $(this).attr("id");
		var lastRowId = $('table.bordercolumn tr:last').attr('id');
		$("#ajax_loaderCode"+inputId).show("slow");
		
		var input = $(this);
		var code  = input.val();
		var master = $("table.bordercolumn");				
		var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/ajax/productByCode/";
		
		$.ajax({
			type: "POST",
			dataType: "json",
			url: urlajax,
			data: 
			{ 
				code : code,
			},
			success: function(data) 
			{
				$("#ajax_loaderCode"+inputId).hide();
				
				if(data=="null") {}
				else if(data=="invalid") 
				{
					input.val("");
					input.focus();
					$("#ajax_errorCode"+inputId).html("Invalid Item Code !");	
				}
				else 
				{
					$("#ajax_errorCode"+inputId).html("");
								
					// quantity operations
					if(data.isWeighted=="yes") {
						var qtyId = "qtywt"; var qtyPlaceholder = "0.0";
						var type = 2;
					}
					else {
						var qtyId = "qty"; var qtyPlaceholder = "0";
						var type = 1;	
					}	
					
					// create dynamic Row for current row input
					if(inputId==lastRowId)
					{
						id++;
						// Get a new row based on the prototype row
						var prot = master.find(".parentRow").clone(true);			
						prot.attr("id", id)
						prot.attr("class", "chieldRow" + id)
						
						// loader error and srl processing
						prot.find(".ajax_loaderCode").attr("id", "ajax_loaderCode"+id);
						prot.find(".ajax_errorCode").attr("id", "ajax_errorCode"+id);
						
						prot.find(".ajax_loaderQty").attr("id", "ajax_loaderQty"+id);
						prot.find(".ajax_errorQty").attr("id", "ajax_errorQty"+id);
						
						prot.find(".codeblur").attr("id",id);
						prot.find(".srlNo").html(id+1);
						prot.find(".codeblur").attr("id",id);
						
						// item quantity next row		
						prot.find(".qtyblur").attr("name", "qty[" + id +"]"); 
						prot.find(".qtyblur").attr("id", "qty" + id); 
						prot.find(".qtyblur").attr("placeholder", 0); 
						input.val(data.itemCode); 
						
						// quantity current weighted or not
                        $("#qty"+inputId).val(""); $("#qty"+inputId).attr("placeholder", qtyPlaceholder); 
                        
						$("#qty"+inputId).attr("id", qtyId + inputId);  
						prot.find(".typeblur").attr("id", "type" + id); prot.find(".typeblur").attr("name", "type[" + id +"]"); $("#type"+inputId).val(type); 
						
						// item processing
						prot.find(".pk").attr("name", "pk[" + id +"]"); prot.find(".pk").attr("id", "pk" + id); $("#pk"+inputId).val(inputId);
						prot.find(".proId").attr("name", "proId[" + id +"]"); prot.find(".proId").attr("id", "proId" + id); $("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						prot.find(".itemName").attr("id", "itemName" + id); $("#itemName"+inputId).html(data.itemName);
						prot.find(".sellPrice").attr("id", "sellPrice" + id); $("#sellPrice"+inputId).html(data.costPrice); 
						prot.find(".sellPriceAmount").attr("id", "sellPriceAmount" + id); prot.find(".sellPriceAmount").attr("name", "sellPriceAmount[" + id +"]"); $("#sellPriceAmount"+inputId).val(data.costPrice);
						prot.find(".netTotal").attr("id", "netTotal" + id); $("#netTotal"+inputId).html(0.00);
						
						// final binding and clone with reset values
						prot.find(".codeblur").attr("value", "");
						prot.find(".itemName").html("");
						prot.find(".qtyblur").val("0");
						prot.find(".sellPrice").html("0.00");
						prot.find(".netTotal").html("0.00");
						master.find("tbody").append(prot);
					}
					else  // last blank row
					{
						// item processing
						$("#proId"+inputId).val(data.itemId); // prot.find(".proId").attr("value", data.itemId);
						$("#itemName"+inputId).html(data.itemName);
						$("#sellPrice"+inputId).html(data.costPrice); 
						$("#sellPriceAmount"+inputId).val(data.costPrice);
						
						// quantity current weighted or not
						var qtyCurrentId = ( $("#qty"+inputId).attr("id") || $("#qtywt"+inputId).attr("id") );
						// $("#"+qtyCurrentId).val(""); $("#"+qtyCurrentId).attr("placeholder", qtyPlaceholder); 
						$("#"+qtyCurrentId).attr("id", qtyId + inputId); 
						$("#type"+inputId).val(type); 
						
						// sum calculations
						var sumQty = 0;
						var sumWeight = 0;
						var sumPrice = 0;
						
						for(var i=0;i<=lastRowId;i++)
						{
							var pk = $("#pk"+i).val();
							if(i==pk) 
							{
								if($("#qty"+i).val()=="") var sumQtyId = 0;
								else if(typeof($("#qty"+i).val())==="undefined") {
									var sumQtyId = 0;
									var sumQtyWtId = $("#qtywt"+i).val();
								}
								else var sumQtyId = $("#qty"+i).val();
								
								if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
								else if(typeof($("#qtywt"+i).val())==="undefined") {
									var sumQtyId = $("#qty"+i).val();
									var sumQtyWtId = 0;
								}
								else var sumQtyWtId = $("#qtywt"+i).val();
					
								if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
								if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
								var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
								if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
								
								sumQty+=parseFloat(sumQtyId); 
								sumWeight+=parseFloat(sumQtyWtId);
								sumPrice+=totalQtyId*parseFloat($("#sellPrice"+i).html());
							}
						}
						
						// value placement/update
						$("#grandQty").html(sumQty);
						$("#grandWeight").html(sumWeight);
						$("#totalAmount").html(sumPrice.toFixed(2));
					}
				}
			},
			error: function() {
				$("#ajax_errorCode"+inputId).html("Invalid Item Code !");
			}
		});
	});	
	
	// quantity operations		
	$(".qtyblur").keyup(function() 
	{
		var sumQty = 0;
		var sumWeight = 0;
		var sumPrice = 0;
		
		var isWeight = $(this).attr('placeholder');
		if(isWeight=='0.0') var qtyIdArr = $(this).attr("id").split('qtywt');	
		else var qtyIdArr = $(this).attr("id").split('qty');
		
		// sell price
		var qtyId = qtyIdArr[1];
		var qty   = $(this).val(); if(qty=="") qty = 0;
		var sellPr = $("#sellPrice"+qtyId).html();
		var netPrice = qty*sellPr;
		
		//$("#ajax_loaderQty"+qtyId).show("slow");
		var input = $(this);
		
        //$("#ajax_loaderQty"+qtyId).hide();
        $("#ajax_errorQty"+qtyId).html("");
        $("#netTotal"+qtyId).html(netPrice.toFixed(2));

        // sum calculations
        var lastRowId = $('table.bordercolumn tr:last').attr('id');
        for(var i=0;i<=lastRowId;i++)
        {
            var pk = $("#pk"+i).val();
            if(i==pk) 
            {
                if($("#qty"+i).val()=="") var sumQtyId = 0;
                else if(typeof($("#qty"+i).val())==="undefined") {
                    var sumQtyId = 0;
                    var sumQtyWtId = $("#qtywt"+i).val();
                }
                else var sumQtyId = $("#qty"+i).val();

                if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
                else if(typeof($("#qtywt"+i).val())==="undefined") {
                    var sumQtyId = $("#qty"+i).val();
                    var sumQtyWtId = 0;
                }
                else var sumQtyWtId = $("#qtywt"+i).val();

                if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
                if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
                var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
                if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;

                sumQty+=parseFloat(sumQtyId); 
                sumWeight+=parseFloat(sumQtyWtId);
                sumPrice+=totalQtyId*parseFloat($("#sellPrice"+i).html());
            }
        }

        // value placement/update
        $("#grandQty").html(sumQty);
        $("#grandWeight").html(sumWeight);
        $("#totalAmount").html(sumPrice.toFixed(2));
        $("#Items_costPrice").val(sumPrice.toFixed(2));
	});
    
    // Remove items functionality
	$("table.bordercolumn img.removeRow").live("click", function() 
	{
		var parentRow = $(this).parents("tr").attr('id');
		if(parentRow>0) // not parentRow
		{
			$(this).parents("tr").remove();  
			// sum calculations
			var sumQty = 0;
			var sumWeight = 0;
			var sumPrice = 0;
			var lastRowId = $('table.bordercolumn tr:last').attr('id');
			id = lastRowId;
			
			for(var i=0;i<=lastRowId;i++)
			{
				var pk = $("#pk"+i).val();
				if(i==pk) 
				{
					if($("#qty"+i).val()=="") var sumQtyId = 0;
					else if(typeof($("#qty"+i).val())==="undefined") {
						var sumQtyId = 0;
						var sumQtyWtId = $("#qtywt"+i).val();
					}
					else var sumQtyId = $("#qty"+i).val();
					
					if($("#qtywt"+i).val()=="") var sumQtyWtId = 0;
					else if(typeof($("#qtywt"+i).val())==="undefined") {
						var sumQtyId = $("#qty"+i).val();
						var sumQtyWtId = 0;
					}
					else var sumQtyWtId = $("#qtywt"+i).val();
				
					if(sumQtyId=="" || isNaN(sumQtyId)) sumQtyId = 0;
					if(sumQtyWtId=="" || isNaN(sumQtyWtId)) sumQtyWtId = 0;
					var totalQtyId = parseFloat(sumQtyId)+parseFloat(sumQtyWtId);
					if(totalQtyId=="" || isNaN(totalQtyId)) totalQtyId = 0;
					
					sumQty+=parseFloat(sumQtyId); 
					sumWeight+=parseFloat(sumQtyWtId);
					sumPrice+=totalQtyId*parseFloat($("#sellPrice"+i).html());
				}
			}
			
			// value placement/update
			$("#grandQty").html(sumQty); 
			$("#grandWeight").html(sumWeight);
			$("#totalAmount").html(sumPrice.toFixed(2));
            $("#Items_costPrice").val(sumPrice.toFixed(2));
			$("table.bordercolumn tr:last .codeblur").focus();
		}
		else alert("First row can,t be removed !");	
	});
	
	// protect float and string
	$('.qtyblur').on('keypress', function(ev) 
	{
		var isWeight = $(this).attr('placeholder');
		var keyCode = window.event ? ev.keyCode : ev.which;
		
		if(isWeight=='0') // not isWeighted
		{
			//codes for 0-9
			if (keyCode < 48 || keyCode > 57) 
			{
				//codes for backspace, delete, enter
				if (keyCode != 0 && keyCode != 8 && keyCode != 13 && !ev.ctrlKey) {
					ev.preventDefault();
				}
			}
		}
	});
	$('#Items_variation_type').on('change', function () {
         var value = this.value;
         if(value == 1){
             $('.variable-v').hide();
             $('.simple-v').show();
         }
         if(value == 2){
             $('.variable-v').show();
             $('.simple-v').hide();
         }
    });
    /*$("#appendHere").appender({
        rowSection: '.row_container',
        addBtn: '.add-new-item',
        appendEffect: 'fade',
        addClass: 'animated bounceInLeft',
        rowNumber: '.row-number',
        deleteBtn: '.delete-btn',
        hideSection: true
    });*/
});
</script>
<style>
    div#delete-image-id {
        background: red;
        display: inline-block;
        color: #fff;
        margin: 5px;
        padding: 0px 10px;
        cursor: pointer;
    }
    .upload-error{
        text-align: center;
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0px;
        font-size: 18px;
    }
    .add-new-item {
        position: absolute;
        right: 0;
        bottom: 0;
        background: #48C3E9;
        padding: 10px 15px;
        display: block;
        color: #fff;
        cursor: pointer;
        z-index: 999;
    }
    .delete-btn {
        color: #fff;
        background: red;
        text-align: center;
        padding: 6px;
        margin: 30px 20px;
        cursor: pointer;
    }
</style>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Items<i class="icon-angle-right"></i></li>
    <li>Add Items</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	'enableAjaxValidation'=>true,
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="portlet-body"> 
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1_1" data-toggle="tab">General</a></li>
            <li><a href="#tab_1_2" data-toggle="tab">E-Commerce</a></li>
          <!--  <li><a href="#tab_1_3" data-toggle="tab">Parent Items</a></li>-->
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="span4">
                        <?php echo $form->labelEx($model,'itemCode'); ?>
                        <?php echo $form->textField($model,'itemCode',array('class'=>'m-wrap span','size'=>60,'maxlength'=>150)); ?>
                        <?php echo $form->error($model,'itemCode'); ?>
                     </div>
                     <div class="span4">
                        <?php echo $form->labelEx($model,'barCode'); ?>
                         <?php echo $form->textField($model,'barCode',array('class'=>'m-wrap span','size'=>60,'maxlength'=>13)); ?>
                        <?php echo $form->error($model,'barCode'); ?>
                     </div>
                     <div class="span4">
                        <?php echo $form->labelEx($model,'itemName'); ?>
                         <?php echo $form->textField($model,'itemName',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200)); ?>
                        <?php echo $form->error($model,'itemName'); ?>
                    </div>  
                </div>

                <div class="row">
                    <div class="span4">
                        <?php echo $form->labelEx($model,'itemName_bd'); ?>
                        <?php echo $form->textField($model,'itemName_bd',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200)); ?>
                        <?php echo $form->error($model,'itemName_bd'); ?>
                    </div>
                    <div class="span4">
                        <?php echo $form->labelEx($model,'costPrice'); ?>
                        <?php 
                            if(!$model->isNewRecord) //  && $model->isParent==Items::IS_PARENTS
                                echo $form->textField($model,'costPrice',array('readonly'=>'readonly','class'=>'m-wrap span'));
                            else echo $form->textField($model,'costPrice',array('class'=>'m-wrap span'));
                        ?>
                        <?php echo $form->error($model,'costPrice'); ?>
                    </div>
                    <div class="span4">
                        <?php echo $form->labelEx($model,'sellPrice'); ?>
                        <?php 
                            if(!$model->isNewRecord) //  && $model->isParent==Items::IS_PARENTS
                                echo $form->textField($model,'sellPrice',array('readonly'=>'readonly','class'=>'m-wrap span'));
                            else echo $form->textField($model,'sellPrice',array('class'=>'m-wrap span'));
                        ?>
                        <?php echo $form->error($model,'sellPrice'); ?>
                    </div>
                    <div class="span4"></div>
                </div>

                <div class="row">	
                	 <div class="span4">
                         <?php echo $form->labelEx($model,'deptId'); ?>
                         <?php echo $form->dropDownList($model,'deptId', Department::getAllDepartment(Department::STATUS_ACTIVE), 
                                      array('class'=>'m-wrap combo combobox','prompt'=>'Select Category',
                                            'id'=>'deptId',
                                            'options'=>($model->isNewRecord) ? '' : array($model->cat->subdept->dept->id=>array('selected'=>'selected')),
                                            'onchange'=>'js:$("#ajax_loaderdept").show()',
                                            'ajax' => array('type'=>'POST', 
                                                            'data'=>array('deptId'=>'js:this.value'),  
                                                            'url'=>CController::createUrl('ajax/dynamicSubDepartment'),
                                                            'success'=>'function(data) 
                                                            {
                                                                $("#ajax_loaderdept").hide();
                                                                $("#subdeptId").html(data);
                                                            }',
                                            )
                                    )); ?>
                         <span id="ajax_loaderdept" style="display:none;">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                         </span>
                         <?php echo $form->error($model,'deptId'); ?>
                    </div>
                    <div class="span4">
                     <?php echo $form->labelEx($model,'subdeptId'); ?>
                     <?php echo $form->dropDownList($model,'subdeptId', Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), 
                                      array('class'=>'m-wrap span','prompt'=>'Select Sub Category',
                                            'id'=>'subdeptId',
                                            'options'=>($model->isNewRecord) ? '' : array($model->cat->subdept->id=>array('selected'=>'selected')),
                                            'onchange'=>'js:$("#ajax_loadersubdept").show()',
                                            'ajax' => array('type'=>'POST', 
                                                            'data'=>array('subdeptId'=>'js:this.value'),  
                                                            'url'=>CController::createUrl('ajax/dynamicCategory'),
                                                            'success'=>'function(data) 
                                                            {
                                                                $("#ajax_loadersubdept").hide();
                                                                $("#catId").html(data);
                                                            }',
                                            )
                                    )); ?>
                         <span id="ajax_loadersubdept" style="display:none;">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                         </span>
                     <?php echo $form->error($model,'subdeptId'); ?>
                    </div>
                    <div class="span4">
                         <?php echo $form->labelEx($model,'catId'); ?>
                         <?php echo $form->dropDownList($model,'catId', Category::getAllCategory(Category::STATUS_ACTIVE), array('class'=>'m-wrap span','prompt'=>'Select Sub-Sub Category','id'=>'catId',)); ?>
                         <?php echo $form->error($model,'catId'); ?>
                    </div>
                </div>
                
                <div class="row">
                    <div class="span4">
                         <?php echo $form->labelEx($model,'brandId'); ?>
                         <?php echo $form->dropDownList($model,'brandId', Brand::getAllBrand(Brand::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox')); ?>
                         <?php echo $form->error($model,'brandId'); ?>
                    </div>
                    
                    <?php 
                    $display = 'block';
                    if(!$model->isNewRecord) :
                        // alreday po or not [ grn approved then not in po ]
                        if(ItemsPrice::checkItemPriceUpdatable($model->id)>0) $display = 'none';
                        // stock >0 or not
                        if(Stock::getItemWiseStock($model->id)>0) $display = 'none';
                    endif; 
					if(isset(Yii::app()->session['supplierId']) && !empty(Yii::app()->session['supplierId'])) : 
						echo '<div class="span4">';
							echo $form->labelEx($model,'supplierId');
							echo Yii::app()->session['supplierName'];
						echo '</div>';
						echo $form->hiddenField($model,'supplierId', array('value'=>Yii::app()->session['supplierId']));
					else : ?>
                    <div class="span4" style="display:<?php echo $display;?>">
                         <?php echo $form->labelEx($model,'supplierId'); ?>
                         <?php echo $form->dropDownList($model,'supplierId', Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox')); ?>
                         <?php echo $form->error($model,'supplierId'); ?>
                    </div>  
                    <?php endif;?>
                    <div class="span4">
                        <?php echo $form->labelEx($model,'isWeighted'); ?>
                        <?php echo $form->dropDownList($model,'isWeighted', UsefulFunction::enumItem($model,'isWeighted'), array('options'=>array('no'=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Weighted/Not')); ?>
                        <?php echo $form->error($model,'isWeighted'); ?>
                    </div>
                </div>
                
                <div class="row">
                	<div class="span4">
						<?php echo $form->labelEx($model,'taxId'); ?>
                        <?php 
                        if($model->isNewRecord)
                            echo $form->dropDownList($model,'taxId', Tax::getAllTax(Tax::STATUS_ACTIVE), array('options'=>array(Tax::getDefaultTax()=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Tax')); 
                        else 
                            echo $form->dropDownList($model,'taxId', Tax::getAllTax(Tax::STATUS_ACTIVE), array('class'=>'m-wrap span','prompt'=>'Select Tax'));
                        ?>
                        <?php echo $form->error($model,'taxId'); ?>
                    </div>
                    <div class="span4">
                        <?php echo $form->labelEx($model,'caseCount'); ?>
                        <?php echo $form->textField($model,'caseCount',array('class'=>'m-wrap span')); ?>
                        <?php echo $form->error($model,'caseCount'); ?>
                    </div>
                    <?php if(isset(Yii::app()->session['supplierId']) && !empty(Yii::app()->session['supplierId'])) : 
						  	 echo $form->hiddenField($model,'status', array('value'=>Items::STATUS_INACTIVE));
					else : ?>
                	<div class="span4">
						<?php echo $form->labelEx($model,'status'); ?>
                        <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Status')); ?>
                        <?php echo $form->error($model,'status'); ?>
                    </div>
                    <?php endif;?>
                </div>
                <div class="row">
                    <div class="span4">
                        <?php echo $form->labelEx($model,'variation_type'); ?>
                        <?php echo $form->dropDownList($model,'variation_type', array(1 => 'Simple', 2 => 'Variation'), array('empty'=>'Select Option'), array('options' => array('class'=>'m-wrap span'))); ?>
                        <?php echo $form->error($model,'variation_type'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="items-variable">


                        <div style="<?php if($model->variation_type == 1){echo "display:block";}else{echo "display:none";} ?>;background: #f5f5f5;padding: 20px;" class="simple-v">
                            <?php echo $form->labelEx($model,'item_quantity'); ?>
                            <?php echo $form->textField($model,'item_quantity',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200)); ?>
                            <?php echo $form->error($model,'item_quantity'); ?>
                        </div>

                        <?php /*
                        <div style="<?php if($model->variation_type == 2){echo "display:block";}else{echo "display:none";} ?>;background: #f5f5f5;padding: 20px 20px 50px 20px; position: relative" class="variable-v">
                            <div class="variable-item">
                                <?php if($model->isNewRecord){ ?>
                                <div class="row_container"> <?php } ?>
                                <?php
                                $i = 0;
                                if(!empty($itemsvariation)){
                                foreach ($itemsvariation as $variation){
                                ?>

                                    <div class="varition-single-item" data-id="<?php echo $i; ?>">
                                        <?php
                                        if(!$variation->isNewRecord){
                                            echo $form->hiddenField($variation,'['.$i.']id');
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="span2">
                                                <?php echo $form->labelEx($variation,'image'); ?>
                                                <?php echo $form->fileField($variation,'['.$i.']image', array('class'=>'variation-image')); ?>
                                                <?php echo $form->error($variation,'image'); ?>
                                                <?php
                                                if(!$variation->isNewRecord){
                                                    if(!empty($variation->image)){
                                                        ?>
                                                        <img class="ItemsVariation_<?php echo $i;?>_image" width="80px" src="<?php echo Yii::app()->baseUrl.'/'.$variation->image;?>" alt="<?php echo $variation->color;?>">
                                                        <div class="delete-image"><div style="display: none" id="delete-image-id" class="delete-image_<?php echo $i;?>">Delete</div></div>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <img class="ItemsVariation_<?php echo $i;?>_image" width="80px" src="https://via.placeholder.com/150" alt="<?php echo $variation->color;?>">
                                                        <div class="delete-image"><div style="display: none" id="delete-image-id" class="delete-image_<?php echo $i;?>">Delete</div></div>
                                                        <?php
                                                    }
                                                }else{
                                                    ?>
                                                        <img class="ItemsVariation_<?php echo $i;?>_image" width="80px" src="https://via.placeholder.com/150" alt="<?php echo $variation->color;?>">
                                                    <div class="delete-image"><div style="display: none" id="delete-image-id" class="delete-image_<?php echo $i;?>">Delete</div></div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($variation,'color'); ?>
                                                <?php echo $form->dropDownList($variation,'['.$i.']color', Attributes::getAllAttributesByTypes(3,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo','prompt'=>'Select Size')); ?>
                                                <?php echo $form->error($variation,'color'); ?>
                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($variation,'size'); ?>
                                                <?php echo $form->dropDownList($variation,'['.$i.']size', Attributes::getAllAttributesByTypes(2,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo','prompt'=>'Select Size')); ?>
                                                <?php echo $form->error($variation,'size'); ?>
                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($variation,'quantity'); ?>
                                                <?php echo $form->textField($variation,'['.$i.']quantity',array('class'=>'m-wrap span')); ?>
                                                <?php echo $form->error($variation,'quantity'); ?>
                                            </div>
                                            <?php
                                            if(!$variation->isNewRecord){
                                                ?>
                                                <div class="span1">
                                                    <div class="delete-btn"><a  onclick="return confirm(' you want to delete?');" style="color:#fff" href="<?php echo Yii::app()->baseUrl."/items/deletevariation?id=".$variation->id.'&itemid='.$model->id; ?>">Delete</a></div>
                                                </div>
                                                <?php
                                            }else{
                                                ?>
                                                <div class="span1">
                                                    <div class="delete-btn">Delete</div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php $i++; }} ?>

                                    <?php if($model->isNewRecord){ ?>
                                    </div> <?php } ?>
                                <?php if(!$model->isNewRecord){ ?>
                                <div class="row_container">
                                    <div class="varition-single-item">
                                        <div class="row">
                                            <div class="span2">
                                                <?php echo $form->labelEx($itemvariation, 'image'); ?>
                                                <?php echo $form->fileField($itemvariation, '[0]image', array('class'=>'variation-image')); ?>
                                                <?php echo $form->error($itemvariation, 'image'); ?>
                                                <img class="preview-image" width="80px" src="https://via.placeholder.com/150" alt="#">
                                                <div class="delete-image"><div style="display: none" id="delete-image-id" class="delete-image">Delete</div></div>

                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($itemvariation, 'color'); ?>
                                                <?php echo $form->dropDownList($variation,'[0]color', Attributes::getAllAttributesByTypes(3,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Size')); ?>
                                                <?php echo $form->error($itemvariation, 'color'); ?>
                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($itemvariation, 'size'); ?>
                                                <?php echo $form->dropDownList($variation,'[0]size', Attributes::getAllAttributesByTypes(2,Attributes::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Size')); ?>
                                                <?php echo $form->error($itemvariation, 'size'); ?>
                                            </div>
                                            <div class="span3">
                                                <?php echo $form->labelEx($itemvariation, 'quantity'); ?>
                                                <?php echo $form->textField($itemvariation, '[0]quantity', array('class' => 'm-wrap span')); ?>
                                                <?php echo $form->error($itemvariation, 'quantity'); ?>
                                            </div>
                                            <div class="span1">
                                                <div class="delete-btn">Delete</div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div style="text-align: right">
                                        <button class="btn red" type="submit" id="add-new-variable" >Add New</button>
                                    </div>-->
                                </div>
                                <?php } ?>

                                <div class="add-new-item" style="margin: 0 10px;">Add New Variation</div>

                                <div id="appendHere"></div>

                                <div class="upload-error"></div>
                            </div>
                        </div>
                        */?>
                    </div>
                </div>
                <?php /*?><div class="row">
                    <?php echo $form->labelEx($model,'negativeStock'); ?>
                    <?php echo $form->dropDownList($model,'negativeStock', UsefulFunction::enumItem($model,'negativeStock'), array('class'=>'m-wrap large','prompt'=>'Select')); ?>
                    <?php echo $form->error($model,'negativeStock'); ?>
                </div><?php */?>
            </div> 
            
            <div class="tab-pane" id="tab_1_2">
            	<div class="span12">
            	    <?php
                      /*  echo $form->labelEx($model,'attributes_color');
                        echo $form->textField($model,'attributes_color',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200, 'placeholder' => 'Example: #000000, #dddddd, #eeeeee'));*/
                    ?>
                    <?php
                      /*  echo $form->labelEx($model,'attributes_size');
                        echo $form->textField($model,'attributes_size',array('class'=>'m-wrap span','size'=>60, 'maxlength'=>200, 'placeholder' => 'Example: M, X, XL, XXL'));*/
                    ?>
                  	<?php /* if(!empty($attTypeModel)) :
						foreach($attTypeModel as $keyType=>$dataType) : ?>
                        <div class="row">
                        	<?php 
								$attId = $checked = '';
								if(!$model->isNewRecord) :
									$itemAttModel = ItemAttributes::getAttributesValuesByItems($dataType->id,$itemId);
									if(!empty($itemAttModel)) :
										$attId = $itemAttModel->attId;
										if($dataType->isHtml==AttributesType::STATUS_INACTIVE)
											echo CHtml::hiddenField('attIdPrev['.$dataType->id.']',$itemAttModel->id, array());
									endif;
								endif;
								
								echo CHtml::hiddenField('attType['.$dataType->id.']',$dataType->id, array());
								echo CHtml::label($dataType->name,''); 
								if($dataType->isHtml==AttributesType::STATUS_ACTIVE)
								{
									foreach(Attributes::getAllAttributesByTypesIsHtml($dataType->id,Attributes::STATUS_ACTIVE) as $cKey=>$cVal) :
										if($cVal->id==$attId) $checked = 'checked'; else $checked = '';
										
										if(!$model->isNewRecord) :
											if(!empty($itemAttModel)) echo CHtml::hiddenField('attIdPrev['.$dataType->id.']',$itemAttModel->id, array());
										endif;
										echo '<label class="radio '.$checked.'" style="background:'.$cVal->name.';">
												 <input type="radio" name="attId['.$dataType->id.']" id="'.$cVal->id.'" value="'.$cVal->id.'" class="radio_color" />
											  </label>';
									endforeach;
								}
								else
								{
									echo CHtml::dropDownList('attId['.$dataType->id.']',$attId,Attributes::getAllAttributesByTypes($dataType->id,Attributes::STATUS_ACTIVE), array('id'=>'attId'.$dataType->id,'class'=>'m-wrap span attId','prompt'=>'Select '.$dataType->name)); 	
								}
							?>
                        </div>
					<?php endforeach;
					endif; */ ?>


                    <style type="text/css">
                        .MultiFile-label{
                            padding-top: 3px;
                        }
                        .MultiFile-remove{
                            float: left;
                            line-height: 28px;
                        }
                        .MultiFile-title{
                            float: left;
                        }
                        .image-item {
                            position: relative;
                        }
                        div.form .image-item label{
                            float: left;
                            margin-left: 5px;
                        }
                        .image-item input[type="radio"] {
                            float: left;
                            position: relative;
                            top: 9px;
                        }
                        .image-item input[type="text"] {
                            float: left;
                            width: 100px;
                            margin: 0 0 0 5px;
                        }
                        .image-item .image-preview {
                            float: left;
                            margin-left: 20px;
                            line-height: 28px;
                            width: 200px;
                        }
                        .image-item .image-alt-text {
                            float: left;
                            margin-left: 20px;
                        }
                        .image-item .image-sort {
                            float: left;
                            margin-left: 10px;
                        }
                        .image-item .image-sort input[type="text"] {
                            width: 40px;
                        }
                        .image-item .image-large {
                            float: left;
                            margin-left: 20px;
                        }
                        .image-item .image-medium {
                            float: left;
                            margin-left: 10px;
                        }
                        .image-item .image-small {
                            float: left;
                            margin-left: 10px;
                        }
                        .image-item .image-remove {
                            float: left;
                            margin-left: 10px;
                        }
                        .image-item-update{
                            border: 1px solid #ddd;
                            overflow: hidden;
                        }
                        .image-item-update .image-alt-text, .image-item-update .image-sort, .image-item-update .image-large, .image-item-update .image-medium, .image-item-update .image-small{
                            padding-top: 5px;
                        }
                        .remove_img{
                            padding: 13px 8px 0 0;
                        }
                        .remove_img i{
                            box-sizing: border-box;
                            color: #48c3e9;
                            display: block;
                            font-size:16px;
                            height: auto;
                            margin: 0;
                            padding: 0;
                            position: absolute;
                            width: auto;
                        }
                        .image-item-insert .image-preview{
                            width: 191px;
                        }
                    </style>
                    
                    <div class="row">&nbsp;</div>
                    <div class="row">
                    	<?php echo CHtml::label('Images <span style="color:red;">[ Only jpeg|jpg|gif|png with maximum 5 MB ]</span>',''); ?>
                        <?php
							$this->widget('CMultiFileUpload', array(
								 'model'=>$model,
								 'attribute'=>'image',
								 'accept'=>'jpeg|jpg|gif|png',
								 'duplicate' => 'Duplicate file!',
								 'denied'=>'File is not allowed',
                                 'file'=>'<div class="row"><div class="image-item image-item-insert">
                                            <div class="image-preview">$file</div>
                                            <div class="image-alt-text">
                                                <label>Alt Text</label>
                                                <input name="Items[image][alt_text][]" type="text" />
                                            </div>
                                            <div class="image-sort">
                                                <label>Sort Order</label>
                                                <input name="Items[image][sort_order][]" type="text" />
                                            </div>
                                            <div class="image-large">
                                                <input name="Items[image][large_image]" value="" type="radio" />
                                                <label>Large Image</label>                                                
                                            </div>
                                            <div class="image-medium">
                                                <input name="Items[image][medium_image]" value="" type="radio" />
                                                <label>Medium Image</label>                                                
                                            </div>
                                            <div class="image-small">
                                                <input name="Items[image][small_image]" value="" type="radio" />
                                                <label>Small Image</label>                                                
                                            </div>
                                         </div></div>',
								 'remove' =>'<i class="fa fa-times"></i>',
								 'htmlOptions'=>array('style'=>'width:98% !important;background:#eeeeee;border:1px solid #ddd;opacity:100;height:40px;width:200px;padding:5px;cursor:pointer;color:#48c3e9;'),
								 //'max'=>3, // max 10 files
								 
								 'options'=>array(
									// 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
									// 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
									// 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
									// 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
									// 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',

                                    'afterFileAppend'=>'function(e, v, m){                                       
                                       if (!$("input[name=\'Items[image][large_image]\']:checked").val()) {
                                            $(".image-large input:radio:first-child").attr("checked", true);
                                       }
                                       if (!$("input[name=\'Items[image][medium_image]\']:checked").val()) {
                                            $(".image-medium input:radio:first-child").attr("checked", true);
                                       }
                                       if (!$("input[name=\'Items[image][small_image]\']:checked").val()) {
                                            $(".image-small input:radio:first-child").attr("checked", true);
                                       }
                                       
                                       // Radio Box
                                       $(".MultiFile-label").each(function( index ) {
                                          $(this).find(".image-large input").val(index);
                                          $(this).find(".image-medium input").val(index);
                                          $(this).find(".image-small input").val(index);
                                       });
                                       
                                    }',
                                    'afterFileRemove'=>'function(e, v, m){
                                       // Radio Box
                                       $(".MultiFile-label").each(function( index ) {
                                          $(this).find(".image-large input").val(index);
                                          $(this).find(".image-medium input").val(index);
                                          $(this).find(".image-small input").val(index);
                                       });                                       
                                    }',
									'afterFileSelect'=>'function(e ,v ,m)
									{
										var fileSize = e.files[0].size;
								        if(fileSize>5000*1024)
										{ 
											alert("Exceeds file upload limit 5MB");
											$(".MultiFile-remove").click(); 
								        }                      
										return true;
									}',
								 ),
							  ));
						?>
                    </div> 
                    <?php 
					// show previous images
					if(!$model->isNewRecord) : 
						$itemImagesModel = ItemImages::getImagesByItems($itemId);
						if(!empty($itemImagesModel)) :
							foreach($itemImagesModel as $iKey => $iData) :

                                $selectedLargeImage = '';
                                if($iData->large_image == 1){
                                    $selectedLargeImage = 'checked="checked"';
                                }

                                $selectedMediumImage = '';
                                if($iData->medium_image == 1){
                                    $selectedMediumImage = 'checked="checked"';
                                }

                                $selectedSmallImage = '';
                                if($iData->small_image == 1){
                                    $selectedSmallImage = 'checked="checked"';
                                }



							    echo '<div class="row">
                                         <div class="image-item image-item-update">
                                            <div class="image-remove">
                                                <a href="javascript:void(0);" class="remove_img" id="'.$iData->id.'">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-preview">
                                                <img src="'. Yii::app()->baseUrl . '/media/catalog/product/' . $iData->image.'" style="width:40px;" />
                                            </div>
                                            <div class="image-alt-text">
                                                <label>Alt Text</label>
                                                <input name="Items[image][alt_text][]" value="'.$iData->alt_text.'" type="text" />
                                            </div>
                                            <div class="image-sort">
                                                <label>Sort Order</label>
                                                <input name="Items[image][sort_order][]" value="'.$iData->sort_order.'" type="text" />
                                            </div>
                                            <div class="image-large">
                                                <input name="Items[image][large_image][]" value="'.$iKey.'" '.$selectedLargeImage.' type="radio" />
                                                <label>Large Image</label>                                                
                                            </div>
                                            <div class="image-medium">
                                                <input name="Items[image][medium_image][]" value="'.$iKey.'" '.$selectedMediumImage.' type="radio" />
                                                <label>Medium Image</label>                                                
                                            </div>
                                            <div class="image-small">
                                                <input name="Items[image][small_image][]" value="'.$iKey.'" '.$selectedSmallImage.' type="radio" />
                                                <label>Small Image</label>                                                
                                            </div>
                                          </div>
                                         </div>';
							endforeach;
						endif;
                    endif;?>

					<div class="clearfix"></div>
                    <div class="row" style="margin:15px 0px;">
                        <?php echo $form->labelEx($model,'isEcommerce'); ?>
                        <?php echo $form->radioButtonList($model,'isEcommerce',array(1=>'Yes',2=>'No'),
                            array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
                        <?php echo $form->error($model,'isEcommerce'); ?>
                    </div>

					<div class="row" style="margin:15px 0px;">
						<?php echo $form->labelEx($model,'isFeatured'); ?>
						<?php echo $form->radioButtonList($model,'isFeatured',array(1=>'Yes',2=>'No'),
								   array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
						<?php echo $form->error($model,'isFeatured'); ?>
					</div>
                    <div class="row" style="margin:15px 0px;">
                        <?php echo $form->labelEx($model,'isNew'); ?>
                        <?php echo $form->radioButtonList($model,'isNew',array(1=>'Yes',2=>'No'),
                            array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
                        <?php echo $form->error($model,'isNew'); ?>
                    </div>
                    <div class="row" style="margin:15px 0px;">
                        <?php echo $form->labelEx($model,'isSpecial'); ?>
                        <?php echo $form->radioButtonList($model,'isSpecial',array(1=>'Yes',2=>'No'),
                            array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
                        <?php echo $form->error($model,'isSpecial'); ?>
                    </div>
					<div class="row" style="margin:15px 0px;">
						<?php echo $form->labelEx($model,'isSales'); ?>
						<?php echo $form->radioButtonList($model,'isSales',array(1=>'Yes',2=>'No'),
								   array('separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
						<?php echo $form->error($model,'isSales'); ?>
					</div>
                 </div>
                 <div class="span12" style="margin: 0">
                    <div class="row">
                        <?php echo $form->labelEx($model,'specification'); ?>
                        <?php 
                             $this->widget('application.extensions.eckeditor.ECKEditor', array(
                                    'model'=>$model,
                                    'attribute'=>'specification',
                                    'config' => array(
                                        /*'toolbar'=>array(
                                            array('Source', '-', 'NewPage', 'Preview', '-','Bold', 'Italic','Underline','Styles','Paragraph','Strike' ),
                                            array('-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                                            array('-','Link', 'Unlink', 'Anchor' ) , // 
                                        ),
                                        'language'=>'en',
                                        'filebrowserImageBrowseUrl'=>'kcfinder/browse.php?type=files',
                                        'filebrowserImageUploadUrl'=>Yii::app()->createAbsoluteUrl('site/redactorimageUp'),*/
                                      ),
                              )); 
                        ?>                   
                        <?php echo $form->error($model, 'specification'); ?>        
                    </div>
                     <div class="row">
                         <?php echo $form->labelEx($model,'specification_bd'); ?>
                         <?php
                         $this->widget('application.extensions.eckeditor.ECKEditor', array(
                             'model'=>$model,
                             'attribute'=>'specification_bd',
                             'config' => array(
                                 /*'toolbar'=>array(
                                     array('Source', '-', 'NewPage', 'Preview', '-','Bold', 'Italic','Underline','Styles','Paragraph','Strike' ),
                                     array('-','Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'),
                                     array('-','Link', 'Unlink', 'Anchor' ) , //
                                 ),
                                 'language'=>'en',
                                 'filebrowserImageBrowseUrl'=>'kcfinder/browse.php?type=files',
                                 'filebrowserImageUploadUrl'=>Yii::app()->createAbsoluteUrl('site/redactorimageUp'),*/
                             ),
                         ));
                         ?>
                         <?php echo $form->error($model, 'specification_bd'); ?>
                     </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'description'); ?>
                        <?php 
                             $this->widget('application.extensions.eckeditor.ECKEditor', array(
                                    'model'=>$model,
                                    'attribute'=>'description',
                              )); 
                        ?>                   
                        <?php echo $form->error($model, 'description'); ?>        
                    </div>
                     <div class="row">
                         <?php echo $form->labelEx($model,'description_bd'); ?>
                         <?php
                         $this->widget('application.extensions.eckeditor.ECKEditor', array(
                             'model'=>$model,
                             'attribute'=>'description_bd',
                         ));
                         ?>
                         <?php echo $form->error($model, 'description_bd'); ?>
                     </div>
                    <div class="row">
                        <?php echo $form->labelEx($model,'shipping_payment'); ?>
                        <?php 
                             $this->widget('application.extensions.eckeditor.ECKEditor', array(
                                    'model'=>$model,
                                    'attribute'=>'shipping_payment',
                              )); 
                        ?>                   
                        <?php echo $form->error($model, 'shipping_payment'); ?>        
                    </div>
                     <div class="row">
                         <?php echo $form->labelEx($model,'shipping_payment_bd'); ?>
                         <?php
                         $this->widget('application.extensions.eckeditor.ECKEditor', array(
                             'model'=>$model,
                             'attribute'=>'shipping_payment_bd',
                         ));
                         ?>
                         <?php echo $form->error($model, 'shipping_payment_bd'); ?>
                     </div>
                 </div>
            </div>      
            <div class="tab-pane" id="tab_1_3">
                <?php $totalQty = $totalWtg = $totalnetAmount = 0; 
                if($model->isParent==Items::IS_PARENTS) : // update case
                    foreach(ItemParents::getAllItemParentsByChield($model->id,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)  :
                        if($dataParents->parent->isWeighted=='no') $totalQty+= $dataParents->qty;
                        else $totalWtg+= $dataParents->qty;
                        $totalnetAmount+= $dataParents->qty*$dataParents->parent->costPrice;
                    endforeach;
                endif;?>
                <div class="row">
                    <table class="table table-striped table-bordered table-advance table-hover">
                        <thead>
                            <tr>
                                <th>Total Quantity</th>
                                <th>Total Weight</th>
                                <th>Total Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="highlight">
                                    <div class="success"></div>&nbsp;&nbsp;
                                    <span style="font-size:20px;" id="grandQty"><?php echo $totalQty;?></span>
                                </td>
                                <td class="highlight">
                                    <div class="info"></div>&nbsp;&nbsp;
                                    <span style="font-size:20px;" id="grandWeight"><?php echo $totalWtg;?></span>
                                </td>
                                <td class="highlight">
                                    <div class="success" style="border-color:#e332ef;"></div>&nbsp;&nbsp;
                                    <span style="font-size:20px;" id="totalAmount"><?php echo $totalnetAmount;?></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php // endif; ?>
                
                <div class="row">
                    <div style="height:400px; overflow:scroll;">
                    <table class="table table-striped table-hover bordercolumn">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>Item Code</th>
                                <th class="hidden-480">Description</th>
                                <th class="hidden-480">Quantity</th>
                                <th class="hidden-480">Cost Price</th>
                                <th>Net Amount</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($model->isParent==Items::IS_PARENTS) : // update case
                            foreach(ItemParents::getAllItemParentsByChield($model->id,ItemParents::STATUS_ACTIVE) as $keyParents=>$dataParents)  :
                                if($keyParents==0) :
                                    $parentClass = 'class="parentRow"';
                                    $parentId = count(ItemParents::getAllItemParentsByChield($model->id,ItemParents::STATUS_ACTIVE));
                                else :
                                    $parentClass = 'class="chieldRow'.$keyParents.'"';
                                    $parentId = $keyParents;
                                endif;
                            ?>
                            <tr <?php echo $parentClass;?> id="<?php echo $keyParents;?>">
                                <td><span class="srlNo"><?php echo $keyParents+1;?></span></td>
                                <td class="hidden-480">
                                    <?php 
                                         echo CHtml::textField('code',$dataParents->parent->itemCode,array('id'=>$keyParents,'class'=>'codeblur','style'=>'width:100px; height:15px;')); // 'readonly'=>'readonly',
                                         echo CHtml::hiddenField('pk['.$keyParents.']',$keyParents, array('id'=>'pk'.$keyParents,'class'=>'pk'));
                                         echo CHtml::hiddenField('proId['.$keyParents.']',$dataParents->parentId, array('id'=>'proId'.$keyParents, 'class'=>'proId'));
                                    ?>
                                    <span id="ajax_loaderCode<?php echo $keyParents;?>" style="display:none;" class="ajax_loaderCode">
                                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                    </span>
                                    <div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
                                </td>
                                <td><span id="itemName<?php echo $keyParents;?>" class="itemName"><?php echo $dataParents->parent->itemName;?></span></td>
                                <td class="hidden-480">
                                    <?php echo CHtml::textField('qty['.$keyParents.']',$dataParents->qty,array('id'=>'qty'.$keyParents, 'class'=>'qtyblur','style'=>'width:50px; height:15px;')); // 'readonly'=>'readonly', ?>
                                    <?php echo CHtml::hiddenField('type['.$keyParents.']','',array('id'=>'type'.$keyParents, 'class'=>'typeblur'));?>
                                    <span id="ajax_loaderQty<?php echo $keyParents;?>" style="display:none;" class="ajax_loaderQty">
                                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                    </span>
                                    <div style="width:100%; color:red; font-size:11px;" id="ajax_errorQty<?php echo $keyParents;?>" class="ajax_errorQty"></div>
                                </td>
                                <td class="hidden-480">
                                    <span id="sellPrice<?php echo $keyParents;?>" class="sellPrice"><?php echo $dataParents->parent->costPrice;?></span>
                                    <?php echo CHtml::hiddenField('sellPriceAmount['.$keyParents.']',$dataParents->parent->costPrice, array('id'=>'sellPriceAmount0','class'=>'sellPriceAmount'));?>
                                </td>
                                <td><span id="netTotal<?php echo $keyParents;?>" class="netTotal"><?php echo $dataParents->qty*$dataParents->parent->costPrice;?></span></td>
                                <td><img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
                            </td>
                            </tr>    
                            <?php endforeach;
                            else : // insert case ?>
                            <tr class="parentRow" id="0">
                                <td><span class="srlNo">1</span></td>
                                <td class="hidden-480">
                                    <?php 
                                         echo CHtml::textField('code','',array('id'=>0,'class'=>'codeblur','style'=>'width:100px; height:15px;'));
                                         echo CHtml::hiddenField('pk[0]',0, array('id'=>'pk0','class'=>'pk'));
                                         echo CHtml::hiddenField('proId[0]','', array('id'=>'proId0', 'class'=>'proId'));
                                    ?>
                                    <span id="ajax_loaderCode0" style="display:none;" class="ajax_loaderCode">
                                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                    </span>
                                    <div style="width:100%; color:red; font-size:11px;" id="ajax_errorCode0" class="ajax_errorCode"></div>
                                </td>
                                <td><span id="itemName0" class="itemName">-</span></td>
                                <td class="hidden-480">
                                    <?php echo CHtml::textField('qty[0]','',array('id'=>'qty0', 'class'=>'qtyblur','style'=>'width:50px; height:15px;'));?>
                                    <?php echo CHtml::hiddenField('type[0]','',array('id'=>'type0', 'class'=>'typeblur'));?>
                                    <span id="ajax_loaderQty0" style="display:none;" class="ajax_loaderQty">
                                        <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                                    </span>
                                    <div style="width:100%; color:red; font-size:11px;" id="ajax_errorQty0" class="ajax_errorQty"></div>
                                </td>
                                <td class="hidden-480">
                                    <span id="sellPrice0" class="sellPrice">0</span>
                                    <?php echo CHtml::hiddenField('sellPriceAmount[0]',0, array('id'=>'sellPriceAmount0','class'=>'sellPriceAmount'));?>
                                </td>
                                <td><span id="netTotal0" class="netTotal">0</span></td>
                                <td><img src="<?php echo Yii::app()->baseUrl;?>/media/images/delete.png" border="0" style=" vertical-align:bottom; cursor:pointer;" class="removeRow" />
                            </tr> 
                        <?php endif;?>
                        </tbody>
                    </table>
                    </div>
                </div> 
            </div> 
        </div><!-- tab-content -->
	</div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
        <?php
        if(!$model->isNewRecord){
            if($model->variation_type ==2) {
                ?>
                <a style="margin-left: 15px" class="btn blue"
                   href="<?php echo Yii::app()->baseUrl . '/items/variation?item_id=' . $model->id; ?>">Add
                    Variation</a>
                <?php
            }
        }
        ?>
    </div>
<?php $this->endWidget(); ?>
</div> <!-- form -->   