<?php
/* @var $this CustpmerDepositController */
/* @var $model CustpmerDeposit */

$this->breadcrumbs=array(
	'Custpmer Deposits'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustpmerDeposit', 'url'=>array('index')),
	array('label'=>'Create CustpmerDeposit', 'url'=>array('create')),
	array('label'=>'View CustpmerDeposit', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustpmerDeposit', 'url'=>array('admin')),
);
?>

<h1>Update CustpmerDeposit <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>