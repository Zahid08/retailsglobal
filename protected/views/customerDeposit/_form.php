<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Customer<i class="icon-angle-right"></i></li>
	<li>Customer Deposit</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'custpmer-deposit-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab">&nbsp;<?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Customer Deposit</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<?php if($model->isNewRecord) { echo $form->labelEx($model,'custId'); ?>
				<?php $this->widget('CAutoComplete',array(
							 'model'=>$model,
							 'id'=>'custId',
							 'attribute' => 'custId',
							 //name of the html field that will be generated
							 //'name'=>'custId', 
							 //replace controller/action with real ids
							 'value'=>($model->custId)?$model->cust->custId:'',
				
							 'url'=>array('ajax/autoCompleteCustomerInstitutional'), 
							 'max'=>100, //specifies the max number of items to display

										 //specifies the number of chars that must be entered 
										 //before autocomplete initiates a lookup
							 'minChars'=>2, 
							 'delay'=>500, //number of milliseconds before lookup occurs
							 'matchCase'=>false, //match case when performing a lookup?
							 'mustMatch' => true,
										 //any additional html attributes that go inside of 
										 //the input field can be defined here
							 'htmlOptions'=>array('class'=>'m-wrap large','size'=>20,'maxlength'=>20), 	
														
							 //'extraParams' => array('sessionId' => 'js:function() { return $("#sessionId").val(); }'),
							 //'extraParams' => array('taskType' => 'desc'),
							 'methodChain'=>".result(function(event,item){})",
							 //'methodChain'=>".result(function(event,item){\$(\"#custId\").val(item[1]);})",
						));?>
				<?php echo $form->error($model,'custId'); } ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'amount'); ?>
				<?php echo $form->textField($model,'amount',array('class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'amount'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'remarks'); ?>
				<?php echo $form->textArea($model,'remarks',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250,'style'=>'height:50px;')); ?>
				<?php echo $form->error($model,'remarks'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
         </div> 
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   