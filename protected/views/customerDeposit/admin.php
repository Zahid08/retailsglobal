<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Customer <i class="icon-angle-right"></i></li>
	<li>Customer Deposit<i class="icon-angle-right"></i></li>
    <li>Manage Customer Deposit</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php CHtml::link('Add CustpmerDeposit', array('custpmer-deposit/create'), array('class'=>'btn light', 'style'=>'float:right;margin-left:10px;'));?>

<div class="form" style="overflow:scroll;">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'custpmer-deposit-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		array('name'=>'custId',
			 'value'=>'$data->cust->custId',
			 'filter'=>Customer::getAllCustomerInstitutional(Customer::STATUS_ACTIVE),
		),
		'amount',
		'remarks',
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		'rank',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('custpmer-deposit-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>