<?php
/* @var $this ContentsTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contents Types',
);

$this->menu=array(
	array('label'=>'Create ContentsType', 'url'=>array('create')),
	array('label'=>'Manage ContentsType', 'url'=>array('admin')),
);
?>

<h1>Contents Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
