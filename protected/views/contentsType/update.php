<?php
/* @var $this ContentsTypeController */
/* @var $model ContentsType */

$this->breadcrumbs=array(
	'Contents Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ContentsType', 'url'=>array('index')),
	array('label'=>'Create ContentsType', 'url'=>array('create')),
	array('label'=>'View ContentsType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ContentsType', 'url'=>array('admin')),
);
?>

<h1>Update ContentsType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>