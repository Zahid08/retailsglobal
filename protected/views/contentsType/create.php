<?php
/* @var $this ContentsTypeController */
/* @var $model ContentsType */

$this->breadcrumbs=array(
	'Contents Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ContentsType', 'url'=>array('index')),
	array('label'=>'Manage ContentsType', 'url'=>array('admin')),
);
?>

<h1>Create ContentsType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>