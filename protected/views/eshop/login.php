<script type="text/javascript" language="javascript">
$(function() 
{
	//-------enter press ------------//
	$(document).keypress(function(e) {
		if(e.which  == 13) 
		{
			$('#login-form').get(0).submit();
		}
	});
	
});
</script>
<?php 
// sign style by company according to sub Domain
$protocol = 'http';
if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https';
$host = $_SERVER['SERVER_NAME'];
$baseUrl = $protocol.'://'.$host;
$url = parse_url($baseUrl ,PHP_URL_HOST);
$domain = strstr(str_replace("www.","",$url), ".",true);
if($domain=='keenlay') : ?>
    <style type="text/css">
        .login .content{ background:none repeat scroll 0 0 #59b210; }
        .btn-login{background-color: #59b210; border:2px solid #59b210 ;}
         .btn-login:hover{background-color: #59b210 !important; border:2px solid #59b210 !important;}
    </style>
<?php endif; ?>
<div class="form">
    <!-- BEGIN LOGIN FORM -->
    <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'login-form',
            'enableAjaxValidation'=>true,
            'htmlOptions'=>array('classs'=>'form-vertical login-form')
         )); ?>
         <?php // echo $form->hiddenField($model,'branchId',array('value'=>Branch::DEFAULT_BRANCH)); ?>		
        <h3 class="form-title">Sign In to your account</h3>
        <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            <span>Enter any username and password.</span>
        </div>
        <div class="control-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"><?php echo CHtml::label('Branch',''); ?></label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <?php echo $form->dropDownList($model,'branchId',Branch::getorg(Branch::STATUS_ACTIVE),array('class'=>"m-wrap",'empty'=>'Please Select a Branch', 'style'=>'width:290px; padding-left:30px; color:#919191;')); ?>
                    <?php echo $form->error($model,'branchId'); ?>
                </div>
            </div>
        </div>
        <div class="control-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,'username'); ?></label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <?php echo $form->textField($model,'username', array('class'=>'m-wrap placeholder-no-fix', 'placeholder'=>'Username')); ?>
                    <?php echo $form->error($model,'username'); ?>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label visible-ie8 visible-ie9"><?php echo $form->labelEx($model,'password'); ?></label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <?php echo $form->passwordField($model,'password', array('class'=>'m-wrap placeholder-no-fix', 'placeholder'=>'Password')); ?>
                    <?php echo $form->error($model,'password'); ?>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <?php echo CHtml::submitButton('Login', array('class'=>'btn btn-login pull-right')); ?>        
        </div>
    <?php $this->endWidget(); ?>
    <!-- END LOGIN FORM -->        
</div>