<script type="text/javascript" language="javascript">	
	// JQUERY STARTED
	$(document).ready(function() 
	{
        // switch types wise pay option
		$("#type").change(function() 
		{
			var type = $(this).val();
			if(type==2) $("#payOption").show("slow");
			else 
            {
                $("#payOption").hide("slow");
                $("#bankRow").hide("slow");
            }
		});
        
		// switch deo types
		$(".isBank").click(function() 
		{
			var type = $(this).val();
			if(type==1) $("#bankRow").show("slow");
			else $("#bankRow").hide("slow");
		});
  }); 
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Finance
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Misc. Income/Expense
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'finance-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) : echo $msg; 
else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>

<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Misc. Income/Expense</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<?php echo $form->labelEx($model,'type'); ?>
				<?php echo $form->dropDownList($model,'type', Lookup::items('Finance'), 
											  array('class'=>'m-wrap large','prompt'=>'Select Type',
													'id'=>'type',
													'options'=>($model->isNewRecord) ? '' : array($model->type=>array('selected'=>'selected')),
													'onchange'=>'js:$("#ajax_loaderdept").show()',
													'ajax' => array('type'=>'POST', 
																	'data'=>array('type'=>'js:this.value'),  
																	'url'=>CController::createUrl('ajax/dynamicFinanceCatByType'),
																	'success'=>'function(data) 
																	{
																		$("#ajax_loaderdept").hide();
																		$("#catId").html(data);
																	}',
													)
											)); ?>
				 <span id="ajax_loaderdept" style="display:none;">
					<img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
				 </span>
				<?php echo $form->error($model,'type'); ?>
			</div>
			<div class="row" style="margin-bottom:20px;">
				<?php echo $form->labelEx($model,'catId'); ?>
				<?php echo $form->dropDownList($model,'catId', FinanceCategory::getAllFinanceCategory(FinanceCategory::STATUS_ACTIVE), array('class'=>'m-wrap large','prompt'=>'Select Category','id'=>'catId',)); ?>
				<?php echo $form->error($model,'catId'); ?>
			</div>
            <?php 
				if(!empty($model->bankId)) $style = 'style="display:block;"';
				else $style = 'style="display:none;"';
			?>
            
            <div class="row" id="payOption" <?php echo $style;?>>
                <?php echo $form->radioButtonList($model,'isBank',array(2=>'Cash',1=>'Bank'),array('class'=>'isBank','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
            </div>
            <div id="bankRow" class="row" <?php echo $style;?>>
                <div class="row">
                    <?php echo $form->labelEx($model,'bankId'); ?>
                    <?php echo $form->dropDownList($model,'bankId',Bank::getAllBankName(Bank::STATUS_ACTIVE),array('class'=>'m-wrap large','style'=>'width:331px !important;','empty'=>'Select Bank')); ?>
                    <?php echo $form->error($model,'bankId'); ?>
                </div> 
                <div class="row">
                    <?php echo $form->labelEx($model,'bankAcNo'); ?>
                    <?php 
                        $this->widget('CAutoComplete',array(
                             'model'=>$model,
                             'id'=>'bankAcNo',
                             'attribute' => 'bankAcNo',
                             //name of the html field that will be generated
                             //'name'=>'poId', 
                                         //replace controller/action with real ids
                             //'value'=>'',

                             'url'=>array('ajax/autoCompleteBankAccountsNo'), 
                             'max'=>100, //specifies the max number of items to display

                                         //specifies the number of chars that must be entered 
                                         //before autocomplete initiates a lookup
                             'minChars'=>2, 
                             'delay'=>500, //number of milliseconds before lookup occurs
                             'matchCase'=>false, //match case when performing a lookup?
                             'mustMatch' =>false,
                                         //any additional html attributes that go inside of 
                                         //the input field can be defined here
                             'htmlOptions'=>array('class'=>'m-wrap span',
                                                  'style'=>'width:331px;',
                                                  'maxlength'=>20,
                                                  'value'=>!empty($model->bankId)?$model->bank->accountNo:'',
                                                 ),  

                             'extraParams' => array('bankId' => 'js:function() { return $("#Finance_bankId").val(); }'),
                             //'extraParams' => array('taskType' => 'desc'),
                             'methodChain'=>".result(function(event,item){})",//\$(\"#user_id\").val(item[1]);
                        ));
                    ?>
                    <?php echo $form->error($model,'bankAcNo'); ?>
                </div> 
            </div>
			<div class="row">
				<?php echo $form->labelEx($model,'amount'); ?>
				<?php echo $form->textField($model,'amount',array('class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'amount'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'remarks'); ?>
				<?php echo $form->textArea($model,'remarks',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250)); ?>
				<?php echo $form->error($model,'remarks'); ?>
			</div>
		</div>	
     </div>
	 <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   