<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Finance <i class="icon-angle-right"></i></li>
   	<li>Misc. Income/Expense <i class="icon-angle-right"></i></li>
    <li>Details Misc. Income/Expense # <?php echo $model->cat->title;?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		array(            
			'label'=>'Category',  
			'value'=>$model->cat->title,
		  ),
        array(            
			'label'=>'Bank/Cash',  
			'value'=>($model->bankId>0)?$model->bank->name:'Cash',
		  ),
		array(            
			'label'=>'Type',  
			'value'=>Lookup::item('Finance',$model->type),
		  ),
		'amount',
		'remarks',
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
		'rank',
	),
)); ?>
</div>
