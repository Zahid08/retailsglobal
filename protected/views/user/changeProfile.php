<?php $this->menu=array(
	array('label'=>'Add User', 'url'=>array('create'),'visible'=>UserRights::getVisibility('user', 'create')),
	array('label'=>'Manage User', 'url'=>array('admin'),'visible'=>UserRights::getVisibility('user', 'admin')),
	array('label'=>'Change Password', 'url'=>array('/user/changepassword'),'visible'=>UserRights::getVisibility('user', 'changepassword')),
	array('label'=>'Change Profile', 'url'=>array('/user/changeProfile'),'visible'=>UserRights::getVisibility('user', 'changeProfile')),
); ?>
<h1>Change Profile</h1>
<!--
<div class="form">
<?php echo CHtml::beginForm(); ?>

	<p class="note" align="right"><?php echo 'Fields with <span class="required">*</span> are required.'; ?></p>
	<?php echo CHtml::errorSummary($form); ?>
	
    <FIELDSET class="fieldset">
    	<LEGEND class="legend">Change Password</LEGEND>
	<div class="row">
		<?php 
		
		echo CHtml::activeLabelEx($form,'verifyoldpass'); ?>
		<?php echo CHtml::activePasswordField($form,'verifyoldpass'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($form,'password'); ?>
		<?php echo CHtml::activePasswordField($form,'password'); ?>
	</div>
	
	<div class="row">
		<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
		<?php echo CHtml::activePasswordField($form,'verifyPassword'); ?>
	</div>
	</FIELDSET>
	<div class="row submit">
		<?php echo CHtml::submitButton("Save"); ?>
	</div>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->-->