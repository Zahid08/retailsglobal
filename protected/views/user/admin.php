<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
	<li>User<i class="icon-angle-right"></i></li>
    <li>Manage User</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
    <?php $this->widget('bootstrap.widgets.TbExtendedGridView',array(
        'id'=>'user-grid',
        'dataProvider'=>$model->search(),
        'filter'=>$model,
        'type' => 'striped bordered condensed',
		'template' => '{summary}{items}{pager}',
		'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
        'columns'=>array(
            array(            
                'name'=>'branchId',
                'value'=>'$data->branch->name',
                'filter'=>(isset(Yii::app()->user->isSuperAdmin) && Yii::app()->user->isSuperAdmin==1) ? Branch::getAllorg(): '',
            ),
            'username',
             array(            
                'name'=>'status',             
                'type'=>'raw',
                'value'=>'Lookup::item("Status", $data->status)', 
                'filter'=>Lookup::items("Status"),
                
            ),  
            array(
                //'class'=>'CButtonColumn',
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'header'=>CHtml::dropDownList('pageSize',
                    $pageSize,
                    array(5=>5,10=>10,20=>20,50=>50,100=>100),
                    array(
                   //
                   // change 'user-grid' to the actual id of your grid!!
                    'onchange'=>
                    "$.fn.yiiGridView.update('user-grid',{ data:{pageSize: $(this).val() }})",
                )),
            ),
        ),
    )); ?>
</div>