<?php $themeSkinPath = Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>
<link href="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo $themeSkinPath; ?>/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>

<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/scripts/jquery.flot.min.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/scripts/jquery.flot.resize.min.js" type="text/javascript"></script>
<script src="<?php echo $themeSkinPath; ?>/scripts/jquery.flot.categories.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->

<h3 class="page-title">
    Dashboard <small>statistics and more</small>
</h3>
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index-2.html">Home</a>
        <i class="icon-angle-right"></i>
    </li>
    <li><a href="#">Dashboard</a></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<!-- BEGIN DASHBOARD  -->
<div id="dashboard">
    <!-- BEGIN DASHBOARD STATS -->
    <div class="row-fluid">
        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="ft ft-icon-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php
                        $custCount = 0;
                        $sqlcust = "SELECT COUNT(id) AS totalCust FROM pos_customer WHERE status=".Customer::STATUS_ACTIVE;
                        $rescust = Yii::app()->db->createCommand($sqlcust)->queryRow();
                        if(!empty($rescust['totalCust'])) $custCount   =  $rescust['totalCust'];
                        echo $custCount;
                        ?>
                    </div>
                    <div class="desc">
                        Total Customer
                    </div>
                </div>
            </div>
        </div>
        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat yellow">
                <div class="visual">
                    <i class="ft ft-icon-bag"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php
                        $orderCount = 0;
                        $sqlOrder = "SELECT COUNT(id) AS totalOrder FROM pos_sales_invoice WHERE status=".SalesInvoice::STATUS_ACTIVE;
                        $resOrder = Yii::app()->db->createCommand($sqlOrder)->queryRow();
                        if(!empty($resOrder['totalOrder'])) $orderCount   =  $resOrder['totalOrder'];
                        echo $orderCount;
                        ?>
                    </div>
                    <div class="desc">
                        Total Order
                    </div>
                </div>
            </div>
        </div>

        <?php /*echo 1;exit(); */?>
        <div class="span3 responsive" data-tablet="span6" data-desktop="span3">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="ft ft-icon-basket"></i>
                </div>
                <div class="details">
                    <div class="number">
                    <?php
                        $todaysales = 0;
                        $cdate = date('Y-m-d');
                        $sqlsales = "SELECT SUM(totalPrice) AS totalPrice,SUM(instantDiscount) AS totalInstantDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$cdate."%' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
                        $ressales = Yii::app()->db->createCommand($sqlsales)->queryRow();

                        if(!empty($ressales)) $todaysales   = ($ressales['totalPrice']-$ressales['totalInstantDiscount']);
                        echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(round($todaysales,2));
                    ?>
                    </div>
                    <div class="desc">Today's Sales</div>
                </div>
            </div>
        </div>
        <div class="span3 responsive" data-tablet="span6  fix-offset" data-desktop="span3">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="ft ft-icon-money"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php
                        $todaypayable = 0;
                        $sqlpurchase = "SELECT SUM(n.totalPrice) AS totalPurchase FROM pos_good_receive_note n,pos_purchase_order p WHERE n.poId=p.id AND p.branchId=".Yii::app()->session['branchId']." AND n.status=".GoodReceiveNote::STATUS_APPROVED." AND n.grnDate LIKE '".$cdate."%' ORDER BY n.grnDate DESC";
                        $respurchase = Yii::app()->db->createCommand($sqlpurchase)->queryRow();
                        if(!empty($respurchase['totalPurchase'])) $todaypayable   =  $respurchase['totalPurchase'];
                        echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(round($todaypayable,2));
                        ?>
                    </div>
                    <div class="desc">Today's Payable </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END DASHBOARD STATS -->

    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span6">
            <!-- BEGIN PORTLET-->
            <div class="portlet solid bordered light-grey">
                <div class="portlet-title">
                    <div class="caption" style="color:#333;"><i class="icon-bar-chart"></i>Sales & Margin Comparison</div>
                </div>
                <div class="portlet-body">
                    <div id="site_statistics_loading">
                        <img src="<?php echo Yii::app()->baseUrl; ?>/media/images/loading.gif" alt="loading" />
                    </div>
                    <div id="site_statistics_content" class="hide">
                        <div id="site_statistics" class="chart"></div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
        <div class="span6">
            <!-- Begin: life time stats -->
            <div class="portlet box blue-steel overview">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-thumb-tack"></i>Overview
                    </div>
                    <!--<div class="tools">
                    <a class="collapse" href="javascript:;" data-original-title="" title="">
                    </a>
                    <a class="config" data-toggle="modal" href="#portlet-config" data-original-title="" title="">
                    </a>
                    <a class="reload" href="javascript:;" data-original-title="" title="">
                    </a>
                    <a class="remove" href="javascript:;" data-original-title="" title="">
                    </a>
                    </div>-->
                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#overview_1">New Order </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#overview_2">New Customers </a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div id="overview_1" class="tab-pane active">
                                <div class="table-responsive">
                                    <div class="slimScrollDiv">
                                        <?php if(!empty($modelOrderPending)) :?>
                                            <table class="table table-striped table-hover table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>Invoice</th>
                                                    <th>Customer</th>
                                                    <th>Total Qty</th>
                                                    <th>Total Price</th>
                                                    <th>Total Discount</th>
                                                    <th>Total Shipping</th>
                                                    <th>Details</th>
                                                </tr>
                                                </thead>
                                                <tbody id="slimscroller">
                                                <?php foreach($modelOrderPending as $keyOrder=>$dataOrder) :?>
                                                    <tr>
                                                        <td><?php echo $dataOrder->invNo;?></td>
                                                        <td><?php echo !empty($dataOrder->custId) ? !empty($dataOrder->cust->name)?$dataOrder->cust->name:'': 'Guest';?></td>
                                                        <td><?php echo $dataOrder->totalQty;?></td>
                                                        <td><?php echo $dataOrder->totalPrice;?></td>
                                                        <td><?php echo $dataOrder->totalDiscount;?></td>
                                                        <td><?php echo $dataOrder->totalTax;?></td>
                                                        <td><?php echo CHtml::link('View',array('salesInvoice/view','id'=>$dataOrder->id),array('class'=>'btn default btn-xs green-stripe')); ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        <?php else : echo '<p style="margin:10px;">No pending order</p>';endif;?>
                                    </div>
                                </div>
                            </div>
                            <div id="overview_2" class="tab-pane">
                                <div class="table-responsive">
                                    <div class="slimScrollDiv">
                                        <?php if(!empty($modelCustomerPending)) :?>
                                            <table class="table table-striped table-hover table-bordered">
                                                <colgroup>
                                                    <col>
                                                    <col>
                                                    <col>
                                                    <col>
                                                    <col>
                                                </colgroup>
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                    <th>Details</th>
                                                </tr>
                                                </thead>
                                                <tbody id="slimscroller">
                                                <?php foreach($modelCustomerPending as $keyCust=>$dataCust) :?>
                                                    <tr>
                                                        <td><?php echo $dataCust->custId;?></td>
                                                        <td><?php echo !empty($dataCust->name)?$dataCust->name:'';?></td>
                                                        <td><?php echo $dataCust->phone;?></td>
                                                        <td><?php echo $dataCust->email;?></td>
                                                        <td><?php echo CHtml::link('View',array('customer/update','id'=>$dataCust->id),array('class'=>'btn default btn-xs green-stripe')); ?></td>
                                                    </tr>
                                                <?php endforeach;?>
                                                </tbody>
                                            </table>
                                        <?php else : echo '<p style="margin:10px;">No pending customer</p>';endif;?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- Overview -->

    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span6">
            <!-- Begin: life time stats -->
            <div class="portlet box blue-steel overview">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="caption" style="color:#333;"><i class="icon-bar-chart"></i>Sales Report</div>
                    </div>
                </div>
                <div class="portlet-body">
                    <?php
                    $lastDaySale= 0;
                    $lastDayDiscountDate =date('Y-m-d',strtotime("-1 days"));
                    $sqlsaleslastDay = "SELECT SUM(totalPrice) AS totalPrice,SUM(instantDiscount) AS totalInstantDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate LIKE '".$lastDayDiscountDate."%' AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
                    $ressalesLastDay = Yii::app()->db->createCommand($sqlsaleslastDay)->queryRow();
                    if(!empty($ressalesLastDay)) $lastDaySale   = ($ressalesLastDay['totalPrice']-$ressalesLastDay['totalInstantDiscount']);

                    $weeklySale= 0;
                    $sqlsalesWeekly ="SELECT SUM(totalPrice) AS totalPrice,SUM(instantDiscount) AS totalInstantDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate >= (CURDATE() + INTERVAL -7 DAY) AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
                    $ressalesWeekly = Yii::app()->db->createCommand($sqlsalesWeekly)->queryRow();
                    if(!empty($ressalesWeekly)) $weeklySale   = ($ressalesWeekly['totalPrice']-$ressalesWeekly['totalInstantDiscount']);

                    $monthly= 0;
                    $sqlsalesMonthly ="SELECT SUM(totalPrice) AS totalPrice,SUM(instantDiscount) AS totalInstantDiscount FROM pos_sales_invoice WHERE branchId=".Yii::app()->session['branchId']." AND orderDate >= (CURDATE() + INTERVAL -30 DAY) AND status=".SalesInvoice::STATUS_ACTIVE." ORDER BY orderDate DESC";
                    $ressalesMonthly= Yii::app()->db->createCommand($sqlsalesMonthly)->queryRow();
                    if(!empty($ressalesMonthly)) $monthly   = ($ressalesMonthly['totalPrice']-$ressalesMonthly['totalInstantDiscount']);
                    ?>
                   <?php
                   $php_data_array=[
                           ['Yesterday'.' ['.'৳ '.$lastDaySale.']',$lastDaySale,500],
                           ['Daily'.' ['.'৳ '.$todaysales.']',$todaysales,800],
                           ['Weekly'.' ['.'৳ '.$weeklySale.']',$weeklySale,1000],
                           ['Monthly'.' ['.'৳ '.$monthly.']',$monthly,1000],
                   ];
                   echo "<script>
                                var my_2d = ".json_encode($php_data_array)."
                        </script>";?>
                    <div id="chart_div" style="margin-top: 40px;"></div>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>

</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Sales Report Details');
        data.addColumn('number', 'Sales In Inventory');
        data.addColumn('number', 'Sales In E-commerce');
        for(i = 0; i < my_2d.length; i++)
            data.addRow([my_2d[i][0], parseInt(my_2d[i][1]),parseInt(my_2d[i][2])]);
        var options = {
            title: 'plus2net.com Sales Profit',
            hAxis: {title: 'Month',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
    ///////////////////////////////
    ////////////////////////////////////
</script>



<!-- END DASHBOARD  -->