<script type="text/javascript" language="javascript">	
	// JQUERY STARTED
	$(document).ready(function() 
	{
		// switch deo types
		$(".isSupplier").click(function() 
		{
			var type = $(this).val();
			if(type==1) $("#supplierRow").show("slow");
			else $("#supplierRow").hide("slow");
		});
  }); 
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management<i class="icon-angle-right"></i></li>
    <li>User</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<!-- BEGIN FORM-->
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
)); ?>
<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) echo $msg; else { ?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php } ?>
    
<div class="portlet-body"> 
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?> User</a></li>
          
        </ul>
        <div class="tab-content">
			 <div class="tab-pane active" id="tab_1_1">
			<?php 
			if(isset(Yii::app()->user->isSuperAdmin) && Yii::app()->user->isSuperAdmin==1) : ?>
				<div class="row">
					<?php echo $form->labelEx($model,'branchId'); ?>
					<?php echo $form->dropDownList($model,'branchId',Branch::getorg(Branch::STATUS_ACTIVE),array('class'=>"large m-wrap",'empty'=>'Please Select a Store')); ?>
					<?php echo $form->error($model,'branchId'); ?>
				</div>
				<?php else : ?>
				<div class="row">
					<?php echo $form->hiddenField($model,'branchId', array('value'=>Yii::app()->session['branchId'])); ?>
				</div> 
			<?php endif; 
			//if($model->isNewRecord) : ?>
				<div class="row">
					<?php  echo CHtml::label('Assign Role <font style="color:#c24e4e">*</font>','',array('class'=>"control-label")); ?>
					<?php  echo CHtml::dropDownList('userTypeId[]', $userTypeArr, (isset(Yii::app()->user->isSuperAdmin) && Yii::app()->user->isSuperAdmin==1) ? UserType::getUserType(UserType::STATUS_ACTIVE) : UserType::getUserTypeByIsNotSuperAdmin(), array('style'=>'height:200px !important;','id'=>'userTypeId[]','class'=>"large m-wrap",'multiple'=>'multiple'));                                                            
					 ?>              
				</div>
		   <?php //endif; ?>
           
            <div class="row">
                <?php echo $form->labelEx($model,'isSupplier'); ?>
                <?php echo $form->radioButtonList($model,'isSupplier',array(1=>'Yes',2=>'No'),array('class'=>'isSupplier','separator'=>'&nbsp;&nbsp;&nbsp;&nbsp;','style'=>'margin-top:-3px !important;','labelOptions'=>array('style'=>'display:inline;min-width:25px !important;'),'template'=>'{input}&nbsp;{label}')); ?>
                <?php echo $form->error($model,'isSupplier'); ?>
           	</div>
            
            <?php 
				if(!empty($model->supplierId) && $model->supplierId>0) $style = 'style="display:block;"';
				else $style = 'style="display:none;"';
			?>
            
            <div id="supplierRow" class="row" <?php echo $style;?>>
            	<?php echo $form->labelEx($model,'supplierId'); ?>
				 <?php echo $form->dropDownList($model,'supplierId', Supplier::getAllSupplier(Supplier::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Supplier')); ?>
                 <?php echo $form->error($model,'supplierId'); ?>
            </div>
			
			<div class="row">
				<?php echo $form->labelEx($model,'username'); ?>
				<?php echo $form->textField($model,'username',array('class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'username'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->labelEx($model,'password'); ?>
				<?php echo $form->passwordField($model,'password',array('value'=>'','class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'conf_password'); ?>		
				<?php echo $form->passwordField($model,'conf_password',array('value'=>'','class'=>'m-wrap large')); ?>
				<?php echo $form->error($model,'conf_password'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
        </div>        
    </div>
	<div class="row buttons">
	   <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>    
</div>
<!-- form -->