<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
    <li>User<i class="icon-angle-right"></i></li>
    <li>Details User : <?php echo $model->username; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
    <?php $this->widget('bootstrap.widgets.TbDetailView', array(
        'data'=>$model,
		'type' => 'striped bordered condensed',
        'attributes'=>array(
            array(               
               'label'=>'Organization ',               
               'value'=>$model->branch->name,
             ), 
            'username',		
			array(               
               'label'=>'Status ',               
               'value'=>Lookup::item("Status", $model->status),
             ), 
        ),
    )); ?>
</div>