<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management<i class="icon-angle-right"></i></li>
    <li>User</li>
</ul>
<!-- BEGIN FORM-->
<div class="form">
<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php endif;?>

<!-- END PAGE TITLE & BREADCRUMB-->
<div class="portlet-body"> 
<?php echo CHtml::beginForm(); ?>
    <div class="tabbable tabbable-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1_1" data-toggle="tab">Change Password</a></li>       
        </ul>
        <div class="tab-content">
			<div class="tab-pane active" id="tab_1_1">		
				<div class="form">
					<?php echo CHtml::errorSummary($form); ?>    
					<div class="row">
						<?php echo CHtml::activeLabelEx($form,'verifyoldpass'); ?>
						<?php echo CHtml::activePasswordField($form,'verifyoldpass' ,array('value'=>'','class'=>'m-wrap large')); ?>
					</div>
					
					<div class="row">
						<?php echo CHtml::activeLabelEx($form,'password'); ?>
						<?php echo CHtml::activePasswordField($form,'password' ,array('value'=>'','class'=>'m-wrap large')); ?>
					</div>
					
					<div class="row">
						<?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
						<?php echo CHtml::activePasswordField($form,'verifyPassword' ,array('value'=>'','class'=>'m-wrap large')); ?>
					</div>   
				</div><!-- form -->			 
		    </div>
	    </div>
        <div class="row buttons">
            <?php echo CHtml::submitButton("Save" , array('class'=>'btn blue')); ?>
        </div>  
	</div>
<?php echo CHtml::endForm(); ?>     
</div>