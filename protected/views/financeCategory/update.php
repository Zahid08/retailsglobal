<?php
/* @var $this FinanceCategoryController */
/* @var $model FinanceCategory */

$this->breadcrumbs=array(
	'Finance Categories'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FinanceCategory', 'url'=>array('index')),
	array('label'=>'Create FinanceCategory', 'url'=>array('create')),
	array('label'=>'View FinanceCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FinanceCategory', 'url'=>array('admin')),
);
?>

<h1>Update FinanceCategory <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>