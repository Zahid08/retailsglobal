<?php
/* @var $this FinanceCategoryController */
/* @var $model FinanceCategory */

$this->breadcrumbs=array(
	'Finance Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FinanceCategory', 'url'=>array('index')),
	array('label'=>'Manage FinanceCategory', 'url'=>array('admin')),
);
?>

<h1>Create FinanceCategory</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>