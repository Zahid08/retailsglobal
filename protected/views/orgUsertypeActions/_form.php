<link href="<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/select2/select2_metro.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css"/>
<?php 	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/select2/select2.min.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.dataTables.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.DT_bootstrap.js'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/scripts/table-managed.js');
?>  

<script type="text/javascript" language="javascript">
$(document).ready(function() 
{   
	TableManaged.init(); 
	$('.controllerActions<?php echo $module; ?>').hide();
	
	/* Check/Uncheck All action*/
	jQuery('.actions_table_1 .group-checkable').change(function () {
		
	   var set = jQuery(this).attr("data-set");
	   var checked = jQuery(this).is(":checked");
		
		jQuery(set).each(function () {
			if (checked) {
				$(this).attr("checked", true);
			} else {
				$(this).attr("checked", false);
			}
		});
	
		var usertypeid = $("#select_user_type").attr("data");
		var contid = $(this).attr("cont-id");
		  
		 $('#cwaitloader'+contid).show();
		 $.ajax({
				type: "post",
				url: '<?php echo Yii::app()->request->baseUrl; ?>'+'/index.php/orgUsertypeActions/dynamicactions/',
				data: {task:checked, usertypeid : usertypeid, contid : contid,  whattodo: 'changeControllerPermision'},			
				success: function (data) {	
					$('#cwaitloader'+contid).hide();	
					$('#cokimages'+contid).show( 300 ).delay( 800 ).hide( 400 );
				}				
		});	
			
		jQuery.uniform.update(set);
	});
	
	
	$('.controllercheckbox<?php echo $module; ?> input').each(function(){
		 var set = jQuery(this).attr("data-set");
		 var checked = true; 
		 jQuery(set).each(function () {
			if (jQuery(this).is(":checked")) { checked = true; } 
			else{ checked = false; }
		});
		
		if(checked) $(this).attr("checked", true);
	});
	

	$(".checkboxes").click(function()
	{
	   var usertypeid = $("#select_user_type").attr("data");
	   var actionid = $(this).attr("data-set");
	   var task =  jQuery(this).is(":checked");			  
	   $('#awaitloader'+actionid).show();  
	   $.ajax({
				type: "post",
				url: '<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/orgUsertypeActions/dynamicactions/',
				data: {task:task, usertypeid : usertypeid, actionid : actionid,  whattodo: 'changeActionPermision'},			
				success: function (data) {	
					$('#awaitloader'+actionid).hide();	
					$('#aokimages'+actionid).show( 300 ).delay( 800 ).hide( 400 );
				}				
		});			   
   });		  		   
});
</script>    
<?php $i= 0; foreach(Controllers::getcontrollers(Controllers::STATUS_ACTIVE,$module) as $cont=>$name){  ?>
<div class="portlet box grey" style="margin-bottom:0px; width:49%; float:left; clear:none; margin-left:4px;">
        <div class="portlet-title">
            <div class="caption"><i class="icon-globe"></i><?php echo $name; ?></div>
            <div class="tools">
                <a href="javascript:;" class="expand"></a>
            </div>
        </div>
        <div class="portlet-body controllerActions<?php echo $module; ?>" style="display:none;">
            <table class="table table-striped table-bordered table-hover actions_table_1" style="margin-top:8px;">
                <thead>
                    <tr>
                        <th style="width:15px;" class="controllercheckbox<?php echo $module; ?>"><input type="checkbox" class="group-checkable" data-set=".actions_table_1 .checkboxes<?php echo $cont; ?>" cont-id="<?php echo $cont; ?>" />
                            <img src="<?php echo Yii::app()->baseUrl; ?>/media/images/ajax-loader.gif"  id="cwaitloader<?php echo $cont; ?>" style="display:none;"  />
                            <img src="<?php echo Yii::app()->baseUrl; ?>/media/images/ok.png"  id="cokimages<?php echo $cont; ?>" style="display:none;"  />                    
                        </th>
                        <th>Action Name</th>
                        <th class="hidden-480">Description</th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php foreach(ContActions::getactionsByController(1, $cont) as $actionid=>$actionname){ 
    $checkname = "action".$actionid;						
                ?>
                    <tr class="odd gradeX">
                            <?php 
                    $exists = OrgUsertypeActions::model()->find('usertypeId=:usertypeId and contActionsId=:contActionsId',array(':usertypeId'=>$selectedUserType,':contActionsId'=>$actionid));				       					
                     $checkedString = "";
                    if(!empty($exists)){	
                        
                        if($exists->status==OrgUsertypeActions::STATUS_ACTIVE)
                            $checkedString = "checked='checked'";	
                    }	
                   
                    
                            ?>
                        <td><input type="checkbox" class="checkboxes<?php echo $cont; ?> checkboxes" name="<?php echo $checkname; ?>" <?php echo $checkedString; ?> data-set="<?php echo $actionid; ?>"/>
                            <img src="<?php echo Yii::app()->baseUrl; ?>/media/images/ajax-loader.gif"  id="awaitloader<?php echo $actionid; ?>" style="display:none;"  />
                            <img src="<?php echo Yii::app()->baseUrl; ?>/media/images/ok.png"  id="aokimages<?php echo $actionid; ?>" style="display:none;"  />
                        </td>
                        <td><?php echo $actionname; ?></td>
                      <td class="hidden-480"></td>
                     </tr>
                     
                    <?php } ?> 
                </tbody>
    
            </table>
        </div>
    </div>

<?php $i++; if($i%2==0){  
    echo "<div style='clear:both;'></div>";
}?>
<?php } ?>