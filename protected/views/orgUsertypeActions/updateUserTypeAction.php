
<div class="midarea">
		
	<h1>Update Role Permission </h1>
	
	<div class="form">
	
	<?php echo CHtml::beginForm(array('orgUsertypeActions/updateUserTypeActions'),'post',array('id'=>'updateUserActions','name'=>'updateUserActions')); ?>
	
		<p class="note" align="right">Fields with <span class="required">*</span> are required.</p>
	
	<FIELDSET class="fieldset">
		<LEGEND class="legend">Update Informations</LEGEND>
	
		
		<div class="row">
			<?php echo CHtml::label('Role',''); ?>
			<?php echo CHtml::dropDownList('usertypeId','',UserType::getUserType(UserType::STATUS_ACTIVE),array(
			'prompt'=>'Select a Role',
			'id'=>'usertypeId',
			'ajax' => array(
				'type'=>'POST',
				'url'=>CController::createUrl('orgUsertypeActions/dynamicactions'),
				'update'=>'#ajaxupdates')
			)); ?>
		</div>
		<div class="row" style="margin-left:50px;"></div>
		<?php $this->beginWidget('ext.ECheckBoxTree.ECheckBoxTree') ?>
			<div class="row" style="margin-left:50px;" id="ajaxupdates">
				
			</div>
		<?php $this->endWidget() ?>	
		<div class="row">
			<?php echo CHtml::label('status',''); ?>
			<?php echo CHtml::dropDownList('status','',Lookup::items('Status')); ?>
			
		</div>
	
	</FIELDSET>
		
	
	<?php echo CHtml::endForm(); 

	 $this->widget('application.extensions.tipsy.Tipsy', array(  
		  'trigger' => 'hover',
		  'items' => array(
			array('id' => '.tiptiptip', 'htmlOptions' => '', 'gravity' => 'sw'),
		  ),  
		));
	?>
	
	</div><!-- form -->
</div>

