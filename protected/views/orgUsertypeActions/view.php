<?php
$this->breadcrumbs=array(
	'Org Usertype Actions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OrgUsertypeActions', 'url'=>array('index')),
	array('label'=>'Create OrgUsertypeActions', 'url'=>array('create')),
	array('label'=>'Update OrgUsertypeActions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrgUsertypeActions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrgUsertypeActions', 'url'=>array('admin')),
);
?>

<h1>View OrgUsertypeActions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'orgUsertypeId',
		'contActionsId',
		'status',
		'crAt',
		'crBy',
		'moAt',
		'moBy',
	),
)); ?>
