<link href="<?php echo Yii::app()->theme->baseUrl; ?>/<?php echo Yii::app()->session['skinsnames']; ?>/plugins/select2/select2_metro.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/<?php echo Yii::app()->session['skinsnames']; ?>/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/<?php echo Yii::app()->session['skinsnames']; ?>/plugins/jquery.js" type="text/javascript"></script>
<?php 	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/select2/select2.min.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.dataTables.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.DT_bootstrap.js'); 
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/scripts/table-managed.js');
?>  
<script type="text/javascript" language="javascript">
$(document).ready(function() 
{   
	TableManaged.init(); 
	
	/* Check/Uncheck All action*/
	jQuery('.actions_table_1 .group-checkable').change(function () {
		
	   var set = jQuery(this).attr("data-set");
	   var checked = jQuery(this).is(":checked");
		
		jQuery(set).each(function () {
			if (checked) {
				$(this).attr("checked", true);
			} else {
				$(this).attr("checked", false);
			}
		});
		jQuery.uniform.update(set);
   });
	     
   $(".controllerwise li a").click(function()
   {
	    $("#select_user_type_box").hide();
		var module = $(this).attr("module");		
		if($(this).attr("data")=='c'){
			var link = $(this).parents('ul').prev("a").html('<i class="icon-user"></i> Collaps All <i class="icon-angle-down"></i>');
			$('.controllerActions' + module).hide();
		}
		else if($(this).attr("data")=='e'){
			var link = $(this).parents('ul').prev("a").html('<i class="icon-user"></i> Expand All <i class="icon-angle-down"></i>');
			$('.controllerActions' + module).show();
		}
		else{
			var id = $(this).attr("data");
			$(this).parents('ul').prev("a").html('<i class="icon-user"></i>'+  $(this).text()+'<i class="icon-angle-down"></i>');
			$(this).parents('ul').prev("a").attr('data',id);
			$.ajax({
				type: "post",
				url: '<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/orgUsertypeActions/dynamicactions/',
				data: {module:module, id : id, whattodo: 'changeUserType'},			
				success: function (data) {			
					$(".updateUserTypeActions"+module).html(data);
					
				}				
			});	
		}
   });
   
   // expand collasp all
   $("#select_excols_btn").click(function()
   {
	  $("#select_excols_boxbtn").toggle('slow'); 
   });
         
   // user type expand
   $("#select_user_type").click(function()
   {
	  $("#select_user_type_box").toggle('slow'); 
   });
   
});
</script>

<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>
        User Management<i class="icon-angle-right"></i>
    </li>
    <li>Assign Permission</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
	<!--BEGIN TABS-->
    <div class="tab-content">
        <div id="tab_1_1" class="tab-pane active">
        <?php 
		$moduleList = Controllers::getmoduleList(); 
        foreach($moduleList as $moduleN) : ?>
            <div class="portlet box grey" style="border:none !important">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-cogs"></i><?php echo $moduleN.'permission'; ?></div>
                    <div class="actions">
                    	<div class="btn-group">
                            <a class="btn blue" href="#" data-toggle="dropdown" id="select_excols_btn">
                            <i class="icon-cogs"></i> Collaps All 
                            <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu controllerwise" id="select_excols_boxbtn">
                                <li><a href="#" data="c" module="<?php echo $moduleN; ?>"> Collaps All</a></li>
                                <li><a href="#" data="e" module="<?php echo $moduleN; ?>"> Expand All</a></li>
                            </ul>
                        </div>
						<div class="btn-group">
                            <a class="btn blue" href="#" data-toggle="dropdown" id="select_user_type">
                            <i class="icon-cogs"></i> Select User Type
                            <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu controllerwise" id="select_user_type_box">
                                <?php foreach($userTypeList as $i=>$v){ ?>
                                    <li><a href="#" module="<?php echo $moduleN; ?>" data="<?php echo $i; ?>"> <?php echo $v;?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="portlet-body" style=" min-height:300px;margin-bottom:7px; border:1px solid #999; padding:10px;">
                   <div class="updateUserTypeActions<?php echo $moduleN;?>"></div>
                   <div style="clear:both;"></div>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
    <!--END TABS-->
</div><!-- form -->