<?php
$this->breadcrumbs=array(
	'Org Usertype Actions',
);

$this->menu=array(
	array('label'=>'Create OrgUsertypeActions', 'url'=>array('create')),
	array('label'=>'Manage OrgUsertypeActions', 'url'=>array('admin')),
);
?>

<h1>Org Usertype Actions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
