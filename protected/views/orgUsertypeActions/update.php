<?php $this->menu=array(
	array('label'=>'Set User Role', 'url'=>array('/roles/create'),'visible'=>UserRights::getVisibility('roles', 'create')),
	array('label'=>'Manage User Role', 'url'=>array('/roles/admin'),'visible'=>UserRights::getVisibility('roles', 'admin')),

	array('label'=>'Assign Role Permission', 'url'=>array('/orgUsertypeActions/create'),'visible'=>UserRights::getVisibility('orgUsertypeActions', 'create')),
	array('label'=>'Manage Role Permission', 'url'=>array('/orgUsertypeActions/updateUserTypeActions'),'visible'=>UserRights::getVisibility('orgUsertypeActions', 'updateUserTypeActions')),

	array('label'=>'Set Organization Role', 'url'=>array('/orgUsertype/create'),'visible'=>UserRights::getVisibility('orgUsertype', 'create')),
	array('label'=>'Manage Organization Role', 'url'=>array('/orgUsertype/admin'),'visible'=>UserRights::getVisibility('orgUsertype', 'admin')),

	array('label'=>'Load Controller/Action', 'url'=>array('/contActions/addAontrollerAction'),'visible'=>UserRights::getVisibility('contActions', 'addAontrollerAction')),	

	array('label'=>'Add Role', 'url'=>array('/userType/create'),'visible'=>UserRights::getVisibility('userType', 'create')),
	array('label'=>'Manage Role', 'url'=>array('/userType/admin'),'visible'=>UserRights::getVisibility('userType', 'admin')),
); ?>
<h1>Update Role Permission </h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>