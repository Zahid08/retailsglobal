
<link href="<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/select2/select2_metro.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'];?>/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css"/>
 
 <?php 	
	
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/select2/select2.min.js');
   Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.dataTables.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/plugins/data-tables/jquery.DT_bootstrap.js'); ?>
    
     <?php 	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/'.Yii::app()->params['skinDefault'].'/scripts/table-managed.js');
	?>

     
	<script>
		$(document).ready(function() {   
			TableManaged.init(); 
		  	$('.taskwiseActions<?php echo $module; ?>').hide();
			
			/* Check/Uncheck All action*/
			jQuery('.actions_table_1 .group-checkable').change(function () {
				
               var set = jQuery(this).attr("data-set");
               var checked = jQuery(this).is(":checked");
				
                jQuery(set).each(function () {
                    if (checked) {
                        $(this).attr("checked", true);
                    } else {
                        $(this).attr("checked", false);
                    }
                });
				
					 
				var usertypeid = $("#selectedUserTypeTask<?php echo $module; ?>").attr("data");
				 
				 //alert(taskid);
				 var modulename = $(this).attr("module-id");
				//var modulename = <?php //echo $module; ?>;
				 
				 $('#tcwaitloader'+modulename).show();
	  			 $.ajax({
						type: "post",
						url: '<?php echo Yii::app()->request->baseUrl; ?>'+'/index.php/orgUsertypeActions/dynamicactions/',
						data: {task:checked, usertypeid : usertypeid, modulename : modulename,  whattodo: 'changeAllTaskPermision'},			
						success: function (data) {	
							$('#ctwaitloader'+modulename).hide();	
							$('#ctokimages'+modulename).show( 300 ).delay( 800 ).hide( 400 );
						}				
				});	
					
                jQuery.uniform.update(set);
            });
			
			
			$('.taskmodulecheckbox<?php echo $module; ?> input').each(function(){
				 var set = jQuery(this).attr("data-set");
				 var checked = true; 
				 jQuery(set).each(function () {
                    if (jQuery(this).is(":checked")) { checked = true; } 
					else{ checked = false; }
                });
				
				if(checked) $(this).attr("checked", true);
			});
			
		   
		   
		   $(".checkboxes").click(function(){
			   var usertypeid = $("#selectedUserTypeTask<?php echo $module; ?>").attr("data");
			   var taskid = $(this).attr("data-set");
			   var task =  jQuery(this).is(":checked");			  
			   $('#tawaitloader'+taskid).show();  
			  $.ajax({
						type: "post",
						url: '<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/orgUsertypeActions/dynamicactions/',
						data: {task:task, usertypeid : usertypeid, taskid : taskid,  whattodo: 'changeTaskPermision'},			
						success: function (data) {	
							$('#tawaitloader'+taskid).hide();	
							$('#taokimages'+taskid).show( 300 ).delay( 800 ).hide( 400 );
						}				
				});			   
		   });		  		   
		});
	</script>
    
    
    <table class="table table-striped table-bordered table-hover actions_table_1">
        <thead>
            <tr>
                <th style="width:15px;" class="taskmodulecheckbox<?php echo $module; ?>"><input type="checkbox" class="group-checkable" data-set=".actions_table_1 .checkboxesTask<?php echo $module; ?>"  module-id="<?php echo $module; ?>" />
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/ajax-loader.gif"  id="ctwaitloader<?php echo $module; ?>" style="display:none;"  />
    <img src="<?php echo Yii::app()->baseUrl; ?>/images/1.png"  id="ctokimages<?php echo $module; ?>" style="display:none;"  />                    
                </th>
                <th>Task Name</th>
                <th class="hidden-480">Description</th>
                
            </tr>
        </thead>
        
        <tbody>
        	<?php $i= 0; foreach(Task::getTaskListByModule(Task::STATUS_ACTIVE,$module) as $taskname){  
				 $checkname = "task".$taskname->id;
				 $checkedString = "";	
				 
				 $taskactionarray = array();
				 $permittedactionforthisrole = array();
				// OrgUsertypeActions::getUserTypeActions
				 
				if(!empty($taskname->tasks)){
					$containsSearch = 0;
					foreach($taskname->tasks as $taskaction){
						$taskactionarray[] = $taskaction->actionId;
					}
					//var_dump($taskactionarray)."<br><br><br>";
					$permittedactionforthisroleList = OrgUsertypeActions::getUserTypeActions($selectedUserType);
					foreach($permittedactionforthisroleList as $p){
						$permittedactionforthisrole[] = $p;
					}
					//var_dump($permittedactionforthisrole)."<br><br><br>\n\n\n";
					
					$containsSearch = count(array_intersect($taskactionarray, $permittedactionforthisrole)) == count($taskactionarray);
					if($containsSearch)
						$checkedString = "checked='checked'";	
				}
				
			
			
			?>
    			<tr class="odd gradeX">
                        <td><input type="checkbox" class="checkboxesTask<?php echo $module; ?> checkboxes" name="<?php echo $checkname; ?>" <?php echo $checkedString; ?> data-set="<?php echo $taskname->id; ?>"/>
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/ajax-loader.gif"  id="tawaitloader<?php echo $taskname->id; ?>" style="display:none;"  />
                        <img src="<?php echo Yii::app()->baseUrl; ?>/images/1.png"  id="taokimages<?php echo $taskname->id; ?>" style="display:none;"  />
                        </td>
                        <td><?php echo $taskname->name; ?></td>
                      <td class="hidden-480"></td>
				</tr>
 			<?php } ?>
    	</tbody>
    </table>
    
    