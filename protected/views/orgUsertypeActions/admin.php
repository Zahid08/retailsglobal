

<div class="midarea">
	<h1>Manage Permission</h1>
	
	<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
	<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div><!-- search-form -->
	<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
	
	<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'org-usertype-actions-grid',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(		
			array(      
				'name'=>'usertypeId',      
				'header'=>'User Type',
				'value'=>'$data->orgUsertype0->name',
				'filter'=>UserType::getUserType(UserType::STATUS_ACTIVE),
			),
			array(         
				'name'=>'contActionsId',     
				'header'=>'Controller',
				'value'=>'$data->contActions0->cont->name',
				'filter'=>Controllers::getcontrollers(Controllers::STATUS_ACTIVE),
			),
			array(            
				'header'=>'Actions',
				'value'=>'$data->contActions0->name',
			),
			array(
				'class'=>'CButtonColumn',
				'header'=>CHtml::dropDownList('pageSize',
					$pageSize,
					array(5=>5,10=>10,20=>20,50=>50,100=>100,500=>500),
					array(
				   //
				   // change 'user-grid' to the actual id of your grid!!
					'onchange'=>
					"$.fn.yiiGridView.update('org-usertype-actions-grid',{ data:{pageSize: $(this).val() }})",
				)),
			),
		),
	)); ?>

</div>
