<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<style>
    .row.checkboxLabel label {
        margin-top: -1.5em;
        margin-left: 23px;
    }
</style>
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Sub department
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'subdepartment-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Sub department</a></li>
    </ul>
	<div class="tab-content">
		<div class="tab-pane active span4" id="tab_1_1">
			 <div class="row">
				<?php echo $form->labelEx($model,'deptId'); ?>
				<?php echo $form->dropDownList($model,'deptId', Department::getAllDepartment(Department::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Department')); ?>
				<?php echo $form->error($model,'deptId'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
            <div class="row">
                <?php echo $form->labelEx($model,'name_bd'); ?>
                <?php echo $form->textField($model,'name_bd',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'name_bd'); ?>
            </div>

			<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap span','prompt'=>'Select Status')); ?>
				<?php echo $form->error($model,'status'); ?>
			</div>
            <?php
            if (!$model->isNewRecord ){
            ?>
            <div class="row">
                <?php echo $form->labelEx($model,'price'); ?>
                <?php echo $form->textField($model,'price',array('class'=>'m-wrap span','size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'price'); ?>
            </div>
            <div class="row checkboxLabel" style="font-size: 16px;margin-top: 17px;width: 500px;">
                <?php echo $form->checkBox($model,'retrieve_previous_price'); ?>
                <?php echo $form->labelEx($model,'retrieve_previous_price'); ?>
                <?php echo $form->error($model,'retrieve_previous_price'); ?>
            </div>
            <?php }?>
        </div>    
    </div>
	<div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   