<?php
/* @var $this SubdepartmentController */
/* @var $model Subdepartment */

$this->breadcrumbs=array(
	'Subdepartments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Subdepartment', 'url'=>array('index')),
	array('label'=>'Manage Subdepartment', 'url'=>array('admin')),
);
?>

<h1>Create Subdepartment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>