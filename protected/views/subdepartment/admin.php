<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Back Store <i class="icon-angle-right"></i></li>
    <li>Sub Department <i class="icon-angle-right"></i></li>
    <li>Manage Sub Department</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php CHtml::link('Add Subdepartment', array('subdepartment/create'), array('class'=>'btn light', 'style'=>'float:right;margin-left:10px;'));?>

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'subdepartment-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
				 'value'=>'$row+1',
		),
		array('name'=>'deptId',
			 'value'=>'!empty($data->dept->name)?$data->dept->name:null',
			 'filter'=>Department::getAllDepartment(Department::STATUS_ACTIVE),
			 //'filter'=>CHtml::activeDropDownList($model, 'deptId',Department::getAllDepartment(Department::STATUS_ACTIVE), array('class'=>'m-wrap combo combobox','prompt'=>'Select Supplier')),
		),
		'name',
		array('name'=>'status',
				 'value'=>'Lookup::item("Status", $data->status)',
				 'filter'=>Lookup::items('Status'),
		),
		'rank',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('subdepartment-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>