<?php
/* @var $this SubdepartmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Subdepartments',
);

$this->menu=array(
	array('label'=>'Create Subdepartment', 'url'=>array('create')),
	array('label'=>'Manage Subdepartment', 'url'=>array('admin')),
);
?>

<h1>Subdepartments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
