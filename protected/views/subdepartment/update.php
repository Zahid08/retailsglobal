<?php
/* @var $this SubdepartmentController */
/* @var $model Subdepartment */

$this->breadcrumbs=array(
	'Subdepartments'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Subdepartment', 'url'=>array('index')),
	array('label'=>'Create Subdepartment', 'url'=>array('create')),
	array('label'=>'View Subdepartment', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Subdepartment', 'url'=>array('admin')),
);
?>

<h1>Update Subdepartment <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>