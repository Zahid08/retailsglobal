<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
        <li>
        Settings
        <i class="icon-angle-right"></i>
    </li>
    <li>Branch <i class="icon-angle-right"></i></li>
    <li>Manage Store</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'branch-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
			 'value'=>'$row+1',
		),
		'branchIdPrefix',
		'name',
		'city',
		'postalcode',
		'phone',
		'fax',
		'email',
		'vatrn',
        /*array('name'=>'isDefault',
            'value'=>'(($data->isDefault == 1) ? "Yes" : "No")',
            'filter'=>array(1=>'Yes',0=>'No'),
        ),
        array('name'=>'isDueInvoice',
            'value'=>'(($data->isDueInvoice == 1) ? "Yes" : "No")',
            'filter'=>array(1=>'Yes',0=>'No'),
        ),
        array('name'=>'isSpecialDiscount',
            'value'=>'(($data->isSpecialDiscount == 1) ? "Yes" : "No")',
            'filter'=>array(1=>'Yes',0=>'No'),
        ),
        array('name'=>'isInstantDiscount',
            'value'=>'(($data->isInstantDiscount == 1) ? "Yes" : "No")',
            'filter'=>array(1=>'Yes',0=>'No'),
        ),*/
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=> empty(Yii::app()->user->isSuperAdmin)?'{update}':'{view} {update} {delete}',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('branch-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>