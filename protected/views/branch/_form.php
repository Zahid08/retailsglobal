<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Settings
	 <i class="icon-angle-right"></i>
	</li>
	 <li>Store
	</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'branch-form',
	'enableAjaxValidation'=>true,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp; Store</a></li>
    </ul>

	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'branchIdPrefix'); ?>
					<?php echo $form->textField($model,'branchIdPrefix',array('class'=>'input-block-level','size'=>5,'maxlength'=>5)); ?>
					<?php echo $form->error($model,'branchIdPrefix'); ?>
				</div>

				<div class="span4">

					<?php echo $form->labelEx($model,'name'); ?>
					<?php echo $form->textField($model,'name',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'name'); ?>
				</div>
                <div class="span2">


                    <?php echo $form->labelEx($model,'branch_image'); ?>
                    <?php echo $form->fileField($model,'branch_image',array('class'=>'m-wrap')); ?>
                    <?php echo $form->error($model,'branch_image'); ?>
                    <?php echo CHtml::image(Yii::app()->getBaseUrl(true).'/'.$model->branch_image,"",array("width"=>50));?>
                </div>
                <div class="span2 checkBox">
					<?php echo $form->checkBox($model,'isDefault'); ?>
                    <?php echo $form->labelEx($model,'isDefault'); ?>
                    <?php echo $form->error($model,'isDefault'); ?>
                </div>
			</div>
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'addressline'); ?>
					<?php echo $form->textField($model,'addressline',array('class'=>'input-block-level','size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'addressline'); ?>
				</div>
				<div class="span4">
					 <?php echo $form->labelEx($model,'city'); ?>
					 <?php echo $form->textField($model,'city',array('class'=>'input-block-level','size'=>60,'maxlength'=>150)); ?>
					 <?php echo $form->error($model,'city'); ?>
				</div>
                <div class="span4 checkBox">
                    <?php echo $form->checkBox($model,'isInvoiceFull'); ?>
                    <?php echo $form->labelEx($model,'isInvoiceFull'); ?>
                    <?php echo $form->error($model,'isInvoiceFull'); ?>
                </div>
			</div>
			
			<div class="row">
				<div class="span4">
					<?php echo $form->labelEx($model,'postalcode'); ?>
					<?php echo $form->textField($model,'postalcode',array('class'=>'input-block-level','size'=>60,'maxlength'=>150)); ?>
					<?php echo $form->error($model,'postalcode'); ?>
				</div>
				<div class="span4">
					 <?php echo $form->labelEx($model,'phone'); ?>
					 <?php echo $form->textField($model,'phone',array('class'=>'input-block-level','size'=>60,'maxlength'=>155)); ?>
					 <?php echo $form->error($model,'phone'); ?>
				</div>
                <div class="span4 checkBox">
                    <?php echo $form->checkBox($model,'isDueInvoice'); ?>
                    <?php echo $form->labelEx($model,'isDueInvoice'); ?>
                    <?php echo $form->error($model,'isDueInvoice'); ?>
                </div>
			</div>
			<div class="row">
			   <div class="span4">
					<?php echo $form->labelEx($model,'fax'); ?>
					<?php echo $form->textField($model,'fax',array('class'=>'input-block-level','size'=>60,'maxlength'=>155)); ?>
					<?php echo $form->error($model,'fax'); ?>
				</div>
				<div class="span4">
					 <?php echo $form->labelEx($model,'email'); ?>
					 <?php echo $form->textField($model,'email',array('class'=>'input-block-level','size'=>60,'maxlength'=>155)); ?>
					 <?php echo $form->error($model,'email'); ?>
				</div>
                <div class="span4 checkBox">
                    <?php echo $form->checkBox($model,'isSpecialDiscount'); ?>
                    <?php echo $form->labelEx($model,'isSpecialDiscount'); ?>
                    <?php echo $form->error($model,'isSpecialDiscount'); ?>
                </div>
			</div>
			<div class="row">
				<div class="span4">
					 <?php echo $form->labelEx($model,'vatrn'); ?>
					 <?php echo $form->textField($model,'vatrn',array('class'=>'input-block-level','size'=>60,'maxlength'=>150)); ?>
					 <?php echo $form->error($model,'vatrn'); ?>
				</div>   
				
				<div class="span4">
					<?php echo $form->labelEx($model,'status'); ?>
					<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'input-block-level','prompt'=>'Select Status')); ?>
					<?php echo $form->error($model,'status'); ?>
				</div>
                <div class="span4 checkBox">
                    <?php echo $form->checkBox($model,'isInstantDiscount'); ?>
                    <?php echo $form->labelEx($model,'isInstantDiscount'); ?>
                    <?php echo $form->error($model,'isInstantDiscount'); ?>
                </div>
			</div>

            <div class="row">
                <div class="span4">
                    <?php echo $form->labelEx($model,'divisions'); ?>
                    <?php echo $form->dropDownList($model,'divisions', Branch::getAllDivisions(),
                        array('class'=>'m-wrap combo combobox','prompt'=>'Select Divisions',
                            'id'=>'divisions',
                            'options'=>($model->isNewRecord) ? '' : array($model->divisions=>array('selected'=>'selected')),
                            'onchange'=>'js:$("#ajax_loaderdept").show()',
                            'ajax' => array('type'=>'POST',
                                'data'=>array('divisions'=>'js:this.value'),
                                'url'=>CController::createUrl('ajax/dynamicDistrict'),
                                'success'=>'function(data) 
                                        {
                                            $("#ajax_loaderdept").hide();
                                            $("#districts").html(data);
                                        }',
                            )
                        )); ?>
                    <span id="ajax_loaderdept" style="display:none;">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                         </span>
                    <?php echo $form->error($model,'divisions'); ?>
                </div>

                <div class="span2">
                    <?php echo $form->labelEx($model,'districts'); ?>
                    <?php echo $form->dropDownList($model,'districts',  Branch::getAllDistric(),
                        array('class'=>'m-wrap span','prompt'=>'Select Sub Department',
                            'id'=>'districts',
                            'options'=>($model->isNewRecord) ? '' : array($model->districts=>array('selected'=>'selected')),
                            'onchange'=>'js:$("#ajax_loadersubdept").show()',
                            'ajax' => array('type'=>'POST',
                                'data'=>array('districts'=>'js:this.value'),
                                'url'=>CController::createUrl('ajax/dynamicUpozela'),
                                'success'=>'function(data) 
                                {
                                    $("#ajax_loadersubdept").hide();
                                    $("#upazilas").html(data);
                                }',
                            )
                        )); ?>
                    <span id="ajax_loadersubdept" style="display:none;">
                            <img src="<?php echo Yii::app()->baseUrl;?>/media/images/ajax-loader.gif" border="0" style=" vertical-align:middle;" />
                         </span>
                    <?php echo $form->error($model,'districts'); ?>
                </div>

                <div class="span2">
                    <?php echo $form->labelEx($model,'upazilas'); ?>
                    <?php echo $form->dropDownList($model,'upazilas',  Branch::getAllUpozelaStatic(), array('class'=>'m-wrap span','prompt'=>'Select Upozela','id'=>'upazilas',)); ?>
                    <?php echo $form->error($model,'upazilas'); ?>
                </div>
            </div>


		</div>		
	</div>
	<div class="row buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   