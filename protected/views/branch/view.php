<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Branch <i class="icon-angle-right"></i></li>
    <li>Details Store # <?php echo $model->name; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'branchIdPrefix',
		'name',
		'addressline',
		'city',
		'postalcode',
		'phone',
		'fax',
		'email',
		'vatrn',
        array('name'=>'isDefault',
             'value'=>($model->isDefault == 1) ? "Yes" : "No",
        ),
		array('name'=>'isInvoiceFull',
			'value'=>($model->isInvoiceFull == 1) ? "Yes" : "No",
		),
        array('name'=>'isDueInvoice',
             'value'=>($model->isDueInvoice == 1) ? "Yes" : "No",
        ),
        array('name'=>'isSpecialDiscount',
             'value'=>($model->isSpecialDiscount == 1) ? "Yes" : "No",
        ),
        array('name'=>'isInstantDiscount',
             'value'=>($model->isInstantDiscount == 1) ? "Yes" : "No",
        ),
		array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
		'crAt',
		array(            
			'label'=>'Created By',  
			'value'=>$model->crBy0->username,),
		'moAt',
		array(            
			'label'=>'Modified By',  
			'type'=>'raw',
			'value'=> (!empty($model->moBy)) ? $model->moBy0->username : '<font color=#fccacb>Not set</font>'),
	),
)); ?>
</div>
