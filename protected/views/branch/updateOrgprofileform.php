
<h1>Theme Settings</h1>
<div class="form">
<?php echo CHtml::beginForm('updateprofile','post'); ?>

	<p class="note" align="right">Fields with <span class="required">*</span> are required.</p>

    <FIELDSET class="fieldset">
        <LEGEND class="legend">Update School Profile</LEGEND>    
        <div class="row">
            <?php echo CHtml::label("Organisation", "orgId");?>
            <?php echo Yii::app()->session['orgname']; ?>
            <?php echo CHtml::hiddenField("orgId",Yii::app()->session['orgid']); ?>
        </div> 

        <div class="row">
			<?php echo CHtml::label("Theme", "themesNames");?>
            <?php 
			
			if(isset(Yii::app()->session['themesnames'])){
				$exp = Yii::app()->session['themesnames'];
				
			}
			
			echo CHtml::dropDownList("themesNames", $exp,Organisation::getThemes(), array(
                                'empty'=>'Please Select a Theme',
								'id'=>'themesNames',
                                'ajax' => array(
                                    'type'=>'POST',
                                    'url'=>CController::createUrl('organisation/getSkin'),
                                    'update'=>'#skinNames'),
                              	    'style'=>'width:653px;'
                                ));
                ?>
		</div>    
        <div class="row">
			<?php echo CHtml::label("Skin", "skinNames");?>
            <?php 
			$arr = array();
			if(isset(Yii::app()->session['skinsnames'])){
				$arr[Yii::app()->session['skinsnames']] = Yii::app()->session['skinsnames'];
			}
			echo CHtml::dropDownList("skinNames", Yii::app()->session['skinsnames'], $arr, array('id'=>'skinNames','empty'=>'Please Select a Skin','style'=>'width:653px;'));
                ?>
		</div>  
    </FIELDSET>

	<div class="row buttons">
	<?php echo CHtml::submitButton('Update',array('name'=>'updtbtn')); ?>
		
	</div>
<?php echo CHtml::endForm(); ?>

</div><!-- form -->