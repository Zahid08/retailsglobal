<?php
/* @var $this BranchController */
/* @var $model Branch */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('class'=>'m-wrap large','size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'branchIdPrefix'); ?>
		<?php echo $form->textField($model,'branchIdPrefix',array('class'=>'m-wrap large','size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addressline'); ?>
		<?php echo $form->textField($model,'addressline',array('class'=>'m-wrap large','size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'city'); ?>
		<?php echo $form->textField($model,'city',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'postalcode'); ?>
		<?php echo $form->textField($model,'postalcode',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('class'=>'m-wrap large','size'=>60,'maxlength'=>155)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crAt'); ?>
		<?php echo $form->textField($model,'crAt',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crBy'); ?>
		<?php echo $form->textField($model,'crBy',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'moAt'); ?>
		<?php echo $form->textField($model,'moAt',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'moBy'); ?>
		<?php echo $form->textField($model,'moBy',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->