<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
    <li>Manage UserActivity</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'user-activity-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
array('header'=>'Sl.',
			 'value'=>'$row+1',
	),
		/*'user_id',*/
        array(
            'name'=>'branch_id',
            'value'=>function($model){
                $model = Branch::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$model->branch_id)));
               return $model->name;
                },
            'filter'=>(isset(Yii::app()->user->isSuperAdmin) && Yii::app()->user->isSuperAdmin==1) ? Branch::getAllorg(): '',
        ),
		'user_name',
		'ipAddress',
        'lastlogin',
        'lastlogout',
		/*
		'os',
		'device_name',
		'lastlogin',
		'lastlogout',
array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
	),
		*/
        array('name'=>'crAt',
            'type'=>'raw',
            'value'=>'date("m-d-Y", strtotime($data->crAt))',
            'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker',array(
                'model' =>$model,'attribute' =>'crAt',
                'htmlOptions' =>array('id' =>'crAt','style'=>'height: auto !important;'),
                'options' =>array('dateFormat'=>'yy-mm-dd')),true)
        ),
		array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}', //'template'=>'{update}{view}{delete}',
            'buttons' => array(
                'view'=>array(
                    'visible'=>'true',
                ),
                'delete'=>array(
                    'visible'=>'true',
                ),
            ),
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('user-activity-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>