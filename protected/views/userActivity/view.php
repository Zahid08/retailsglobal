<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
    <li>Details UserActivity # <?php echo $model->id; ?></li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="form">
<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
    'type' => 'striped bordered condensed',
	'attributes'=>array(
		'id',
		/*'user_id',*/
		/*'branch_id',*/
		'user_name',
		'email',
		'ipAddress',
		'browser',
		'os',
		'device_name',
		'lastlogin',
		'lastlogout',
array(            
			'label'=>'Status',  
			'value'=>Lookup::item('Status',$model->status),
		  ),
	),
)); ?>
</div>
