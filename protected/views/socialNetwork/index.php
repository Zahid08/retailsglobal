<?php
/* @var $this SocialNetworkController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Social Networks',
);

$this->menu=array(
	array('label'=>'Create SocialNetwork', 'url'=>array('create')),
	array('label'=>'Manage SocialNetwork', 'url'=>array('admin')),
);
?>

<h1>Social Networks</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
