<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>E-Commerce<i class="icon-angle-right"></i></li>
    <li>Social Network<i class="icon-angle-right"></i></li>
    <li><?php echo $model->isNewRecord ? 'Add' : 'Update';?> Network</li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'social-network-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Social Network</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
			    <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('class'=>'m-wrap large','size'=>50,'maxlength'=>50)); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'icon'); ?>
                <?php echo $form->fileField($model,'icon',array('class'=>'m-wrap large')); ?>
                <?php echo $form->error($model,'icon'); ?>				
                <?php
                     echo CHtml::image(Yii::app()->baseUrl.$model->icon,"",array("width"=>50))
                ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'url'); ?>
                <?php echo $form->textField($model,'url',array('class'=>'m-wrap large','size'=>60,'maxlength'=>150)); ?>
                <?php echo $form->error($model,'url'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   