<?php
/* @var $this SocialNetworkController */
/* @var $model SocialNetwork */

$this->breadcrumbs=array(
	'Social Networks'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SocialNetwork', 'url'=>array('index')),
	array('label'=>'Create SocialNetwork', 'url'=>array('create')),
	array('label'=>'View SocialNetwork', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SocialNetwork', 'url'=>array('admin')),
);
?>

<h1>Update SocialNetwork <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>