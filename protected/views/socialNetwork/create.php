<?php
/* @var $this SocialNetworkController */
/* @var $model SocialNetwork */

$this->breadcrumbs=array(
	'Social Networks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SocialNetwork', 'url'=>array('index')),
	array('label'=>'Manage SocialNetwork', 'url'=>array('admin')),
);
?>

<h1>Create SocialNetwork</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>