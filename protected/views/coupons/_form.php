<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>Setting<i class="icon-angle-right"></i></li>
	<li>Coupons / Gift Voucher </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'coupons-form',
	'enableAjaxValidation'=>true,
)); ?>

<?php echo $form->errorSummary($model); ?>

<?php if(!empty($msg)) :?>
<div class="notification note-success">
	<p><?php echo $msg;?></p>
</div>
<?php else :?>
<div class="notification note-error">
	<p>All star marked <span class="required">*</span> fields are mandatory, please fill up all mandatory fields.</p>
</div>
<?php endif;?>
<div class="tabbable tabbable-custom">
	<ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1_1" data-toggle="tab"><?php echo $model->isNewRecord ? 'Add' : 'Update';?>&nbsp;Coupons</a></li>
    </ul>
    
	<div class="tab-content">
		<div class="tab-pane active" id="tab_1_1">
            <?php if($model->isNewRecord) : ?>
                <div class="row">
                    <?php echo $form->labelEx($model,'couponQty'); ?>
                    <?php echo $form->textField($model,'couponQty',array('class'=>'m-wrap large','size'=>50,'maxlength'=>50)); ?>
                    <?php echo $form->error($model,'couponQty'); ?>
                </div>
            <?php endif;?>
            <div class="row">
                <?php echo $form->labelEx($model,'couponAmount'); ?>
                <?php echo $form->textField($model,'couponAmount',array('class'=>'m-wrap large','size'=>10,'maxlength'=>10)); ?>
                <?php echo $form->error($model,'couponAmount'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'couponStartDate'); ?>                
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       'model'=>$model,
                       'attribute'=>'couponStartDate',
                       'id'=>'couponStartDate',                      
                       //'value' =>'couponStartDate',  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                         'style'=>'width: 333px !important',
                       ),
                    ));
                ?>
                <?php echo $form->error($model,'couponStartDate'); ?>
            </div>
                <div class="row">
                <?php echo $form->labelEx($model,'couponEndDate'); ?>
                <?php //echo $form->textField($model,'couponEndDate',array('class'=>'m-wrap large')); ?>
                <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                       'model'=>$model,
                       'attribute'=>'couponEndDate',
                       'id'=>'couponEndDate',                      
                       //'value' =>'couponStartDate',  
                       'options'=>array(
                       'dateFormat'=>'yy-mm-dd', // how to change the input format? see http://docs.jquery.com/UI/Datepicker/formatDate
                       'showAnim'=>'clip',  // animation effect, see http://docs.jquery.com/UI/Effects
                       'changeMonth' => 'true',
                       'changeYear' => 'true',
                       'showButtonPanel' => 'true',
                       'constrainInput' => 'false',
                       'duration'=>'normal',
                      ),
                      'htmlOptions'=>array(
                         'class'=>'m-wrap large',
                         'style'=>'width: 333px !important',
                       ),
                    ));
                ?>
                <?php echo $form->error($model,'couponEndDate'); ?>
            </div>

            <div class="row">
                <?php echo $form->labelEx($model,'subdeptId'); ?>
                <?php echo $form->dropDownList($model,'subdeptId', Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE), array('id'=>'subdeptId','class'=>'m-wrap large','prompt'=>'Select All')); ?>
                <?php echo $form->error($model,'subdeptId'); ?>
            </div>
            
                <div class="row">
                <?php echo $form->labelEx($model,'status'); ?>
                <?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>
                	</div>   
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Save' : 'Update', array('class'=>'btn blue')); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
    
</div> <!-- form -->   