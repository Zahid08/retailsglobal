<?php
/* @var $this CouponsController */
/* @var $data Coupons */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('couponNo')); ?>:</b>
	<?php echo CHtml::encode($data->couponNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('couponAmount')); ?>:</b>
	<?php echo CHtml::encode($data->couponAmount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('couponStartDate')); ?>:</b>
	<?php echo CHtml::encode($data->couponStartDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('couponEndDate')); ?>:</b>
	<?php echo CHtml::encode($data->couponEndDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('crAt')); ?>:</b>
	<?php echo CHtml::encode($data->crAt); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('crBy')); ?>:</b>
	<?php echo CHtml::encode($data->crBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moAt')); ?>:</b>
	<?php echo CHtml::encode($data->moAt); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('moBy')); ?>:</b>
	<?php echo CHtml::encode($data->moBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rank')); ?>:</b>
	<?php echo CHtml::encode($data->rank); ?>
	<br />

	*/ ?>

</div>