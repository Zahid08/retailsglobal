<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>Home
        <i class="icon-angle-right"></i>
    </li>
    <li>User Management <i class="icon-angle-right"></i></li>
    <li>Manage Coupons / Gift Voucher </li>
</ul>
<!-- END PAGE TITLE & BREADCRUMB-->

<?php 
Yii::app()->clientScript->registerScript('re-install-date-picker', "
function reinstallDatePicker(id, data) {    
    $('#couponStartDate').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en'],{'dateFormat':'yy-mm-dd'}));
    $('#crAt').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en'],{'dateFormat':'yy-mm-dd'}));
    $('#couponEndDate').datepicker(jQuery.extend({showMonthAfterYear:false},jQuery.datepicker.regional['en'],{'dateFormat':'yy-mm-dd'}));
}
");
?>
<div class="form">
<?php $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); ?>
<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
	'id'=>'coupons-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'reinstallDatePicker', 
    'type' => 'striped bordered condensed',
    'template' => '{summary}{items}{pager}',
    'pager'=> array('header'=>'','class'=> 'MyLinkPager'),
	'columns'=>array(
		array('header'=>'Sl.',
					 'value'=>'$row+1',
			),
        array('name'=>'subdeptId',
            'value'=>'$data->subdeptId?$data->subdept->name:"All Sub Cat"',
            'filter'=> Subdepartment::getAllSubdepartment(Subdepartment::STATUS_ACTIVE),
        ),
		'couponNo',
		'couponAmount',
//		array('name'=>'crAt',
//			  'type'=>'raw',
//			  'value'=>'date("m-d-Y", strtotime($data->crAt))',
//			  'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker',array(
//			  	'model' =>$model,'attribute' =>'crAt',
//			  	'htmlOptions' =>array('id' =>'crAt','style'=>'height: auto !important;'),
//			  	'options' =>array('dateFormat'=>'yy-mm-dd')),true)
//		 ),
		array('name'=>'couponStartDate',			 
			  'value'=>'date("m-d-Y", strtotime($data->couponStartDate))',            
			  'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			  	'model' =>$model,'attribute' =>'couponStartDate',
			  	'htmlOptions' =>array('id' =>'couponStartDate','style'=>'height: auto !important;'),
			  	'options' =>array('dateFormat'=>'yy-mm-dd')),true)
		 ),
		array('name'=>'couponEndDate',			 
			  'value'=>'date("m-d-Y", strtotime($data->couponEndDate))',            
			  'filter'=>$this->widget('zii.widgets.jui.CJuiDatePicker',array(
			  	'model' =>$model,'attribute' =>'couponEndDate',
			  	'htmlOptions' =>array('id' =>'couponEndDate','style'=>'height: auto !important;'),
			  	'options' =>array('dateFormat'=>'yy-mm-dd')),true)
		 ),
			
	
		array('name'=>'status',
			 'value'=>'Lookup::item("Status", $data->status)',
			 'filter'=>Lookup::items('Status'),
			),
		//'rank',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'viewButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/view.png',
			'updateButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/update.png',
			'deleteButtonImageUrl' => Yii::app()->theme->baseUrl.'/images/gridview/delete.png',
            'header'=>CHtml::dropDownList('pageSize',
		        $pageSize,
		        array(5=>5,10=>10,20=>20,50=>50,100=>100),
		        array(
		       //
		       // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>
		        "$.fn.yiiGridView.update('coupons-grid',{ data:{pageSize: $(this).val() }})",
		    )),
		),
	),
)); ?>
</div>