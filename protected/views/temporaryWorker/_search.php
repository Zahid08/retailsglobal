<?php
/* @var $this TemporaryWorkerController */
/* @var $model TemporaryWorker */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'twNo'); ?>
		<?php echo $form->textField($model,'twNo',array('class'=>'m-wrap large','size'=>60,'maxlength'=>250)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'branchId'); ?>
		<?php echo $form->textField($model,'branchId',array('class'=>'m-wrap large','size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'wkId'); ?>
		<?php echo $form->textField($model,'wkId',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'itemId'); ?>
		<?php echo $form->textField($model,'itemId',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qty'); ?>
		<?php echo $form->textField($model,'qty',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'twDate'); ?>
		<?php echo $form->textField($model,'twDate',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'costPrice'); ?>
		<?php echo $form->textField($model,'costPrice',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crAt'); ?>
		<?php echo $form->textField($model,'crAt',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'crBy'); ?>
		<?php echo $form->textField($model,'crBy',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'moAt'); ?>
		<?php echo $form->textField($model,'moAt',array('class'=>'m-wrap large')); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'moBy'); ?>
		<?php echo $form->textField($model,'moBy',array('class'=>'m-wrap large','size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', Lookup::items('Status'), array('options'=>array(User::STATUS_ACTIVE=>array('selected'=>'selected')),'class'=>'m-wrap large','prompt'=>'Select Status')); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->