<?php
/* @var $this TemporaryWorkerController */
/* @var $model TemporaryWorker */

$this->breadcrumbs=array(
	'Temporary Workers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TemporaryWorker', 'url'=>array('index')),
	array('label'=>'Manage TemporaryWorker', 'url'=>array('admin')),
);
?>

<h1>Create TemporaryWorker</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>