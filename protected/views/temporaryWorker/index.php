<?php
/* @var $this TemporaryWorkerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Temporary Workers',
);

$this->menu=array(
	array('label'=>'Create TemporaryWorker', 'url'=>array('create')),
	array('label'=>'Manage TemporaryWorker', 'url'=>array('admin')),
);
?>

<h1>Temporary Workers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
