<?php
Yii::import('zii.widgets.CPortlet');

class EshopRecentlyViewed extends CPortlet
{
    public $companyModel;
    public $recentlyViewedModel;
    public $limit = 20;
	public function init()
	{
		parent::init();
	}
	protected function renderContent()
	{
        $recentlyViewedModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND status=:status',
                                                             'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),
                                                             'group'=>'itemName','order'=>'rand(),crAt DESC','limit'=>$this->limit,
                                                        ));
		$this->render(Yii::app()->theme->name.'/eshopRecentlyViewed',array(
            'recentlyViewedModel'=>$recentlyViewedModel
        ));
	}
}