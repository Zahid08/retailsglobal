<?php
Yii::import('zii.widgets.CPortlet');

class EshopHomePageProductsTab extends CPortlet
{
    public $featuredModel;
    public $position;
    public $newArrivalModel;
    public $topRatedModel;
    public $limit=8;
    public function init()
    {
        parent::init();
    }

    protected function renderContent()
    {
        // featured items
        $featuredModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND isFeatured=:isFeatured AND status=:status',
                                                        'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':isFeatured'=>Items::IS_FEATURED,':status'=>Items::STATUS_ACTIVE),
                                                        'group'=>'itemName','order'=>'crAt DESC','limit'=> $this->limit,
                        ));        

        // last 1 month data
        $sqlNewArrival = "SELECT * FROM pos_items WHERE isEcommerce=".Items::IS_ECOMMERCE." AND crAt>=DATE_SUB(NOW(),INTERVAL 30 DAY)
                          AND status=".Items::STATUS_ACTIVE." GROUP BY itemName ORDER BY crAt DESC LIMIT 0,$this->limit";
        $newArrivalModel = Items::model()->findAllBySql($sqlNewArrival);

        // top rated items
        $sqlTopRated = "SELECT * FROM pos_items WHERE id IN(SELECT DISTINCT(itemId) FROM pos_item_review WHERE STATUS=1 ORDER BY rating DESC)
                       AND isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY itemName ORDER BY crAt DESC LIMIT 0,$this->limit";
        $topRatedModel = Items::model()->findAllBySql($sqlTopRated);

        $this->render(Yii::app()->theme->name.'/eshopHomePageProductsTab',array(
            'featuredModel'=>$featuredModel,
            'newArrivalModel'=>$newArrivalModel,
            'topRatedModel'=>$topRatedModel,
        ));
    }
}