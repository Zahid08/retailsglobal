<?php
/*********************************************************
-*- File: Cart.php
-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
-*- Date: 2015.01.26
-*- Position:  protected/component
-*- YII-*- version 1.1.16
/*********************************************************/
class Cart 
{
	var $cart_name;       // The name of the cart/session variable
	var $items = array(); // The array for storing items in the cart
	
	/**
	 * __construct() - Constructor. This assigns the name of the cart
	 *                 to an instance variable and loads the cart from
	 *                 session.
	 *
	 * @param string $name The name of the cart.
	 */
	public function __construct($name) {
		$this->cart_name = $name;
		$this->items = Yii::app()->session[$this->cart_name];
	}
	
	/**
	 * setItemQuantity() - Set the quantity of an item.
	 *
	 * @param string $order_code The order code of the item.
	 * @param int $quantity The quantity.
	 */
	public function setItemQuantity($order_code, $quantity) {
		$this->items[$order_code] = $quantity;
	}
	
	/**
	 * getItemName() - Get the name of an item.
	 * @param string $order_code The order code of the item.
     * @return int The item name.
	 */
	public function getItemName($order_code) {
		// This is where the code that retrieves product names
		// goes. We'll just return something generic for this tutorial.
		$str_count = substr_count($order_code, '_');
		if($str_count >0) 
		{
			$arr = explode("_",$order_code);
			$order_code = $arr[0];
		}
		$model = Items::model()->findByPk($order_code);
		if(!empty($model)) return $model->itemName;
		else return '';
	}

    /**
     * getItemBrand() - Get the name of an item.
     * @param string $order_code The order code of the item.
     * @return int The item brand.
     */
    public function getItemBrand($order_code) {
        // This is where the code that retrieves product names
        // goes. We'll just return something generic for this tutorial.
        $str_count = substr_count($order_code, '_');
        if($str_count >0)
        {
            $arr = explode("_",$order_code);
            $order_code = $arr[0];
        }
        $model = Items::model()->findByPk($order_code);
        if(!empty($model)) return $model->brand->name;
        else return '';
    }
	
	/**
	 * getItemDescription() - Get the short description of an item.
	 *
	 * @param string $order_code The order code of the item.
     * @return int The description.
	 */
	public function getItemDescription($order_code) {
		// This is where the code that retrieves product names
		// goes. We'll just return something generic for this tutorial.
		$str_count = substr_count($order_code, '_');
		if($str_count >0) 
		{
			$arr = explode("_",$order_code);
			$order_code = $arr[0];
		}
		$model = Items::model()->findByPk($order_code);
		if(!empty($model)) return substr($model->specification,0,200).'..';
		else return '';
	}
	
	/**
	 * getItemQuantity() - Get the quantity of an item in the cart.
	 *
	 * @param string $order_code The order code.
	 * @return int The quantity.
	 */
	public function getItemQuantity($order_code) {
		if(!empty($this->items[$order_code]))
			return (int) $this->items[$order_code];
		else return 0;
	}
	
	/**
	 * getItemSize() - Get the size of an item in the cart.
	 *
	 * @param string $order_code The order code.
	 * @return string The Size.
	 */
	public function getItemSize($order_code) {
		$str_count = substr_count($order_code, '_');
		if($str_count >0) 
		{
			$arr = explode("_",$order_code);
			$model = Size::model()->findByPk($arr[1]);
			return $model->name;
		}
		else return "Empty";
	}
	
	/**
	 * getItemColor() - Get the color of an item in the cart.
	 *
	 * @param string $order_code The order code.
	 * @return string The Color.
	 */
	public function getItemColor($order_code) {
		$str_count = substr_count($order_code, '_');
		if($str_count >1) 
		{
			$arr = explode("_",$order_code);
			$model = Color::model()->findByPk($arr[2]);
			return $model->hexcolor;
		}
		else return "empty";
	}
	
	 /**
	 * getItemPrice() - Get the price of an item.
	 *
	 * @param string $order_code The order code of the item.
	 * @return int The price.
	 */
	public function getItemPrice($order_code) {
        $price = 0;
		// This is where the code taht retrieves prices
		// goes. We'll just say everything costs $9.99 for this tutorial.
		$str_count = substr_count($order_code, '_');
		if($str_count >0) 
		{
			$arr = explode("_",$order_code);
			$order_code = $arr[0];
		}
		$model = Items::model()->findByPk($order_code);
		if(!empty($model)) {
            if($model->tax->taxRate>0)
                $price = round($model->sellPrice+(($model->sellPrice*$model->tax->taxRate)/100));
            else $price = $model->sellPrice;
        }
		return $price;
	}

    /**
     * getItemSalesPrice() - Get the price of an item.
     *
     * @param string $order_code The order code of the item.
     * @return int The price.
     */
    public function getItemSalesPrice($order_code) {
        // This is where the code taht retrieves prices
        // goes. We'll just say everything costs $9.99 for this tutorial.
        $str_count = substr_count($order_code, '_');
        if($str_count >0)
        {
            $arr = explode("_",$order_code);
            $order_code = $arr[0];
        }
        $model = Items::model()->findByPk($order_code);
        if(!empty($model)) return $model->sellPrice;
        else return 0;
    }

    /**
     * getItemCostPrice() - Get the price of an item.
     *
     * @param string $order_code The order code of the item.
     * @return int The price.
     */
    public function getItemCostPrice($order_code) {
        // This is where the code taht retrieves prices
        // goes. We'll just say everything costs $9.99 for this tutorial.
        $str_count = substr_count($order_code, '_');
        if($str_count >0)
        {
            $arr = explode("_",$order_code);
            $order_code = $arr[0];
        }
        $model = Items::model()->findByPk($order_code);
        if(!empty($model)) return $model->costPrice;
        else return 0;
    }
	
	/**
	 * getItemImages() - Get the image of an item.
	 *
	 * @param string $order_code The order code of the item.
     * @return int The item image.
	 */
	public function getItemImages($order_code) {
		// This is where the code that retrieves product names
		// goes. We'll just return something generic for this tutorial.
		$str_count = substr_count($order_code, '_');
		if($str_count >0) 
		{
			$arr = explode("_",$order_code);
			$order_code = $arr[0];
		}
        $model = Items::model()->findByPk($order_code);
		if(!empty($model)) return $model->itemImages[0]->image;
	}
	
	/**
	 * getItems() - Get all items.
	 *
	 * @return array The items.
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * hasItems() - Checks to see if there are items in the cart.
	 *
	 * @return bool True if there are items.
	 */
	public function hasItems() {
		return (bool) $this->items;
	}
	
	/**
	 * clean() - Cleanup the cart contents. If any items have a
	 *           quantity less than one, remove them.
	 */
	public function clean() {
		foreach ( $this->items as $order_code=>$quantity ) {
			if ( $quantity < 1 )
				unset($this->items[$order_code]);
		}
	}
	
	/**
	 * save() - Saves the cart to a session variable.
	 */
	public function save() {
		$this->clean();
		Yii::app()->session[$this->cart_name] = $this->items;
	}
}