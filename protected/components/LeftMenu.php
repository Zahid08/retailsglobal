<?php
Yii::import('zii.widgets.CPortlet');

class LeftMenu extends CPortlet
{
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
		$this->render('leftMenu');
	}
}