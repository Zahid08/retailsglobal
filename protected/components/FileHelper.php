<?php

class FileHelper
{

    public static function getCacheFilePath($fileName, $imageType)
    {
        $filePath = realpath(Yii::app()->basePath . '/../media/catalog/product/cache');

        $dirArray = explode('/', $fileName);

        if(!is_dir($filePath . '/' . $dirArray[0])){
            mkdir($filePath . '/' . $dirArray[0], 0777, true);
        }

        if(!is_dir($filePath . '/' . $dirArray[0] . '/' . $dirArray[1])){
            mkdir($filePath . '/' . $dirArray[0] . '/' . $dirArray[1], 0777, true);
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        return $filePath . '/' . $dirArray[0] . '/' . $dirArray[1] .  '/' . $name . '-' . $imageType . '.' . $extension;
    }

    public static function getUniqueFilename($fileName, $filePath = null) {

        if(empty($filePath)){
            $filePath = realpath(Yii::app()->basePath . '/../media');
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $newFileName = $fileName;
        $newFilePath = $filePath . '/' . $fileName;

        $counter = 1;
        while (file_exists($newFilePath)) {
            $newFileName = $name . '_' . $counter . $extension;
            $newFilePath = $filePath . '/' . $newFileName;
            $counter++;
        }

        return $newFileName;
    }

    public static function getUniqueFilenameWithPath($fileName, $dirName = null, $basePath = null) {

        if(empty($basePath)){
            $basePath = realpath(Yii::app()->basePath . '/../media');
        }

        $filePath = $basePath;
        if(isset($dirName)){
            $filePath = $filePath . '/' . $dirName;
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $newFilePath = $filePath . '/' . $fileName;

        $counter = 1;
        while (file_exists($newFilePath)) {
            $newFileName = $name . '_' . $counter . '.' . $extension;
            $newFilePath = $filePath . '/' . $newFileName;
            $counter++;
        }

        return $newFilePath;
    }

    public static function createTwoDirSegmentFromImageName($fileName)
    {
        if(empty($filePath)){
            $filePath = realpath(Yii::app()->basePath . '/../media/catalog/product');
        }

        $name = pathinfo($fileName, PATHINFO_FILENAME);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);

        $name = strtolower(trim(str_replace(' ', '', $name)));
        $productFirstDir = $name[0];
        $productSecondDir = isset($name[1]) ? $name[1] : '-';

        if(!is_dir($filePath . '/' . $productFirstDir)){
            mkdir($filePath . '/' . $productFirstDir, 0777, true);
        }

        if(!is_dir($filePath . '/' . $productFirstDir. '/' .$productSecondDir)){
            mkdir($filePath . '/' . $productFirstDir. '/' .$productSecondDir, 0777, true);
        }

        $productDir = $productFirstDir . '/' . $productSecondDir;

        return $productDir;
    }

    public static function getFilename($filePath) {
        return $baseName = pathinfo($filePath, PATHINFO_BASENAME);
    }

    public static function getFileExt($fileName) {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        return $extension;
    }

    public static function getDirectoryExt($directoryPath) {
        $extension = pathinfo($directoryPath, PATHINFO_EXTENSION);
        return $extension;
    }

    function removeFile($fileName, $dirName = null, $basePath = null) {

        if(empty($basePath)){
            $basePath = realpath(Yii::app()->basePath . '/../media');
        }


        $filePath = $basePath;
        if(isset($dirName)){
            $filePath = $filePath . '/' . $dirName;
        }

        $filePath = $filePath . '/' . $fileName;

        if (empty($filePath) || !file_exists($filePath)) {
            return false;
        }

        if(@unlink($filePath)) {
            return true;
        }

        return false;
    }

    function isImage($fileName)
    {
        static $imageTypes = 'xcf|odg|gif|jpg|png|bmp';
        return preg_match("/$imageTypes/i",$fileName);
    }

}
