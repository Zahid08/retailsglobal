<?php
Yii::import('zii.widgets.CPortlet');

class EshopFooterPart extends CPortlet
{
    public $companyModel;
    public $categoryModel;
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $nlModel = new Newsletter;
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                       array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
        $paymentModel = PaymentMethod::model()->findAll('apiShortName!=:apiShortName AND status=:status',
                                              array(':apiShortName'=>PaymentMethod::API_GENERAL,':status'=>Company::STATUS_ACTIVE));
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        // Get All Active Content Type
        $criteria = new CDbCriteria();
        $criteria->condition = 'position=:position AND status=:status';
        $criteria->order = 'rank';
        $criteria->params = array(':position'=>ContentsType::POSITION_BOTTOM,':status'=>ContentsType::STATUS_ACTIVE);
        $contentTypeModel =ContentsType::model()->findAll($criteria);

        // random category
        $criteria = new CDbCriteria();
        $criteria->condition = 'status=:status';
        $criteria->limit = '14';
        $criteria->order = 'rand()';
        $criteria->params = array(':status'=>Company::STATUS_ACTIVE);
        $categoryModel = Category::model()->findAll($criteria);
		$this->render(Yii::app()->theme->name.'/eshopFooterPart',array(
            'nlModel'=>$nlModel,
            'companyModel'=>$companyModel,'branchModel'=>$branchModel,
            'categoryModel'=>$categoryModel,'contentTypeModel'=>$contentTypeModel,
            'paymentModel'=>$paymentModel,'socialModel'=>$socialModel,
        ));
	}
}