<?php
Yii::import('zii.widgets.CPortlet');

class EshopTopMenu extends CPortlet
{
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
		$this->render(Yii::app()->theme->name.'/eshopTopMenu');
	}
}