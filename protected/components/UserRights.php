<?php
/*********************************************************
        -*- File: UserRights.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/component
		-*-  YII-*- version 1.1.13
/*********************************************************/

class UserRights
{
	public static function getRights($contname1)
	{
		if(!isset(Yii::app()->session['visibilityarray']))
			Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(array(UserType::TYPE_GUEST=>UserType::TYPE_GUEST));
		if(array_key_exists($contname1, Yii::app()->session['visibilityarray']))
		{
			if(Yii::app()->user->isGuest)
			{
				$getRightsarray = array(
					array('allow',
						'actions'=>Yii::app()->session['visibilityarray'][$contname1],
						'users'=>array('*'),
					),
					array('deny',
						'users'=>array('*'),
					),
				);

			}
			else
			{
				$getRightsarray = array(
					array('allow',
						'actions'=>Yii::app()->session['visibilityarray'][$contname1],
						'users'=>array(Yii::app()->user->name),
					),
					array('deny',
						'users'=>array('*'),
					),
				);
			}
		}
		else
		{
			$getRightsarray = array(
				array('deny',
					'users'=>array('*'),
				),
			);
		}
		return $getRightsarray;
	}

	public static function getVisibility($contname1,$actionName)
	{
		if(array_key_exists($contname1, Yii::app()->session['visibilityarray'])){
			if(in_array($actionName, Yii::app()->session['visibilityarray'][$contname1]))
				return true;
			else
				return false;
		}
		else
			return false;
	}

	public static function getpermittedactions($userTypesIds)
	{
		$returnarray = array();
		$contArray = array();
		$criteria=new CDbCriteria;
		$criteria->condition='status=:status';
		$criteria->params=array(':status'=>OrgUsertypeActions::STATUS_ACTIVE);
		$criteria->addInCondition("usertypeId", $userTypesIds);
		$actionCollectmodel=OrgUsertypeActions::model()->findAll($criteria);

		foreach($actionCollectmodel as $Permittedactions){
				$returnarray[$Permittedactions->contActions0->cont->name][] = $Permittedactions->contActions0->name;
		}
		return $returnarray;
	}

}
