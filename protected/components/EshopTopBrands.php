<?php
Yii::import('zii.widgets.CPortlet');

class EshopTopBrands extends CPortlet
{
    public $companyModel;
    public $topBrandsModel;
    public $limit = 6;
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $topBrandsModel = Brand::model()->findAll(array('condition'=>'status=:status AND LENGTH(image)>:image',
                                                        'params'=>array(':status'=> Brand::STATUS_ACTIVE,':image'=>0),
                                                        'limit'=>$this->limit,'order'=>'id desc',
                                        ));
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
		$this->render(Yii::app()->theme->name.'/eshopTopBrands',array(
            'companyModel'=>$companyModel,
            'topBrandsModel'=>$topBrandsModel,
        ));
	}
}