<?php
Yii::import('zii.widgets.CPortlet');

class EshopBestSellers extends CPortlet
{
    public $companyModel;
    public $bestSellers;
    public $bestSellersRandom;
    public $limit = 8;
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $sqlBestSeller = "SELECT i.* FROM pos_items i,pos_sales_details s WHERE s.itemId=i.id AND i.status=".Items::STATUS_ACTIVE." AND i.isEcommerce=".Items::IS_ECOMMERCE."
                          GROUP BY s.itemId ORDER BY SUM(s.qty) DESC,s.salesDate DESC LIMIT 0,$this->limit";
        $bestSellers = Items::model()->findAllBySql($sqlBestSeller);

        $sqlBestSellerRandom = "SELECT i.* FROM pos_items i,pos_sales_details s WHERE s.itemId=i.id AND i.status=".Items::STATUS_ACTIVE." AND i.isEcommerce=".Items::IS_ECOMMERCE."
                                GROUP BY s.itemId ORDER BY RAND(),SUM(s.qty) DESC LIMIT 0,1";
        $bestSellersRandom = Items::model()->findBySql($sqlBestSellerRandom);

		$this->render(Yii::app()->theme->name.'/eshopBestSellers',array(
            'bestSellers'=>$bestSellers,
            'bestSellersRandom'=>$bestSellersRandom,

        ));
	}
}