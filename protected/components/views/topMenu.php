<!-- BEGIN TOP NAVIGATION MENU -->
<ul class="nav pull-right">
    <!-- BEGIN USER LOGIN DROPDOWN -->
    <?php
	 if(isset(Yii::app()->user->isSuperAdmin) && Yii::app()->user->isSuperAdmin==1) : ?>
    <li class="reload"><?php echo CHtml::link('<i class="fa fa-refresh"></i> <span>Reload</span>', array('site/refresh'), array('style'=>'color:#fff;'));?></li>
    <?php endif; ?>
    <!--<li class="eshop"><?php /*echo CHtml::link('<i class="ft ft-icon-shop"></i> <span>E-Shop</span>', array('eshop/index'), array('target'=>'_blank','style'=>'color:#fff;'));*/?></li>-->
    <li class="eshop"><a target="_blank" style="color:#fff;" href="http://pluspointretail.unlockretail.com/salesInvoice/create"><i class="fa fa-shopping-cart"></i> <span>Sale</span></a></li>
    <li class="eshop"><a  style="color:#fff;" href="http://pluspointretail.unlockretail.com/items/create"><i class="fa fa-plus-square"></i> <span>Add Item</span></a></li>
    <li class="eshop"><a target="_blank" style="color:#fff;" href="https://pluspointfashion.com/"><i class="ft ft-icon-shop"></i> <span>E-Shop</span></a></li>

    <!-- BEGIN NOTIFICATION DROPDOWN  
    <li class="dropdown" id="header_notification_bar">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" id="order_notification_bar">
        	<i class="fa fa-bell-o"></i>
        	<span class="badge">12</span>
        </a>
        <ul class="dropdown-menu extended notification" id="header_notification_content">
            <li class="external">
                <h3>
                    <span class="bold">12 pending</span>
                    notifications
                </h3>
                <a href="extra_profile.html">view all</a>
            </li>
            <li>
               <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
                <ul id="slimscroller">
                    <li>
                        <a href="#">
                            <span class="details">
									<span class="label label-sm label-icon label-success">
									<i class="fa fa-plus"></i>
									</span>
								    New user registered.
                           </span>
                            <span class="time">Just now</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="details">
									<span class="label label-sm label-icon label-warning">
									<i class="fa fa-bolt"></i>
									</span>
								    Server #2 not responding.
                           </span>
                            <span class="time">15 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="details">
									<span class="label label-sm label-icon label-danger">
									<i class="fa fa-bell-o"></i>
									</span>
								    Application error. 
                           </span>
                            <span class="time">22 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="details">
									<span class="label label-sm label-icon label-info">
									<i class="fa fa-bell-o"></i>
									</span>
								    Database overloaded 68%.
                           </span>
                            <span class="time">40 mins</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="details">
									<span class="label label-sm label-icon label-success">
									<i class="fa fa-plus"></i>
									</span>
								    New user registered.
                           </span>
                            <span class="time">2 hrs</span>
                        </a>
                    </li>
                </ul>
                </div>
            </li>
        </ul>
    </li>
    END NOTIFICATION DROPDOWN -->
	
	<!-- New Customer Dropdown -->
    <li class="dropdown" id="header_customer_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="order_customer_bar">
        	<i class="ft ft-icon-user-1"></i>
        	<span class="badge"><?php echo Customer::countPendingCustomer();?></span>
        </a>
        <ul class="dropdown-menu extended notification" id="header_customer_content">
            <li class="external">
                <h3>
                    <span class="bold">
                     <?php echo Customer::countPendingCustomer();?> New Customer
                    </span>
                </h3>
                <?php echo CHtml::link('View all',array('customer/admin')); ?>
            </li>
            <li>
               <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: auto;">
                <ul id="slimscroller">
                    <?php  if(!empty($modelCustomerPendingReview)) :
                          foreach($modelCustomerPendingReview as $keyPendingReview=>$dataPendingReview):
                        ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('/customer/update', array('id' => $dataPendingReview->id)) ?>">
                                <span class="photo">
                                    <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo Yii::app()->params['skinDefault']; ?>/img/avatar.png" style="border:1px solid #555; margin-right:5px;" />
                                </span>
                                <span class="subject">
                                    <span class="name"><?php echo $dataPendingReview->title.' '.$dataPendingReview->name; ?></span>
                                    <span class="time">
                                         <?php
                                           $result =UsefulFunction::getCustomerReviewTime($dataPendingReview->crAt);
                                           $priview=''  ;
                                           $day='Day';
                                           if($result['d']>1) $day='Days';
                                           if($result['d']>0) $priview.=$result['d'].'&nbsp;'.$day.'&nbsp;';
                                           if($result['h']>0) $priview.=$result['h'].'&nbsp;Hours&nbsp;';
                                           if($result['m']>0) $priview.=$result['m'].'&nbsp;Mins';
                                           echo $priview;
                                        ?>
                                    </span>
                                </span>
                                <span class="details-user">
                                    <span class="email"><?php echo $dataPendingReview->email; ?></span><br/>
                                    <span class="phone"><?php echo $dataPendingReview->phone; ?></span>
                                </span>
                            </a>
                        </li>
                    <?php
                        endforeach;
                    endif;?>
                </ul>
                </div>
            </li>
        </ul>
    </li>
    <!-- End New Customer Dropdown -->
	
	<!-------Supplier notification--------->
    <li class="dropdown" id="header_supplier_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="header_supplier_bar">
            <i class="ft ft-icon-truck"></i>
            <span class="badge"><?php echo Supplier::countPendingSupplier();?></span>
        </a>
        <ul class="dropdown-menu extended notification" id="header_supplier_content">
            <li class="external">
                <h3>
                    <span class="bold">
                     <?php echo Supplier::countPendingSupplier();?>   New Supplier
                    </span>
                </h3>
                <?php echo CHtml::link('View all',array('supplier/admin')); ?>
            </li>
            <li>
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height:auto">
                    <ul id="slimscroller">
                        <?php  if(!empty($modelSupplierPendingReview)) :
                            foreach($modelSupplierPendingReview as $keyPendingReview=>$dataPendingReview):
                                ?>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('/supplier/update', array('id' => $dataPendingReview->id)) ?>">
                                        <span class="photo">
                                            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo Yii::app()->params['skinDefault']; ?>/img/avatar.png" style="border:1px solid #555; margin-right:5px;" />
                                        </span>
                                        <span class="subject">
                                            <span class="name"><?php echo $dataPendingReview->name; ?></span>
                                            <span class="time">
                                                 <?php
                                                 $result =UsefulFunction::getCustomerReviewTime($dataPendingReview->crAt);
                                                 $priview='';
                                                 $day='Day';
                                                 if($result['d']>1) $day='Days';
                                                 if($result['d']>0) $priview.=$result['d'].'&nbsp;'.$day.'&nbsp;';
                                                 if($result['h']>0) $priview.=$result['h'].'&nbsp;Hours&nbsp;';
                                                 if($result['m']>0) $priview.=$result['m'].'&nbsp;Mins';
                                                 echo $priview;
                                                 ?>
                                            </span>
                                        </span>
                                        <span class="details-user">
                                            <span class="email"><?php echo $dataPendingReview->email; ?></span><br/>
                                            <span class="phone"><?php echo $dataPendingReview->phone; ?></span>
                                        </span>
                                    </a>
                                </li>
                            <?php
                            endforeach;
                        endif;?>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <!-------end Supplier notifications--------->
	
	<!---------Product Review ------------>
    <li class="dropdown" id="header_review_bar">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="header_review_bar">
            <i class="ft ft-icon-note"></i>
            <span class="badge"><?php echo ItemReview::countReviewAllPending();?></span>
        </a>
        <ul class="dropdown-menu extended notification" id="header_review_content">
            <li class="external">
                <h3>
                    <span class="bold">
                     <?php echo ItemReview::countReviewAllPending();?> New Product Review
                    </span>
                </h3>
                <?php echo CHtml::link('View all',array('itemReview/admin')); ?>
            </li>
            <li>
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height:auto">
                    <ul id="slimscroller">
                        <?php  if(!empty($modelItemPendingReview)) :
                            foreach($modelItemPendingReview as $keyPendingReview=>$dataPendingReview):
                                ?>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('/itemReview/update', array('id' => $dataPendingReview->id)) ?>">
                                        <span class="photo">
                                            <img alt="avatar" height="70" width="70" src="<?php echo (!empty($dataPendingReview->item->itemImages[0]->image)) ?$dataPendingReview->item->itemImages[0]->image:'';?>">
                                        </span>
                                        <span class="subject">
                                            <span class="name"><?php  echo $dataPendingReview->name; ?></span>
                                            <span class="time">
                                                 <?php
                                                 $result =UsefulFunction::getCustomerReviewTime($dataPendingReview->crAt);
                                                 $priview='';
                                                 $day='Day';
                                                 if($result['d']>1) $day='Days';
                                                 if($result['d']>0) $priview.=$result['d'].'&nbsp;'.$day.'&nbsp;';
                                                 if($result['h']>0) $priview.=$result['h'].'&nbsp;Hours&nbsp;';
                                                 if($result['m']>0) $priview.=$result['m'].'&nbsp;Mins';
                                                 echo $priview;
                                                 ?>
                                            </span>
                                        </span>
                                        <span class="details-user">
                                            <span class="email"><?php echo $dataPendingReview->email; ?></span><br/>
                                            <span class="phone"><?php echo substr($dataPendingReview->comment,0,20); ?></span>
                                        </span>
                                    </a>
                                </li>
                            <?php
                            endforeach;
                        endif;?>
                    </ul>
                </div>
            </li>
        </ul>
    </li>
    <!---------Product Review ------------>
	
	<!-- New Order Dropdown -->
    <li class="dropdown" id="header_order_bar">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" id="header_order_bar">
        	<i class="ft ft-icon-bag"></i>
        	<span class="badge"><?php echo SalesInvoice::countPendingInvoice();?></span>
        </a>
        <ul class="dropdown-menu extended notification" id="header_order_content">
            <li class="external">
                <h3>
                    <span class="bold"><?php echo SalesInvoice::countPendingInvoice();?> New Order</span>
                </h3>
                <?php echo CHtml::link('View all',array('salesInvoice/admin')); ?>
            </li>
            <li>
               <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
                <ul id="slimscroller">
                    <?php 
                    if(!empty($modelOrderPendingReview)) :
                            
                            foreach($modelOrderPendingReview as $keyPendingReview=>$dataPendingReview) :
                        ?>
                            <li>
                               <a href="<?php echo Yii::app()->createUrl('/salesInvoice/view', array('id' => $dataPendingReview->id)) ?>">
                                    <span class="photo">
                                        <img alt="avatar" height="70" width="70" src="<?php echo (!empty($dataPendingReview->salesDetails[0]->item->itemImages[0]->image))?$dataPendingReview->salesDetails[0]->item->itemImages[0]->image:'';?>">
                                    </span>
                                    <span class="subject">
                                        <span class="name">
                                            <?php //echo !empty($dataPendingReview->custId) ? $dataPendingReview->cust->name : 'Guest';?>
                                        </span>
                                        <span class="time">
                                           <?php
                                             $result =UsefulFunction::getCustomerReviewTime($dataPendingReview->crAt);
                                             $priview='';
                                             $day='Day';
                                             if($result['d']>1) $day='Days';
                                             if($result['d']>0) $priview.=$result['d'].'&nbsp;'.$day.'&nbsp;';
                                             if($result['h']>0) $priview.=$result['h'].'&nbsp;Hours&nbsp;';
                                             if($result['m']>0) $priview.=$result['m'].'&nbsp;Mins';
                                             echo $priview;
                                           ?>
                                        </span>
                                    </span>
                                    <span class="details-order">
                                        <span>Total Amount
                                            <strong><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($dataPendingReview->totalPrice+$dataPendingReview->totalTax+$dataPendingReview->totalShipping)-$dataPendingReview->totalDiscount);?>
                                            </strong>
                                        </span>
                                        <span>
                                             <?php //  echo !empty($dataPendingReview->custId) ? $dataPendingReview->cust->email : 'Guest';?>
                                        </span>
                                    </span>
                                </a>
                            </li>
                    <?php
                        endforeach;
                    endif; ?>
                </ul>
                </div>
            </li>
        </ul>
    </li>
    <!-- End New Order Dropdown -->
    <!-- USER LOGIN DROPDOWN -->
    <li class="dropdown user">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="header_user_top">
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/<?php echo Yii::app()->params['skinDefault']; ?>/img/avatar.png" style="border:1px solid #555; margin-right:5px;" />
            <span class="username" style="color:#fff;"><?php echo Yii::app()->user->name; ?></span>
            <i class="fa fa-angle-down"></i>
        </a>
        <ul class="dropdown-menu" id="header_user_top_content">
            <li><?php echo CHtml::link('<i class="ft ft-icon-key"></i> &nbsp; Sign Out', array('eshop/logout'), array());?></li>
            <li><?php echo CHtml::link('<i class="ft ft-icon-lock"></i> &nbsp; Change Password', array('user/changepassword'), array());?></li>
        </ul>
    </li>
    <!-- END USER LOGIN DROPDOWN -->   
	
</ul>
<!-- END TOP NAVIGATION MENU -->