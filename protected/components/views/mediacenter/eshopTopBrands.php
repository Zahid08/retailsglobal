<?php if(!empty($topBrandsModel)) :?>
    <section id="top-brands" class="wow fadeInUp">
        <div class="container">
            <div class="carousel-holder" >
                <div class="title-nav">
                    <h1>Top Brands</h1>
                    <?php if(count($topBrandsModel)>6){?>
                        <div class="nav-holder">
                            <a href="#prev" data-target="#owl-brands" class="slider-prev btn-prev fa fa-angle-left"></a>
                            <a href="#next" data-target="#owl-brands" class="slider-next btn-next fa fa-angle-right"></a>
                        </div>
                    <?php } ?>
                </div><!-- /.title-nav -->
                <div id="owl-brands" class="owl-carousel brands-carousel">
                    <?php foreach($topBrandsModel as $topKey=>$topBrandData):    ?>
                        <div class="carousel-item">
							<a href="javascript:void(0);" title="<?php echo $topBrandData->name;?>">
                            	<?php echo '<img src="'.$topBrandData->image.'" alt="'.$topBrandData->name.'" />';?>
							</a>
                        </div><!-- /.carousel-item -->
                    <?php endforeach;?>
                    <!-- /.carousel-item -->
                </div><!-- /.brands-caresoul -->
            </div><!-- /.carousel-holder -->
        </div><!-- /.container -->
    </section><!-- /#top-brands -->
<?php endif;?>
