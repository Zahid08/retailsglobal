<!-- ============================================================= TOP NAVIGATION ============================================================= -->
<?php if (isset(Yii::app()->session['newsletterMsg']) && !empty(Yii::app()->session['newsletterMsg'])) : ?>
   <div class="notification_msg">
      <?php
      echo Yii::app()->session['newsletterMsg'];
      unset(Yii::app()->session['newsletterMsg']);
      ?>
   </div>
<?php endif; ?>
<nav class="top-bar animate-dropdown">
   <div class="container">
      <div class="col-xs-12 <?php if (!empty(Yii::app()->session['custId']) && Yii::app()->user->name != 'Guest') : ?>col-sm-6 <?php else : ?> col-sm-7 <?php endif; ?>no-margin">
         <ul>
            <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
            <?php
            if (!empty($contentTypeModel)):
               foreach ($contentTypeModel as $mKey => $menuValue):
                  ?>
                  <li> <?php echo CHtml::link($menuValue->name, array('eshop/contentsDetails', 'id' => $menuValue->id)); ?></li>
                  <?php
               endforeach;
            endif;
            ?>
         </ul>
      </div><!-- /.col -->

      <div class="col-xs-12 <?php if (!empty(Yii::app()->session['custId']) && Yii::app()->user->name != 'Guest') : ?>col-sm-5 <?php else : ?> col-sm-4 <?php endif; ?>no-margin">
         <ul class="right">
            <li><?php echo CHtml::link('My Cart', array('eshop/viewMyCart')); ?></li>
            <?php if (!empty(Yii::app()->session['custId']) && Yii::app()->user->name != 'Guest') : ?>
               <li><?php echo CHtml::link('My Account', array('eshop/customerDashboard')); ?></li>
               <li><?php echo CHtml::link('<span style="color:#f8484a;">Signout (' . Yii::app()->user->name . ')</span> ', array('eshop/logout')); ?></li>
            <?php else : ?>
               <li><?php echo CHtml::link('Signup', array('eshop/customerSignup')); ?></li>
               <li><?php echo CHtml::link('Signin', array('eshop/customerSignin')); ?></li>
            <?php endif; ?>
            <li> <?php echo Chtml::link('Contact Us', array('eshop/contractUs')) ?></li>
         </ul>
      </div><!-- /.col -->
      <div class="col-xs-12 col-sm-1 no-margin">
         <?php if (!empty($socialModel)) : ?>
            <ul class="right">
               <?php
               foreach ($socialModel as $socialKey => $socialData) :
                  echo '<a href="' . $socialData->url . '" title="' . $socialData->name . '" target="_blank"><img alt="' . $socialData->name . '" src="' . Yii::app()->baseUrl . $socialData->icon . '" style="height:20px; margin-left:2px"></a>';
               endforeach;
               ?>
            </ul>
         <?php endif; ?>
      </div><!-- /.col -->
   </div><!-- /.container -->
</nav><!-- /.top-bar -->
<header>
   <div class="container no-padding">

      <div class="col-xs-12 col-md-3 logo-holder">
         <div class="logo">
            <?php if (!empty($companyModel)) echo CHtml::link('<img src="' . Yii::app()->baseUrl . $companyModel->logo . '" style="height:88px;" />', array('eshop/index'), array('title' => $companyModel->name)); ?>
         </div><!-- /.logo -->
      </div><!-- /.logo-holder -->

      <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder no-margin">
         <div class="contact-row">
            <div class="phone inline">
               <i class="fa fa-phone"></i> <?php if (!empty($branchModel)) echo $branchModel->phone; ?>
            </div>
            <div class="contact inline">
               <i class="fa fa-envelope"></i> <?php if (!empty($companyModel)) echo $companyModel->email; ?>
            </div>
         </div><!-- /.contact-row -->
         <!-- ==================================================== SEARCH AREA ================================================ -->
         <div class="search-area">
            <form method="post" action="<?php echo Yii::app()->getBaseUrl(true) . '/index.php/eshop/proByCatSearch/' ?>">
               <div class="control-group input-group input-group-lg">
                  <!-- <input class="search-field" placeholder="Search for item" />  -->
                  <?php
                  $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                      'name' => 'searchItems',
                      'source' => Yii::app()->createUrl('eshop/autoSuggestItemSearch'),
                      'options' => array(
                          'minLength' => 1, // console.log(ui.item.id +":"+ui.item.value);
                          'showAnim' => 'fold',
                          'select' => 'js:function(event, ui) {
								window.location.href = "' . Yii::app()->baseUrl . '/eshop/proDetails?id="+ui.item.id;
							}'
                      ),
                      'htmlOptions' => array(
                          'id' => 'items',
                          'rel' => 'val',
                          'class' => "search-field form-control",
                      ),
                  ));
                  ?>
                  <span class="input-group-btn">
                     <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                  </span>
               </div>
            </form>
         </div><!-- /.search-area -->
         <!-- =================================================== SEARCH AREA : END =============================================== -->		</div><!-- /.top-search-holder -->

      <div class="col-xs-12 col-sm-12 col-md-3 top-cart-row no-margin">
         <div class="top-cart-row-container">
            <div class="wishlist-compare-holder">
               <div class="wishlist" style="margin-top:12px;">
                  <a href="<?php echo Yii::app()->createAbsoluteUrl('eshop/customerWishlist'); ?>" title="Wishlist"><i class="fa fa-heart"></i> wishlist <span class="value" id="wishListCount"><?php echo (!empty(Yii::app()->session['custId'])) ? Wishlist::countWishListByCustomer(Yii::app()->session['custId']) : '(0)'; ?></span> </a>
               </div>
            </div>

            <!-- ============================================= SHOPPING CART DROPDOWN =========================================== -->
            <div class="top-cart-holder dropdown animate-dropdown">
               <div class="basket">
                  <?php $this->widget('EshopProByCart'); ?>
               </div><!-- /.basket -->
            </div><!-- /.top-cart-holder -->
         </div><!-- /.top-cart-row-container -->
         <!-- ============================================================= SHOPPING CART DROPDOWN : END ============================================================= -->
      </div><!-- /.top-cart-row -->
   </div><!-- /.container -->
</header>