<?php 
$eCab= Advertisement::model()->find(array('condition'=>'status=:status AND position=:position',
                                          'params'=>array(':status'=>Advertisement::STATUS_ACTIVE,':position'=>'bottom'),
                            ));

?>
<footer id="footer" class="">    
    <div class="sub-form-row">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                  <?php $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl('/eshop/newsLetter'),
                        'id'=>'newsletter-form',
                        'enableAjaxValidation'=>true,
                        'htmlOptions'=>array(
                            'class'=>'login-form cf-style-1',
                            'role'=>'form'
                        )
                    )); 
                    echo $form->textField($nlModel,'email',array('placeholder'=>'Subscribe to our newsletter')); 
                    echo $form->error($nlModel,'email');?>
                    <button class="le-button" type="submit">Subscribe</button>
                <?php $this->endWidget(); ?>
            </div>
        </div><!-- /.container -->
    </div><!-- /.sub-form-row -->

    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-2 ">
            <!-- ============================================================= CONTACT INFO ============================================================= -->
			<div class="contact-info">
				<div class="footer-logo">
					<?php if(!empty($companyModel)) : ?>
						<img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="height:88px;" />
					<?php endif; ?> 
				</div><!-- /.footer-logo -->				
			</div>
<!-- ============================================================= CONTACT INFO : END ============================================================= -->         
		</div>
		<div class="col-xs-12 col-md-2 ">
            <!-- ============================================================= CONTACT INFO ============================================================= -->
			<div class="contact-info">				
				 <p class="regular-bold"> Contact Us</p>
				<p><?php if(!empty($branchModel)) echo $branchModel->addressline.', '.$branchModel->city.', '.$branchModel->postalcode;?></p>
			</div>
<!-- ============================================================= CONTACT INFO : END ============================================================= -->         
		</div>
		
		<div class="col-xs-12 col-md-3 ">
            <!-- ============================================================= CONTACT INFO ============================================================= -->
			<div class="contact-info">				
				<div class="">
					<h3>Get in touch</h3>
					<?php if(!empty($socialModel)) : ?>
						<div class="pull-left">
							<ul>
							<?php foreach($socialModel as $socialKey=>$socialData) :
									echo '<a style="padding-right:2px;" href="'.$socialData->url.'" title="'.$socialData->name.'" target="_blank"><img alt="'.$socialData->name.'" src="'.Yii::app()->baseUrl.$socialData->icon.'" style="height:40px;"></a>';
							 endforeach; ?>
							</ul>
						</div>
					<?php endif;?>
				</div><!-- /.social-icons -->				
			</div>
<!-- ============================================================= CONTACT INFO : END ============================================================= -->         
		</div>
		<div class="col-xs-12 col-md-3 ">
            <!-- ============================================================= CONTACT INFO ============================================================= -->
			<div class="">		
				<div class="social-icons">       
					<?php if(!empty($eCab)) : ?>
						<div class="pull-left">
							<h3><?php echo $eCab['title'];?></h3>
							<ul>
							<?php 
								echo '<a href="'.$eCab['url'].'" title="'.$eCab['title'].'" target="_blank"><img alt="'.$eCab['title'].'" src="'.$eCab['image'].'"></a>';
							  ?>
							</ul>
						</div>
					<?php endif;?>
				</div><!-- /.social-icons -->  
			</div>
<!-- ============================================================= CONTACT INFO : END ============================================================= -->         
		</div>
		
		
<?php /*
 <div class="col-xs-12 col-md-6 no-margin">
                <!-- ============================================================= LINKS FOOTER ============================================================= -->
    <div class="link-widget" id="linkWidget">
        <div class="widget" id="footerLinkWidget">
            <h3>Category</h3>
            <?php if(!empty($categoryModel)):
                foreach($categoryModel as $category): ?>
                    <ul id="footerLink">
                        <li> <?php echo CHtml::link($category->name,array('eshop/proByCat','catId'=>$category->id)); ?></li>
                    </ul>
            <?php endforeach;
            endif; ?>
        </div><!-- /.widget -->
    </div><!-- /.link-widget -->
    <!-- /.link-widget -->
<!-- /.link-widget -->
<!-- ============================================================= LINKS FOOTER : END ============================================================= -->  
 </div>  */  ?>
<div class="col-xs-12 col-md-2 no-margin">
    <div class="link-widget">
        <div class="widget">
            <h3 style="padding-bottom:4px;">Information</h3>
            <ul>                
				<?php if(!empty($contentTypeModel)):
					foreach($contentTypeModel as $mKey=>$menuValue):?>
						<li> <?php echo CHtml::link($menuValue->name,array('eshop/contentsDetails','id'=>$menuValue->id)); ?></li>
					<?php endforeach;
				endif; ?>
            </ul>
        </div><!-- /.widget -->
    </div>
</div>
 </div><!-- /.container -->
    </div><!-- /.link-list-row -->

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="copyright">
                    &copy; <?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt)).'-'.date("Y");?>  <a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a> - all rights reserved, Developed By : <a href="http://www.unlockliveretail.com/" target="_blank">UNLOCKLIVE</a>
                </div><!-- /.copyright -->
            </div>
            <div class="col-xs-12 col-sm-1 no-margin">
                <div class="copyright">
                    We Accept
                </div><!-- /.copyright -->
            </div>
            <div class="col-xs-12 col-sm-5 no-margin">
			<?php if(!empty($paymentModel)) : ?>
				<div class="payment-methods ">
                    <ul>
					<?php foreach($paymentModel as $paymentKey=>$paymentData) :
                        	echo '<li><img alt="'.$paymentData->title.'" src="'.$paymentData->image.'"></li>';
                     endforeach; ?>
                    </ul>
                </div>
			<?php endif;?>
            </div>
        </div><!-- /.container -->
    </div><!-- /.copyright-bar -->
</footer><!-- /#footer -->
<style>
    .payment-methods img{
        max-width: 460px;
    }
</style>