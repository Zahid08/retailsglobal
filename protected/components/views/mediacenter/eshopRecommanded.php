<section id="recommended-products" class="wow fadeInUp">
<div class="container">
<div class="carousel-holder hover">
<div class="title-nav">
    <h2 class="h1">Recommended Products</h2>
    <div class="nav-holder">
        <a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
        <a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
    </div>
</div><!-- /.title-nav -->
<div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
<?php foreach($recommandedModel as $rVKey=>$recommandedData):?>
    <div class="no-margin carousel-item product-item-holder size-small hover">
        <div class="product-item">
            <?php // mark up new + sales + best sales
				echo (Items::getIsNewItem($recommandedData->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
				echo (Items::getIsSaleItem($recommandedData->id))?'<div class="ribbon red"><span>sale</span></div>':'';
				echo (Items::getIsBestSaleItem($recommandedData->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
			?>
            <div class="image">
                <?php if(!empty($recommandedData->itemImages)) : ?>
                    <?php echo CHtml::link('<img alt="" src="'.$recommandedData->itemImages[0]->image.'" data-echo="'.$recommandedData->itemImages[0]->image.'" style="width:194px;height:143px;" />',array('eshop/proDetails','id'=>$recommandedData->id),  array('title'=>$recommandedData->itemName));?>
                <?php endif;?>
            </div>
            <div class="body">
                <div class="title">
                    <?php if(!empty($recommandedData->itemName)) : ?>
                        <?php echo CHtml::link($recommandedData->itemName,array('eshop/proDetails','id'=>$recommandedData->id));?>
                    <?php endif;?>
                </div>
                <div class="brand">
                    <?php echo $recommandedData->brand->name;?>
                </div>
            </div>
            <div class="prices">
                <?php if(Items::getItemWiseOfferPrice($recommandedData->id)>0) :?>
					<div class="price-prev" style="text-decoration: line-through;">
						<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recommandedData->id);?>
					</div>
					<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($recommandedData->id);?></div>
				<?php else : ?>
					<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recommandedData->id);?></div>
				<?php endif;?>
            </div>
            <div class="hover-area">
                <div class="add-cart-button">
                    <?php echo CHtml::link('add to cart',array('eshop/proDetails','id'=>$recommandedData->id),  array('class'=>'le-button'));?>
                </div>
                <div class="wish-compare">
                    <a href="javascript:void(0);" id="<?php echo $recommandedData->id;?>" class="btn-add-to-wishlist" title="add to wishlist">wishlist</a>
					<?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$recommandedData->id),  array('class'=>'btn-add-to-compare'));?>
                </div>
            </div>
        </div><!-- /.product-item -->
    </div><!-- /.product-item-holder -->
<?php endforeach; ?>
<!-- /.product-item-holder -->
</div><!-- /.carousel-holder -->
</div><!-- /.container -->
</section><!-- /#recently-reviewd -->