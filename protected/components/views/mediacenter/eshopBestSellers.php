<!-- ========================================= BEST SELLERS ========================================= -->
<section id="bestsellers" class="color-bg wow fadeInUp">
    <div class="container">
        <h1 class="section-title">Best Sellers</h1>

        <div class="product-grid-holder medium clearfix best-sellers">
            <div class="col-xs-12 col-md-12 no-margin">
                <div class="row no-margin">
                    <?php foreach($bestSellers as $bestSellKey=>$bestSellData):?>
                        <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
                            <div class="product-item">
                                <?php // mark up new + sales + best sales
                                    echo (Items::getIsNewItem($bestSellData->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
                                    echo (Items::getIsSaleItem($bestSellData->id))?'<div class="ribbon red"><span>sale</span></div>':'';
                                    echo (Items::getIsBestSaleItem($bestSellData->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
                                ?>
                                <div class="image">
                                    <?php if(!empty($bestSellData->itemImages)) : ?>
                                        <?php echo CHtml::link('<img alt="" src="'.$bestSellData->itemImages[0]->image.'" data-echo="'.$bestSellData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$bestSellData->id),  array('title'=>$bestSellData->itemName));?>
                                    <?php endif;?>
                                </div>
                                <div class="body">
                                    <div class="label-discount clear"></div>
                                    <div class="title">
                                        <?php if(!empty($bestSellData->itemName)) : ?>
                                            <?php echo CHtml::link($bestSellData->itemName,array('eshop/proDetails','id'=>$bestSellData->id));?>
                                        <?php endif;?>
                                    </div>
                                    <div class="brand"><?php echo $bestSellData->brand->name;?></div>
                                </div>
                                <div class="prices">
                                    <?php if(Items::getItemWiseOfferPrice($bestSellData->id)>0) :?>
                                        <div class="price-prev" style="text-decoration: line-through;">
                                            <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($bestSellData->id);?>
                                        </div>
                                        <div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($bestSellData->id);?></div>
                                    <?php else : ?>
                                        <div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($bestSellData->id);?></div>
                                    <?php endif;?>
                                </div>
                                <div class="hover-area">
                                    <div class="add-cart-button">
                                        <?php echo CHtml::link('add to cart',array('eshop/proDetails','id'=>$bestSellData->id),  array('class'=>'le-button'));?>
                                    </div>
                                    <div class="wish-compare">
                                        <a href="javascript:void(0);" id="<?php echo $bestSellData->id;?>" class="btn-add-to-wishlist" title="add to wishlist">wishlist</a>
                                        <?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$bestSellData->id),  array('class'=>'btn-add-to-compare'));?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.product-item-holder -->
                    <?php endforeach; ?>
                </div><!-- /.row -->
            </div><!-- /.col -->
        </div><!-- /.product-grid-holder -->
    </div><!-- /.container -->
</section><!-- /#bestsellers -->
<!-- ========================================= BEST SELLERS : END ========================================= -->