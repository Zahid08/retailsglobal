<?php if(!empty($advertisementModel)){  ?>
	<section id="banner-holder" class="wow fadeInUp">
		<div class="container">
			<?php foreach($advertisementModel as $aKey=>$aData) : ?>
				<div class="col-xs-12 col-lg-6 banner" style="padding:0px 2px; margin:0px;">
                    <?php if(!empty($aData->url)) : ?>
                        <a href="<?php echo $aData->url;?>" target="_blank" title="<?php echo $aData->title;?>">
                            <img class="banner-image" src="<?php echo $aData->image;?>" alt="<?php echo $aData->title;?>" style="width:570px;height:157px;">
                        </a>
                    <?php else : ?>
					   <img class="banner-image" src="<?php echo $aData->image;?>" alt="<?php echo $aData->title;?>" height="157" width="570" >
                    <?php endif;?>
				</div>
			<?php endforeach; ?>
		</div><!-- /.container -->
	</section><!-- /#banner-holder -->
<?php } ?>