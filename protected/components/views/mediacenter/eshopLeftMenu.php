<!-- ================================== TOP NAVIGATION ================================== -->
<div class="side-menu animate-dropdown">
    <div class="head"><i class="fa fa-list"></i> all departments</div>
    <nav class="yamm megamenu-horizontal" role="navigation">
        <ul class="nav">
        <?php
        foreach(Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey=>$dept) : ?>
            <li class="dropdown menu-item">
                <?php echo CHtml::link($dept->name,array('eshop/index'),
                           array('title'=>$dept->name,'class'=>"dropdown-toggle",'data-hover'=>"dropdown",'data-toggle'=>"dropdown"));
				
						// column count by item count
						if(Category::getAllCatCountByDept($dept->id)>10)
							$itemColumn = ceil(Category::getAllCatCountByDept($dept->id)/10);
						else $itemColumn = 1;
                        
                        // width automation (%)
                        if($itemColumn==1) $width = 'auto';
                        else if($itemColumn==2) $width = '175%';
                        else if($itemColumn==3) $width = '275%';
                        else $width = '350%';
				if(!empty(Category::getAllCatByDept($dept->id))) : ?>
                <ul class="dropdown-menu mega-menu" style="<?php echo 'width:'.$width;?>;-moz-column-count:<?php echo $itemColumn;?>;-webkit-column-count:<?php echo $itemColumn;?>;-o-column-count:<?php echo $itemColumn;?>;-ms-column-count:<?php echo $itemColumn;?>;column-count:<?php echo $itemColumn;?>">
                    <li>
						<?php
							foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) :
								echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));
							endforeach;
						?>
                     </li>
                </ul>
				<?php endif;?>
            </li><!-- /.menu-item -->
        <?php endforeach;?>
            <li>
                <?php echo CHtml::link('All Categories',array('eshop/allDeptSubDeptCat'),array('title'=>'More'));?>
            </li> 
        </ul><!-- /.nav -->
    </nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->