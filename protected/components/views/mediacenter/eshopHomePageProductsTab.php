<div id="products-tab" class="wow fadeInUp">
    <div class="container">
        <div class="tab-holder">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" >
            <?php if(!empty($featuredModel)) :?>
                <li class="active"><a href="#featured" data-toggle="tab">featured</a></li>
            <?php endif; ?>
            <?php if(!empty($newArrivalModel)) :?>
                <li><a href="#new-arrivals" data-toggle="tab">new arrivals</a></li>
            <?php endif; ?>
            <?php if(!empty($topRatedModel)) :?>
            	<li><a href="#top-sales" data-toggle="tab">top rated</a></li>
            <?php endif; ?>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!---********************** FEATURED PRODUCTED ********************************   -->
            <?php if(!empty($featuredModel)):?>
                <div class="tab-pane active" id="featured">
                    <div class="product-grid-holder clearfix">
                        <?php foreach($featuredModel as $fImageKey=>$feturedImageData) :?>
                            <div class="col-sm-4 col-md-3  no-margin product-item-holder hover">
                                <div class="product-item">
                                    <?php // mark up new + sales + best sales
										echo (Items::getIsNewItem($feturedImageData->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
										echo (Items::getIsSaleItem($feturedImageData->id))?'<div class="ribbon red"><span>sale</span></div>':'';
										echo (Items::getIsBestSaleItem($feturedImageData->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
                                    ?>
                                    <div class="image">
                                        <?php if(!empty($feturedImageData->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$feturedImageData->itemImages[0]->image.'" data-echo="'.$feturedImageData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$feturedImageData->id),  array('title'=>$feturedImageData->itemName));?>
                                        <?php endif;?>
                                    </div>
                                    <div class="body">
                                        <div class="title">
                                            <?php if(!empty($feturedImageData->itemName)) : ?>
                                                <?php echo CHtml::link($feturedImageData->itemName,array('eshop/proDetails','id'=>$feturedImageData->id));?>
                                            <?php endif;?>
                                        </div>
                                        <div class="brand"><?php echo $feturedImageData->brand->name;?></div>
                                    </div>
                                    <div class="prices">
                                        <?php if(Items::getItemWiseOfferPrice($feturedImageData->id)>0) :?>
											<div class="price-prev" style="text-decoration: line-through;">
												<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($feturedImageData->id);?>
											</div>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($feturedImageData->id);?></div>
										<?php else : ?>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($feturedImageData->id);?></div>
										<?php endif;?>
                                    </div>
                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <?php echo CHtml::link('add to cart',array('eshop/proDetails','id'=>$feturedImageData->id),  array('class'=>'le-button'));?>
                                        </div>
                                        <div class="wish-compare">
											<a href="javascript:void(0);" id="<?php echo $feturedImageData->id;?>" class="btn-add-to-wishlist" title="add to wishlist">add to wishlist</a>
											<?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$feturedImageData->id),  array('class'=>'btn-add-to-compare'));?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                      </div>
                </div>
            <?php endif; ?>
            <!---********************** FEATURED PRODUCTED ********************************   -->
			
            <!---********************** NEW ARRIVAL  PRODUCTED ********************************   -->
            <?php if(!empty($newArrivalModel)) :?>
                <div class="tab-pane" id="new-arrivals">
                    <div class="product-grid-holder clearfix">
                        <?php foreach($newArrivalModel as $newProduct=>$newArrivalData) :?>
                            <div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
                                <div class="product-item">
                                    <?php // mark up new + sales + best sales
										echo (Items::getIsNewItem($newArrivalData->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
										echo (Items::getIsSaleItem($newArrivalData->id))?'<div class="ribbon red"><span>sale</span></div>':'';
										echo (Items::getIsBestSaleItem($newArrivalData->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
									?>
                                    <div class="image">
                                        <?php if(!empty($newArrivalData->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$newArrivalData->itemImages[0]->image.'" data-echo="'.$newArrivalData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$newArrivalData->id),  array('title'=>$newArrivalData->itemName));?>
                                        <?php endif;?>
                                    </div>
                                    <div class="body">
                                        <div class="label-discount clear"></div>
                                        <div class="title">
                                            <?php if(!empty($newArrivalData->itemName)) : ?>
                                                <?php echo CHtml::link($newArrivalData->itemName,array('eshop/proDetails','id'=>$newArrivalData->id));?>
                                            <?php endif;?>
                                        </div>
                                        <div class="brand"><?php echo $newArrivalData->brand->name;?></div>
                                    </div>
                                    <div class="prices">
                                        <?php if(Items::getItemWiseOfferPrice($newArrivalData->id)>0) :?>
											<div class="price-prev" style="text-decoration: line-through;">
												<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($newArrivalData->id);?>
											</div>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($newArrivalData->id);?></div>
										<?php else : ?>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($newArrivalData->id);?></div>
										<?php endif;?>
                                    </div>
                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <?php echo CHtml::link('add to cart',array('eshop/proDetails','id'=>$newArrivalData->id),  array('class'=>'le-button'));?>
                                        </div>
                                        <div class="wish-compare">
											<a href="javascript:void(0);" id="<?php echo $newArrivalData->id;?>" class="btn-add-to-wishlist" title="add to wishlist">add to wishlist</a>
                                            <?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$newArrivalData->id),  array('class'=>'btn-add-to-compare'));?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                 </div>
            <?php endif; ?>
           <!---********************** NEW ARRIVAL  PRODUCTED ********************************   -->
            <!---**********************TOP RATED PRODUCTED ********************************   -->
            <?php if(!empty($topRatedModel)):?>
                <div class="tab-pane" id="top-sales">
                    <div class="product-grid-holder clearfix">
                        <?php foreach($topRatedModel as $topRateKey=>$topRateData) :?>
                            <div class="col-sm-4 col-md-3 no-margin product-item-holder hover">
                                <div class="product-item">
                                    <?php // mark up new + sales + best sales
										echo (Items::getIsNewItem($topRateData->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
										echo (Items::getIsSaleItem($topRateData->id))?'<div class="ribbon red"><span>sale</span></div>':'';
										echo (Items::getIsBestSaleItem($topRateData->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
									?>
                                    <div class="image">
                                        <?php if(!empty($topRateData->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$topRateData->itemImages[0]->image.'" data-echo="'.$topRateData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$topRateData->id),  array('title'=>$topRateData->itemName));?>
                                        <?php endif;?>
                                    </div>
                                    <div class="body">
                                        <div class="label-discount clear"></div>
                                        <div class="title">
                                            <?php if(!empty($topRateData->itemName)) : ?>
                                                <?php echo CHtml::link($topRateData->itemName,array('eshop/proDetails','id'=>$topRateData->id));?>
                                            <?php endif;?>
                                        </div>
                                        <div class="brand"><?php echo $topRateData->brand->name;?></div>
                                    </div>
                                    <div class="prices">
                                        <?php if(Items::getItemWiseOfferPrice($topRateData->id)>0) :?>
											<div class="price-prev" style="text-decoration: line-through;">
												<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($topRateData->id);?>
											</div>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($topRateData->id);?></div>
										<?php else : ?>
											<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($topRateData->id);?></div>
										<?php endif;?>
                                    </div>
                                    <div class="hover-area">
                                        <div class="add-cart-button">
                                            <?php echo CHtml::link('add to cart',array('eshop/proDetails','id'=>$topRateData->id),  array('class'=>'le-button'));?>
                                        </div>
                                        <div class="wish-compare">
                                           <a href="javascript:void(0);" id="<?php echo $topRateData->id;?>" class="btn-add-to-wishlist" title="add to wishlist">add to wishlist</a>
                                            <?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$topRateData->id),  array('class'=>'btn-add-to-compare'));?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                     </div>
                </div>
            <?php endif;?>
            <!---**********************TOP RATED PRODUCTED ********************************   -->
        </div>
        </div>
    </div>
</div>