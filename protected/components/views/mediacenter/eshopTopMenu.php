<nav id="top-megamenu-nav" class="megamenu-vertical animate-dropdown">
    <div class="container">
        <div class="yamm navbar">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mc-horizontal-menu-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.navbar-header -->
            
            <div class="collapse navbar-collapse" id="mc-horizontal-menu-collapse">
                <ul class="nav navbar-nav">
                <?php
                    $ptrCount = 0;
                	foreach(Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey=>$dept) : 
                    if(Category::getAllCatCountByDept($dept->id)>0) : $ptrCount++; 
                        if($ptrCount>10) break; ?>
                        <li class="dropdown">
                            <?php echo CHtml::link($dept->name,array('eshop/index'),
                                       array('title'=>$dept->name,'class'=>"dropdown-toggle",'data-hover'=>"dropdown",'data-toggle'=>"dropdown"));
                                // column count by item count
                                if(Category::getAllCatCountByDept($dept->id)>10)
                                    $itemColumn = ceil(Category::getAllCatCountByDept($dept->id)/10);
                                else $itemColumn = 1;
                             if(!empty(Category::getAllCatByDept($dept->id))) : ?>
                             <ul class="dropdown-menu menu-col" style="-moz-column-count:<?php echo $itemColumn;?>;-webkit-column-count:<?php echo $itemColumn;?>;-o-column-count:<?php echo $itemColumn;?>;-ms-column-count:<?php echo $itemColumn;?>;column-count:<?php echo $itemColumn;?>">
                             <?php 
                                foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) : ?>
                                    <li>
                                        <?php echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));?>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                            <?php endif;?>
                        </li>
                    <?php endif; endforeach;?>
                    <li>
                        <?php echo CHtml::link('All Categories',array('eshop/allDeptSubDeptCat'),array('title'=>'More'));?>
                    </li> 
                </ul><!-- /.navbar-nav -->
            </div><!-- /.navbar-collapse -->
        </div><!-- /.navbar -->
    </div><!-- /.container -->
</nav><!-- /.megamenu-vertical -->
