<div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
   <div class="more-info-tab clearfix ">
      <h3 class="new-product-title pull-left">New Products</h3>
      <ul class="nav nav-tabs nav-tab-line pull-right" id="new-products-1">
         <?php if (!empty($featuredModel)) : ?>
            <li class="active"><a data-transition-type="backSlide" href="#smartphone" data-toggle="tab">featured</a></li>
            <?php
         endif;
         if (!empty($newArrivalModel)) :
            ?>
            <li><a data-transition-type="backSlide" href="#laptop" data-toggle="tab">new arrivals</a></li>
            <?php
         endif;
         if (!empty($topRatedModel)) :
            ?>
            <li><a data-transition-type="backSlide" href="#apple" data-toggle="tab">top rated</a></li>
         <?php endif; ?>
      </ul><!-- /.nav-tabs -->
   </div>
   <div class="tab-content outer-top-xs">
      <?php if (!empty($featuredModel)): ?>
         <div class="tab-pane in active" id="smartphone">
            <div class="product-slider">
               <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                  <?php foreach ($featuredModel as $fImageKey => $feturedImageData) : ?>
                     <div class="item item-carousel">
                        <div class="products">
                           <div class="product">
                              <div class="product-image">
                                 <div class="image">
                                    <?php if (!empty($feturedImageData->itemImages)) : ?>
                                       <?php echo CHtml::link('<img alt="" src="' . $feturedImageData->itemImages[0]->image . '" data-echo="' . $feturedImageData->itemImages[0]->image . '" style="width:194px;height:243px;" />', array('eshop/proDetails', 'id' => $feturedImageData->id), array('title' => $feturedImageData->itemName)); ?>
                                    <?php endif; ?>
                                 </div><!-- /.image -->

                                 <?php
                                 echo (Items::getIsNewItem($feturedImageData->id)) ? '<div class="tag new" style="position: absolute;"><span>new!</span></div>' : '';
                                 echo (Items::getIsSaleItem($feturedImageData->id)) ? '<div class="tag sale" style="position: absolute;top:20px;"><span>sale</span></div>' : '';
                                 //echo (Items::getIsBestSaleItem($feturedImageData->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                                 ?>

                              </div><!-- /.product-image -->
                              <div class="product-info text-left">
                                 <h3 class="name">
                                    <?php if (!empty($feturedImageData->itemName)) : ?>
                                       <?php echo CHtml::link($feturedImageData->itemName, array('eshop/proDetails', 'id' => $feturedImageData->id)); ?>
                                    <?php endif; ?>
                                 </h3>
                                 <div class="description"></div>
                                 <div class="product-price">
                                    <?php if (Items::getItemWiseOfferPrice($feturedImageData->id) > 0) : ?>
                                       <div class="price">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($feturedImageData->id); ?>
                                       </div>
                                       <div class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($feturedImageData->id); ?></div>
                                    <?php else : ?>
                                       <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($feturedImageData->id); ?></span>
                                    <?php endif; ?>
                                 </div><!-- /.product-price -->
                              </div><!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                 <div class="action">
                                    <ul class="list-unstyled">
                                       <li class="add-cart-button btn-group">
                                          <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                             <i class="fa fa-shopping-cart"></i>
                                          </button>
                                          <div class="btn btn-primary add-cart-button">
                                             <?php echo CHtml::link('add to cart', array('eshop/proDetails', 'id' => $feturedImageData->id), array('class' => 'le-button')); ?>
                                          </div>
                                       </li>
                                       <li class="lnk wishlist">
                                          <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $feturedImageData->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                                             <i class="icon fa fa-heart"></i>
                                          </a>
                                       </li>
                                       <li class="lnk">
                                          <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $feturedImageData->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                                       </li>
                                    </ul>
                                 </div><!-- /.action -->
                              </div><!-- /.cart -->
                           </div><!-- /.product -->
                        </div><!-- /.products -->
                     </div><!-- /.item -->
                  <?php endforeach; ?>
               </div><!-- /.home-owl-carousel -->
            </div><!-- /.product-slider -->
         </div><!-- /.tab-pane -->
      <?php endif; ?>
      <!---********************** FEATURED PRODUCTED ********************************   -->

      <!---********************** NEW ARRIVAL  PRODUCTED ********************************   -->
      <?php if (!empty($newArrivalModel)) :   //newArrivalModel  ?>
         <div class="tab-pane" id="laptop">
            <div class="product-slider">
               <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                  <?php foreach ($newArrivalModel as $fImageKey => $newArrivalData) : ?>
                     <div class="item item-carousel">
                        <div class="products">
                           <div class="product">
                              <div class="product-image">
                                 <div class="image">
                                    <?php if (!empty($newArrivalData->itemImages)) : ?>
                                       <?php echo CHtml::link('<img alt="" src="' . $newArrivalData->itemImages[0]->image . '" data-echo="' . $newArrivalData->itemImages[0]->image . '" style="width:194px;height:243px;" />', array('eshop/proDetails', 'id' => $newArrivalData->id), array('title' => $newArrivalData->itemName)); ?>
                                    <?php endif; ?>
                                 </div><!-- /.image -->

                                 <?php
                                 echo (Items::getIsNewItem($newArrivalData->id)) ? '<div class="tag new" style="position: absolute;"><span>new!</span></div>' : '';
                                 echo (Items::getIsSaleItem($newArrivalData->id)) ? '<div class="tag sale" style="position: absolute;top:20px;"><span>sale</span></div>' : '';
                                 //echo (Items::getIsBestSaleItem($newArrivalData->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                                 ?>

                              </div><!-- /.product-image -->
                              <div class="product-info text-left">
                                 <h3 class="name">
                                    <?php if (!empty($newArrivalData->itemName)) : ?>
                                       <?php echo CHtml::link($newArrivalData->itemName, array('eshop/proDetails', 'id' => $newArrivalData->id)); ?>
                                    <?php endif; ?>
                                 </h3>
                                 <div class="description"></div>
                                 <div class="product-price">
                                    <?php if (Items::getItemWiseOfferPrice($newArrivalData->id) > 0) : ?>
                                       <div class="price">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($newArrivalData->id); ?>
                                       </div>
                                       <div class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($newArrivalData->id); ?></div>
                                    <?php else : ?>
                                       <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($newArrivalData->id); ?></span>
                                    <?php endif; ?>
                                 </div><!-- /.product-price -->
                              </div><!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                 <div class="action">
                                    <ul class="list-unstyled">
                                       <li class="add-cart-button btn-group">
                                          <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                             <i class="fa fa-shopping-cart"></i>
                                          </button>
                                          <div class="btn btn-primary add-cart-button">
                                             <?php echo CHtml::link('add to cart', array('eshop/proDetails', 'id' => $newArrivalData->id), array('class' => 'le-button')); ?>
                                          </div>
                                       </li>
                                       <li class="lnk wishlist">
                                          <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $newArrivalData->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                                             <i class="icon fa fa-heart"></i>
                                          </a>
                                       </li>                                     
                                       <li class="lnk">
                                          <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $newArrivalData->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                                       </li>
                                    </ul>
                                 </div><!-- /.action -->
                              </div><!-- /.cart -->
                           </div><!-- /.product -->
                        </div><!-- /.products -->
                     </div><!-- /.item -->
                  <?php endforeach; ?>
               </div><!-- /.home-owl-carousel -->
            </div><!-- /.product-slider -->
         </div><!-- /.tab-pane -->
      <?php endif; ?>
      <!---********************** NEW ARRIVAL  PRODUCTED ********************************   -->
      <!---**********************TOP RATED PRODUCTED ********************************   -->
      <?php if (!empty($topRatedModel)): //topRatedModel ?>
         <div class="tab-pane" id="apple">
            <div class="product-slider">
               <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">
                  <?php foreach ($topRatedModel as $topRateKey => $topRateData) : ?>
                     <div class="item item-carousel">
                        <div class="products">
                           <div class="product">
                              <div class="product-image">
                                 <div class="image">
                                    <?php if (!empty($topRateData->itemImages)) : ?>
                                       <?php echo CHtml::link('<img alt="" src="' . $topRateData->itemImages[0]->image . '" data-echo="' . $topRateData->itemImages[0]->image . '" style="width:194px;height:243px;" />', array('eshop/proDetails', 'id' => $topRateData->id), array('title' => $topRateData->itemName)); ?>
                                    <?php endif; ?>
                                 </div><!-- /.image -->
                                 <?php
                                 echo (Items::getIsNewItem($topRateData->id)) ? '<div class="tag new" style="position: absolute;top:-7px;"><span>new!</span></div>' : '';
                                 echo (Items::getIsSaleItem($topRateData->id)) ? '<div class="tag sale" style="position: absolute;top:20px;"><span>sale</span></div>' : '';
                                 //echo (Items::getIsBestSaleItem($topRateData->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                                 ?>
                              </div><!-- /.product-image -->
                              <div class="product-info text-left">
                                 <h3 class="name">
                                    <?php if (!empty($topRateData->itemName)) : ?>
                                       <?php echo CHtml::link($topRateData->itemName, array('eshop/proDetails', 'id' => $topRateData->id)); ?>
                                    <?php endif; ?>
                                 </h3>
                                 <div class="description"></div>
                                 <div class="product-price">
                                    <?php if (Items::getItemWiseOfferPrice($topRateData->id) > 0) : ?>
                                       <div class="price">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($topRateData->id); ?>
                                       </div>
                                       <div class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($topRateData->id); ?></div>
                                    <?php else : ?>
                                       <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($topRateData->id); ?></span>
                                    <?php endif; ?>
                                 </div><!-- /.product-price -->
                              </div><!-- /.product-info -->
                              <div class="cart clearfix animate-effect">
                                 <div class="action">
                                    <ul class="list-unstyled">
                                       <li class="add-cart-button btn-group">
                                          <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                             <i class="fa fa-shopping-cart"></i>
                                          </button>
                                          <div class="btn btn-primary add-cart-button">
                                             <?php echo CHtml::link('add to cart', array('eshop/proDetails', 'id' => $topRateData->id), array('class' => 'le-button')); ?>
                                          </div>
                                       </li>                                    
                                      <li class="lnk wishlist">
                                          <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $topRateData->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                                             <i class="icon fa fa-heart"></i>
                                          </a>
                                       </li> 
                                       <li class="lnk">
                                          <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $newArrivalData->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                                       </li>
                                    </ul>
                                 </div><!-- /.action -->
                              </div><!-- /.cart -->
                           </div><!-- /.product -->
                        </div><!-- /.products -->
                     </div><!-- /.item -->
                  <?php endforeach; ?>
               </div><!-- /.home-owl-carousel -->
            </div><!-- /.product-slider -->
         </div><!-- /.tab-pane -->
      <?php endif; ?>
   </div><!-- /.tab-content -->
</div>