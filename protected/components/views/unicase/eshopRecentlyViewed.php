<?php if (count($recentlyViewedModel) > 0) : ?>
   <section class="section wow fadeInUp">
      <h3 class="section-title">recently Viewed</h3>
      <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
         <?php foreach ($recentlyViewedModel as $keyRecent => $recentlyViewedData): ?>
            <div class="item item-carousel">
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image">
                           <?php if (!empty($recentlyViewedData->itemImages)) : ?>
                              <?php echo CHtml::link('<img alt="" src="' . $recentlyViewedData->itemImages[0]->image . '" data-echo="' . $recentlyViewedData->itemImages[0]->image . '" style="width:194px;height:243px;" />', array('eshop/proDetails', 'id' => $recentlyViewedData->id), array('title' => $recentlyViewedData->itemName)); ?>
                           <?php endif; ?>
                        </div><!-- /.image -->

                        <?php
                        echo (Items::getIsNewItem($recentlyViewedData->id)) ? '<div class="tag new" style="position: absolute;"><span>new!</span></div>' : '';
                        echo (Items::getIsSaleItem($recentlyViewedData->id)) ? '<div class="tag sale" style="position: absolute;top:20px;"><span>sale</span></div>' : '';
                        //echo (Items::getIsBestSaleItem($recentlyViewedData->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                        ?>
                     </div><!-- /.product-image -->



                     <div class="product-info text-left">
                        <h3 class="name">
                           <?php if (!empty($recentlyViewedData->itemName)) : ?>
                              <?php echo CHtml::link($recentlyViewedData->itemName, array('eshop/proDetails', 'id' => $recentlyViewedData->id)); ?>
                           <?php endif; ?>
                        </h3>

                        <div class="description"></div>
                        <div class="product-price">
                           <?php if (Items::getItemWiseOfferPrice($recentlyViewedData->id) > 0) : ?>
                              <div class="price">
                                 <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recentlyViewedData->id); ?>
                              </div>
                              <div class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($recentlyViewedData->id); ?></div>
                           <?php else : ?>
                              <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recentlyViewedData->id); ?></span>
                           <?php endif; ?>
                        </div><!-- /.product-price -->
                     </div><!-- /.product-info -->
                     <div class="cart clearfix animate-effect">
                        <div class="action">
                           <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                 <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                    <i class="fa fa-shopping-cart"></i>
                                 </button>
                                 <div class="btn btn-primary add-cart-button">
                                    <?php echo CHtml::link('add to cart', array('eshop/proDetails', 'id' => $recentlyViewedData->id), array('class' => 'le-button')); ?>
                                 </div>
                              </li>
                              <li class="lnk wishlist">

                                 <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $recentlyViewedData->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                                    <i class="icon fa fa-heart"></i>
                                 </a>

                              </li>
                              <li class="lnk">
                                 <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $recentlyViewedData->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                              </li>
                           </ul>
                        </div>
                     </div><!-- /.cart -->
                  </div><!-- /.product -->
               </div><!-- /.products -->
            </div><!-- /.item -->
         <?php endforeach; ?>
      </div><!-- /.home-owl-carousel -->
   </section><!-- /.section -->
<?php endif; ?>