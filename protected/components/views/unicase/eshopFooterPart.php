<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<footer id="footer" class="footer color-bg">
   <div class="links-social inner-top-sm">
      <div class="container">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="contact-info">
                  <div class="footer-logo">
                     <div class="logo">
                        <?php if (!empty($companyModel)) : ?>
                           <img src="<?php echo Yii::app()->baseUrl . $companyModel->logo; ?>" style="height:88px;" />
                        <?php endif; ?>
                     </div><!-- /.logo -->
                  </div><!-- /.footer-logo -->
                  <div class="module-body m-t-20">
                     <div class="social">
                        <?php
                        foreach ($socialModel as $socialKey => $socialData) :
                           echo '<a style="padding-right:2px;" href="' . $socialData->url . '" title="' . $socialData->name . '" target="_blank"><img alt="' . $socialData->name . '" src="' . Yii::app()->baseUrl . $socialData->icon . '" style="height:40px;"></a>';
                        endforeach;
                        ?>
                     </div>
                  </div><!-- /.module-body -->
               </div><!-- /.contact-info -->
               <div class="sidebar-widget newsletter wow fadeInUp">
                  <h3 class="section-title">Newsletters</h3>
                  <div class="sidebar-widget-body outer-top-xs form">
                     <p>Sign Up for Our Newsletter!</p>

                     <?php
                     $form = $this->beginWidget('CActiveForm', array(
                         'action' => Yii::app()->createUrl('/eshop/newsLetter'),
                         'id' => 'newsletter-form',
                         'enableAjaxValidation' => true,
                         'htmlOptions' => array(
                             'class' => 'login-form cf-style-1',
                             'role' => 'form'
                         )
                     ));
                     ?>
                     <div class="form-group">
                        <?php
                        echo $form->textField($nlModel, 'email', array('class' => 'form-control', 'placeholder' => 'Subscribe to our newsletter'));
                        echo $form->error($nlModel, 'email');
                        ?>
                     </div>
                     <button class="le-button btn btn-danger btn-sm" type="submit">Subscribe</button>
                     <?php $this->endWidget(); ?>

                  </div><!-- /.sidebar-widget-body -->
               </div><!-- /.sidebar-widget -->
            </div><!-- /.col -->
            <div class="col-xs-12 col-sm-12 col-md-6">
               <div class="contact-timing">
                  <div class="module-heading">
                     <h4 class="module-title">Quick shop</h4>
                  </div>
                  <div class="module-body outer-top-xs">
                     <div class="table-responsive">
                        <table class="table">
                           <?php if (!empty($categoryModel)): ?>
                              <tbody>
                                 <?php
                                 $i = 1;
                                 foreach ($categoryModel as $category):
                                    if ($i % 2 == 0)
                                       $flo = 'right;';
                                    else
                                       $flo = 'left;';
                                    ?>
                                    <tr style="width:48%; float:<?php echo $flo; ?>">
                                       <td class="pull-left"><?php echo CHtml::link($category->name, array('eshop/proByCat', 'catId' => $category->id)); ?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                 endforeach;
                                 ?>
                              </tbody>
                           <?php endif; ?>
                        </table>
                     </div><!-- /.table-responsive -->
                  </div><!-- /.module-body -->
               </div><!-- /.contact-timing -->
            </div><!-- /.col -->
            <div class="col-xs-12 col-sm-6 col-md-3">
               <div class="contact-information">
                  <div class="module-heading">
                     <h4 class="module-title">information</h4>
                  </div><!-- /.module-heading -->
                  <div class="module-body outer-top-xs">
                     <ul class="toggle-footer">
                        <li><?php echo CHtml::link('My Cart', array('eshop/viewMyCart')); ?></li>                        
                        <?php
                        if (!empty($contentTypeModel)):
                           foreach ($contentTypeModel as $mKey => $menuValue):
                              ?>
                              <li> <?php echo CHtml::link($menuValue->name, array('eshop/contentsDetails', 'id' => $menuValue->id)); ?></li>
                              <?php
                           endforeach;
                        endif;
                        ?>
                        <?php if (!empty(Yii::app()->session['custId'])) : ?>
                           <li><?php echo CHtml::link('My Account', array('eshop/customerDashboard')); ?></li>
                           <li><?php echo CHtml::link('<span style="color:#f8484a;">Signout (' . Yii::app()->user->name . ')</span> ', array('eshop/logout')); ?></li>
                        <?php else : ?>
                           <li><?php echo CHtml::link('Signup', array('eshop/customerSignup')); ?></li>
                           <li><?php echo CHtml::link('Signin', array('eshop/customerSignin')); ?></li>
                        <?php endif; ?>
                     </ul>
                  </div><!-- /.module-body -->
               </div><!-- /.contact-timing -->
            </div><!-- /.col -->
         </div><!-- /.row -->
      </div><!-- /.container -->
   </div><!-- /.links-social -->
   <div class="copyright-bar">
      <div class="container">
         <div class="col-xs-12 col-sm-6 no-padding">
            <div class="copyright">
               &copy; <?php if (!empty($companyModel)) echo date("Y", strtotime($companyModel->crAt)); ?>  <a href="javascript:void(0);"><?php if (!empty($companyModel)) echo $companyModel->name; ?></a> - all rights reserved, Developed By : <a href="http://www.unlockliveretail.com/" target="_blank">UNLOCKLIVE</a>
            </div>
         </div>
         <div class="col-xs-12 col-sm-6 no-padding">
           <?php if(!empty($paymentModel)) : ?>
            <div class="clearfix payment-methods">
               <ul>
                 <?php foreach($paymentModel as $paymentKey=>$paymentData) :
                        	echo '<li><img alt="'.$paymentData->title.'" src="'.$paymentData->image.'"></li>';
                 endforeach; ?>
               </ul>
            </div><!-- /.payment-methods -->
           <?php endif;?>            
        </div>
      </div>
   </div>
</footer>



