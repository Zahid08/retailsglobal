<?php if (!empty($topBrandsModel)) : ?>
   <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <h3 class="section-title">Our Brands</h3>
      <div class="logo-slider-inner">
         <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
            <?php foreach ($topBrandsModel as $topKey => $topBrandData): ?>
               <div class="carousel-item">
                  <a class="image Tupperware" href="javascript:void(0);" title="<?php echo $topBrandData->name; ?>">
                     <?php echo '<img src="' . $topBrandData->image . '" class="Tupperware" alt="' . $topBrandData->name . '" />'; ?>
                  </a>
               </div><!--/.item-->
            <?php endforeach; ?>
         </div>
      </div>
   </div>
<?php endif; ?>
