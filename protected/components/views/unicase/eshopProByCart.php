<a id="show_cart_items" href="javascript:void(0);" class="dropdown-toggle lnk-cart" data-toggle="dropdown">
   <div class="items-cart-inner">
      <div class="total-price-basket">
         <span class="lbl">cart -</span>
         <span class="total-price">
            <span class="sign"><?php echo Company::getCurrency(); ?></span>
            <span class="value"><?php echo $total_price; ?></span>
         </span>
      </div>
      <div class="basket-1">
         <i class="glyphicon glyphicon-shopping-cart"></i>
      </div>
      <div class="basket-item-count"><span class="count"><?php echo $total_qty; ?></span></div>
   </div>
</a>

<?php if ($total_qty > 0) : ?>
   <ul id="show_cart_items_wrapper" class="dropdown-menu">
      <?php foreach ($cart->getItems() as $order_code => $quantity) : ?>
         <li>
            <div class="basket-item cart-item product-summary">
               <div class="row">
                  <div class="col-xs-4">
                     <div class="image">
                        <a href="javascript:void(0);">
                           <img alt="" style="width:60px" src="<?php echo $cart->getItemImages($order_code); ?>"/>
                        </a>
                     </div>
                  </div>
                  <div class="col-xs-7">
                     <h3 class="name"><a href="javascript:void(0);"><?php echo $cart->getItemName($order_code); ?></a></h3>
                     <div class="price">
                        <?php echo 'Quantity : <strong>' . $quantity . '</strong>'; ?>
                     </div>
                     <div class="price">
                        <?php
                        if (Items::getItemWiseOfferPrice($order_code) > 0) :
                           echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($order_code) * $quantity;
                        else : echo Company::getCurrency() . '&nbsp;' . $cart->getItemPrice($order_code) * $quantity;
                        endif;
                        ?>
                     </div>
                  </div>
                  <div class="col-xs-1 action">
                     <a id="<?php echo $order_code; ?>" class="close-btn remove_from_cart" href="javascript:void(0);"><i class="fa fa-trash"></i></a>
                  </div>
               </div>
            </div><!-- /.cart-item -->
            <div class="clearfix"></div>
            <hr>
         </li>
      <?php endforeach; ?>
      <li class="checkout col-sm-12 no-padding">
         <div class="basket-item">
            <div class="row">
               <div class="col-xs-6 col-sm-6 text-center">
                  <?php echo CHtml::link('View cart', array('eshop/viewMyCart'), array('class' => 'btn btn-cart inverse')); ?>
               </div>
               <div class="col-xs-6 col-sm-6">
                  <?php
                  if (!empty(Yii::app()->session['custId']))
                     echo CHtml::link('Checkout', array('checkoutProcess/checkoutShipping'), array('class' => 'btn btn-cart le-button'));
                  else
                     echo CHtml::link('Checkout', array('checkoutProcess/checkout'), array('class' => 'btn btn-cart le-button'));
                  ?>
               </div>
            </div>
         </div>
      </li>
   </ul>
<?php endif; ?>