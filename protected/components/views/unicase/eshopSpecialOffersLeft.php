<?php  if(!empty($offerModel)) : ?> 
    <h3 class="section-title">Special Offer</h3>
    <div class="featured-item">
	<ul class="product-list">
	<?php foreach($offerModel as $offerKey=>$offerData) :?>
		<li class="sidebar-product-list-item">
			<div class="row">
				<div class="col-xs-4 col-sm-4 no-margin">
				<?php if(!empty($offerData->itemImages)) : ?>
                    <?php echo CHtml::link('<img alt="" src="'.$offerData->itemImages[0]->image.'" data-echo="'.$offerData->itemImages[0]->image.'" style="width:73px;height:73px;" />',array('eshop/proDetails','id'=>$offerData->id),  array('class'=>'thumb-holder','title'=>$offerData->itemName));?>
                <?php endif;?>
				</div>
				<div class="col-xs-8 col-sm-8 no-margin">
					<?php echo CHtml::link($offerData->itemName,array('eshop/proDetails','id'=>$offerData->id));?>
					<div class="brand">
						<?php echo $offerData->brand->name;?>
					</div>
                    <div class="clearfix"></div>
					<div class="price" style=' width: 150px;' >
						<?php if(Items::getItemWiseOfferPrice($offerData->id)>0) :?>
							<div class="price-prev" style="text-decoration: line-through; width:50%; float:left;">
								<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($offerData->id);?>
							</div>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($offerData->id);?></div>
						<?php else : ?>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($offerData->id);?></div>
						<?php endif;?>
					</div>
                    <div class="clearfix"></div>
				</div>
			</div>
		</li><!-- /.sidebar-product-list-item -->
    <?php endforeach;?>
	</ul><!-- /.product-list -->
    </div>

<?php endif;  ?>


