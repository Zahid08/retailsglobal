<?php if ($position == Advertisement::POSITION_TOP && !empty($advertisementModel)) { ?>
   <div class="shipping text-center top-advertisement">
      <div class="row">
         <?php foreach ($advertisementModel as $key => $advertisementData): ?>
            <div class="col-sm-4">
            <?php if(!empty($advertisementData->url)) : ?>
            <a href="<?php echo $advertisementData->url;?>" target="_blank" title="<?php echo $advertisementData->title;?>">
               <?php echo CHtml::image($advertisementData->image, $advertisementData->title, array('style' => 'margin-top: 30px; border: 3px solid #ccc;')); ?>
             </a>
             <?php else : ?>
                <?php echo CHtml::image($advertisementData->image, $advertisementData->title, array('style' => 'margin-top: 30px; border: 3px solid #ccc;')); ?>
            <?php endif;?>
            </div>
         <?php endforeach; ?>
      </div>

   </div>
<?php } ?>

<?php if ($position == Advertisement::POSITION_LEFT && !empty($advertisementModel)) : ?>
   <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
      <div id="advertisement" class="advertisement">
         <?php foreach ($advertisementModel as $key => $advertisementData): ?>
         <?php endforeach; ?>
      </div>
   </div>
<?php endif; ?>