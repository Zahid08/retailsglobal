<?php if (!empty($recommandedModel)): ?>
   <section class="section featured-product wow fadeInUp">
      <h3 class="section-title">recommended items</h3>
      <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
         <?php foreach ($recommandedModel as $key => $recommandedModelValues) : ?>
            <div class="item item-carousel">
               <div class="products">
                  <div class="product">
                     <div class="product-image">
                        <div class="image">
                           <?php if (!empty($recommandedModelValues->itemImages)) : ?>
                              <?php echo CHtml::link('<img alt="" src="' . $recommandedModelValues->itemImages[0]->image . '" data-echo="' . $recommandedModelValues->itemImages[0]->image . '" style="width:173px;" />', array('eshop/proDetails', 'id' => $recommandedModelValues->id), array('class' => 'thumb-holder', 'title' => $recommandedModelValues->itemName)); ?>
                           <?php endif; ?>
                        </div><!-- /.image -->
                        <?php
                        echo (Items::getIsNewItem($recommandedModelValues->id)) ? '<div class="tag new"><span>new!</span></div>' : '';
                        echo (Items::getIsSaleItem($recommandedModelValues->id)) ? '<div class="tag sale"><span>sale</span></div>' : '';
                        //echo (Items::getIsBestSaleItem($recommandedModelValues->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                        ?>
                     </div><!-- /.product-image -->
                     <div class="product-info text-left">
                        <h3 class="name">
                           <?php if (!empty($recommandedModelValues->itemName)) : ?>
                              <?php echo CHtml::link($recommandedModelValues->itemName, array('eshop/proDetails', 'id' => $recommandedModelValues->id)); ?>
                           <?php endif; ?>
                        </h3>
                        <div class="description"></div>
                        <div class="product-price">
                           <?php if (Items::getItemWiseOfferPrice($recommandedModelValues->id) > 0) : ?>
                              <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recommandedModelValues->id); ?></span>
                              <span class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($recommandedModelValues->id); ?></span>
                           <?php else : ?>
                              <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recommandedModelValues->id); ?></span>
                           <?php endif; ?>

                        </div><!-- /.product-price -->
                     </div><!-- /.product-info -->
                     <div class="cart clearfix animate-effect">
                        <div class="action">
                           <ul class="list-unstyled">
                              <li class="add-cart-button btn-group">
                                 <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                    <i class="fa fa-shopping-cart"></i>
                                 </button>
                                 <?php echo CHtml::link('Add to cart', array('eshop/proDetails', 'id' => $recommandedModelValues->id), array('class' => 'btn btn-primary')); ?>
                              </li>
                              <li class="lnk wishlist wish-compare">
                                 <a href="javascript:void(0);" id="<?php echo $recommandedModelValues->id; ?>" class="btn-add-to-wishlist"> <i class="icon fa fa-heart"></i></a>

                              </li>
                              <li class="lnk">
                                 <?php echo CHtml::link(' <i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $recommandedModelValues->id), array('class' => 'add-to-cart', 'title' => "Review & Rate")); ?>
                              </li>
                           </ul>
                        </div><!-- /.action -->
                     </div><!-- /.cart -->
                  </div><!-- /.product -->
               </div><!-- /.products -->
            </div><!-- /.item -->
         <?php endforeach ?>
      </div><!-- /.home-owl-carousel -->
   </section>
<?php endif; ?>