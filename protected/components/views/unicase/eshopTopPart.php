<?php if (isset(Yii::app()->session['newsletterMsg']) && !empty(Yii::app()->session['newsletterMsg'])) : ?>
   <div class="notification_msg">
      <?php echo Yii::app()->session['newsletterMsg'];
      unset(Yii::app()->session['newsletterMsg']);
      ?>
   </div>
<?php endif; ?>
<!-- ============================================== TOP MENU ============================================== -->
<div class="top-bar animate-dropdown">
   <div class="container">
      <div class="header-top-inner">
         <div class="cnt-account">
            <ul class="list-unstyled">
               <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
               <?php
               if (!empty($contentTypeModel)) :
                  foreach ($contentTypeModel as $mKey => $menuValue):
                     ?>
                     <li> <?php echo CHtml::link($menuValue->name, array('eshop/contentsDetails', 'id' => $menuValue->id)); ?></li>
                  <?php
                  endforeach;
               endif;
               ?>
            </ul>
         </div><!-- /.cnt-account -->

         <div class="cnt-block">
            <ul class="list-unstyled list-inline">
               <li><?php echo CHtml::link('My Cart', array('eshop/viewMyCart')); ?></li>
<?php if (!empty(Yii::app()->session['custId']) && Yii::app()->user->name != 'Guest') : ?>
                  <li><?php echo CHtml::link('My Account', array('eshop/customerDashboard')); ?></li>
                  <li><a href="<?php echo Yii::app()->createAbsoluteUrl('eshop/customerWishlist'); ?>" title="Wishlist">Wishlist<span class="value" id="wishListCount"><?php echo (!empty(Yii::app()->session['custId']))?Wishlist::countWishListByCustomer(Yii::app()->session['custId']):'(0)'; ?></span> </a></li>
                  <li><?php echo CHtml::link('<span style="color:#f8484a;">Signout (' . Yii::app()->user->name . ')</span> ', array('eshop/logout')); ?></li>
               <?php else : ?>
                  <li><?php echo CHtml::link('Signup', array('eshop/customerSignup')); ?></li>
                  <li><?php echo CHtml::link('Signin', array('eshop/customerSignin')); ?></li>
<?php endif; ?>
               <li> <?php echo Chtml::link('Contact Us', array('eshop/contractUs')) ?></li>
            </ul>
         </div><!-- /.cnt-cart -->
         <div class="clearfix"></div>
      </div><!-- /.header-top-inner -->
   </div><!-- /.container -->
</div><!-- /.header-top -->
<!-- ============================================== TOP MENU : END ============================================== -->
<div class="main-header">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
            <!-- ============================================================= LOGO ============================================================= -->
            <div class="logo">
<?php if (!empty($companyModel)) echo CHtml::link('<img src="' . Yii::app()->baseUrl . $companyModel->logo . '" style="height:88px;" />', array('eshop/index'), array('title' => $companyModel->name)); ?>
            </div><!-- /.logo -->
            <!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->

         <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
            <div class="contact-row">
               <div class="phone inline">
                  <i class="icon fa fa-phone"></i> <?php if (!empty($branchModel)) echo $branchModel->phone; ?>
               </div>
               <div class="contact inline">
                  <i class="icon fa fa-envelope"></i>  <?php if (!empty($companyModel)) echo $companyModel->email; ?>
               </div>
            </div><!-- /.contact-row -->
            <!-- ============================================================= SEARCH AREA ============================================================= -->
            <div class="search-area">
               <form>
                  <div class="control-group">
                     <?php
                     $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                         'name' => 'searchItems',
                         'source' => Yii::app()->createUrl('eshop/autoSuggestItemSearch'),
                         'options' => array(
                             'minLength' => 1, // console.log(ui.item.id +":"+ui.item.value);
                             'showAnim' => 'fold',
                             'select' => 'js:function(event, ui) {
							window.location.href = "' . Yii::app()->baseUrl . '/eshop/proDetails?id="+ui.item.id;
						}'
                         ),
                         'htmlOptions' => array(
                             'id' => 'items',
                             'rel' => 'val',
                             'placeholder' => 'Search here',
                             'class' => "search-field",
                         ),
                     ));
                     ?>
                     <a class="search-button" href="#" ></a>
                  </div>
               </form>
            </div><!-- /.search-area -->
            <!-- ============================================================= SEARCH AREA : END ============================================================= -->				</div><!-- /.top-search-holder -->

         <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
            <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
            <div class="dropdown dropdown-cart">
               <div class="top-cart-holder dropdown animate-dropdown">
                  <div class="basket">
                    <?php $this->widget('EshopProByCart'); ?>
                  </div><!-- /.basket -->
               </div><!-- /.top-cart-holder -->
            </div><!-- /.dropdown-cart -->

            <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->
         </div><!-- /.top-cart-row -->
      </div><!-- /.row -->
   </div><!-- /.container -->
</div><!-- /.main-header -->