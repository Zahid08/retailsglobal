<div class="sidebar-widget wow fadeInUp outer-bottom-vs">
   <h3 class="section-title">Best seller</h3>
   <div class="sidebar-widget-body outer-top-xs">
      <div class="owl-carousel best-seller custom-carousel owl-theme outer-top-xs">
         <?php foreach ($bestSellers as $bestSellKey => $bestSellData): ?>
            <div class="item">
               <div class="products best-product">
                  <div class="product">
                     <div class="product-micro">
                        <div class="row product-micro-row">
                           <div class="col col-xs-5">
                              <div class="product-image">
                                 <div class="image">
                                    <?php if (!empty($bestSellData->itemImages)) : ?>
                                       <?php echo CHtml::link('<img alt="" src="' . $bestSellData->itemImages[0]->image . '" data-echo="' . $bestSellData->itemImages[0]->image . '" style="width:100px;height:130px;" />', array('eshop/proDetails', 'id' => $bestSellData->id), array('title' => $bestSellData->itemName)); ?>
                                    <?php endif; ?>
                                 </div><!-- /.image -->
                                 <?php
                                 echo (Items::getIsNewItem($bestSellData->id)) ? '<div class="tag new" style="position: absolute;"><span>new!</span></div>' : '';
                                 echo (Items::getIsSaleItem($bestSellData->id)) ? '<div class="tag sale" style="position: absolute;"><span>sale</span></div>' : '';
                                 //echo (Items::getIsBestSaleItem($bestSellData->id)) ? '<div class="tag hot" style="position: absolute;"><span>bestseller</span></div>' : '';
                                 ?>
                              </div><!-- /.product-image -->
                           </div><!-- /.col -->
                           <div class="col col-xs-7">
                              <div class="product-info">
                                 <h3 class="name">
                                    <?php if (!empty($bestSellData->itemName)) : ?>
                                       <?php echo CHtml::link($bestSellData->itemName, array('eshop/proDetails', 'id' => $bestSellData->id)); ?>
                                    <?php endif; ?>
                                 </h3>
                                 <div class="rating rateit-small"></div>
                                 <div class="product-price">
                                    <?php if (Items::getItemWiseOfferPrice($bestSellData->id) > 0) : ?>
                                       <div class="price">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($bestSellData->id); ?>
                                       </div>
                                       <div class="price-before-discount"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($bestSellData->id); ?></div>
                                    <?php else : ?>
                                       <span class="price"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($bestSellData->id); ?></span>
                                    <?php endif; ?>
                                 </div><!-- /.product-price -->
                                 <div class="cart clearfix animate-effect">
                                    <div class="action">
                                       <ul class="list-unstyled">
                                          <li class="add-cart-button btn-group">
                                             <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                <i class="fa fa-shopping-cart"></i>
                                             </button>
                                             <div class="btn btn-primary add-cart-button">
                                                <?php echo CHtml::link('add to cart', array('eshop/proDetails', 'id' => $bestSellData->id), array('class' => 'le-button')); ?>
                                             </div>
                                          </li>
                                          <li class="lnk wishlist">
                                             <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $bestSellData->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                                                <i class="icon fa fa-heart"></i>
                                             </a>
                                          </li>
                                          <li class="lnk">
                                             <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $bestSellData->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                                          </li>
                                       </ul>
                                    </div><!-- /.action -->
                                 </div><!-- /.cart -->
                              </div>
                           </div><!-- /.col -->
                        </div><!-- /.product-micro-row -->
                     </div><!-- /.product-micro -->
                  </div>
               </div>
            </div>
         <?php endforeach; ?>
      </div>
   </div><!-- /.sidebar-widget-body -->
</div><!-- /.sidebar-widget -->
