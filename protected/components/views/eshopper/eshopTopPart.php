<?php if(isset(Yii::app()->session['newsletterMsg']) && !empty(Yii::app()->session['newsletterMsg'])) : ?>
    <div class="notification_msg">
        <?php echo Yii::app()->session['newsletterMsg'];unset(Yii::app()->session['newsletterMsg']);?>
    </div>
<?php endif;

$socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));
?>
<!--<div class="container">header_top
    <div class="row header_top">
        <div class="col-sm-3">

        </div>
        <div class="col-sm-9">

        </div>
    </div>
</div>/header_top-->

<div class="container"><!--header-middle-->
    <div class="row header-middle">
        <div class="header-content">
<!--        <div class="col-sm-3">-->
<!--            <div class="logo pull-left">-->
<!--                --><?php //if(!empty($companyModel)) echo CHtml::link('<img src="'.Yii::app()->baseUrl.$companyModel->logo.'" style="height:88px;" />',array('eshop/index'),array('title'=>$companyModel->name));?>
<!--            </div>-->
<!--            <div class="btn-group pull-right">-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="col-sm-5">-->
<!--            <div class="search_box">-->
<!--                <form method="post" action="--><?php //echo Yii::app()->getBaseUrl(true).'/index.php/eshop/proByCatSearch/'?><!--">-->
<!--                    <div class="input-group-lg">-->
<!--                        <!-- <input class="search-field" placeholder="Search for item" />  -->
<!--                        --><?php
//                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
//                            'name'=>'searchItems',
//                            //'placeholder'=>'Search',
//                            'source'=>Yii::app()->createUrl('eshop/autoSuggestItemSearch'),
//                            'options'=>array(
//                                'minLength'=>1, // console.log(ui.item.id +":"+ui.item.value);
//                                'showAnim'=>'fold',
//                                'select'=>'js:function(event, ui) {
//                                    window.location.href = "'.Yii::app()->baseUrl.'/eshop/proDetails?id="+ui.item.id;
//                                }'
//                            ),
//                            'htmlOptions'=>array(
//                                'id'=>'items',
//                                'class'=>'form-control',
//                                'rel'=>'val',
//                                'placeholder'=>'Search',
//                            ),
//                        ));?>
<!--                        <!--<span class="input-group-btn">-->
<!--                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>-->
<!--                        </span>
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
        <div class="col-sm-12">
            <div class="shop-menu pull-right">
                <ul class="nav navbar-nav">
                    <li>
                        <div class="wishlist-compare-holder">
                            <div class="wishlist" style="margin-top:12px;">
                                <a href="<?php echo Yii::app()->createAbsoluteUrl('eshop/customerWishlist');?>" title="Wishlist"><i class="fa fa-star"></i> Wishlist <span class="value" id="wishListCount"><?php echo (!empty(Yii::app()->session['custId']))?Wishlist::countWishListByCustomer(Yii::app()->session['custId']) :'(0)';?></span> </a>
                            </div>
                        </div>
                    </li>
                    </li>
                    <?php if(!empty(Yii::app()->session['custId']) && Yii::app()->user->name!='Guest') :?>
                        <li><?php echo CHtml::link('<i class="fa fa-user"></i>My Account',array('eshop/customerDashboard')); ?></li>
                        <li><?php echo CHtml::link('<span style="color:#fe980f;">Signout ('.Yii::app()->user->name.')</span> ',array('eshop/logout')); ?></li>
                    <?php else : ?>
                        <li><?php echo CHtml::link('<i class="fa fa-user"></i>Signup',array('eshop/customerSignup')); ?></li>
                        <li><?php echo CHtml::link('<i class="fa fa-lock"></i>Signin',array('eshop/customerSignin')); ?></li>
                    <?php endif;?>
                    <li> <?php echo Chtml::link('Contact Us',array('eshop/contractUs'))?></li>

                    <?php if(!empty($socialModel)):?>
                        <?php
                        foreach ($socialModel as $key => $socialData):
                            $socialNet = SocialNetwork::getDomainName($socialData->url); ?>
                            <?php if($socialNet=='facebook'){ ?>
                            <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-facebook"></i></a></li>
                        <?php } else if($socialNet=='twitter'){ ?>
                            <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-twitter"></i></a></li>
                        <?php } else if($socialNet=='linkedin'){ ?>
                            <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                        <?php } else if($socialNet=='dribbble'){ ?>
                            <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-dribbble"></i></a></li>
                        <?php }?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="contactinfo pull-right">
                <ul class="list-unstyled">
                    <li><a href="#"><i class="fa fa-phone"></i> </i> <?php if(!empty($branchModel)) echo $branchModel->phone;?></a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i> <?php if(!empty($companyModel)) echo $companyModel->email;?></a></li>
                </ul>
            </div>
        </div>
            <div class="col-sm-4">
                <div class="cart-nav pull-right">
                    <a href="javascript:void(0);" class="dropdown-toggle" >
                        <div class="basket" id="show_cart_items">
                            <?php $this->widget('EshopProByCart'); ?>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="slogan">
                    <h3>Journey with new Colours</h3>
                </div>
            </div>
        </div>
    </div>
</div><!--/header-middle-->     
<script type="text/javascript">
    $('#show_cart_items').click(function(e){
        e.preventDefault();
        $('#show_cart_items_wrapper').toggle('slow');
    });    
</script>   