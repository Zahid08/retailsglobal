<div class="container">
    <div class="row header-bottom"><!--header-bottom-->
        <div class="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="mainmenu pull-left">
                <ul class="nav navbar-nav collapse navbar-collapse">
				<li>
                        <?php echo CHtml::link('HOME',array('eshop/index'),array('title'=>'Home'));?>
				</li>
                <?php
                    $ptrCount = 0;
                    foreach(Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey=>$dept) :
                     if(Category::getAllCatCountByDept($dept->id)>0) : $ptrCount++;
                        if($ptrCount>10) break; ?>
                        <li class="dropdown">
                            <?php echo CHtml::link($dept->name.'<i class=\'fa fa-angle-down\'></i>',array('eshop/index')); ?>
                            <?php if(!empty(Category::getAllCatByDept($dept->id))) : ?>
                                <ul role="menu" class="sub-menu">
                                    <?php  foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) :  ?>
                                        <li>
                                            <?php echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));?>
                                        </li>
                                    <?php  endforeach;?>
                                </ul>
                            <?php endif;?>
                        </li>
                    <?php endif;
                    endforeach;?>
                    <li>
                        <?php echo CHtml::link('All CATEGORIES',array('eshop/allDeptSubDeptCat'),array('title'=>'All Categories'));?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div><!--/header-bottom-->