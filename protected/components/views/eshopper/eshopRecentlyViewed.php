<?php 
if(count($recentlyViewedModel)>0) :  ?>
    <div class="recommended_items">
        <h2 class="title text-center">Recently Viewed</h2>                        
        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">            
                <div class="item active">
                    <?php foreach ($recentlyViewedModel as $keyRecent=>$recentlyViewedData):?>
                        <div class="col-sm-3">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <?php if(!empty($recentlyViewedData->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$recentlyViewedData->itemImages[0]->image.'" data-echo="'.$recentlyViewedData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$recentlyViewedData->id),  array('title'=>$recentlyViewedData->itemName));?>
                                        <?php endif;?>                                      
                                        <div class="price clearfix">
                                                <?php if(Items::getItemWiseOfferPrice($recentlyViewedData->id)>0) :?>
                                                <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recentlyViewedData->id);?></h2>
                                                </div>
                                                <div class="price-current">
                                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($recentlyViewedData->id);?></h2>
                                                </div>                                   
                                                 <?php else : ?>                                    
                                                    <div class="price-current">
                                                        <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recentlyViewedData->id);?></h2>
                                                    </div>
                                                <?php endif;?>
                                        </div>

                                        <p style="height:45px;"><?php echo $recentlyViewedData->itemName;?></p>
                                        <p> <?php echo $recentlyViewedData->brand->name;?></p>
                                        <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$recentlyViewedData->id), array('class'=>'btn btn-default add-to-cart')); ?>
                                    </div> 
                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">                            
                                           <li>
                                                 <a style="font-size: 11px;" href="javascript:void(0);" id="<?php echo $recentlyViewedData->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                                            </li>
                                            <li>
                                                <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$recentlyViewedData->id),  array('class'=>'btn-add-to-compare','style'=>"font-size: 11px;"));?>
                                            </li>
                                           
                                        </ul>
                                    </div>                           
                                </div>
                            </div>
                        </div> 
                    <?php endforeach; ?>
                </div>                    
            </div>                
        </div>
    </div>
<?php endif;?>