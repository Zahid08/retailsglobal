<a id="show_cart_items" class="dropdown-toggle" href="javascript:void(0);">
    <div class="basket-item-count">
        <span>Your Cart:</span>
        <span class="count badge"><?php echo $total_qty;?></span>       
    </div>

	<div class="total-price-basket">
		<span class="total-price">
            <span class="sign"><?php echo Company::getCurrency();?></span> <span class="value badge"><?php echo $total_price;?></span></span>
	</div>
</a>
<?php if($total_qty>0) : ?>
<ul id="show_cart_items_wrapper" class="dropdown-menu">
<?php foreach ($cart->getItems() as $order_code=>$quantity) :?>
    <li class="col-sm-12">
		<div class="basket-item">
			<div class="">
				<div class="col-xs-4 col-sm-4 no-margin text-center">
					<div class="thumb">
						<img alt="" src="<?php echo $cart->getItemImages($order_code);?>" style=" height: 50px;margin-left: 7px; width: auto;" />
					</div>
				</div>
				<div class="col-xs-7 col-sm-7 no-margin item-details">
					<div class="title"><?php echo $cart->getItemName($order_code); ?></div>
					<div class="item-quantity"><?php echo 'Quantity : <strong>'.$quantity.'</strong>'; ?></div>
					<div class="price">
					<?php if(Items::getItemWiseOfferPrice($order_code)>0) :
							 echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($order_code)*$quantity;
						  else : echo Company::getCurrency().'&nbsp;'.$cart->getItemPrice($order_code)*$quantity;
					 endif;?>
					</div>
				</div>
                <a id="<?php echo $order_code;?>" class="close-btn remove_from_cart" href="javascript:void(0);">×</a>
			</div>			
		</div>
	</li>
<?php endforeach;?>
    <li class="checkout col-sm-12">
		<div class="basket-item">
			<div class="row">
				<div class="col-xs-6 col-sm-6 text-center">
                    <?php echo CHtml::link('View cart',array('eshop/viewMyCart'),array('class'=>'btn btn-cart inverse')); ?>
				</div>
				<div class="col-xs-6 col-sm-6">
					<?php 
						if(!empty(Yii::app()->session['custId']))
                            echo CHtml::link('Checkout',array('checkoutProcess/checkoutShipping'),array('class'=>'btn btn-cart le-button')); 
						else echo CHtml::link('Checkout',array('checkoutProcess/checkout'),array('class'=>'btn btn-cart le-button')); 
					?>
				</div>
			</div>
		</div>
	</li>
</ul>
<?php endif;?>