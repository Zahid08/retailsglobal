<?php if(!empty($featuredModel)) : ?>
<div class="widget left-sidebar">
	<h2>Featured Item</h2>
    <div class="featured-item">
	<ul class="product-list">
	<?php foreach($featuredModel as $feturedKey=>$feturedData) :?>
		<li class="sidebar-product-list-item">
			<div class="row">
				<div class="col-xs-4 col-sm-4 no-margin">
				<?php if(!empty($feturedData->itemImages)) : ?>
                    <?php echo CHtml::link('<img alt="" src="'.$feturedData->itemImages[0]->image.'" data-echo="'.$feturedData->itemImages[0]->image.'" style="width:73px;" />',array('eshop/proDetails','id'=>$feturedData->id),  array('class'=>'thumb-holder','title'=>$feturedData->itemName));?>
                <?php endif;?>
				</div>
				<div class="col-xs-8 col-sm-8 no-margin">
					<p style="color:#696763;"><?php echo CHtml::link($feturedData->itemName,array('eshop/proDetails','id'=>$feturedData->id));?></p>
					<div class="brand">
						<?php echo $feturedData->brand->name;?>
					</div>
					<div class="price">
						<?php if(Items::getItemWiseOfferPrice($feturedData->id)>0) :?>
                        <div class="price-prev pull-left" style="text-decoration: line-through;">
								<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($feturedData->id);?>
							</div>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($feturedData->id);?></div>
						<?php else : ?>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($feturedData->id);?></div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</li><!-- /.sidebar-product-list-item -->
    <?php endforeach;?>
	</ul><!-- /.product-list -->
    </div>
</div><!-- /.widget -->
<?php endif;?>