<!-- ================================== TOP NAVIGATION ================================== -->
<h2>Category</h2>
<div class="panel-group category-products" id="accordian"><!--category-productsr-->
    <div class="panel panel-default">
    <?php foreach(Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey=>$dept) : ?>
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordian" href="#sportswear_<?php echo $dkey ;?>">
                    <?php if(!empty(Category::getAllCatByDept($dept->id))) : ?><span class="badge pull-right"><i class="fa fa-plus"></i></span><?php endif;?>
                    <?php echo  $dept->name;?>
                </a>               
            </h4>
        </div>
        <?php if(!empty(Category::getAllCatByDept($dept->id))) : ?>
        <div id="sportswear_<?php echo $dkey ;?>" class="panel-collapse collapse">
            <div class="panel-body">
                <ul>
                    <li>
                        <?php
                            foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) :
                                echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));
                            endforeach;
                        ?>
                    </li>                                  
                </ul>
            </div>
        </div>
        <?php endif;?>
    <?php endforeach; ?>
    <div class="panel-heading">
            <h4 class="panel-title">                
                <?php echo CHtml::link('All Categories',array('eshop/allDeptSubDeptCat'),array('title'=>'All Categories'));?>             
            </h4>
        </div>
    </div> 
</div><!--/category-products-->
