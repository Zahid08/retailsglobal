<?php if(!empty($featuredModel)):?>
<div class="features_items"><!--features_items-->
    <h2 class="title text-center">Features Items</h2>
     <?php foreach ($featuredModel as $featuredModelkey => $featuredModelData):?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <?php if(!empty($featuredModelData->itemImages)) : ?>
                        <?php echo CHtml::link('<img alt="" src="'.$featuredModelData->itemImages[0]->image.'" data-echo="'.$featuredModelData->itemImages[0]->image.'" style="width:268px; height:249px;" />',array('eshop/proDetails','id'=>$featuredModelData->id),  array('title'=>$featuredModelData->itemName));?>
                    <?php endif;?>
                        
                    <?php if(Items::getItemWiseOfferPrice($featuredModelData->id)>0) :?>
                        <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                            <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($featuredModelData->id);?></h2>
                        </div>
                        <div class="price-current">
                            <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($featuredModelData->id);?></h2>
                        </div>                                   
                     <?php else : ?>                                    
                        <div class="price-current">
                            <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($featuredModelData->id);?></h2>
                        </div>
                    <?php endif;?>
                    <p style="min-height:38px;"><?php echo $featuredModelData->itemName;?></p>
                    <p><?php echo $featuredModelData->brand->name;?></p>
                    <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$featuredModelData->id), array('class'=>'btn btn-default add-to-cart')); ?>
                </div>                               
            </div>
            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li>
                         <a href="javascript:void(0);" id="<?php echo $featuredModelData->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                    </li>
                    <li>
                        <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$featuredModelData->id),  array('class'=>'btn-add-to-compare'));?>
                    </li>
                </ul>
            </div>
        </div>
    </div> 
    <?php endforeach; ?> 
         
</div><!--features_items-->
<?php endif; ?>          
            
   

<div class="category-tab"><!--category-tab-->
    <div class="col-sm-12">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#tshirt">new arrivals</a></li>
            <li><a data-toggle="tab" href="#blazers">top rated</a></li>								
        </ul>
    </div>
    <div class="tab-content">
        <?php if(!empty($newArrivalModel)) :?>
        <div id="tshirt" class="tab-pane fade active in">
              <?php foreach($newArrivalModel as $newProduct=>$newArrivalData) :?> 
            <div class="col-sm-3">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">                           
                            <?php if(!empty($newArrivalData->itemImages)) : ?>
                                <?php echo CHtml::link('<img alt="" src="'.$newArrivalData->itemImages[0]->image.'" data-echo="'.$newArrivalData->itemImages[0]->image.'" style="width:207px; height:183px;" />',array('eshop/proDetails','id'=>$newArrivalData->id),  array('title'=>$newArrivalData->itemName));?>
                            <?php endif;?>
                             <?php if(Items::getItemWiseOfferPrice($newArrivalData->id)>0) :?>
                                <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($newArrivalData->id);?></h2>
                                </div>
                                <div class="price-current">
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($newArrivalData->id);?></h2>
                                </div>                                   
                             <?php else : ?>                                    
                                <div class="price-current">
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($newArrivalData->id);?></h2>
                                </div>
                            <?php endif;?>
                            <p style="height:50px;"><?php echo $newArrivalData->itemName;?></p>
                            <p><?php echo $newArrivalData->brand->name;?></p>
                            <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$newArrivalData->id), array('class'=>'btn btn-default add-to-cart')); ?>
                        </div>
                         <div class="choose">
                            <ul class="nav nav-pills nav-justified">
                                <li>
                                     <a href="javascript:void(0);" id="<?php echo $newArrivalData->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                                </li>
                                <li>
                                    <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$newArrivalData->id),  array('class'=>'btn-add-to-compare'));?>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div> 
             <?php endforeach; ?>
        </div>
        <?php endif; ?>
        <?php if(!empty($topRatedModel)):?>  
        <div id="blazers" class="tab-pane fade">
          <?php foreach($topRatedModel as $topRateKey=>$topRateData) :?> 
            <div class="col-sm-3">
                <div class="product-image-wrapper">
                    <div class="single-products">
                        <div class="productinfo text-center">
                            <?php if(!empty($topRateData->itemImages)) : ?>
                                <?php echo CHtml::link('<img alt="" src="'.$topRateData->itemImages[0]->image.'" data-echo="'.$topRateData->itemImages[0]->image.'" style="width:207px; height:183px;" />',array('eshop/proDetails','id'=>$topRateData->id),  array('title'=>$topRateData->itemName));?>
                            <?php endif;?>
                           
                           <?php if(Items::getItemWiseOfferPrice($topRateData->id)>0) :?>
                                <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($topRateData->id);?></h2>
                                </div>
                                <div class="price-current">
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($topRateData->id);?></h2>
                                </div>                                   
                             <?php else : ?>                                    
                                <div class="price-current">
                                    <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($topRateData->id);?></h2>
                                </div>
                            <?php endif;?>
                            <p style="height:50px;"><?php echo $topRateData->itemName;?></p>
                            <p><?php echo $topRateData->brand->name;?></p>
                            <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$topRateData->id), array('class'=>'btn btn-default add-to-cart')); ?>
                        </div>
                         <div class="choose">
                            <ul class="nav nav-pills nav-justified">
                                <li>
                                     <a href="javascript:void(0);" id="<?php echo $topRateData->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                                </li>
                                <li>
                                    <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$topRateData->id),  array('class'=>'btn-add-to-compare'));?>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>
           <?php endforeach; ?> 
        </div>	
    <?php endif; ?>        
    </div>
</div>


                    
