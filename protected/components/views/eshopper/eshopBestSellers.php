<?php if(count($bestSellers)>0) :  ?>
    <div class="recommended_items">
        <h2 class="title text-center">Best Sells</h2>                        
        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">            
                <div class="item active">
                    <?php foreach ($bestSellers as $keyBestSell=>$bestSellData):?>
                        <div class="col-sm-3">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <?php if(!empty($bestSellData->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$bestSellData->itemImages[0]->image.'" data-echo="'.$bestSellData->itemImages[0]->image.'" style="width:246px;height:186px;" />',array('eshop/proDetails','id'=>$bestSellData->id),  array('title'=>$bestSellData->itemName));?>
                                        <?php endif;?>                                        
                                        <div class="price">
                                            <h2>                                       
                                                <?php if(Items::getItemWiseOfferPrice($bestSellData->id)>0) :?>
                                                <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                                                      <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($bestSellData->id);?>
                                                </div>
                                                <div class="price-current pull-right">
                                                    <?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($bestSellData->id);?>
                                                </div>                                   
                                                 <?php else : ?>                                    
                                                    <div class="price-current">
                                                          <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($bestSellData->id);?>
                                                    </div>
                                                <?php endif;?>
                                            </h2>
                                        </div>
                                        <p style="height:45px;"><?php echo $bestSellData->itemName;?></p>
                                        <p><?php echo $bestSellData->brand->name;?></p>
                                        <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$bestSellData->id), array('class'=>'btn btn-default add-to-cart')); ?>
                                     </div> 
                                      <div class="choose">
                                        <ul class="nav nav-pills nav-justified">                            
                                           <li>
                                                 <a style="font-size: 11px;" href="javascript:void(0);" id="<?php echo $bestSellData->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                                            </li>
                                            <li>
                                                <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$bestSellData->id),  array('class'=>'btn-add-to-compare','style'=>"font-size: 11px;"));?>
                                            </li>
                                           
                                        </ul>
                                    </div>                           
                                </div>
                            </div>
                        </div> 
                    <?php endforeach; ?>
                </div>                    
            </div>                
        </div>
    </div>
<?php endif;?>