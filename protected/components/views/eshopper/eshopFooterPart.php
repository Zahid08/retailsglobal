<?php 
$topBrandsModel = Brand::model()->findAll(array('condition'=>'status = :status AND LENGTH(image)>:image','limit' => 4,'order'=>'id desc',
            'params'=>array(':status' => Brand::STATUS_ACTIVE,':image'=>0)
        ));

// Get All Active Content Type
$criteria = new CDbCriteria();
$criteria->condition = 'position=:position AND status=:status';
$criteria->order = 'rank';
$criteria->params = array(':position'=>ContentsType::POSITION_TOP,':status'=>ContentsType::STATUS_ACTIVE);
$contentTypeModel =ContentsType::model()->findAll($criteria);

?>

<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>


<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <?php if(!empty($companyModel)) : ?>
                            <img src="<?php echo Yii::app()->baseUrl.$companyModel->logo;?>" style="height:88px;" />
                        <?php endif; ?> 
                    </div>
                </div>
                <?php /*if(count($topBrandsModel)>0):?>
                    <div class="col-sm-8">
                        <h2 class='ourBrands'>Our Brands</h2>
                        <?php foreach($topBrandsModel as $topKey=>$topBrandData):    ?>
                            <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="javascript:void(0);" title="<?php echo $topBrandData->name;?>">
                                        <div class="iframe-img">
                                            <?php echo '<img src="'.$topBrandData->image.'" alt="'.$topBrandData->name.'" />';?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                         <?php endforeach; ?>
                    </div>
                <?php endif; */ ?>
            </div>
        </div>
    </div>
    
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2>Service</h2>                            
                        <ul class="nav nav-pills nav-stacked">
                            <li ><?php echo CHtml::link('My Cart',array('eshop/viewMyCart')); ?></li>
                            <?php if(!empty(Yii::app()->session['custId'])) :?>
                                <li><?php echo CHtml::link('My Account',array('eshop/customerDashboard')); ?></li>
                                <li><?php echo CHtml::link('<span style="color:#fe980f;">Signout ('.Yii::app()->user->name.')</span> ',array('eshop/logout')); ?></li>
                            <?php else : ?>
                                <li><?php echo CHtml::link('Signup',array('eshop/customerSignup')); ?></li>
                                <li><?php echo CHtml::link('Signin',array('eshop/customerSignin')); ?></li>
                            <?php endif;?>                               
                        </ul>
                    </div>
                </div>
                <?php if(!empty($categoryModel) ):?>
                    <div class="col-sm-5">
                        <div class="single-widget">
                            <h2>Quick Shop</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <?php foreach($categoryModel as $category): ?>
                                    <li style="width:50%; float:left"><?php echo CHtml::link(ucfirst($category->name),array('eshop/proByCat','catId'=>$category->id)); ?></li>                                    
                                <?php endforeach;?>    
                             </ul>                           
                        </div>
                    </div>
                <?php endif; ?> 
                <?php if(!empty($contentTypeModel)) : ?>
                    <div class="col-sm-2">
                        <div class="single-widget">
                            <h2>Policies</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <?php foreach ($contentTypeModel as $key => $menuValue) :?>
                                   <li> <?php echo CHtml::link($menuValue->name,array('eshop/contentsDetails','id'=>$menuValue->id)); ?></li>
                                <?php endforeach; ?>                               
                            </ul>
                        </div>
                    </div> 
                <?php endif;?>

                <div class="col-sm-3">
                    <div class="single-widget form">
                        <h2>News letter</h2>                         
                        <?php $form=$this->beginWidget('CActiveForm', array(
                                'action'=>Yii::app()->createUrl('/eshop/newsLetter'),
                                'id'=>'newsletter-form',
                                'enableAjaxValidation'=>true,
                                'htmlOptions'=>array(
                                    'class'=>'searchform',
                                    'role'=>'form',                                     
                                )
                            )); 
                            echo $form->textField($nlModel,'email',array('placeholder'=>'Subscribe to our newsletter')); 
                            ?>
                            <button type="submit"  class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                            <?php echo $form->error($nlModel,'email',array('class'=>'errorMessage'))?>
                            <p>Get the most recent updates from <br />our site and be updated your self...</p>
                        <?php $this->endWidget(); ?>                            
                    </div>
                </div>                    
            </div>
        </div>
    </div>        
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">&copy;<?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt)).'-'.date("Y");?>&nbsp;&nbsp;<a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a>- all rights reserved.</p>
                <p class="pull-right">Developed by <span><a href="http://www.unlockliveretail.com/" target="_blank">UNLOCKLIVE</a></span></p>
            </div>
        </div>
    </div>
    
</footer><!--/Footer-->
