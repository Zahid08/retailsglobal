<?php if(!empty($offerModel)) : ?>
<div class="widget">
	<h1 class="border">special offers</h1>
	<ul class="product-list">
	<?php foreach($offerModel as $offerKey=>$offerData) :?>
		<li class="sidebar-product-list-item">
			<div class="row">
				<div class="col-xs-4 col-sm-4 no-margin">
				<?php if(!empty($offerData->itemImages)) : ?>
                    <?php echo CHtml::link('<img alt="" src="'.$offerData->itemImages[0]->image.'" data-echo="'.$offerData->itemImages[0]->image.'" style="width:73px;" />',array('eshop/proDetails','id'=>$offerData->id),  array('class'=>'thumb-holder','title'=>$offerData->itemName));?>
                <?php endif;?>
				</div>
				<div class="col-xs-8 col-sm-8 no-margin">
					<?php echo CHtml::link($offerData->itemName,array('eshop/proDetails','id'=>$offerData->id));?>
					<div class="brand">
						<?php echo $offerData->brand->name;?>
					</div>
					<div class="price">
						<?php if(Items::getItemWiseOfferPrice($offerData->id)>0) :?>
							<div class="price-prev" style="text-decoration: line-through;">
								<?php echo Company::getCurrency().'&nbsp;'.$offerData->sellPrice;?>
							</div>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($offerData->id);?></div>
						<?php else : ?>
							<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.$offerData->sellPrice;?></div>
						<?php endif;?>
					</div>
				</div>
			</div>
		</li><!-- /.sidebar-product-list-item -->
    <?php endforeach;?>
	</ul><!-- /.product-list -->
</div><!-- /.widget -->
<?php endif;?>