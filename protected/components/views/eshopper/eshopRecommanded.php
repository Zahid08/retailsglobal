<?php if(!empty($recommandedModel)) : ?>
    <div class="recommended_items"><!--recommended_items-->
        <h2 class="title text-center">recommended items</h2>                        
        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">               
                <div class="item active">
                    <?php foreach ($recommandedModel as $key => $recommandedModelValues) :?>
                         <div class="col-sm-3">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <?php if(!empty($recommandedModelValues->itemImages)) : ?>
                                            <?php echo CHtml::link('<img alt="" src="'.$recommandedModelValues->itemImages[0]->image.'" data-echo="'.$recommandedModelValues->itemImages[0]->image.'" style="width:100%;" />',array('eshop/proDetails','id'=>$recommandedModelValues->id),  array('class'=>'thumb-holder','title'=>$recommandedModelValues->itemName));?>
                                        <?php endif;?>
                                        <div class="price clearfix">
                                            <?php if(Items::getItemWiseOfferPrice($recommandedModelValues->id)>0) :?>
                                            <div class="price-prev pull-left">
                                                   <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recommandedModelValues->id);?></h2>
                                               </div>
                                            <div class="price-current">
                                                   <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($recommandedModelValues->id);?></h2>
                                               </div>
                                            <?php else : ?>
                                            <div class="price-current">
                                                <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($recommandedModelValues->id);?></h2>
                                            </div>
                                            <?php endif;?>
                                        </div>
                                        <p><?php echo $recommandedModelValues->brand->name;?></p>
                                        <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$recommandedModelValues->id), array('class'=>'btn btn-default add-to-cart')); ?>
                                    </div> 

                                    <div class="choose">
                                        <ul class="nav nav-pills nav-justified">                            
                                           <li>
                                                 <a style="font-size: 11px;" href="javascript:void(0);" id="<?php echo $recommandedModelValues->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                                            </li>
                                            <li>
                                                <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$recommandedModelValues->id),  array('class'=>'btn-add-to-compare','style'=>"font-size: 11px;"));?>
                                            </li>                                           
                                        </ul>
                                    </div>                       
                                </div>
                            </div>
                        </div> 
                    <?php endforeach?>
                </div>                
            </div>       
        </div>
    </div><!--/recommended_items-->
<?php endif; ?>