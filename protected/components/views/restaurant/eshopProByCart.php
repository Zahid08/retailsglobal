<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>
<script>
   $(function () {
      // remove from cart global
      $(".remove_from_cart").click(function ()
      {
         // values & validations
         var itemId = $(this).attr('id');
         $.ajax({
            type: "POST",
            dataType: "json",
            beforeSend: function () {
               $(".preloader").show();
            },
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
            data: {
               itemId: itemId,
            },
            success: function (data) {
               $(".preloader").hide();

               // update shopping bag
               $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
                     $(this).removeClass("basket").addClass("basket open");
                     $("#show_cart_items_wrapper").slideDown(200, function () {
                        $('html, body').delay('200').animate({
                           scrollTop: $(this).offset().top - 111
                        }, 200);
                     });
               });
            },
            error: function () {
            }
         });
      });
   });
</script>
<div class="btn-cart-md">
   <a id="show_cart_items" class="cart-link" href="javascript:void(0);">
      <!-- Image -->
      <img class="img-responsive" src="<?php echo $basePath; ?>img/cart.png" alt="" />
      <!-- Heading -->
      <h4>Shopping Cart</h4>
      <span><?php echo $total_qty; ?> items <?php echo Company::getCurrency(); ?><span class="value"><?php echo $total_price; ?></span></span>
      <div class="clearfix"></div>
   </a>
   <?php if ($total_qty > 0) : ?>
      <ul  id="show_cart_items_wrapper" class="cart-dropdown" role="menu">
         <?php foreach ($cart->getItems() as $order_code => $quantity) : ?>
            <li>
               <!-- Cart items for shopping list -->
               <div class="cart-item">
                  <!-- Item remove icon -->

                  <a id="<?php echo $order_code; ?>" class="close-btn remove_from_cart" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                  <!-- Image -->
                  <img alt="" src="<?php echo $cart->getItemImages($order_code); ?>"  class="img-responsive img-rounded" style="width:73px;height:73px;" />
                  <!-- Title for purchase item -->
                  <span class="cart-title"><a href="#"><?php echo $cart->getItemName($order_code); ?></a></span>
                  <span class="cart-title"><?php echo 'Quantity : <strong>' . $quantity . '</strong>'; ?></span>
                  <!-- Cart item price -->
                  <span class="cart-price pull-right red">
                     <?php
                     if (Items::getItemWiseOfferPrice($order_code) > 0) :
                        echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($order_code) * $quantity;
                     else : echo Company::getCurrency() . '&nbsp;' . $cart->getItemPrice($order_code) * $quantity;
                     endif;
                     ?>
                  </span>
                  <div class="clearfix"></div>
               </div>
            </li>
         <?php endforeach; ?>
         <li>
            <!-- Cart items for shopping list -->
            <div class="cart-item">
               <?php 
                    echo CHtml::link('View Cart',array('eshop/viewMyCart'),array('class'=>'btn btn-danger rs_viewcart'));
                    if(!empty(Yii::app()->session['custId']))
                        echo CHtml::link('Checkout',array('checkoutProcess/checkoutShipping'),array('class'=>'btn btn-danger rs_checkout')); 
                    else echo CHtml::link('Checkout',array('checkoutProcess/checkout'),array('class'=>'btn btn-danger rs_checkout')); 
                ?>
            </div>
         </li>
      </ul>
   <?php endif; ?>
   <div class="clearfix"></div>
</div>
