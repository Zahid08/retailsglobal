<div class="header-bottom"><!--header-bottom-->
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
               </button>
            </div>
            <div class="mainmenu pull-left">
               <ul class="nav navbar-nav collapse navbar-collapse">
                  <?php
                  $total = 1;
                  foreach (Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey => $dept) :
                     if ($total <= 8):
                        ?>
                        <li class="dropdown">
                           <?php echo CHtml::link($dept->name . '<i class=\'fa fa-angle-down\'></i>', array('#')); ?>
                              <?php if (!empty(Category::getAllCatByDept($dept->id))) : ?>
                              <ul role="menu" class="sub-menu">
                                    <?php foreach (Category::getAllCatByDept($dept->id) as $ckey => $cat) : ?>
                                    <li>
                                    <?php echo CHtml::link($cat->name, array('eshop/proByCat', 'catId' => $cat->id), array('title' => $cat->name)); ?>
                                    </li>
                              <?php endforeach; ?>
                              </ul>
                        <?php endif; ?>
                        </li>
                        <?php
                        $total++;
                     endif;
                  endforeach;
                  ?>
                  <li>
<?php echo CHtml::link('All Categories', array('eshop/allDeptSubDeptCat'), array('title' => 'All Categories')); ?>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div><!--/header-bottom--> 