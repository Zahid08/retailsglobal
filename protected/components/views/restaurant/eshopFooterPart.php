<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>
<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- Footer Start -->

<div class="footer padd padding-bottom10 padding-top20">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-6">
            <!-- Footer widget -->
            <div class="footer-widget">
               <!-- Logo area -->
               <div class="logo">
                  <?php if (!empty($companyModel)) : ?>
                     <img src="<?php echo Yii::app()->baseUrl . $companyModel->logo; ?>" class="img-responsive" />
                  <?php endif; ?>
                  <!-- Heading -->

               </div>
               <hr />
               <!-- Heading -->
               <h6>On-line Payment Clients</h6>
               <!-- Images -->

               <?php if (!empty($paymentModel)) : ?>
                  <?php
                  foreach ($paymentModel as $paymentKey => $paymentData) :
                     echo '<a><img alt="' . $paymentData->title . '" src="' . $paymentData->image . '" class="payment img-responsive" ></a>';
                  endforeach;
                  ?>
                <?php endif; ?>

            </div> <!--/ Footer widget end -->
         </div>
         <!-- Best Seller as Famous Dishes  -->
          <?php $this->widget('EshopBestSellers', array('limit' => 9)); ?>

         <div class="clearfix visible-sm"></div>
         <div class="col-md-3 col-sm-6">
            <!-- Footer widget -->
            <div class="footer-widget form">
               <!-- Heading -->
               <h4>Join Us Today</h4>
               <!-- Subscribe form -->
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'action' => Yii::app()->createUrl('/eshop/newsLetter'),
                   'id' => 'newsletter-form',
                   'enableAjaxValidation' => true,
                   'htmlOptions' => array(
                       'class' => 'login-form cf-style-1',
                       'role' => 'form'
                   )
               ));
               ?>
               <div class="form-group">
               <?php
               echo $form->textField($nlModel, 'email', array('class' => 'form-control', 'placeholder' => 'Subscribe to our newsletter'));
               echo $form->error($nlModel, 'email');
               ?>
               </div>
               <button class="le-button btn btn-danger btn-sm" type="submit">Subscribe</button>
            <?php $this->endWidget(); ?>
            </div> <!--/ Footer widget end -->
         </div>
         <div class="col-md-3 col-sm-6">
            <!-- Footer widget -->
            <div class="footer-widget">
               <!-- Heading -->
               <h4>Contact Us</h4>
               <div class="contact-details">
               <?php if (!empty($branchModel)) : ?>
                     <!-- Address / Icon -->
                     <i class="fa fa-map-marker br-red"></i> <span><?php echo $branchModel->addressline; ?></span>
                     <div class="clearfix"></div>
                     <!-- Contact Number / Icon -->
                     <i class="fa fa-phone br-green"></i> <span><?php echo $branchModel->phone; ?></span>
                     <div class="clearfix"></div>
                     <!-- Email / Icon -->
                     <i class="fa fa-envelope-o br-lblue"></i> <span><a href="mailto:<?php echo $branchModel->email; ?>"><?php echo $branchModel->email; ?></a></span>

               <?php endif; ?>

                  <div class="clearfix"></div>
                  <?php //if(!empty($branchModel)) echo $branchModel->addressline.', '.$branchModel->city.', '.$branchModel->postalcode;?>
               </div>
               <!-- Social media icon -->
               <?php if (!empty($socialModel)) : ?>
                  <div class="social">
   <?php
   foreach ($socialModel as $socialKey => $socialData) :
      echo '<a style="padding-right:2px;" href="' . $socialData->url . '" title="' . $socialData->name . '" target="_blank"><img alt="' . $socialData->name . '" src="' . Yii::app()->baseUrl . $socialData->icon . '" style="height:40px;"></a>';
   endforeach;
   ?>
                  </div>
<?php endif; ?>
            </div> <!--/ Footer widget end -->
         </div>
      </div>
      <!-- Copyright -->
      <div class="footer-copyright">
         <!-- Paragraph -->
         <p> &copy; <?php if (!empty($companyModel)) echo date("Y", strtotime($companyModel->crAt)) . '-' . date("Y"); ?>  <a href="javascript:void(0);"><?php if (!empty($companyModel)) echo $companyModel->name; ?></a> - all rights reserved, Developed By : <a href="http://www.unlockliveretail.com/" target="_blank">UNLOCKLIVE</a></p>
      </div>
   </div>
</div>
<!-- Footer End -->