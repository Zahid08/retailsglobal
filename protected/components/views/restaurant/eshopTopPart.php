<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>
<?php if (isset(Yii::app()->session['newsletterMsg']) && !empty(Yii::app()->session['newsletterMsg'])) : ?>
   <div class="notification_msg">
      <?php
        echo Yii::app()->session['newsletterMsg'];
        unset(Yii::app()->session['newsletterMsg']);
      ?>
   </div>
   <?php
endif; ?>
<!-- Header Start -->
<div class="header">
   <div class="container">
      <div class="row">
         <div class="res-top-bar">
            <div class="col-xs-12 col-md-6 no-margin no-padding-left left-menu">
               <ul>
                  <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
                  <?php
                  if (!empty($contentTypeModel)):
                     foreach ($contentTypeModel as $mKey => $menuValue):
                        ?>
                        <li> <?php echo CHtml::link($menuValue->name, array('eshop/contentsDetails', 'id' => $menuValue->id)); ?></li>
                        <?php
                     endforeach;
                  endif;
                  ?>
               </ul>
            </div><!-- /.col -->

            <div class="col-xs-12 col-md-6 no-margin right-menu">
               <div class="top-cart-holder dropdown animate-dropdown pull-right">
                  <div class="basket">
                     <?php $this->widget('EshopProByCart'); ?>
                  </div><!-- /.basket -->
               </div><!-- /.top-cart-holder -->
               <ul class="right">
                  <?php if (!empty(Yii::app()->session['custId']) && Yii::app()->user->name != 'Guest') : ?>
                     <li><?php echo CHtml::link('My Account', array('eshop/customerDashboard')); ?></li>
                     <li><a href="<?php echo Yii::app()->createAbsoluteUrl('eshop/customerWishlist'); ?>" title="Wishlist"> Wishlist <span class="value" id="wishListCount"><?php echo (!empty(Yii::app()->session['custId'])) ? Wishlist::countWishListByCustomer(Yii::app()->session['custId']) : '(0)'; ?></span> </a></li>

                     <li><?php echo CHtml::link('<span style="color:#f8484a;">Signout (' . Yii::app()->user->name . ')</span> ', array('eshop/logout')); ?></li>
                  <?php else : ?>
                     <li><?php echo CHtml::link('Signup', array('eshop/customerSignup')); ?></li>
                     <li><?php echo CHtml::link('Signin', array('eshop/customerSignin')); ?></li>
                  <?php endif; ?>

               </ul>
            </div><!-- /.col -->
         </div>
      </div>
      <!-- Header top area content -->

      <div class="row">
         <div class="logo-menu-wrapper">
            <div class="col-md-3 col-sm-4 col-xs-5 logo-contact">
               <div class="header-contact">
                  <!-- Contact number -->
                  <span><i class="fa fa-phone red"></i>  <?php if (!empty($branchModel)) echo $branchModel->phone; ?></span>
               </div>
               <!-- Link -->
               <a href="index-2.html">
                  <!-- Logo area -->
                  <div class="logo">
                     <?php if (!empty($companyModel)) echo CHtml::link('<img src="' . Yii::app()->baseUrl . $companyModel->logo . '" style="" class="img-responsive" />', array('eshop/index'), array('title' => $companyModel->name)); ?>
                  </div>
               </a>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-7 mobile-hide">
               <div class="header-search">
                  <form class="form" role="form" method="post" action="<?php echo Yii::app()->getBaseUrl(true) . '/index.php/eshop/proByCatSearch/' ?>">
                     <div class="input-group">
                        <?php
                        $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                            'name' => 'searchItems',
                            'source' => Yii::app()->createUrl('eshop/autoSuggestItemSearch'),
                            'options' => array(
                                'minLength' => 1, // console.log(ui.item.id +":"+ui.item.value);
                                'showAnim' => 'fold',
                                'select' => 'js:function(event, ui) {
                                                        window.location.href = "' . Yii::app()->baseUrl . '/eshop/proDetails?id="+ui.item.id;
                                                    }'
                            ),
                            'htmlOptions' => array(
                                'id' => 'items',
                                'rel' => 'val',
                                'class' => "form-control",
                                'placeholder' => 'Search......'
                            ),
                        ));
                        ?>
                        <span class="input-group-btn">
                           <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                     </div>
                  </form>
               </div>
            </div>

            <div class="col-md-5 col-sm-4 col-xs-12 small-right">
               <!-- Navigation -->
               <nav class="navbar navbar-default navbar-right" role="navigation">
                  <div class="container-fluid">
                     <!-- Brand and toggle get grouped for better mobile display -->
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                           <span class="sr-only">Toggle navigation</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                        </button>
                     </div>

                     <!-- Collect the nav links, forms, and other content for toggling -->
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                           <li>
                              <?php
                              echo CHtml::link('<img class="img-responsive" src="' . $basePath . '/img/nav-menu/nav1.jpg" />Home', array('eshop/index'), array('class' => ''));
                              ?>
                           </li>
                           <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo $basePath; ?>img/nav-menu/nav4.jpg" class="img-responsive" alt="" /> Menu <b class="caret"></b></a>
                              <?php if(!empty(Category::getAllCategoryByItemsHas())) : // dynamic menu
                                 $dynamicStyle = 'style="width:200px;"';
                                 $sp = '12';
                                 if (sizeof(Category::getAllCategoryByItemsHas()) > 10) :
                                    $dynamicStyle = 'style="width:400px;"';
                                    $sp = '6';
                                 endif; ?>
                                 <ul class="dropdown-menu dropdown-md" <?php echo $dynamicStyle; ?>>
                                    <li>
                                       <div class="row">
                                          <?php foreach (Category::getAllCategoryByItemsHas() as $key => $data) : ?>
                                             <div class="col-md-<?php echo $sp; ?> col-sm-<?php echo $sp; ?>">
                                                <!-- Menu Item -->
                                                <div class="menu-item">
                                                   <?php
                                                   echo CHtml::link($data->name, array('eshop/proByCat?catId=' . $data->id), array('class' => 'dropdown-toggle'));
                                                   ?>
                                                </div>
                                             </div>
                                          <?php endforeach; ?>
                                       </div>
                                    </li>
                                 </ul>
                              <?php endif; ?>
                           </li>
                           <li><?php
                              echo CHtml::link('<img class="img-responsive" src="' . $basePath . '/img/nav-menu/nav2.jpg" class="img-responsive"  />About', array('eshop/contentsDetails/6'), array('class' => 'dropdown-toggle'));
                              ?></li>
                           <li>
                              <?php
                              echo CHtml::link('<img class="img-responsive" src="' . $basePath . '/img/nav-menu/nav6.jpg" />Contact', array('eshop/contractUs'), array('class' => 'dropdown-toggle'));
                              ?>
                           </li>
                        </ul>
                     </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
               </nav>
            </div>
         </div>
      </div>
   </div> <!-- / .container -->
</div>

<!-- Header End -->