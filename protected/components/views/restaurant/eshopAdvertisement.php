<?php if (!empty($advertisementModel)) : ?>
   <div class="showcase padding-top20">
      <div class="container">
         <div class="row">
            <?php foreach ($advertisementModel as $aKey => $aData) : ?>
               <div class="col-md-4 col-sm-4">
                  <div class="menu-head">
                     <?php if (!empty($aData->url)) : ?>
                        <a href="<?php echo $aData->url; ?>" target="_blank" title="<?php echo $aData->title; ?>">
                           <img class="menu-img img-responsive img-thumbnail" src="<?php echo $aData->image; ?>" alt="<?php echo $aData->title; ?>">
                        </a>
                     <?php else : ?>
                        <img class="menu-img img-responsive img-thumbnail" src="<?php echo $aData->image; ?>" alt="<?php echo $aData->title; ?>">
                     <?php endif; ?>
                     <div class="clearfix"></div>
                  </div>
               </div>
            <?php endforeach; ?>
         </div>
      </div>
   </div>
<?php endif; ?>

