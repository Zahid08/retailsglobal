<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>
<?php if (!empty($featuredModel)) : ?>
   <div class="dishes margin-top20">
      <div class="container">
         <!-- Default Heading -->
         <div class="default-heading margin0">
            <!-- Crown image -->
            <img class="img-responsive" src="<?php echo $basePath; ?>img/menu.png" alt="" />
            <!-- Heading -->
            <h2>New Dishes</h2>
            <!-- Paragraph -->
            <!-- Border -->
            <div class="border"></div>
         </div>
         <div class="row">
            <?php foreach ($featuredModel as $fImageKey => $feturedImageData) : ?>
               <div class="col-md-3 col-sm-6">
                  <div class="dishes-item-container margin-top20">
                     <!-- Image Frame -->
                     <div class="img-frame" id="disher-wrapper">
                        <!-- Image -->
                        <?php if (!empty($feturedImageData->itemImages)) : ?>
                           <?php echo CHtml::link('<img alt="" src="' . $feturedImageData->itemImages[0]->image . '" data-echo="' . $feturedImageData->itemImages[0]->image . '" class="" />', array('eshop/proDetails', 'id' => $feturedImageData->id), array('title' => $feturedImageData->itemName)); ?>
                        <?php endif; ?>
                        <!-- Block for on hover effect to image -->
                        <div class="img-frame-hover">
                           <!-- Hover Icon -->
                           <?php if (!empty($feturedImageData->itemImages)) : ?>
                              <?php echo CHtml::link('<i class="fa fa-cutlery"></i>', array('eshop/proDetails', 'id' => $feturedImageData->id), array('title' => $feturedImageData->itemName)); ?>
                           <?php endif; ?>
                        </div>
                     </div>
                     <!-- Dish Details -->
                     <div class="dish-details">
                        <!-- Heading -->
                        <h3>
                           <?php echo CHtml::link($feturedImageData->itemName, array('eshop/proDetails', 'id' => $feturedImageData->id), array('title' => $feturedImageData->itemName)); ?>
                        </h3>
                        <!-- Paragraph -->
                        <p><?php if (!empty($feturedImageData->description)) echo substr($feturedImageData->description, 0, 50) . '..'; // specification     ?></p>
                        <!-- Button -->
                        <?php echo CHtml::link('Add to cart', array('eshop/proDetails', 'id' => $feturedImageData->id), array('class' => 'btn btn-danger btn-sm')); ?>
                     </div>
                  </div>
               </div>
            <?php endforeach; ?>
         </div>
      </div>
   </div>
<?php endif; ?>

