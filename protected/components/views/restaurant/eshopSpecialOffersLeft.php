<?php if(!empty($offerModel)) : ?>
<div class="pricing padding-top20">
    <div class="container">
        <!-- Default Heading -->
        <div class="default-heading margin0">        
            <!-- Crown image -->
            <img class="img-responsive" src="<?php echo Yii::app()->theme->baseUrl;?>/img/menu.png" alt="" />
            <!-- Heading -->
            <h2>Special Offer</h2>
            <!-- Border -->
            <div class="border"></div>
        </div>
        <div class="row">
        <?php $pageBreak = 1; foreach($offerModel as $offerKey=>$offerData) : ?>
            <div class="col-md-6 col-sm-6">
                <!-- Pricing list Item -->
                <div class="pricing-item" id="speacial-offer">
                    <!-- Image -->
                    <?php echo CHtml::link('<img alt="" src="'.$offerData->itemImages[0]->image.'" data-echo="'.$offerData->itemImages[0]->image.'" class="img-responsive img-thumbnail" />',array('eshop/proDetails','id'=>$offerData->id),array('title'=>$offerData->itemName));?>
                    <!-- Pricing item details -->
                    <div class="pricing-item-details">
                        <!-- Heading -->
                        <h3><?php echo CHtml::link($offerData->itemName,array('eshop/proDetails','id'=>$offerData->id));?></h3>
                        <!-- Paragraph -->
                        <p><?php echo substr($offerData->description,0,100);?>...</p>
                        <!-- Buy link -->
                        <p>
                            <div class="price-prev" style="float:left;text-decoration: line-through;font-size:16px;margin:10px;color:#ed4747;">
                                <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($offerData->id);?>
                            </div>
                        </p>
                        <?php echo CHtml::link('Order now',array('eshop/proDetails','id'=>$offerData->id),array('class'=>'btn btn-danger pull-right'));?>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Tag -->
                    <span class="hot-tag br-red">
				        <div class="price-current"><?php echo '<span style="font-size:10px;">'.Company::getCurrency().'</span><br/>'.Items::getItemWiseOfferPrice($offerData->id);?></div>
                    </span>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php if($pageBreak==2) : ?>
                <div class="clearfix visible-md"></div>
            <?php endif;  ?>
           <?php $pageBreak++; endforeach;?> 
        </div>
    </div>
</div>
<?php endif;  ?>