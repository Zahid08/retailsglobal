<div class="col-md-3 col-sm-6">
    <!-- Footer widget -->
    <div class="footer-widget">
        <!-- Heading -->
        <h4>Famous Dishes</h4>
        <!-- Images -->
        <?php if (!empty($bestSellers)) : foreach($bestSellers as $bestSellKey=>$bestSellData):
        if(!empty($bestSellData->itemImages)) : ?>
            <?php echo CHtml::link('<img alt="" src="'.$bestSellData->itemImages[0]->image.'" data-echo="'.$bestSellData->itemImages[0]->image.'" class="dish img-responsive" />',array('eshop/proDetails','id'=>$bestSellData->id),  array('title'=>$bestSellData->itemName));?>
        <?php endif; endforeach; endif; ?>
    </div> <!--/ Footer widget end -->
</div>