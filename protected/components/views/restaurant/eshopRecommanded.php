<?php if (!empty($recommandedModel)): ?>
   <div class="recommended_items"><!--recommended_items-->
      <h2 class="title text-left">Recommended items</h2>

      <div class="shopping">
         <div class="">
            <div class="shopping-content">
               <div class="row">
                  <div id="yw0" class="list-view">
                     <div class="items clearfix">
                        <?php foreach ($recommandedModel as $key => $recommandedModelValues) : ?>
                           <div class="col-md-3 col-sm-6">
                              <div class="shopping-item">
                                 <?php if (!empty($recommandedModelValues->itemImages)) : ?>
                                    <?php echo CHtml::link('<img alt="" src="' . $recommandedModelValues->itemImages[0]->image . '" data-echo="' . $recommandedModelValues->itemImages[0]->image . '" style="width:100%;" />', array('eshop/proDetails', 'id' => $recommandedModelValues->id), array('class' => 'thumb-holder', 'title' => $recommandedModelValues->itemName)); ?>
                                 <?php endif; ?>
                                 <h4 class="pull-left">
                                    <?php echo $recommandedModelValues->itemName; ?>
                                 </h4>
                                 <span class="item-price pull-right">
                                    <?php if (Items::getItemWiseOfferPrice($recommandedModelValues->id) > 0) : ?>
                                       <div class="price-prev pull-left">
                                          <h2><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recommandedModelValues->id); ?></h2>
                                       </div>
                                       <div class="price-current pull-right">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($recommandedModelValues->id); ?>
                                       </div>
                                    <?php else : ?>
                                       <div class="price-current pull-right">
                                          <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($recommandedModelValues->id); ?>
                                       </div>
                                    <?php endif; ?>
                                 </span>
                                 <div class="clearfix"></div>
                                 <div class="item-hover br-red hidden-xs"></div>
                                 <?php
                                 echo CHtml::link('Add to cart', array('eshop/proDetails', 'id' => $recommandedModelValues->id), array('class' => 'link hidden-xs'));
                                 ?>
                                 <div class="productinfo text-center"> </div>
                              </div>
                           </div>
                        <?php endforeach ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div><!--/recommended_items-->
<?php endif; ?>