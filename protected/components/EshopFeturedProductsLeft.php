<?php

Yii::import('zii.widgets.CPortlet');

class EshopFeturedProductsLeft extends CPortlet
{
    public $featuredModel;
    public $limit = 10;
    public function init()
    {
        parent::init();
    }

    protected function renderContent()
    {
        $featuredModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce AND status=:status',
                                                       'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),
                                                       'group'=>'itemName','order'=>'rand(),crAt DESC','limit'=>$this->limit,
                         ));
        $this->render(Yii::app()->theme->name.'/eshopFeturedProductsLeft',array('featuredModel'=>$featuredModel));
    }
}