<?php
Yii::import('zii.widgets.CPortlet');

class EshopAdvertisement extends CPortlet
{
    public $advertisementModel;
    public $position;
    public $limit=2;
   

    public function init()
    {
        parent::init();
    }
    protected function renderContent()
    {
      $advertisementModel = Advertisement::model()->findAll(array('condition'=>'startDate<=:startDate AND endDate>=:endDate AND status=:status AND position=:position',
                                                                   'params'=>array(':startDate'=>date("Y-m-d"),':endDate'=>date("Y-m-d"),
                                                                                   ':status'=>Advertisement::STATUS_ACTIVE,':position'=>$this->position),
                                                                   'limit' =>$this->limit,'order'=>'crAt DESC',
                             ));
        $this->render(Yii::app()->theme->name.'/eshopAdvertisement',array('advertisementModel'=>$advertisementModel,'position'=>$this->position));
    }
}