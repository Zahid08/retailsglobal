<?php
/*********************************************************
        -*- File: main.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/config
		-*-  YII-*- version 1.1.13
/*********************************************************/

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	public $branchId;
	
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	 
	public function authenticate()
	{
		$user = User::model()->find(array('condition'=>'branchId=:branchId AND username=:username', //  AND status=:status
										  'params'=>array(':branchId'=>$this->branchId,':username'=>$this->username))); // ,':status'=>User::STATUS_ACTIVE

		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		
		else
		{
			$this->_id = $user->id;
			$this->setState('userid', $user->id);
			$this->setState('username', $user->username);

			// set super administrative status
			$modelSuperAdmin = Roles::model()->exists(array('condition'=>'userTypeId=:userTypeId and userId=:userId',
															'params'=>array(':userTypeId'=>UserType::getSuperUserType(),':userId'=>$user->id)));
			if(!empty($modelSuperAdmin))
				Yii::app()->user->setState('isSuperAdmin', true);
			else
				Yii::app()->user->setState('isSuperAdmin', false);	
				
			
			//-- generate usertype list/role for specific user --
			$usertypesIdarr = array();
			$usertypesIdarr = CHtml::listData(Roles::model()->findAll('userId=:userId',array(':userId'=>$user->id)), 'userTypeId', 'userTypeId');
			Yii::app()->session['usertypesIds'] = $usertypesIdarr;
			Yii::app()->session['branchId'] = $user->branchId;
			Yii::app()->session['branchName'] = $user->branch->name;	
			
			// if supplier then set session 
			if(!empty($user->supplierId) && $user->supplierId>0)
			{
				Yii::app()->session['supplierId'] = $user->supplierId;	
				Yii::app()->session['supplierName'] = $user->supplier->name;
			}
            // if customer then set session
            else if(!empty($user->custId) && $user->custId>0)
            {
                Yii::app()->session['custId'] = $user->custId;
                Yii::app()->session['custName'] = $user->customer->name;
            }
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode==self::ERROR_NONE;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}