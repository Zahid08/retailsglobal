<?php
Yii::import('zii.widgets.CPortlet');

class EshopProByCart extends CPortlet
{
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $cart = new Cart('cart');
        $total_qty = $total_price = 0;
        $cart = new Cart('cart');
        if($cart->hasItems()) :
            foreach ($cart->getItems() as $order_code=>$quantity ) :
                $total_qty = $total_qty + $quantity;
                if(Items::getItemWiseOfferPrice($order_code)>0)
                    $total_price+= $quantity*Items::getItemWiseOfferPrice($order_code);
                else $total_price+= $quantity*$cart->getItemPrice($order_code);
            endforeach;
        endif;
		$this->render(Yii::app()->theme->name.'/eshopProByCart',array(
            'cart'=>$cart,'total_qty'=>$total_qty,'total_price'=>$total_price,
        ));
	}
}