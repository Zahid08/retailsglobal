<?php

/*********************************************************
        -*- File: Browser.php	
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/components
		-*-  YII-*- version 1.1.13
/*********************************************************/

class Browser 
{ 
    private $props    = array("Version" => "0.0.0", 
                                "Name" => "unknown", 
                                "Agent" => "unknown",
    							 "osInfo" => "unknown"
    
    ) ; 

    public function __Construct() 
    { 
        $browsers = array("firefox", "msie", "opera", "chrome", "safari", 
                            "mozilla", "seamonkey",    "konqueror", "netscape", 
                            "gecko", "navigator", "mosaic", "lynx", "amaya", 
                            "omniweb", "avant", "camino", "flock", "aol"); 
		
        
        $oses = array (
			'iPhone' => '(iphone)',
			'Windows 3.11' => 'win16',
			'Windows 95' => '(windows 95)|(win95)|(windows_95)', // Use regular expressions as value to identify operating system
			'Windows 98' => '(windows 98)|(win98)',
			'Windows 2000' => '(windows nt 5.0)|(windows 2000)',
			'Windows XP' => '(windows nt 5.1)|(windows xp)',
			'Windows 2003' => '(windows nt 5.2)',
			'Windows Vista' => '(windows nt 6.0)|(windows vista)',
			'Windows 7' => '(windows nt 6.1)|(windows 7)',
			'Windows NT 4.0' => '(windows nt 4.0)|(winnt4.0)|(winnt)|(windows nt)',
			'Windows ME' => 'windows me',
			'Open BSD'=>'openbsd',
			'Sun OS'=>'sunos',
			'Linux'=>'(linux)|(x11)',
			'Safari' => '(safari)',
			'Macintosh'=>'(mac_powerpc)|(macintosh)',
			'QNX'=>'qnx',
			'BeOS'=>'beos',
			'OS/2'=>'os\/2',
			'Search Bot'=>'(nuhk)|(googlebot)|(yammybot)|(openbot)|(slurp\/cat)|(msnbot)|(ia_archiver)'
		);
        
        
        $this->Agent = strtolower($_SERVER['HTTP_USER_AGENT']); 
        foreach($browsers as $browser) 
        { 
            if (preg_match("#($browser)[/ ]?([0-9.]*)#", $this->Agent, $match)) 
            { 
                $this->Name = $match[1] ; 
                $this->Version = $match[2] ; 
                break ; 
            } 
        } 
        
    	foreach($oses as $os=>$pattern){ // Loop through $oses array
	    // Use regular expressions to check operating system type
			  if (preg_match("/$pattern/", $this->Agent)) {
			 // Check if a value in $oses array matches current user agent.
				$this->osInfo = $os; // Operating system was matched so return $oses key
				break;
			}
		}      	 	
    } 
 	
    public function __Get($name) 
    { 
        if (!array_key_exists($name, $this->props)) 
        { 
            die ("No such property or function $name") ; 
        } 
        return $this->props[$name] ; 
    } 

    public function __Set($name, $val) 
    { 
        if (!array_key_exists($name, $this->props)) 
        { 
            SimpleError("No such property or function.", "Failed to set $name", $this->props) ; 
            die ; 
        } 
        $this->props[$name] = $val ; 
    } 
} 

?> 