<?php
Yii::import('zii.widgets.CPortlet');
class EshopSpecialOffersLeft extends CPortlet
{
    public $offerModel;
    public $limit = 10;
    public function init()
    {
        parent::init();
    }
    protected function renderContent()
    {
        $date = date("Y-m-d");
        $branchId = (!empty(Yii::app()->session['branchId']))?Yii::app()->session['branchId']:Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
        $sqlOffer = "SELECT * from pos_items WHERE id IN(
                        SELECT itemId FROM pos_poffers WHERE branchId=".$branchId." AND startDate<='".$date."' AND endDate>='".$date."'
                    ) AND isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY itemName ORDER BY crAt DESC LIMIT 0,$this->limit";
        $offerModel = Items::model()->findAllBySql($sqlOffer);
        $this->render(Yii::app()->theme->name.'/eshopSpecialOffersLeft',array('offerModel'=>$offerModel));
    }
}