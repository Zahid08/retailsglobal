<?php
Yii::import('zii.widgets.CPortlet');

class TopMenu extends CPortlet
{
	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $modelCustomerPendingReview = Customer::model()->findAll(array('condition'=>'status = :status',
                                                                       'params'=>array(':status' => Customer::STATUS_INACTIVE),
                                                                       'order'=>'crAt DESC','limit'=>10,
                                      ));
        $modelSupplierPendingReview = Supplier::model()->findAll(array('condition'=>'status = :status',
                                                                       'params'=>array(':status' => Supplier::STATUS_INACTIVE),
                                                                       'order'=>'crAt DESC','limit'=>10,
                                       ));

        $modelItemPendingReview = ItemReview::model()->findAll(array('condition'=>'status = :status',
                                                                     'params'=>array(':status' => ItemReview::STATUS_INACTIVE),
                                                                     'order'=>'crAt DESC','limit'=>10,
                                   ));
        $modelOrderPendingReview = SalesInvoice::model()->findAll(array('condition'=>'status=:status AND isEcommerce=:isEcommerce',
                                                                        'params'=>array(':status' => SalesInvoice::STATUS_PROCESSING,':isEcommerce'=>SalesInvoice::IS_ECOMMERCE),
                                                                        'order'=>'crAt DESC','limit'=>10,
                                   ));
        $this->render('topMenu',array(
            'modelCustomerPendingReview'=>$modelCustomerPendingReview,
            'modelSupplierPendingReview'=>$modelSupplierPendingReview,
            'modelItemPendingReview'=>$modelItemPendingReview,
            'modelOrderPendingReview'=>$modelOrderPendingReview,
        ));
	}
}