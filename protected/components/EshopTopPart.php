<?php
Yii::import('zii.widgets.CPortlet');

class EshopTopPart extends CPortlet
{
    public $companyModel;
    public $contentTypeModel;

	public function init()
	{
		parent::init();
	}

	protected function renderContent()
	{
        $socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));

        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        $branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                              array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));

        // Get All Active Content Type
        $criteria = new CDbCriteria();
        $criteria->condition = 'position=:position AND status=:status';
        $criteria->order = 'rank';
        $criteria->params = array(':position'=>ContentsType::POSITION_TOP,':status'=>ContentsType::STATUS_ACTIVE);
        $contentTypeModel =ContentsType::model()->findAll($criteria);

		$this->render(Yii::app()->theme->name.'/eshopTopPart',array(
            'companyModel'=>$companyModel,'branchModel'=>$branchModel,
            'contentTypeModel'=>$contentTypeModel,
            'socialModel'=>$socialModel,
        ));
	}
}