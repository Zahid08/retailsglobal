<?php
/*********************************************************
        -*- File: Controller.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/component
		-*-  YII-*- version 1.1.16
/*********************************************************/
class Controller extends CController
{
	/**
	 * @var array the breadcrumbs of the current page.
	 */
	public $breadcrumbs=array();

	/**
	 * @var array context menu items.
	 */
	public $menu=array();

	/**
	 * @var array the portlets of the current page.
	 */
	public $leftPortlets=array();
	public $rightPortlets=array();

	/**
	 * @var array the ips of privilege.
	 */
	public $ips = array(
		'127.0.0.1',
		'::1', // localhost
	);
	
	public $layout='//layouts/defaultcolumn';
	public $metaTitle = NULL;
    public $metaKeywords = NULL;
    public $metaDescription = NULL;
    public $metaOgUrl = NULL;
    public $metaOgImage = NULL;
	
	// global permission to all cont actions 
	public function accessRules()
	{	
		//echo $this->getUniqueId();
		/*if(isset($this->module))
			return UserRights::getRights($this->module->id.'_'.$this->id);
		else return UserRights::getRights($this->getUniqueId());
		*/
		return UserRights::getRights($this->getUniqueId());
	}

	/**
	 * initialize
	 */
	function init()
	{
        // set company domain in session
        if(isset(Yii::app()->session['orgId']) && !empty(Yii::app()->session['orgId'])) unset(Yii::app()->session['orgId']);
        $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
        if(!empty($companyModel)) Yii::app()->session['orgId'] = $companyModel->domain;
        else
        {
            $protocol = 'http';
            if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https';
            $host = $_SERVER['SERVER_NAME'];
            $baseUrl = $protocol.'://'.$host;
            $url = parse_url($baseUrl ,PHP_URL_HOST);
            $domain = strstr(str_replace("www.","",$url), ".",true);
            Yii::app()->session['orgId'] = $domain;
        }

        // auto redirect to login if not get session data
		if(!isset(Yii::app()->user->userid) && empty(Yii::app()->user->userid))
			if(($this->getUniqueId()!='eshop') && ($this->getUniqueId()!='checkoutProcess')) $this->redirect(array('eshop/login'));

		// load theme skin dynamically from company settings
        if(!empty($companyModel->theme)) Yii::app()->theme = $companyModel->theme;
        else Yii::app()->theme = Company::DEFAULT_THEME;

		Yii::app()->session['themesnames'] = Yii::app()->theme->baseUrl;
		Yii::app()->session['skinsnames'] = Yii::app()->params->skinEshop;		

		if(Yii::app()->getRequest()->getParam('printview')) Yii::app()->layout='print';	
		if(!isset(Yii::app()->session['visibilityarray']))
		{
			if(Yii::app()->user->isGuest)
			{
				Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(array(UserType::TYPE_GUEST=>UserType::TYPE_GUEST));
			}
			else
			{
				Yii::app()->session['visibilityarray'] = UserRights::getpermittedactions(Yii::app()->session['usertypesIds']);
			}	
		}
		// $Menus['visible'] = UserRights::getVisibility($model->controllerName, $model->actionName);

        // set metaKey & metaDescription & og_url
        $seoMetaKeyModel = Seo::model()->find('tagName=:tagName AND status=:status',array(':tagName'=>Seo::TAG_KEY,':status'=>Seo::STATUS_ACTIVE));
        if(!empty($seoMetaKeyModel)) $this->metaKeywords = $seoMetaKeyModel->description;

        $seoMetaDescModel = Seo::model()->find('tagName=:tagName AND status=:status',array(':tagName'=>Seo::TAG_DESCRIPTION,':status'=>Seo::STATUS_ACTIVE));
        if(!empty($seoMetaDescModel)) $this->metaDescription = $seoMetaDescModel->description;

        $this->metaOgUrl = 'http://www.'.$companyModel->domain;
        $this->metaOgImage = 'http://www.'.$companyModel->domain.$companyModel->logo;
	}

	/**
	 * @return array behaviors
	 */
	public function behaviors()
	{
		return array();
	}

	/**
	 * @return boolean true if request IP matches given pattern, otherwise false
	 */
	public function isIpMatched()
	{
		$ip=Yii::app()->request->userHostAddress;

		foreach($this->ips as $rule)
		{
			if($rule==='*' || $rule===$ip || (($pos=strpos($rule,'*'))!==false && !strncmp($ip,$rule,$pos)))
				return true;
		}
		return false;
	}

    /*  load before current controller loaded //
    public function beforeAction($event)
    {
        module/global loaded
        if(!empty($event->controller->module)) $name = $event->controller->module->id.'_'.$event->controller->id;
        else $name = $event->controller->id;

        $contModel = Controllers::model()->find(array('condition'=>'name=:name', 'params'=>array(':name'=>$name)));
        $layoutModel = ContActions::model()->find(array('condition'=>'contId=:contId and name=:name',
                                                        'params'=>array(':contId'=>$contModel->id, ':name'=>$event->id)));
        if(!empty($layoutModel))
        {
            if(in_array($layoutModel->layouts->name, Layouts::getLayouts(Yii::app()->session['themesnames'])))
                return $event->controller->layout = '//layouts/'.$layoutModel->layouts->name;
            else return $event->controller->layout = Yii::app()->session['defaultlayout'];
        }
        else return $event->controller->layout = Yii::app()->session['defaultlayout'];
        // import the module-level models and components
    }*/
}
?>