<?php
/*********************************************************
        -*- File: UsefulFunction.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: protected/config
		-*-  YII-*- version 1.1.13
/*********************************************************/

class UsefulFunction 
{
    const IMAGE_TYPE_SMALL  = 'small';
    const IMAGE_TYPE_MEDIUM = 'medium';
    const IMAGE_TYPE_LARGE  = 'large';

    const IMAGE_SIZE_SMALL  = 100;
    const IMAGE_SIZE_MEDIUM = 240;
    const IMAGE_SIZE_LARGE  = 1100;

	// dynamic enum generators
	public static function enumItem($model,$attribute)
    {
		$attr = $attribute;
		//self::resolveName($model,$attr);
		preg_match('/\((.*)\)/',$model->tableSchema->columns[$attr]->dbType,$matches);
		foreach(explode(',', $matches[1]) as $value)
		{
			$value=str_replace("'",null,$value);
			$values[$value]=Yii::t('enumItem',$value);
		}
		return $values;
    }  
	 
	// generate password
	public static function generatePassword ($length = 20)
	{	
		$password = "";
		$possible = "012346789abcdfghjkmnpqrtvwxyzABCDFGHJKLMNPQRTVWXYZ";
		$maxlength = strlen($possible);
		if ($length > $maxlength) {
		  $length = $maxlength;
		}
		$i = 0; 
		while ($i < $length) { 
		  $char = substr($possible, mt_rand(0, $maxlength-1), 1);
		  if (!strstr($password, $char)) { 
			$password .= $char;
			$i++;
		  }
		}
		return $password;
	 }
	 
	 // customize capcha
	 public static function captcha_raw()
	 {
		unset(Yii::app()->session['random_number']);
		$string = '';			
		for ($i = 0; $i < 5; $i++) {
			$string .= chr(rand(97, 122));
		}
		Yii::app()->session['random_number'] = $string;
		return Yii::app()->session['random_number'];
	 }
	 
	 // money format ref : http://www.php.net/manual/ru/function.money-format.php
	 public static function formatMoney($number, $fractional=false) {
		if ($fractional) {
			$number = sprintf('%.2f', $number);
		}
		while (true) {
			$replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
			if ($replaced != $number) {
				$number = $replaced;
			} else {
				break;
			}
		}
		return $number;
	} 
	
	// database backup
	public static function backupDb($filepath, $tables = '*') 
	{
		if ($tables == '*') {
			$tables = array();
			$tables = Yii::app()->db->schema->getTableNames();
		} else {
			$tables = is_array($tables) ? $tables : explode(',', $tables);
		}
		$return = '';
	
		foreach ($tables as $table) {
			$result = Yii::app()->db->createCommand('SELECT * FROM ' . $table)->query();
			$return.= 'DROP TABLE IF EXISTS ' . $table . ';';
			$row2 = Yii::app()->db->createCommand('SHOW CREATE TABLE ' . $table)->queryRow();
			$return.= "\n\n" . $row2['Create Table'] . ";\n\n";
			foreach ($result as $row) {
				$return.= 'INSERT INTO ' . $table . ' VALUES(';
				foreach ($row as $data) {
					$data = addslashes($data);
	
					// Updated to preg_replace to suit PHP5.3 +
					$data = preg_replace("/\n/", "\\n", $data);
					if (isset($data)) {
						$return.= '"' . $data . '"';
					} else {
						$return.= '""';
					}
					$return.= ',';
				}
				$return = substr($return, 0, strlen($return) - 1);
				$return.= ");\n";
			}
			$return.="\n\n\n";
		}
		//save file
		$handle = fopen($filepath, 'w+');
		fwrite($handle, $return);
		fclose($handle);
	}

    // e-shop contents position
    public static function eshopContentsTypePosition()
    {
        return array(
            'top'=>'Top',
            'bottom'=>'Bottom'
        );
    }
    // e-shop advertisement position
    public static function eshopAdvertisementposition()
    {
        return array(
            'left-sidebar'=>'Left Sidebar',
            'slider-right'=>'Slider Right',
/*            'left'=>'Left',
            'right'=>'Right',*/
        );
    }
    // customer title
    public static function getCustomerTitle()
    {
        return array(
            'Mr'=>'Mr',
            'Ms'=>'Ms',
            'Mrs'=>'Mrs',
            'M/s'=>'M/s',
            'Dr'=>'Dr',
        );
    }
    // customer gender
    public static function getCustomerGender()
    {
        return array(
            'Male'=>'Male',
            'Female'=>'Female',
        );
    }

    //shipping Method is parent
    public static function getShippingIsParent()
    {
       return array(
           'No'=>'No',
           'Yes'=>'Yes',
       );
    }

    // Payment API short Name
    public static function getApiShostName()
    {
        return array(
            'paypal'=>'Paypal',
            'skrill'=>'Skrill',
            'easypay'=>'Easypay',
            'sslcommerze'=>'Sslcommerze',
            'general'=>'General',
        );
    }

    // Review Previous Day notifications
    public  static function getCustomerReviewTime ($date) {
        $currentDate=strtotime(date('d M Y G.i.s'));
        $reviewDate = strtotime($date);
        $all = round(($currentDate - $reviewDate) / 60);
        $d = floor ($all / 1440);
        $h = floor (($all - $d * 1440) / 60);
        $m = $all - ($d * 1440) - ($h * 60);//Since you need just hours and mins
        return array(
            'd'=>$d,
            'h'=>$h,
            'm'=>$m
        );
    }
}
