<?php

/**
 * This is the model class for table "{{roles}}".
 *
 * The followings are the available columns in table '{{roles}}':
 * @property string $id
 * @property string $actionId
 * @property string $userId
 * @property integer $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property ContActions $action
 * @property User $crBy0
 * @property User $user
 */
class Roles extends CActiveRecord
{
	public $branchId;
	public $usernames;
	
	const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Roles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{roles}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userTypeId, userId, status', 'required'),
			array('userTypeId+userId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
		/*	array('rank', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),*/
			array('status', 'numerical', 'integerOnly'=>true),
			array('userTypeId', 'length', 'max'=>10),
			array('userId, crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt, branchId, usernames', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, userTypeId, userId, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'userType0' => array(self::BELONGS_TO, 'UserType', 'userTypeId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'user' => array(self::BELONGS_TO, 'User', 'userId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'branchId' => 'Organization',
			'id' => 'ID',
			'userTypeId' => 'Role',
			'userId' => 'User',
			'userId' => 'User',
			'status' => 'Status',
			'crAt' => 'Create At',
			'crBy' => 'Create By',
			'moAt' => 'Last Modify At',
			'moBy' => 'Last Modify By'
		);
	}
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=$this->crBy=(!empty(Yii::app()->user->id))?Yii::app()->user->id:User::getUserDefaultBranch(Branch::getDefaultBranch(Branch::STATUS_ACTIVE));
			}
			else{
				$this->moBy=$this->crBy=(!empty(Yii::app()->user->id))?Yii::app()->user->id:User::getUserDefaultBranch(Branch::getDefaultBranch(Branch::STATUS_ACTIVE));
				$this->moAt=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}
	
	public static function getUserTypeByIsSuperAdminWise($isSuperAdmin,$branchId)
	{
		$sql = "SELECT * FROM os_roles WHERE userId IN(SELECT id FROM os_user WHERE branchId=$branchId) AND 
				userTypeId NOT IN (SELECT id FROM os_user_type WHERE isSuperAdmin=$isSuperAdmin) GROUP BY userId";
	    
        return Roles::model()->findAllBySql($sql);
        
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria(array(
            'with'=>array(
                'user'=>array(
	                'alias'=>'c',
	                'together'=>true,
				),
            ),
        ));
		
		if(!Yii::app()->user->isAdmin)
			$criteria->addSearchCondition("c.branchId",Yii::app()->session['orgid']);
		
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('userTypeId',$this->userTypeId);
		$criteria->compare('c.username',$this->usernames,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('c.branchId',$this->branchId);
		
		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}