<?php

/**
 * This is the model class for table "{{payment_method}}".
 *
 * The followings are the available columns in table '{{payment_method}}':
 * @property string $id
 * @property string $title
 * @property string $apiShortName
 * @property string $image
 * @property string $description
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class PaymentMethod extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

    const API_GENERAL = 'general';
    const API_EASY_PAY = 'easypay';
    const API_PAYPAL = 'paypal';
    const API_SKRILL = 'skrill';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PaymentMethod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{payment_method}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, apiShortName,image, description, status', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			array('apiShortName', 'length', 'max'=>11),
			array('status', 'length', 'max'=>10),
			array('crBy, moBy', 'length', 'max'=>20),
            array('image', 'file',
                'types'=>'jpg, gif, png',
                'maxSize'=>1024 * 250, // 250kb
                'tooLarge'=>'The file was larger than 250KB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert'
            ),
			array('image, description, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, apiShortName,image, description, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'apiShortName' => 'Api Short Name',
            'image' => 'Image',
			'description' => 'Description',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as dropdown
    public static function getAllPaymentMethod($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'title');
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('apiShortName',$this->apiShortName,true);
        $criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}