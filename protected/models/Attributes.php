<?php

/**
 * This is the model class for table "{{attributes}}".
 *
 * The followings are the available columns in table '{{attributes}}':
 * @property string $id
 * @property integer $attTypeId
 * @property string $name
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property AttributesType $attType
 * @property ItemAttributes[] $itemAttributes
 */
class Attributes extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Attributes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{attributes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('attTypeId, name, status', 'required'),
            array('attTypeId+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('attTypeId', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('rank', 'length', 'max'=>10),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, attTypeId, name, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'attType' => array(self::BELONGS_TO, 'AttributesType', 'attTypeId'),
			//'itemAttributes' => array(self::HAS_MANY, 'ItemAttributes', 'attId'),
            'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'attTypeId' => 'Attributes Type',
			'name' => 'Name',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as dropdown
    public static function getAllAttributes($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    // get attributes by att type
    public static function getAllAttributesByTypes($attTypeId,$isActive)
    {
        return  CHtml::listData(self::model()->findAll(array('condition'=>'attTypeId=:attTypeId AND status=:status',
                                                             'params'=>array(':attTypeId'=>$attTypeId,':status'=>$isActive))), 'id', 'name');
    }

    // get attributes by att type with is Html
    public static function getAllAttributesByTypesIsHtml($attTypeId,$isActive)
    {
        return self::model()->findAll(array('condition'=>'attTypeId=:attTypeId AND status=:status','params'=>array(':attTypeId'=>$attTypeId,':status'=>$isActive)));
    }


    public static function getNameById($attId){
	    return Attributes::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$attId)));
    }

    public static function getNameName($name){
        return Attributes::model()->find(array('condition'=>'name=:name', 'params'=>array(':name'=>$name)));
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('attTypeId',$this->attTypeId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}