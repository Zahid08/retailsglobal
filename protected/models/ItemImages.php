<?php

/**
 * This is the model class for table "{{item_images}}".
 *
 * The followings are the available columns in table '{{item_images}}':
 * @property string $id
 * @property string $itemId
 * @property string $image
 * @property string $alt_text
 * @property string $small_image
 * @property string $medium_image
 * @property string $large_image
 * @property integer $sort_order
 * @property integer $status
 * @property integer $rank
 * @property string $crAt
 * @property string $crBy
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property Items $item
 */
class ItemImages extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemImages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{item_images}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('itemId, image', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('status, small_image, medium_image, large_image, sort_order, rank', 'numerical', 'integerOnly'=>true),
			array('itemId, crBy', 'length', 'max'=>20),
			array('image, alt_text, crAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, itemId, image, status, rank, crAt, crBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemId' => 'Item',
			'image' => 'Image',
			'alt_text' => 'Alt Text',
			'small_image' => 'Small Image',
			'medium_image' => 'Medium Image',
			'large_image' => 'Large Image',
			'sort_order' => 'Sort Order',
			'status' => 'Status',
			'rank' => 'Rank',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
                $this->status = self::STATUS_ACTIVE;
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllItemImages($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	
	// get attributes item wise  
    public static function getImagesByItems($itemId)
    {
        return self::model()->findAll(array('condition'=>'itemId=:itemId AND status=:status','params'=>array(':itemId'=>$itemId,':status'=>self::STATUS_ACTIVE)));
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}