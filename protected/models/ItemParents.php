<?php

/**
 * This is the model class for table "{{item_parents}}".
 *
 * The followings are the available columns in table '{{item_parents}}':
 * @property string $id
 * @property string $itemId
 * @property string $parentId
 * @property double $qty
 * @property integer $status
 * @property integer $rank
 * @property string $crAt
 * @property string $crBy
 *
 * The followings are the available model relations:
 * @property Items $parent
 * @property Items $item
 * @property User $crBy0
 */
class ItemParents extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemParents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{item_parents}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('itemId, parentId, qty', 'required'),
            //array('name', 'unique'),
            array('itemId+parentId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('status, rank', 'numerical', 'integerOnly'=>true),
			array('qty', 'numerical'),
			array('itemId, parentId, crBy', 'length', 'max'=>20),
			array('crAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, itemId, parentId, qty, status, rank, crAt, crBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'Items', 'parentId'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemId' => 'Item',
			'parentId' => 'Parent',
			'qty' => 'Qty',
			'status' => 'Status',
			'rank' => 'Rank',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
                $this->status = self::STATUS_ACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by itemId (child)
    public static function getAllItemParentsByChield($itemId,$isActive)
	{      
		 return self::model()->findAll(array('condition'=>'itemId=:itemId AND status=:status', 'params'=>array(':itemId'=>$itemId,':status'=>$isActive)));
	}

    // get all record by itemId (parent)
    public static function getAllItemChildByParent($parentId,$isActive)
    {
        return self::model()->findAll(array('condition'=>'parentId=:parentId AND status=:status', 'params'=>array(':parentId'=>$parentId,':status'=>$isActive)));
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('parentId',$this->parentId,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('status',$this->status);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}