<?php
/**
 * This is the model class for table "{{items}}".
 *
 * The followings are the available columns in table '{{items}}':
 * @property string $id
 * @property string $itemCode
 * @property string $barCode
 * @property string $itemName
 * @property string $itemName_bd
 * @property double $costPrice
 * @property double $sellPrice
 * @property string $catId
 * @property string $brandId
 * @property string $supplierId
 * @property string $taxId
 * @property string $negativeStock
 * @property string $isWeighted
 * @property integer $caseCount
 * @property integer $isEcommerce
 * @property integer $isFeatured
 * @property integer $isSales
 * @property integer $isParent
 * @property string $specification
 * @property string $specification_bd
 * @property string $shipping_payment
 * @property string $shipping_payment_bd
 * @property string $description
 * @property string $description_bd
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Brand $brand
 * @property User $crBy0
 * @property User $moBy0
 * @property Subcategory $subcat
 * @property Supplier $supplier
 * @property Tax $tax
 * @property ItemsPrice[] $itemsPrices
 * @property PoDetails[] $poDetails
 * @property Stock[] $stocks
 */
class Items extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

    const IS_ECOMMERCE=1;
    const IS_NOT_ECOMMERCE=2;

    const IS_FEATURED=1;
    const IS_NOT_FEATURED=2;

    const IS_SALES=1;
    const IS_NOT_SALES=2;

    const IS_PARENTS=1;
    const IS_NOT_PARENTS=2;
	
	const IS_WEIGHTED = "yes";
	const NOT_WEIGHTED = "no";
    const DEFAULT_CASE_COUNT = 1;
    const DEFAULT_BARCODE = 0;
	
	public $deptId;
	public $subdeptId;
    public $xlsFile;
    public $image;
    public $item_quantity;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	array('catId,itemCode,itemName, costPrice, sellPrice, brandId, supplierId, taxId,isWeighted,caseCount,status,variation_type', 'required'),
        	array('subdeptId, deptId', 'safe'),
        	array('itemCode', 'unique'), // barCode
        	//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('caseCount,costPrice, sellPrice,isEcommerce,isFeatured, isSales,isParent,isNew,isSpecial', 'numerical'),
			array('barCode', 'validateStarting'), // custom function
			array('itemCode', 'length', 'max'=>150),
			array('barCode', 'length', 'max'=>150),
			array('itemName, itemName_bd', 'length', 'max'=>200),
			array('catId, crBy, moBy', 'length', 'max'=>20),
			array('brandId, supplierId, taxId', 'length', 'max'=>10),
			array('negativeStock, isWeighted', 'length', 'max'=>3),
			array('status', 'length', 'max'=>11),
			array('xlsFile, isEcommerce,isNew,isSpecial specification, specification_bd, shipping_payment, shipping_payment_bd, description, description_bd, crAt, moAt, deptId, slugName, attributes_color, attributes_size , catId, item_quantity, variation', 'safe'),

            array('xlsFile', 'file',
                'types'=>'csv',
                'maxSize'=>1024 * 1024 * 10, // 10MB
                'tooLarge'=>'The file was larger than 10MB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert, update'
            ),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, itemCode, barCode, itemName, costPrice, sellPrice, catId, brandId, supplierId, taxId, negativeStock,
			       isWeighted,caseCount,isEcommerce,isFeatured, isSales,isParent, specification, shipping_payment, description, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'damageWastages' => array(self::HAS_MANY, 'DamageWastage', 'itemId'),
			'inventories' => array(self::HAS_MANY, 'Inventory', 'itemId'),
            'itemAttributes' => array(self::HAS_MANY, 'ItemAttributes', 'itemId'),
			'itemImages' => array(self::HAS_MANY, 'ItemImages', 'itemId'),
            'cat' => array(self::BELONGS_TO, 'Category', 'catId'),
            'brand' => array(self::BELONGS_TO, 'Brand', 'brandId'),
            'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
            'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplierId'),
            'tax' => array(self::BELONGS_TO, 'Tax', 'taxId'),
            'itemsPrices' => array(self::HAS_MANY, 'ItemsPrice', 'itemId'),
            'poDetails' => array(self::HAS_MANY, 'PoDetails', 'itemId'),
            'poffers' => array(self::HAS_MANY, 'Poffers', 'itemId'),
			'poReturns' => array(self::HAS_MANY, 'PoReturns', 'itemId'),
            'salesDetails' => array(self::HAS_MANY, 'SalesDetails', 'itemId'),
			'salesReturns' => array(self::HAS_MANY, 'SalesReturn', 'itemId'),
            'stocks' => array(self::HAS_MANY, 'Stock', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'variation_type'=>'Type',
			'item_quantity'=>'Quantity',
			'deptId'=>'Category',
			'subdeptId'=>'Sub Category',
			'catId'=>'Category',
			'itemCode' => 'Item Code',
			'barCode' => 'Bar Code',
			'itemName' => 'Item Name',
			'itemName_bd' => 'Item Name (বাংলা)',
			'costPrice' => 'Cost Price',
			'sellPrice' => 'Sell Price',
			'catId' => 'Child Sub Category',
			'brandId' => 'Brand',
			'supplierId' => 'Supplier',
			'taxId' => 'Tax',
			'negativeStock' => 'Negative Stock',
			'isWeighted' => 'Is Weighted',
			'caseCount' => 'Case Count',
            'isEcommerce' => 'Is Ecommerce',
            'isFeatured' => 'Is Featured',
            'isSales' => 'Is Sales',
            'isParent'=>'Is Parent',
            'specification' => 'Specification',
            'specification_bd' => 'Specification (বাংলা)',
            'shipping_payment' => 'Shipping & Payment',
            'shipping_payment_bd' => 'Shipping & Payment (বাংলা)',
            'description' => 'Description',
            'description_bd' => 'Description (বাংলা)',
            'attributes_color' => 'Color',
            'attributes_size' => 'Size',
            'xlsFile'=>'Import Csv File',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
				$this->negativeStock = 'yes';
			}
			else
			{
				$this->moAt=date("Y-m-d H:i:s");
				$this->moBy=Yii::app()->user->id;
			}
			return true;
		}
		else
			return false;
	}
	// validateStarting for barcoode
	public function validateStarting($attribute)
	{    
		$pos = strpos($this->$attribute, '99');		
		if($pos===0)
			$this->addError($attribute,Yii::t('yii', 'You can,t input a barcode starting with 99', array('{attribute}'=>$this->getAttributeLabel($attribute))));
	}

    // item count by brand
    public static function getItemsCountByBrand($brandId,$catId)
    {
        $totalItems = 0;
        $db = Yii::app()->db;
        $sql = "SELECT SUM(total) AS itemTotal FROM(
                  SELECT COUNT(id) AS total FROM pos_items WHERE brandId=".$brandId." AND catId=".$catId." AND isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY itemName
                ) AS t";

        $res = $db->createCommand($sql)->queryRow();
        if(!empty($res['itemTotal'])) $totalItems = $res['itemTotal'];
        return $totalItems;
    }

    //-----------get all record by status as dropdown--------//
    public static function getAllItems($isActive)
	{      
		 return CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'itemCode');
	}
	
	// get itemwise standard profit
	public static function getStandardProfit($itemId)
	{    
		 $sp = 0;
		 // price change/not
		 $sqlsells = "SELECT costPrice,sellPrice FROM pos_items_price WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".ItemsPrice::STATUS_ACTIVE." ORDER BY changeDate ASC LIMIT 0,1";
		 $ressells = Yii::app()->db->createCommand($sqlsells)->queryRow();
		 if(!empty($ressells['costPrice']) && !empty($ressells['sellPrice'])) 
		 {
			 $sp  = (($ressells['sellPrice']-$ressells['costPrice'])*100)/$ressells['sellPrice'];	
		 }
		 else 
		 {
			 // item model as usual
			 $itemModel = self::model()->findByPk($itemId);
			 if(!empty($itemModel)) 
			 {
				 $sp  = (($itemModel->sellPrice-$itemModel->costPrice)*100)/$itemModel->sellPrice;	
			 }	 
		 }
		 return $sp;
	}

    // is new item for e-commerce
    public static function getIsNewItem($itemId)
    {
        $sql = "SELECT id FROM pos_items WHERE isEcommerce=".Items::IS_ECOMMERCE." AND crAt>=DATE_SUB(NOW(),INTERVAL 30 DAY)
                  AND id=".$itemId." AND status=".Items::STATUS_ACTIVE." ORDER BY crAt DESC";
        $model = Items::model()->findBySql($sql);
        if(!empty($model)) return $model->id;
    }

    // is sale item for e-commerce
    public static function getIsSaleItem($itemId)
    {
        $model = Items::model()->find(array('condition'=>'isEcommerce=:isEcommerce AND isSales=:isSales AND id=:id AND status=:status',
                                            'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':isSales'=>Items::IS_SALES,':id'=>$itemId,':status'=>Items::STATUS_ACTIVE),
                                            'order'=>'crAt DESC',
                ));
        if(!empty($model)) return $model->id;
    }

    // is best sale item for e-commerce
    public static function getIsBestSaleItem($itemId)
    {
        $sql = "SELECT i.* FROM pos_items i,pos_sales_details s WHERE s.itemId=i.id AND i.status=".Items::STATUS_ACTIVE." AND i.isEcommerce=".Items::IS_ECOMMERCE."
                AND i.id=".$itemId." GROUP BY s.itemId ORDER BY SUM(s.qty) DESC,s.salesDate DESC";
        $model = Items::model()->findBySql($sql);
        if(!empty($model)) return $model->id;
    }

    // is return item or not
    public static function getIsReturnItem($salesId,$itemId)
    {
        $model = SalesReturn::model()->find(array('condition'=>'salesId=:salesId AND itemId=:itemId AND status=:status',
                                                  'params'=>array(':salesId'=>$salesId,':itemId'=>$itemId,':status'=>SalesReturn::STATUS_ACTIVE),
                                                  'order'=>'crAt DESC',
                ));
        if(!empty($model)) return $model->id;
    }

    // item wise offer/cut after price
    public static function getItemWiseOfferPrice($itemId)
    {
        $offerPrice =  0;
        $date = date("Y-m-d");
        $branchId = (!empty(Yii::app()->session['branchId']))?Yii::app()->session['branchId']:Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
        $sqlOffer = "SELECT * FROM pos_poffers WHERE itemId=".$itemId." AND branchId=".$branchId." AND startDate<='".$date."' AND endDate>='".$date."'";
        $offerModel = Poffers::model()->findBySql($sqlOffer);
        if(!empty($offerModel))
        {
            if($offerModel->item->tax->taxRate>0)
                $offerPrice = round(($offerModel->item->sellPrice-($offerModel->package->discount*$offerModel->item->sellPrice)/100)) + round(($offerModel->item->sellPrice*$offerModel->item->tax->taxRate)/100);
            else $offerPrice = round(($offerModel->item->sellPrice-($offerModel->package->discount*$offerModel->item->sellPrice)/100));
        }
        return $offerPrice;
    }

    // item wise offer/cut ori price
    public static function getItemWiseOfferOriPrice($itemId)
    {
        $offerPrice = 0;
        $date = date("Y-m-d");
        $branchId = (!empty(Yii::app()->session['branchId']))?Yii::app()->session['branchId']:Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
        $sqlOffer = "SELECT * FROM pos_poffers WHERE itemId=".$itemId." AND branchId=".$branchId." AND startDate<='".$date."' AND endDate>='".$date."'";
        $offerModel = Poffers::model()->findBySql($sqlOffer);
        if(!empty($offerModel))
        {
            $offerPrice = round(($offerModel->package->discount*$offerModel->item->sellPrice)/100);
        }
        return $offerPrice;
    }

    // get item price with vat
    public static function getItemPriceWithVat($id)
    {
        $price = 0;
        if(!empty($id))
        {
            $model = self::model()->findByPk($id);
            if($model->tax->taxRate>0)
                $price = round($model->sellPrice+(($model->sellPrice*$model->tax->taxRate)/100));
            else $price = $model->sellPrice;
        }
        return $price;
    }

    // get item price with vat original
    public static function getItemPriceWithVatOri($id)
    {
        $price = 0;
        if(!empty($id))
        {
            $model = self::model()->findByPk($id);
            if($model->tax->taxRate>0) $price = round(($model->sellPrice*$model->tax->taxRate)/100);
        }
        return $price;
    }


    /*
     * get tax rate
     */

    public function getTaxRateById($taxId){
        $tax = Tax::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$taxId)));
        if(!empty($tax)){
            return $tax->taxRate;
        }
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		/*$criteria->with = array(
			'brand'=>array(
				'alias'=>'b',
				'together'=>true,
			),
		);*/
		if(isset(Yii::app()->session['supplierId']) && !empty(Yii::app()->session['supplierId']))
		{
			//$criteria->addSearchCondition("supplierId",Yii::app()->session['supplierId']);
			$criteria->condition = 'supplierId=:supplierId';


			$criteria->params = array('supplierId'=>Yii::app()->session['supplierId']);
		}

        $criteria->addCondition('variation=0');

        //$criteria->addCondition("variation",'');
		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemCode',$this->itemCode,true);
		$criteria->compare('barCode',$this->barCode,true);
		$criteria->compare('itemName',$this->itemName,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('sellPrice',$this->sellPrice);
		$criteria->compare('catId',$this->catId);
		$criteria->compare('brandId',$this->brandId);
		$criteria->compare('supplierId',$this->supplierId);
		$criteria->compare('taxId',$this->taxId);
		$criteria->compare('negativeStock',$this->negativeStock,true);
		$criteria->compare('isWeighted',$this->isWeighted,true);
		$criteria->compare('caseCount',$this->caseCount);
        $criteria->compare('isEcommerce',$this->isEcommerce);
        $criteria->compare('isFeatured',$this->isFeatured);
        $criteria->compare('isSales',$this->isSales);
        $criteria->compare('isParent',$this->isParent);
        $criteria->compare('specification',$this->specification,true);
        $criteria->compare('shipping_payment',$this->shipping_payment,true);
        $criteria->compare('description',$this->description,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);
		$criteria->order = 'itemCode';

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}


	public static function ItemsSlug($itemName, $oldSlug = null){

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $itemName);
        $slug = trim(strtolower($slug));

        if(!empty($oldSlug)){
            if($slug == $oldSlug){
                return $slug;
            }
        }

        $sql = "SELECT * FROM pos_items WHERE slugName='".$slug."' ";
        $items = Items::model()->findAllBySql($sql);

        if(empty($items)){
            return $slug;
        }

        $uniqueSlug = $slug;
        $iteration = 1;
        $isSlugExit= true;

        while ($isSlugExit) {
            $uniqueSlug = $slug . '-' . $iteration;

            $sql = "SELECT * FROM pos_items WHERE slugName='".$uniqueSlug."' ";
            $items = Items::model()->findAllBySql($sql);

            if(empty($items)){
                $isSlugExit = false;
            }

            $iteration++;
        }

        return $uniqueSlug;

    }


}