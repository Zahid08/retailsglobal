<?php

/**
 * This is the model class for table "{{sales_details}}".
 *
 * The followings are the available columns in table '{{sales_details}}':
 * @property string $id
 * @property string $salesId
 * @property string $itemId
 * @property double $qty
 * @property string $salesDate
 * @property double $costPrice
 * @property double $salesPrice
 * @property double $discountPrice
 * @property double $vatPrice
 * @property double $shippingPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property Items $item
 * @property User $moBy0
 * @property SalesInvoice $sales
 */
class SalesDetails extends CActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    const STATUS_PROCESSING=3;
    const STATUS_CANCEL=4;
    const STATUS_RETURN=5;
    const STATUS_DAMAGE=6; //new

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SalesDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_details}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('salesId, itemId, qty, costPrice,salesPrice, discountPrice, vatPrice', 'required'),
            array('attributes_color', 'safe'),
            //array('salesId', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('qty, costPrice,salesPrice, discountPrice, vatPrice,shippingPrice', 'numerical'),
			array('salesId, itemId, crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('salesDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, salesId, itemId, qty,costPrice,salesDate, salesPrice, discountPrice, vatPrice,shippingPrice, attributes_color, attributes_size, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'sales' => array(self::BELONGS_TO, 'SalesInvoice', 'salesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'salesId' => 'Sales',
			'attributes_color' => 'Attributes_color',
			'itemId' => 'Item',
			'qty' => 'Qty',
			'costPrice' => 'Cost Price',
			'salesDate' => 'Sales Date',
			'salesPrice' => 'Sales Price',
			'discountPrice' => 'Discount Price',
			'vatPrice' => 'Vat Price',
            'shippingPrice' => 'Shipping Price',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=$this->salesDate = date("Y-m-d H:i:s");
                if(Yii::app()->urlManager->parseUrl(Yii::app()->request)=='goodReceiveNote/directGrn')
                    $this->crBy = User::getDirectGrnUser();
                else $this->crBy=Yii::app()->user->id;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllSalesDetails($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	// get itemwise 15 days movements
	public static function get15DaysMovement($itemId)
	{    
		 $sells =  0;
		 $db = Yii::app()->db; 
		 
		 // sells calculations
		 $sqlsells = "SELECT SUM(qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']." 
		              AND s.itemId=".$itemId." AND s.salesDate >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)";
		 $ressells = $db->createCommand($sqlsells)->queryRow();
		 if(!empty($ressells['qty'])) $sells    =  $ressells['qty'];	
		 
		 return round($sells,2);
	}
	
	// get unique vat wise sales prices
	public static function getuniqueVatWiseSells($salesId,$tax)
	{    
		 $netTotal =  0;
		 $db = Yii::app()->db; 
		 
		 // sells calculations
		 $sqlsells = "SELECT SUM(qty*salesPrice) AS netTotal FROM pos_sales_details WHERE salesId=".$salesId." AND itemId IN
					 (
					   SELECT id FROM pos_items WHERE taxId=(SELECT id FROM pos_tax WHERE taxRate=".$tax.")
					 )";
		 $ressells = $db->createCommand($sqlsells)->queryRow();
		 if(!empty($ressells['netTotal'])) $netTotal   =  $ressells['netTotal'];	
		 
		 return round($netTotal,2);
	}

    // get all unique item name by invoice ID
    public static function getAllItemNameByInvoice($id)
    {
        $str = '';
        $model = self::model()->findAll(array('condition'=>'salesId=:salesId', 'params'=>array(':salesId'=>$id)));
        if(!empty($model))
        {
            foreach($model as $key=>$data)
            {
                if(count($model)==$key+1) $str.=$data->item->itemName;
                else $str.=$data->item->itemName.', ';
            }
        }
        return $str;
    }

    // get item & invoice wise special discount
    public static function getInvoiceItemWiseSpDiscount($salesId,$itemQty)
    {
        $spDiscount =  0;
        $invModel = SalesInvoice::model()->findByPk($salesId);
        if($invModel->spDiscount>0) $spDiscount = round((($invModel->spDiscount/($invModel->totalQty+$invModel->totalWeight))*$itemQty) ,2);
        return $spDiscount;
    }

    // get item & invoice wise instant discount
    public static function getInvoiceItemWiseInstantDiscount($salesId,$itemQty)
    {
        $instantDiscount =  0;
        $invModel = SalesInvoice::model()->findByPk($salesId);
        if($invModel->instantDiscount>0) $instantDiscount = round((($invModel->instantDiscount/($invModel->totalQty+$invModel->totalWeight))*$itemQty) ,2);
        return $instantDiscount;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('salesId',$this->salesId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('salesDate',$this->salesDate,true);
	    $criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('salesPrice',$this->salesPrice);
		$criteria->compare('discountPrice',$this->discountPrice);
		$criteria->compare('vatPrice',$this->vatPrice);
        $criteria->compare('shippingPrice',$this->shippingPrice);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}