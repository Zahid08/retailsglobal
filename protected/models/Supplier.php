<?php

/**
 * This is the model class for table "{{supplier}}".
 *
 * The followings are the available columns in table '{{supplier}}':
 * @property string $id
 * @property string $suppId
 * @property string $name
 * @property string $name_bd
 * @property string $address
 * @property string $city
 * @property string $email
 * @property string $phone
 * @property string $crType
 * @property integer $creditDay
 * @property integer $consignmentDay
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property Items[] $items
 * @property User $crBy0
 * @property User $moBy0
 */
class Supplier extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

	const CREDIT_SUPP = 'credit';
	const CONSIG_SUPP = 'consignment';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Supplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{supplier}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('suppId,name, address, city, email, phone, crType, creditDay, consignmentDay, status', 'required'),
            array('suppId,email,phone', 'unique'),
		    array('email', 'email'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('creditDay, consignmentDay', 'numerical', 'integerOnly'=>true),
			array('name, name_bd', 'length', 'max'=>50),
			array('address', 'length', 'max'=>250),
			array('suppId,city, email, phone', 'length', 'max'=>150),
			array('crType, status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('rank', 'length', 'max'=>10),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,suppId,name, address, city, email, phone, crType, creditDay, consignmentDay, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items' => array(self::HAS_MANY, 'Items', 'supplierId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'suppId' => 'Supplier ID',
			'name' => 'Name',
			'name_bd' => 'Name (বাংলা)',
			'address' => 'Address',
			'city' => 'City',
			'email' => 'Email',
			'phone' => 'Phone',
			'crType' => 'Credit Type',
			'creditDay' => 'Credit Day',
			'consignmentDay' => 'Consignment Day',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
                $this->crBy=$this->crBy=(!empty(Yii::app()->user->id))?Yii::app()->user->id:User::getUserDefaultBranch(Branch::getDefaultBranch(Branch::STATUS_ACTIVE));
			}
			return true;
		}
		else
			return false;
	}
	
	// get supplier combined info
	protected function getSupIdFullName()
	{
		return $this->suppId.' ('.$this->name.')';
	}
    
    // get all record by status as dropdown
    public static function getAllSupplier($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'supIdFullName');      
	}
	
	// get all record by status as dropdown by type
    public static function getAllSupplierByType($type)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status and crType=:crType', 'params'=>array(':status'=>Supplier::STATUS_ACTIVE, ':crType'=>$type))), 'id', 'supIdFullName');      
	}

    // Count Pending Supplier
    public static function countPendingSupplier()
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('status'=>self::STATUS_INACTIVE));
        return $countData;
    }

	// get supplier balance (deposit-widraw)
	public static function getSupplierBalance($suppId)
	{    
		 $deposit = $widraw = 0;
		 $db = Yii::app()->db;  
		 
		 // deposit calculations
		 $sqldeposit = "SELECT SUM(amount) AS deposit FROM pos_supplier_deposit WHERE supId=".$suppId." AND branchId=".Yii::app()->session['branchId'];
		 $resdeposit =	$db->createCommand($sqldeposit)->queryRow();
		 if(!empty($resdeposit['deposit'])) $deposit = $resdeposit['deposit'];	
		 
		 // widraw calculations
		 $sqlwidraw = "SELECT SUM(amount) AS widraw FROM pos_supplier_widraw WHERE supId=".$suppId." AND branchId=".Yii::app()->session['branchId'];
		 $reswidraw =	$db->createCommand($sqlwidraw)->queryRow();
		 if(!empty($reswidraw['widraw'])) $widraw  =  $reswidraw['widraw'];	
		 
		 return ($deposit - $widraw);
	}
	
	// get supplier balance (sells-widraw)
	public static function getSupplierBalanceConsig($suppId)
	{    
		 $deposit = $widraw = 0;
		 $db = Yii::app()->db;  
		 
		 // sales calculations
		 $sqldeposit = "SELECT SUM(s.qty*s.costPrice) AS deposit FROM pos_sales_invoice i, pos_sales_details s, pos_items t WHERE i.id=s.salesId AND s.itemId=t.id AND i.branchId=".Yii::app()->session['branchId']."
						AND s.itemId IN(SELECT id FROM pos_items WHERE supplierId=".$suppId.")";
		 $resdeposit =	$db->createCommand($sqldeposit)->queryRow();
		 if(!empty($resdeposit['deposit'])) $deposit 	 =  $resdeposit['deposit'];	
		 
		 
		 // widraw calculations
		 $sqlwidraw = "SELECT SUM(amount) AS widraw FROM pos_supplier_widraw WHERE supId=".$suppId." AND branchId=".Yii::app()->session['branchId'];
		 $reswidraw =	$db->createCommand($sqlwidraw)->queryRow();
		 if(!empty($reswidraw['widraw'])) $widraw  =  $reswidraw['widraw'];	
		 
		 return ($deposit - $widraw);
	}
	
	// get supplier consigSales total
	public static function getSupplierConsigSales($suppId,$consigday)
	{    
		 $sales = 0;
		 $db = Yii::app()->db;  
		 
		 // sales calculations
		 $sqldeposit = "SELECT SUM(s.qty*s.costPrice) AS deposit FROM pos_sales_invoice i, pos_sales_details s, pos_items t WHERE i.id=s.salesId AND s.itemId=t.id AND i.branchId=".Yii::app()->session['branchId']."
						AND s.itemId IN(SELECT id FROM pos_items WHERE supplierId=".$suppId.") AND s.salesDate >= DATE_SUB(CURDATE(), INTERVAL ".$consigday." DAY)";
		 $resdeposit =	$db->createCommand($sqldeposit)->queryRow();
		 if(!empty($resdeposit['deposit'])) $sales 	=  $resdeposit['deposit'];	
		 
		 return $sales;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->order = 'crAt DESC';
		$criteria->compare('id',$this->id,true);
        $criteria->compare('suppId',$this->suppId,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('crType',$this->crType,true);
		$criteria->compare('creditDay',$this->creditDay);
		$criteria->compare('consignmentDay',$this->consignmentDay);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}