<?php

/**
 * This is the model class for table "{{coupons}}".
 *
 * The followings are the available columns in table '{{coupons}}':
 * @property string $id
 * @property string $couponNo
 * @property string $couponAmount
 * @property string $couponStartDate
 * @property string $couponEndDate
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class Coupons extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

    const DEFAULT_COUPON=0;
    const DEFAULT_QTY = 1;
	public $couponQty;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Coupons the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{coupons}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	//array('id, couponNo, couponAmount, couponStartDate, couponEndDate, status, crAt, crBy, moAt, moBy, rank', 'required'),
        	array('couponQty,couponAmount, couponStartDate, couponEndDate, status', 'required'),
        	array('couponNo', 'unique'),
        	//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('couponNo', 'length', 'max'=>50),
			//array('couponQty', 'length', 'max'=>50),
			array('couponAmount, rank', 'length', 'max'=>10),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('couponQty,couponStartDate, couponEndDate, crAt, moAt,subdeptId', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,couponQty ,couponNo, couponAmount, couponStartDate, couponEndDate, status, crAt, crBy, moAt, moBy, rank,subdeptId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'subdept' => array(self::BELONGS_TO, 'Subdepartment', 'subdeptId'),
            'salesInvoices' => array(self::HAS_MANY, 'SalesInvoice', 'couponId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'couponQty' => 'Quantity',
			'subdeptId' => 'Sub Category',
			'couponNo' => 'Coupon No.',
			'couponAmount' => 'Amount',
			'couponStartDate' => 'Start Date',
			'couponEndDate' => 'End Date',
			'status' => 'Status',
			'crAt' => 'Created Date',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
            else
            {
                $this->moAt=date("Y-m-d H:i:s");
                $this->moBy=Yii::app()->user->id;
            }
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllCoupons($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('couponNo',$this->couponNo,true);
		$criteria->compare('couponAmount',$this->couponAmount);
		$criteria->compare('couponStartDate',$this->couponStartDate,true);
		$criteria->compare('couponEndDate',$this->couponEndDate,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}