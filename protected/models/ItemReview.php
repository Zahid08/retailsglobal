<?php

/**
 * This is the model class for table "{{item_review}}".
 *
 * The followings are the available columns in table '{{item_review}}':
 * @property string $id
 * @property string $itemId
 * @property integer $rating
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property string $crAt
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Items $item
 */
class ItemReview extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
    const CREATE_BY=1;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemReview the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{item_review}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('itemId, rating, name, email, comment', 'required'),
            array('email', 'email'),
            array('itemId+email', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('rating', 'required'),
			array('rating', 'numerical', 'integerOnly'=>true),
			array('itemId', 'length', 'max'=>20),
			array('name, email', 'length', 'max'=>150),
			array('status', 'length', 'max'=>11),
			array('comment, crAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, itemId, rating, name, email, comment, crAt, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemId' => 'Item',
			'rating' => 'Rating',
			'name' => 'Name',
			'email' => 'Email',
			'comment' => 'Comment',
			'crAt' => 'Cr At',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->status=self::STATUS_INACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllItemReview($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    //Count Review
    public static function countReviewByItemId($itemId)
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('itemId'=>$itemId,'status'=>self::STATUS_ACTIVE));
        return $countData;
    }
    //Count Review Pending
    public static function countReviewAllPending()
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('status'=>self::STATUS_INACTIVE));
        return $countData;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->order='crAt DESC';
		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}