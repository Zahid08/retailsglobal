<?php
/**
 * This is the model class for table "{{cont_actions}}".
 *
 * The followings are the available columns in table '{{cont_actions}}':
 * @property string $id
 * @property string $contId
 * @property string $name
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $description
 * The followings are the available model relations:
 * @property Controllers $cont
 * @property User $crBy0
 * @property User $moBy0
 * @property Roles[] $roles
 */
class ContActions extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	public $theme;
	/**
	 * Returns the static model of the specified AR class.
	 * @return ContActions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{cont_actions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contId,name,layout,status', 'required'),
			/*array('contId+name', 'application.extensions.uniqueMultiColumnValidator'),*/
			array('contId', 'length', 'max'=>10),
			array('name', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt, description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contId, name, status, crAt, crBy, moAt, moBy, description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cont' => array(self::BELONGS_TO, 'Controllers', 'contId'),
			'layouts' => array(self::BELONGS_TO, 'Layouts', 'layout'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'roles' => array(self::HAS_MANY, 'Roles', 'actionId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contId' => 'Controller',
			'name' => 'Action',
			'theme' => 'Theme',
			'layout'=>'Layout',
			'description'=>'Description',
			'status' => 'Status',
			'crAt' => 'Create At',
			'crBy' => 'Create By',
			'moAt' => 'Last Modify At',
			'moBy' => 'Last Modify By',
		);
	}
	/**
	 * @return string the URL that shows the detail of the organisation
	 */
	public function getUrl()
	{
		return Yii::app()->createUrl('contActions/view', array(
			'id'=>$this->id,
			'name'=>$this->name,
		));
	}
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
			}
			else{
				$this->moBy=Yii::app()->user->id;
				$this->moAt=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}
	/**
	 * added by habib 26/07/2011
	 * find all Action
	 * @param $status
	 * @return array names
	 */
	
	public static function getAllcontAction()
    {      
         return  CHtml::listData(self::model()->findAll(), 'id', 'name');      
    }
    	
	/**
	 * added by habib 26/07/2011
	 * find all active Actions
	 * @param $status
	 * @return array names
	 */
	
	public static function getcontAction($status)
    {      
         return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$status))), 'id', 'name');      
    }
    
	/**
	 * added by habib 04/07/2011
	 * find all active Actions by Controllers
	 * @param $status,
	 * @param $countryId
	 * @return array names
	 */
	public static function getactionsByController($status,$contId)
    {      
         return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status and contId=:contId', 'params'=>array(':status'=>$status,':contId'=>$contId))), 'id', 'name');      
    }
	// action description get
	public static function getactionsDesc($id)
    {      
          return  self::model()->findByPk($id)->description;  
    }
    
	/* added by badal 23/10/2013
	 * @param $contId
	 * @param $name
	 * @return array */
	
	public static function getactionByContName($contId,$name)
    {      
         return  self::model()->find(array('condition'=>'contId=:contId and name=:name',
										   'params'=>array(':contId'=>$contId,
										   ':name'=>$name
										)));    
    }
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('contId',$this->contId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('theme',$this->theme,true);
		$criteria->compare('layout',$this->layout,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('description',$this->description,true);
		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}