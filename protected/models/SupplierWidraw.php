<?php

/**
 * This is the model class for table "{{supplier_widraw}}".
 *
 * The followings are the available columns in table '{{supplier_widraw}}':
 * @property string $id
 * @property string $branchId
 * @property string $bankId
 * @property string $supId
 * @property string $pgrnNo
 * @property integer $type
 * @property double $amount
 * @property string $remarks
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 * @property Supplier $sup
 */
class SupplierWidraw extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const GRN_WIDRAW=1;
	const PO_RETURNS=2;

    const IS_BANK = 1;
    const IS_CASH = 2;

    public $isBank;
    public $bankAcNo;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierWidraw the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{supplier_widraw}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('supId,pgrnNo,type, amount', 'required'),
        	//array('name', 'unique'),
        	//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('amount', 'numerical'),
			array('branchId,supId, rank', 'length', 'max'=>10),
			array('pgrnNo, remarks', 'length', 'max'=>250),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt,isBank,bankId,bankAcNo', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,branchId,bankId,supId,pgrnNo, type, amount, remarks, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'bank' => array(self::BELONGS_TO, 'Bank', 'bankId'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'sup' => array(self::BELONGS_TO, 'Supplier', 'supId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchId' => 'Branch',
            'bankId' => 'Bank',
            'bankAcNo' => 'Account No',
			'supId' => 'Supplier',
			'pgrnNo' => 'GRN No.',
			'type' => 'Type',
			'amount' => 'Amount',
			'remarks' => 'Remarks',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{		
				$this->branchId = Yii::app()->SESSION['branchId'];
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
				$this->status=self::STATUS_ACTIVE;
				$this->rank = 5;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllSupplierWidraw($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('branchId',$this->branchId,true);
        $criteria->compare('bankId',$this->bankId,true);
		$criteria->compare('supId',$this->supId,true);
    	$criteria->compare('pgrnNo',$this->pgrnNo,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}