<?php
/**
 * This is the model class for table "{{items}}".
 *
 * The followings are the available columns in table '{{items}}':
 * @property string $id
 * @property string $itemCode
 * @property string $barCode
 * @property string $itemName
 * @property string $itemName_bd
 * @property double $costPrice
 * @property double $sellPrice
 * @property string $catId
 * @property string $brandId
 * @property string $supplierId
 * @property string $taxId
 * @property string $negativeStock
 * @property string $isWeighted
 * @property integer $caseCount
 * @property integer $isEcommerce
 * @property integer $isFeatured
 * @property integer $isSales
 * @property integer $isParent
 * @property string $specification
 * @property string $specification_bd
 * @property string $shipping_payment
 * @property string $shipping_payment_bd
 * @property string $description
 * @property string $description_bd
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Brand $brand
 * @property User $crBy0
 * @property User $moBy0
 * @property Subcategory $subcat
 * @property Supplier $supplier
 * @property Tax $tax
 * @property ItemsPrice[] $itemsPrices
 * @property PoDetails[] $poDetails
 * @property Stock[] $stocks
 */
class ItemsVariation extends CActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Items the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{items_variation}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('color, size, quantity, sell_price, cost_price', 'required'),
            array('id,image, item_id,bar_code,subdeptId', 'safe'),
            array('item_code', 'unique'), // barCode
            array('image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'),

        );
    }

    public function type($item_id, $variation_type){

        if($variation_type == 2){
            echo '<a style="width:56px;background: #48c3e9;color: #fff;padding: 4px 10px;display: inline-block;border-radius: 3px !important;text-decoration: none;" href="'.Yii::app()->baseUrl.'/items/variation?item_id='.$item_id.'">Variation</a>';
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'subdeptId' => 'SubdeptId',
            'item_id' => 'Item ID',
            'item_code' => 'Item Code',
            'bar_code' => 'Bar Code',
            'image' => 'Image',
            'color' => 'Color',
            'size' => 'Size',
            'quantity' => 'Quantity',
        );
    }

    public function variationAttributesById($id){
       $attributes = Attributes::model()->find(array('condition'=>'id=:id',
            'params'=>array(':id'=>$id)));

       if(!empty($attributes)){
            return $attributes->name;
       }
    }

}