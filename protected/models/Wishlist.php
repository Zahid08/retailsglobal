<?php

/**
 * This is the model class for table "{{wishlist}}".
 *
 * The followings are the available columns in table '{{wishlist}}':
 * @property string $id
 * @property string $itemId
 * @property string $custId
 * @property string $status
 * @property string $crAt
 * @property string $moAt
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property Customer $cust
 * @property Items $item
 */
class Wishlist extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Wishlist the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{wishlist}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('itemId, custId', 'required'),
            array('itemId+custId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('itemId, custId', 'required'),
			array('itemId', 'length', 'max'=>20),
			array('custId, rank', 'length', 'max'=>10),
			array('status', 'length', 'max'=>11),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, itemId, custId, status, crAt, moAt, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cust' => array(self::BELONGS_TO, 'Customer', 'custId'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'itemId' => 'Item',
			'custId' => 'Customer',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'moAt' => 'Mo At',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
            $this->status = self::STATUS_ACTIVE;
			if($this->isNewRecord)
				$this->crAt=date("Y-m-d H:i:s");
            else
                $this->moAt=date("Y-m-d H:i:s");
			return true;
		}
		else return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllWishlist($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    // wish list auto count by customer
    public static function countWishListByCustomer($custId)
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('custId'=>$custId,'status'=>self::STATUS_ACTIVE));
        return '('.$countData.')';
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('custId',$this->custId,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}