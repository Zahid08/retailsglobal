<?php
// customer sign up custom model

class CustomerSignup extends CFormModel
{
    public $username;
    public $password;
    public $conf_password;
    public $custId;
    public $title;
    public $name;
    public $gender;
    public $addressline;
    public $city;
    public $email;
    public $phone;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title,custId,name, gender, addressline, city, phone, email,username, password,conf_password', 'required'),
            array('username', 'checkUniqueUsername'),
            array('email', 'checkUniqueEmail'),
            array('email', 'email'),
			array('title', 'length', 'max'=>3),
			array('name, addressline', 'length', 'max'=>255),
			array('gender', 'length', 'max'=>6),
			array('custId,city', 'length', 'max'=>150),
			array('phone, email', 'length', 'max'=>155),
            array('username,password,conf_password', 'length', 'max'=>128),       // here you say it can't be more than 40 characters
            array('conf_password', 'compare', 'compareAttribute'=>'password'),   // here you say that the password must be the same as conf_password
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'custId' => 'Customer ID',
			'title' => 'Title',
			'name' => 'Name',
			'gender' => 'Gender',
			'addressline' => 'Addressline',
			'city' => 'City',
			'phone' => 'Phone',
			'email' => 'Email',
            'username' => 'Username',
            'password' => 'Password',
            'conf_password'=>'Confirm Password',
		);
	}

    // check username unique
    public function checkUniqueUsername($attributes,$params)
    {
        $model = User::model()->find(array('condition'=>'branchId=:branchId AND username=:username',
                                           'params'=>array(':branchId'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE),':username'=>$this->username)));
        if(!empty($model)) $this->addError('username','Username already exists !');
    }
    // check email unique
    public function checkUniqueEmail($attributes,$params)
    {
        $model = Customer::model()->find(array('condition'=>'email=:email', 'params'=>array(':email'=>$this->email)));
        if(!empty($model)) $this->addError('email','Email already exists !');
    }
}