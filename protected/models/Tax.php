<?php

/**
 * This is the model class for table "{{tax}}".
 *
 * The followings are the available columns in table '{{tax}}':
 * @property string $id
 * @property string $taxRate
 * @property integer $taxType
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property Items[] $items
 * @property User $crBy0
 * @property User $moBy0
 */
class Tax extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const DEFAULT_TAX = 0;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Tax the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tax}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        array('taxRate, taxType, status', 'required'),
        array('taxRate+taxType', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('taxRate,taxType', 'numerical', 'integerOnly'=>true),
			array('taxRate, status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('rank', 'length', 'max'=>10),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, taxRate, taxType, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'items' => array(self::HAS_MANY, 'Items', 'taxId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'taxRate' => 'Tax Rate (%)',
			'taxType' => 'Tax Type',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllTax($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'taxRate');      
	}
	
	// get default tax
    public static function getDefaultTax()
	{      
		 $model = self::model()->find(array('condition'=>'taxRate=:taxRate AND status=:status', 'params'=>array(':taxRate'=>self::DEFAULT_TAX,':status'=>self::STATUS_ACTIVE)));
		 if(!empty($model)) return $model->id;
	}


    //-----------get all record by status as dropdown--------//
    public static function getTaxByTaxId($taxId)
    {
        $tax = self::model()->find(array('condition'=>'id=:id', 'params'=>array(':id'=>$taxId)));
       if(!empty($tax)){
           return $tax->taxRate;
       }else{
           return 0;
       }
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('taxRate',$this->taxRate,true);
		$criteria->compare('taxType',$this->taxType);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}