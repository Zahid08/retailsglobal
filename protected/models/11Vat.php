<?php

/**
 * This is the model class for table "{{bank}}".
 *
 * The followings are the available columns in table '{{bank}}':
 * @property string $id
 * @property string $name
 * @property string $branch
 * @property string $accountNo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class Vat extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bank the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vat}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array( 'branch,status,percentage', 'required'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('branch', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt', 'safe'),
			array('id, branch, status, crAt,percentage ,crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'percentage' => 'Percentage',
			'branch' => 'Branch',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as dropdown //
    public static function getAllVat($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status',
                                                               'params'=>array(':status'=>$isActive),
                                                               'group'=>'name',
                                                               'order'=>'name',
                                                        )), 'id', 'name');
	}	

    // get all record by status as dropdown //
    public static function getAllBankName($isActive)
    {
        return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status',
                                                            'params'=>array(':status'=>$isActive),
                                                            'group'=>'name',
                                                            'order'=>'name',
                                                        )), 'name', 'name');
    }

    // get bank id by accountNo
    public static function getBankByAccount($accountNo)
    {
        $model =  self::model()->find(array('condition'=>'accountNo=:accountNo AND status=:status',
                                             'params'=>array(':accountNo'=>$accountNo,':status'=>self::STATUS_ACTIVE),
                                 ));
        if(!empty($model)) return $model->id;
    }

    public static function getPercentaget($bracnId)
    {
        $model =  self::model()->find(array('condition'=>'branch=:branch AND status=:status',
            'params'=>array(':branch'=>$bracnId,':status'=>self::STATUS_ACTIVE),
        ));
        if(!empty($model)) return $model->percentage;
    }

    // get bank a/c wise current balance

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('branch',$this->branch,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('percentage',$this->percentage,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}