<?php

/**
 * This is the model class for table "{{temporary_worker}}".
 *
 * The followings are the available columns in table '{{temporary_worker}}':
 * @property string $id
 * @property string $twNo
 * @property string $branchId
 * @property string $wkId
 * @property string $itemId
 * @property double $qty
 * @property double $rqty
 * @property string $twDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property User $wk
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 */
class TemporaryWorker extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TemporaryWorker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{temporary_worker}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('twNo, wkId, itemId, qty,rqty, costPrice', 'required'),
            //array('name', 'unique'),
            array('twNo+itemId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('qty,rqty,costPrice', 'numerical'),
			array('twNo', 'length', 'max'=>250),
			array('branchId', 'length', 'max'=>10),
			array('wkId, itemId, crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('twDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, twNo, branchId, wkId, itemId, qty,rqty,twDate, costPrice, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'wk' => array(self::BELONGS_TO, 'User', 'wkId'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'twNo' => 'Temporary Worker Number',
			'branchId' => 'Branch',
			'wkId' => 'Worker',
			'itemId' => 'Item',
			'qty' => 'Qty',
            'rqty' => 'Received Qty',
			'twDate' => 'Tw Date',
			'costPrice' => 'Cost Price',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
                $this->branchId = Yii::app()->session['branchId'];
                $this->crAt = $this->twDate = date("Y-m-d H:i:s");
                $this->crBy = Yii::app()->user->id;
                $this->status = self::STATUS_ACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllTemporaryWorker($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	//-----------get all record by status as dropdown--------//
    public static function getAllTemporaryWorkerList($branchId)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'branchId=:branchId AND status=:status', 'params'=>array(':branchId'=>$branchId,':status'=>self::STATUS_ACTIVE))), 'twNo', 'twNo');      
	}
	
	// get worker balance (deposit-widraw)
    public static function getWorkerBalance($wkId)
    {
        $deposit = $withdraw = 0;
        $db = Yii::app()->db;

        // deposit calculations
        $sqldeposit = "SELECT SUM(qty*costPrice) AS totalamount FROM `pos_temporary_worker` WHERE wkId=".$wkId." AND qty=rqty ORDER BY crAt DESC";
        $resdeposit =	$db->createCommand($sqldeposit)->queryRow();
        if(!empty($resdeposit['totalamount'])) $deposit = $resdeposit['totalamount'];

        // widraw calculations
        $sqlwithdraw = "SELECT SUM(amount) AS totalamount FROM `pos_temporary_worker_widraw` WHERE wkId=".$wkId." ORDER BY crAt DESC";
        $reswithdraw =	$db->createCommand($sqlwithdraw)->queryRow();
        if(!empty($reswithdraw['totalamount'])) $withdraw  =  $reswithdraw['totalamount'];
        return ($deposit-$withdraw);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('twNo',$this->twNo,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('wkId',$this->wkId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty);
        $criteria->compare('rqty',$this->rqty);
		$criteria->compare('twDate',$this->twDate,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}