<?php
// customer checkout custom model
class CustomerCheckout extends CFormModel
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

    const IS_SHIPPING = 1;
    const IS_NOT_SHIPPING = 2;

    public $title;
    public $name;
    public $addressline;
    public $city;
    public $zipCode;
    public $email;
    public $phone;
    public $isShipping;

    public $sh_fname;
    public $sh_lastname;
    public $sh_streetaddress;
    public $sh_city;
    public $sh_zip;
    public $sh_phone;

	public function rules()
	{
		return array(
            array('title,name, addressline,city, zipCode, city, email,phone', 'required'),
            array('sh_fname,sh_lastname,sh_streetaddress,sh_city,sh_zip,sh_phone', 'checkShippingValidation'),
            array('title, name', 'length', 'max'=>255),
            array('email', 'email'),
            array('addressline', 'length', 'max'=>255),
            array('city', 'length', 'max'=>150),
            array('phone, email', 'length', 'max'=>155),
		);
	}

	public function attributeLabels()
	{
		return array(
			'title' => 'First Name',
			'name' => 'Last Name',
			'addressline' => 'Address',
			'city' => 'City',
			'zipCode' => 'Zip Code/Post Code',
			'phone' => 'Phone',
            'email' => 'Email',
		);
	}

    // check shipping validations
    public function checkShippingValidation($attributes,$params)
    {
        if($this->isShipping==self::IS_SHIPPING)
        {
            if(empty($this->sh_fname)) $this->addError('sh_fname','First Name cannot be blank.');
            if(empty($this->sh_lastname)) $this->addError('sh_lastname','Last Name cannot be blank.');
            if(empty($this->sh_streetaddress)) $this->addError('sh_streetaddress','Address cannot be blank.');
            if(empty($this->sh_city)) $this->addError('sh_city','City cannot be blank.');
            if(empty($this->sh_zip)) $this->addError('sh_zip','Zip Code cannot be blank.');
            if(empty($this->sh_phone)) $this->addError('sh_phone','Phone cannot be blank.');
        }
    }

    /**
     * Generates a salt that can be used to generate a password hash.
     * @return string the salt
     */
    public function generateSalt()
    {
        return uniqid('',true);
    }

    /**
     * Generates the password hash.
     * @param string password
     * @param string salt
     * @return string hash
     */
    public function hashPassword($password,$salt)
    {
        return md5($salt.$password);
    }
}