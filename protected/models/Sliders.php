<?php

/**
 * This is the model class for table "{{sliders}}".
 *
 * The followings are the available columns in table '{{sliders}}':
 * @property string $id
 * @property string $position
 * @property string $title
 * @property string $shortDesc
 * @property string $url
 * @property string $image
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property integer $rank
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class Sliders extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const POSITION_TOP='top';
	const POSITION_LEFT='left';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sliders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sliders}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('position, title,status', 'required'),
			array('image', 'required', 'on'=>'insert'),
			//array('name', 'unique'),
			//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('rank', 'numerical', 'integerOnly'=>true),
			array('position', 'length', 'max'=>6),
			array('title', 'length', 'max'=>200),
			array('url', 'length', 'max'=>250),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('shortDesc, image, crAt, moAt, secondTitle', 'safe'),
			array('image', 'file',
                'types'=>'jpg, gif, png',
                'maxSize'=>1024 * 700, // 250kb
                'tooLarge'=>'The file was larger than 250KB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert'
            ),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, position, title,shortDesc, url, image, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'position' => 'Position',
			'title' => 'Title',
			'shortDesc' => 'short Description',
			'url' => 'Url',
			'image' => 'Image',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllSliders($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('title',$this->title,true);
        $criteria->compare('shortDesc',$this->shortDesc,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}