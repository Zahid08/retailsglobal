<?php

/**
 * This is the model class for table "{{inventory}}".
 *
 * The followings are the available columns in table '{{inventory}}':
 * @property string $id
 * @property string $invNo
 * @property string $branchId
 * @property string $itemId
 * @property double $qty
 * @property double $adjustQty
 * @property string $invDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 */
class Inventory extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	const STATUS_CLOSE=3;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Inventory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{inventory}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	array('invNo, itemId,qty,adjustQty,costPrice,invDate', 'required'),
        	//array('invNo+itemId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('qty,adjustQty,costPrice', 'numerical'),
			array('invNo', 'length', 'max'=>250),
			array('branchId', 'length', 'max'=>10),
			array('itemId, crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('invDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invNo, branchId, itemId, qty,adjustQty, invDate, costPrice, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invNo' => 'Inventory No.',
			'branchId' => 'Branch',
			'itemId' => 'Item',
			'qty' => 'Qty',
			'adjustQty' => 'Adjust Qty',
			'invDate' => 'Inv Date',
			'costPrice' => 'Cost Price',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->session['branchId'];
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
				$this->status = self::STATUS_INACTIVE;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllInventory($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
    // item qty sum by invno and itemid
    public static function getInvItemQtySumByInvNo($invNo,$itemId)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(qty) as qty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId."
		           AND invNo='".$invNo."' order by invDate ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['qty'])) $inventory  =  $resinv['qty'];
		return $inventory;
	}
	
	 // item adj qty sum by invno and itemid
    public static function getInvAdjItemQtySumByInvNo($invNo,$itemId)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId."
		           AND invNo='".$invNo."' order by invDate ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];
		return $inventory;
	}
	
	// item adj qty sum by invno
    public static function getInvAdjItemQtySumByInvNoOnly($invNo)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND invNo='".$invNo."' 
				   order by invDate ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];
		return $inventory;
	}
	
	
	// item adj price sum by invno
    public static function getInvAdjItemPriceSumByInvNoOnly($invNo)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(adjustQty*costPrice) as totalAdjPrice FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." 
		           AND invNo='".$invNo."' order by invDate ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['totalAdjPrice'])) $inventory  =  $resinv['totalAdjPrice'];
		return $inventory;
	}
	
	// item qty latest sum by invno and itemid
    public static function getInvItemQtyLatestSumByInvNo($invNo)
	{  
		$inventory = 0;
		$sqlinv = "SELECT SUM(adjustQty) as totalAdj FROM
				  (
   					SELECT * FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND invNo='".$invNo."' GROUP BY itemId DESC 
					order by id DESC
				  ) AS t";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['totalAdj'])) $inventory  =  $resinv['totalAdj'];
		return $inventory;
	}
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('invNo',$this->invNo,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('adjustQty',$this->adjustQty);
		$criteria->compare('invDate',$this->invDate,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}