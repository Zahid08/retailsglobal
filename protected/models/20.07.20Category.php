<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property string $id
 * @property string $subdeptId
 * @property string $name
 * @property string $name_bd
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property User $crBy0
 * @property Subcategory[] $subcategories
 */
class Category extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	public $deptId;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('subdeptId,name, status', 'required'),
        	array('subdeptId+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('name, name_bd', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('subdeptId,crBy, moBy', 'length', 'max'=>20),
			array('rank', 'length', 'max'=>10),
			array('crAt, moAt, image, slugName', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, subdeptId, name, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'subdept' => array(self::BELONGS_TO, 'Subdepartment', 'subdeptId'),
			'items' => array(self::HAS_MANY, 'Items', 'catId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'deptId'=>'Department',
			'subdeptId' => 'Sub Department',
			'name' => 'Name',
			'name_bd' => 'Name (বাংলা)',
			'image' => 'Category Image',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllCategory($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    // get all category including e-commerce items
    public static function getAllCategoryByItemsHas()
    {
        $sql = "SELECT * FROM `pos_category` WHERE id IN(
                  SELECT catId FROM `pos_items` WHERE isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY catId
                ) AND status=".self::STATUS_ACTIVE." ORDER BY name";
        return  self::model()->findAllBySql($sql);
    }
	
	// return category model data
    public static function getAllCategoryData($isActive)
    {
        return  self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive)));
    }
	
	// get all cat by subdept
    public static function getAllCatBySubDept($subdeptId,$isActive)
    {
        return self::model()->findAll(array('condition'=>'subdeptId=:subdeptId and status=:status', 'params'=>array(':subdeptId'=>$subdeptId,':status'=>$isActive)));
    }

    // get dynamic filtering price 
    public static function priceDynamic($catId)
    {
    	//SELECT MIN(sellPrice) AS minPrice,MAX(sellPrice) AS maxPrice FROM pos_items i WHERE i.catId=3 AND isEcommerce=1 AND i.status=1
    	$row = Yii::app()->db->createCommand(array(
				    'select' => array('MIN(sellPrice) AS minPrice', 'MAX(sellPrice) AS maxPrice'),
				    'from' => 'pos_items',
				    'where' => 'catId=:catId AND isEcommerce=:isEcommerce AND status=:status',
				    'params' => array(':catId'=>$catId, ':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),
				))->queryRow();
    	return $row ;
    }


    // get all cat by dept
    public static function getAllCatByDept($deptId)
    {
        $subDeptArr = $model = $itemArr = array();
        $itemModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce and status=:status',
                                                   'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE)));
        if(!empty($itemModel)) :
            foreach($itemModel as $iKey=>$iData) :
                $itemArr[] = $iData->catId;
            endforeach;
        endif;

        $subDepartmentModel = Subdepartment::model()->findAll(array('condition'=>'deptId=:deptId and status=:status',
                                                                    'params'=>array(':deptId'=>$deptId,':status'=>Subdepartment::STATUS_ACTIVE)));
        if(!empty($subDepartmentModel)) :
            foreach($subDepartmentModel as $sKey=>$sData) :
                $subDeptArr[] = $sData->id;
            endforeach;
        endif;

        $criteria=new CDbCriteria;
        $criteria->condition = 'status=:status';
        $criteria->params =array(':status'=>self::STATUS_ACTIVE);
        $criteria->addInCondition("id", $itemArr);
        $criteria->addInCondition("subdeptId", $subDeptArr);
        $criteria->order = 'name';
        $model = self::model()->findAll($criteria);
        return $model;
    }

    // get all cat count by dept
    public static function getAllCatCountByDept($deptId)
    {
        $catCount = 0;
        $subDeptArr = $itemArr = array();
        $itemModel = Items::model()->findAll(array('condition'=>'isEcommerce=:isEcommerce and status=:status',
                                                   'params'=>array(':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE)));
        if(!empty($itemModel)) :
            foreach($itemModel as $iKey=>$iData) :
                $itemArr[] = $iData->catId;
            endforeach;
        endif;

        $subDepartmentModel = Subdepartment::model()->findAll(array('condition'=>'deptId=:deptId and status=:status',
                                                                    'params'=>array(':deptId'=>$deptId,':status'=>Subdepartment::STATUS_ACTIVE)));
        if(!empty($subDepartmentModel)) :
            foreach($subDepartmentModel as $sKey=>$sData) :
                $subDeptArr[] = $sData->id;
            endforeach;
        endif;

        $criteria=new CDbCriteria;
        $criteria->condition = 'status=:status';
        $criteria->params =array(':status'=>self::STATUS_ACTIVE);
        $criteria->addInCondition("id", $itemArr);
        $criteria->addInCondition("subdeptId", $subDeptArr);
        $catCount = self::model()->count($criteria);
        return $catCount;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
        $criteria->compare('subdeptId',$this->subdeptId);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}



    public static function ItemsSlug($itemName, $oldSlug = null){

        $slug = preg_replace('/[^A-Za-z0-9-]+/', '-', $itemName);
        $slug = trim(strtolower($slug));

        if(!empty($oldSlug)){
            if($slug == $oldSlug){
                return $slug;
            }
        }

        $sql = "SELECT * FROM pos_category WHERE slugName='".$slug."' ";
        $items = Items::model()->findAllBySql($sql);

        if(empty($items)){
            return $slug;
        }

        $uniqueSlug = $slug;
        $iteration = 1;
        $isSlugExit= true;

        while ($isSlugExit) {
            $uniqueSlug = $slug . '-' . $iteration;

            $sql = "SELECT * FROM pos_items WHERE slugName='".$uniqueSlug."' ";
            $items = Items::model()->findAllBySql($sql);

            if(empty($items)){
                $isSlugExit = false;
            }

            $iteration++;
        }

        return $uniqueSlug;

    }
}