<?php

/**
 * This is the model class for table "{{stock_transfer}}".
 *
 * The followings are the available columns in table '{{stock_transfer}}':
 * @property string $id
 * @property string $transferNo
 * @property string $fromBranchId
 * @property string $toBranchId
 * @property string $itemId
 * @property double $qty
 * @property string $stockDate
 * @property double $costPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Branch $fromBranch
 * @property Branch $toBranch
 * @property User $crBy0
 * @property Items $item
 */
class StockTransfer extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StockTransfer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{stock_transfer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('transferNo, toBranchId, itemId, qty, costPrice', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('qty, costPrice', 'numerical'),
			array('transferNo', 'length', 'max'=>250),
			array('fromBranchId, toBranchId', 'length', 'max'=>10),
			array('itemId, crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('stockDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, transferNo, fromBranchId, toBranchId, itemId, qty, stockDate, costPrice, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'fromBranch' => array(self::BELONGS_TO, 'Branch', 'fromBranchId'),
			'toBranch' => array(self::BELONGS_TO, 'Branch', 'toBranchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'transferNo' => 'Transfer No',
			'fromBranchId' => 'Store From',
			'toBranchId' => 'Store To',
			'itemId' => 'Item',
			'qty' => 'Qty',
			'stockDate' => 'Transfer Date',
			'costPrice' => 'Cost Price',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
                $this->fromBranchId = Yii::app()->session['branchId'];
				$this->crAt=$this->stockDate = date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
                $this->status = self::STATUS_INACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllStockTransfer($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	public static function getStockTransferListByTransferId($transferNo){
        $stockTransfer = StockTransfer::model()->findAll('transferNo=:transferNo AND status=:status',
            array(':transferNo'=>$transferNo,':status'=>StockTransfer::STATUS_ACTIVE));
        if(!empty($stockTransfer)){
            return $stockTransfer;
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('transferNo',$this->transferNo,true);
		$criteria->compare('fromBranchId',$this->fromBranchId,true);
		$criteria->compare('toBranchId',$this->toBranchId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('stockDate',$this->stockDate,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}