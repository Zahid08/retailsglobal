<?php

/**
 * This is the model class for table "{{items_price}}".
 *
 * The followings are the available columns in table '{{items_price}}':
 * @property string $id
 * @property string $icnNo
 * @property string $branchId 
 * @property string $itemId
 * @property double $costPrice
 * @property double $sellPrice
 * @property string $changeDate
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property User $crBy0
 * @property Items $item
 */
class ItemsPrice extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemsPrice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{items_price}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        array('icnNo, itemId, costPrice, sellPrice', 'required'),
        array('icnNo', 'unique'),
        //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('costPrice, sellPrice', 'numerical'),
			array('icnNo', 'length', 'max'=>250),
			array('branchId', 'length', 'max'=>10),
			array('itemId, crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('changeDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, icnNo,branchId, itemId, costPrice, sellPrice, changeDate, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'icnNo' => 'Icn No',
			'branchId' => 'Branch',
			'itemId' => 'Item',
			'costPrice' => 'Cost Price',
			'sellPrice' => 'Sell Price',
			'changeDate' => 'Change Date',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->session['branchId'];
				$this->crAt=$this->changeDate = date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
				$this->status = self::STATUS_INACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllItemsPrice($isActive)
	{      
		 return CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	// check an item price should be changed or not [ after grn approved it will not count in PO ] //
    public static function checkItemPriceUpdatable($itemId)
	{      
		$sql = "SELECT id FROM pos_good_receive_note WHERE STATUS!=".GoodReceiveNote::STATUS_APPROVED." AND poId
				IN(SELECT poId FROM pos_po_details WHERE itemId=".$itemId." and status=".PoDetails::STATUS_ACTIVE." 
				ORDER BY crAt DESC)";
		$itemModel = GoodReceiveNote::model()->findAllBySql($sql); 
		if(empty($itemModel)) return 0;
		else return 1;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('icnNo',$this->icnNo,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('sellPrice',$this->sellPrice);
		$criteria->compare('changeDate',$this->changeDate,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}