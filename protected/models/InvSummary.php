<?php

/**
 * This is the model class for table "{{inv_summary}}".
 *
 * The followings are the available columns in table '{{inv_summary}}':
 * @property string $id
 * @property string $branchId
 * @property string $invNo
 * @property string $invDate
 * @property double $totalQty
 * @property double $totalWeight
 * @property double $totalAdjustQty
 * @property double $totalPrice
 * @property double $totalAdjPrice
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property User $crBy0
 */
class InvSummary extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvSummary the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{inv_summary}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('invNo,totalQty, totalPrice,totalAdjPrice,totalWeight,totalAdjustQty', 'required'),
        	//array('invNo', 'unique'),
        	//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('totalQty, totalPrice,totalAdjPrice, totalWeight,totalAdjustQty', 'numerical'),
			array('branchId', 'length', 'max'=>10),
			array('invNo', 'length', 'max'=>250),
			array('crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('invDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,branchId,invNo, invDate, totalQty, totalPrice,totalAdjPrice, totalWeight,totalAdjustQty,crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
     		'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchId' => 'Branch',
			'invNo' => 'Inv No',
			'invDate' => 'Inv Date',
			'totalQty' => 'Total Qty',
			'totalPrice' => 'Total Price',
			'totalAdjPrice' => 'Total Adjust Price',
			'totalWeight' => 'Total Weight',
			'totalAdjustQty' => 'Total Adjust Quantity',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->SESSION['branchId'];
				$this->crAt=$this->invDate=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
				$this->status = self::STATUS_INACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllInvSummary($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	// item adjqty sum by invno
    public static function getInvItemAdjQtySumByInvNo($invNo)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(totalAdjustQty) as totalAdjustQty FROM pos_inv_summary WHERE branchId=".Yii::app()->session['branchId']." AND 
		           invNo='".$invNo."' order by crAt ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['totalAdjustQty'])) $inventory  =  $resinv['totalAdjustQty'];
		return $inventory;
	}
	
	// item adjprice sum by invno
    public static function getInvItemAdjPriceSumByInvNo($invNo)
	{  
		$inventory = 0;
		$sqlinv = "SELECT sum(totalAdjPrice) as totalAdjPrice FROM pos_inv_summary WHERE branchId=".Yii::app()->session['branchId']." AND 
		           invNo='".$invNo."' order by crAt ASC";
		$resinv = Yii::app()->db->createCommand($sqlinv)->queryRow();
		if(!empty($resinv['totalAdjPrice'])) $inventory  =  $resinv['totalAdjPrice'];
		return $inventory;
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('invNo',$this->invNo,true);
		$criteria->compare('invDate',$this->invDate,true);
		$criteria->compare('totalQty',$this->totalQty);
		$criteria->compare('totalPrice',$this->totalPrice);
		$criteria->compare('totalAdjPrice',$this->totalAdjPrice);
		$criteria->compare('totalWeight',$this->totalWeight);
		$criteria->compare('totalAdjustQty',$this->totalAdjustQty);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}