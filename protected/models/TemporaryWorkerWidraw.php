<?php

/**
 * This is the model class for table "{{temporary_worker_widraw}}".
 *
 * The followings are the available columns in table '{{temporary_worker_widraw}}':
 * @property string $id
 * @property string $branchId
 * @property string $bankId
 * @property string $wkId
 * @property string $twNo
 * @property double $amount
 * @property string $remarks
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property Branch $branch
 * @property User $crBy0
 * @property User $moBy0
 */
class TemporaryWorkerWidraw extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

	public $bankAcNo;
	public $isBank=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TemporaryWorkerWidraw the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{temporary_worker_widraw}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
           //array('id, branchId, bankId, wkId, twNo, amount, remarks, status, crAt, crBy, moAt, moBy, rank', 'required'),
           array('wkId, twNo, amount', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('amount', 'numerical'),
			array('branchId, bankId, rank', 'length', 'max'=>10),
			array('wkId, crBy, moBy', 'length', 'max'=>20),
			array('twNo, remarks', 'length', 'max'=>250),
			array('status', 'length', 'max'=>11),
			array('crAt, moAt, isBank,bankAcNo', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, branchId, bankId, wkId, twNo, amount, remarks, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'wk' => array(self::BELONGS_TO, 'User', 'wkId'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchId' => 'Branch',
			'bankId' => 'Bank',
			'wkId' => 'Worker',
			'twNo' => 'Temporary Worker Number',
			'amount' => 'Amount',
			'remarks' => 'Remarks',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
			    $this->branchId = Yii::app()->SESSION['branchId'];
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
				$this->status=self::STATUS_ACTIVE;
				$this->rank = 5;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllTemporaryWorkerWidraw($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('bankId',$this->bankId,true);
		$criteria->compare('wkId',$this->wkId,true);
		$criteria->compare('twNo',$this->twNo,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}