<?php
// supplier sign up custom model
class SupplierSignup extends CFormModel
{
    public $username;
    public $password;
    public $conf_password;
    public $suppId;
    public $name;
    public $address;
    public $city;
    public $email;
    public $phone;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('suppId,name, address, city, email, phone, username, password,conf_password', 'required'),
            array('username', 'checkUniqueUsername'),
            array('email', 'checkUniqueEmail'),
            array('phone', 'checkUniquePhone'),
		    array('email', 'email'),
			array('name', 'length', 'max'=>50),
			array('address', 'length', 'max'=>250),
			array('suppId,city, email, phone', 'length', 'max'=>150),
            array('username,password,conf_password', 'length', 'max'=>128),       // here you say it can't be more than 40 characters
            array('conf_password', 'compare', 'compareAttribute'=>'password'),   // here you say that the password must be the same as conf_password
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'suppId' => 'Supplier ID',
			'name' => 'Name',
			'address' => 'Address',
			'city' => 'City',
			'email' => 'Email',
			'phone' => 'Phone',
            'username' => 'Username',
            'password' => 'Password',
            'conf_password'=>'Confirm Password',
		);
	}

    // check username unique
    public function checkUniqueUsername($attributes,$params)
    {
        $model = User::model()->find(array('condition'=>'branchId=:branchId AND username=:username',
                                            'params'=>array(':branchId'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE),':username'=>$this->username)));
        if(!empty($model)) $this->addError('username','Username already exists !');
    }
    // check email unique
    public function checkUniqueEmail($attributes,$params)
    {
        $model = Supplier::model()->find(array('condition'=>'email=:email', 'params'=>array(':email'=>$this->email)));
        if(!empty($model)) $this->addError('email','Email already exists !');
    }
    // check phone unique
    public function checkUniquePhone($attributes,$params)
    {
        $model = Supplier::model()->find(array('condition'=>'phone=:phone', 'params'=>array(':phone'=>$this->phone)));
        if(!empty($model)) $this->addError('phone','Phone already exists !');
    }
}