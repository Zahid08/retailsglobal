<?php
/**
 * UserChangePassword class.
 * UserChangePassword is the data structure for keeping
 * user change password form data. It is used by the 'changepassword' action of 'UserController'.
 */
class CustomerChangePassword extends CFormModel {
	
	public $password;
	public $verifyPassword;	
	
	//$oldpass = User::model()->findByPk(Yii::app()->user->id);
	
	public function rules() {
		return array(
			array('password, verifyPassword', 'required'),
			array('password', 'length', 'max'=>128),			
			array('verifyPassword', 'compare', 'compareAttribute'=>'password'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(			
			'password'=>'New Password',
			'verifyPassword'=>'Retype Password',
		);
	}
	
	/*public function getOldpasses(){
		$pass = User::model()->findByPk(Yii::app()->user->id);		
		die($pass->password);
		$this->oldpass = User::model()->hashPassword($pass->password, $pass->salt);
	}*/
} 