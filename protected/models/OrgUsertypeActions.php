<?php
/**
 * This is the model class for table "{{org_usertype_actions}}".
 *
 * The followings are the available columns in table '{{org_usertype_actions}}':
 * @property string $id
 * @property string $usertypeId
 * @property string $contActionsId
 * @property integer $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property ContActions $contActions
 * @property User $crBy0
 */
class OrgUsertypeActions extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return OrgUsertypeActions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{branch_usertype_actions}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('usertypeId, contActionsId, status', 'required'),
/*			array('usertypeId+contActionsId', 'application.extensions.uniqueMultiColumnValidator'),*/
			array('status', 'numerical', 'integerOnly'=>true),
			array('usertypeId, crBy, moBy', 'length', 'max'=>20),
			array('contActionsId', 'length', 'max'=>10),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, usertypeId, contActionsId, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'contActions0' => array(self::BELONGS_TO, 'ContActions', 'contActionsId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'orgUsertype0' => array(self::BELONGS_TO, 'UserType', 'usertypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'usertypeId' => 'Role',
			'contActionsId' => 'Actions',
			'status' => 'Status',
			'crAt' => 'Create At',
			'crBy' => 'Create By',
			'moAt' => 'Last Modify At',
			'moBy' => 'Last Modify By'
		);
	}
/***
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
			}
			else{
				$this->moBy=Yii::app()->user->id;
				$this->moAt=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}
	
	/**
	 * find all user type by Organization
	 * @return array names
	 */
	
	public static function getUserTypeActions($usertypeId)
    {      
         return  CHtml::listData(self::model()->findAll(array('condition'=>'usertypeId=:usertypeId and status=:status', 'params'=>array(':usertypeId'=>$usertypeId,':status'=>self::STATUS_ACTIVE))), 'contActionsId', 'contActionsId');      
    }
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('usertypeId',$this->usertypeId);
		$criteria->compare('contActionsId',$this->contActionsId);
		$criteria->compare('status',$this->status);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);

		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}