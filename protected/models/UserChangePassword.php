<?php
/**
 * UserChangePassword class.
 * UserChangePassword is the data structure for keeping
 * user change password form data. It is used by the 'changepassword' action of 'UserController'.
 */
class UserChangePassword extends CFormModel {
	public $password;
	public $verifyPassword;
	public $oldpass;
	public $verifyoldpass;
	
	
	//$oldpass = User::model()->findByPk(Yii::app()->user->id);
	
	public function rules() {
		return array(
			array('password, verifyPassword, verifyoldpass', 'required'),
			array('password', 'length', 'max'=>128),
			array('verifyoldpass', 'compare', 'compareAttribute'=>'oldpass'),
			array('verifyPassword', 'compare', 'compareAttribute'=>'password'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyoldpass'=>'Old Password',
			'password'=>'New Password',
			'verifyPassword'=>'Retype Password',
		);
	}
	
	/*public function getOldpasses(){
		$pass = User::model()->findByPk(Yii::app()->user->id);		
		die($pass->password);
		$this->oldpass = User::model()->hashPassword($pass->password, $pass->salt);
	}*/
} 