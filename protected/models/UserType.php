<?php
/**
 * This is the model class for table "{{user_type}}".
 *
 * The followings are the available columns in table '{{user_type}}':
 * @property string $id
 * @property string $name
 * @property string $isType
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class UserType extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const TYPE_SUPER_ADMIN=1;
	const TYPE_ADMIN=2;
	const TYPE_CUSTOMER=3;
    const TYPE_SUPPLIER=4;
    const TYPE_DIRECT_GRN=5;
    const TYPE_TMP_WORKER=6;
    const TYPE_GUEST=7;

	/*
	 * Returns the static model of the specified AR class.
	 * @return UserType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(		
			array('name,status', 'required'),
			array('name', 'unique'),	
			array('name', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('isType, rank,crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,name,isType,rank,status,crAt,crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orgUsertypes' => array(self::HAS_MANY, 'OrgUsertype', 'usertypeId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'orgUsertypesActions' => array(self::HAS_MANY, 'OrgUsertype', 'usertypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'isType'=>'Type',
			'status' => 'Status',
			'crAt' => 'Create At',
			'crBy' => 'Create By',
			'moAt' => 'Last Modify At',
			'moBy' => 'Last Modify By',
			'rank' => 'Rank',
		);
	}
	public function getUrl()
	{
		return Yii::app()->createUrl('userType/view', array(
			'id'=>$this->id,
			'name'=>$this->name,
		));
	}
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
			}
			else{
				$this->moBy=Yii::app()->user->id;
				$this->moAt=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}

	/**
	 * @param $status
	 * @return array names
	 */
	public static function getUserType($status)
    {      
         return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$status),'order'=>'name')), 'id', 'name');
    }
	
	// get super user type
	public static function getSuperUserType()
    {      
         $model = self::model()->find(array('condition'=>'isType=:isType AND status=:status', 'params'=>array(':isType'=>self::TYPE_SUPER_ADMIN,':status'=>self::STATUS_ACTIVE)));
         if(!empty($model)) return $model->id;   
    }
	
	// get SUPPLIER user type
	public static function getSupplierType()
    {      
         $model = self::model()->find(array('condition'=>'isType=:isType AND status=:status', 'params'=>array(':isType'=>self::TYPE_SUPPLIER,':status'=>self::STATUS_ACTIVE)));
         if(!empty($model)) return $model->id;   
    }

    // get CUSTOMER user type
    public static function getCustomerType()
    {
        $model = self::model()->find(array('condition'=>'isType=:isType AND status=:status', 'params'=>array(':isType'=>self::TYPE_CUSTOMER,':status'=>self::STATUS_ACTIVE)));
        if(!empty($model)) return $model->id;
    }

    // get Direct GRN user type
    public static function getDirectGrnType()
    {
        $model = self::model()->find(array('condition'=>'isType=:isType AND status=:status', 'params'=>array(':isType'=>self::TYPE_DIRECT_GRN,':status'=>self::STATUS_ACTIVE)));
        if(!empty($model)) return $model->id;
    }

    // get temporary worker user type
    public static function getTemporaryWorkerType()
    {
        $model = self::model()->find(array('condition'=>'isType=:isType AND status=:status', 'params'=>array(':isType'=>self::TYPE_TMP_WORKER,':status'=>self::STATUS_ACTIVE)));
        if(!empty($model)) return $model->id;
    }
	
	// Using Add User,Roles Form
	public static function getUserTypeByIsNotSuperAdmin()
	{    
         return  CHtml::listData(self::model()->findAll(array('condition'=>'isType!=:isType and status=:status',
         													  'params'=>array(':isType'=>self::TYPE_SUPER_ADMIN,':status'=>UserType::STATUS_ACTIVE),
                                                              'order'=>'name')), 'id', 'name');
	}

    // getAllUserIsType
    public static function getAllUserIsType($status)
    {
        return array(
            self::TYPE_CUSTOMER=>'Customer',
            self::TYPE_SUPPLIER=>'Supplier',
            self::TYPE_DIRECT_GRN=>'Direct GRN',
            self::TYPE_TMP_WORKER=>'Temporary Worker',
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank);

		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}