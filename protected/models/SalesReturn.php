<?php

/**
 * This is the model class for table "{{sales_return}}".
 *
 * The followings are the available columns in table '{{sales_return}}':
 * @property string $id
 * @property string $salesId
 * @property string $branchId
 * @property string $itemId
 * @property double $qty
 * @property string $returnDate
 * @property double $totalPrice
 * @property double $totalTax
 * @property double $totalDiscount
 * @property double $totalSpDiscount
 * @property double $totalInsDiscount
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Branch $branch
 * @property User $crBy0
 * @property Items $item
 */
class SalesReturn extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SalesReturn the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_return}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('salesId,itemId, qty, totalPrice,totalTax, totalDiscount,totalSpDiscount,totalInsDiscount', 'required'),
        	//array('name', 'unique'),
        	array('salesId+itemId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('qty, totalPrice,totalTax, totalDiscount,totalSpDiscount,totalInsDiscount', 'numerical'),
			array('salesId, itemId, crBy, moBy', 'length', 'max'=>20),
			array('branchId', 'length', 'max'=>10),
			array('status', 'length', 'max'=>11),
			array('returnDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, salesId, branchId, itemId, qty, returnDate, totalPrice,totalTax, totalDiscount,totalSpDiscount,totalInsDiscount, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
            'sales' => array(self::BELONGS_TO, 'SalesInvoice', 'salesId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'salesId' => 'Sales',
			'branchId' => 'Branch',
			'itemId' => 'Item',
			'qty' => 'Qty',
			'returnDate' => 'Return Date',
			'totalPrice' => 'Total Price',
            'totalTax' => 'Total Tax',
            'totalDiscount' => 'Total Discount',
            'totalSpDiscount' => 'Total Special Discount',
            'totalInsDiscount' => 'Total Instant Discount',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->session['branchId'];
				$this->crAt= $this->returnDate = date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
				$this->status = self::STATUS_ACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllSalesReturn($isActive)
	{
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('salesId',$this->salesId,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty);
		$criteria->compare('returnDate',$this->returnDate,true);
		$criteria->compare('totalPrice',$this->totalPrice);
        $criteria->compare('totalTax',$this->totalTax);
        $criteria->compare('totalDiscount',$this->totalDiscount);
        $criteria->compare('totalDiscount',$this->totalSpDiscount);
        $criteria->compare('totalDiscount',$this->totalInsDiscount);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}