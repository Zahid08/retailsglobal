<?php
/*********************************************************
        -*- File: User.php
		-*- Author: Md.kamruzzaman<kzaman.badal@gmail.com>
        -*- Date: 20.01.2014
        -*- Position: root
		-*- YII-*- version 1.1.13
/*********************************************************/

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property string $id
 * @property string $branchId
 * @property string $supplierId
 * @property string $custId
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $ipAddress
 * @property string $browser
 * @property string $os
 * @property string $lastlogin
 * @property string $lastlogout
 * @property string $totalLoginTime
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Attributes[] $attributes
 * @property Attributes[] $attributes1
 * @property AttributesType[] $attributesTypes
 * @property AttributesType[] $attributesTypes1
 * @property Branch[] $branches
 * @property Branch[] $branches1
 * @property BranchUsertypeActions[] $branchUsertypeActions
 * @property BranchUsertypeActions[] $branchUsertypeActions1
 * @property Brand[] $brands
 * @property Brand[] $brands1
 * @property CardTypes[] $cardTypes
 * @property CardTypes[] $cardTypes1
 * @property Category[] $categories
 * @property Category[] $categories1
 * @property Company[] $companies
 * @property Company[] $companies1
 * @property ContActions[] $contActions
 * @property ContActions[] $contActions1
 * @property Controllers[] $controllers
 * @property Controllers[] $controllers1
 * @property Customer[] $customers
 * @property Customer[] $customers1
 * @property CustomerDeposit[] $customerDeposits
 * @property CustomerDeposit[] $customerDeposits1
 * @property CustomerLoyalty[] $customerLoyalties
 * @property CustomerLoyalty[] $customerLoyalties1
 * @property CustomerType[] $customerTypes
 * @property CustomerType[] $customerTypes1
 * @property DamageWastage[] $damageWastages
 * @property Department[] $departments
 * @property Department[] $departments1
 * @property Finance[] $finances
 * @property Finance[] $finances1
 * @property FinanceCategory[] $financeCategories
 * @property FinanceCategory[] $financeCategories1
 * @property GoodReceiveNote[] $goodReceiveNotes
 * @property GoodReceiveNote[] $goodReceiveNotes1
 * @property InvSettings[] $invSettings
 * @property InvSettings[] $invSettings1
 * @property InvSummary[] $invSummaries
 * @property InvSummary[] $invSummaries1
 * @property Inventory[] $inventories
 * @property ItemImages[] $itemImages
 * @property Items[] $items
 * @property Items[] $items1
 * @property ItemsPrice[] $itemsPrices
 * @property ItemsPrice[] $itemsPrices1
 * @property PoDetails[] $poDetails
 * @property PoReturns[] $poReturns
 * @property PofferPackages[] $pofferPackages
 * @property PofferPackages[] $pofferPackages1
 * @property Poffers[] $poffers
 * @property Poffers[] $poffers1
 * @property PurchaseOrder[] $purchaseOrders
 * @property PurchaseOrder[] $purchaseOrders1
 * @property Roles[] $roles
 * @property Roles[] $roles1
 * @property Roles[] $roles2
 * @property SalesDetails[] $salesDetails
 * @property SalesDetails[] $salesDetails1
 * @property SalesInvoice[] $salesInvoices
 * @property SalesInvoice[] $salesInvoices1
 * @property SalesReturn[] $salesReturns
 * @property Stock[] $stocks
 * @property Subdepartment[] $subdepartments
 * @property Subdepartment[] $subdepartments1
 * @property Supplier[] $suppliers
 * @property Supplier[] $suppliers1
 * @property SupplierDeposit[] $supplierDeposits
 * @property SupplierDeposit[] $supplierDeposits1
 * @property SupplierWidraw[] $supplierWidraws
 * @property SupplierWidraw[] $supplierWidraws1
 * @property Tax[] $taxes
 * @property Tax[] $taxes1
 * @property Tracer[] $tracers
 * @property Tracer[] $tracers1
 * @property Tracer[] $tracers2
 * @property UserType[] $userTypes
 * @property UserType[] $userTypes1
 */ 
class User extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_PENDING=2;
	const STATUS_DISABLE=2;

	const IS_SUPPLIER= 1;
	const IS_NOT_SUPPLIER = 2;
    const SYSTEM_CUST_SUPP = 0;
	
	const SUPER_USER = 'unlock';
	
	public $conf_password;     // Here you declare the "confirm password" property
	public $isSupplier;
	
	/*
	 * Returns the static model of the specified AR class.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/*
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('isSupplier,username, password, branchId, conf_password,status', 'required'),
			array('branchId+username', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('branchId, supplierId,custId, status', 'length', 'max'=>10),
			array('username, password, salt, browser, os', 'length', 'max'=>128),
            array('ipAddress, totalLoginTime', 'length', 'max'=>20),
            array('lastlogin, lastlogout', 'length', 'max'=>12),
			array('conf_password', 'length', 'max'=>128),       // here you say it can't be more than 40 characters
			array('conf_password', 'compare', 'compareAttribute'=>'password'),   // here you say that the password must be the same as conf_password
			array('supplierId,custId,conf_password', 'safe'), // here you say that conf_password can be massively assigned from POST
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, branchId, supplierId,custId, username, password_reset_token, auth_key, password, salt, ipAddress, browser, os, lastlogin, lastlogout, totalLoginTime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(		
			'branchcrBy' => array(self::HAS_MANY, 'Branch', 'crBy'),
			'branchmoBy' => array(self::HAS_MANY, 'Branch', 'moBy'),		
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'customer' => array(self::BELONGS_TO, 'Customer', 'custId'),
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplierId'),
            'branchUsertypeActions' => array(self::HAS_MANY, 'BranchUsertypeActions', 'crBy'),
            'branchUsertypeActions1' => array(self::HAS_MANY, 'BranchUsertypeActions', 'moBy'),
            'brands' => array(self::HAS_MANY, 'Brand', 'crBy'),
            'brands1' => array(self::HAS_MANY, 'Brand', 'moBy'),
            'categories' => array(self::HAS_MANY, 'Category', 'crBy'),
            'categories1' => array(self::HAS_MANY, 'Category', 'moBy'),
            'companies' => array(self::HAS_MANY, 'Company', 'crBy'),
            'companies1' => array(self::HAS_MANY, 'Company', 'moBy'),
            'contActions' => array(self::HAS_MANY, 'ContActions', 'crBy'),
            'contActions1' => array(self::HAS_MANY, 'ContActions', 'moBy'),
            'controllers' => array(self::HAS_MANY, 'Controllers', 'crBy'),
            'controllers1' => array(self::HAS_MANY, 'Controllers', 'moBy'),
            'customers' => array(self::HAS_MANY, 'Customer', 'crBy'),
            'customers1' => array(self::HAS_MANY, 'Customer', 'moBy'),
            'customerDeposits' => array(self::HAS_MANY, 'CustomerDeposit', 'crBy'),
            'customerDeposits1' => array(self::HAS_MANY, 'CustomerDeposit', 'moBy'),
            'customerLoyalties' => array(self::HAS_MANY, 'CustomerLoyalty', 'crBy'),
            'customerLoyalties1' => array(self::HAS_MANY, 'CustomerLoyalty', 'moBy'),
            'customerTypes' => array(self::HAS_MANY, 'CustomerType', 'crBy'),
            'customerTypes1' => array(self::HAS_MANY, 'CustomerType', 'moBy'),
            'damageWastages' => array(self::HAS_MANY, 'DamageWastage', 'crBy'),
            'departments' => array(self::HAS_MANY, 'Department', 'crBy'),
            'departments1' => array(self::HAS_MANY, 'Department', 'moBy'),
            'finances' => array(self::HAS_MANY, 'Finance', 'crBy'),
            'finances1' => array(self::HAS_MANY, 'Finance', 'moBy'),
            'financeCategories' => array(self::HAS_MANY, 'FinanceCategory', 'crBy'),
            'financeCategories1' => array(self::HAS_MANY, 'FinanceCategory', 'moBy'),
            'goodReceiveNotes' => array(self::HAS_MANY, 'GoodReceiveNote', 'crBy'),
            'goodReceiveNotes1' => array(self::HAS_MANY, 'GoodReceiveNote', 'moBy'),
            'invSettings' => array(self::HAS_MANY, 'InvSettings', 'crBy'),
            'invSettings1' => array(self::HAS_MANY, 'InvSettings', 'moBy'),
            'invSummaries' => array(self::HAS_MANY, 'InvSummary', 'crBy'),
            'invSummaries1' => array(self::HAS_MANY, 'InvSummary', 'moBy'),
            'inventories' => array(self::HAS_MANY, 'Inventory', 'crBy'),
            'itemImages' => array(self::HAS_MANY, 'ItemImages', 'crBy'),
            'items' => array(self::HAS_MANY, 'Items', 'crBy'),
            'items1' => array(self::HAS_MANY, 'Items', 'moBy'),
            'itemsPrices' => array(self::HAS_MANY, 'ItemsPrice', 'crBy'),
            'itemsPrices1' => array(self::HAS_MANY, 'ItemsPrice', 'moBy'),
            'packages' => array(self::HAS_MANY, 'Packages', 'crBy'),
            'packages1' => array(self::HAS_MANY, 'Packages', 'moBy'),
            'paymentMethods' => array(self::HAS_MANY, 'PaymentMethod', 'crBy'),
            'paymentMethods1' => array(self::HAS_MANY, 'PaymentMethod', 'moBy'),
            'poDetails' => array(self::HAS_MANY, 'PoDetails', 'crBy'),
            'poReturns' => array(self::HAS_MANY, 'PoReturns', 'crBy'),
            'pofferPackages' => array(self::HAS_MANY, 'PofferPackages', 'crBy'),
            'pofferPackages1' => array(self::HAS_MANY, 'PofferPackages', 'moBy'),
            'poffers' => array(self::HAS_MANY, 'Poffers', 'crBy'),
            'poffers1' => array(self::HAS_MANY, 'Poffers', 'moBy'),
            'purchaseOrders' => array(self::HAS_MANY, 'PurchaseOrder', 'crBy'),
            'purchaseOrders1' => array(self::HAS_MANY, 'PurchaseOrder', 'moBy'),
            'roles' => array(self::HAS_MANY, 'Roles', 'crBy'),
            'roles1' => array(self::HAS_MANY, 'Roles', 'moBy'),
            'roles2' => array(self::HAS_MANY, 'Roles', 'userId'),
            'salesDetails' => array(self::HAS_MANY, 'SalesDetails', 'crBy'),
            'salesDetails1' => array(self::HAS_MANY, 'SalesDetails', 'moBy'),
            'salesInvoices' => array(self::HAS_MANY, 'SalesInvoice', 'crBy'),
            'salesInvoices1' => array(self::HAS_MANY, 'SalesInvoice', 'moBy'),
            'salesReturns' => array(self::HAS_MANY, 'SalesReturn', 'crBy'),
            'stocks' => array(self::HAS_MANY, 'Stock', 'crBy'),
            'subdepartments' => array(self::HAS_MANY, 'Subdepartment', 'crBy'),
            'subdepartments1' => array(self::HAS_MANY, 'Subdepartment', 'moBy'),
            'suppliers' => array(self::HAS_MANY, 'Supplier', 'crBy'),
            'suppliers1' => array(self::HAS_MANY, 'Supplier', 'moBy'),
            'supplierDeposits' => array(self::HAS_MANY, 'SupplierDeposit', 'crBy'),
            'supplierDeposits1' => array(self::HAS_MANY, 'SupplierDeposit', 'moBy'),
            'supplierWidraws' => array(self::HAS_MANY, 'SupplierWidraw', 'crBy'),
            'supplierWidraws1' => array(self::HAS_MANY, 'SupplierWidraw', 'moBy'),
            'taxes' => array(self::HAS_MANY, 'Tax', 'crBy'),
            'taxes1' => array(self::HAS_MANY, 'Tax', 'moBy'),
            'tracers' => array(self::HAS_MANY, 'Tracer', 'cashierId'),
            'tracers1' => array(self::HAS_MANY, 'Tracer', 'crBy'),
            'tracers2' => array(self::HAS_MANY, 'Tracer', 'moBy'),
            'userTypes' => array(self::HAS_MANY, 'UserType', 'crBy'),
            'userTypes1' => array(self::HAS_MANY, 'UserType', 'moBy'),
			'useractions' => array(self::HAS_MANY, 'Roles', 'userId'),
		);
	}

	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->salt)===$this->password;
	}
	
	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password,$salt)
	{
		return md5($salt.$password);
	}

	/**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
    public function generateSalt()
	{
		return uniqid('',true);
	}

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::app()->getSecurityManager()->generateRandomString(32);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString(32) . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'branchId' => 'Store',
            'supplierId' => 'Supplier',
            'custId' => 'Customer',
            'username' => 'Username',
            'password' => 'Password',
			'conf_password'=>'Confirm Password',
            'salt' => 'Salt',
            'ipAddress' => 'Ip Address',
            'browser' => 'Browser',
            'os' => 'Os',
            'lastlogin' => 'Lastlogin',
            'lastlogout' => 'Lastlogout',
            'totalLoginTime' => 'Total Login Time',
            'status' => 'Status',
		);
	}

	/*
	 * find all  user by Current Organization
	 * @return array names
	 */
	public static function getAllUser()
    {      
        return  CHtml::listData(self::model()->findAll(array('condition'=>'branchId=:branchId', 'params'=>array(':branchId'=>Yii::app()->session['orgid']))), 'id', 'username');
    }
    
    /*
	 * find all  user by Organization/branch
	 * @return array names
	 */
	public static function getUserByOrg($branchId)
    {
        return CHtml::listData(self::model()->findAll(array('condition'=>'branchId=:branchId', 'params'=>array(':branchId'=>$branchId))), 'id', 'username');
    }

    /*
	 * find all system user by Organization/branch
	 * @return array names
	 */
    public static function getAllSystemUserByOrg($branchId)
    {
        return CHtml::listData(self::model()->findAll(array('condition'=>'branchId=:branchId AND supplierId=:supplierId AND custId=:custId',
                                                            'params'=>array(':branchId'=>$branchId,':supplierId'=>self::SYSTEM_CUST_SUPP,':custId'=>self::SYSTEM_CUST_SUPP))), 'id', 'username');
    }

    // get a default user of default branch
    public static function getUserDefaultBranch($branchId)
    {
        $model = self::model()->find(array('condition'=>'branchId=:branchId AND status=:status', 'params'=>array(':branchId'=>$branchId,':status'=>self::STATUS_ACTIVE)));
        if(!empty($model)) return $model->id;
    }

    // get a direct grn user
    public static function getDirectGrnUser()
    {
        $sql = "SELECT id FROM pos_user WHERE branchId=".Yii::app()->session['branchId']." AND id IN(SELECT userId FROM pos_roles WHERE userTypeId=".UserType::getDirectGrnType().")";
        $model = UserType::model()->findBySql($sql);
        if(!empty($model)) return $model->id;
    }

    // get a temporary worker user
    public static function getTemporaryWorkerUser()
    {
        $dataArray = array();
        if(!empty(UserType::getTemporaryWorkerType()))
        {
            $sql = "SELECT * FROM pos_user WHERE branchId=".Yii::app()->session['branchId']." AND id IN(SELECT userId FROM pos_roles WHERE userTypeId=".UserType::getTemporaryWorkerType().")";
            $dataArray = CHtml::listData(self::model()->findAllBySql($sql), 'id', 'username');
        }
        return $dataArray;
    }

	/*public function scopes()
    {
        return array(           
            'superuser'=>array(
                'condition'=>'branchId=1',
            ),    
            'notsafe'=>array(
            	'select' => 'id, username, password, salt, branchId, ipAddress, browser, os, lastlogin, lastlogout, totalLoginTime',
            ),
        );
    }

	public function defaultScope()
    {
        return array(
            'select' => 'id, username, password, salt, branchId, ipAddress, browser, os, lastlogin, lastlogout, totalLoginTime',
        );
    }*/
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		
        //$criteria->condition = 'supplierId=:supplierId AND custId=:custId';
        //$criteria->params = array(':supplierId'=>self::SYSTEM_CUST_SUPP,':custId'=>self::SYSTEM_CUST_SUPP);
		
		if(empty(Yii::app()->user->isSuperAdmin))
		{
			$criteria->addSearchCondition("branchId",Yii::app()->session['branchId']);
			$criteria->condition = 'username<>:username';
			$criteria->params = array(':username'=>self::SUPER_USER);
		}
		
		$criteria->compare('id',$this->id,true);
        $criteria->compare('branchId',$this->branchId,true);
        $criteria->compare('supplierId',$this->supplierId,true);
        $criteria->compare('custId',$this->custId,true);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('salt',$this->salt,true);
        $criteria->compare('ipAddress',$this->ipAddress,true);
        $criteria->compare('browser',$this->browser,true);
        $criteria->compare('os',$this->os,true);
        $criteria->compare('lastlogin',$this->lastlogin,true);
        $criteria->compare('lastlogout',$this->lastlogout,true);
        $criteria->compare('totalLoginTime',$this->totalLoginTime,true);
        $criteria->compare('status',$this->status,true);

		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}