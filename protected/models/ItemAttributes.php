<?php

/**
 * This is the model class for table "{{item_attributes}}".
 *
 * The followings are the available columns in table '{{item_attributes}}':
 * @property string $id
 * @property integer $attTypeId
 * @property string $attId
 * @property string $itemId
 *
 * The followings are the available model relations:
 * @property Items $item
 * @property Attributes $att
 * @property AttributesType $attType
 */
class ItemAttributes extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemAttributes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{item_attributes}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('attTypeId, attId, itemId', 'required'),
            array('attTypeId+attId+itemId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('attTypeId', 'numerical', 'integerOnly'=>true),
			array('attId', 'length', 'max'=>10),
			array('itemId', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, attTypeId, attId, itemId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
			'att' => array(self::BELONGS_TO, 'Attributes', 'attId'),
			'attType' => array(self::BELONGS_TO, 'AttributesType', 'attTypeId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'attTypeId' => 'Att Type',
			'attId' => 'Att',
			'itemId' => 'Item',
		);
	}

    //-----------get all record by status as dropdown--------//
    public static function getAllItemAttributes($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	// get attributes item wise selected values 
    public static function getAttributesValuesByItems($attTypeId,$itemId)
    {
        return self::model()->find(array('condition'=>'attTypeId=:attTypeId AND itemId=:itemId','params'=>array(':attTypeId'=>$attTypeId,':itemId'=>$itemId)));
    }

    /* get attributes all item wise selected values
    public static function getAttributesByAllItems($attTypeId,$itemName)
    {
        $itemArr = $attModel = array();
        $attItemModel = Items::model()->findAll(array('condition'=>'itemName=:itemName and isEcommerce=:isEcommerce and status=:status',
                                                      'params'=>array(':itemName'=>$itemName,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE)));
        if(!empty($attItemModel)) :
            foreach($attItemModel as $key=>$data) $itemArr[$data->id] = $data->id;
            $criteria=new CDbCriteria;
            $criteria->condition = 'attTypeId=:attTypeId';
            $criteria->params = array(':attTypeId'=>$attTypeId);
            $criteria->addInCondition("itemId", $itemArr);
            //$criteria->group = 'attId';
            $criteria->order = 'attId';
            $attModel = self::model()->findAll($criteria);
        endif;
        return $attModel;
    }*/

    // get attributes all item wise selected values grouped
    public static function getAttributesByAllItemsGroup($itemName)
    {
        $itemArr = $attModel = array();
        $attItemModel = Items::model()->findAll(array('condition'=>'itemName=:itemName and isEcommerce=:isEcommerce and status=:status',
                                                      'params'=>array(':itemName'=>$itemName,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE)));
        if(!empty($attItemModel)) :
            foreach($attItemModel as $key=>$data) $itemArr[$data->id] = $data->id;
            $criteria=new CDbCriteria;
            $criteria->addInCondition("itemId", $itemArr);
            $criteria->group = 'itemId';
            $criteria->order = 'attId';
            $attModel = self::model()->findAll($criteria);
        endif;
        return $attModel;
    }

    // get attributes all item wise selected values matched with grouped itemId
    public static function getAttributesByAllItemsGroupMatched($attTypeId,$itemId,$itemName)
    {
        $itemArr = $attModel = array();
        $attItemModel = Items::model()->findAll(array('condition'=>'itemName=:itemName and isEcommerce=:isEcommerce and status=:status',
            'params'=>array(':itemName'=>$itemName,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE)));
        if(!empty($attItemModel)) :
            foreach($attItemModel as $key=>$data) $itemArr[$data->id] = $data->id;
            $criteria=new CDbCriteria;
            $criteria->condition = 'attTypeId=:attTypeId AND itemId=:itemId';
            $criteria->params = array(':attTypeId'=>$attTypeId,':itemId'=>$itemId);
            $criteria->addInCondition("itemId", $itemArr);
            //$criteria->group = 'attId';
            $attModel = self::model()->findAll($criteria);
        endif;
        return $attModel;
    }


    // get attributes all item wise selected values as dropdown
    public static function getAttributesValuesByAllItems($attTypeId,$itemName)
    {
        return CHtml::listData(self::getAttributesByAllItems($attTypeId,$itemName),'itemId','itemId'); // attributesNameById
    }

    // get attributes all item wise selected values as dropdown with default item match
    public static function getAttributesValuesByAllItemsDefault($attTypeId,$itemId,$itemName)
    {
        return CHtml::listData(self::getAttributesByAllItemsDefault($attTypeId,$itemId,$itemName),'itemId','itemId'); // attributesNameById
    }

    // get attributes info by id
    protected function getAttributesNameById()
    {
        return $this->att->name;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('attTypeId',$this->attTypeId);
		$criteria->compare('attId',$this->attId,true);
		$criteria->compare('itemId',$this->itemId,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}