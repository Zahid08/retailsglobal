<?php
/**
 * This is the model class for table "{{controllers}}".
 *
 * The followings are the available columns in table '{{controllers}}':
 * @property string $id
 * @property string $module
 * @property string $name
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $description
 * The followings are the available model relations:
 * @property ContActions[] $contActions
 * @property User $crBy0
 * @property User $moBy0
 */
class Controllers extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Controllers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{controllers}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, status', 'required'),
			/*array('name', 'application.extensions.uniqueMultiColumnValidator'),*/
			array('name,module', 'length', 'max'=>50),
			array('status', 'length', 'max'=>11),
			array('crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt,description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, module, status, crAt, crBy, moAt, moBy,description', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contActions' => array(self::HAS_MANY, 'ContActions', 'contId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',		
			'module' =>'Module',	
			'status' => 'Status',
			'crAt' => 'Create At',
			'crBy' => 'Create By',
			'moAt' => 'Last Modify At',
			'moBy' => 'Last Modify By',
			'description'=>'Description'
			
		);
	}

	/**
	 * @return string the URL that shows the detail of the organisation
	 */
	public function getUrl()
	{
		return Yii::app()->createUrl('controllers/view', array(
			'id'=>$this->id,
			'name'=>$this->name,
		));
	}
	
	/**
	 * This is invoked before the record is saved.
	 * @return boolean whether the record should be saved.
	 */
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
			}
			else{
				$this->moBy=Yii::app()->user->id;
				$this->moAt=date("Y-m-d H:i:s");
			}
			return true;
		}
		else
			return false;
	}
	
	/**
	 * @param $status
	 * @return array names
	 */
	
	public static function getAllcontrollers()
    {      
         return  CHtml::listData(self::model()->findAll(), 'id', 'name');      
    }
    
	/**
	 * @param $status
	 * @return array names
	 */
	
	public static function getcontrollers($status,$module = "" )
    {      
		if($module=="") $module = Yii::app()->db->tablePrefix;
		return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status and module=:module', 'params'=>array(':status'=>$status,':module'=>$module))), 'id', 'name');      
    }

	public static function getdesc($id)
    {      
         return  self::model()->findByPk($id)->description;     
    }
    	
	/**
	 * @param $status
	 * @return array names
	 */
	
	public static function getmoduleList()
    {      
         return CHtml::listData(Controllers::model()->findAll(array('select'=>'distinct(module) as module')), 'module', 'module');      
		 //return self::model()->findAll(array('select'=>'distinct(module) as module'));      
    }
	
	 /* 
	 * @param $module
	 * @param $name
	 * @return array */
	
	public static function getcontrollersByModuleName($module,$name)
    {      
         return self::model()->find(array('condition'=>'module=:module and name=:name',
											'params'=>array(':module'=>$module,
											':name'=>$name)
										));      
    }
    
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('module',$this->module,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('description',$this->description,true);

		return new CActiveDataProvider(get_class($this), array(
			'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}