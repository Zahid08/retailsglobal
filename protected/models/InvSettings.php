<?php

/**
 * This is the model class for table "{{inv_settings}}".
 *
 * The followings are the available columns in table '{{inv_settings}}':
 * @property string $id
 * @property string $invNo
 * @property string $branchId
 * @property string $subDeptId
 * @property integer $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property Branch $branch
 * @property Subdepartment $subDept
 * @property User $crBy0
 * @property User $moBy0
 */
class InvSettings extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InvSettings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{inv_settings}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	array('invNo,subDeptId, status', 'required'),
        	//array('invNo+subDeptId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
        	// array('branchId+subDeptId', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('invNo', 'length', 'max'=>250),
			array('status', 'numerical', 'integerOnly'=>true),
			array('branchId, rank', 'length', 'max'=>10),
			array('subDeptId, crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,invNo,branchId, subDeptId, status, crAt, crBy, moAt, moBy, rank', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'subDept' => array(self::BELONGS_TO, 'Subdepartment', 'subDeptId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invNo' => 'Inv No',
			'branchId' => 'Branch',
			'subDeptId' => 'Sub Department',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'rank' => 'Rank',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->SESSION['branchId'];
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
	
	// return settings by subcat status
    public static function getAllActiveSubDept()
	{ 
		$invSubDept = '';     
		$invModel = self::model()->findAll('branchId=:branchId and status=:status',array(':branchId'=>Yii::app()->SESSION['branchId'],':status'=>InvSettings::STATUS_ACTIVE)); 
		if(!empty($invModel)) :
			$ptr = 0;
			foreach($invModel as $inv) : $ptr++;
				if(count($invModel)==$ptr)
					$invSubDept.= $inv->subDeptId;
				else $invSubDept.= $inv->subDeptId.',';
			endforeach;
		endif;
		
		return $invSubDept;
	}
    
    
    //-----------get all record by status as dropdown--------//
    public static function getAllInvSettings($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('invNo',$this->invNo,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('subDeptId',$this->subDeptId,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('rank',$this->rank,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}