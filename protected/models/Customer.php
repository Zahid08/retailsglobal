<?php

/**
 * This is the model class for table "{{customer}}".
 *
 * The followings are the available columns in table '{{customer}}':
 * @property string $id
 * @property string $custId
 * @property integer $custType
 * @property string $title
 * @property string $name
 * @property string $gender
 * @property string $addressline
 * @property string $city
 * @property string $phone
 * @property string $email
 * @property string $crLimit
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class Customer extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const STATUS_GENERAL=1;
	const CUST_INSTITUTIONAL=2;
	const DEFAULT_CUST=0;

    const IS_SHIPPING = 1;
    const IS_NOT_SHIPPING = 2;

    public $zipCode;
    public $isShipping;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customer}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('custId, phone', 'required'), // title,gender
            array('zipCode', 'checkCheckoutValidation'),
		    array('custId', 'unique'),
            array('email', 'email'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('custType', 'numerical', 'integerOnly'=>true),
			array('crLimit', 'numerical'),
			array('title', 'length', 'max'=>3),
			array('name, addressline', 'length', 'max'=>255),
			array('gender', 'length', 'max'=>6),
			array('custId,city', 'length', 'max'=>150),
			array('phone, email', 'length', 'max'=>155),
			array('crLimit, status', 'length', 'max'=>10),
			array('crBy, moBy', 'length', 'max'=>20),
			array('zipCode,isShipping,crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, custId,custType,title, name, gender, addressline, city, phone, email, crLimit, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'custType0' => array(self::BELONGS_TO, 'CustomerType', 'custType'),
            'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
            'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'customerDeposits' => array(self::HAS_MANY, 'CustomerDeposit', 'custId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'custId' => 'Customer ID',
			'custType' => 'Customer Type',
			'title' => 'Title',
			'name' => 'Name',
			'gender' => 'Gender',
			'addressline' => 'Addressline',
			'city' => 'City',
			'phone' => 'Phone',
			'email' => 'Email',
			'crLimit' => 'Credit Limit',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
		);
	}

    // check checkout validations
    public function checkCheckoutValidation($attributes,$params)
    {
        if(!empty($this->isShipping))
            if(empty($this->zipCode)) $this->addError('zipCode','Zip Code cannot be blank.');
    }
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
                $this->crBy=$this->crBy=(!empty(Yii::app()->user->id))?Yii::app()->user->id:User::getUserDefaultBranch(Branch::getDefaultBranch(Branch::STATUS_ACTIVE));
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllCustomer($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'custId');      
	}
	
	//-----------get all record by status as dropdown--------//
    public static function getAllCustomerInstitutional($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status and custType=:custType', 'params'=>array(':status'=>$isActive, ':custType'=>Customer::CUST_INSTITUTIONAL))), 'id', 'custId');      
	}
	
	// get customer balance (deposit-sells)
	public static function getCustomerBalance($custId)
	{    
		 $deposit = $invoice = 0;
		 $db = Yii::app()->db;  
		 
		 // deposit calculations
		 $sqldeposit = "SELECT SUM(amount) AS deposit FROM pos_customer_deposit WHERE custId=".$custId." AND branchId=".Yii::app()->session['branchId'];
		 $resdeposit =	$db->createCommand($sqldeposit)->queryRow();
		 if(!empty($resdeposit['deposit'])) $deposit 	 =  $resdeposit['deposit'];	
		 
		 // invoice calculations
		 $sqlinvoice = "SELECT SUM(totalPrice) AS tprice, SUM(totalDiscount) AS tdiscount, SUM(totalTax) AS ttax FROM pos_sales_invoice WHERE custId=".$custId." AND branchId=".Yii::app()->session['branchId'];
		 $resinvoice =	$db->createCommand($sqlinvoice)->queryRow();
		 if(!empty($resinvoice['tprice'])) $invoice  =  ($resinvoice['tprice'] + $resinvoice['ttax']) - $resinvoice['tdiscount'];	
		 return ($deposit - $invoice);
	}
	
	
	// get customer loyality point by sells id
	public static function getCustomerLoyaltyPointBySalesId($salesId)
	{    
		 $loyalty = 0;
		 $db = Yii::app()->db;  
		 
		 // loyalty calculations
		 $sqlloyalty = "SELECT loyaltyAmount FROM pos_customer_loyalty WHERE salesId=".$salesId;
		 $resloyalty =	$db->createCommand($sqlloyalty)->queryRow();
		 if(!empty($resloyalty['loyaltyAmount'])) $loyalty 	 =  $resloyalty['loyaltyAmount'];	
		 return $loyalty;
	}
	
	// get customer loyality point total
	public static function getCustomerLoyaltyPoint($custId)
	{    
		 $loyalty = 0;
		 $db = Yii::app()->db;  
		 
		 // loyalty calculations
		 $sqlloyalty = "SELECT SUM(loyaltyAmount) AS loyalty FROM pos_customer_loyalty l, pos_sales_invoice i WHERE l.salesId=i.id AND l.custId=".$custId." AND i.branchId=".Yii::app()->session['branchId'];
		 $resloyalty =	$db->createCommand($sqlloyalty)->queryRow();
		 if(!empty($resloyalty['loyalty'])) $loyalty 	 =  $resloyalty['loyalty'];	
		 return $loyalty;
	}
	
	// get customer loyality invoice
	public static function getCustomerLoyaltyInvoice($custId)
	{    
		 $invoice = 0;
		 $db = Yii::app()->db;  
		 
		 // invoice loyalty calculations
		 $sqlinvoice = "SELECT SUM(loyaltyPaid) AS tloyaltyPaid FROM pos_sales_invoice WHERE custId=".$custId." AND branchId=".Yii::app()->session['branchId'];
		 $resinvoice =	$db->createCommand($sqlinvoice)->queryRow();
		 if(!empty($resinvoice['tloyaltyPaid'])) $invoice =  $resinvoice['tloyaltyPaid'];	
		 return $invoice;
	}
	
	// get customer loyality balance (loyalty amount - sells loyalty)
	public static function getCustomerLoyaltyBalance($custId)
	{    
		 $loyalty = $invoice = 0;
		 $db = Yii::app()->db;  
		 
		 // loyalty calculations
		 $sqlloyalty = "SELECT SUM(loyaltyAmount) AS loyalty FROM pos_customer_loyalty l, pos_sales_invoice i WHERE l.salesId=i.id AND l.custId=".$custId." AND i.branchId=".Yii::app()->session['branchId'];
		 $resloyalty =	$db->createCommand($sqlloyalty)->queryRow();
		 if(!empty($resloyalty['loyalty'])) $loyalty 	 =  $resloyalty['loyalty'];	
		 
		 // invoice loyalty calculations
		 $sqlinvoice = "SELECT SUM(loyaltyPaid) AS tloyaltyPaid FROM pos_sales_invoice WHERE custId=".$custId." AND branchId=".Yii::app()->session['branchId'];
		 $resinvoice =	$db->createCommand($sqlinvoice)->queryRow();
		 if(!empty($resinvoice['tloyaltyPaid'])) $invoice =  $resinvoice['tloyaltyPaid'];	
		 return ($loyalty - $invoice);
	}


    // COunt Pending Customer
    public static function countPendingCustomer()
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('status'=>self::STATUS_INACTIVE));
        return $countData;
    }
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->order = 'crAt DESC';
		$criteria->compare('id',$this->id,true);
		$criteria->compare('custId',$this->custId);
		$criteria->compare('custType',$this->custType);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('addressline',$this->addressline,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('crLimit',$this->crLimit,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}