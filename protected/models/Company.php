<?php

/**
 * This is the model class for table "{{company}}".
 *
 * The followings are the available columns in table '{{company}}':
 * @property string $id
 * @property string $packageId
 * @property string $orgIdPrefix
 * @property string $name
 * @property string $domain
 * @property string $email
 * @property string $favicon
 * @property string $logo
 * @property string $theme
 * @property string $skin
 * @property string $detailsinfo
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property Branch[] $branches
 * @property Packages $package
 * @property User $crBy0
 * @property User $moBy0
 * @property Items[] $items
 */
class Company extends CActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    const DEFAULT_CURRENCY = '৳';
    const DEFAULT_COMPANY = 1;
    const DEFAULT_THEME = 'mediacenter';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Company the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{company}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('orgIdPrefix, name, domain, email,theme ,status', 'required'),
            array('logo,favicon', 'required', 'on'=>'insert'),
            array('orgIdPrefix,name,domain,email', 'unique'),
            array('email', 'email'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
            array('packageId, status', 'length', 'max'=>10),
            array('orgIdPrefix', 'length', 'max'=>5),
            array('name, domain, email, theme, skin', 'length', 'max'=>150),
            array('crBy, moBy', 'length', 'max'=>20),
            array('detailsinfo, crAt, moAt', 'safe'),

            array('logo', 'file',
                'types'=>'jpg, jpeg, gif, png',
                'maxSize'=>1024 * 1024 * 1, // 1MB
                'tooLarge'=>'The file was larger than 1MB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert'
            ),

            array('favicon', 'file',
                'types'=>'ico',
                'maxSize'=>1024 * 200, // 100kb
                'tooLarge'=>'The file was larger than 200KB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert'
            ),
            array('logo, favicon, sms_text, sms_api_key', 'safe'),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, packageId, orgIdPrefix, name, domain, email, favicon, logo,theme, skin, detailsinfo, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'branches' => array(self::HAS_MANY, 'Branch', 'orgId'),
            'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
            'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'items' => array(self::HAS_MANY, 'Items', 'orgId'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'packageId' => 'Package',
            'orgIdPrefix' => 'Prefix',
            'name' => 'Name',
            'domain' => 'Domain',
            'email' => 'Email',
            'favicon' => 'Favicon',
            'logo' => 'Logo',
            'theme' => 'Theme',
            'skin' => 'Skin',
            'detailsinfo' => 'Invoice Email Text',
            'sms_text' => 'SMS Text',
            'sms_api_key' => 'SMS API Key',
            'status' => 'Status',
            'crAt' => 'Cr At',
            'crBy' => 'Cr By',
            'moAt' => 'Mo At',
            'moBy' => 'Mo By',
        );
    }

    /*-----------Save before functionality------------*/
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->crAt=date("Y-m-d H:i:s");
                $this->crBy=Yii::app()->user->id;
            }
            return true;
        }
        else
            return false;
    }

    //-----------get all record by status as dropdown--------//
    public static function getAllCompany($isActive)
    {
        return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');
    }

    // company wise currency
    public static function getCurrency()
    {
        return self::DEFAULT_CURRENCY;
    }

    /**
     * find all active Theame
     * @return array names
     */
    public static function getThemes()
    {
        if ($handle = opendir(dirname(Yii::app()->request->scriptFile) . '/themes/')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." && !(preg_match('/\./', $file))) {
                    $dirsNames[$file] =  $file;
                }
            }
        }
        return $dirsNames;
    }

    /*
     * find all active Theame
     * @return array names
     */
    public static function getSkins($themes)
    {
        if ($handle = opendir(dirname(Yii::app()->request->scriptFile) . '/themes/'.$themes.'/')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "images" && $file != "views" && $file != "." && $file != ".." && !(preg_match('/\./', $file))) {
                    $dirsNames[$file] =  $file;
                }
            }
        }
        return $dirsNames;
    }

    // -- get specific theme and skins for branch --
    public static function getThemesSkins()
    {
        if(isset(Yii::app()->session['branchId']))
            return self::model()->findByPk((int)Yii::app()->session['branchId']);
        else
            return self::model()->findByPk(1);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        /*if(empty(Yii::app()->user->isSuperAdmin))
        {
            $criteria->condition = 'id>:id';
            $criteria->params = array(':id'=>self::DEFAULT_COMPANY);
        }*/
        $criteria->compare('id',$this->id,true);
        $criteria->compare('packageId',$this->packageId,true);
        $criteria->compare('orgIdPrefix',$this->orgIdPrefix,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('domain',$this->domain,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('favicon',$this->favicon,true);
        $criteria->compare('logo',$this->logo,true);
        $criteria->compare('theme',$this->theme);
        $criteria->compare('skin',$this->skin);
        $criteria->compare('detailsinfo',$this->detailsinfo,true);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('crAt',$this->crAt,true);
        $criteria->compare('crBy',$this->crBy,true);
        $criteria->compare('moAt',$this->moAt,true);
        $criteria->compare('moBy',$this->moBy,true);

        return new CActiveDataProvider($this, array(
            'pagination'=>array(
                //
                // please check how we get the
                // the pageSize from user's state
                'pageSize'=> Yii::app()->user->getState('pageSize',
                        //
                        // we have previously set defaultPageSize
                        // on the params section of our main.php config file
                        Yii::app()->params['defaultPageSize']),
            ),
            'criteria'=>$criteria,
        ));
    }

    public function sendSms($data){
        $company = Company::model()->find();
        if(!empty($data)){

            $url = 'http://sms.sendinsms.com/api/web/v1/send-sms/send-sms?access-token='.$company->sms_api_key;

            $dataArray = $data;

            $dataArray = http_build_query($dataArray);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataArray); // datas

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// get respose
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);

            // Disable SSL verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($ch);	// Execute the request
            curl_close($ch);
        }

    }
}