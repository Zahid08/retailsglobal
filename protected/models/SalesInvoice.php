<?php

/**
 * This is the model class for table "{{sales_invoice}}".
 *
 * The followings are the available columns in table '{{sales_invoice}}':
 * @property string $id
 * @property string $invNo
 * @property string $branchId
 * @property string $custId
 * @property string $orderDate
 * @property string $delivaryDate
 * @property double $totalQty
 * @property double $totalWeight
 * @property double $totalPrice
 * @property double $totalDiscount
 * @property double $instantDiscount
 * @property double $totalTax
 * @property double $totalShipping
 * @property double $cashPaid
 * @property double $cardPaid
 * @property double $crbalPaid
 * @property double $loyaltyPaid
 * @property string $cardId
 * @property string $cardNo
 * @property string $cardAppNo
 * @property double $totalChangeAmount
 * @property double $spDiscount
 * @property string $couponId
 * @property string $remarks
 * @property string $message
 * @property integer $isEcommerce
 * @property string $billingAddress
 * @property string $shippingAddress
 * @property string $shippingId
 * @property string $paymentId
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property SalesDetails[] $salesDetails
 * @property Branch $branch
 * @property User $crBy0
 * @property User $moBy0
 */
class SalesInvoice extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
    const STATUS_PROCESSING=3;
    const STATUS_CANCEL=4; // full return
    const STATUS_RETURN=5; // partial return

    const IS_ECOMMERCE=1;
    const IS_NOT_ECOMMERCE=2;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SalesInvoice the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sales_invoice}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	array('invNo, totalQty, totalWeight, totalPrice, totalDiscount, totalTax,status', 'required'),
        	array('divisions,districts,upazilas', 'safe'),
       		array('invNo', 'unique'),
            array('isEcommerce', 'numerical', 'integerOnly'=>true),
        	//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('totalQty, totalWeight, totalPrice, totalDiscount,instantDiscount, totalTax,totalShipping, cashPaid, cardPaid, crbalPaid, loyaltyPaid,totalChangeAmount,spDiscount', 'numerical'),
			array('invNo, remarks, message', 'length', 'max'=>250),
			array('branchId, custId, cardId,couponId,shippingId, paymentId', 'length', 'max'=>10),
			array('cardNo, cardAppNo', 'length', 'max'=>150),
			array('crBy, moBy', 'length', 'max'=>20),
			array('status', 'length', 'max'=>11),
			array('orderDate, delivaryDate,billingAddress, shippingAddress,crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, invNo, branchId, custId, orderDate, delivaryDate, totalQty, totalWeight, totalPrice, totalDiscount,instantDiscount, totalTax,totalShipping, cashPaid, cardPaid,
			       crbalPaid, loyaltyPaid, cardId, cardNo, cardAppNo,totalChangeAmount,spDiscount,couponId,remarks, message,isEcommerce, billingAddress, shippingAddress,
			       shippingId, paymentId,crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'customerLoyalties' => array(self::HAS_MANY, 'CustomerLoyalty', 'salesId'),
			'salesDetails' => array(self::HAS_MANY, 'SalesDetails', 'salesId'),
            'coupon' => array(self::BELONGS_TO, 'Coupons', 'couponId'),
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'card' => array(self::BELONGS_TO, 'CardTypes', 'cardId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'cust' => array(self::BELONGS_TO, 'Customer', 'custId'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'salesReturns' => array(self::HAS_MANY, 'SalesReturn', 'salesId'),
            'shipping' => array(self::BELONGS_TO, 'ShippingMethod', 'shippingId'),
            'payment' => array(self::BELONGS_TO, 'PaymentMethod', 'paymentId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'invNo' => 'Invoice No',
			'branchId' => 'Branch',
			'divisions' => 'Divisions',
			'districts' => 'Districts',
			'upazilas' => 'Upazilas',
			'custId' => 'Customer',
			'orderDate' => 'Order Date',
			'delivaryDate' => 'Delivary Date',
			'totalQty' => 'Total Quantity',
			'totalWeight' => 'Total Weight',
			'totalPrice' => 'Total Amount',
			'totalDiscount' => 'Total Discount',
            'instantDiscount' => 'Instant Discount',
			'totalTax' => 'Total Tax',
            'totalShipping' => 'Delivery/Shipping Charge',
			'cashPaid' => 'Cash Paid',
			'cardPaid' => 'Card Paid',
			'crbalPaid' => 'Paid From Balance',
			'loyaltyPaid' => 'Total Point',
			'cardId' => 'Credit Card Type',
			'cardNo' => 'Credit Card No.',
			'cardAppNo' => 'Card Approval No.',
			'totalChangeAmount' => 'Change/Refund Amount',
			'spDiscount' => 'Special Discount',
            'couponId' => 'Coupon',
			'remarks' => 'Remarks',
			'message' => 'Message',
            'isEcommerce' => 'Is Ecommerce',
            'billingAddress' => 'Billing Address',
            'shippingAddress' => 'Shipping Address',
            'shippingId' => 'Shipping Method',
            'paymentId' => 'Payment Method',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
                $this->branchId=(!empty(Yii::app()->session['branchId']))?Yii::app()->session['branchId']:Branch::getDefaultBranch(Branch::STATUS_ACTIVE);
				$this->crAt = $this->orderDate = $this->delivaryDate = date("Y-m-d H:i:s");
                if(Yii::app()->urlManager->parseUrl(Yii::app()->request)=='goodReceiveNote/grnApprove')
                    $this->crBy = User::getDirectGrnUser();
                else $this->crBy=(!empty(Yii::app()->user->id))?Yii::app()->user->id:User::getUserDefaultBranch(Branch::getDefaultBranch(Branch::STATUS_ACTIVE));
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as drop down
    public static function getAllSalesInvoice($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    // Count Pending Invoice
    public static function countPendingInvoice()
    {
        $countData = 0;
        $countData = self::model()->countByAttributes(array('isEcommerce'=>self::IS_ECOMMERCE,'status'=>self::STATUS_PROCESSING));
        return $countData;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
	   // print_r(Yii::app()->session['branchId']);exit();

		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

        $branchModel = Branch::model()->find(array('condition'=>'id=:id', 'params'=>array('id'=>Yii::app()->session['branchId'])));

		$criteria=new CDbCriteria;
        $criteria->addSearchCondition('isEcommerce',self::IS_ECOMMERCE);
        if (Yii::app()->session['branchId']!=1) {
            $criteria->addSearchCondition('branchId', Yii::app()->session['branchId']);
        }
//        $criteria->addSearchCondition('districts',$branchModel->districts);
//        $criteria->addSearchCondition('upazilas',$branchModel->upazilas);
        $criteria->order = 'orderDate DESC';

		$criteria->compare('id',$this->id,true);
		$criteria->compare('invNo',$this->invNo,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('custId',$this->custId,true);
		$criteria->compare('orderDate',$this->orderDate,true);
		$criteria->compare('delivaryDate',$this->delivaryDate,true);
		$criteria->compare('totalQty',$this->totalQty);
		$criteria->compare('totalWeight',$this->totalWeight);
		$criteria->compare('totalPrice',$this->totalPrice);
		$criteria->compare('totalDiscount',$this->totalDiscount);
        $criteria->compare('instantDiscount',$this->instantDiscount);
		$criteria->compare('totalTax',$this->totalTax);
        $criteria->compare('totalShipping',$this->totalShipping);
		$criteria->compare('cashPaid',$this->cashPaid);
		$criteria->compare('cardPaid',$this->cardPaid);
		$criteria->compare('crbalPaid',$this->crbalPaid);
		$criteria->compare('loyaltyPaid',$this->loyaltyPaid);
		$criteria->compare('cardId',$this->cardId,true);
		$criteria->compare('cardNo',$this->cardNo,true);
		$criteria->compare('cardAppNo',$this->cardAppNo,true);
		$criteria->compare('totalChangeAmount',$this->totalChangeAmount);
        $criteria->compare('spDiscount',$this->spDiscount);
        $criteria->compare('couponId',$this->couponId,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('message',$this->message,true);
        $criteria->compare('isEcommerce',$this->isEcommerce);
        $criteria->compare('billingAddress',$this->billingAddress,true);
        $criteria->compare('shippingAddress',$this->shippingAddress,true);
        $criteria->compare('shippingId',$this->shippingId,true);
        $criteria->compare('paymentId',$this->paymentId,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}