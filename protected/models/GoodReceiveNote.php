<?php

/**
 * This is the model class for table "{{good_receive_note}}".
 *
 * The followings are the available columns in table '{{good_receive_note}}':
 * @property string $id
 * @property string $supplierId
 * @property string $poId
 * @property string $grnNo
 * @property string $grnDate
 * @property string $totalQty
 * @property double $totalPrice
 * @property double $totalDiscount
 * @property string $totalWeight
 * @property string $remarks
 * @property string $message
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property PurchaseOrder $po
 * @property Supplier $supplier
 * @property Stock[] $stocks
 */
class GoodReceiveNote extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	const STATUS_APPROVED=3; // after grn approved

    const AUTO_GRN_DISCOUNT_AMOUNT = 0;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GoodReceiveNote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{good_receive_note}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('supplierId, poId, grnNo, totalQty,totalWeight,totalPrice, totalDiscount', 'required'),
            array('poId', 'unique','message'=>'GRN already done for this PO.'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('totalPrice, totalDiscount', 'numerical'),
			array('supplierId', 'length', 'max'=>10),
			array('poId, crBy, moBy', 'length', 'max'=>20),
			array('grnNo, remarks, message', 'length', 'max'=>250),
			array('status', 'length', 'max'=>11),
			array('grnDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, supplierId, poId, grnNo, grnDate, totalQty, totalPrice, totalDiscount, totalWeight, remarks, message, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'po' => array(self::BELONGS_TO, 'PurchaseOrder', 'poId'),
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplierId'),
			'stocks' => array(self::HAS_MANY, 'Stock', 'grnId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'supplierId' => 'Supplier',
			'poId' => 'Po No',
			'grnNo' => 'GRN No',
			'grnDate' => 'GRN Date',
			'totalQty' => 'Total Qty',
			'totalPrice' => 'Total Price',
			'totalDiscount' => 'Total Discount',
			'totalWeight' => 'Total Weight',
			'remarks' => 'Remarks',
			'message' => 'Message',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=$this->grnDate=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;
				if(isset($_REQUEST['ItemsVariation'])){
                    if(!empty($_REQUEST['ItemsVariation'])){
                        $this->status = 3;
                    }
                } else{
                    $this->status = self::STATUS_ACTIVE;
                }
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllGoodReceiveNote($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('supplierId',$this->supplierId,true);
		$criteria->compare('poId',$this->poId,true);
		$criteria->compare('grnNo',$this->grnNo,true);
		$criteria->compare('grnDate',$this->grnDate,true);
		$criteria->compare('totalQty',$this->totalQty,true);
		$criteria->compare('totalPrice',$this->totalPrice);
		$criteria->compare('totalDiscount',$this->totalDiscount);
		$criteria->compare('totalWeight',$this->totalWeight,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}