<?php

/**
 * This is the model class for table "{{branch}}".
 *
 * The followings are the available columns in table '{{branch}}':
 * @property string $id
 * @property string $branchIdPrefix
 * @property string $name
 * @property string $addressline
 * @property string $city
 * @property string $postalcode
 * @property string $phone
 * @property string $fax
 * @property string $email
 * @property string $vatrn
 * @property string $status
 * @property integer $isDefault
 * @property integer $isInvoiceFull
 * @property integer $isDueInvoice
 * @property integer $isSpecialDiscount
 * @property integer $isInstantDiscount
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $crBy0
 * @property User $moBy0
 */
class Branch extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	
	const DEFAULT_BRANCH=1;
	const IS_FULL_INVOICE_LAYOUT=1;
    const IS_DUE_INVOICE=1;
    const IS_SPECIAL_DISCOUNT=1;
    const IS_INSTANT_DISCOUNT=1;
	const ADMIN_SKIN='responsive';
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Branch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{branch}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('branchIdPrefix, name, addressline, city, postalcode, phone,vatrn,status,divisions,districts,upazilas', 'required'),
            array('name', 'unique'),
		    array('email', 'email'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
            array('isDefault,isInvoiceFull,isDueInvoice, isSpecialDiscount,isInstantDiscount', 'numerical', 'integerOnly'=>true),
			array('branchIdPrefix', 'length', 'max'=>5),
			array('name, addressline', 'length', 'max'=>255),
			array('city, postalcode,vatrn', 'length', 'max'=>150),
			array('phone, fax, email', 'length', 'max'=>155),
			array('status', 'length', 'max'=>10),
			array('crBy, moBy', 'length', 'max'=>20),
			array('crAt, moAt', 'safe'),
			array('crAt, moAt', 'safe'),
            array('branch_image', 'required', 'on'=>'insert'),
            array('branch_image', 'file',
                'types'=>'jpg, gif, png',
                'maxSize'=>2048 * 250, // 250kb
                'tooLarge'=>'The file was larger than 250KB. Please upload a smaller file.',
                'allowEmpty'=>'false',
                'on'=>'insert'
            ),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, branchIdPrefix, name, addressline, city, postalcode, phone, fax, email,vatrn, status,isDefault,isInvoiceFull,isDueInvoice, isSpecialDiscount,isInstantDiscount, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'bankDeposits' => array(self::HAS_MANY, 'BankDeposit', 'branchId'),
            'bankWithdraws' => array(self::HAS_MANY, 'BankWithdraw', 'branchId'),
            'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
            'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
            'customerDeposits' => array(self::HAS_MANY, 'CustomerDeposit', 'branchId'),
            'damageWastages' => array(self::HAS_MANY, 'DamageWastage', 'branchId'),
            'finances' => array(self::HAS_MANY, 'Finance', 'branchId'),
            'invSettings' => array(self::HAS_MANY, 'InvSettings', 'branchId'),
            'invSummaries' => array(self::HAS_MANY, 'InvSummary', 'branchId'),
            'inventories' => array(self::HAS_MANY, 'Inventory', 'branchId'),
            'itemsPrices' => array(self::HAS_MANY, 'ItemsPrice', 'branchId'),
            'poReturns' => array(self::HAS_MANY, 'PoReturns', 'branchId'),
            'poffers' => array(self::HAS_MANY, 'Poffers', 'branchId'),
            'purchaseOrders' => array(self::HAS_MANY, 'PurchaseOrder', 'branchId'),
            'salesInvoices' => array(self::HAS_MANY, 'SalesInvoice', 'branchId'),
            'salesReturns' => array(self::HAS_MANY, 'SalesReturn', 'branchId'),
            'stocks' => array(self::HAS_MANY, 'Stock', 'branchId'),
            'supplierDeposits' => array(self::HAS_MANY, 'SupplierDeposit', 'branchId'),
            'supplierWidraws' => array(self::HAS_MANY, 'SupplierWidraw', 'branchId'),
            'temporaryWorkers' => array(self::HAS_MANY, 'TemporaryWorker', 'branchId'),
            'tracers' => array(self::HAS_MANY, 'Tracer', 'branchId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchIdPrefix' => 'Prefix',
			'name' => 'Name',
			'branch_image' => 'Branch Image',
			'addressline' => 'Addressline',
			'divisions' => 'Divisions',
			'districts' => 'Districts',
			'upazilas' => 'Upazilas',
			'city' => 'City',
			'postalcode' => 'Postalcode',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'email' => 'Email',
			'vatrn' =>'Vat Registration No.',
			'status' => 'Status',
            'isDefault' => 'Is Default',
            'isInvoiceFull' => 'Full Invoice Layout',
            'isDueInvoice' => 'Has Due Invoice',
            'isSpecialDiscount' => 'Has Special Discount',
            'isInstantDiscount' => 'Has Instant Discount',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as drop down
    public static function getAllBranch($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

    // get all branch for report global
    public static function getAllBranchGlobal($isActive)
    {
        $list = array('all'=>'All Branch');
        $data = self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive)));
        $list[] = CHtml::listData($data,'id','name');
        return $list;
    }

    // get default branch
    public static function getDefaultBranch($isActive)
    {
        $model = self::model()->find(array('condition'=>'isDefault=:isDefault AND status=:status', 'params'=>array(':isDefault'=>self::DEFAULT_BRANCH,':status'=>$isActive)));
        if(!empty($model)) return $model->id;
        else return self::DEFAULT_BRANCH;
    }

    // get invoice layout value
    public static function getInvoiceLayoutValue($id)
    {
        $model = self::model()->findByPk($id);
        if(!empty($model)) return $model->isInvoiceFull;
    }

    public static function getBranchNameValue($id)
    {
        $model = self::model()->findByPk($id);
        if(!empty($model)) return $model->name;
    }


    // get special discount value
    public static function getSpecialDiscountValue($id)
    {
        $model = self::model()->findByPk($id);
        if(!empty($model)) return $model->isSpecialDiscount;
    }

    // get instant discount value
    public static function getInstantDiscountValue($id)
    {
        $model = self::model()->findByPk($id);
        if(!empty($model)) return $model->isInstantDiscount;
    }

    // get due invoice value
    public static function getDueInvoiceValue($id)
    {
        $model = self::model()->findByPk($id);
        if(!empty($model)) return $model->isDueInvoice;
    }

	/*
	 * find all active branch
	 * @param $status
	 * @return array names
	 */
	public static function getAllorgByuser()
    {      
         return  CHtml::listData(self::model()->findAll(array('condition'=>'id=:id', 'params'=>array(':id'=>Yii::app()->session['orgid']))), 'id', 'name');      
    }
	public static function getorgdesc($org)
    {      
    	$detailsinfos = self::model()->findByPk($org);
        return $detailsinfos->detailsinfo;
    }
	
	public static function getorgdata($org)
    {      
    	$detailsinfos = self::model()->findByPk($org);
        return $detailsinfos;
    }
	
	/*
	 * find all active branch
	 * @param $status
	 * @return array names
	 */
	public static function getAllorg()
    {      
         return  CHtml::listData(self::model()->findAll(), 'id', 'name');      
    }
	
	/**
	 * find all active branch
	 * @param $status
	 * @return array names
	 */
	public static function getorg($status)
    {      
        //return CHtml::listData(self::model()->findAll(array('condition'=>'id>:id AND status=:status', 'params'=>array('id'=>Company::DEFAULT_COMPANY,':status'=>$status))), 'id', 'name');
        return CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$status))), 'id', 'name');
    }

    /**
     * find all active branch except signed in
     * @param $status
     * @return array names
     */
    public static function getBranchExceptSigned($status)
    {
        return CHtml::listData(self::model()->findAll(array('condition'=>'id<>:id AND status=:status', 'params'=>array('id'=>Yii::app()->session['branchId'],':status'=>$status))), 'id', 'name');
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        if(empty(Yii::app()->user->isSuperAdmin))
        {
            $criteria->condition = 'id>:id';
            $criteria->params = array(':id'=>Company::DEFAULT_COMPANY);
        }
		$criteria->compare('id',$this->id,true);
		$criteria->compare('branchIdPrefix',$this->branchIdPrefix,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('addressline',$this->addressline,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('postalcode',$this->postalcode,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('vatrn',$this->vatrn,true);
		$criteria->compare('status',$this->status,true);
        $criteria->compare('isDefault',$this->isDefault);
        $criteria->compare('isInvoiceFull',$this->isInvoiceFull);
        $criteria->compare('isDueInvoice',$this->isDueInvoice);
        $criteria->compare('isSpecialDiscount',$this->isSpecialDiscount);
        $criteria->compare('isInstantDiscount',$this->isInstantDiscount);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}

    public static function getAllDivisions($id=null)
    {
        $divisionFilePath='http://pluspointretail.unlockretail.com'.'/division/divisions.json';
        $string = file_get_contents($divisionFilePath);
        $json_divisionArray = json_decode($string, true);

        $dataList=[];
        foreach ($json_divisionArray as $key=>$json_divisionArrayItem){
            $dataList[$json_divisionArrayItem['id']]=$json_divisionArrayItem['name'];
        }
        if (func_num_args() == 0) return $dataList;
        else if (array_key_exists($id, $dataList)) return $dataList[$id];
        else return '';
    }
    public static function getAllDistric($id=null)
    {
        $districtsFilePath='http://pluspointretail.unlockretail.com'.'/division/districts.json';
        $string = file_get_contents($districtsFilePath);
        $json_districArray = json_decode($string, true);

        $dataList=[];
        foreach ($json_districArray as $key=>$json_districArrayItem){
            $dataList[$json_districArrayItem['id']]=$json_districArrayItem['name'];
        }
        if (func_num_args() == 0) return $dataList;
        else if (array_key_exists($id, $dataList)) return $dataList[$id];
        else return '';
    }
    public static function getAllDistricByDivision($divisionId)
    {
        $districtsFilePath='http://pluspointretail.unlockretail.com'.'/division/districts.json';
        $string = file_get_contents($districtsFilePath);
        $json_districArray = json_decode($string, true);
        $dataList=[];
        foreach ($json_districArray as $key=>$json_districArrayItem){
            if ($json_districArrayItem['division_id']==$divisionId){
                $dataList[$json_districArrayItem['id']]=$json_districArrayItem['name'];
            }
        }
        return $dataList;
    }

    public static function getAllUpozela($distric)
    {
        $upozela='http://pluspointretail.unlockretail.com'.'/division/upazilas.json';
        $string = file_get_contents($upozela);
        $json_districArray = json_decode($string, true);
        $dataList=[];
        foreach ($json_districArray as $key=>$json_districArrayItem){
            if ($json_districArrayItem['district_id']==$distric){
                $dataList[$json_districArrayItem['id']]=$json_districArrayItem['name'];
            }
        }
        return $dataList;
    }

    public static function getAllUpozelaStatic($id=null)
    {
        $districtsFilePath='http://pluspointretail.unlockretail.com'.'/division/upazilas.json';
        $string = file_get_contents($districtsFilePath);
        $json_districArray = json_decode($string, true);

        $dataList=[];
        foreach ($json_districArray as $key=>$json_districArrayItem){
            $dataList[$json_districArrayItem['id']]=$json_districArrayItem['name'];
        }
        if (func_num_args() == 0) return $dataList;
        else if (array_key_exists($id, $dataList)) return $dataList[$id];
        else return '';
    }


}