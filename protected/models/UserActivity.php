<?php

/**
 * This is the model class for table "{{user_activity}}".
 *
 * The followings are the available columns in table '{{user_activity}}':
 * @property integer $id
 * @property integer $user_id
 * @property integer $brand_id
 * @property string $user_name
 * @property string $email
 * @property string $ipAddress
 * @property string $browser
 * @property string $os
 * @property string $device_name
 * @property string $lastlogin
 * @property string $lastlogout
 * @property integer $status
 */
class UserActivity extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserActivity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user_activity}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        array('user_id,crAt, branch_id, user_name, email, ipAddress, browser, os, device_name, lastlogin, lastlogout, status', 'safe'),
			array('user_id, branch_id, status', 'numerical', 'integerOnly'=>true),
			array('user_name, email', 'length', 'max'=>100),
			array('ipAddress, lastlogin, lastlogout', 'length', 'max'=>50),
			array('browser, os', 'length', 'max'=>60),
			array('device_name', 'length', 'max'=>70),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, branch_id, user_name, email, ipAddress, browser, os, device_name, lastlogin, lastlogout, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'branch_id' => 'Branch Name',
			'user_name' => 'User Name',
			'email' => 'Email',
			'ipAddress' => 'Ip Address',
			'browser' => 'Browser',
			'os' => 'OS',
			'device_name' => 'Device',
			'lastlogin' => 'Opening Shop',
			'lastlogout' => 'Closing Shop',
			'status' => 'Status',
			'crAt' => 'Login Date',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllUserActivity($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('ipAddress',$this->ipAddress,true);
		$criteria->compare('browser',$this->browser,true);
		$criteria->compare('os',$this->os,true);
		$criteria->compare('device_name',$this->device_name,true);
		$criteria->compare('lastlogin',$this->lastlogin,true);
		$criteria->compare('lastlogout',$this->lastlogout,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('crAt',$this->crAt);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}