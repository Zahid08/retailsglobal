<?php

/**
 * This is the model class for table "{{shipping_method}}".
 *
 * The followings are the available columns in table '{{shipping_method}}':
 * @property integer $id
 * @property string $title
 * @property string $day
 * @property string $image
 * @property string $description
 * @property string $isPercent
 * @property double $price
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 *
 * The followings are the available model relations:
 * @property User $moBy0
 * @property User $crBy0
 */
class ShippingMethod extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;

    const IS_PERCENT = 'Yes';
    const IS_NOT_PERCENT = 'No';

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ShippingMethod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{shipping_method}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title,day, division, description, isPercent, price, status', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('title, price', 'required'),
			array('price', 'numerical'),
			array('title', 'length', 'max'=>255),
			array('isPercent', 'length', 'max'=>3),
			array('status', 'length', 'max'=>10),
			array('crBy, moBy', 'length', 'max'=>20),
            array('image', 'file',
                'types'=>'jpg, gif, png',
                'maxSize'=>1024 * 250, // 250kb
                'tooLarge'=>'The file was larger than 250KB. Please upload a smaller file.',
                'allowEmpty'=>'true',
                'on'=>'insert'
            ),
			array('image, description, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title,day, image, description, isPercent, price, status, crAt, crBy, moAt, moBy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'division' => 'Division',
            'day' => 'Day',
            'image' => 'Image',
			'description' => 'Description',
			'isPercent' => 'Is Percent',
			'price' => 'Price',
			'status' => 'Status',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->crAt=date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
			}
			return true;
		}
		else
			return false;
	}
    
    // get all record by status as drop down
    public static function getAllShippingMethod($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'title');
	}

    // get shipping method price
    public static function getShippingMethodPrice($id,$total)
    {
        $price = 0;
        if(!empty($id))
        {
            $model = self::model()->findByPk($id);
            if($model->isPercent==self::IS_PERCENT)
                $price = round(($model->price*$total)/100);
            else $price = $model->price;
        }
        return $price;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
        $criteria->compare('day',$this->day,true);
        $criteria->compare('image',$this->image,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('isPercent',$this->isPercent,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}