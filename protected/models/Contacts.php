<?php

/**
 * This is the model class for table "{{contents}}".
 *
 * The followings are the available columns in table '{{contents}}':
 * @property string $id
 * @property string $typeId
 * @property string $descriptions
 * @property string $status
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $rank
 *
 * The followings are the available model relations:
 * @property ContentsType $type
 * @property User $crBy0
 * @property User $moBy0
 */
class Contacts extends CActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_INACTIVE=2;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Contents the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{contacts}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('first_name, last_name, email', 'required'),
            array('first_name, last_name, email, phone_number, message, crAt', 'safe'),
        );
    }



    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'message' => 'Message',
        );
    }

    /*-----------Save before functionality------------*/
    protected function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->crAt=date("Y-m-d H:i:s");
            }
            return true;
        }
        else
            return false;
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('first_name',$this->first_name,true);
        $criteria->compare('last_name',$this->last_name,true);
        $criteria->compare('phone_number',$this->phone_number,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('message',$this->message,true);

        return new CActiveDataProvider($this, array(
            'pagination'=>array(
                //
                // please check how we get the
                // the pageSize from user's state
                'pageSize'=> Yii::app()->user->getState('pageSize',
                    //
                    // we have previously set defaultPageSize
                    // on the params section of our main.php config file
                    Yii::app()->params['defaultPageSize']),
            ),
            'criteria'=>$criteria,
        ));
    }
}