<?php

/**
 * This is the model class for table "{{purchase_order}}".
 *
 * The followings are the available columns in table '{{purchase_order}}':
 * @property string $id
 * @property string $branchId 
 * @property string $supplierId
 * @property string $poNo
 * @property string $poDate
 * @property string $totalQty
 * @property double $totalPrice
 * @property string $totalWeight
 * @property string $remarks
 * @property string $message
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property GoodReceiveNote[] $goodReceiveNotes
 * @property PoDetails[] $poDetails
 * @property User $crBy0
 * @property User $moBy0
 * @property Supplier $supplier
 */
class PurchaseOrder extends CActiveRecord
{
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
    const STATUS_AUTO_GRN=3;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PurchaseOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{purchase_order}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
        	array('supplierId, poNo, totalQty, totalPrice, totalWeight', 'required'),
        	array('poNo', 'unique'),
       		//array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('totalPrice', 'numerical'),
			array('id, crBy, moBy', 'length', 'max'=>20),
			array('branchId,supplierId', 'length', 'max'=>10),
			array('poNo, remarks, message', 'length', 'max'=>250),
			array('status', 'length', 'max'=>11),
			array('status,poDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id,branchId, supplierId, poNo, poDate, totalQty, totalPrice, totalWeight, remarks, message, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'goodReceiveNotes' => array(self::HAS_MANY, 'GoodReceiveNote', 'poId'),
			'poDetails' => array(self::HAS_MANY, 'PoDetails', 'poId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
			'supplier' => array(self::BELONGS_TO, 'Supplier', 'supplierId'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchId' => 'Branch',
			'supplierId' => 'Supplier',
			'poNo' => 'Po No',
			'poDate' => 'Po Date',
			'totalQty' => 'Total Qty',
			'totalPrice' => 'Total Price',
			'totalWeight' => 'Total Weight',
			'remarks' => 'Remarks',
			'message' => 'Message',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->SESSION['branchId'];
				$this->crAt=$this->poDate = date("Y-m-d H:i:s");
				$this->crBy=Yii::app()->user->id;	
				//$this->status = self::STATUS_ACTIVE;
			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllPurchaseOrder($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
 	    $criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('supplierId',$this->supplierId,true);
		$criteria->compare('poNo',$this->poNo,true);
		$criteria->compare('poDate',$this->poDate,true);
		$criteria->compare('totalQty',$this->totalQty,true);
		$criteria->compare('totalPrice',$this->totalPrice);
		$criteria->compare('totalWeight',$this->totalWeight,true);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}