<?php

/**
 * This is the model class for table "{{stock}}".
 *
 * The followings are the available columns in table '{{stock}}':
 * @property string $id
 * @property string $branchId
 * @property string $grnId
 * @property string $itemId
 * @property string $qty
 * @property string $stockDate
 * @property double $costPrice
 * @property integer $discountPercent
 * @property string $crAt
 * @property string $crBy
 * @property string $moAt
 * @property string $moBy
 * @property string $status
 *
 * The followings are the available model relations:
 * @property GoodReceiveNote $grn
 * @property Items $item
 */
class Stock extends CActiveRecord
{
    public $itemCode;
	const STATUS_ACTIVE=1;
	const STATUS_INACTIVE=2;
	const STATUS_APPROVED=3; // after this stock will freeze
		
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Stock the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{stock}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('grnId, itemId, qty, costPrice', 'required'),
            //array('name', 'unique'),
            //array('id+name', 'application.extensions.uniqueMultiColumnValidator.uniqueMultiColumnValidator'),
			array('discountPercent', 'numerical', 'integerOnly'=>true),
			array('costPrice', 'numerical'),
			array('grnId, itemId, crBy, moBy', 'length', 'max'=>20),
			array('branchId', 'length', 'max'=>10),
			array('status', 'length', 'max'=>11),
			array('stockDate, crAt, moAt', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, branchId, grnId, itemId, qty, stockDate, costPrice, discountPercent, crAt, crBy, moAt, moBy, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'branch' => array(self::BELONGS_TO, 'Branch', 'branchId'),
			'grn' => array(self::BELONGS_TO, 'GoodReceiveNote', 'grnId'),
			'item' => array(self::BELONGS_TO, 'Items', 'itemId'),
			'crBy0' => array(self::BELONGS_TO, 'User', 'crBy'),
			'moBy0' => array(self::BELONGS_TO, 'User', 'moBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branchId' => 'Branch',
			'grnId' => 'Grn',
			'itemId' => 'Item',
			'qty' => 'Qty',
			'stockDate' => 'Stock Date',
			'costPrice' => 'Cost Price',
			'discountPercent' => 'Discount Percent',
			'crAt' => 'Cr At',
			'crBy' => 'Cr By',
			'moAt' => 'Mo At',
			'moBy' => 'Mo By',
			'status' => 'Status',
		);
	}
    
    /*-----------Save before functionality------------*/
	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->branchId = Yii::app()->session['branchId'];
				$this->crAt==$this->stockDate=date("Y-m-d H:i:s");
				$this->crBy = Yii::app()->user->id;

                if(isset($_REQUEST['ItemsVariation'])){
                    if(!empty($_REQUEST['ItemsVariation'])){
                        $this->status = 3;
                    }
                } else{
                    $this->status = self::STATUS_ACTIVE;
                }


			}
			return true;
		}
		else
			return false;
	}
    
    //-----------get all record by status as dropdown--------//
    public static function getAllStock($isActive)
	{      
		 return  CHtml::listData(self::model()->findAll(array('condition'=>'status=:status', 'params'=>array(':status'=>$isActive))), 'id', 'name');      
	}
	
	// get item wise stock
	public static function getItemWiseStock($itemId)
	{    
		 $stock = $grn = $inventory = $sells = $damage = $poreturn = $salesreturn = $inStock = $outStock = 0;
		 $db = Yii::app()->db;  
		 
		 // stock calculations
		 $sqlstock = "SELECT SUM(qty) AS qty FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".self::STATUS_APPROVED;
		 $resstock = $db->createCommand($sqlstock)->queryRow();
		 if(!empty($resstock['qty'])) $grn   =  $resstock['qty'];	
		 
		 // inventory adjustment ( always sum with stock +)	with close status
		 //$sqlinv = "SELECT adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC LIMIT 0,1";

		 $sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC";
		 $resinv = $db->createCommand($sqlinv)->queryRow();
		 if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];
		 
		 // sales return calculations
		 $sqsalesreturn = "SELECT SUM(qty) AS qty FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
		 $ressalesreturn = $db->createCommand($sqsalesreturn)->queryRow();
		 if(!empty($ressalesreturn['qty'])) $salesreturn   =  $ressalesreturn['qty'];

        // stock transfer in calculations
        $sqlInStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE toBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".self::STATUS_ACTIVE;
        $resInStock = $db->createCommand($sqlInStock)->queryRow();
        if(!empty($resInStock['qty'])) $inStock   =  $resInStock['qty'];

        // sells calculations
		$sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']." AND s.itemId=".$itemId;
		$ressells = $db->createCommand($sqlsells)->queryRow();
		if(!empty($ressells['qty'])) $sells   =  $ressells['qty'];
		 
		// damage/wastage calculations
		$sqldamwas = "SELECT SUM(qty) AS qty FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
		$resdamwas = $db->createCommand($sqldamwas)->queryRow();
		if(!empty($resdamwas['qty'])) $damage    =  $resdamwas['qty'];
		 
		// po return calculations
		$sqporeturn = "SELECT SUM(qty) AS qty FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
		$resporeturn = $db->createCommand($sqporeturn)->queryRow();
		if(!empty($resporeturn['qty'])) $poreturn   =  $resporeturn['qty'];

        // stock transfer out calculations
        $sqlOutStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".self::STATUS_ACTIVE;
        $resOutStock = $db->createCommand($sqlOutStock)->queryRow();
        if(!empty($resOutStock['qty'])) $outStock   =  $resOutStock['qty'];

        //$stock = round($grn + $salesreturn + $inStock) - round($sells + $damage + $poreturn + $outStock);
        $stock = ($grn + $salesreturn + $inStock) - ($sells + $damage + $poreturn + $outStock);
		$stock = $stock + ($inventory);
		return round($stock,2);
	}


    // Stock Transfer get item wise stock
    public static function getStItemWiseStock($itemId)
    {
        $stock = $grn = $inventory = $sells = $damage = $poreturn = $salesreturn = $inStock = $outStock = 0;
        $db = Yii::app()->db;

        // stock calculations
        $sqlstock = "SELECT SUM(qty) AS qty FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".self::STATUS_APPROVED;
        $resstock = $db->createCommand($sqlstock)->queryRow();
        if(!empty($resstock['qty'])) $grn   =  $resstock['qty'];

        // inventory adjustment ( always sum with stock +)	with close status
        //$sqlinv = "SELECT adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC LIMIT 0,1";

        $sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC";
        $resinv = $db->createCommand($sqlinv)->queryRow();
        if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];

        // sales return calculations
        $sqsalesreturn = "SELECT SUM(qty) AS qty FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
        $ressalesreturn = $db->createCommand($sqsalesreturn)->queryRow();
        if(!empty($ressalesreturn['qty'])) $salesreturn   =  $ressalesreturn['qty'];

        // stock transfer in calculations
        $sqlInStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE toBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
        $resInStock = $db->createCommand($sqlInStock)->queryRow();
        if(!empty($resInStock['qty'])) $inStock   =  $resInStock['qty'];

        // sells calculations
        $sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']." AND s.itemId=".$itemId;
        $ressells = $db->createCommand($sqlsells)->queryRow();
        if(!empty($ressells['qty'])) $sells   =  $ressells['qty'];

        // damage/wastage calculations
        $sqldamwas = "SELECT SUM(qty) AS qty FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
        $resdamwas = $db->createCommand($sqldamwas)->queryRow();
        if(!empty($resdamwas['qty'])) $damage    =  $resdamwas['qty'];

        // po return calculations
        $sqporeturn = "SELECT SUM(qty) AS qty FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
        $resporeturn = $db->createCommand($sqporeturn)->queryRow();
        if(!empty($resporeturn['qty'])) $poreturn   =  $resporeturn['qty'];

        // stock transfer out calculations
        $sqlOutStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId;
        $resOutStock = $db->createCommand($sqlOutStock)->queryRow();
        if(!empty($resOutStock['qty'])) $outStock   =  $resOutStock['qty'];

        //$stock = round($grn + $salesreturn + $inStock) - round($sells + $damage + $poreturn + $outStock);
        $stock = ($grn + $salesreturn + $inStock) - ($sells + $damage + $poreturn + $outStock);
        $stock = $stock + ($inventory);
        return round($stock,2);
    }

    // get item wise stock by date end
    public static function getItemWiseStockByDate($itemId,$endDate)
    {
        $stock = $grn = $inventory = $sells = $damage = $poreturn = $salesreturn = $inStock = $outStock = 0;
        $db = Yii::app()->db;

        // stock calculations
        $sqlstock = "SELECT SUM(qty) AS qty FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND status=".self::STATUS_APPROVED;
        $resstock = $db->createCommand($sqlstock)->queryRow();
        if(!empty($resstock['qty'])) $grn   =  $resstock['qty'];

        // inventory adjustment ( always sum with stock +)	with close status
        //$sqlinv = "SELECT adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC LIMIT 0,1";

        $sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND DATE_FORMAT(invDate, '%Y-%m-%d')<='".$endDate."'
                   AND status=".Inventory::STATUS_CLOSE." order by invDate DESC";
        $resinv = $db->createCommand($sqlinv)->queryRow();
        if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];

        // sales return calculations
        $sqsalesreturn = "SELECT SUM(qty) AS qty FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(returnDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $ressalesreturn = $db->createCommand($sqsalesreturn)->queryRow();
        if(!empty($ressalesreturn['qty'])) $salesreturn   =  $ressalesreturn['qty'];

        // stock transfer in calculations
        $sqlInStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE toBranchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resInStock = $db->createCommand($sqlInStock)->queryRow();
        if(!empty($resInStock['qty'])) $inStock   =  $resInStock['qty'];

        // sells calculations
        $sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(i.orderDate, '%Y-%m-%d')<='".$endDate."' AND s.itemId=".$itemId;
        $ressells = $db->createCommand($sqlsells)->queryRow();
        if(!empty($ressells['qty'])) $sells   =  $ressells['qty'];

        // damage/wastage calculations
        $sqldamwas = "SELECT SUM(qty) AS qty FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(damDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resdamwas = $db->createCommand($sqldamwas)->queryRow();
        if(!empty($resdamwas['qty'])) $damage    =  $resdamwas['qty'];

        // po return calculations
        $sqporeturn = "SELECT SUM(qty) AS qty FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(prDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resporeturn = $db->createCommand($sqporeturn)->queryRow();
        if(!empty($resporeturn['qty'])) $poreturn   =  $resporeturn['qty'];

        // stock transfer out calculations
        $sqlOutStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resOutStock = $db->createCommand($sqlOutStock)->queryRow();
        if(!empty($resOutStock['qty'])) $outStock   =  $resOutStock['qty'];

        // $stock = round($grn + $salesreturn + $inStock) - round($sells + $damage + $poreturn + $outStock);
        $stock = ($grn + $salesreturn + $inStock) - ($sells + $damage + $poreturn + $outStock);
        $stock = $stock + ($inventory);
        return round($stock,2);
    }

    // get item wise stock by date end branch wise
    public static function getBranchWiseItemStockByDate($branch,$itemId,$endDate)
    {
        $stock = $grn = $inventory = $sells = $damage = $poreturn = $salesreturn = $inStock = $outStock = 0;
        $db = Yii::app()->db;

        // stock calculations
        $sqlstock = "SELECT SUM(qty) AS qty FROM pos_stock WHERE branchId=".$branch." AND itemId=".$itemId." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND status=".self::STATUS_APPROVED;
        $resstock = $db->createCommand($sqlstock)->queryRow();
        if(!empty($resstock['qty'])) $grn   =  $resstock['qty'];

        // inventory adjustment ( always sum with stock +)	with close status
        //$sqlinv = "SELECT adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC LIMIT 0,1";

        $sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".$branch." AND itemId=".$itemId." AND DATE_FORMAT(invDate, '%Y-%m-%d')<='".$endDate."'
                   AND status=".Inventory::STATUS_CLOSE." order by invDate DESC";
        $resinv = $db->createCommand($sqlinv)->queryRow();
        if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];

        // sales return calculations
        $sqsalesreturn = "SELECT SUM(qty) AS qty FROM pos_sales_return WHERE branchId=".$branch." AND DATE_FORMAT(returnDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $ressalesreturn = $db->createCommand($sqsalesreturn)->queryRow();
        if(!empty($ressalesreturn['qty'])) $salesreturn   =  $ressalesreturn['qty'];

        // stock transfer in calculations
        $sqlInStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE toBranchId=".$branch." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resInStock = $db->createCommand($sqlInStock)->queryRow();
        if(!empty($resInStock['qty'])) $inStock   =  $resInStock['qty'];

        // sells calculations
        $sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".$branch." AND DATE_FORMAT(i.orderDate, '%Y-%m-%d')<='".$endDate."' AND s.itemId=".$itemId;
        $ressells = $db->createCommand($sqlsells)->queryRow();
        if(!empty($ressells['qty'])) $sells   =  $ressells['qty'];

        // damage/wastage calculations
        $sqldamwas = "SELECT SUM(qty) AS qty FROM pos_damage_wastage WHERE branchId=".$branch." AND DATE_FORMAT(damDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resdamwas = $db->createCommand($sqldamwas)->queryRow();
        if(!empty($resdamwas['qty'])) $damage    =  $resdamwas['qty'];

        // po return calculations
        $sqporeturn = "SELECT SUM(qty) AS qty FROM pos_po_returns WHERE branchId=".$branch." AND DATE_FORMAT(prDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resporeturn = $db->createCommand($sqporeturn)->queryRow();
        if(!empty($resporeturn['qty'])) $poreturn   =  $resporeturn['qty'];

        // stock transfer out calculations
        $sqlOutStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE fromBranchId=".$branch." AND DATE_FORMAT(stockDate, '%Y-%m-%d')<='".$endDate."' AND itemId=".$itemId;
        $resOutStock = $db->createCommand($sqlOutStock)->queryRow();
        if(!empty($resOutStock['qty'])) $outStock   =  $resOutStock['qty'];

        //$stock = round($grn + $salesreturn + $inStock) - round($sells + $damage + $poreturn + $outStock);
        $stock = ($grn + $salesreturn + $inStock) - ($sells + $damage + $poreturn + $outStock);
        $stock = $stock + ($inventory);
        return round($stock,2);
    }
	
	// get item wise stock while inventory
	public static function getItemWiseStockWhileInventory($itemId,$invDate)
	{    
		 $stock = $grn = $inventory = $sells = $damage = $poreturn = $salesreturn = $inStock = $outStock = 0;
		 $db = Yii::app()->db;  
		 
		 // stock calculations
		 $sqlstock = "SELECT SUM(qty) AS qty FROM pos_stock WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".self::STATUS_APPROVED;
		 $resstock = $db->createCommand($sqlstock)->queryRow();
		 if(!empty($resstock['qty'])) $grn   =  $resstock['qty'];	
		 
		 // inventory adjustment ( always sum with stock +)	with close status
		 //$sqlinv = "SELECT adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC LIMIT 0,1";
		 
		 $sqlinv = "SELECT sum(adjustQty) as adjustQty FROM pos_inventory WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND status=".Inventory::STATUS_CLOSE." order by invDate DESC";
		 $resinv = $db->createCommand($sqlinv)->queryRow();
		 if(!empty($resinv['adjustQty'])) $inventory  =  $resinv['adjustQty'];
		 
		 // sales return calculations
		 $sqsalesreturn = "SELECT SUM(qty) AS qty FROM pos_sales_return WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND returnDate<'".$invDate."'";
		 $ressalesreturn = $db->createCommand($sqsalesreturn)->queryRow();
		 if(!empty($ressalesreturn['qty'])) $salesreturn   =  $ressalesreturn['qty'];

        // stock transfer in calculations
        $sqlInStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE toBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND stockDate<'".$invDate."'";
        $resInStock = $db->createCommand($sqlInStock)->queryRow();
        if(!empty($resInStock['qty'])) $inStock   =  $resInStock['qty'];

        // sells calculations
		$sqlsells = "SELECT SUM(s.qty) AS qty FROM pos_sales_invoice i, pos_sales_details s WHERE i.id=s.salesId AND i.branchId=".Yii::app()->session['branchId']."
		             AND s.itemId=".$itemId." AND orderDate<'".$invDate."'";
		$ressells = $db->createCommand($sqlsells)->queryRow();
		if(!empty($ressells['qty'])) $sells   =  $ressells['qty'];
		 
		// damage/wastage calculations
		$sqldamwas = "SELECT SUM(qty) AS qty FROM pos_damage_wastage WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND damDate<'".$invDate."'";
		$resdamwas = $db->createCommand($sqldamwas)->queryRow();
		if(!empty($resdamwas['qty'])) $damage  =  $resdamwas['qty'];
		 
		// po return calculations
		$sqporeturn = "SELECT SUM(qty) AS qty FROM pos_po_returns WHERE branchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND prDate<'".$invDate."'";
		$resporeturn = $db->createCommand($sqporeturn)->queryRow();
		if(!empty($resporeturn['qty'])) $poreturn   =  $resporeturn['qty'];

        // stock transfer out calculations
        $sqlOutStock = "SELECT SUM(qty) AS qty FROM pos_stock_transfer WHERE fromBranchId=".Yii::app()->session['branchId']." AND itemId=".$itemId." AND stockDate<'".$invDate."'";
        $resOutStock = $db->createCommand($sqlOutStock)->queryRow();
        if(!empty($resOutStock['qty'])) $outStock   =  $resOutStock['qty'];

        //$stock = round($grn + $salesreturn + $inStock) - round($sells + $damage + $poreturn + $outStock);
        $stock = ($grn + $salesreturn + $inStock) - ($sells + $damage + $poreturn + $outStock);
		$stock = $stock + ($inventory);
		return round($stock,2);
	}

    // item fifo wise costPrice
    public static function getItemFifoCostPrice($itemId,$currentStock)
    {
        $itemModel = Items::model()->findByPk($itemId);

        // negative stock then current cost price
        if($currentStock<=0) $costPrice = $currentStock*$itemModel->costPrice;
        else
        {
            // finding item actual cost price FIFO (N.B : if negative stock then get current stock price) rules = currentStock-grnQtySumByDesc == 0
            $costPrice = $stockSum = $costPriceSum = 0;

            // general stock & stock transfer model
            $stockModel = array();
            $stockSql = "SELECT qty,stockDate,costPrice FROM pos_stock  WHERE itemId=".$itemId." AND branchId=".Yii::app()->session['branchId']." AND status=".self::STATUS_APPROVED." UNION
                         SELECT qty,stockDate,costPrice FROM pos_stock_transfer WHERE itemId=".$itemId." AND toBranchId=".Yii::app()->session['branchId']." AND status=".StockTransfer::STATUS_ACTIVE." ORDER BY stockDate DESC";
            $stockModel = Stock::model()->findAllBySql($stockSql);
            if (!empty($stockModel)) {
                foreach ($stockModel as $sKey => $sData) :
                    $stockSum += $sData->qty;
                    $costPriceSum+=$sData->qty*$sData->costPrice;
                    if($currentStock<=$stockSum)
                    {
                        $costPrice = (($currentStock-$stockSum)*$sData->costPrice)+$costPriceSum;
                        break;
                    }
                endforeach;
            }
            else $costPrice = $currentStock*$itemModel->costPrice;
        }
        return $costPrice;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('branchId',$this->branchId,true);
		$criteria->compare('grnId',$this->grnId,true);
		$criteria->compare('itemId',$this->itemId,true);
		$criteria->compare('qty',$this->qty,true);
		$criteria->compare('stockDate',$this->stockDate,true);
		$criteria->compare('costPrice',$this->costPrice);
		$criteria->compare('discountPercent',$this->discountPercent);
		$criteria->compare('crAt',$this->crAt,true);
		$criteria->compare('crBy',$this->crBy,true);
		$criteria->compare('moAt',$this->moAt,true);
		$criteria->compare('moBy',$this->moBy,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
        	'pagination'=>array(
				//
				// please check how we get the
				// the pageSize from user's state
				'pageSize'=> Yii::app()->user->getState('pageSize',
				//
				// we have previously set defaultPageSize
				// on the params section of our main.php config file
				Yii::app()->params['defaultPageSize']),
			),
			'criteria'=>$criteria,
		));
	}
}