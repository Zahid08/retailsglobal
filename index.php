<?php

/*
   - Yii Framework version 1.1.16
   - server default settings
*/
//ini_set('memory_limit', '64M');
//ini_set('max_execution_time', '1800'); // 30*60 Minuties
date_default_timezone_set('Asia/Dhaka');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

// change the following paths if necessary
$yii=dirname(__FILE__).'/core/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',false);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();

