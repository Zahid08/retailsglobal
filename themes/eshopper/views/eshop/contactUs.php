<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<?php 
	$socialModel = SocialNetwork::model()->findAll('status=:status',array(':status'=>SocialNetwork::STATUS_ACTIVE));
?>
<div class="container" id="contact-page">
	<div class="bg">
		<?php if(Yii::app()->user->hasFlash('contact')): ?>
	    	<div class="row">    		
	    		<div class="col-sm-12">    			   			
				    <div class="status alert alert-success"><?php echo Yii::app()->user->getFlash('contact'); ?></div>			 		
				</div>			
			</div>
		<?php endif; ?>
		<div class="row">  	
    		<div class="col-sm-8">
    			<div class="contact-form form">
    				<h2 class="title text-center">Get In Touch</h2>
    				<div style="display: none" class="status alert alert-success"></div>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                            ),
                            'htmlOptions'=>array(
                                'class'=>'contact-form row',
                            ),
                        )); ?>
						<?php echo $form->errorSummary($model); ?>
			            <div class="form-group col-md-6">
			                <?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
							<?php echo $form->error($model,'name'); ?>
			            </div>
			            <div class="form-group col-md-6">			                
			                <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
							<?php echo $form->error($model,'email'); ?> 
			            </div>
			            <div class="form-group col-md-12">
			                <?php echo $form->textField($model,'subject',array('class'=>'form-control','placeholder'=>'Subject')); ?>
							<?php echo $form->error($model,'subject'); ?> 
			            </div>			          
			            <div class="form-group col-md-12">
																					            
			                <?php echo $form->textArea($model,'body',array('rows'=>'4','cols'=>'50','class'=>'formText','placeholder'=>'Your Message Here')); ?>
							<?php echo $form->error($model,'body'); ?>
			            </div> 
		            
			            <div class="form-group col-md-6">	
			                <?php if(CCaptcha::checkRequirements()): ?>		              
								<?php $this->widget('CCaptcha'); ?>
								<?php echo $form->textField($model,'verifyCode'); ?>
							    <?php echo $form->error($model,'verifyCode'); ?>
						    <?php endif; ?> 
			            </div>		            

			            <div class="form-group col-md-6">			                
			                <?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-primary pull-right')); ?>
			            </div>			            
			      <?php $this->endWidget(); ?>
    			</div>
    		</div>
    		<div class="col-sm-4">
    			<div class="contact-info">
    				<h2 class="title text-center">Contact Info</h2>
    				<address>
    					<p><?php echo $companyModel->name;?></p>
						<p><?php echo $branchModel->addressline.'&nbsp,'.$branchModel->city.'&nbsp,'.$branchModel->postalcode;?></p>
						<p>Mobile : <?php echo $branchModel->phone;?></p>
						<p>Email : <?php echo $companyModel->email;?></p>
    				</address>
    				<div class="social-networks">
    					<?php if(!empty($socialModel)): ?>
    					<h2 class="title text-center">Social Networking</h2>    					
						<ul>
							<?php foreach ($socialModel as $key => $socialData):
								$socialNet = SocialNetwork::getDomainName($socialData->url);
							 ?>
                                <?php if($socialNet=='facebook'){ ?>
                                    <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-facebook"></i></a></li>
                                <?php } else if($socialNet=='twitter'){ ?> 
                                    <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-twitter"></i></a></li>
                                <?php } else if($socialNet=='linkedin'){ ?> 
                                    <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                                <?php } else if($socialNet=='dribbble'){ ?> 
                                    <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-dribbble"></i></a></li>
                                <?php }?>  
                            <?php endforeach; ?>   					
						</ul>
						<?php endif;?>
    				</div>
    			</div>
			</div>    			
    	</div>  
	</div>	
</div>
<style>
	.formText
	{
		background-color: #fff;
	    border: 1px solid #ddd;
	    border-radius: 4px;
	    box-shadow: inherit;
	    color: #696763;
	    font-size: 16px;																							
	    padding: 6px 12px;
	    width: 100%;
	}
	.formText:focus,.formText:hover
	{
	  border-color: #fdb45e;
	  box-shadow: inherit;	  
	}
	.formText:focus,.formText.error:hover
	{
	  border-color: #fdb45e;
	  box-shadow: inherit;	 border: 1px solid #fe980f; 
	}
	div.form div.error input, div.form div.error textarea, div.form div.error select, div.form input.error, div.form textarea.error, div.form select.error {
        background-color: #fff;
	    border: 1px solid #ddd;
	    
	}

</style>