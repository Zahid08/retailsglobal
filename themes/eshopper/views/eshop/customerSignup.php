<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<section>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';//echo $catModel->subdept->dept->name;?></a></li>
                <li class="active"><?php echo 'Signup'; ?></li>
            </ol>
        </div>	
    </div>
</section><!-- ========================================= BREADCRUMB : END ========================================= -->

<!-- END PAGE TITLE & BREADCRUMB-->

<section id="form"><!--form-->
    <div class="container">
    <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>
        <div class="row">
            <div class="col-sm-5 form">
                <div class="login-form"><!--login form-->
                    <h2 class="bordered">Create New Account As Supplier</h2>
                <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'supplier-form',
                    'enableAjaxValidation'=>true,
                    'htmlOptions'=>array(
                        'class'=>'login-form cf-style-1',
                        'role'=>'form'
                    )
                )); 
                echo $form->hiddenField($modelSupplier,'suppId', array('value'=>'SN'.date("ymdhis")));?>
                <?php echo $form->errorSummary($modelSupplier); ?>
                
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'name'); ?>
                    <?php echo $form->textField($modelSupplier,'name',array('class'=>'le-input','size'=>50,'maxlength'=>50,'placeholder'=>'Name')); ?>
                    <?php echo $form->error($modelSupplier,'name'); ?>
                </div>
                
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'email'); ?>
                    <?php echo $form->textField($modelSupplier,'email',array('class'=>'le-input','size'=>60,'maxlength'=>150,'placeholder'=>'Email')); ?>
                    <?php echo $form->error($modelSupplier,'email'); ?>
                </div>
                
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'address'); ?>
                    <?php echo $form->textArea($modelSupplier,'address',array('class'=>'le-input','size'=>60,'maxlength'=>250,'placeholder'=>'Address')); ?>
                    <?php echo $form->error($modelSupplier,'address'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'city'); ?>
                    <?php echo $form->textField($modelSupplier,'city',array('class'=>'le-input','size'=>60,'maxlength'=>150,'placeholder'=>'City')); ?>
                    <?php echo $form->error($modelSupplier,'city'); ?>
                </div>

                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'phone'); ?>
                    <?php echo $form->textField($modelSupplier,'phone',array('class'=>'le-input','size'=>60,'maxlength'=>150,'placeholder'=>'Phone')); ?>
                    <?php echo $form->error($modelSupplier,'phone'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'username'); ?>
                    <?php echo $form->textField($modelSupplier,'username',array('class'=>'le-input','placeholder'=>'Username')); ?>
                    <?php echo $form->error($modelSupplier,'username'); ?>
                </div>

                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'password'); ?>
                    <?php echo $form->passwordField($modelSupplier,'password',array('value'=>'','class'=>'le-input','placeholder'=>'Password')); ?>
                    <?php echo $form->error($modelSupplier,'password'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelSupplier,'conf_password'); ?>       
                    <?php echo $form->passwordField($modelSupplier,'conf_password',array('value'=>'','class'=>'le-input','placeholder'=>'Retype password')); ?>
                    <?php echo $form->error($modelSupplier,'conf_password'); ?>
                </div>
                <div class="buttons-holder">
                 <?php echo CHtml::submitButton('Sign Up', array('class'=>'btn btn-default','style'=>'width:120px')); ?>
                </div><!-- /.buttons-holder -->
                <?php $this->endWidget(); ?>
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-5 form">
                <div class="signup-form"><!--sign up form-->
                <h2 class="bordered">Create New Account as Customer </h2>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'customer-form',
                        'enableAjaxValidation'=>true,
                        'htmlOptions'=>array(
                            'class'=>'login-form cf-style-1',   
                        )
                    )); ?>
                <?php echo $form->errorSummary($modelCustomer); ?>
                <?php echo $form->hiddenField($modelCustomer,'custId', array('id'=>'custId','class'=>'le-input','readonly'=>'true', 'value'=>'CN'.date("Ymdhis"))); ?>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'title'); ?>
                    <?php echo $form->dropDownList($modelCustomer,'title', UsefulFunction::getCustomerTitle(), array('class'=>'le-input','prompt'=>'Select Title')); ?>
                    <?php echo $form->error($modelCustomer,'title'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'name'); ?>
                    <?php echo $form->textField($modelCustomer,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Name')); ?>
                    <?php echo $form->error($modelCustomer,'name'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'email'); ?>
                    <?php echo $form->textField($modelCustomer,'email',array('class'=>'le-input','size'=>60,'maxlength'=>155,'placeholder'=>'Email')); ?>
                    <?php echo $form->error($modelCustomer,'email'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'addressline'); ?>
                    <?php echo $form->textArea($modelCustomer,'addressline',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Addressline')); ?>
                    <?php echo $form->error($modelCustomer,'addressline'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'city'); ?>
                    <?php echo $form->textField($modelCustomer,'city',array('class'=>'le-input','size'=>60,'maxlength'=>150,'placeholder'=>'City')); ?>
                    <?php echo $form->error($modelCustomer,'city'); ?>
                </div>
                <div class="field-row form-group">
                    <?php // echo $form->labelEx($modelCustomer,'phone'); ?>
                    <?php echo $form->textField($modelCustomer,'phone',array('class'=>'le-input','size'=>60,'maxlength'=>155,'placeholder'=>'Phone')); ?>
                    <?php echo $form->error($modelCustomer,'phone'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'gender'); ?>
                    <?php echo $form->dropDownList($modelCustomer,'gender', UsefulFunction::getCustomerGender(), array('class'=>'le-input','prompt'=>'Select Gender')); ?>
                    <?php echo $form->error($modelCustomer,'gender'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'username'); ?>
                    <?php echo $form->textField($modelCustomer,'username',array('class'=>'le-input','placeholder'=>'username')); ?>
                    <?php echo $form->error($modelCustomer,'username'); ?>
                </div>

                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'password'); ?>
                    <?php echo $form->passwordField($modelCustomer,'password',array('value'=>'','class'=>'le-input','placeholder'=>'Password')); ?>
                    <?php echo $form->error($modelCustomer,'password'); ?>
                </div>
                <div class="field-row form-group">
                    <?php //echo $form->labelEx($modelCustomer,'conf_password'); ?>       
                    <?php echo $form->passwordField($modelCustomer,'conf_password',array('value'=>'','class'=>'le-input','placeholder'=>'Retype password')); ?>
                    <?php echo $form->error($modelCustomer,'conf_password'); ?>
                </div>
                <div class="buttons-holder form-group">
                    <?php echo CHtml::submitButton('Sign Up', array('class'=>'btn btn-default custom-btn','style'=>'width:120px')); ?>
                </div><!-- /.buttons-holder -->
                <?php $this->endWidget(); ?>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section><!--/form-->
<style type="text/css">
  .btn.btn-default.custom-btn{
      background: none repeat scroll 0 0 #fe980f;
      border: medium none;
      border-radius: 0;
      color: #ffffff;
      display: block;
      font-family: "Roboto",sans-serif;
      padding: 6px 25px;
    }
</style>