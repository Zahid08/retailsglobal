<?php $themeBasePath = Yii::app()->theme->baseUrl.'/assests/'; ?>
<script>
 $(function() {
	// item quantity updated +/-
	$(".quantity").click(function() {
		var itemId = $(this).attr('id');
		var quantity = $("#quantity_"+itemId).val();	

		// price calculation
		var item_price     = parseFloat($("#priceInd_"+itemId).val());
		var item_price_upd = item_price*quantity;
		//console.log(item_price);
		console.log(quantity);
		
		$.ajax({
			type: "POST",
			dataType: "json",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/updateQty/",
			data: {
				itemId: itemId, 
				quantity : quantity
			},					
			success: function(data) {
				$(".preloader").hide();

				// update price
				$("#price_"+itemId).html(item_price_upd);
				$("#total_subtotal").html(data.total_price);
                $("#total_discount").html(data.total_discount);
                var dataGrandTotal = (data.total_price-data.total_discount);
				$("#total_grandtotal").html(dataGrandTotal);
				
				// update shopping bag 
				$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
			},
			error: function() {}
		});	
	});
	 
	// remove from cart global
	$(".remove_from_cart_my_cart").click(function() 
	{
		// values & validations
		var itemId = $(this).attr('id');
		
		// price calculation
		var item_price     = parseFloat($("#price_"+itemId).html());
		var total_subtotal = parseFloat($("#total_subtotal").html());
		var totalbal_subtotal = total_subtotal-item_price;
		
		var total_grandtotal = parseFloat($("#total_grandtotal").html());
		var totalbal_grandtotal = total_grandtotal-item_price;
		
		$.ajax({
			type: "POST",
			dataType: "json",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
			data: {
				itemId: itemId, 
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#cart_item_holder_"+itemId).fadeOut(500, function() {
					$(this).remove();
				});
				
				// update price
				$("#total_subtotal").html(totalbal_subtotal);
				$("#total_grandtotal").html(totalbal_grandtotal);
				
				// update shopping bag 
				$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
			},
			error: function() {}
		});	
	});
	 
	// item moved to wishlist
	$(".move_to_wishlist").click(function() {
		<?php if(!empty(Yii::app()->session['custId'])) : ?>
			var itemId = $(this).attr('id');

			// price calculation
			var item_price     = parseFloat($("#price_"+itemId).html());
			var total_subtotal = parseFloat($("#total_subtotal").html());
			var totalbal_subtotal = total_subtotal-item_price;

			var total_grandtotal = parseFloat($("#total_grandtotal").html());
			var totalbal_grandtotal = total_grandtotal-item_price;

			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
				data: {
					itemId: itemId, 
				},					
				success: function(data) {
					$(".preloader").hide();
					if(data=='exists') alert("Already added to wishlist !");
					else 
					{
						$("#cart_item_holder_"+itemId).fadeOut(500, function() {
							$(this).remove();
						});

						// update price
						$("#total_subtotal").html(totalbal_subtotal);
						$("#total_grandtotal").html(totalbal_grandtotal);

						// update wishlist count
						$("#wishListCount").html(data);
						// update shopping bag 
						$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
					}
				},
				error: function() {}
			});
		<?php else : ?> alert("please signin first !"); <?php endif;?>
	});
});


// Quantity Spinner


$(document).ready(function (){	
	$('.le-quantity a').click(function(e){	
	    e.preventDefault();
	    var currentQty= $(this).parent().parent().find('input').val();
	    if( $(this).hasClass('minus') && currentQty>1){
	        $(this).parent().parent().find('input').val(parseInt(currentQty, 10) - 1);
	    }else{
	        if( $(this).hasClass('plus')){
	            $(this).parent().parent().find('input').val(parseInt(currentQty, 10) + 1);
	        }
	    }
	});
});

</script>

<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">
			  <li><a href="javascript:void(0);">Home</a></li>
			  <li class="active"><?php echo $this->metaTitle;?></li>			 
			</ol>
		</div>	
	</div>
</section>

<?php if($total_qty>0) : ?>
	<section id="cart_items">
		<div class="container">			
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class=""></td>
							<td>Sl. No</td>
							<td class="image">Image</td>
							<td class="description">Product Name</td>							
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
							
						</tr>
					</thead>
					<tbody>
						<?php $slNo=0; foreach ($cart->getItems() as $order_code=>$quantity) : $slNo++;?>
							<tr class="row no-margin cart-item" id="cart_item_holder_<?php echo $order_code;?>">
								<td>
									<?php echo $slNo;?>
								</td>
								<td class="cart_product">
									<?php if(!empty($cart->getItemImages($order_code))) : echo CHtml::link('<img class="lazy" alt="" src="'.$cart->getItemImages($order_code).'" style="width:110px;height:110px;" />',array('eshop/proDetails','id'=>$order_code),  array('class'=>'thumb-holder','title'=>$cart->getItemName($order_code)));?>
									<?php endif;?>
								</td>
								<td class="cart_description">
									<h4>
										<?php echo CHtml::link($cart->getItemName($order_code),array('eshop/proDetails','id'=>$order_code),  array('title'=>$cart->getItemName($order_code)));?>
									</h4>
									<p><?php echo $cart->getItemBrand($order_code);?></p>
									<p><a id="<?php echo $order_code;?>" class="move_to_wishlist" href="javascript:void(0);" title="move to wishlist"><i class="fa fa-heart"></i> move to wishlist</a></p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button quantity" id="<?php echo $order_code;?>">
										<div class="le-quantity">
											<a class="cart_quantity_up minus" href="#reduce"> - </a>
											<input id="quantity_<?php echo $order_code;?>" class="cart_quantity_input" type="text" name="quantity" readonly=''  value="<?php echo $quantity;?>" autocomplete="off" size="2">
											<a class="cart_quantity_down plus" href="#add"> + </a>
										</div>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">
										<?php if(Items::getItemWiseOfferPrice($order_code)>0) :?>
											<input type="hidden" id="priceInd_<?php echo $order_code;?>" value="<?php echo Items::getItemWiseOfferPrice($order_code);?>" />
											<?php echo Company::getCurrency();?> <span id="price_<?php echo $order_code;?>"><?php echo Items::getItemWiseOfferPrice($order_code)*$quantity;?></span>
										<?php else : ?>
											<input type="hidden" id="priceInd_<?php echo $order_code;?>" value="<?php echo $cart->getItemPrice($order_code);?>" />
											<?php echo Company::getCurrency();?> <span id="price_<?php echo $order_code;?>"><?php echo $cart->getItemPrice($order_code)*$quantity;?></span>
										<?php endif;?>
									</p>									
								</td>
								<td class="cart_delete">
									<a id="<?php echo $order_code;?>" class="cart_quantity_delete close-btn remove_from_cart_my_cart" href="javascript:void(0);"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						<?php endforeach;?>
					</tbody>
				</table>
			</div>		
		</div>
	</section> <!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
				</div>
				<div class="col-sm-6">
					<div class="total_area">
                        <ul class="list-unstyled">
							<li>Cart Sub Total <span><?php echo Company::getCurrency();?>&nbsp;&nbsp;<span id="total_subtotal"><?php echo $total_price;?></span></span></li>
							<li>Discount <span><?php echo Company::getCurrency();?>&nbsp;&nbsp;<span id="total_discount"><?php echo $total_discount;?></span></span></li>
							<li>Total <span><?php echo Company::getCurrency();?>&nbsp;&nbsp;<span id="total_grandtotal"><?php echo ($total_price-$total_discount);?></span></span></li>
						</ul>
                        <a class="btn btn-default check_out" href="javascript: window.history.go(-1);" >Continue shopping</a>
                        <?php echo CHtml::link('Check Out',array('checkoutProcess/checkout'), array('class'=>'btn btn-default check_out pull-right'))?>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
<?php 	else : ?> 
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<div class="table-responsive cart_info">
					Your Cart Is Empty !
				</div>
			</div>	
		</div>
	</section>
<?php endif;?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php $this->widget('EshopRecommanded',array('limit'=>4));?> 
		</div>
	</div>
</div>
