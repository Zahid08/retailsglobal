
<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 padding-left">
				<div class="left-sidebar">
					<!-- LEFT CAtegoty Manu-->
					<?php $this->widget('EshopLeftMenu');?>
					<!-- LEFT CAtegoty Manu-->
				</div>
				<div class="badges-post">
					<ul class="list-unstyled">
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-trophy"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-reply"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-shield"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<div class="media">
								<div class="media-left">
									<div class="badges-icon">
										<i class="fa fa-money"></i>
									</div>
								</div>
								<div class="media-body">
									<h4>Top Rated Merchants</h4>
									<p>View our top rated merchants</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<?php $this->widget('EshopAdvertisement',array('position'=>'left'));?>
			</div>
			<div class="col-sm-9 padding-right padding-left">
				<div class="portlet slider clearfix" id="yw2">

					<?php if(!empty($sliderModel)) : ?>
	<section id="slider"><!--slider-->
		<div id="slider-carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<?php foreach($sliderModel as $key => $value) : if($key==0) {$ac='active';} else {$ac='';}?>
					 <li data-target="#slider-carousel" data-slide-to="<?php echo $key+1; ?>" class="<?php echo $ac;?>"></li>
				<?php endforeach;?>
			</ol>
			<div class="carousel-inner">
				<?php foreach ($sliderModel as $key => $value) : if($key==0) {$ac='active';} else {$ac='';} ?>
					<div class="item <?php echo $ac;?>">
						<div class="slider-text">
							<h1>
								<?php echo $value->title; ?>
							</h1>
							<h2>
							<?php if(!empty($value->url)) : ?>
								<a href="<?php echo $value->url;?>" class="btn btn-default get" title="<?php echo $value->title;?>">Shop Now</a>
							<?php else : ?>
								<a class="btn btn-default get" href="javascript:void(0);"><?php echo $value->title;?></a>
							<?php endif;?>
						</div>
						<div class="slider-bg">
						<?php if(!empty($value->image)) :
								  echo CHtml::image($value->image,'',array());
						endif;?>
						</div>
					</div>
				<?php endforeach;?>
			</div>

			<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>
		</div>

	</section><!--/slider-->
<?php endif; ?>
				</div>
				<?php
					$this->widget('EshopHomePageProductsTab',array('limit'=>6));
					//$this->widget('EshopRecommanded');
					$this->widget('EshopBestSellers',array('limit'=>4));
				    $this->widget('EshopRecentlyViewed',array('limit'=>6));
				    //$this->widget('EshopTopBrands');
				?>
			</div>
		</div>
	</div>
</section>