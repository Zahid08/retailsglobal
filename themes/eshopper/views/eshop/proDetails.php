<?php $themeBasePath = Yii::app()->theme->baseUrl.'/assests/'; 

$totalReview= Yii::app()->db->createCommand('SELECT SUM(rating) as rating, COUNT(*) as row FROM pos_item_review WHERE itemId='.$model->id.' AND status=1')->queryAll();
$review=0;
if(!empty($totalReview))
{
	if(!empty($totalReview[0]['rating']))	
		$review = round($totalReview[0]['rating'] / $totalReview[0]['row']);
}

?>

<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/etalage.css' type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl ?>/js/jquery.etalage.min.js"></script> 
<script>
 $(function() {
	// image slider zoom 
	$('#etalage').etalage({
		thumb_image_width: 250,
		thumb_image_height: 250,
		source_image_width: 900,
		source_image_height: 1200,
		show_hint: true,
		click_callback: function(image_anchor, instance_id){
			return false;
		}
	    //change_callback(){ alert('it moved'); } 
	});
	 
	// add to cart
	$("#addto-cart").click(function() 
	{
		// values & validations
		var isError = 0,
			isAttributes = $("#isAttributes").val(),
			quantity = $('#quantity').val();

		if(isAttributes==1) { // has attributes
			var itemId =$('input[name="itemId"]:checked').val();
			if(typeof(itemId)=="undefined" || itemId=="") {
				$("#attError").html("Please select at least one Attributes");
				isError = 1;
			}
			else
			{
				$("#attError").html("");
				isError = 0;	
			}
		}
		else var itemId = <?php echo $model->id;?>;
		
		// cart processing
		if(isError==0)
		{
			$.ajax({
				type: "POST",
				dataType: "json",				
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addToCart/",
				data: {
					itemId: itemId, 
					quantity : quantity,
				},		
				beforeSend : function() {
					$(".preloader").show();									
				},			
				success: function(data) {					
					$(".preloader").hide();
					
					// update shopping bag 
					$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
						$(this).removeClass("basket").addClass("basket open");
						$(this).slideDown(200, function() {
							$('html, body').delay('200').animate({
							scrollTop: $(this).offset().top - 111
							}, 200);
						});
					}); 
				},
				error: function() {}
			});	
		}
	});
     
    // item att wise price change
	$('.radio_item_attributes').click(function()
	{
        var itemId = $(this).attr('id');
        var urlajax = "<?php  echo Yii::app()->request->baseUrl; ?>/index.php/eshop/changeItemPriceStrByAttributes/";
        $.ajax({
            type: "POST",
            url: urlajax,
            beforeSend: function() 
            {
                $(".preloader").show();
            },
            data: 
            {
                itemId : itemId,
            },
            success: function(data) 
            {
                $(".preloader").hide();
                $('.prices').html(data); 
            },
            error: function() {}
        });
	});
});
</script>
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">			
			  <li><a href="javascript:void(0);"><?php echo $model->cat->subdept->dept->name;?></a></li>
			  <li><a href="javascript:void(0);"><?php echo $model->cat->subdept->name;?></a></li>
			  <li class="active"><?php echo $model->cat->name;?></li>				 
			</ol>
		</div>	
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="left-sidebar">
					<?php $this->widget('EshopLeftMenu');?>											
				</div>
				<div class="badges-post">
					<ul class="list-unstyled">
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-trophy"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-reply"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<a href="#">
								<div class="media">
									<div class="media-left">
										<div class="badges-icon">
											<i class="fa fa-shield"></i>
										</div>
									</div>
									<div class="media-body">
										<h4>Top Rated Merchants</h4>
										<p>View our top rated merchants</p>
									</div>
								</div>
							</a>
						</li>
						<li class="badges-item">
							<div class="media">
								<div class="media-left">
									<div class="badges-icon">
										<i class="fa fa-money"></i>
									</div>
								</div>
								<div class="media-body">
									<h4>Top Rated Merchants</h4>
									<p>View our top rated merchants</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<?php $this->widget('EshopAdvertisement',array('position'=>'left'));?>
			</div>			
			<div class="col-sm-9 padding-right">
				<div class="product-details"><!--product-details-->
					<div class="col-sm-5">
						<ul id="etalage" class="etalage">
							<?php 
							 if(!empty($model->itemImages)) : // main image ?>
							  <li class="etalage_thumb thumb_<?php echo $model->itemImages[0]->id;?> etalage_thumb_active" style="display: list-item; background-image: none; opacity: 1;">
									<img title="<?php echo $model->itemName;?>" src="<?php echo $model->itemImages[0]->image;?>" class="etalage_source_image" style="display: inline; opacity: 1;" />
							  </li>
							<?php  foreach($model->itemImages as $image) : // thumbs ?>                                
								<li class="etalage_thumb thumb_<?php echo $image->id;?>" style="background-image: none; display: none; opacity: 0;">
									<img title="<?php echo $model->itemName;?>" src="<?php echo $image->image;?>" class="etalage_source_image">
								</li>
							<?php endforeach;
							endif;  ?>
						</ul>
					</div>
					<div class="col-sm-7">
					<div class="product-information" style="padding:0;"><!--/product-information-->
						<div class="product-name">
							<h1><a href="javascript:void(0);"><?php echo ucwords($model->itemName);?></a> </h1>
						</div>
						<!--<p>Web ID: 1089772</p>	-->
						<div class="clearfix">
							<h5 style='margin-bottom:0px;'>Rating : &nbsp;</h5>
							<?php 						
							 $this->widget('CStarRating',array(
                                'name'=>'rating',
                                'minRating'=>1,
                                'maxRating'=>5,
                                'starCount'=>5,
                                'value'=>$review,
                                'readOnly'=>true,
                            ));
						?>
						</div>
                        <div class="description">
							<p><?php echo substr($model->specification,0,200).'..';?></p>
						</div>
                        <p><b>Brand:</b> <?php echo $model->brand->name;?></p>
						
						<div class="dynamic-attr">
						  <span id="attError" style="color:#F00;"></span>
						<div>
						<!-- dynamic attributes starts here -->
                            <?php if(!empty(ItemAttributes::getAttributesByAllItemsGroup($model->itemName))) : 
                                foreach(ItemAttributes::getAttributesByAllItemsGroup($model->itemName) as $keyDefault=>$dataDefault) : ?>
                                <div class="attr-row">
                                    <div class="">
                                    <?php 
                                        if($dataDefault->attType->isHtml==AttributesType::STATUS_ACTIVE) // html color
                                        {
                                            echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;margin-right:25px;" />';
                                            echo '<span class="color-box" style="float:left;clear:right;background:'.$dataDefault->att->name.';"></span>';
                                        }
                                        else 
                                        {
                                            echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;" />';
                                            echo '<label for="'.$dataDefault->itemId.'">'.$dataDefault->att->name.'</label>';
                                        }
                                    ?>
                                    </div>
                                    <?php 
                                    if(!empty($defaultAttTypeModel)) :
                                        foreach($defaultAttTypeModel as $keyType=>$dataType) : 
                                        if($dataType->id!=$dataDefault->attTypeId) :?>
                                        <div class="">
                                        <?php 
                                            if($dataType->isHtml==AttributesType::STATUS_ACTIVE) // html color
                                            {
                                                foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $cKey=>$cVal) :
                                                echo '<span class="color-box" style="background:'.$cVal->att->name.';"></span>';
                                                endforeach;
                                            }
                                            else // non html 
                                            {
                                                foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $key=>$data) :
                                                    echo '<span>'.$data->att->name.'</span>';
                                                endforeach;	
                                            }
                                        ?>
                                        </div>
                                        <?php endif;
                                        endforeach;
                                    endif;?>
                                </div>
                            <?php endforeach;
                            echo '<input type="hidden" id="isAttributes" value="1" />';
                        else : echo '<input type="hidden" id="isAttributes" value="0" />';
                        endif;?>
                        <!-- dynamic attributes ends here -->
		                </div>
							<div class="price">
								<?php if(Items::getItemWiseOfferPrice($model->id)>0) :?>
									<div class="price-current">
										<h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($model->id);?></h2>
									</div>
									<div class="price-prev" style="text-decoration: line-through;">
										<h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?></h2>
									</div>
								<?php else : ?>
									<div class="price-current">
										<h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?></h2>
									</div>
								<?php endif;?>
							</div>


<!--								<a href="javascript:void(0);">  </a>-->
<!--									<input id="quantity" name="quantity" readonly="readonly" type="text" value="1" />-->
<!--								<a class="cart_quantity_down plus" id='qtyPlus' href="javascript:void(0);">  </a>-->

							<div class="quantity-block">
								<label>Quantity:</label>

								<div class="input-group col-sm-3">
									<div class="input-group-btn"><a class="cart_quantity_up minus btn btn-default" id='qtyMinus' id="basic-addon1"><i class="fa fa-minus"></i></a></div>
									<input id="quantity" name="quantity" readonly="readonly" type="text" value="1" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
									<div class="input-group-btn"><a class="cart_quantity_down plus btn btn-default" id='qtyPlus' id="basic-addon1"><i class="fa fa-plus"></i></a></div>
								</div>
							</div>
							<div class="cart-button">
								<a style="margin-left: 3px;" id="addto-cart" class="btn btn-fefault cart le-button huge" href="javascript:void(0);">
									<i class="fa fa-shopping-cart"></i>
									Add to cart
								</a>
								<a href="javascript:void(0);" id="<?php echo $model->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-heart"></i> Add to wishlist</a>
							</div>
						
						</div><!--/product-information-->
					</div>
				</div><!--/product-details-->
				<div class="clearfix"></div>
                    
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<li><a href="#details" data-toggle="tab">Specification</a></li>
							<li><a href="#companyprofile" data-toggle="tab">Description</a></li>
							<li><a href="#tag" data-toggle="tab">Shipping & Payments</a></li>
							<li class="active"><a href="#reviews" data-toggle="tab">Reviews <?php if(ItemReview::countReviewByItemId($model->id)>1){echo '';} echo ' ('.ItemReview::countReviewByItemId($model->id).')';?></a></li>
						</ul>
					</div>

					<div class="tab-content">
						<!--Ship MEnt $ specification-->
						<div class="tab-pane fade" id="details" >							
							<div class="col-sm-12">
								<div class="product-image-wrapper">
									<div class="single-products">
										<?php echo $model->specification;?>
									</div>
								</div>
							</div>
						</div>	
						<!--Ship MEnt $ description-->
						<div class="tab-pane fade" id="companyprofile" >
							<div class="col-sm-12">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<?php echo $model->description;?>
										</div>
									</div>
								</div>
							</div>				
						</div>
						<!--Ship MEnt $ Payment-->
						<div class="tab-pane fade" id="tag" >
							<div class="col-sm-12">
								<div class="product-image-wrapper">
									<div class="single-products">
										<div class="productinfo text-center">
											<?php echo $model->shipping_payment;?>
										</div>
									</div>
								</div>
							</div>							
						</div>						
						<div class="tab-pane fade active in" id="reviews" >								
							<div class="col-sm-12 form">
								<?php if(!empty($modelItemReview)):
	                              $i=1;  foreach($modelItemReview as $reviewProductData):
	                            	?>	                            	
									<ul>
										<li><a href="javascript:void(0);" ><i class="fa fa-user"></i> <?php echo $reviewProductData->name;?></a><?php //echo CHtml::link(' '.$reviewProductData->name,array('#'),array('class'=>'bold'))?></li>
										<li><a href="javascript:void(0);"><i class="fa fa-clock-o"></i><?php echo date('H:i:s', strtotime($reviewProductData->crAt));   ?></a></li>
										<li><a href="javascript:void(0);"><i class="fa fa-calendar-o"></i><?php  echo date('d M Y', strtotime($reviewProductData->crAt));   ?></a></li>
									</ul>
									<p><?php echo $reviewProductData->comment; ?></p>
									<p> <?php
	                                        $rate=$reviewProductData['rating'];
	                                            $this->widget('CStarRating',array(
	                                                'name'=>'rating'.$i,
	                                                'minRating'=>1,
	                                                'maxRating'=>5,
	                                                'starCount'=>5,
	                                                'value'=>$rate,
	                                                'readOnly'=>true,
	                                            ));
	                                        $i++;
                                        ?>
									</p>
									<div  style="clear:both; margin-bottom:20px;"></div> 
								<?php
	                            		endforeach;
	                       		endif; ?>
	                       		<p><b>Write Your Review</b>
	                       			<div id="reviewStatus" style="display: none;">
                                  		<div class="notification note-success"><p style="margin-bottom:0px;">Thank You For Your Review.Your Review is Waiting For Approval !</p></div>
                               		</div>
	                       		</p>
								<?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'item-Review',
                                    'enableAjaxValidation'=>true,
                                    'action' => Yii::app()->createUrl('eshop/itemReview'),                                    
                                    'htmlOptions'=>array(
                                            'onsubmit'=>"return false;", //Disable normal form submit
                                            'onkeypress'=>" if(event.keyCode == 13){ sendSubmit(); } " // Do ajax call when user presses enter key
                                        ),
                                    )); ?>
                                    <div class="form-group clearfix">
										<div id="nameAddClass">
                                            <?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$model->id));?>                                        
                                            <?php echo $form->textField($modelReview,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Name')); ?>
                                            <?php echo $form->error($modelReview,'name'); ?>
                                        </div>
                                        <div id="emailAddClass">                                          
                                            <?php echo $form->textField($modelReview,'email',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Email')); ?>
                                            <?php echo $form->error($modelReview,'email'); ?>
                                        </div>
									</div>
									 <?php echo $form->textArea($modelReview,'comment',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Comment')); ?>
                                     <?php echo $form->error($modelReview,'comment'); ?>							
									<div class="field-row star-row">
                                        <?php echo 'Rating'; //$form->labelEx($modelReview,'rating'); ?>                                       
                                        <?php  $this->widget('CStarRating',array(
                                            'model'=>$modelReview,
                                            'name'=>$modelReview['rating'],
                                            'attribute'=>'rating',
                                            'minRating'=>1, //minimal value
                                            'maxRating'=>5,//max value
                                            'starCount'=>5, //number of stars
                                            'titles'=>array(
                                                '1'=>'Normal',   '2'=>'Average',
                                                '3'=>'OK',    '4'=>'Good',
                                                '5'=>'Excellent'
                                            ),
                                        ));
                                        ?>
                                        <?php echo $form->error($modelReview,'rating'); ?>
                                    </div><!-- /.field-row -->									
                            <?php echo CHtml::Button('SUBMIT',array('onclick'=>'sendSubmit();','class'=>'btn btn-default pull-right')); ?>
								<?php $this->endWidget(); ?>
							</div>
						</div>						
					</div>
				</div><!--/category-tab-->				
			</div>
		</div>
		<div class="clearfix"></div>
		<?php $this->widget('EshopRecommanded',array('limit'=>4)); ?>
	</div>
</section>

<script>
    function sendSubmit() 
	{
        var ItemReviewName= $('#ItemReview_name').val();
        var ItemReviewEmail= $('#ItemReview_email').val();
        var ItemReviewComment= $('#ItemReview_comment').val();
        var rating= $('.star-rating-applied').val();
        if(ItemReviewName==''){
            $("#ItemReview_name_em_").show();
            $('#nameAddClass').addClass('error');
            $("#ItemReview_name_em_").html('Name cannot be blank.');
        }
        if(ItemReviewEmail==''){
            $("#ItemReview_email_em_").show();
            $('#emailAddClass').addClass('error');
            $("#ItemReview_email_em_").html('Email cannot be blank.');
        }
        if(ItemReviewComment==''){
            $("#ItemReview_comment_em_").show();
            $('#contentAddClass').addClass('error');
            $("#ItemReview_comment_em_").html('Comment cannot be blank.');
        }
        var data=$("#item-Review").serialize();
        if(ItemReviewName!='' && ItemReviewEmail!='' && ItemReviewComment!='' ){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl("eshop/itemReview"); ?>',
                data:data,
                success:function(data){
                    $('#reviewStatus').show('slow');
                },
                error: function(data) { // if error occured
                    alert("Error occured.please try again");
                }
            });
        }
    }

	$(document).ready(function(){
	    $("#qtyPlus").click(function(){
	    	var inp = $('#quantity').val();	       
	        $('#quantity').val(parseInt(inp)+1);	       
	    });

	    $("#qtyMinus").click(function(){
	    	var inp = $('#quantity').val();	  
	    	if(parseInt(inp)>1){ 
	        	$('#quantity').val(parseInt(inp)-1);	  
	        }     
	    });
	}); 
</script>