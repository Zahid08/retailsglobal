<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">         
              <li><a href="javascript:void(0);">Home</a></li>            
              <li class="active"><?php echo $this->metaTitle;?></li>                
            </ol>
        </div>  
    </div>
</section>

<main id="authentication" class="inner-bottom-md product-review">
    <div class="container">
        <div class="row">
            <div class="col-md-6 form">
                <section class="section sign-in inner-right-xs" style="margin: 0px;">
                    <h2 class="bordered">Customer Review</h2>
                    <div class="tab-pane fade active in" id="reviews" style="padding-left: 0px;" >
                        <div class="col-sm-12">
                        <?php if(!empty($modelItemReview)):
                                $i=1; 
                                foreach($modelItemReview as $reviewProductData):
                            ?>  
                            <ul>
                                <li><a href="#"><i class="fa fa-user"></i><?php echo $reviewProductData->name; ?></a></li>
                                <li><a href="#"><i class="fa fa-clock-o"></i><?php echo date('H:i:s', strtotime($reviewProductData->crAt));   ?></a></li>
                                <li><a onclick="return false;" href="#"><i class="fa fa-calendar-o"></i><?php  echo date('d M Y', strtotime($reviewProductData->crAt));   ?></a></li>
                            </ul>  
                            <p><?php echo $reviewProductData->comment; ?></p>
                            <p> <?php
                                    $rate=$reviewProductData['rating'];
                                        $this->widget('CStarRating',array(
                                            'name'=>'rating'.$i,
                                            'minRating'=>1,
                                            'maxRating'=>5,
                                            'starCount'=>5,
                                            'value'=>$rate,
                                            'readOnly'=>true,
                                        ));
                                    $i++;
                                ?>
                            </p> 
                            <div style="clear:both;"></div> 
                            <?php
                                endforeach;
                            endif; ?>                       
                        </div>
                    </div>                   
                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6 form">
                <section class="section register inner-left-xs" style="margin: 0px;">
                    <h2 class="bordered">Add Review</h2>
                    <?php if(!empty($msg)) :?>
                        <div class="notification note-success">
                            <p><?php echo $msg;?></p>
                        </div>
                    <?php endif; ?>
                    <div id="reviews" class="new-review-form">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'item-Review-Item',
                            'enableAjaxValidation'=>true,
                        'htmlOptions'=> array('class'=>'clearfix')

                        )); ?>

                        <div class="row field-row form-group">
                            <div id="nameAddClass" class="no-margin">
                                <?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$id));?>
                                <?php echo $form->textField($modelReview,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Name')); ?>
                                <?php echo $form->error($modelReview,'name'); ?>
                            </div>
                            <div id="emailAddClass" class="form-group">
                                
                                <?php echo $form->textField($modelReview,'email',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Email')); ?>
                                <?php echo $form->error($modelReview,'email'); ?>
                            </div>
                        </div><!-- /.field-row -->

                        <div class="field-row star-row clearfix form-group">
                            <?php echo $form->labelEx($modelReview,'rating'); ?>
                            <?php //echo $form->textField($modelReview,'rating',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                            <?php
                            $this->widget('CStarRating',array(
                                'model'=>$modelReview,
                                'name'=>$modelReview['rating'],
                                'attribute'=>'rating',
                                'minRating'=>1, //minimal value
                                'maxRating'=>5,//max value
                                'starCount'=>5, //number of stars
                                'titles'=>array(
                                    '1'=>'Normal',
                                    '2'=>'Average',
                                    '3'=>'OK',
                                    '4'=>'Good',
                                    '5'=>'Excellent'
                                ),

                            ));
                            ?>
                            <?php echo $form->error($modelReview,'rating'); ?>
                        </div><!-- /.field-row -->
                        <div style="clear: both;"></div>
                        <div id="contentAddClass" class="field-row">
                            <?php echo $form->textArea($modelReview,'comment',array('class'=>'le-input','size'=>60,'maxlength'=>255,'placeholder'=>'Comment')); ?>
                            <?php echo $form->error($modelReview,'comment'); ?>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder" style="margin-top:10px;">
                            <?php echo CHtml::submitButton('SUBMIT', array('class'=>'btn btn-default pull-right')); ?>
                        </div><!-- /.buttons-holder -->
                        <?php $this->endWidget(); ?><!-- /.contact-form -->
                    </div><!-- /.new-review-form -->
                 </section><!-- /.register -->

            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</main>

<style>
    #ItemReview_rating_em_{
        margin-top: 30px;
    }
</style>