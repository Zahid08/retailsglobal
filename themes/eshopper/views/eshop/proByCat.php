<?php $themeBasePath = Yii::app()->theme->baseUrl.'/assests/'; ?>
<script src="<?php echo $themeBasePath; ?>js/jquery.js"></script>
<script src="<?php echo $themeBasePath; ?>js/main.js"></script>
<?php
$minPrice =0;
$maxPrice = 500;
if(!empty($catModel))
{					 
 	$priceRange = Category::priceDynamic($catModel->id);
 	if(!empty($priceRange))
 	{
 		if(!empty($priceRange['minPrice']) && !empty($priceRange['maxPrice']))
 		{
 			$minPrice=$priceRange['minPrice'];
 			$maxPrice=$priceRange['maxPrice'];
 		}
 	}
} ?>	
<script>
 $(function() {
	// brand filter
	$('.le-checkbox').click(function() 
	{
		var isCheckedBrand = '';
		$(".le-checkbox:checked").each(function (i) {
			isCheckedBrand+=$(this).val()+',';
		});
		isCheckedBrand = isCheckedBrand.slice(0,-1);
		var startPrice = parseFloat($("#startPrice").html()),
			endPrice = parseFloat($("#endPrice").html());
		
		// ajax call
		$.ajax({
			type: "POST",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
			data: {
				catId : <?php echo $catModel->id;?>,
				brand : isCheckedBrand,
				startPrice : startPrice,
				endPrice : endPrice
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#catGridFilterByAjax").html(data);
			},
			error: function() {}
		});   
	});
	
	// price filtering
	$('#priceFilter').slider({
		   range:true,
		   min: <?php echo $minPrice;?>,
		   max: <?php echo $maxPrice;?>,
		   step: 100,
		   value: [<?php echo $minPrice;?>, <?php echo $maxPrice;?>],
		   handle: "square",
	});
	 
	$('#priceFilter').slider().on('slide', function(event) {
		var isCheckedBrand = '';
		$(".le-checkbox:checked").each(function (i) {
			isCheckedBrand+=$(this).val()+',';
		});
		isCheckedBrand = isCheckedBrand.slice(0,-1);
		var startPrice = event.value[0],
			endPrice = event.value[1];
		
		$("#startPrice").html(startPrice);
		$("#endPrice").html(endPrice);
		
		// ajax call
		$.ajax({
			type: "POST",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
			data: {
				catId : <?php echo $catModel->id;?>,
				brand : isCheckedBrand,
				startPrice : startPrice,
				endPrice : endPrice
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#catGridFilterByAjax").html(data);
			},
			error: function() {}
		}); 
	});
	
});
</script>

<!-- ========================================= BREADCRUMB ========================================= -->
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<?php if(!empty($catModel)) : ?>
				<ol class="breadcrumb">
				  <li><a href="javascript:void(0);"><?php echo $catModel->subdept->dept->name;?></a></li>
				  <li><a href="javascript:void(0);"><?php echo $catModel->subdept->name;?></a></li>
				  <li class="active"><?php echo $catModel->name;?></li>			 
				</ol>
			<?php endif;?>
		</div>	
	</div>
</section>
 <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <!-- ========================================= SIDEBAR ========================================= -->
    <div class="col-sm-3">
        <!-- ========================================= PRODUCT FILTER ========================================= -->
        <div class="widget left-sidebar">
			<div class="body bordered">
				<?php if(!empty($brandModel)) : ?>
				<div class="category-filter">
					<h2>Brands</h2>
                    <div class="brands-name">
					<ul class="list-unstyled">
						<?php foreach($brandModel as $keyBrand=>$dataBrand) : ?>
							<li>
                                <input id="<?php echo $dataBrand->brandId;?>" class="le-checkbox" type="checkbox" value="<?php echo $dataBrand->brandId;?>" />
                                <label for="<?php echo $dataBrand->brandId;?>"><?php echo $dataBrand->brand->name;?></label> 
                                <span class="badge pull-right"><?php echo Items::getItemsCountByBrand($dataBrand->brandId,$catModel->id);?></span>
							</li>
						<?php endforeach;?>
					</ul>
                    </div>
				</div><!-- /.category-filter -->
				<?php endif;?>
				
				<div class="price-filter">								
					<div class="price-range"><!--price-range-->
						<h2 style="">Price Range</h2>
						<div id="priceFilter"  class="well">
							 <input type="text" class="span2 price-slider" value="" data-slider-min="<?php echo $minPrice;?>" data-slider-max="<?php echo $maxPrice;?>" data-slider-step="5" data-slider-value="[<?php echo $minPrice;?>,<?php echo $maxPrice;?>]" id="sl2" ><br />
							 <span class="min-max">
									Price: <?php echo Company::getCurrency();?> <span id="startPrice"><?php echo $minPrice;?></span> - <span id="endPrice"><?php echo $maxPrice;?></span>
							</span>
						</div>
					</div><!--/price-range-->
				</div><!-- /.price-filter -->
                
                <?php /*
                <div class="price-filter">	
                    <div class="price-range"><!--price-range-->
                        <h2>Price Range</h2>
                        <div  class="well text-center">
                             <div class="slider slider-horizontal" style="width: 176px;">
                             <div class="slider-track">
                                <div class="slider-selection" style="left: 41.6667%; width: 33.3333%;"></div><div class="slider-handle round left-round" style="left: 41.6667%;"></div><div class="slider-handle round" style="left: 75%;"></div>
                             </div>
                             <div class="tooltip top" style="top: -30px; left: 70.1667px;">
                                 <div class="tooltip-arrow"></div>
                                 <div class="tooltip-inner"><?php echo $minPrice;?> : <?php echo $maxPrice;?></div>
                             </div>
                             <input type="text" id="sl2" data-slider-value="[250,450]" data-slider-step="5" data-slider-max="600" data-slider-min="0" value="" class="span2" style=""></div><br>
                             <b class="pull-left">Price: <?php echo Company::getCurrency();?>  <?php echo $minPrice;?></b> <b class="pull-right"><?php echo $maxPrice;?></b>
                        </div>
                    </div>
                </div>  */ ?>
                        
			</div><!-- /.body -->
		</div><!-- /.widget -->
		 <?php
         	//$this->widget('EshopSpecialOffersLeft');
         	$this->widget('EshopFeturedProductsLeft');
         ?>
	</div>
	<!-- ========================================= SIDEBAR : END ========================================= -->

	<!-- ========================================= CONTENT ========================================= -->
	<div class="col-xs-12 col-sm-9 no-margin wide sidebar">
		<section id="gaming">
			<div class="grid-list-products">
                <h2 class="title text-center"><?php if(!empty($catModel)) echo $catModel->name;?></h2>
				<div class="product-grid-holder medium clearfix" id="catGridFilterByAjax">
            		<div class="col-xs-12 col-md-12 no-margin">
						<div class="row no-margin">
							<?php $this->widget('zii.widgets.CListView', array(
								'dataProvider'=>$dataProvider,					
								'ajaxUpdate'=>true,
								'enableSorting' => true,
								'enablePagination'=>true,
								'emptyText' => 'No records found.',
								//'summaryText' => "{start} - {end} of {count}",
								'template' => '{items} {pager}', // {summary} {sorter} 
								//'sorterHeader' => 'Sort by:',
								//'sortableAttributes' => array('title', 'price'),
								'itemView'=>'_proByCatGrid',
								//'htmlOptions'=>array('class'=>'clearfix'),
								'pager'=> array('cssFile'=>true, 'pageSize' => 1,
									'header'=>false,
									'class'          => 'CLinkPager',
									'firstPageLabel' => 'First',
									'prevPageLabel'  => '<',
									'nextPageLabel'  => '>',
									'lastPageLabel'  => 'Last',
									'htmlOptions'=>array('class'=>'pagination')
								  ),
							 )); ?>
						</div>
					</div><!-- /.col-xs-12 col-md-12 -->
				</div><!-- /.product-grid-holder -->
			</div><!-- /.grid-list-products -->
		</section><!-- /#gaming -->
	</div><!-- /.col -->
</div><!-- /.container -->
