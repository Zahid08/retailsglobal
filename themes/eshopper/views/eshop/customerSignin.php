<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" language="javascript">
$(function() 
{
	//-------enter press ------------//
	$(document).keypress(function(e) {
		if(e.which  == 13) 
		{
			$('#login-form').get(0).submit();
		}
	});
	
});
</script>
<section>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';//echo $catModel->subdept->dept->name;?></a></li>
                <li class="active"><?php echo 'Signin'; ?></li>
            </ol>
        </div>	
    </div>
</section>

<section id="form" style="margin-bottom: 150px;"><!--form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <div class="login-form forget-pass text-center"><!--login form-->
                        <h2>Forget Password</h2>                        
                        <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'forgot-login-form',
                            'enableAjaxValidation'=>true,                     
                            'action'=>Yii::app()->createUrl('/eshop/resetPassword'),
                            'htmlOptions'=>array(
                                'class'=>'login-form cf-style-1',
                                'role'=>'form'
                            )
                        )); ?>                             
                        <?php echo CHtml::textField('username', '', array('size'=>50, 'maxlength'=>50,'placeholder'=>'Username')); ?>
                        <span>Or</span>
                        <div class="form-group">
                            <?php echo CHtml::textField('email', '', array('placeholder'=>'Email Address')); ?>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <?php $this->endWidget(); ?>
                    </div><!--/login form-->
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-5 form">
                    <div class="signup-form"><!--sign up form-->
                        <h2>Signin as Customer</h2>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'login-form',
                            'enableAjaxValidation'=>true,
                            'htmlOptions'=>array(
                                'class'=>'login-form cf-style-1',
                                'role'=>'form'
                            )
                        )); 
                        echo $form->hiddenField($model,'branchId', array('value'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE))); ?>
                            <?php echo $form->errorSummary($model); ?>

                            <div class="form-group">
                                <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255)); ?>
                                <?php echo $form->error($model,'username'); ?>
                            </div>

                            <div class="form-group">
                                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255)); ?>
                                <?php echo $form->error($model,'password'); ?>
                            </div>  

                            <button type="submit" class="btn btn-default">Sign In</button>
                            <?php /* echo CHtml::submitButton('Signin', array('class'=>'btn btn-default'));  */ ?>
                        <?php $this->endWidget(); ?>
                    </div><!--/sign up form-->
                </div>
            </div>
        </div>
    </section><!--/form-->
