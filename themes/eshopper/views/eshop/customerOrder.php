<section id="cart_items">
    <div class="container">         
        <div class="table-responsive cart_info">
            <?php if(count($orderModel)>0): ?>
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Sl.No</td>
                            <td class="description">Invoice No</td>
                            <td class="price">Quantity</td>
                            <td class="quantity">Price</td>
                            <td class="total">Shipping</td>
                            <td class="total">Discount</td>
                            <td class="total">Date</td>
                            <td class="total">Status</td>
                            <td class="total">Details</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($orderModel as $keyOrder=>$orderValue) :?>
                            <tr>
                                <td class="cart_product"><?php echo $keyOrder+1;?></td>
                                <td class="cart_description"><?php echo $orderValue->invNo;?></td>
                                <td class="cart_quantity"><?php echo $orderValue->totalQty;?></td>
                                <td class="cart_price"><?php echo Company::getCurrency().'&nbsp;'.$orderValue->totalPrice;?></td>                                
                                <td class="cart_total"><?php echo Company::getCurrency().'&nbsp;'.ShippingMethod::getShippingMethodPrice($orderValue->shippingId,$orderValue->totalPrice);?></td>
                                <td class="cart_price"><?php echo Company::getCurrency().'&nbsp;'.$orderValue->totalDiscount;?></td>
                                <td class="cart_price"><?php echo date( "Y-m-d", strtotime($orderValue->orderDate) );?></td>
                                <td class="cart_price"><?php echo Lookup::item('Status',$orderValue->status);?></td>
                                <td class="cart_price">
                                    <?php echo CHtml::link('View',array('eshop/customerOderDetails','id'=>$orderValue->id),array('class'=>'button hug'));?>
                                </td>               
                            </tr>   
                        <?php endforeach; ?>               
                     </tbody>
                </table>
            <?php else:?>
                <h1>You Have No Order !</h1>
            <?php endif;?>
        </div>      
    </div>
</section>