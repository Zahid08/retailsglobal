<!-- ========================================= BREADCRUMB ========================================= -->
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <?php if(!empty($catModel)) : ?>
                <ol class="breadcrumb">
                  <li><a href="javascript:void(0);"><?php echo 'Home';?></a></li>
                  <li class="active"><?php echo $contentsDetails->type->name; ?></li>          
                </ol>
            <?php endif;?>
        </div>  
    </div>
</section>
 <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="posts sidemeta">
                <div class="post format-image">
                    <?php if(!empty($contentsDetails)):?>
                    <div class="post-content">

                        <h2 class="post-title"><?php echo $contentsDetails->type->name; ?></h2>
                       <!-- <ul class="meta">
                            <li>Posted By : Admin</li>
                            <li><a href="#">OffTopic</a>, <a href="#">Announcements</a></li>
                            <li><a href="#">3 Comments</a></li>
                        </ul>--><!-- /.meta -->
                        <?php echo $contentsDetails->descriptions; ?>
                       <!-- <a class="le-button huge" href="index2eb8.html?page=blog-post">Read More</a>  -->
                    </div><!-- /.post-content -->
                    <?php
                        endif;
                    ?>
                </div><!-- /.post -->
            </div><!-- /.posts -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.container -->
