


<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                <img alt="" src="images/shop/product12.jpg">
                <?php if(!empty($data->itemImages)) : 
					echo CHtml::link('<img alt="" src="'.$data->itemImages[0]->image.'" data-echo="'.$data->itemImages[0]->image.'" style="width:268px;height:249px;" />',array('eshop/proDetails','id'=>$data->id),  array('title'=>$data->itemName));?>
				<?php endif;?>
               
               
               <?php if(Items::getItemWiseOfferPrice($data->id)>0) :?>
                    <div class="price-prev pull-left" style="text-decoration: line-through;"> 
                        <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($data->id);?></h2>
                    </div>
                    <div class="price-current">
                        <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($data->id);?></h2>
                    </div>                                   
                 <?php else : ?>                                    
                    <div class="price-current">
                        <h2><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($data->id);?></h2>
                    </div>
                <?php endif;?>
                <p style="height:50px;"><?php echo $data->itemName;?></p>
                <p><?php echo $data->brand->name;?></p>
                <?php echo CHtml::link('<i class=\'fa fa-shopping-cart\'></i>Add to cart',array('eshop/proDetails','id'=>$data->id), array('class'=>'btn btn-default add-to-cart')); ?>
            </div>									
        </div>
        <div class="choose">
           <ul class="nav nav-pills nav-justified">
                <li>
                     <a href="javascript:void(0);" id="<?php echo $data->id;?>" class="btn-add-to-wishlist" title="add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a>
                </li>
                <li>
                    <?php echo CHtml::link('<i class="fa fa-plus-square"></i>Review & Rate',array('eshop/proReview','id'=>$data->id),  array('class'=>'btn-add-to-compare'));?>
                </li>
            </ul>
        </div>
    </div>
</div>
