<style>
    .ginfo_left{
        width: 300px;
        float: left;
    }
    .ginfo_right{
        width: 300px;
        float: left;
    }
    .generalInfo tr {
        height: 30px;
        line-height: 29px;
    }
    .generalInfo{
        margin-top: 25px;
    }
    .infoLeft{
        width: 80%;
        float: left;
    }
    .infoRight{
        width: 20%;
        float: left;
    }
</style>
<section id="cart_items">
    <div class="container">         
        <div class="table-responsive cart_info"> 
            <?php if(!empty($model)) :?>         
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="quantity" style="width:25%;"><h5> General Information</h5>  </td>
                            <td class="quantity" style="width:25%;"> </h5> <?php echo $model->title.'.'.$model->name; ?></h5> </td>                            
                            <td class="price" style="text-align:right;padding-right: 25px !important;">
                                <?php echo CHtml::link('Update',array('eshop/CustomerInfo','id'=>$model->id),array('class'=>'le-button hug','style'=>'color:white;'));?>
                            </td>                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="cart_product"><h5>User Name</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $modelUser->username; ?></a></h5>                                
                            </td>                            
                        </tr> 
                        <tr>
                            <td class="cart_product"><h5>Email</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $model->email; ?></a></h5>                                
                            </td>                            
                        </tr> 
                        <tr>
                            <td class="cart_product"><h5>Phone</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $model->phone; ?></a></h5>                                
                            </td>                            
                        </tr>
                        <tr>
                            <td class="cart_product"><h5>Gender</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $model->gender; ?></a></h5>                                
                            </td>                            
                        </tr>  
                        <tr>
                            <td class="cart_product"><h5>Address</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $model->addressline; ?></a></h5>                                
                            </td>                            
                        </tr> 
                        <tr>
                            <td class="cart_product"><h5>City</h5></td>
                            <td colspan="2" class="cart_quantity">
                                <h5><a href="javascript:void(0);"><?php echo $model->city; ?></a></h5>                                
                            </td>                            
                        </tr>         
                    </tbody>
                </table>
            <?php endif; ?>          
        </div>      
    </div>
</section>