<?php $this->beginContent('//layouts/eshop_main'); ?>
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">           
            <ol class="breadcrumb">
              <li><a href="javascript:void(0);"><?php echo 'Home';?></a></li>
              <li class="active"><?php echo CHtml::encode($this->metaTitle); ?></li>          
            </ol>            
        </div>  
    </div>
</section>

 <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
        <!-- ================================== TOP NAVIGATION ================================== -->
        <div class="side-menu animate-dropdown">
            <div class="head"><i class="fa fa-list"></i> dashboard</div>
            <nav role="navigation" class="yamm megamenu-horizontal">
                <ul class="nav">
                    <li class="dropdown menu-item">
                        <?php echo CHtml::link('&raquo;&nbsp;My Information',array('eshop/customerDashboard'))?>
                    </li><!-- /.menu-item -->
                    <li class="dropdown menu-item">
                        <?php echo CHtml::link('&raquo;&nbsp;My Wishlist',array('eshop/customerWishlist'))?>
                        <!-- /.menu-item -->
                    <li class="dropdown menu-item">
                        <?php echo CHtml::link('&raquo;&nbsp;My Order',array('eshop/customerOrder'))?>
                    </li><!-- /.menu-item 
					<li class="dropdown menu-item">
                        <a data-toggle="dropdown" href="#">&raquo;&nbsp;My Return</a>
                    </li><!-- /.menu-item -->
					<li class="dropdown menu-item">
                        <?php echo CHtml::link('&raquo;&nbsp;Change Password',array('eshop/customerChangePassword'))?>
                    </li><!-- /.menu-item -->
                    <!-- /.menu-item -->
                </ul><!-- /.nav -->
            </nav><!-- /.megamenu-horizontal -->
        </div><!-- /.side-menu -->
        <!-- ================================== TOP NAVIGATION : END ================================== -->		</div><!-- /.sidemenu-holder -->

    <!-- BEGIN PAGE -->
    <?php echo $content; ?>
    <!-- END PAGE -->      
<?php $this->endContent(); ?>