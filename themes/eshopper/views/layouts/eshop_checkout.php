<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php echo isset($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
    <?php if(isset($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
    <?php if(isset($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>
	
    <meta name="subject" content="Unlocklive" />
    <meta name="language" content="English" />
    <meta name="copyright" content="© www.unlocklive.com/">
    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Googlebot" content="all" />
    <meta name="language" content="en" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="Rating" content="General" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="coverage" content="Worldwide" />
    <meta name = "Server" content = "New" />
    <meta name="expires" content="0" />
    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
    <meta name="reply-to" content="info@unlocklive.com" />
	<!-- END GLOBAL MANDATORY STYLES -->	
    
    <?php $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));?>
    
    <!-- Og:tag # https://developers.facebook.com/docs/sharing/webmasters#images / https://developers.facebook.com/tools/debug/og/object/ -->
    <meta property="og:title" content="<?php echo $companyModel->name;?>" />
    <meta property="og:site_name" content="<?php echo $companyModel->domain;?>"/>
    <meta property="og:type" content="website" />     
    <?php if(!empty($this->metaOgUrl)){ ?> <meta property="og:url" content="<?php echo CHtml::encode($this->metaOgUrl);?>" /> <?php } ?>
    <?php /* if(!empty($this->metaOgImage)){ ?> <meta property="og:image" content="og_decoder.php?data=<?php echo CHtml::encode($this->metaOgImage);?>"/><?php } */?>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>
    
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/checkout/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo Yii::app()->theme->baseUrl ?>/checkout/css/bootstrap.css" rel="stylesheet" type="text/css" title='main' media="screen"  />
	<link href="<?php echo Yii::app()->theme->baseUrl ?>/checkout/css/skin.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- END HEAD -->
<body>
    <div class="wrap">
        <div class="containte-980">
            <div class="row">
                <div class="hader-top">
                    <div class="logo">
						<?php 
						$companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));
						$branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
									   array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
						if(!empty($companyModel)) echo CHtml::link('<img src="'.Yii::app()->baseUrl.$companyModel->logo.'" style="height:88px;" />',array('eshop/index'),array('title'=>$companyModel->name));?>
                    </div>
                    <div class="contacts pull-right">
                        <h5><i class="icon icon-phone"></i><?php if(!empty($branchModel)) echo $branchModel->phone;?></h5>
                        <h5><i class="icon icon-chat"></i><?php if(!empty($companyModel)) echo $companyModel->email;?></h5>
                    </div>
                </div>
                <!-- Hader Top -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="top-3 col-lg-12">
                    <div class="text-center">
						<?php echo CHtml::link('<h5> ← Back to Shopping</h5>',array('eshop/index'),array('title'=>'Back to Shopping'));?>
					</div>
                </div>
            </div>
            <!-- row -->
			
            <?php echo $content;?>
					
            <div class="row">
				<footer>
				<?php 
				$paymentModel = PaymentMethod::model()->findAll('apiShortName!=:apiShortName AND status=:status',
                                              			  array(':apiShortName'=>PaymentMethod::API_GENERAL,':status'=>Company::STATUS_ACTIVE));
				if(!empty($paymentModel)) :
					foreach($paymentModel as $paymentKey=>$paymentData) :
                		echo '<img alt="'.$paymentData->title.'" src="'.$paymentData->image.'" style="height:30px;">';
                	endforeach;
				endif;?>
                </footer>
            </div>
        </div>
        <!-- . containte-980 -->
    </div>
    <!--.End Wrapper -->	
	<?php 
		Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
	?>
	<script>
        $(document).ready(function(){
            jQuery('img.svg').each(function(){
                var $img = jQuery(this);
                var imgID = $img.attr('id');
                var imgClass = $img.attr('class');
                var imgURL = $img.attr('src');

                jQuery.get(imgURL, function(data) {
                    // Get the SVG tag, ignore the rest
                    var $svg = jQuery(data).find('svg');

                    // Add replaced image's ID to the new SVG
                    if(typeof imgID !== 'undefined') {
                        $svg = $svg.attr('id', imgID);
                    }
                    // Add replaced image's classes to the new SVG
                    if(typeof imgClass !== 'undefined') {
                        $svg = $svg.attr('class', imgClass+' replaced-svg');
                    }

                    // Remove any invalid XML tags as per http://validator.w3.org
                    $svg = $svg.removeAttr('xmlns:a');

                    // Replace image with new SVG
                    $img.replaceWith($svg);
                }, 'xml');
            });  
        });     
    </script>
</body>
</html>