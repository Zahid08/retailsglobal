<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content=""> 
	    <title><?php echo !empty($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
	    <?php if(!empty($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
	    <?php if(!empty($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>
	    <meta name="subject" content="Unlocklive" />
	    <meta name="language" content="English" />
	    <meta name="copyright" content="© www.unlocklive.com/">
	    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
	    <meta name="robots" content="index, follow">
	    <meta name="revisit-after" content="7 days">
	    <meta name="Googlebot" content="all" />
	    <meta name="language" content="en" />
	    <meta http-equiv="imagetoolbar" content="no" />
	    <meta name="Rating" content="General" />
	    <meta http-equiv="Pragma" content="no-cache" />
	    <meta name="resource-type" content="document" />
	    <meta name="distribution" content="Global" />
	    <meta name="coverage" content="Worldwide" />
	    <meta name = "Server" content = "New" />
	    <meta name="expires" content="0" />
	    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
	    <meta name="reply-to" content="info@unlocklive.com" />
        
        <?php $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));?>
    
        <!-- Og:tag # https://developers.facebook.com/docs/sharing/webmasters#images / https://developers.facebook.com/tools/debug/og/object/ -->
        <meta property="og:title" content="<?php echo $companyModel->name;?>" />
        <meta property="og:site_name" content="<?php echo $companyModel->domain;?>"/>
        <meta property="og:type" content="website" />     
        <?php if(!empty($this->metaOgUrl)){ ?> <meta property="og:url" content="<?php echo CHtml::encode($this->metaOgUrl);?>" /> <?php } ?>
        <?php /* if(!empty($this->metaOgImage)){ ?> <meta property="og:image" content="og_decoder.php?data=<?php echo CHtml::encode($this->metaOgImage);?>"/><?php } */?>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>

	    <link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/bootstrap.min.css" rel="stylesheet">
	    <link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/font-awesome.min.css" rel="stylesheet">
	    <link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/prettyPhoto.css" rel="stylesheet">
	    <link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/price-range.css" rel="stylesheet">
	    <link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/animate.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/main.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/responsive.css" rel="stylesheet">
		<link href="<?php echo Yii::app()->session['themesnames'];?>/assests/css/custom.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       

        <?php // custom top css / js scripts
            $csModelTop = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_TOP,':status'=>CustomScript::STATUS_ACTIVE));
            if(!empty($csModelTop)) : 
                foreach ($csModelTop as $csKey => $csData) :						
                    if(!empty($csData->script)): echo $csData->script;
                    endif; 
                endforeach;  
            endif;  
         ?>
	</head><!--/head-->
	<body>
		<header>
			<?php 
				$this->widget('EshopTopPart');
				$this->widget('EshopTopMenu');
			?>
		</header><!--/header-->	
		<?php  
			echo '<div id="ajaxContainer">'.$content.'</div>'; 
			$this->widget('EshopFooterPart');
		?>	
		<div class="preloader" style="">
	        <div class="preloader-container">
	            <span class="animated-preloader">tt</span>
	        </div>
	    </div>		

	    <script>
	$(function() {		
		// remove from cart global
		$(".remove_from_cart").click(function() 
		{
			// values & validations
			var itemId = $(this).attr('id');			
			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();					
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
				data: {
					itemId: itemId, 
				},					
				success: function(data) {
					$(".preloader").hide();
					// update shopping bag 
					$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
						$(this).removeClass("basket").addClass("basket open");
						$(this).show();
					});
				},
				error: function() {}
			});	
		});
		
		// item add to wishlist global
		$(".btn-add-to-wishlist").click(function() {
			<?php if(!empty(Yii::app()->session['custId'])) : ?>
				var itemId = $(this).attr('id');
				$.ajax({
					type: "POST",
					dataType: "json",
					beforeSend : function() {
						$(".preloader").show();
					},
					url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
					data: {
						itemId: itemId, 
					},					
					success: function(data) {
						$(".preloader").hide();
						if(data=='exists') alert("Already added to wishlist !");
						else 
						{
							$("#wishListCount").html(data);
							alert("Added to wishlist !");
						}
					},
					error: function() {}
				});
			<?php else : ?> alert("please signin first !"); <?php endif;?>
		});
	});
	</script>
    <script src="<?php echo Yii::app()->session['themesnames'];?>/assests/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'];?>/assests/js/jquery.scrollUp.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'];?>/assests/js/price-range.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'];?>/assests/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'];?>/assests/js/main.js"></script>

    <?php // custom bottom css / js scripts
        $csModelBottom = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_BOTTOM,':status'=>CustomScript::STATUS_ACTIVE));
        if(!empty($csModelBottom)) : 
            foreach ($csModelBottom as $csKey => $csData) :						
                if(!empty($csData->script)): echo $csData->script;
                endif; 
            endforeach;  
        endif;  
     ?>
</body>
</html>