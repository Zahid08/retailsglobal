<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php echo !empty($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
    <?php if(!empty($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
    <?php if(!empty($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>

    <meta name="subject" content="Unlocklive" />
    <meta name="language" content="English" />
    <meta name="copyright" content="© www.unlocklive.com/">
    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Googlebot" content="all" />
    <meta name="language" content="en" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="Rating" content="General" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="coverage" content="Worldwide" />
    <meta name = "Server" content = "New" />
    <meta name="expires" content="0" />
    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
    <meta name="reply-to" content="info@unlocklive.com" />
	<!-- END GLOBAL MANDATORY STYLES -->	
    
    <?php $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));?>
    
    <!-- Og:tag # https://developers.facebook.com/docs/sharing/webmasters#images / https://developers.facebook.com/tools/debug/og/object/ -->
    <meta property="og:title" content="<?php echo $companyModel->name;?>" />
    <meta property="og:site_name" content="<?php echo $companyModel->domain;?>"/>
    <meta property="og:type" content="website" />     
    <?php if(!empty($this->metaOgUrl)){ ?> <meta property="og:url" content="<?php echo CHtml::encode($this->metaOgUrl);?>" /> <?php } ?>
    <?php /* if(!empty($this->metaOgImage)){ ?> <meta property="og:image" content="og_decoder.php?data=<?php echo CHtml::encode($this->metaOgImage);?>"/><?php } */?>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>
		
    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/custom.css" rel="stylesheet">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/settings.css" rel="stylesheet">		
    <!-- FlexSlider Css -->
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames']; ?>/css/flexslider.css" media="screen" />
    <!-- Portfolio CSS -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/prettyPhoto.css" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/font-awesome.min.css" rel="stylesheet">	
    <!-- Custom Less -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/less-style.css" rel="stylesheet">	
    <!-- Custom CSS -->
    <link href="<?php echo Yii::app()->session['themesnames']; ?>/css/style.css" rel="stylesheet">
    <!--[if IE]><link rel="stylesheet" href="css/ie-style.css"><![endif]-->		
    <?php // custom top css / js scripts
		$csModelTop = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_TOP,':status'=>CustomScript::STATUS_ACTIVE));
		if(!empty($csModelTop)) : 
			foreach ($csModelTop as $csKey => $csData) :						
			    if(!empty($csData->script)): echo $csData->script;
				endif; 
			endforeach;  
		endif;  
	 ?>
</head>

<body>
    <!-- Page Wrapper -->
    <div class="wrapper">        
        <?php
            // Top Part Widget
            $this->widget('EshopTopPart');
            // Main Content
            echo $content;
            // Footer Part Widget
            $this->widget('EshopFooterPart');
        ?> 
    </div><!-- / Wrapper End -->
	
	 <div class="preloader">
		<div class="preloader-container">
			<span class="animated-preloader"></span>
		</div>
	</div>
			
			
    
    <!-- Scroll to top -->
    <span class="totop"><a href="#"><i class="fa fa-angle-up"></i></a></span> 
    <!-- Javascript files -->
    <!-- jQuery -->
    <?php 
        Yii::app()->clientScript->registerCoreScript('jquery'); 
        Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
    ?>
    <!-- Bootstrap JS -->
    <script src="<?php echo Yii::app()->session['themesnames']; ?>/js/bootstrap.min.js"></script>

    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="<?php echo Yii::app()->session['themesnames']; ?>/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->session['themesnames']; ?>/js/jquery.themepunch.revolution.min.js"></script>
    <!-- FLEX SLIDER SCRIPTS  -->
    <script defer src="<?php echo Yii::app()->session['themesnames']; ?>/js/jquery.flexslider-min.js"></script>
    <!-- Pretty Photo JS -->
    <script src="<?php echo Yii::app()->session['themesnames']; ?>/js/jquery.prettyPhoto.js"></script>
    <!-- Respond JS for IE8 -->
    <script src="<?php echo Yii::app()->session['themesnames']; ?>/js/respond.min.js"></script>
    <!-- HTML5 Support for IE -->
    <script src="<?php echo Yii::app()->session['themesnames']; ?>/js/html5shiv.js"></script>
    <!-- Custom JS -->
    <script src="<?php echo Yii::app()->session['themesnames']; ?>/js/custom.js"></script>    
    <!-- JS code for this page -->
    <script>
    /* ******************************************** */
    /*  JS for SLIDER REVOLUTION  */
    /* ******************************************** */
    jQuery(document).ready(function() {
       jQuery('.tp-banner').revolution(
        {
            delay:9000,
            startheight:500,

            hideThumbs:10,

            navigationType:"bullet",	

            hideArrowsOnMobile:"on",

            touchenabled:"on",
            onHoverStop:"on",

            navOffsetHorizontal:0,
            navOffsetVertical:20,

            stopAtSlide:-1,
            stopAfterLoops:-1,

            shadow:0,

            fullWidth:"on",
            fullScreen:"off"
        });
    });
    /* ******************************************** */
    /*  JS for FlexSlider  */
    /* ******************************************** */
    $(window).load(function(){
        $('.flexslider-recent').flexslider({
            animation:		"fade",
            animationSpeed:	1000,
            controlNav:		true,
            directionNav:	false
        });
        $('.flexslider-testimonial').flexslider({
            animation: 		"fade",
            slideshowSpeed:	5000,
            animationSpeed:	1000,
            controlNav:		true,
            directionNav:	false
        });
    });
    /* Gallery */
    jQuery(".gallery-img-link").prettyPhoto({
       overlay_gallery: false, social_tools: false
    });
    </script>

    <!-- global ajax call starts-->
    <script>
    $(function() {
        // remove from cart global
        $(".remove_from_cart").click(function() 
        {
            // values & validations
            var itemId = $(this).attr('id');
            $.ajax({
                type: "POST",
                dataType: "json",
                beforeSend : function() {
                    $(".preloader").show();
                },
                url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
                data: {
                    itemId: itemId, 
                },					
                success: function(data) {
                    $(".preloader").hide();

                    // update shopping bag 
                    $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
                        $(this).removeClass("basket").addClass("basket open");
                        $(this).show();
                    });
                },
                error: function() {}
            });	
        });

        // item add to wishlist global
        $(".btn-add-to-wishlist").click(function() {
            <?php if(!empty(Yii::app()->session['custId'])) : ?>
                var itemId = $(this).attr('id');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    beforeSend : function() {
                        $(".preloader").show();
                    },
                    url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
                    data: {
                        itemId: itemId, 
                    },					
                    success: function(data) {
                        $(".preloader").hide();
                        if(data=='exists') alert("Already added to wishlist !");
                        else 
                        {
                            $("#wishListCount").html(data);
                            alert("Added to wishlist !");
                        }
                    },
                    error: function() {}
                });
            <?php else : ?> alert("please signin first !"); <?php endif;?>
        });

    });
    </script>
    <!-- global ajax call ends -->
    <?php // custom bottom css / js scripts
        $csModelBottom = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_BOTTOM,':status'=>CustomScript::STATUS_ACTIVE));
        if(!empty($csModelBottom)) : 
            foreach ($csModelBottom as $csKey => $csData) :						
                if(!empty($csData->script)): echo $csData->script;
                endif; 
            endforeach;  
        endif;  
     ?>
</body>	
</html>