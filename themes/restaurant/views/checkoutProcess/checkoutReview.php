<script type="text/javascript" language="javascript">
	$(function() 
	{
		$("#optionsRadios1").click(function() {
			$("#displayPassword").hide("slow");
		});
		$("#optionsRadios2").click(function() {
			$("#displayPassword").show("slow");
		});
	});
</script>
<div class="row">
    <div class="col-lg-12">
		<div class="progressbar">
			<ul class="list-inline">
				<li class="active-pgb">
					<span>1</span>
					<h5>Shopping Bag</h5>
				</li>
				<li class="active-pgb">
					<span>2</span>
					<h5>Checkout</h5>
				</li>
				<li>
					<span>3</span>
					<h5>Thank You</h5>
				</li>
			</ul>
		</div>
		<!-- .End Progressbar -->
		<div class="Breadcrumbs">
			<div id="crumbs">
				<ul class="list-inline">
                    <li><a href=""><div>
                        <svg class="svg logo-svg" width="28" height="23" viewBox="0 0 31 30" style=""><g transform="scale(0.03125 0.03125)"><path d="M928 128h-832c-52.8 0-96 43.2-96 96v576c0 52.8 43.2 96 96 96h832c52.8 0 96-43.2 96-96v-576c0-52.8-43.2-96-96-96zM96 192h832c17.346 0 32 14.654 32 32v96h-896v-96c0-17.346 14.654-32 32-32zM928 832h-832c-17.346 0-32-14.654-32-32v-288h896v288c0 17.346-14.654 32-32 32zM128 640h64v128h-64zM256 640h64v128h-64zM384 640h64v128h-64z"/></g></svg>
                    </div>
                    Checkout
                    </a>
					</li>
					<li class="finish-crumbs"><a href="#1"><span></span>Login or<br>checkout as a guest</a></li>
					<li class="finish-crumbs"><a href="#2"><span></span>Billing <br> & Shipping Details</a></li>
					<li class="finish-crumbs"><a href="#3"><span></span>Shipping Options</a></li>
					<li class="finish-crumbs"><a href="#4"><span></span>Payment <br> Options</a></li>
					<li class="active-crumbs"><a href="#5"><span></span>Review</a></li>
				</ul>
			</div>
			<!-- #End crumbs -->
		  </div>
		  <!-- .End Breadcrumbs -->
    </div>
    <!-- .End col-lg-12 -->
</div>
<!-- .End row -->

<div class="row">
	<?php if(isset($errMsg) && !empty($errMsg)) : ?>
		<div class="alert-box error"><span>Error : </span><?php echo $errMsg;?></div>
	<?php endif;?>

    <div class="col-lg-12 cont" id="checkout-form">
        <div class="row">
            <div class="col-lg-6">
                <address>
                 <h4>Your order will be delivered to:</h4>
                  <p>
                      <em><?php if(isset(Yii::app()->session['cust_shipping_address']) && !empty(Yii::app()->session['cust_shipping_address'])) echo Yii::app()->session['cust_shipping_address'];?></em>
                  </p>
               </address>
            </div>
            <div class="col-lg-6">
                <address>
                 <h4>Your Billing Address:</h4>
                  <p>
                      <em><?php if(isset(Yii::app()->session['cust_billing_address']) && !empty(Yii::app()->session['cust_billing_address'])) echo Yii::app()->session['cust_billing_address'];?></em>
                  </p>
               </address>
            </div>
        </div>
        <h4>The following item(s) will arrive within :</h4>
        <div class="row">
            <div class="col-lg-12">
                <ul class="product list-inline">
                <?php 
				if($total_qty>0) : 
                      foreach ($cart->getItems() as $order_code=>$quantity) : ?>
						<li>                      
							<div class="lazyImage catalog-item loaded sprite-loaded">
								<figure class="image" id="<?php echo $cart->getItemImages($order_code);?>">
									<?php if(!empty($cart->getItemImages($order_code))) : echo '<img class="lazy" alt="" src="'.$cart->getItemImages($order_code).'" style="width:170px;" />';
									endif;?>
								</figure>
							</div>

							<figcaption> 
								<div style="width:160px;margin:5px;"><?php echo $cart->getItemName($order_code);?></div>  
								<div style="width:160px;margin:5px;">Quantity : <?php echo $quantity;?></div>  
								<div style="margin-left:5px;">
								<?php if(Items::getItemWiseOfferPrice($order_code)>0)								
										  echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($order_code)*$quantity;
									  else echo Company::getCurrency().'&nbsp;'.$cart->getItemPrice($order_code)*$quantity;
								?>
								</div>       
							</figcaption>
						</li>
                     <?php endforeach; ?>       
                    </ul>
					<?php else : ?>
                    <div id="mycart_empty" style="border:1px dashed #ddd; padding:5px 10px;">
                         <p>You currently have no items saved in your Shopping Bag.</p>
                         <a href="javascript:void(0);" class="btn btn-continue btn-primary pull-right" onclick="window.history.back();" style=" float:right;">Shopping</a>
                    </div>
                    <?php endif; ?>
                </div>
        	</div>
        </div>
    <div class="col-lg-12">
        <p class="text-center">
			<em><strong><?php if(!empty($shippingModel)) echo $shippingModel->day;?></strong> business days from <?php if(!empty($companyModel)) echo $companyModel->name;?> <strong><?php if(!empty($shippingModel)) echo $shippingModel->title;?></strong></em></p>
	
        <div class="row">
            <div class="col-lg-8">
                <div class="well ppl text-center">
                    <h4>You will be redirectad to <?php if(!empty($paymentModel)) echo $paymentModel->title;?></h4>
					<?php if(!empty($paymentModel)) : ?>
                    	<img style="height:88px;" src="<?php echo $paymentModel->image;?>" alt="<?php echo $paymentModel->title;?>">
					<?php endif;?>
                </div>
                <p class="text-center"><em>By pressing pay now you accept our Terms & Conditions</em></p>
            </div>
            <div class="col-lg-4">
                <div class="bill">
                    <h4><?php if(!empty($companyModel)) echo $companyModel->name;?></h4>
                    <div class="ammount">
                        <ul class="list-unstyled pull-left">
                            <li>Sub Total:</li>
                            <li>Shipping:</li>
                        </ul>
                        <ul class="list-unstyled pull-right text-right">
                            <li><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($total_price);?></li>
                            <li><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(ShippingMethod::getShippingMethodPrice($shippingModel->id,$total_price));?></li>
                        </ul>
                    </div>
                    <div class="total" style=" border-bottom: 1px dashed #c2c2c2;">
                        <ul class="list-unstyled pull-left">
                            <li><h4>Total:</h4></li>
                            <li>Discount:</li>
                        </ul>
                        <ul class="list-unstyled">
                            <li><h4><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($total_price+ShippingMethod::getShippingMethodPrice($shippingModel->id,$total_price));?></h4></li>
                            <li class="pull-right"><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($total_price_discount);?></li>
                        </ul>
                    </div>
                    <div class="total">
                        <ul class="list-unstyled pull-left">
                            <li><h4>Grand Total:</h4></li>
                        </ul>
                        <ul class="list-unstyled">
                            <li><h4><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($total_price+ShippingMethod::getShippingMethodPrice($shippingModel->id,$total_price))-$total_price_discount);?></h4></li> 
                        </ul>
                    </div>
                    <small>Thank you</small>
					<?php // local payment gateway
					$protocol = 'http://';
            		if(isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') $protocol = 'https://';
					$successUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutSuccess';
					$failureUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutFailure';
					$cancelUrl = $protocol.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.'/checkoutProcess/checkoutCancel';

					if(!empty($paymentModel) && $paymentModel->apiShortName==PaymentMethod::API_EASY_PAY) : 
                        // http://www.easypayway.com.bd/secure_pay/paynow.php 
                    ?>
					<form action="https://www.easypayway.com/secure_pay/paynow.php" method="post" name="form1">
						<input type="hidden" name="store_id" value="keenlay">
						<input type="hidden" name="tran_id" value="<?php echo 'IN'.date("Ymdhis");?>">
						<input type="hidden" name="amount" value="<?php echo ($total_price+ShippingMethod::getShippingMethodPrice($shippingModel->id,($total_price-$total_price_discount)))-$total_price_discount;?>">
						<input type="hidden" name="desc" value="<?php echo $cart_items;?>">
						<input type="hidden" name="currency" value="<?php echo Company::getCurrency();?>">
						<input type="hidden" name="opt_a" value="<?php echo $total_qty; // total quantity ?>">
						<input type="hidden" name="opt_b" value="<?php echo $total_price; // total price ?>">
						<input type="hidden" name="opt_c" value="<?php echo $total_price_vat; // vat price ?>">
						<input type="hidden" name="opt_d" value="<?php echo ShippingMethod::getShippingMethodPrice($shippingModel->id,($total_price-$total_price_discount)).'#'.$total_price_discount; // shipping price # discount price ?>">
						<input type="hidden" name="cus_name" value="<?php echo $customerName;?>">
						<input type="hidden" name="cus_email" value="<?php echo $customerEmail;?>">
						<input type="hidden" name="success_url" value="<?php echo $successUrl?>">
						<input type="hidden" name="fail_url" value = "<?php echo $failureUrl;?>">
						<input type="hidden" name="cancel_url" value = "<?php echo $cancelUrl;?>">
						
                        <button type="submit" class="btn btn-pay btn-primary">Pay Now</button>
                    </form>
					<?php else : // global abroad ?>
                    <form action="" method="post">
                        <input type="hidden" name="total_qty" value="<?php echo $total_qty;?>" />  
                        <input type="hidden" name="total_price" value="<?php echo $total_price;?>" />  
						<input type="hidden" name="total_shipping" value="<?php echo ShippingMethod::getShippingMethodPrice($shippingModel->id,($total_price-$total_price_discount));?>" />  
						<input type="hidden" name="total_price_vat" value="<?php echo $total_price_vat;?>" /> 
                        <input type="hidden" name="total_price_discount" value="<?php echo $total_price_discount;?>" /> 
                        <button type="submit" class="btn btn-pay btn-primary">Pay Now</button>

                    </form>
					<?php endif;?>
                </div>
            </div>
        </div>
    </div>

    </div>
</div>
<!-- .End row -->