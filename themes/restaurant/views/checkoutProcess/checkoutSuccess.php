<div class="row">
    <div class="col-lg-12">
		<div class="progressbar">
			<ul class="list-inline">
				<li class="active-pgb">
					<span>1</span>
					<h5>Shopping Bag</h5>
				</li>
				<li class="active-pgb">
					<span>2</span>
					<h5>Checkout</h5>
				</li>
				<li class="active-pgb">
					<span>3</span>
					<h5>Thank You</h5>
				</li>
			</ul>
		</div>
		<!-- .End Progressbar -->
    </div>
    <!-- .End col-lg-12 -->
</div>
<!-- .End row -->

<div class="row">
    <div class="col-md-12">
		   <h4 class="alert alert-success">Your payment was successful, please check your email for invoice.</h4>
    </div>
</div>
<!-- .End row -->