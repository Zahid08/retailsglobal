<?php
$catModel = array();
if (isset($_REQUEST['catId']) && !empty($_REQUEST['catId'])) {
   $catModel = Category::model()->findByPk($_REQUEST['catId']);
};
?>

<div class="container">
   <h2 class="white"></h2>
   <ol class="breadcrumb">
      <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
      <li>Menu</li>
      <?php if (!empty($catModel)) : ?>
         <li class="active"><?php echo $catModel->name; ?></li>
      <?php endif; ?>
   </ol>
   <div class="clearfix"></div>
</div>

<!-- Banner End -->
<!-- Inner Content -->
<div class="inner-page">
   <!-- Shopping Start -->
   <div class="shopping">
      <div class="container">
         <!-- Shopping items content -->
         <div class="shopping-content">
            <div class="row">
               <!-- Shopping items -->

               <?php
               $this->widget('zii.widgets.CListView', array(
                   'dataProvider' => $dataProvider,
                   'ajaxUpdate' => true,
                   'enableSorting' => true,
                   'enablePagination' => true,
                   'emptyText' => 'No records found.',
                   //'summaryText' => "{start} - {end} of {count}",
                   'template' => '{items} {pager}', // {summary} {sorter}
                   //'sorterHeader' => 'Sort by:',
                   //'sortableAttributes' => array('title', 'price'),
                   'itemView' => '_proByCatGrid',
                   //'htmlOptions'=>array('class'=>'clearfix'),
                   'pager' => array('cssFile' => true, 'pageSize' => 1,
                       'header' => false,
                       'class' => 'CLinkPager',
                       'firstPageLabel' => 'First',
                       'prevPageLabel' => '<<',
                       'nextPageLabel' => '>>',
                       'lastPageLabel' => 'Last',
                       'selectedPageCssClass' => 'active',
                       'htmlOptions' => array('class' => 'pagination'),
                   ),
               ));
               ?>
            </div>
         </div>
      </div>
   </div>

   <!-- Shopping End -->

</div><!-- / Inner Page Content End -->