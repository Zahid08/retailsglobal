<script>
$(function() {
	// item add to wishlist global
	$(".btn-add-to-wishlist").click(function() {
		<?php if(!empty(Yii::app()->session['custId'])) : ?>
			var itemId = $(this).attr('id');
			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
				data: {
					itemId: itemId, 
				},					
				success: function(data) {
					$(".preloader").hide();
					if(data=='exists') alert("Already added to wishlist !");
					else 
					{
						$("#wishListCount").html(data);
						alert("Added to wishlist !");
					}
				},
				error: function() {}
			});
		<?php else : ?> alert("please signin first !"); <?php endif;?>
	});

});
</script>
<div class="col-xs-12 col-md-12 no-margin">
	<div class="row no-margin">
		<?php $this->widget('zii.widgets.CListView', array(
			'dataProvider'=>$dataProvider,					
			'ajaxUpdate'=>true,
			'enableSorting' => true,
			'enablePagination'=>true,
			'emptyText' => 'No records found.',
			//'summaryText' => "{start} - {end} of {count}",
			'template' => '{items} {pager}', // {summary} {sorter} 
			//'sorterHeader' => 'Sort by:',
			//'sortableAttributes' => array('title', 'price'),
			'itemView'=>'_proByCatGrid',
			//'htmlOptions'=>array('class'=>'clearfix'),
			'pager'=> array('cssFile'=>true, 'pageSize' => 1,
				'header'=>false,
				'class'          => 'CLinkPager',
				'firstPageLabel' => 'First',
				'prevPageLabel'  => '<',
				'nextPageLabel'  => '>',
				'lastPageLabel'  => 'Last',
				'htmlOptions'=>array('class'=>'pagination')
			  ),
		 )); ?>
	</div>
</div><!-- /.col-xs-12 col-md-12 -->

