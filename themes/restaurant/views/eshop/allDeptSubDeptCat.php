<style>
    ul.nav > li > h2 {
      border-bottom: 1px solid #ddd;
      color: #59b210;
      margin-bottom: 10px;
    }
    ul.nav > li > ul > li a {
        color: #777;
    }
    ul.nav > li > ul > li a:hover {
        color: #87d647;
    }
    ul.nav > li > ul {
        margin-bottom: 20px;
    }
</style>
<!-- ========================================= BREADCRUMB ========================================= -->
<div class="animate-dropdown">
    <div id="breadcrumb-alt">
        <div class="container">
            <div class="breadcrumb-nav-holder minimal">
                <ul>
                    <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';?></a></li>
                    <li class="dropdown breadcrumb-item current"><a href="javascript:void(0);"><?php echo 'All Category'; ?></a></li>
                 </ul>
            </div>
        </div><!-- /.container -->
    </div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="posts sidemeta">
                <div class="post format-image">
                    <div class="collapse navbar-collapse" id="mc-horizontal-menu-collapse">
                        <ul class="nav">
                        <?php
                            foreach(Department::getAllDepartmentDataByPage(Department::STATUS_ACTIVE) as $dkey=>$dept) : ?>
                            <li>
                                <?php echo '<h2>'. $dept->name .'</h2>';
                                    // column count by item count
                                    if(Category::getAllCatCountByDept($dept->id)>10)
                                        $itemColumn = ceil(Category::getAllCatCountByDept($dept->id)/10);
                                    else $itemColumn = 1;
                                 if(!empty(Category::getAllCatByDept($dept->id))) : ?>
                                 <ul class="menu-col" style="-moz-column-count:<?php echo $itemColumn;?>;-webkit-column-count:<?php echo $itemColumn;?>;-o-column-count:<?php echo $itemColumn;?>;-ms-column-count:<?php echo $itemColumn;?>;column-count:<?php echo $itemColumn;?>">
                                 <?php 
                                    foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) : ?>
                                        <li>
                                            <?php echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));?>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                                <?php endif;?>
                            </li>
                            <?php endforeach;?>
                        </ul><!-- /.navbar-nav -->
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.post -->
            </div><!-- /.posts -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.container -->
