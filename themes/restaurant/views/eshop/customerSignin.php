<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<div class="container">
   <h2 class="white"></h2>
   <ol class="breadcrumb">
      <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
      <li>Signin</li>
   </ol>
   <div class="clearfix"></div>
</div>

<div class="inner-page">
   <!-- Booking Start -->
   <div class="booking">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <!-- Heading -->
               <h3>Forget Password</h3>
               <!-- Paragraph -->
               <p>All star marked <span class="required" style="color:red">*</span> fields are mandatory.</p>
               <!-- Image Slider -->
               <div class="booking-form form">
                  <?php if (!empty($msg)) echo '<div class="row">' . $msg . '</div>'; ?>
                  <?php
                  $form = $this->beginWidget('CActiveForm', array(
                      'id' => 'forgot-login-form',
                      'enableAjaxValidation' => true,
                      'action' => Yii::app()->createUrl('/eshop/resetPassword'),
                      'htmlOptions' => array(
                          'class' => 'login-form cf-style-1',
                          'role' => 'form'
                      )
                  ));
                  ?>
                  <!-- Form label -->
                  <label>User Name</label>
                  <div class="form-group">
                     <?php echo CHtml::textField('username', '', array('class' => 'form-control', 'size' => 50, 'maxlength' => 50)); ?>
                     <?php echo CHtml::hiddenField('reset', ''); ?>
                  </div>
                  <!-- Form label -->

                  <label>Or</label>
                  <div class="form-group">

                  </div>

                  <label>Email</label>
                  <div class="form-group">
                     <?php echo CHtml::emailField('email', '', array('class' => 'form-control')); ?>
                  </div>
                  <!-- Form button -->
                  <?php echo CHtml::submitButton('Submit', array('class' => 'btn btn-danger btn-sm')); ?>
                  <?php $this->endWidget(); ?>
               </div>
            </div>
            <div class="col-md-6">
               <!-- Booking form area -->
               <div class="booking-form form">
                  <!-- Heading -->
                  <h3>Signin as Customer</h3>

                  <!-- Paragraph -->
                  <p>All star marked <span class="required" style="color:red">*</span> fields are mandatory.</p>
                  <!-- Booking form -->
                  <?php
                  $form = $this->beginWidget('CActiveForm', array(
                      'id' => 'login-form',
                      'enableAjaxValidation' => true,
                      'htmlOptions' => array(
                          'class' => '',
                          'role' => 'form'
                      )
                  ));
                  echo $form->hiddenField($model, 'branchId', array('value' => Branch::getDefaultBranch(Branch::STATUS_ACTIVE)));
                  ?>
                  <?php echo $form->errorSummary($model); ?>
                  <!-- Form label -->
                  <?php echo $form->labelEx($model, 'username'); ?>
                  <div class="form-group">
                     <!-- Form input -->
                     <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                     <?php echo $form->error($model, 'username'); ?>
                  </div>
                  <!-- Form label -->
                  <?php echo $form->labelEx($model, 'password'); ?>
                  <div class="form-group">
                     <!-- Form input -->
                     <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                     <?php echo $form->error($model, 'password'); ?>
                  </div>
                  <!-- Form button -->
                  <?php echo CHtml::submitButton('Signin', array('class' => 'btn btn-danger btn-sm')); ?>
                  <?php $this->endWidget(); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Booking End -->
</div><!-- / Inner Page Content End -->

