<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/etalage.css' type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl ?>/js/jquery.etalage.min.js"></script>
<style type="text/css">
   #etalage .etalage_small_thumbs ul {
      border: none;
   }
   #etalage .inner-page .single-item ul li {
      border-top: 1px dashed #ecdd94;
      color: #888;
      padding: 2px 9px;
   }
   #etalage {background: transparent; border: none;}
   #etalage li.etalage_icon {display: none !important;}
   ul#etalage li {border-top: none;}
   ul#etalage .etalage_small_thumbs {padding: 0 !important;}
</style>
<script>
   $(function () {
      // image slider zoom
      $('#etalage').etalage({
         thumb_image_width: 250,
         thumb_image_height: 150,
         source_image_width: 900,
         source_image_height: 1200,
         show_hint: true,
         click_callback: function (image_anchor, instance_id) {
            return false;
         }
         //change_callback(){ alert('it moved'); }
      });

      //increase amount
      $('.add').on('click', function () {
         var $qty = $(this).closest('p').find('.qty');
         var currentVal = parseInt($qty.val());
         if (!isNaN(currentVal)) {
            $qty.val(currentVal + 1);
         }
      });

      $('.minus').on('click', function () {
         var $qty = $(this).closest('p').find('.qty');
         var currentVal = parseInt($qty.val());
         if (!isNaN(currentVal) && currentVal > 0) {
            $qty.val(currentVal - 1);
         }
      });

      // add to cart
      $("#addto-cart").click(function ()
      {
         // values & validations
         var isError = 0,
                 isAttributes = $("#isAttributes").val(),
                 quantity = $('#quantity').val();
         if (isAttributes == 1) { // has attributes
            var itemId = $('input[name="itemId"]:checked').val();
            if (typeof (itemId) == "undefined" || itemId == "") {
               $("#attError").html("Please select at least one Attributes");
               isError = 1;
            }
            else
            {
               $("#attError").html("");
               isError = 0;
            }
         }
         else
            var itemId = <?php echo $model->id; ?>;
         // cart processing
         if (isError == 0)
         {
            $.ajax({
               type: "POST",
               dataType: "json",
               beforeSend: function () {
                  $(".preloader").show();
               },
               url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addToCart/",
               data: {
                  itemId: itemId,
                  quantity: quantity,
               },
               success: function (data) {
                  $(".preloader").hide();
                  // update shopping bag
                  $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
                     $(this).removeClass("basket").addClass("basket open");
                     $("#show_cart_items_wrapper").slideDown(200, function () {
                        $('html, body').delay('200').animate({
                           scrollTop: $(this).offset().top - 111
                        }, 200);
                     });
                  });
               },
               error: function () {
               }
            });
         }
      });


      // item att wise price change
      $('.radio_item_attributes').click(function ()
      {
         var itemId = $(this).attr('id');
         var urlajax = "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/changeItemPriceStrByAttributes/";
         $.ajax({
            type: "POST",
            url: urlajax,
            beforeSend: function ()
            {
               $(".preloader").show();
            },
            data:
                    {
                       itemId: itemId,
                    },
            success: function (data)
            {
               $(".preloader").hide();
               $('.prices').html(data);
            },
            error: function () {
            }
         });
      });
   });</script>

<!-- Banner Start -->

<div class="container">
   <h2 class="white"></h2>
   <ol class="breadcrumb">
      <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
      <li>Menu</li>
      <li><?php echo $model->cat->subdept->name; ?></li>
      <li><?php echo $model->cat->name; ?></li>
   </ol>
   <div class="clearfix"></div>
</div>
<!-- Banner End -->
<div class="inner-page">
   <!-- Single Item Start -->
   <div class="single-item">
      <div class="container">
         <!-- Shopping single item contents -->
         <div class="single-item-content">
            <div class="row">
               <div class="col-md-4 col-sm-5">
                  <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
                     <div class="product-item-holder size-big single-product-gallery small-gallery">
                        <!-- image zoomer starts -->
                        <ul id="etalage" class="etalage">
                           <?php if (!empty($model->itemImages)) : // main image ?>
                              <li class="etalage_thumb thumb_<?php echo $model->itemImages[0]->id; ?> etalage_thumb_active" style="display: list-item; background-image: none; opacity: 1;">
                                 <img title="<?php echo $model->itemName; ?>" src="<?php echo $model->itemImages[0]->image; ?>" class="etalage_source_image" style="display: inline; opacity: 1;" />
                              </li>

                              <?php foreach ($model->itemImages as $image) : // thumbs  ?>
                                 <li class="etalage_thumb thumb_<?php echo $image->id; ?>" style="background-image: none; display: none; opacity: 0;">
                                    <img title="<?php echo $model->itemName; ?>" src="<?php echo $image->image; ?>" class="etalage_source_image">
                                 </li>
                                 <?php
                              endforeach;
                           endif;
                           ?>
                        </ul>
                        <!-- image zoomer ends -->
                     </div><!-- /.single-product-gallery -->
                  </div><!-- /.gallery-holder -->
               </div>
               <div class="col-md-8 col-sm-7">
                  <!-- Heading -->
                  <h3><?php echo $model->itemName; ?>
                     <div class="buttons-holder pull-right">
                        <a id="<?php echo $model->id; ?>" class="btn-add-to-wishlist" href="javascript:void(0);" title="Add to Wish List"><i class="fa fa-heart"></i>Add to wishlist</a>
                     </div>
                  </h3>
                  <div class="row">
                     <div class="col-md-7 col-sm-12">
                        <!-- Single item details -->
                        <div class="item-details">
                           <!-- Paragraph -->
                           <p class="text-justify"><?php if (!empty($model->description)) echo $model->description; ?></p>
                           <!-- Heading -->
                        </div>
                     </div>
                     <div class="col-md-5 col-sm-12">
                        <!-- Form inside table wrapper -->
                        <div class="table-responsive">
                           <!-- Ordering form -->
                           <form role="form">
                              <!-- Table -->
                              <table class="table table-bordered">
                                 <tr>
                                    <td>Price</td>
                                    <td class="prices">
                                       <?php if (Items::getItemWiseOfferPrice($model->id) > 0) : ?>
                                          <div class="price-current"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($model->id); ?></div>
                                          <div class="price-prev" style="text-decoration: line-through;">
                                             <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($model->id); ?>
                                          </div>
                                       <?php else : ?>
                                          <div class="price-current"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($model->id); ?></div>
                                       <?php endif; ?>
                                    </td>
                                 </tr>
                                 <?php if (!empty(ItemAttributes::getAttributesByAllItemsGroup($model->itemName))) : ?>
                                    <tr>
                                       <td>Attributes</td>
                                       <td>
                                          <div class="dynamic-attr">
                                             <span id="attError" style="color:#F00;"></span>
                                             <!-- dynamic attributes starts here -->
                                             <?php foreach (ItemAttributes::getAttributesByAllItemsGroup($model->itemName) as $keyDefault => $dataDefault) : ?>
                                                <div class="attr-row">
                                                   <div class="">
                                                      <?php
                                                      if ($dataDefault->attType->isHtml == AttributesType::STATUS_ACTIVE) { // html color
                                                         echo '<input type="radio" name="itemId" id="' . $dataDefault->itemId . '" value="' . $dataDefault->itemId . '" class="radio_item_attributes" style="float:left;clear:right; margin-top:11px; margin-right:6px;" />';
                                                         echo '<span class="color-box" style="background:' . $dataDefault->att->name . ';"></span>';
                                                      } else {
                                                         echo '<input type="radio" name="itemId" id="' . $dataDefault->itemId . '" value="' . $dataDefault->itemId . '" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;" />';
                                                         echo '<label for="' . $dataDefault->itemId . '">' . $dataDefault->att->name . '</label>';
                                                      }
                                                      ?>
                                                   </div>
                                                   <?php
                                                   if (!empty($defaultAttTypeModel)) :
                                                      foreach ($defaultAttTypeModel as $keyType => $dataType) :
                                                         if ($dataType->id != $dataDefault->attTypeId) :
                                                            ?>
                                                            <div class="">
                                                               <?php
                                                               if ($dataType->isHtml == AttributesType::STATUS_ACTIVE) { // html color
                                                                  foreach (ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id, $dataDefault->itemId, $model->itemName) as $cKey => $cVal) :
                                                                     echo '<span class="color-box" style="background:' . $cVal->att->name . ';"></span>';
                                                                  endforeach;
                                                               }
                                                               else { // non html
                                                                  foreach (ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id, $dataDefault->itemId, $model->itemName) as $key => $data) :
                                                                     echo '<span>' . $data->att->name . '</span>';
                                                                  endforeach;
                                                               }
                                                               ?>
                                                            </div>
                                                            <?php
                                                         endif;
                                                      endforeach;
                                                   endif;
                                                   ?>
                                                </div>
                                                <?php
                                             endforeach;
                                             echo '<input type="hidden" id="isAttributes" value="1" />';
                                             ?>
                                             <!-- dynamic attributes ends here -->
                                          </div> <!-- /.dynamic-attr -->
                                       </td>
                                    </tr>
                                    <?php
                                 else : echo '<input type="hidden" id="isAttributes" value="0" />';
                                 endif;
                                 ?>
                                 <tr>
                                    <td>Quantity</td>
                                    <td>
                                       <div class="form-group">
                                          <p id="amount-wrapper">
                                             <input readonly="readonly" id="quantity" type="text" class="form-control input-sm qty" value="1"/>
                                             <span class="clickable add icon-plus-sign-alt"><i class="fa fa-plus-square"></i></span>
                                             <span class="clickable minus icon-minus-sign-alt"><i class="fa fa-minus-square"></i></span>
                                          </p>
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                       <div class="form-group">
                                          <a id="addto-cart" href="javascript:void(0);" class="le-button huge btn btn-danger btn-sm">add to cart</a>
                                       </div>
                                    </td>
                                 </tr>
                              </table>
                           </form><!--/ Table End-->
                        </div><!--/ Table responsive class end -->
                     </div>
                  </div><!--/ Inner row end  -->
               </div>
            </div>
         </div>
      </div>
   </div>

   <!-- Single Item End -->

</div><!-- / Inner Page Content End -->

<div class="inner-page padd">
   <!-- General Info Start -->
   <div class="general">
      <div class="container">
         <div class="row">
            <div class="col-md-12 col-sm-12">
               <!-- General information content -->
               <div class="general-content">
                  <!-- Navigation tab -->
                  <ul class="nav nav-tabs">
                     <li class="active">
                        <a href="#tab3" data-toggle="tab">Review
                           <?php
                           if (ItemReview::countReviewByItemId($model->id) > 1) {
                              echo '';
                           } echo ' (' . ItemReview::countReviewByItemId($model->id) . ')';
                           ?>
                        </a>
                     </li>
                  </ul>
                  <!-- Tab content -->
                  <div class="tab-content">
                     <!-- In "id", use the value which you used in above anchor tags -->
                     <div class="tab-pane active" id="tab3">
                        <div class="row">
                           <div class="col-md-6">
                              <!-- Contact form -->
                              <div class="contact-form form">
                                 <?php
                                 if (!empty($modelItemReview)):
                                    $i = 1;
                                    foreach ($modelItemReview as $reviewProductData):
                                       ?>
                                       <div class="comment-item" style='border-bottom: 1px solid #ccc; padding-bottom: 3px;'>
                                          <div class="row no-margin">
                                             <div class="col-lg-2 col-xs-12 col-sm-2 no-margin">
                                                <div class="avatar">
                                                   <img class="img-responsive img-circle" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/user.jpg" alt="" />
                                                </div><!-- /.avatar -->
                                             </div><!-- /.col -->

                                             <div class="col-xs-12 col-lg-10 col-sm-10 no-margin">
                                                <div class="comment-body">
                                                   <div class="meta-info">
                                                      <div class="author inline">
                                                         <?php echo CHtml::link($reviewProductData->name, array('#'), array('class' => 'bold')) ?>
                                                      </div>
                                                      <div class="star-holder inline">
                                                         <?php
                                                         $rate = $reviewProductData['rating'];
                                                         $this->widget('CStarRating', array(
                                                             'name' => 'rating' . $i,
                                                             'minRating' => 1,
                                                             'maxRating' => 5,
                                                             'starCount' => 5,
                                                             'value' => $rate,
                                                             'readOnly' => true,
                                                         ));
                                                         $i++;
                                                         ?>
                                                      </div>
                                                      <div class="date inline pull-right">
                                                         <?php echo date('d M Y', strtotime($reviewProductData->crAt)); ?>
                                                      </div>
                                                   </div><!-- /.meta-info -->
                                                   <div class="clearfix"></div>
                                                   <div class="author inline">
                                                      <?php echo $reviewProductData->comment; ?>
                                                   </div><!-- /.comment-text -->
                                                </div><!-- /.comment-body -->
                                             </div><!-- /.col -->
                                          </div><!-- /.row -->
                                       </div><!-- /.comment-item -->
                                       <?php
                                    endforeach;
                                 endif;
                                 ?>
                              </div><!--/ Contact form end -->
                           </div>
                           <div class="col-md-6">
                              <!-- Contact form -->
                              <div class="contact-form form">
                                 <!-- Heading -->
                                 <h3>Add review</h3>
                                 <!-- Form -->
                                 <div id="reviewStatus" style="display: none;"> </div>
                                 <?php
                                 $form = $this->beginWidget('CActiveForm', array(
                                     'id' => 'item-Review',
                                     'enableAjaxValidation' => true,
                                     'action' => Yii::app()->createUrl('eshop/itemReview'),
                                     /* 'htmlOptions'=>array(
                                       'class'=>'login-form cf-style-1',
                                       'role'=>'form'
                                       ) */
                                     'htmlOptions' => array(
                                         'onsubmit' => "return false;", //Disable normal form submit
                                         'onkeypress' => " if(event.keyCode == 13){ sendSubmit(); } " // Do ajax call when user presses enter key
                                     ),
                                 ));
                                 ?>
                                 <div id="nameAddClass" class="form-group">
                                    <?php echo $form->hiddenField($modelReview, 'itemId', array('value' => $model->id)); ?>
                                    <?php echo $form->labelEx($modelReview, 'name'); ?>
                                    <?php echo $form->textField($modelReview, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                                    <?php echo $form->error($modelReview, 'name'); ?>
                                 </div>
                                 <div id="emailAddClass" class="form-group">
                                    <?php echo $form->labelEx($modelReview, 'email'); ?>
                                    <?php echo $form->textField($modelReview, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                                    <?php echo $form->error($modelReview, 'email'); ?>
                                 </div>
                                 <div class="form-group"  style="margin-bottom:31px">
                                    <?php echo $form->labelEx($modelReview, 'rating'); ?>
                                    <?php
                                    $this->widget('CStarRating', array(
                                        'model' => $modelReview,
                                        'name' => $modelReview['rating'],
                                        'attribute' => 'rating',
                                        'id' => 'yii_rating',
                                        'minRating' => 1, //minimal value
                                        'maxRating' => 5, //max value
                                        'starCount' => 5, //number of stars
                                        'titles' => array(
                                            '1' => 'Normal',
                                            '2' => 'Average',
                                            '3' => 'OK',
                                            '4' => 'Good',
                                            '5' => 'Excellent'
                                        ),
                                    ));
                                    ?>
                                    <?php echo $form->error($modelReview, 'rating'); ?>
                                 </div>

                                 <div class="form-group">
                                    <?php echo $form->labelEx($modelReview, 'comment'); ?>
                                    <?php echo $form->textArea($modelReview, 'comment', array('class' => 'form-control', 'rows' => "3", 'size' => 60, 'maxlength' => 255)); ?>
                                    <?php echo $form->error($modelReview, 'comment'); ?>
                                 </div>
                                 <?php echo CHtml::Button('SUBMIT', array('onclick' => 'sendSubmit();', 'class' => 'btn btn-danger btn-sm')); ?>
                                 <?php $this->endWidget(); ?>
                              </div><!--/ Contact form end -->
                           </div>
                        </div>
                     </div> <!--/ tab-pane end -->
                  </div><!--/ Tab content end -->
               </div>
            </div>
         </div>
         <?php $this->widget('EshopRecommanded', array('limit' => 4)); ?>
      </div>
   </div>
   <!-- General Info End -->
</div><!-- / Inner Page Content End -->


<script>
   function sendSubmit()
   {
      var ItemReviewName = $('#ItemReview_name').val();
      var ItemReviewEmail = $('#ItemReview_email').val();
      var ItemReviewComment = $('#ItemReview_comment').val();
      var rating = $('#ItemReview_rating_em_').val();

      if (ItemReviewName == '') {
         $("#ItemReview_name_em_").show();
         $('#nameAddClass').addClass('error');
         $("#ItemReview_name_em_").html('Name cannot be blank.');
      }
      if (ItemReviewEmail == '') {
         $("#ItemReview_email_em_").show();
         $('#emailAddClass').addClass('error');
         $("#ItemReview_email_em_").html('Email cannot be blank.');
      }
      if (ItemReviewComment == '') {
         $("#ItemReview_comment_em_").show();
         $('#contentAddClass').addClass('error');
         $("#ItemReview_comment_em_").html('Comment cannot be blank.'); // ItemReview_rating_em_
      }
      var data = $("#item-Review").serialize();
      if (ItemReviewName != '' && ItemReviewEmail != '' && ItemReviewComment != '') {
         $.ajax({
            type: 'POST',
            url: '<?php echo Yii::app()->createAbsoluteUrl("eshop/itemReview"); ?>',
            data: data,
            success: function (data) {
               $('#reviewStatus').html(data);
               $('#reviewStatus').show('slow');
            },
            error: function (data) { // if error occured
               alert("Error occured.please try again");
            }
         });
      }
   }
</script>