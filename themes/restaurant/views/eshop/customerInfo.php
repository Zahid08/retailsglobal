<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE TITLE & BREADCRUMB-->


<div class="col-md-7">
   <!-- Booking form area -->
   <div class="booking-form form">
      <!-- Heading -->
      <h3>Update <?php echo $modelCustomer->name; ?> Inforamtion</h3>
      <?php if (!empty($msg)) : ?>
         <div class="notification note-success"><p><?php echo $msg; ?></p></div>
      <?php endif; ?>

      <!-- Booking form -->
      <?php
      $form = $this->beginWidget('CActiveForm', array(
          'id' => 'customer-form',
          'enableAjaxValidation' => true,
          'htmlOptions' => array(
              'class' => 'login-form cf-style-1',
          )
      ));
      ?>
      <?php echo $form->errorSummary($modelCustomer); ?>
      <?php echo $form->hiddenField($modelCustomer, 'custId', array('id' => 'custId', 'class' => 'le-input', 'readonly' => 'true', 'value' => $modelCustomer->custId)); ?>
<?php echo $form->hiddenField($modelCustomer, 'custType', array('id' => 'custType', 'class' => 'le-input', 'value' => Customer::STATUS_GENERAL)); ?>
<?php echo $form->hiddenField($modelCustomer, 'status', array('id' => 'status', 'value' => Customer::STATUS_INACTIVE)); ?>


      <!-- Form label -->
         <?php echo $form->labelEx($modelCustomer, 'title'); ?>
      <div class="form-group">
<?php echo $form->dropDownList($modelCustomer, 'title', UsefulFunction::getCustomerTitle(), array('class' => 'form-control', 'prompt' => 'Select Title')); ?>
<?php echo $form->error($modelCustomer, 'title'); ?>
      </div>


         <?php echo $form->labelEx($modelCustomer, 'name'); ?>
      <div class="form-group">
      <?php echo $form->textField($modelCustomer, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
      <?php echo $form->error($modelCustomer, 'name'); ?>
      </div>
         <?php echo $form->labelEx($modelCustomer, 'email'); ?>
      <div class="form-group">
<?php echo $form->textField($modelCustomer, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 155)); ?>
      <?php echo $form->error($modelCustomer, 'email'); ?>
      </div>

         <?php echo $form->labelEx($modelCustomer, 'addressline'); ?>
      <div class="form-group">
<?php echo $form->textArea($modelCustomer, 'addressline', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
      <?php echo $form->error($modelCustomer, 'addressline'); ?>
      </div>

         <?php echo $form->labelEx($modelCustomer, 'city'); ?>
      <div class="form-group">
<?php echo $form->textField($modelCustomer, 'city', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150)); ?>
      <?php echo $form->error($modelCustomer, 'city'); ?>
      </div>

         <?php echo $form->labelEx($modelCustomer, 'phone'); ?>
      <div class="form-group">
<?php echo $form->textField($modelCustomer, 'phone', array('class' => 'form-control', 'size' => 60, 'maxlength' => 155)); ?>
      <?php echo $form->error($modelCustomer, 'phone'); ?>
      </div>

         <?php echo $form->labelEx($modelCustomer, 'gender'); ?>
      <div class="form-group">
<?php echo $form->dropDownList($modelCustomer, 'gender', UsefulFunction::getCustomerGender(), array('class' => 'form-control', 'prompt' => 'Select Gender')); ?>
      <?php echo $form->error($modelCustomer, 'gender'); ?>
      </div>
      <!-- Form button -->
      <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-danger btn-sm margin-top6')); ?>

<?php $this->endWidget(); ?>
   </div>
</div>
