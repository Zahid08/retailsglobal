<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<?php $basePath = Yii::app()->theme->baseUrl .'/';
$branchModel = Branch::model()->find('isDefault=:isDefault AND status=:status',
                                    array(':isDefault'=>Branch::DEFAULT_BRANCH,':status'=>Company::STATUS_ACTIVE));
$colsp=12;
if(!empty($branchModel)) $colsp=6 ;     
?> 
<div class="container">       
    <h2 class="white"></h2>
    <ol class="breadcrumb">
        <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>            
        <li class="active">Contact Us</li>         
    </ol>
    <div class="clearfix"></div>
</div>       
           
<div class="inner-page">			
    <!-- Contact Us Start -->    
    <div class="contactus">
    <?php  
    if(Yii::app()->user->hasFlash('contact')): ?>
        <div class="row">    		
            <div class="col-sm-12">    			   			
                <div class="status alert alert-success"><?php echo Yii::app()->user->getFlash('contact'); ?></div>			 		
            </div>			
        </div>
    <?php endif; ?>
    
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Contact Us content -->
                    <?php if(!empty($branchModel)) :?>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <!-- Contact content details -->
                            <div class="contact-details">
                                <!-- Heading -->
                                <h4>Location</h4><!-- Address / Icon -->
                                <i class="fa fa-map-marker br-red"></i> <span><?php echo $branchModel->addressline; ?></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <!-- Contact content details -->
                            <div class="contact-details">
                                <!-- Heading -->
                                <h4>On-line Order</h4>
                                <!-- Contact Number / Icon -->
                                <i class="fa fa-phone br-green"></i> <span><?php echo $branchModel->phone; ?></span>
                                <div class="clearfix"></div>
                                <!-- Email / Icon -->
                                <i class="fa fa-envelope-o br-lblue"></i> <span><a href="mailto:<?php echo $branchModel->email; ?>"><?php echo $branchModel->email; ?></a></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <!-- Contact content details -->
                            <div class="contact-details form">
                                <!-- Heading -->
                                <h4>Contact Us</h4>
                                <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'contact-form',
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                            ),
                            'htmlOptions'=>array(                              
                                'role'=>"form",
                            ),
                        )); ?>
                         <?php echo $form->errorSummary($model); ?>
                    
                            <div class="form-group">                                                           
                                <?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                                <?php echo $form->error($model,'name'); ?>
                            </div>
                            <div class="form-group">                                                         
                                <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                <?php echo $form->error($model,'email'); ?> 
                            </div>
                            <div class="form-group">                                                         
                               <?php echo $form->textField($model,'subject',array('class'=>'form-control','placeholder'=>'Subject')); ?>
                               <?php echo $form->error($model,'subject'); ?> 
                            </div>                            
                            <div class="form-group">                                
                                <?php echo $form->textArea($model,'body',array('rows'=>'3','class'=>'form-control','placeholder'=>'Your Message Here')); ?>
                                <?php echo $form->error($model,'body'); ?>
                            </div>
                            <div class="form-group">                                                         
                              <?php if(CCaptcha::checkRequirements()): ?>		              
                                <?php $this->widget('CCaptcha'); ?>
                                <?php echo $form->textField($model,'verifyCode',array('class'=>'le-input','style'=>'height:40px;')); ?>
                                <?php echo $form->error($model,'verifyCode'); ?>
                              <?php endif; ?> 
                            </div>
                            <!-- Form button -->                            
                            <?php echo CHtml::submitButton('Submit',array('class'=>'btn btn-danger btn-sm')); ?>
                       <?php $this->endWidget(); ?>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div><!--/ Inner row end -->  
                <?php endif;?>                    
                </div>
                
            </div>
        </div>
    </div>
    
    <!-- Contact Us End -->
    
</div><!-- / Inner Page Content End -->	