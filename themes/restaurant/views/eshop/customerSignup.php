<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<div class="container">
   <h2 class="white"></h2>
   <ol class="breadcrumb">
      <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
      <li>Customer Signup</li>
   </ol>
   <div class="clearfix"></div>
</div>

<!-- END PAGE TITLE & BREADCRUMB-->

<div class="inner-page" id="authentication" >
   <div class="booking">
      <div class="container">
         <div>
            <p>All star marked <span class="required" style="color:red">*</span> fields are mandatory.</p>
         </div>

         <?php if (!empty($msg)) echo '<div class="row">' . $msg . '</div>'; ?>
         <div class="row">
            <div class="signup-wrapper">
               <div class="col-md-8 form">
                  <section style="margin: 0px;" class="section register inner-left-xs">
                     <h2 class="bordered">Create New Account as Customer </h2>
                     <?php
                     $form = $this->beginWidget('CActiveForm', array(
                         'id' => 'customer-form',
                         'enableAjaxValidation' => true,
                         'htmlOptions' => array(
                             'class' => 'login-form cf-style-1',
                         )
                     ));
                     ?>
                     <?php echo $form->errorSummary($modelCustomer); ?>
                     <?php echo $form->hiddenField($modelCustomer, 'custId', array('id' => 'custId', 'class' => 'form-controlt', 'readonly' => 'true', 'value' => 'CN' . date("Ymdhis"))); ?>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'title'); ?>
                        <?php echo $form->dropDownList($modelCustomer, 'title', UsefulFunction::getCustomerTitle(), array('class' => 'form-control', 'prompt' => 'Select Title')); ?>
                        <?php echo $form->error($modelCustomer, 'title'); ?>
                     </div>

                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'name'); ?>
                        <?php echo $form->textField($modelCustomer, 'name', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                        <?php echo $form->error($modelCustomer, 'name'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'email'); ?>
                        <?php echo $form->textField($modelCustomer, 'email', array('class' => 'form-control', 'size' => 60, 'maxlength' => 155)); ?>
                        <?php echo $form->error($modelCustomer, 'email'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'addressline'); ?>
                        <?php echo $form->textArea($modelCustomer, 'addressline', array('class' => 'form-control', 'size' => 60, 'maxlength' => 255)); ?>
                        <?php echo $form->error($modelCustomer, 'addressline'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'city'); ?>
                        <?php echo $form->textField($modelCustomer, 'city', array('class' => 'form-control', 'size' => 60, 'maxlength' => 150)); ?>
                        <?php echo $form->error($modelCustomer, 'city'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'phone'); ?>
                        <?php echo $form->textField($modelCustomer, 'phone', array('class' => 'form-control', 'size' => 60, 'maxlength' => 155)); ?>
                        <?php echo $form->error($modelCustomer, 'phone'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'gender'); ?>
                        <?php echo $form->dropDownList($modelCustomer, 'gender', UsefulFunction::getCustomerGender(), array('class' => 'form-control', 'prompt' => 'Select Gender')); ?>
                        <?php echo $form->error($modelCustomer, 'gender'); ?>
                     </div>

                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'username'); ?>
                        <?php echo $form->textField($modelCustomer, 'username', array('class' => 'form-control')); ?>
                        <?php echo $form->error($modelCustomer, 'username'); ?>
                     </div>

                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'password'); ?>
                        <?php echo $form->passwordField($modelCustomer, 'password', array('value' => '', 'class' => 'form-control')); ?>
                        <?php echo $form->error($modelCustomer, 'password'); ?>
                     </div>
                     <div class="form-group">
                        <?php echo $form->labelEx($modelCustomer, 'conf_password'); ?>
                        <?php echo $form->passwordField($modelCustomer, 'conf_password', array('value' => '', 'class' => 'form-control')); ?>
                        <?php echo $form->error($modelCustomer, 'conf_password'); ?>
                     </div>
                     <div class="buttons-holder">
                        <?php echo CHtml::submitButton('Sign Up', array('class' => 'btn btn-danger btn-sm')); ?>
                     </div><!-- /.buttons-holder -->
                     <?php $this->endWidget(); ?>
                  </section><!-- /.register -->
               </div>
            </div><!-- /.col -->

         </div><!-- /.row -->
      </div>
   </div><!-- /.container -->
</div>