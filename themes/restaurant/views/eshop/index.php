<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>
<!-- Slider Start
#################################
   - THEMEPUNCH BANNER -
#################################	-->
<?php if (!empty($sliderModel)) : ?>
   <div class="tp-banner-container">
      <div class="tp-banner" >
         <ul>	<!-- SLIDE  -->
            <?php foreach ($sliderModel as $keySlider => $dataSlider) : ?>
               <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                  <!-- MAIN IMAGE -->
                  <!-- LAYERS NR. 1 -->
                  <div class="tp-caption lfl"
                       data-x="left"
                       data-y="100"
                       data-speed="800"
                       data-start="1200"
                       data-easing="Power4.easeOut"
                       data-endspeed="300"
                       data-endeasing="Linear.easeNone"
                       data-captionhidden="off">
                     <img class="img-responsive" src="<?php echo (!empty($dataSlider->image)) ? $dataSlider->image : '' ?>" alt="" />
                  </div>
                  <!-- LAYERS NR. 2 -->
                  <?php if (!empty($dataSlider->url)) : ?>
                     <div class="tp-caption lfr large_bold_grey heading white"
                          data-x="right" data-hoffset="-10"
                          data-y="120"
                          data-speed="800"
                          data-start="2000"
                          data-easing="Power4.easeOut"
                          data-endspeed="300"
                          data-endeasing="Linear.easeNone"
                          data-captionhidden="off">
                        <a href="<?php echo $dataSlider->url; ?>" class="big le-button" title="<?php echo $dataSlider->title; ?>">Shop Now</a>

                     </div>
                  <?php endif; ?>
                  <!-- LAYER NR. 3 -->
                  <div class="tp-caption whitedivider3px customin customout tp-resizeme"
                       data-x="right" data-hoffset="-20"
                       data-y="210" data-voffset="0"
                       data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                       data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                       data-speed="700"
                       data-start="2300"
                       data-easing="Power3.easeInOut"
                       data-splitin="none"
                       data-splitout="none"
                       data-elementdelay="0.1"
                       data-endelementdelay="0.1"
                       data-endspeed="500"
                       style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">&nbsp;
                  </div>
                  <!-- LAYER NR. 4 -->
                  <div class="tp-caption finewide_medium_white randomrotate customout tp-resizeme"
                       data-x="right" data-hoffset="-10"
                       data-y="245" data-voffset="0"
                       data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                       data-speed="1000"
                       data-start="2700"
                       data-easing="Power3.easeInOut"
                       data-splitin="chars"
                       data-splitout="chars"
                       data-elementdelay="0.08"
                       data-endelementdelay="0.08"
                       data-endspeed="500"
                       style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;"><?php echo $dataSlider->title; ?>
                  </div>
                  <!-- LAYER NR. 5 -->
                  <?php if (!empty($dataSlider->shortDesc)) : ?>
                     <div class="tp-caption finewide_verysmall_white_mw white customin customout tp-resizeme text-right paragraph"
                          data-x="right" data-hoffset="-10"
                          data-y="300"
                          data-customin="x:0;y:50;z:0;rotationX:-120;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 0%;"
                          data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                          data-speed="1000"
                          data-start="3500"
                          data-easing="Power3.easeInOut"
                          data-splitin="lines"
                          data-splitout="lines"
                          data-elementdelay="0.2"
                          data-endelementdelay="0.08"
                          data-endspeed="300"
                          style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap;">
                             <?php echo $dataSlider->shortDesc; ?>
                     </div>
                  <?php endif; ?>

               </li>
            <?php endforeach; ?>
         </ul>
         <!-- Banner Timer -->
         <div class="tp-bannertimer"></div>
      </div>
   </div>
<?php endif; ?>
<!-- Slider End -->
<!-- Main Content -->
<div class="main-content">
   <!-- Showcase Start -->
   <?php $this->widget('EshopAdvertisement', array('position'=>'top','limit'=>'3')); ?>

   <!-- Showcase End -->

   <!-- Dishes Start -->
   <?php $this->widget('EshopHomePageProductsTab', array('limit'=>4)); ?>
   <!-- Dishes End -->

   <!-- menu Start -->
   <?php
   // Get Random 3 Category with items 
   $sqlRandCategory = "SELECT * FROM `pos_category` WHERE id IN(
              SELECT catId FROM `pos_items` WHERE isEcommerce=".Items::IS_ECOMMERCE." AND status=".Items::STATUS_ACTIVE." GROUP BY catId
           ) AND status=".Category::STATUS_ACTIVE." ORDER BY rand() LIMIT 0,3";
   $categoryModel = Category::model()->findAllBySql($sqlRandCategory);;
   if(!empty($categoryModel)) : ?>
      <div class="menu padd padding-top20 padding-bottom0 margin-top20">
         <div class="container">
            <!-- Default Heading -->
            <div class="default-heading margin0 ">
               <!-- Crown image -->
               <img class="img-responsive" src="<?php echo $basePath; ?>img/menu.png" alt="" />
               <!-- Heading -->
               <h2>Menu</h2>
               <!-- Border -->
               <div class="border"></div>
            </div>
            <!-- Menu content container -->
            <div class="menu-container">
               <div class="row">
                  <?php $bgColor = 0;
                  foreach ($categoryModel as $key => $catData) {
                     $itemsModel = Items::model()->findAll(array('condition'=>'catId=:catId and isEcommerce=:isEcommerce and status=:status',
                    'params'=>array(':catId'=>$catData->id,':isEcommerce'=>Items::IS_ECOMMERCE,':status'=>Items::STATUS_ACTIVE),'limit'=>5,'order'=>'RAND()'));
                     if ($key == 0) $dynamicBar = 'br-red';
                     else if ($key == 1) $dynamicBar = 'br-green';
                     else $dynamicBar = 'br-lblue';
                     if(!empty($itemsModel)) : ?>
                        <div class="col-md-4 col-sm-4">
                           <!-- Menu header -->
                           <div class="menu-head">
                              <!-- Image for menu item -->
                              <?php if (!empty($itemsModel[0]->itemImages[0]->image)) : ?>
                                 <img class="menu-img img-responsive img-thumbnail" src="<?php echo $itemsModel[0]->itemImages[0]->image; ?>" alt="" />
                              <?php else : ?>
                                 <img class="menu-img img-responsive img-thumbnail" src="<?php echo $basePath; ?>img/menu/menu1.jpg" alt="" />
                              <?php endif; ?>
                              <!-- Menu title / Heading -->
                              <h3><?php echo $catData->name; ?></h3>
                              <!-- Border for above heading -->
                              <div class="title-border <?php echo $dynamicBar; ?>"></div>
                           </div>
                           <!-- Menu item details -->

                           <div class="menu-details <?php echo $dynamicBar; ?>">
                              <!-- Menu list -->
                              <ul class="list-unstyled">
                                 <?php foreach ($itemsModel as $subKey => $iteData) : ?>
                                    <li>
                                       <div class="menu-list-item">
                                          <!-- Icon -->
                                          <i class="fa fa-angle-right"></i>
                                          <?php echo CHtml::link($iteData->itemName, array('eshop/proDetails', 'id' => $iteData->id), array('class' => '')); ?>
                                          <!-- Price badge -->
                                          <span class="pull-right">
                                             <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($iteData->id);?>
                                          </span>
                                          <div class="clearfix"></div>
                                       </div>
                                    </li>
                                    <?php
                                 endforeach; ?>
                              </ul>
                           </div><!-- / Menu details end -->
                        </div>
                        <?php endif;
                    } ?>
               </div>
            </div> <!-- /Menu container end -->
         </div>
      </div>
    <?php endif; ?>
   <!-- Menu End -->
   <!-- Pricing Start -->
   <?php $this->widget('EshopSpecialOffersLeft', array('limit' => 4)); ?>
   <!-- Pricing End -->
   <!-- Chefs End -->
   <!-- Testimonial Start -->
   <!--Recent Dishes -->
   <?php
   // last 1 month data
   $sqlNewArrival = "SELECT * FROM pos_items WHERE isEcommerce=" . Items::IS_ECOMMERCE . " AND crAt>=DATE_SUB(NOW(),INTERVAL 30 DAY)
                    AND status=" . Items::STATUS_ACTIVE . " GROUP BY itemName ORDER BY crAt DESC LIMIT 0,3";
   $newArrivalModel = Items::model()->findAllBySql($sqlNewArrival); ?>
   <div class="testimonial padd padding-top10 padding-bottom0">
      <div class="container">
         <div class="row">
            <div class="col-md-6">
               <!-- BLock heading -->
               <h3>Recent Dishes</h3>
               <!-- Flex slider Content -->
               <?php if (!empty($newArrivalModel)) : ?>
                  <div class="flexslider-recent flexslider">
                     <ul class="slides">
                        <?php foreach ($newArrivalModel as $newArrivalData) : ?>
                           <li>
                              <!-- Image for background -->
                              <?php if (!empty($newArrivalData->itemImages)) : ?>
                                 <?php echo CHtml::link('<img alt="" src="' . $newArrivalData->itemImages[0]->image . '" data-echo="' . $newArrivalData->itemImages[0]->image . '" class="img-responsive" />', array('eshop/proDetails', 'id' => $newArrivalData->id), array('title' => $newArrivalData->itemName)); ?>
                              <?php endif; ?>
                              <!-- Slide content -->
                              <div class="slider-content">
                                 <!-- Heading -->
                                 <?php if (!empty($newArrivalData->itemName)) : ?>
                                    <h4>
                                       <?php echo CHtml::link($newArrivalData->itemName, array('eshop/proDetails', 'id' => $newArrivalData->id)); ?>
                                    </h4>
                                 <?php endif; ?>
                                 <!-- Paragraph -->
                                 <p><?php echo substr($newArrivalData->description,0,100);?>...</p>
                              </div>
                           </li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
               <?php endif; ?>
            </div>
            <?php
            $modelItemReview = ItemReview::model()->findAll(array('condition' => 'status=:status',
                'params' => array(':status' => ItemReview::STATUS_ACTIVE),
                'limit' => '4',
                'order' => 'crAt DESC'
            )); ?>
            <div class="col-md-6">
               <!-- BLock heading -->
               <h3>Our Client Says</h3>
               <!-- Flex slider Content -->
               <div class="flexslider-testimonial flexslider">
                  <?php if (!empty($modelItemReview)) : ?>
                     <ul class="slides">
                        <?php
                        $i = 1;
                        foreach ($modelItemReview as $reviewProductData):
                           ?>
                           <li>
                              <!-- Testimonial Content -->
                              <div class="testimonial-item">
                                 <!-- Quote -->
                                 <span class="quote lblue">&#8220;</span>
                                 <!-- Your comments -->
                                 <blockquote>
                                    <!-- Paragraph -->
                                    <p><?php echo $reviewProductData->comment; ?></p>
                                 </blockquote>
                                 <!-- Heading with image -->
                                 <h4><img class="img-responsive img-circle" src="<?php echo $basePath; ?>img/user.jpg" alt="" /> <?php echo $reviewProductData->name; ?></h4>
                                 <div class="clearfix"></div>
                              </div>
                           </li>
                        <?php endforeach; ?>
                     </ul>
                  <?php endif; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Testimonial End -->
</div><!-- / Main Content End -->