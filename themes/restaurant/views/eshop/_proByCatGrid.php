<?php
$dataIndex = 1;
$dataIndex+=$index;
?>
<div class="col-md-3 col-sm-6">
   <div class="shopping-item">
      <!-- Image -->
      <?php
      if (!empty($data->itemImages)) :
         echo CHtml::link('<img alt="" src="' . $data->itemImages[0]->image . '" data-echo="' . $data->itemImages[0]->image . '" class="img-responsive" />', array('eshop/proDetails', 'id' => $data->id), array('title' => $data->itemName));
         ?>
      <?php endif; ?>
      <!-- Shopping item name / Heading -->
      <h4 class="">
         <?php echo CHtml::link($data->itemName, array('eshop/proDetails', 'id' => $data->id), array('title' => $data->itemName)); ?>
      </h4>

      <span class="item-price show">
         <?php if (Items::getItemWiseOfferPrice($data->id) > 0) : ?>
            <div class="price-prev pull-left" style="text-decoration: line-through;">
               <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($data->id); ?>
            </div>
            <div class="price-current pull-right"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($data->id); ?></div>
         <?php else : ?>
            <div class="price-current pull-right"><?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($data->id); ?></div>
         <?php endif; ?>
      </span>
      <div class="clearfix"></div>
      <!-- Shopping item hover block & link -->
      <div class="item-hover br-red hidden-xs"></div>
      <?php
      echo CHtml::link('Add to cart', array('eshop/proDetails', 'id' => $data->id), array('class' => 'link hidden-xs'));
      ?>
   </div>
</div>
<?php if (($dataIndex % 4) == 0) { ?>
   <div class="clearfix"></div>
<?php } ?>

