<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE TITLE & BREADCRUMB-->
<div class="col-xs-12 col-sm-8 col-md-9">
   <div id="hero">
      <div class="infoLeft">
         <div>
            <h3>
               Change Password
            </h3>
         </div>
      </div>
   </div>
</div>
<div class="col-md-7">
   <!-- Booking form area -->
   <div class="booking-form form">
      <!-- Heading -->
      <?php if (!empty($msg)) : ?>
         <?php echo $msg; ?>
      <?php endif; ?>

      <!-- Booking form -->
      <?php echo CHtml::beginForm('', 'post', array('id' => 'customer-change-password', 'class' => 'cf-style-1')); ?>
      <?php echo CHtml::errorSummary($form); ?>


      <div class="form-group">
         <?php echo CHtml::activeLabelEx($form, 'verifyoldpass'); ?>
         <?php echo CHtml::activePasswordField($form, 'verifyoldpass', array('value' => '', 'class' => 'form-control')); ?>
      </div>
      <div class="form-group">
         <?php echo CHtml::activeLabelEx($form, 'password'); ?>
         <?php echo CHtml::activePasswordField($form, 'password', array('value' => '', 'class' => 'form-control')); ?>
      </div>
      <div class="form-group">
         <?php echo CHtml::activeLabelEx($form, 'verifyPassword'); ?>
         <?php echo CHtml::activePasswordField($form, 'verifyPassword', array('value' => '', 'class' => 'form-control')); ?>
      </div>
      <!-- Form button -->
      <?php echo CHtml::submitButton('Update', array('class' => 'btn btn-danger btn-sm margin-top6')); ?>
      <?php echo CHtml::endForm(); ?>
   </div>
</div>



