<div class="col-xs-12 col-sm-8 col-md-9">
   <div id="hero">
      <div class="infoLeft">
         <div>
            <h3>
               Order List
            </h3>
         </div>
      </div>

   </div>
</div>
<div class="">
   <div class="col-xs-12 col-sm-8 col-md-9">
      <!-- Form inside table wrapper -->
      <div class="table-responsive">
         <!-- Ordering form -->
         <form role="form">
            <!-- Table -->
            <table class="table table-bordered">
               <thead>
                  <tr>
                     <th class="classSl" >Sl.No</th>
                     <th class="myorder" >Invoice No</th>
                     <th class="myorder" >Quantity</th>
                     <th class="myorder" >Price</th>
                     <th class="myorder" >Shipping</th>
                     <th class="myorder" >Discount</th>
                     <th class="myorder" >Date</th>
                     <th class="myorder" >Status</th>
                     <th class="myorder" >Details</th>
                  </tr>
               </thead>
               <tbody>
                  <?php foreach ($orderModel as $keyOrder => $orderValue) : ?>
                     <tr>
                        <td><?php echo $keyOrder + 1; ?></td>
                        <td><?php echo $orderValue->invNo; ?></td>
                        <td><?php echo $orderValue->totalQty; ?></td>
                        <td><?php echo Company::getCurrency() . '&nbsp;' . ($orderValue->totalPrice + $orderValue->totalTax); ?></td>
                        <td><?php echo Company::getCurrency() . '&nbsp;' . ShippingMethod::getShippingMethodPrice($orderValue->shippingId, $orderValue->totalPrice); ?></td>
                        <td><?php echo Company::getCurrency() . '&nbsp;' . $orderValue->totalDiscount; ?></td>
                        <td><?php echo date("Y-m-d", strtotime($orderValue->orderDate)); ?></td>
                        <td><?php echo Lookup::item('Status', $orderValue->status); ?></td>
                        <td><?php echo CHtml::link('View', array('eshop/customerOderDetails', 'id' => $orderValue->id), array('class' => 'le-button huge btn btn-danger btn-sm')); ?></td>
                     </tr>
                  <?php endforeach; ?>


               </tbody></table>
         </form><!--/ Table End-->
      </div><!--/ Table responsive class end -->
   </div>
</div>

