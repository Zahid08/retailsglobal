
<script>
   $(function () {
      // add to cart from wishlist
      $(".add_to_cart_from_wishlist").click(function ()
      {
         // values & validations
         var itemId = $(this).attr('id'),
                 id = $(this).attr('data-id'),
                 quantity = 1;

         $.ajax({
            type: "POST",
            dataType: "json",
            beforeSend: function () {
               $(".preloader").show();
            },
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addToCartFromWishList/",
            data: {
               itemId: itemId,
               id: id,
               quantity: quantity,
            },
            success: function (data) {
               $(".preloader").hide();

               // update shopping bag
               $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
                  $(this).removeClass("basket").addClass("basket open");
                  $(this).slideDown(200, function () {
                     $('html, body').delay('200').animate({
                        scrollTop: $(this).offset().top - 111
                     }, 200);
                  });
               });
               // wishlist count
               $("#wishListCount").html(data);
               $("#wishlist_item_holder_" + id).fadeOut(500, function () {
                  $(this).remove();
               });

            },
            error: function () {
            }
         });
      });

      // remove from wishlist
      $(".remove_from_my_wishlist").click(function ()
      {
         // values & validations
         var id = $(this).attr('id');

         $.ajax({
            type: "POST",
            dataType: "json",
            beforeSend: function () {
               $(".preloader").show();
            },
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromWishList/",
            data: {
               id: id,
            },
            success: function (data) {
               if (data != 'invalid')
               {
                  $(".preloader").hide();
                  $("#wishListCount").html(data);
                  $("#wishlist_item_holder_" + id).fadeOut(500, function () {
                     $(this).remove();
                  });
               }
            },
            error: function () {
            }
         });
      });
   });
</script>
<div class="col-xs-12 col-sm-8 col-md-9">
   <div id="hero">
      <div class="infoLeft">
         <div>
            <h3>
               My Wish list
            </h3>
         </div>
      </div>
   </div>
</div>


<div class="col-xs-12 col-sm-8 col-md-9">
   <!-- Form inside table wrapper -->
   <div class="table-responsive">
      <table class="table table-bordered">
         <tbody>
            <!-- /.cart-item starts-->
            <?php
            if (!empty($wishListModel)) :
               foreach ($wishListModel as $wishKey => $wishListData):
                  ?>
                  <tr id="wishlist_item_holder_<?php echo $wishListData->id; ?>" >
                     <td style="width:80px;">
                        <?php if (!empty($wishListData->item->itemImages)) : echo CHtml::link('<img class="lazy" alt="" src="' . $wishListData->item->itemImages[0]->image . '" style="width:73px;height:73px;" />', array('eshop/proDetails', 'id' => $wishListData->itemId), array('class' => 'thumb-holder', 'title' => $wishListData->item->itemName)); ?>
      <?php endif; ?>
                     </td>
                     <td>
                        <div class="title" style="height:53px;">
      <?php echo CHtml::link($wishListData->item->itemName, array('eshop/proDetails', 'id' => $wishListData->itemId), array('title' => $wishListData->item->itemName)); ?>
                        </div>
                        <div class="brand">
                           <a id="<?php echo $wishListData->itemId; ?>" data-id="<?php echo $wishListData->id; ?>" class="add_to_cart_from_wishlist" href="javascript:void(0);" title="add to cart"><i class="fa fa-shopping-cart"></i> add to cart</a>
                        </div>
                     </td>
                     <td>
                        <div class="price">
                           <?php if (Items::getItemWiseOfferPrice($wishListData->itemId) > 0) : ?>
                              <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($wishListData->itemId); ?>
                           <?php else : ?>
                              <?php echo Company::getCurrency(); ?> <span><?php echo $wishListData->item->sellPrice; ?></span>
      <?php endif; ?>
                        </div>
                     </td>
                     <td>
                        <a id="<?php echo $wishListData->id; ?>" class="close-btn remove_from_my_wishlist" href="javascript:void(0);" title="remove from wishlist"><i class="fa fa-times"></i></a>
                     </td>
                  </tr>
               <?php
               endforeach;
            else :
               ?>
               <tr id="wishlist_item_holder_" >
                  <td colspan='4' style="text-align: center;"> Your Wish List is now empty ! </td>
               </tr>
<?php endif; ?>
         </tbody>
      </table>
   </div>
</div>
</section>