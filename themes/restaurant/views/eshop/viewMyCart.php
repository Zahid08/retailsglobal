<style type="text/css">
   .quantity .le-quantity input {width: 70%;}
</style>
<script>
   $(function () {
      //increase amount
      $('.add').on('click', function () {
         var itemId = $(this).attr('id');
         var $qty = $("#quantity_" + itemId);
         var currentVal = parseInt($qty.val());
         if (!isNaN(currentVal)) {
            $qty.val(currentVal + 1);
         }
      });

      $('.minus').on('click', function () {
         var itemId = $(this).attr('id');
         var $qty = $("#quantity_" + itemId);
         var currentVal = parseInt($qty.val());
         if (!isNaN(currentVal) && currentVal > 0) {
            $qty.val(currentVal - 1);
         }
      });

      // item quantity updated +/-
      $(".quantity").click(function () {
         var itemId = $(this).attr('id');
         var quantity = $("#quantity_" + itemId).val();

         // price calculation
         var item_price = parseFloat($("#priceInd_" + itemId).val());
         var item_price_upd = item_price * quantity;

         $.ajax({
            type: "POST",
            dataType: "json",
            beforeSend: function () {
               $(".preloader").show();
            },
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/updateQty/",
            data: {
               itemId: itemId,
               quantity: quantity
            },
            success: function (data) {
               $(".preloader").hide();

               // update price
               $("#price_" + itemId).html(item_price_upd);
               $("#total_subtotal").html(data.total_price);
               $("#total_discount").html(data.total_discount);
               var dataGrandTotal = (data.total_price - data.total_discount);
               $("#total_grandtotal").html(dataGrandTotal);

               // update shopping bag
               $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
               });
            },
            error: function () {
            }
         });
      });

      // remove from cart global
      $(".remove_from_cart_my_cart").click(function ()
      {
         // values & validations
         var itemId = $(this).attr('id');

         // price calculation
         var item_price = parseFloat($("#price_" + itemId).html());
         var total_subtotal = parseFloat($("#total_subtotal").html());
         var totalbal_subtotal = total_subtotal - item_price;

         var total_grandtotal = parseFloat($("#total_grandtotal").html());
         var totalbal_grandtotal = total_grandtotal - item_price;

         $.ajax({
            type: "POST",
            dataType: "json",
            beforeSend: function () {
               $(".preloader").show();
            },
            url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
            data: {
               itemId: itemId,
            },
            success: function (data) {
               $(".preloader").hide();
               $("#cart_item_holder_" + itemId).fadeOut(500, function () {
                  $(this).remove();
               });

               // update price
               $("#total_subtotal").html(totalbal_subtotal);
               $("#total_grandtotal").html(totalbal_grandtotal);

               // update shopping bag
               $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
               });
            },
            error: function () {
            }
         });
      });

      // item moved to wishlist
      $(".move_to_wishlist").click(function () {
<?php if (!empty(Yii::app()->session['custId'])) : ?>
            var itemId = $(this).attr('id');

            // price calculation
            var item_price = parseFloat($("#price_" + itemId).html());
            var total_subtotal = parseFloat($("#total_subtotal").html());
            var totalbal_subtotal = total_subtotal - item_price;

            var total_grandtotal = parseFloat($("#total_grandtotal").html());
            var totalbal_grandtotal = total_grandtotal - item_price;

            $.ajax({
               type: "POST",
               dataType: "json",
               beforeSend: function () {
                  $(".preloader").show();
               },
               url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
               data: {
                  itemId: itemId,
               },
               success: function (data) {
                  $(".preloader").hide();
                  if (data == 'exists')
                     alert("Already added to wishlist !");
                  else
                  {
                     $("#cart_item_holder_" + itemId).fadeOut(500, function () {
                        $(this).remove();
                     });

                     // update price
                     $("#total_subtotal").html(totalbal_subtotal);
                     $("#total_grandtotal").html(totalbal_grandtotal);

                     // update wishlist count
                     $("#wishListCount").html(data);
                     // update shopping bag
                     $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
                     });
                  }
               },
               error: function () {
               }
            });
<?php else : ?> alert("please signin first !");
<?php endif; ?>
      });
   });
</script>
<!-- ========================================= BREADCRUMB ========================================= -->
<div class="container">
   <h2 class="white"></h2>
   <ol class="breadcrumb">
      <li><?php echo CHtml::link('Home', array('eshop/index'), array('title' => 'Home')); ?></li>
      <li class="active"><?php echo $this->metaTitle; ?></li>
   </ol>
   <div class="clearfix"></div>
</div>
<!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="inner-page">
   <div class="container">
      <!-- ========================================= CONTENT ========================================= -->
      <?php if ($total_qty > 0) : ?>
         <div class="col-xs-12 col-md-9 items-holder no-margin">
            <div class="row no-margin cart-item">
               <div class="col-xs-12 col-sm-1 ">
                  Sl
               </div>
               <div class="col-xs-12 col-sm-2 no-margin">
                  Image
               </div>

               <div class="col-xs-12 col-sm-5 ">
                  Product Name
               </div>

               <div class="col-xs-12 col-sm-2 no-margin">
                  Quantity
               </div>

               <div class="col-xs-12 col-sm-2 no-margin">
                  Total
               </div>
            </div><!-- /.cart-item ends-->

            <!-- /.cart-item starts-->
            <?php
            $slNo = 0;
            foreach ($cart->getItems() as $order_code => $quantity) : $slNo++;
               ?>
               <div class="row no-margin cart-item" id="cart_item_holder_<?php echo $order_code; ?>">
                  <div class="col-xs-12 col-sm-1">  <?php echo $slNo; ?>     </div>
                  <div class="col-xs-12 col-sm-2 no-margin">
                     <?php if (!empty($cart->getItemImages($order_code))) : echo CHtml::link('<img class="lazy" alt="" src="' . $cart->getItemImages($order_code) . '" style="width:73px;height:73px;" />', array('eshop/proDetails', 'id' => $order_code), array('class' => 'thumb-holder', 'title' => $cart->getItemName($order_code))); ?>
                     <?php endif; ?>
                  </div>

                  <div class="col-xs-12 col-sm-5 ">
                     <div class="title">
                        <?php echo CHtml::link($cart->getItemName($order_code), array('eshop/proDetails', 'id' => $order_code), array('title' => $cart->getItemName($order_code))); ?>
                     </div>
                     <div class="brand"><?php echo $cart->getItemBrand($order_code); ?></div>
                     <div class="brand"><a id="<?php echo $order_code; ?>" class="move_to_wishlist" href="javascript:void(0);" title="Move to Wishlist"><i class="fa fa-heart"></i> Move to Wishlist</a></div>
                  </div>

                  <div class="col-xs-12 col-sm-2 no-margin">
                     <div class="quantity" id="<?php echo $order_code; ?>">
                        <div class="le-quantity">
                           <span id="<?php echo $order_code; ?>" class="clickable add icon-plus-sign-alt"><i class="fa fa-plus-square"></i></span>
                           <input id="quantity_<?php echo $order_code; ?>" name="quantity" readonly="readonly" type="text" value="<?php echo $quantity; ?>" />
                           <span id="<?php echo $order_code; ?>" class="clickable minus icon-minus-sign-alt"><i class="fa fa-minus-square"></i></span>
                        </div>
                     </div>
                  </div>

                  <div class="col-xs-12 col-sm-2 no-margin">
                     <div class="price">
                        <?php if (Items::getItemWiseOfferPrice($order_code) > 0) : ?>
                           <input type="hidden" id="priceInd_<?php echo $order_code; ?>" value="<?php echo Items::getItemWiseOfferPrice($order_code); ?>" />
                           <?php echo Company::getCurrency(); ?> <span id="price_<?php echo $order_code; ?>"><?php echo Items::getItemWiseOfferPrice($order_code) * $quantity; ?></span>
                        <?php else : ?>
                           <input type="hidden" id="priceInd_<?php echo $order_code; ?>" value="<?php echo $cart->getItemPrice($order_code); ?>" />
                           <?php echo Company::getCurrency(); ?> <span id="price_<?php echo $order_code; ?>"><?php echo $cart->getItemPrice($order_code) * $quantity; ?></span>
                        <?php endif; ?>
                     </div>
                     <a id="<?php echo $order_code; ?>" class="close-btn remove_from_cart_my_cart" href="javascript:void(0);"><i class="fa fa-times"></i></a>
                  </div>
               </div><!-- /.cart-item ends-->
            <?php endforeach; ?>
         </div>
         <!-- ========================================= CONTENT : END ========================================= -->

         <!-- ========================================= SIDEBAR ========================================= -->
         <div class="col-xs-12 col-md-3 no-margin sidebar ">
            <div class="widget cart-summary">
               <h1 class="border">Shopping cart</h1>
               <div class="body">
                  <ul class="tabled-data no-border inverse-bold">
                     <li>
                        <label>cart subtotal</label>
                        <div class="value pull-right">
                           <span class="sign"><?php echo Company::getCurrency(); ?></span>
                           <span id="total_subtotal"><?php echo $total_price; ?></span>
                        </div>
                     </li>
                     <li>
                        <label>discount</label>
                        <div class="value pull-right">
                           <span class="sign"><?php echo Company::getCurrency(); ?></span>
                           <span id="total_discount"><?php echo $total_discount; ?></span>
                        </div>
                     </li>
                  </ul>
                  <ul id="total-price" class="tabled-data inverse-bold no-border">
                     <li>
                        <label>order total</label>
                        <div class="value pull-right">
                           <span class="sign"><?php echo Company::getCurrency(); ?></span>
                           <span id="total_grandtotal"><?php echo ($total_price - $total_discount); ?></span>
                        </div>
                     </li>
                  </ul>
                  <div class="buttons-holder">
                     <?php
                     if (!empty(Yii::app()->session['custId']))
                        echo CHtml::link('Checkout', array('checkoutProcess/checkoutShipping'), array('class' => 'btn btn-danger rs_checkout'));
                     else
                        echo CHtml::link('Checkout', array('checkoutProcess/checkout'), array('class' => 'btn btn-danger rs_checkout'));
                     ?>
                     <a class="btn btn-danger pull-left" href="javascript: window.history.go(-1);" >Continue shopping</a>
                  </div>
               </div>
            </div><!-- /.widget -->
         </div><!-- /.sidebar -->
         <?php
      else : echo 'Your cart is empty !';
      endif;
      ?>
      <div style="clear:both;"></div>
      <?php $this->widget('EshopRecommanded', array('limit' => 4)); ?>
   </div>
</div>
