<style>
   .ginfo_left{
      width: 300px;
      float: left;
   }
   .ginfo_right{
      width: 300px;
      float: left;
   }
   .generalInfo tr {
      height: 30px;
      line-height: 29px;
   }
   .generalInfo{
      margin-top: 25px;
   }
   .infoLeft{
      width: 80%;
      float: left;
   }
   .infoRight{
      width: 20%;
      float: left;
   }
</style>
<?php $basePath = Yii::app()->theme->baseUrl . '/'; ?>


<div class="col-xs-12 col-sm-8 col-md-9">
   <div id="hero">
      <?php if (!empty($model)): ?>
         <div class="form-group pull-right">
            <?php echo CHtml::link('Update', array('eshop/CustomerInfo', 'id' => $model->id), array('class' => 'btn btn-danger btn-sm')); ?>
         </div>
         <div>
            <div>
               <h3>
                  General Information <?php echo $model->title . '.' . $model->name; ?>
               </h3>
            </div>
         </div>

      <?php endif; ?>
   </div>
</div>


<div>
   <div class="col-xs-12 col-sm-8 col-md-9">
      <!-- Form inside table wrapper -->
      <div class="table-responsive">
         <?php if (!empty($model)): ?>
            <!-- Ordering form -->
            <form role="form">
               <!-- Table -->
               <table class="table table-bordered">
                  <tbody>
                     <tr>
                        <td style="width:200px;">User Name</td>
                        <td><?php echo $modelUser->username; ?></td>
                     </tr>
                     <tr>
                        <td>Email</td>
                        <td><?php echo $model->email; ?></td>
                     </tr>
                     <tr>
                        <td>Phone</td>
                        <td><?php echo $model->phone; ?></td>
                     </tr>
                     <tr>
                        <td>Gender</td>
                        <td><?php echo $model->gender; ?></td>
                     </tr>
                     <tr>
                        <td>Address</td>
                        <td><?php echo $model->addressline; ?></td>
                     </tr>
                     <tr>
                        <td>City</td>
                        <td><?php echo $model->city; ?></td>
                     </tr>

                  </tbody></table>
            </form><!--/ Table End-->
         <?php endif; ?>
      </div><!--/ Table responsive class end -->
   </div>
</div>