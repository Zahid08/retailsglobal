<style>
	table, tr,td{
		border: 0;
	}
	.billling_shipping a{color:#fff;}
</style>
<table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: Arial, sans-serif;">
	<tbody>
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
					<tr>
						<td bgcolor="#70bbd9" align="center" style="padding: 20px 0 30px 0; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
						   <?php if(!empty($companyModel)) : ?>
							   <img src="<?php echo $_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.$companyModel->logo;?>" style="height:78px;" />	
						   <?php endif; ?>
						</td>
					</tr>
					<tr>
						<td bgcolor="#54afd3" align="center">
							<table>
								<tbody>
									<tr>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $companyModel->domain;?>" target="_blank" title="Home">Home</a>
										</td>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $companyModel->domain;?>" target="_blank" title="Home">Support</a>
										</td>
										<td width="100" align="center" style="padding:8px;">
											<a style="color:#fff;text-decoration:none;" href="<?php echo $companyModel->domain;?>" target="_blank" title="Home">Blog</a>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<table cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr>
										<td bgcolor="#d0d0d0" width="4px"></td>
										<td bgcolor="#eee">
											<h4 style="margin:10px 20px; color:#555; font-weight:100;">Order Details</h4>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#fcfcfc" style="padding:40px 30px 40px 30px">
						   <table>
							   <tbody>
								   <tr>
									   <td align="center" style="font-size: 22px;padding:0px 30px 40px 30px;">
										   <a href="javascript:void(0);" target="_blank" style="color:#505050"><?php echo $orderModel->invNo.'('.date("F j Y, g:i a",strtotime($orderModel->orderDate)).')';?></a>
									   </td>
								   </tr>
								   <tr>
									   <td>
									   	   <?php if(!empty($orderDetailsModel)) : ?>
										   <table width="100%" style="border:1px solid #eee; border-collapse:collapse;">
											   <tbody>
												   <tr style="border:1px solid #eee;">
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Product
													   </td>
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Quantity
													   </td>
													   <td bgcolor="#eee" align="center" style="border:1px solid #eee;padding:8px">
														   Price
													   </td>
												   </tr>
												   <?php foreach($orderDetailsModel as $key=>$data) : ?>
												   <tr style="border:1px solid #eee;">
													   <td align="center" style="border:1px solid #eee;">
														   <table width="100%">
															   <tbody>
																   <tr><td><?php echo $data->item->itemName;?></td></tr>
															   </tbody>
													   		</table>
													   </td>
													   <td align="center" style="border:1px solid #eee;"><?php echo $data->qty;?></td>
													   <td align="center" style="border:1px solid #eee;"><?php echo Company::getCurrency().'&nbsp;'.($data->salesPrice-$data->discountPrice)*$data->qty;?></td>
												   </tr>
												   <?php endforeach; ?>
											   </tbody>
										   </table>
										   <?php endif;?>  
										   
										   <table width="100%" style="border:1px solid #eee;border-collapse:collapse;margin-top:10px " >
											   <tbody>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Cart Subtotal :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalPrice);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Shipping :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalTax);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Total :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalPrice+$orderModel->totalTax);?>
													   </td>
												   </tr>
                                                   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Discount :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalDiscount);?>
													   </td>
												   </tr>
												   <tr>
													   <td width="370px" style="border:1px solid #eee;padding:5px;">
														   Grand Total :
													   </td>
													   <td style="border:1px solid #eee;padding:5px;text-align:right;">
														   <?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($orderModel->totalPrice+$orderModel->totalTax)-$orderModel->totalDiscount);?>
													   </td>
												   </tr>
											   </tbody>
										   </table>
									   </td>
								   </tr>
							   </tbody>
						   </table>
						</td>
					</tr>
					<tr>
						<td>
							<table cellspacing="0" cellpadding="0" width="100%">
								<tbody>
									<tr>
										<td bgcolor="#a51c04" width="4px"></td>
										<td bgcolor="#d1400f">
											<h4 style="margin:10px 20px; color:#fff; font-weight:100;font-size: 28px;" >Customer details</h4>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#e35525" style="padding:10px 30px 10px 30px; color:#fff;">
							<table width="100%">
								<tbody>
									<tr>
										<td style="padding-right:10px">
										   <table>
											   <tbody>
												   <tr>
													   <td>
														   <h3 style="margin:0;">Billing address</h4>
													   </td>
												   </tr>
												   <tr>
													   <td class="billling_shipping">
														   <p style="margin-top:0;margin-left:0;margin-right:0;margin-bottom:20px"><?php echo $orderModel->billingAddress;?></p>
													   </td>
												   </tr>
											   </tbody>
										   </table>
										</td>
										<td style="padding-left:10px">
											<table>
												<tbody>
													<tr>
														<td>
															<h3 style="margin:0;">Shipping address</h4>
														</td>
													</tr>
													<tr>
														<td class="billling_shipping">
															<p style="margin-top:0;margin-left:0;margin-right:0;margin-bottom:20px"><?php echo $orderModel->shippingAddress;?></p>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<?php if(!empty($socialModel)) : ?>
					<tr>
						<td align="center" bgcolor="#eaeaea" style="padding:10px 30px 10px 30px;">
							<?php foreach($socialModel as $socialKey=>$socialData) :
								echo '<a href="'.$socialData->url.'" title="'.$socialData->name.'" target="_blank" style="margin-right:5px;"><img alt="'.$socialData->name.'" src="'.$_SERVER['HTTP_HOST'].Yii::app()->request->baseUrl.$socialData->icon.'" height="30"></a>';
				 			endforeach; ?>
						</td>
					</tr>
					<?php endif;?>
					<tr>
						<td align="center" bgcolor="#dedede" style="font-size:9px;padding:10px">
							<p>&copy; <?php if(!empty($companyModel)) echo date("Y",strtotime($companyModel->crAt));?>  <a href="javascript:void(0);"><?php if(!empty($companyModel)) echo $companyModel->name;?></a> - all rights reserved | <?php if(!empty($branchModel)) echo $branchModel->addressline.','.$branchModel->city.','.$branchModel->postalcode;?></p>
							
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
	</table>