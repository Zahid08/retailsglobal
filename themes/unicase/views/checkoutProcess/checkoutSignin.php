<script type="text/javascript" language="javascript">
	$(function() 
	{
		$("#optionsRadios1").click(function() {
			$("#displayPassword").hide("slow");
		});
		$("#optionsRadios2").click(function() {
			$("#displayPassword").show("slow");
		});
	});
</script>
<div class="row">
    <div class="col-lg-12">
		<div class="progressbar">
			<ul class="list-inline">
				<li class="active-pgb">
					<span>1</span>
					<h5>Shopping Bag</h5>
				</li>
				<li class="active-pgb">
					<span>2</span>
					<h5>Checkout</h5>
				</li>
				<li>
					<span>3</span>
					<h5>Thank You</h5>
				</li>
			</ul>
		</div>
		<!-- .End Progressbar -->
		<div class="Breadcrumbs">
			<div id="crumbs">
				<ul class="list-inline">
                    <li><a href=""><div><svg class="svg logo-svg" width="28" height="23" viewBox="0 0 31 30" style=""><g transform="scale(0.03125 0.03125)"><path d="M928 128h-832c-52.8 0-96 43.2-96 96v576c0 52.8 43.2 96 96 96h832c52.8 0 96-43.2 96-96v-576c0-52.8-43.2-96-96-96zM96 192h832c17.346 0 32 14.654 32 32v96h-896v-96c0-17.346 14.654-32 32-32zM928 832h-832c-17.346 0-32-14.654-32-32v-288h896v288c0 17.346-14.654 32-32 32zM128 640h64v128h-64zM256 640h64v128h-64zM384 640h64v128h-64z"/></g></svg>
                        </div>Checkout</a>
					</li>
					<li class="active-crumbs"><a href="#1"><span></span>Login or<br>checkout as a guest</a></li>
					<li><a href="#2"><span></span>Billing <br> & Shipping Details</a></li>
					<li><a href="#3"><span></span>Shipping Options</a></li>
					<li><a href="#4"><span></span>Payment <br> Options</a></li>
					<li><a href="#5"><span></span>Review</a></li>
				</ul>
			</div>
			<!-- #End crumbs -->
		  </div>
		  <!-- .End Breadcrumbs -->
    </div>
    <!-- .End col-lg-12 -->
</div>
<!-- .End row -->

<div class="row">
	<?php if(isset($errMsg) && !empty($errMsg)) : ?>
    <div class="alert-box alert alert-danger"><span>Error : </span><?php echo $errMsg;?></div>
    <?php endif;?>
    
    <div class="form-wrap" id="checkout-form">
        <form action="" class="checkout-login" role="form" method="post">
            <div class="form-group">
                <label for="input-email" class="label-title">Email Address</label>
                <input name="email" id="input-email" class="form-control" type="email" placeholder="info@example.com">
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked><span class="check"></span>I'm a guest customer(You can create an account later)
                </label>
            </div>
            <div class="radio">
                <label>
                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"><span class="check"></span>I'm a returning customer and my password is
                </label>
            </div>
            
            <div id="displayPassword" style="display:none;">
                <div class="form-group">
					<input type="hidden" name="branchId" value="<?php echo Branch::getDefaultBranch(Branch::STATUS_ACTIVE);?>" />
                    <label for="input-pass" class="label-title">Password</label>
                    <input name="password" id="input-pass" class="form-control" type="password" placeholder="Enter your password">
                </div>
                <div class="checkbox">
                    <!--<label>
                        <input type="checkbox" name="rememberMe" value="">
                        <span class="check"></span>
                        Remember me
                    </label>-->
                </div>
                <?php echo CHtml::link('<i>Forgotten your password?</i>',array('eshop/customerSignin'),array('style'=>'float:right;','target'=>'_blank'));?>
            </div>
            <button type="submit" class="btn btn-continue btn-primary pull-right">Continue</button>
        </form>
    </div>
</div>
<!-- .End row -->