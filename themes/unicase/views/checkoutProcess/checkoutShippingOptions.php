<div class="row">
    <div class="col-lg-12">
		<div class="progressbar">
			<ul class="list-inline">
				<li class="active-pgb">
					<span>1</span>
					<h5>Shopping Bag</h5>
				</li>
				<li class="active-pgb">
					<span>2</span>
					<h5>Checkout</h5>
				</li>
				<li>
					<span>3</span>
					<h5>Thank You</h5>
				</li>
			</ul>
		</div>
		<!-- .End Progressbar -->
		<div class="Breadcrumbs">
			<div id="crumbs">
				<ul class="list-inline">
                    <li><a href=""><div><svg class="svg logo-svg" width="28" height="23" viewBox="0 0 31 30" style=""><g transform="scale(0.03125 0.03125)"><path d="M928 128h-832c-52.8 0-96 43.2-96 96v576c0 52.8 43.2 96 96 96h832c52.8 0 96-43.2 96-96v-576c0-52.8-43.2-96-96-96zM96 192h832c17.346 0 32 14.654 32 32v96h-896v-96c0-17.346 14.654-32 32-32zM928 832h-832c-17.346 0-32-14.654-32-32v-288h896v288c0 17.346-14.654 32-32 32zM128 640h64v128h-64zM256 640h64v128h-64zM384 640h64v128h-64z"/></g></svg>
                        </div>Checkout</a>
					</li>
					<li class="finish-crumbs"><a href="#1"><span></span>Login or<br>checkout as a guest</a></li>
					<li class="finish-crumbs"><a href="#2"><span></span>Billing <br> & Shipping Details</a></li>
					<li class="active-crumbs"><a href="#3"><span></span>Shipping Options</a></li>
					<li><a href="#4"><span></span>Payment <br> Options</a></li>
					<li><a href="#5"><span></span>Review</a></li>
				</ul>
			</div>
			<!-- #End crumbs -->
		  </div>
		  <!-- .End Breadcrumbs -->
    </div>
    <!-- .End col-lg-12 -->
</div>
<!-- .End row -->

<div class="row">
	<?php if(isset($errMsg) && !empty($errMsg)) : ?>
    <div class="alert-box alert alert-danger"><span>Error : </span><?php echo $errMsg;?></div>
	<?php endif;?>

    <div class="form-wrap" id="checkout-form">
        <?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'shipping-method-form',
				'enableAjaxValidation'=>true,
				'htmlOptions'=>array(
					'class'=>'login-form cf-style-1',
					'role'=>'form'
				)
		)); ?>  
	   <div class="row">
		   <div class="col-lg-8">
		   		<h4>Overnight Shipping by <?php if(!empty($companyModel)) echo $companyModel->name;?></h4>
	   			<h4>Receive your item(s) in</h4>
	   	   </div>
		   <div class="col-lg-4 free">
			  <em>
			   <span>Below method</span><br>
			   shipping business days
			   </em>
		   </div>
	   </div>
		<div class="form-group">
			<label for="selection01" class="label-title">If no one is available to sign for your order, it will be: <span class="required">*</span></label>
			<?php echo CHtml::dropDownList('shippingMethod','',ShippingMethod::getAllShippingMethod(ShippingMethod::STATUS_ACTIVE), array('id'=>'shippingMethod','class'=>'form-control','prompt'=>'Select Shipping Method')); ?>
		</div>
		<em>If you opt to leave your order unattended,<br><?php if(!empty($companyModel)) echo $companyModel->name;?> will not be responsible for any lost or stolen packages.</em>
<!--		<button type="submit" class="btn btn-back btn-default pull-left" onclick="window.history.back();">Back</button>-->
		<button type="submit" class="btn btn-continue btn-primary pull-right">Continue</button>
        <?php $this->endWidget(); ?>
    </div>
</div>
<!-- .End row -->