<!DOCTYPE html>
<html lang="en">
<head>	
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="MediaCenter, Template, eCommerce">
    <meta name="robots" content="all">
    <title><?php echo isset($this->metaTitle) ? CHtml::encode($this->metaTitle) : CHtml::encode($this->pageTitle);?></title>
    <?php if(isset($this->metaKeywords)){ ?><meta name="keywords" content="<?php echo CHtml::encode($this->metaKeywords); ?>" /><?php  } ?>
    <?php if(isset($this->metaDescription)){ ?> <meta name="description" content="<?php echo CHtml::encode($this->metaDescription); ?>" /><?php } ?>
    
    <meta name="subject" content="Unlocklive" />
    <meta name="language" content="English" />
    <meta name="copyright" content="© www.unlocklive.com/">
    <meta http-equiv="author" content="Md.Kamruzzaman :: <kzaman.badal@gmail.com>" />
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="7 days">
    <meta name="Googlebot" content="all" />
    <meta name="language" content="en" />
    <meta http-equiv="imagetoolbar" content="no" />
    <meta name="Rating" content="General" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta name="resource-type" content="document" />
    <meta name="distribution" content="Global" />
    <meta name="coverage" content="Worldwide" />
    <meta name = "Server" content = "New" />
    <meta name="expires" content="0" />
    <meta name="audience" content="all, experts, advanced, professionals, business, software" />
    <meta name="reply-to" content="info@unlocklive.com" />
       
    <?php $companyModel = Company::model()->find('status=:status',array(':status'=>Company::STATUS_ACTIVE));?>
    
    <!-- Og:tag # https://developers.facebook.com/docs/sharing/webmasters#images / https://developers.facebook.com/tools/debug/og/object/ -->
    <meta property="og:title" content="<?php echo $companyModel->name;?>" />
    <meta property="og:site_name" content="<?php echo $companyModel->domain;?>"/>
    <meta property="og:type" content="website" />     
    <?php if(!empty($this->metaOgUrl)){ ?> <meta property="og:url" content="<?php echo CHtml::encode($this->metaOgUrl);?>" /> <?php } ?>
    <?php /* if(!empty($this->metaOgImage)){ ?> <meta property="og:image" content="og_decoder.php?data=<?php echo CHtml::encode($this->metaOgImage);?>"/><?php } */?>
    
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl.$companyModel->favicon;?>"/>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/bootstrap.min.css">
    
    <!-- Customizable CSS -->
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/main.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/green.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/owl.transitions.css">
    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/lightbox.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/animate.min.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/rateit.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/bootstrap-select.min.css">

    <!-- Demo Purpose Only. Should be removed in production -->
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/config.css">

    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/green.css" rel="alternate stylesheet" title="Green color">
    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/blue.css" rel="alternate stylesheet" title="Blue color">
    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/red.css" rel="alternate stylesheet" title="Red color">
    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/orange.css" rel="alternate stylesheet" title="Orange color">
    <link href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/dark-green.css" rel="alternate stylesheet" title="Darkgreen color">
    <!-- Demo Purpose Only. Should be removed in production : END -->

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="<?php echo Yii::app()->session['themesnames'].'/assests/';?>css/font-awesome.min.css">

    <!-- Fonts --> 
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
    <!--[if lt IE 9]>
        <script src="<?php echo Yii::app()->session['themesnames'].'/assests/';?>js/html5shiv.js"></script>
        <script src="<?php echo Yii::app()->session['themesnames'].'/assests/';?>js/respond.min.js"></script>
    <![endif]-->
    <?php // custom top css / js scripts
		$csModelTop = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_TOP,':status'=>CustomScript::STATUS_ACTIVE));
		if(!empty($csModelTop)) : 
			foreach ($csModelTop as $csKey => $csData) :						
			    if(!empty($csData->script)): echo $csData->script;
				endif; 
			endforeach;  
		endif;  
	 ?>
</head>
<body class="cnt-home">
    <header class="header-style-1">
    <?php  
        $this->widget('EshopTopPart'); 
        $this->widget('EshopTopMenu');
     ?>
    </header>
    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
            <?php echo $content;?> 
            <?php $this->widget('EshopTopBrands'); ?>
        </div>
        <?php $this->widget('EshopFooterPart'); ?>
        <div class="preloader">
            <div class="preloader-container">
                <span class="animated-preloader"></span>
            </div>
        </div>
    </div>
    <?php 
        Yii::app()->clientScript->registerCoreScript('jquery'); 
        Yii::app()->clientScript->registerCoreScript('jquery.ui'); 
    ?>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/';?>/js/jquery-migrate-1.2.1.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/bootstrap-hover-dropdown.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/owl.carousel.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/echo.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/jquery.easing-1.3.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/bootstrap-slider.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/jquery.rateit.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/bootstrap-select.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/wow.min.js"></script>
    <script src="<?php echo Yii::app()->session['themesnames'].'/assests/'; ?>js/scripts.js"></script>
    
    <!-- global ajax call starts-->
	<script>
	$(function() {
		// remove from cart global
		$(".remove_from_cart").click(function() 
		{
			// values & validations
			var itemId = $(this).attr('id');
			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
				data: {
					itemId: itemId, 
				},					
				success: function(data) {
					$(".preloader").hide();

					// update shopping bag 
					$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
						$(this).removeClass("basket").addClass("basket open");
						$(this).show();
					});
				},
				error: function() {}
			});	
		});
		
		// item add to wishlist global
		$(".btn-add-to-wishlist").click(function() {
			<?php if(!empty(Yii::app()->session['custId'])) : ?>
				var itemId = $(this).attr('id');
				$.ajax({
					type: "POST",
					dataType: "json",
					beforeSend : function() {
						$(".preloader").show();
					},
					url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
					data: {
						itemId: itemId, 
					},					
					success: function(data) {
						$(".preloader").hide();
						if(data=='exists') alert("Already added to wishlist !");
						else 
						{
							$("#wishListCount").html(data);
							alert("Added to wishlist !");
						}
					},
					error: function() {}
				});
			<?php else : ?> alert("please signin first !"); <?php endif;?>
		});

	});
	</script>
	<!-- global ajax call ends -->
    <?php // custom bottom css / js scripts
		$csModelBottom = CustomScript::model()->findAll('position=:position AND status=:status',array(':position'=>CustomScript::POSITION_BOTTOM,':status'=>CustomScript::STATUS_ACTIVE));
		if(!empty($csModelBottom)) : 
			foreach ($csModelBottom as $csKey => $csData) :						
			    if(!empty($csData->script)): echo $csData->script;
				endif; 
			endforeach;  
		endif;  
	 ?>
</body>
</html>