<?php $themeBasePath = Yii::app()->theme->baseUrl . '/assests/'; ?>
<script>
$(function () {
  // item quantity updated +/-
  $(".quantity").click(function () {
     var itemId = $(this).attr('id');
     var quantity = $("#quantity_" + itemId).val();

     // price calculation
     var item_price = parseFloat($("#priceInd_" + itemId).val());
     var item_price_upd = item_price * quantity;
     //console.log(item_price);
     console.log(quantity);

     $.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function () {
           $(".preloader").show();
        },
        url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/updateQty/",
        data: {
           itemId: itemId,
           quantity: quantity
        },
        success: function (data) {
           $(".preloader").hide();

           // update price
           $("#price_" + itemId).html(item_price_upd);
           $("#total_subtotal").html(data.total_price);
           $("#total_discount").html(data.total_discount);
           var dataGrandTotal = (data.total_price - data.total_discount);
           $("#total_grandtotal").html(dataGrandTotal);

           // update shopping bag
           $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
           });
        },
        error: function () {
        }
     });
  });

  // remove from cart global
  $(".remove_from_cart_my_cart").click(function ()
  {
     // values & validations
     var itemId = $(this).attr('id');

     // price calculation
     var item_price = parseFloat($("#price_" + itemId).html());
     var total_subtotal = parseFloat($("#total_subtotal").html());
     var totalbal_subtotal = total_subtotal - item_price;

     var total_grandtotal = parseFloat($("#total_grandtotal").html());
     var totalbal_grandtotal = total_grandtotal - item_price;

     $.ajax({
        type: "POST",
        dataType: "json",
        beforeSend: function () {
           $(".preloader").show();
        },
        url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
        data: {
           itemId: itemId,
        },
        success: function (data) {
           $(".preloader").hide();
           $("#cart_item_holder_" + itemId).fadeOut(500, function () {
              $(this).remove();
           });

           // update price
           $("#total_subtotal").html(totalbal_subtotal);
           $("#total_grandtotal").html(totalbal_grandtotal);

           // update shopping bag
           $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
           });
        },
        error: function () {
        }
     });
  });

  // item moved to wishlist
  $(".move_to_wishlist").click(function () {
      <?php if (!empty(Yii::app()->session['custId'])) : ?>
        var itemId = $(this).attr('id');

        // price calculation
        var item_price = parseFloat($("#price_" + itemId).html());
        var total_subtotal = parseFloat($("#total_subtotal").html());
        var totalbal_subtotal = total_subtotal - item_price;

        var total_grandtotal = parseFloat($("#total_grandtotal").html());
        var totalbal_grandtotal = total_grandtotal - item_price;

        $.ajax({
           type: "POST",
           dataType: "json",
           beforeSend: function () {
              $(".preloader").show();
           },
           url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
           data: {
              itemId: itemId,
           },
           success: function (data) {
              $(".preloader").hide();
              if (data == 'exists')
                 alert("Already added to wishlist !");
              else
              {
                 $("#cart_item_holder_" + itemId).fadeOut(500, function () {
                    $(this).remove();
                 });

                 // update price
                 $("#total_subtotal").html(totalbal_subtotal);
                 $("#total_grandtotal").html(totalbal_grandtotal);

                 // update wishlist count
                 $("#wishListCount").html(data);
                 // update shopping bag
                 $(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function () {
                 });
              }
           },
           error: function () {
           }
        });
      <?php else : ?> alert("please signin first !");
      <?php endif; ?>
  });

  //increase amount
  $('.plus').on('click', function () {
     var itemId = $(this).attr('id');
     var $qty = $("#quantity_" + itemId);
     var currentVal = parseInt($qty.val());
     if (!isNaN(currentVal)) {
        $qty.val(currentVal + 1);
     }
  });

  $('.minus').on('click', function () {
     var itemId = $(this).attr('id');
     var $qty = $("#quantity_" + itemId);
     var currentVal = parseInt($qty.val());
     if (!isNaN(currentVal) && currentVal > 0) {
        $qty.val(currentVal - 1);
     }
  });

});
</script>
<section id="cart_items">
   <div class="container">
      <div class="breadcrumbs">
         <ol class="breadcrumb">
            <li><a href="javascript:void(0);">Home</a></li>
            <li class="active"><?php echo $this->metaTitle; ?></li>
         </ol>
      </div>
   </div>
</section>

<?php if ($total_qty > 0) : ?>
   <div class="container">
      <div class="row inner-bottom-sm">
         <div class="shopping-cart">
            <div class="col-md-12 col-sm-12 shopping-cart-table ">
               <div class="table-responsive">
                  <table class="table table-bordered">
                     <thead>
                        <tr>
                           <th class="cart-romove item">Sl</th>
                           <th class="cart-description item">Image</th>
                           <th class="cart-product-name item">Product Name</th>
                           <th class="cart-qty item">Quantity</th>
                           <th class="cart-sub-total item">Subtotal</th>
                           <th class="cart-romove item">Remove</th>
                        </tr>
                     </thead><!-- /thead -->

                     <tbody>
                        <?php
                        $k = 0;
                        foreach ($cart->getItems() as $order_code => $quantity) : $k++;
                           ?>
                           <tr id="cart_item_holder_<?php echo $order_code; ?>">
                              <td style="text-align: center;"><?php echo $k; ?></td>
                              <td class="cart-image" style="text-align: center;">
                                 <a class="entry-thumbnail" href="#">
                                    <?php if (!empty($cart->getItemImages($order_code))) : echo CHtml::link('<img class="lazy" alt="" src="' . $cart->getItemImages($order_code) . '" style="width:110px;height:110px;" />', array('eshop/proDetails', 'id' => $order_code), array('class' => 'thumb-holder', 'title' => $cart->getItemName($order_code))); ?>
                                    <?php endif; ?>
                                 </a>

                              </td>
                              <td class="cart-product-name-info" style="text-align: center;">
                                 <h4 class='cart-product-description'><?php echo CHtml::link($cart->getItemName($order_code), array('eshop/proDetails', 'id' => $order_code), array('title' => $cart->getItemName($order_code))); ?></h4>
                                 <div class="row">
                                    <div class="col-sm-12">
                                       <?php echo $cart->getItemBrand($order_code); ?>
                                    </div>
                                 </div><!-- /.row -->
                                 <div class="cart-product-info">
                                    <span class="product-color"><a id="<?php echo $order_code; ?>" class="move_to_wishlist" href="javascript:void(0);" title="move to wishlist"><i class="fa fa-heart"></i> move to wishlist</a></span>
                                 </div>
                              </td>
                              <td class="cart-product-quantity">
                                 <div class="cart_quantity_button quantity" id="<?php echo $order_code; ?>">
                                    <div class="le-quantity">
                                       <a class="cart_quantity_up minus" href="#reduce"><i class="fa fa-minus-square"></i> </a>
                                       <input id="quantity_<?php echo $order_code; ?>" class="cart_quantity_input" style="text-align:center;" type="text" name="quantity" readonly=''  value="<?php echo $quantity; ?>" autocomplete="off" size="2">
                                       <a class="cart_quantity_down plus" href="#add"> <i class="fa fa-plus-square"></i></a>
                                    </div>
                                 </div>
                              </td>
                              <td class="cart-product-grand-total">
                                 <div class="price">
                                    <?php if (Items::getItemWiseOfferPrice($order_code) > 0) : ?>
                                       <input type="hidden" id="priceInd_<?php echo $order_code; ?>" value="<?php echo Items::getItemWiseOfferPrice($order_code); ?>" />
                                       <?php echo Company::getCurrency(); ?> <span id="price_<?php echo $order_code; ?>"><?php echo Items::getItemWiseOfferPrice($order_code) * $quantity; ?></span>
                                    <?php else : ?>
                                       <input type="hidden" id="priceInd_<?php echo $order_code; ?>" value="<?php echo $cart->getItemPrice($order_code); ?>" />
                                       <?php echo Company::getCurrency(); ?> <span id="price_<?php echo $order_code; ?>"><?php echo $cart->getItemPrice($order_code) * $quantity; ?></span>
                                    <?php endif; ?>
                                 </div>
                              </td>
                              <td class="romove-item">
                                 <a id="<?php echo $order_code; ?>" class="icon cart_quantity_delete close-btn remove_from_cart_my_cart" href="javascript:void(0);"><i class="fa fa-trash-o"></i></a>
                              </td>
                           </tr>
                        <?php endforeach; ?>
                     </tbody><!-- /tbody -->

                  </table><!-- /table -->
               </div>
            </div><!-- /.shopping-cart-table -->
            <div class="col-md-4 col-sm-12 cart-shopping-total" style="float: right;">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th style='padding:19px'>
                  <div class="cart-sub-total">
                     Cart Sub Total<span class="inner-left-md"><span><?php echo Company::getCurrency(); ?>&nbsp;&nbsp;<span id="total_subtotal"><?php echo $total_price; ?></span></span></span>
                  </div>
                  <div class="cart-sub-total">
                     Discount<span class="inner-left-md"><?php echo Company::getCurrency(); ?>&nbsp;&nbsp;<span id="total_discount"><?php echo $total_discount; ?></span></span></span>
                  </div>
                  <div class="cart-grand-total">
                     Total<span class="inner-left-md"><?php echo Company::getCurrency(); ?>&nbsp;&nbsp;<span id="total_grandtotal"><?php echo ($total_price - $total_discount); ?></span></span></span>
                  </div>
                  </th>
                  </tr>
                  </thead><!-- /thead -->
                  <tbody>
                     <tr>
                        <td style="padding-right: 10px; padding-left: 10px;">
                           <a class="btn btn-upper btn-primary outer-left-xs" href="javascript: window.history.go(-1);" >Continue shopping</a>
                           <div class="cart-checkout-btn pull-right">
                              <?php echo CHtml::link('Check Out', array('checkoutProcess/checkout'), array('class' => 'btn btn-primary')) ?>
                           </div>
                        </td>
                     </tr>
                  </tbody><!-- /tbody -->

               </table><!-- /table -->
            </div>
         </div>
      </div>
   </div>
<?php else : ?>
   <div class="container">
      <div class="row inner-bottom-sm">
         <div class="shopping-cart">
            <div class="col-md-12 col-sm-12 shopping-cart-table ">
               <div class="table-responsive">
                  Your Cart Is Empty !
               </div>
            </div><!-- /.shopping-cart-table -->
         </div>
      </div>
   </div>
<?php endif; ?>
<?php $this->widget('EshopRecommanded', array('limit' => 6)); ?>