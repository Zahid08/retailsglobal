<style>
    ul.nav > li > h4 {
	border-bottom: 1px solid #ddd;
	color: #fe980f;
	margin-bottom: 10px;
	padding-bottom: 5px;
    }
    ul.nav > li > ul > li a {
        color: #777;
    }
    ul.nav > li > ul > li a:hover {
        color: #fe980f !important;
    }
    ul.nav > li > ul {
        margin-bottom: 20px;
    }
</style>
<div class="container" id="contact-page">
	<div class="bg">		
		<div class="row">  	
    		<div class="col-sm-12">
    			<div class="contact-info">    				
    				<ul class="nav">
    					<?php foreach(Department::getAllDepartmentData(Department::STATUS_ACTIVE) as $dkey=>$dept) :
                            if(Category::getAllCatCountByDept($dept->id)>0) : ?>
                            <li>
                                <h4><?php echo $dept->name; ?></h4>
                                <?php if(!empty(Category::getAllCatByDept($dept->id))) : ?>
                                    <ul class="clearfix">
                                        <?php  foreach(Category::getAllCatByDept($dept->id) as $ckey=>$cat) :  ?>
                                            <li class="col-sm-3">
                                                <?php echo CHtml::link($cat->name,array('eshop/proByCat','catId'=>$cat->id),array('title'=>$cat->name));?>
                                            </li>   
                                        <?php  endforeach;?>                                             
                                    </ul>
                                    <div style="clear:both"></div>   
                                <?php endif;?>	
                            </li> 	
    					<?php endif; endforeach;?>
    				</ul>     								
    			</div>
    		</div>    		    			
    	</div>  
	</div>	
</div>
