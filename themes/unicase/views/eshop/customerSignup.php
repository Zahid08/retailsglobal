<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<section>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';//echo $catModel->subdept->dept->name;?></a></li>
                <li class="active"><?php echo 'Signup'; ?></li>
            </ol>
        </div>	
    </div>
</section><!-- ========================================= BREADCRUMB : END ========================================= -->

<!-- END PAGE TITLE & BREADCRUMB-->


<div class="sign-in-page inner-bottom-sm">
	<div class="row">							
        <div class="col-md-6 col-sm-6 sign-in form">
            <h4 class="">Create New Account As Supplier</h4>
            <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>
         
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'supplier-form',
                    'enableAjaxValidation'=>true,
                    'htmlOptions'=>array(
                        'class'=>'register-form outer-top-xs',
                        'role'=>'form'
                    )
                )); 
                echo $form->hiddenField($modelSupplier,'suppId', array('value'=>'SN'.date("ymdhis")));?>
                <?php echo $form->errorSummary($modelSupplier); ?>
                
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'name'); ?>
                    <?php echo $form->textField($modelSupplier,'name',array('class'=>'form-control unicase-form-control text-input','size'=>50,'maxlength'=>50,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'name'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'email'); ?>
                    <?php echo $form->textField($modelSupplier,'email',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>150,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'email'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'address'); ?>
                   <?php echo $form->textArea($modelSupplier,'address',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>250,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'address'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'city'); ?>
                    <?php echo $form->textField($modelSupplier,'city',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>150,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'city'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'phone'); ?>
                    <?php echo $form->textField($modelSupplier,'phone',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>150,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'phone'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'username'); ?>
                    <?php echo $form->textField($modelSupplier,'username',array('class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'username'); ?>
                </div>
                <div class="form-group">
                    <?php  echo $form->labelEx($modelSupplier,'password'); ?>
                    <?php echo $form->passwordField($modelSupplier,'password',array('value'=>'','class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'password'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelSupplier,'conf_password'); ?>       
                    <?php echo $form->passwordField($modelSupplier,'conf_password',array('value'=>'','class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelSupplier,'conf_password'); ?>
                </div>  
                <?php echo CHtml::submitButton('Sign Up', array('class'=>'btn-upper btn btn-primary checkout-page-button')); ?>                
             <?php $this->endWidget(); ?>	
        </div>
        <!-- Sign-in -->

        <!-- create a new account -->
        <div class="col-md-6 col-sm-6 create-new-account form">
            <h4 class="checkout-subtitle">Create New Account as Customer</h4>           
            <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'customer-form',
                        'enableAjaxValidation'=>true,
                        'htmlOptions'=>array(
                            'class'=>'register-form outer-top-xs',   
                            'role'=>'form'
                        )
                    )); ?>
                <?php echo $form->errorSummary($modelCustomer); ?>
                <?php echo $form->hiddenField($modelCustomer,'custId', array('id'=>'custId','class'=>'form-control unicase-form-control text-input','readonly'=>'true', 'value'=>'CN'.date("Ymdhis"))); ?>
               
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'title'); ?>
                    <?php echo $form->dropDownList($modelCustomer,'title', UsefulFunction::getCustomerTitle(), array('class'=>'form-control unicase-form-control text-input','prompt'=>'Select Title')); ?>
                    <?php echo $form->error($modelCustomer,'title'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'name'); ?>
                    <?php echo $form->textField($modelCustomer,'name',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>255,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'name'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'email'); ?>
                    <?php echo $form->textField($modelCustomer,'email',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>155,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'email'); ?>
                </div>
                <div class="form-group">
                   <?php echo $form->labelEx($modelCustomer,'addressline'); ?>
                    <?php echo $form->textArea($modelCustomer,'addressline',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>255,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'addressline'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'city'); ?>
                    <?php echo $form->textField($modelCustomer,'city',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>150,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'city'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'phone'); ?>
                    <?php echo $form->textField($modelCustomer,'phone',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>155,'placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'phone'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'gender'); ?>
                    <?php echo $form->dropDownList($modelCustomer,'gender', UsefulFunction::getCustomerGender(), array('class'=>'form-control unicase-form-control text-input','prompt'=>'Select Gender')); ?>
                    <?php echo $form->error($modelCustomer,'gender'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'username'); ?>
                    <?php echo $form->textField($modelCustomer,'username',array('class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'username'); ?>
                </div>

                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'password'); ?>
                    <?php echo $form->passwordField($modelCustomer,'password',array('value'=>'','class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'password'); ?>
                </div>
                <div class="form-group">
                    <?php echo $form->labelEx($modelCustomer,'conf_password'); ?>       
                    <?php echo $form->passwordField($modelCustomer,'conf_password',array('value'=>'','class'=>'form-control unicase-form-control text-input','placeholder'=>'')); ?>
                    <?php echo $form->error($modelCustomer,'conf_password'); ?>
                </div>
                <?php echo CHtml::submitButton('Sign Up', array('class'=>'btn-upper btn btn-primary checkout-page-button')); ?>
            <?php $this->endWidget(); ?>         
        </div>	
    </div><!-- /.row -->
</div><!-- /.sigin-in-->
        
        


<style type="text/css">
  .btn.btn-default.custom-btn{
      background: none repeat scroll 0 0 #fe980f;
      border: medium none;
      border-radius: 0;
      color: #ffffff;
      display: block;
      font-family: "Roboto",sans-serif;
      padding: 6px 25px;
    }
</style>
