<div class="col-sm-6 col-md-4 wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
    <div class="products">
        <div class="product">		
            <div class="product-image">
                <div class="image">
                    <?php if (!empty($data->itemImages)) : ?>
                    <?php echo CHtml::link('<img alt="" src="' . $data->itemImages[0]->image . '" data-echo="' . $data->itemImages[0]->image . '" style="height:170px;width: 100%;" />', array('eshop/proDetails', 'id' => $data->id), array('class' => 'thumb-holder', 'title' => $data->itemName)); ?>
                  <?php endif; ?>
                </div><!-- /.image -->			
   
                <?php
                   // mark up new + sales + best sales
                   echo (Items::getIsNewItem($data->id)) ? '<div class="tag new" style="position: absolute;top:-7px;"><span>new!</span></div>' : '';
                   echo (Items::getIsSaleItem($data->id)) ? '<div class="tag sale" style="position: absolute;top:20px;"><span>sale</span></div>' : '';
                   //echo (Items::getIsBestSaleItem($data->id)) ? '<div class="tag hot" style="position: absolute;top:40px;"><span>bestseller</span></div>' : '';
                 ?>
            </div><!-- /.product-image -->

            <div class="product-info text-left">
                <h3 class="name"><?php echo $data->itemName; ?></h3>
                <div class="description"></div>

                <div class="product-price">	
                    <?php if (Items::getItemWiseOfferPrice($data->id) > 0) : ?>
                     <div class="price-before-discount" style="text-decoration: line-through;">
                        <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($data->id); ?>
                     </div>
                     <div class="price">
                        <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemWiseOfferPrice($data->id); ?>
                     </div>
                  <?php else : ?>
                     <div class="price">
                        <?php echo Company::getCurrency() . '&nbsp;' . Items::getItemPriceWithVat($data->id); ?>
                     </div>
                  <?php endif; ?>
                </div><!-- /.product-price -->
            </div><!-- /.product-info -->
            
            <div class="cart clearfix animate-effect">
                <div class="action">
                    <ul class="list-unstyled">
                        <li class="add-cart-button btn-group">
                            <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                <i class="fa fa-shopping-cart"></i>													
                            </button>
                            <?php echo CHtml::link('Add to cart', array('eshop/proDetails', 'id'=>$data->id), array('class'=>'btn btn-primary'));?>
                        </li>
                        <li class="lnk wishlist">

                            <a class="add-to-cart btn-add-to-wishlist" id="<?php echo $data->id; ?>" data-toggle="tooltip" data-placement="right" href="javascript:void(0);">
                             <i class="icon fa fa-heart"></i>
                            </a>
                        </li>
                        <li class="lnk">
                            <?php echo CHtml::link('<i class="fa fa-retweet"></i>', array('eshop/proReview', 'id' => $data->id), array('class' => 'add-to-cart btn-add-to-compare')); ?>
                        </li>
                    </ul>
                </div><!-- /.action -->
            </div><!-- /.cart -->
        </div><!-- /.product -->
    </div><!-- /.products -->
</div>
<?php if(($index+1)%3==0) : ?>
   <div class="clearfix"></div>
<?php endif; ?>