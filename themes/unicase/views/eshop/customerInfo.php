<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<div class="body-content">
   <div class="container">
      <div class="sign-in-page inner-bottom-sm">
         <div class="row">
            <div class="col-md-8 col-sm-8 sign-in form">
               <?php if (!empty($msg)) echo '<div class="row highLight">' . $msg . '</div>'; ?>
               <h4 style="margin-top: 0;">Update <?php echo $modelCustomer->name; ?> Inforamtion</h4>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'customer-form',
                   'enableAjaxValidation' => true,
                   'htmlOptions' => array(
                       'class' => 'login-form cf-style-1',
                   )
               ));
               ?>
               <?php echo $form->errorSummary($modelCustomer); ?>
               <?php echo $form->hiddenField($modelCustomer, 'custType', array('id' => 'custType', 'class' => 'le-input', 'value' => Customer::STATUS_GENERAL)); ?>
               <?php echo $form->hiddenField($modelCustomer, 'status', array('id' => 'status', 'value' => Customer::STATUS_INACTIVE)); ?>
               <?php echo $form->hiddenField($modelCustomer, 'custId', array('id' => 'custId', 'class' => 'le-input', 'readonly' => 'true', 'value' => 'CN' . date("Ymdhis"))); ?>

               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'title'); ?>
                  <?php echo $form->dropDownList($modelCustomer, 'title', UsefulFunction::getCustomerTitle(), array('class' => 'form-control unicase-form-control text-input', 'prompt' => 'Select Title')); ?>
                  <?php echo $form->error($modelCustomer, 'title'); ?>
               </div>

               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'name'); ?>
                  <?php echo $form->textField($modelCustomer, 'name', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 255, 'placeholder' => 'Name')); ?>
                  <?php echo $form->error($modelCustomer, 'name'); ?>
               </div>

               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'email'); ?>
                  <?php echo $form->textField($modelCustomer, 'email', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 155, 'placeholder' => 'Email')); ?>
                  <?php echo $form->error($modelCustomer, 'email'); ?>
               </div>

               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'addressline'); ?>
                  <?php echo $form->textArea($modelCustomer, 'addressline', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 255, 'placeholder' => 'Addressline')); ?>
                  <?php echo $form->error($modelCustomer, 'addressline'); ?>
               </div>
               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'city'); ?>
                  <?php echo $form->textField($modelCustomer, 'city', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 150, 'placeholder' => 'City')); ?>
                  <?php echo $form->error($modelCustomer, 'city'); ?>
               </div>
               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'phone'); ?>
                  <?php echo $form->textField($modelCustomer, 'phone', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 155, 'placeholder' => 'Phone')); ?>
                  <?php echo $form->error($modelCustomer, 'phone'); ?>
               </div>
               <div class="form-group">
                  <?php echo $form->labelEx($modelCustomer, 'gender'); ?>
                  <?php echo $form->dropDownList($modelCustomer, 'gender', UsefulFunction::getCustomerGender(), array('class' => 'form-control unicase-form-control text-input', 'prompt' => 'Select Gender')); ?>
                  <?php echo $form->error($modelCustomer, 'gender'); ?>
               </div>
               <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit</button>
               <?php $this->endWidget(); ?>
            </div>
            <!-- Sign-in -->

         </div><!-- /.row -->
      </div><!-- /.sigin-in-->
   </div>
</div><!-- /.body-content -->







<style type="text/css">
   .highLight
   {
      background: none repeat scroll 0 0 #fe980f;
      color: white;
      padding: 10px;
   }
   .btn.btn-default
   {
      background: none repeat scroll 0 0 #fe980f;
      color: white;
      padding: 5px;
   }
</style>
