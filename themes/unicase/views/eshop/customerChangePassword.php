<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>

<div class="container">
    <div class="sign-in-page inner-bottom-sm">
        <div class="row">
            <div class="col-md-6 col-sm-6 sign-in form">
             <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?> 
                <h4 class="">Change Password</h4>                   
                <?php echo CHtml::beginForm('','post',array('id'=>'customer-change-password','class'=>'cf-style-1'));?>
                <?php echo CHtml::errorSummary($form); ?>  
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($form,'verifyoldpass'); ?>
                        <?php echo CHtml::activePasswordField($form,'verifyoldpass' ,array('value'=>'','class'=>'form-control unicase-form-control text-input')); ?>
                     </div>
                     <div class="form-group">
                        <?php echo CHtml::activeLabelEx($form,'password'); ?>
                        <?php echo CHtml::activePasswordField($form,'password' ,array('value'=>'','class'=>'form-control unicase-form-control text-input')); ?>
                     </div>
                     <div class="form-group">
                        <?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
                        <?php echo CHtml::activePasswordField($form,'verifyPassword' ,array('value'=>'','class'=>'form-control unicase-form-control text-input')); ?>
                     </div>
                     <?php echo CHtml::submitButton('Update', array('class'=>'btn-upper btn btn-primary checkout-page-button')); ?>                 
                <?php echo CHtml::endForm(); ?>  	
            </div>            
            
        </div><!-- /.row -->
    </div><!-- /.sigin-in-->	
</div>





