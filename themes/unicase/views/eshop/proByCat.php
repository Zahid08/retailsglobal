<?php
$minPrice = 0;
$maxPrice = 500;
if (!empty($catModel)) {
   $priceRange = Category::priceDynamic($catModel->id);
   if (!empty($priceRange)) {
      if (!empty($priceRange['minPrice']) && !empty($priceRange['maxPrice'])) {
         $minPrice = $priceRange['minPrice'];
         $maxPrice = $priceRange['maxPrice'];
      }
   }
} ?>
<script>
   $(function () {
      // brand filter
        $('.le-checkbox').click(function() 
        {
            var isCheckedBrand = '';
            $(".le-checkbox:checked").each(function (i) {
                isCheckedBrand+=$(this).val()+',';
            });
            isCheckedBrand = isCheckedBrand.slice(0,-1);
            var startPrice = parseFloat($("#startPrice").html()),
                endPrice = parseFloat($("#endPrice").html());

            // ajax call
            $.ajax({
                type: "POST",
                beforeSend : function() {
                    $(".preloader").show();
                },
                url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
                data: {
                    catId : <?php echo $catModel->id;?>,
                    brand : isCheckedBrand,
                    startPrice : startPrice,
                    endPrice : endPrice
                },					
                success: function(data) {
                    $(".preloader").hide();
                    $("#catGridFilterByAjax").html(data);
                },
                error: function() {}
            });   
        });

        // price filtering
        $('#priceFilter').slider({
            range:true,
            min: <?php echo $minPrice;?>,
            max: <?php echo $maxPrice;?>,
            step: 100,
            value: [<?php echo $minPrice;?>, <?php echo $maxPrice;?>],
            handle: "square",
        });

        $('#priceFilter').slider().on('slide', function(event) {
            var isCheckedBrand = '';
            $(".le-checkbox:checked").each(function (i) {
                isCheckedBrand+=$(this).val()+',';
            });
            isCheckedBrand = isCheckedBrand.slice(0,-1);
            var startPrice = event.value[0],
                endPrice = event.value[1];
            
            $("#startPrice").html(startPrice);
            $("#endPrice").html(endPrice);

            // ajax call
            $.ajax({
                type: "POST",
                beforeSend : function() {
                    $(".preloader").show();
                },
                url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
                data: {
                    catId : <?php echo $catModel->id;?>,
                    brand : isCheckedBrand,
                    startPrice : startPrice,
                    endPrice : endPrice
                },					
                success: function(data) {
                    $(".preloader").hide();
                    $("#catGridFilterByAjax").html(data);
                },
                error: function() {}
            }); 
        });
   });
</script>

<!-- ========================================= BREADCRUMB ========================================= -->
<section id="cart_items">
   <div class="container">
      <div class="breadcrumbs">
         <?php if (!empty($catModel)) : ?>
            <ol class="breadcrumb">
               <li><a href="javascript:void(0);"><?php echo $catModel->subdept->dept->name; ?></a></li>
               <li><a href="javascript:void(0);"><?php echo $catModel->subdept->name; ?></a></li>
               <li class="active"><?php echo $catModel->name; ?></li>
            </ol>
         <?php endif; ?>
      </div>
   </div>
</section>

<div class="container">
   <div class="col-sm-3">
      <div class="widget left-sidebar">
         <div class="body bordered">
            <?php if (!empty($brandModel)) : ?>
               <div class="category-filter">
                  <h3 class="section-title">Brands</h2>
                  <div class="brands-name">
                     <ul class="list-unstyled">
                        <?php foreach ($brandModel as $keyBrand => $dataBrand) : ?>
                           <li>                              
                              <input class="le-checkbox" type="checkbox" value="<?php echo $dataBrand->brandId;?>" />
                              <label for="<?php echo $dataBrand->brandId; ?>"><?php echo $dataBrand->brand->name; ?></label>
                              <span class="badge pull-right"><?php echo Items::getItemsCountByBrand($dataBrand->brandId,$catModel->id); ?></span>
                           </li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
               </div><!-- /.category-filter -->
            <?php endif; ?>
            <div class="sidebar-widget-body m-t-20">
               <div class="price-filter">
					<h3 class="section-title">Price</h3>
					<div class="price-range-holder">
						<div id="priceFilter"><input type="text" class="price-slider" value="" /></div>
						<span class="min-max">
							Price: <?php echo Company::getCurrency();?> <span id="startPrice"><?php echo $minPrice;?></span> - <span id="endPrice"><?php echo $maxPrice;?></span>
						</span>
					</div>
				</div><!-- /.price-filter -->
            </div><!-- /.sidebar-widget-body -->
         </div><!-- /.body -->
      </div><!-- /.widget -->
      <div class="widget left-sidebar">
     <?php  $this->widget('EshopSpecialOffersLeft'); ?>
     
      </div>
      <div class="widget left-sidebar">
     <?php $this->widget('EshopFeturedProductsLeft');  ?>
      </div>
    
   </div>
  
   <!-- ========================================= CONTENT ========================================= -->
   <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
      <section id="gaming">
         <div class="grid-list-products">
            <h3 class="title text-center section-title"><?php if (!empty($catModel)) echo $catModel->name; ?></h3>
            <div class="product-grid-holder medium clearfix" id="catGridFilterByAjax">
               <div class="col-xs-12 col-md-12 no-margin">
                  <div class="row no-margin">
                     <?php
                     $this->widget('zii.widgets.CListView', array(
                         'dataProvider' => $dataProvider,
                         'ajaxUpdate' => true,
                         'enableSorting' => true,
                         'enablePagination' => true,
                         'emptyText' => 'No records found.',
                         //'summaryText' => "{start} - {end} of {count}",
                         'template' => '{items} {pager}', // {summary} {sorter}
                         //'sorterHeader' => 'Sort by:',
                         //'sortableAttributes' => array('title', 'price'),
                         'itemView' => '_proByCatGrid',
                         //'htmlOptions'=>array('class'=>'clearfix'),
                         'pager' => array('cssFile' => true, 'pageSize' => 1,
                             'header' => false,
                             'class' => 'CLinkPager',
                             'firstPageLabel' => 'First',
                             'prevPageLabel' => '<',
                             'nextPageLabel' => '>',
                             'lastPageLabel' => 'Last',
                             'htmlOptions' => array('class' => 'pagination')
                         ),
                     ));
                     ?>
                  </div>
               </div><!-- /.col-xs-12 col-md-12 -->
            </div><!-- /.product-grid-holder -->
         </div><!-- /.grid-list-products -->
      </section><!-- /#gaming -->
   </div><!-- /.col -->
</div><!-- /.container -->
