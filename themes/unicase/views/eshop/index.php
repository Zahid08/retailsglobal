<div class="row">
<div class="col-xs-12 col-sm-12 col-md-3 sidebar">
   <!-- ================================== TOP NAVIGATION ================================== -->
   <?php
   $this->widget('EshopLeftMenu');
   //$this->widget('eshopSpecialOffersLeft');
   //$this->widget('EshopAdvertisement', array('position' => Advertisement::POSITION_LEFT, 'limit' => 2));
   ?>

</div><!-- /.sidemenu-holder -->
<div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
   <!--Home Page Slider -->
   <?php if (!empty($sliderModel)) :  //POSITION_TOP  ?>
      <div id="hero">
         <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
            <?php foreach ($sliderModel as $keySlider => $dataSlider) : ?>
               <div class="item" style="background-image: url(<?php echo (!empty($dataSlider->image)) ? $dataSlider->image : '' ?>)">
                  <div class="container-fluid">
                     <div class="caption vertical-center text-left">
                        <div class="big-text fadeInDown-1">
                           <div class="price-current"><?php echo $dataSlider->title; ?></div>
                           <div class="excerpt fadeInDown-2">
                              <?php echo $dataSlider->shortDesc; ?>
                           </div>
                        </div>
                        <div class="small fadeInDown-2">
                           <?php if (Items::getItemWiseOfferPrice($dataSlider->id) > 0) echo 'terms and conditions apply'; ?>
                        </div>
                        <div class="button-holder fadeInDown-3">
                           <a href="<?php echo $dataSlider->url; ?>" class="btn btn-primary pull-right" title="<?php echo $dataSlider->title; ?>">Shop Now</a>
                        </div>
                     </div><!-- /.caption -->
                  </div><!-- /.container-fluid -->
               </div><!-- /.item -->
            <?php endforeach; ?>
         </div><!-- /.owl-carousel -->
      </div>
   <?php endif; ?>
    
   <?php $this->widget('EshopAdvertisement', array('position'=>'top','limit'=>'3'));?>
   <!-- ============================================== INFO BOXES : END ============================================== -->
 
   <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->
</div>
</div>

 <div class="row">
 <?php
    $this->widget('EshopHomePageProductsTab');
    $this->widget('EshopBestSellers');
    $this->widget('EshopRecentlyViewed',array('limit'=>15));
?>
</div>