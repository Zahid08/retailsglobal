<div class="container">
    <!-- ========================================= SIDEBAR ========================================= -->
    <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">
        <!-- ========================================= PRODUCT FILTER ========================================= -->
		 <?php
         	$this->widget('EshopSpecialOffersLeft');
         	$this->widget('EshopFeturedProductsLeft');
         ?>
	</div>
	<!-- ========================================= SIDEBAR : END ========================================= -->

	<!-- ========================================= CONTENT ========================================= -->
	<div class="col-xs-12 col-sm-9 no-margin wide sidebar">
		<section id="gaming">
			<div class="grid-list-products">
				<h2 class="section-title"><?php if(!empty($searchItems)) echo 'Search result for : '.$searchItems;?></h2>
				<div class="product-grid-holder medium clearfix" id="catGridFilterByAjax">
            		<div class="col-xs-12 col-md-12 no-margin">
						<div class="row no-margin">
							<?php $this->widget('zii.widgets.CListView', array(
								'dataProvider'=>$dataProvider,					
								'ajaxUpdate'=>true,
								'enableSorting' => true,
								'enablePagination'=>true,
								'emptyText' => 'No records found.',
								//'summaryText' => "{start} - {end} of {count}",
								'template' => '{items} {pager}', // {summary} {sorter} 
								//'sorterHeader' => 'Sort by:',
								//'sortableAttributes' => array('title', 'price'),
								'itemView'=>'_proByCatGrid',
								//'htmlOptions'=>array('class'=>'clearfix'),
								'pager'=> array('cssFile'=>true, 'pageSize' => 1,
									'header'=>false,
									'class'          => 'CLinkPager',
									'firstPageLabel' => 'First',
									'prevPageLabel'  => '<',
									'nextPageLabel'  => '>',
									'lastPageLabel'  => 'Last',
									'htmlOptions'=>array('class'=>'pagination')
								  ),
							 )); ?>
						</div>
					</div><!-- /.col-xs-12 col-md-12 -->
				</div><!-- /.product-grid-holder -->
			</div><!-- /.grid-list-products -->
		</section><!-- /#gaming -->
		
		<?php //$this->widget('EshopRecommanded');?> 
	</div><!-- /.col -->
</div><!-- /.container -->