<style>
   .ginfo_left{
      width: 300px;
      float: left;
   }
   .ginfo_right{
      width: 300px;
      float: left;
   }
   .generalInfo tr {
      height: 30px;
      line-height: 29px;
   }
   .generalInfo{
      margin-top: 25px;
   }
   .infoLeft{
      width: 80%;
      float: left;
   }
   .infoRight{
      width: 20%;
      float: left;
   }
</style>
<div class="checkout-box inner-bottom-sm">
   <div class="row">
      <div class="col-md-8">
         <div class="checkout-progress-sidebar ">
            <div class="panel-group">
               <?php if (!empty($model)): ?>
                  <div class="panel-default">
                     <div class="panel-heading">
                        <h4  class="unicase-checkout-title">General Information <?php echo $model->title . '.' . $model->name; ?></h4>
                        <h4 style="right: 26px; position: absolute;top: 10px;"><?php echo CHtml::link('Update', array('eshop/CustomerInfo', 'id' => $model->id), array('class' => '')); ?></h4>
                     </div>
                     <div class="">
                        <table class="">
                           <tbody>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">User Name</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $modelUser->username; ?> </td>
                              </tr>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">Email</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $model->email; ?> </td>
                              </tr>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">Phone</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $model->phone; ?> </td>
                              </tr>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">Gender</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $model->gender; ?> </td>
                              </tr>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">Address</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $model->addressline; ?> </td>
                              </tr>
                              <tr>
                                 <td style='padding:11px' class="col-md-1">City</td>
                                 <td style='padding:11px' class="col-md-4"><?php echo $model->city; ?> </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </div><!-- /.row -->
</div><!-- /.checkout-box -->


