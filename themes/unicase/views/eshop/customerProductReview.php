<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<section id="cart_items">
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">         
              <li><a href="javascript:void(0);">Home</a></li>            
              <li class="active"><?php echo $this->metaTitle;?></li>                
            </ol>
        </div>  
    </div>
</section>

<main id="authentication" class="inner-bottom-md product-review">
    <div class="container">
        <div class="row">
            <div class="col-md-6 form">
                <section class="section sign-in inner-right-xs" style="margin: 0px;">
                    <h2 class="bordered">Customer Review</h2>
                    <div class="tab-pane fade active in" id="reviews" style="padding-left: 0px;" >
                        <div class="col-sm-12">
                        <?php if(!empty($modelItemReview)):
                                $i=1; 
                                foreach($modelItemReview as $reviewProductData):
                            ?>  
                            <div class="review">
                                <div class="review-title"><span class="summary">Best Product For me :) </span><span class="date"><i class="fa fa-calendar"></i><span><?php echo date('H:i:s', strtotime($reviewProductData->crAt));   ?></span></span></div>
                                <div class="text"><?php echo 'Comments :'.$reviewProductData->comment; ?></div>
                                <div class="text"> 
                                <?php
                                $rate=$reviewProductData['rating'];
                                    $this->widget('CStarRating',array(
                                        'name'=>'rating'.$i,
                                        'minRating'=>1,
                                        'maxRating'=>5,
                                        'starCount'=>5,
                                        'value'=>$rate,
                                        'readOnly'=>true,
                                    ));
                                $i++;
                                ?>
                                </div>
                                <div class="author m-t-15"><i class="fa fa-pencil-square-o"></i> <span class="name"><?php echo 'Name :'.$reviewProductData->name; ?></span></div>									
                            </div>  
                            <?php
                                endforeach;
                            endif; ?>                       
                        </div>
                    </div>                   
                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6 col-sm-6 sign-in form">
             <h2 class="bordered">Add Review</h2>
                    <?php if(!empty($msg)) :?>
                        <div class="notification note-success">
                            <p><?php echo $msg;?></p>
                        </div>
                    <?php endif; ?>                        
                 <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'item-Review-Item',
                            'enableAjaxValidation'=>true,
                        'htmlOptions'=> array('class'=>'clearfix')

                        )); ?>
                    <div class="form-group">
                        <?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$id));?>
                        <?php echo $form->textField($modelReview,'name',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>255,'placeholder'=>'Name')); ?>
                        <?php echo $form->error($modelReview,'name'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->textField($modelReview,'email',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>255,'placeholder'=>'Email')); ?>
                        <?php echo $form->error($modelReview,'email'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $form->labelEx($modelReview,'rating'); ?>
                            <?php
                            $this->widget('CStarRating',array(
                                'model'=>$modelReview,
                                'name'=>$modelReview['rating'],
                                'attribute'=>'rating',
                                'minRating'=>1, //minimal value
                                'maxRating'=>5,//max value
                                'starCount'=>5, //number of stars
                                'titles'=>array(
                                    '1'=>'Normal',
                                    '2'=>'Average',
                                    '3'=>'OK',
                                    '4'=>'Good',
                                    '5'=>'Excellent'
                                ),

                            ));
                            ?>
                            <?php echo $form->error($modelReview,'rating'); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group" style="margin-top: 15px;" >
                         <?php echo $form->textArea($modelReview,'comment',array('class'=>'form-control unicase-form-control text-input','size'=>60,'maxlength'=>255,'placeholder'=>'Comment')); ?>
                         <?php echo $form->error($modelReview,'comment'); ?>
                    </div>
                     <?php echo CHtml::submitButton('SUBMIT', array('class'=>'btn-upper btn btn-primary checkout-page-button')); ?> 
                   
                  <?php $this->endWidget(); ?>  	
            </div> 
            
            
            
            
            
            

        </div><!-- /.row -->
    </div><!-- /.container -->
</main>

<style>
    #ItemReview_rating_em_{
        margin-top: 30px;
    }
</style>