<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" language="javascript">
   $(function ()
   {
      //-------enter press ------------//
      $(document).keypress(function (e) {
         if (e.which == 13)
         {
            $('#login-form').get(0).submit();
         }
      });

   });
</script>

<section>
   <div class="container">
      <div class="breadcrumbs">
         <ol class="breadcrumb">
            <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home'; //echo $catModel->subdept->dept->name;  ?></a></li>
            <li class="active"><?php echo 'Signin'; ?></li>
         </ol>
      </div>
   </div>
</section>

<div class="body-content outer-top-bd">
   <div class="container">
      <div class="sign-in-page inner-bottom-sm">
         <div class="row">
            <div class="col-md-6 col-sm-6 sign-in form">
               <h4 class="">Forget Password</h4>
               <?php if (!empty($msg)) echo '<div class="row">' . $msg . '</div>'; ?>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'forgot-login-form',
                   'enableAjaxValidation' => true,
                   'action' => Yii::app()->createUrl('/eshop/resetPassword'),
                   'htmlOptions' => array(
                       'class' => 'register-form outer-top-xs',
                       'role' => 'form'
                   )
               ));
               ?>

               <div class="form-group">
                  <label class="info-title">Username</label>
                  <?php echo CHtml::textField('username', '', array('class' => 'form-control unicase-form-control text-input', 'size' => 50, 'maxlength' => 50, 'placeholder' => 'Username')); ?>
               </div>

               <div class="form-group">
                  <label class="info-title" for="exampleInputPassword1">Or</label>
               </div>
               <div class="form-group">
                  <label class="info-title">Email</label>
                  <?php echo CHtml::textField('email', '', array('class' => 'form-control unicase-form-control text-input', 'placeholder' => 'Email Address')); ?>
               </div>
               <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit</button>
               <?php $this->endWidget(); ?>
            </div>
            <!-- Sign-in -->

            <!-- create a new account -->
            <div class="col-md-6 col-sm-6 create-new-account form">
               <h4 class="checkout-subtitle">Signin as Customer</h4>
               <?php
               $form = $this->beginWidget('CActiveForm', array(
                   'id' => 'login-form',
                   'enableAjaxValidation' => true,
                   'htmlOptions' => array(
                       'class' => 'register-form outer-top-xs',
                       'role' => 'form'
                   )
               ));
               echo $form->hiddenField($model, 'branchId', array('value' => Branch::getDefaultBranch(Branch::STATUS_ACTIVE)));
               ?>
               <?php echo $form->errorSummary($model); ?>

               <div class="form-group">
                  <?php echo $form->labelEx($model, 'username'); ?>
                  <?php echo $form->textField($model, 'username', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 255)); ?>
                  <?php echo $form->error($model, 'username'); ?>
               </div>
               <div class="form-group">
                  <?php echo $form->labelEx($model, 'password'); ?>
                  <?php echo $form->passwordField($model, 'password', array('class' => 'form-control unicase-form-control text-input', 'size' => 60, 'maxlength' => 255)); ?>
                  <?php echo $form->error($model, 'password'); ?>
               </div>
               <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Sign In</button>
               <?php $this->endWidget(); ?>
            </div>
         </div><!-- /.row -->
      </div><!-- /.sigin-in-->
   </div>
</div><!-- /.body-content -->


