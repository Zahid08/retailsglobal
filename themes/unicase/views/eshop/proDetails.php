<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/etalage.css' type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl ?>/js/jquery.etalage.min.js"></script>
<script>
 $(function() {	
    // image slider zoom 
	$('#etalage').etalage({
		thumb_image_width: 250,
		thumb_image_height: 250,
		source_image_width: 900,
		source_image_height: 1200,
		show_hint: true,
		click_callback: function(image_anchor, instance_id){
			return false;
		}
	    //change_callback(){ alert('it moved'); } 
	});
	// add to cart
	$("#addto-cart").click(function() 
	{
		// values & validations       
		var isError = 0,
			isAttributes = $("#isAttributes").val(),
			quantity = $('#quantity').val();

		if(isAttributes==1) { // has attributes
			var itemId =$('input[name="itemId"]:checked').val();
			if(typeof(itemId)=="undefined" || itemId=="") {
				$("#attError").html("Please select at least one Attributes");
				isError = 1;
			}
			else
			{
				$("#attError").html("");
				isError = 0;	
			}
		}
		else var itemId = <?php echo $model->id;?>;
		// cart processing
		if(isError==0)
		{
            $.ajax({
				type: "POST",
				dataType: "json",				
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addToCart/",
				data: {
					itemId: itemId, 
					quantity : quantity,
				},		
				beforeSend : function() {
					$(".preloader").show();									
				},			
				success: function(data) {
				
					$(".preloader").hide();
					
					// update shopping bag 
					$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
						$(this).removeClass("basket").addClass("basket open");
						$(this).slideDown(200, function() {
							$('html, body').delay('200').animate({
							scrollTop: $(this).offset().top - 111
							}, 200);
						});
					}); 
				},
				error: function() {}
			});	
		}
	});
     
    // item att wise price change
	$('.radio_item_attributes').click(function()
	{
        var itemId = $(this).attr('id');
        var urlajax = "<?php  echo Yii::app()->request->baseUrl; ?>/index.php/eshop/changeItemPriceStrByAttributes/";
        $.ajax({
            type: "POST",
            url: urlajax,
            beforeSend: function() 
            {
                $(".preloader").show();
            },
            data: 
            {
                itemId : itemId,
            },
            success: function(data) 
            {
                $(".preloader").hide();
                $('.prices').html(data); 
            },
            error: function() {}
        });
	});
});
</script>
<section id="cart_items">
	<div class="container">
		<div class="breadcrumbs">
			<ol class="breadcrumb">			
			  <li><a href="javascript:void(0);"><?php echo $model->cat->subdept->dept->name;?></a></li>
			  <li><a href="javascript:void(0);"><?php echo $model->cat->subdept->name;?></a></li>
			  <li class="active"><?php echo $model->cat->name;?></li>				 
			</ol>
		</div>	
	</div>
</section>
<div class="body-content outer-top-xs">
	<div class='container'>
		<div class='row single-product outer-bottom-sm '>		
			<div class='col-md-12'>
				<div class="row wow fadeInUp">
					<div class="col-xs-12 col-sm-4 col-md-4 gallery-holder">
                        <div class="product-item-holder size-big single-product-gallery small-gallery">
                            <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">
                                    <!-- image zoomer starts -->
                                    <ul id="etalage" class="etalage">
                                        <?php 
                                          if(!empty($model->itemImages)) : // main image ?>
                                          <li class="etalage_thumb thumb_<?php echo $model->itemImages[0]->id;?> etalage_thumb_active" style="display: list-item; background-image: none; opacity: 1;">
                                                <img title="<?php echo $model->itemName;?>" src="<?php echo $model->itemImages[0]->image;?>" class="etalage_source_image" style="display: inline; opacity: 1;" />
                                          </li>

                                        <?php foreach($model->itemImages as $image) : // thumbs ?>                                
                                            <li class="etalage_thumb thumb_<?php echo $image->id;?>" style="background-image: none; display: none; opacity: 0;">
                                                <img title="<?php echo $model->itemName;?>" src="<?php echo $image->image;?>" class="etalage_source_image">
                                            </li>
                                        <?php endforeach;
                                        endif; ?>
                                    </ul>
                                    <!-- image zoomer ends -->   
                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->   
                        </div><!-- /.single-product-gallery -->
                    </div><!-- /.gallery-holder -->        			
					<div class='col-sm-8 col-md-8 product-info-block'>
						<div class="product-info">
							<h1 class="name"><a href="javascript:void(0);"><?php echo $model->itemName;?></a> &nbsp;
                            <a class="btn btn-primary btn-add-to-wishlist" id="<?php echo $model->id; ?>" data-toggle="tooltip" data-placement="right" title="Wishlist" href="javascript:void(0);">
                                <i class="fa fa-heart"></i>
                            </a>
                            </h1>
							<div class="description-container m-t-20">
								<p><?php echo substr($model->specification,0,200).'..';?></p>
							</div><!-- /.description-container -->
							<div class="price-container info-container m-t-20">
								<div class="row">
									<div class="col-sm-6">
										<div class="price-box">											
                                            <?php if(Items::getItemWiseOfferPrice($model->id)>0) :?>
                                               <span class="price"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($model->id);?></span>
                                               <span class="price-strike">
                                                    <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?>
                                              </span>
                                            <?php else : ?>
                                                <span class="price"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?></span>
                                            <?php endif;?>
                                        </div>
									</div>                                   
                                </div><!-- /.row -->
							</div><!-- /.price-container -->
                            <div class="row">
                                <div class="col-sm-12">
										<div class="dynamic-attr">
                                            <span id="attError" style="color:#F00;"></span>
                                            <!-- dynamic attributes starts here -->
                                            <?php if(!empty(ItemAttributes::getAttributesByAllItemsGroup($model->itemName))) : 
                                                foreach(ItemAttributes::getAttributesByAllItemsGroup($model->itemName) as $keyDefault=>$dataDefault) : ?>
                                                <div class="attr-row">
                                                    <div class="">
                                                    <?php 
                                                        if($dataDefault->attType->isHtml==AttributesType::STATUS_ACTIVE) // html color
                                                        {
                                                            echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;margin-right:25px;" />';
                                                            echo '<span class="color-box" style="float:left;clear:right;background:'.$dataDefault->att->name.';"></span>';
                                                        }
                                                        else 
                                                        {
                                                            echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;" />';
                                                            echo '<label for="'.$dataDefault->itemId.'">'.$dataDefault->att->name.'</label>';
                                                        }
                                                    ?>
                                                    </div>
                                                    <?php 
                                                    if(!empty($defaultAttTypeModel)) :
                                                        foreach($defaultAttTypeModel as $keyType=>$dataType) : 
                                                        if($dataType->id!=$dataDefault->attTypeId) :?>
                                                        <div class="">
                                                        <?php 
                                                            if($dataType->isHtml==AttributesType::STATUS_ACTIVE) // html color
                                                            {
                                                                foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $cKey=>$cVal) :
                                                                echo '<span class="color-box" style="background:'.$cVal->att->name.';"></span>';
                                                                endforeach;
                                                            }
                                                            else // non html 
                                                            {
                                                                foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $key=>$data) :
                                                                    echo '<span>'.$data->att->name.'</span>';
                                                                endforeach;	
                                                            }
                                                        ?>
                                                        </div>
                                                        <?php endif;
                                                        endforeach;
                                                    endif;?>
                                                </div>
                                            <?php endforeach;
                                            echo '<input type="hidden" id="isAttributes" value="1" />';
                                        else : echo '<input type="hidden" id="isAttributes" value="0" />';
                                        endif;?>
                                        <!-- dynamic attributes ends here -->
                                        </div> <!-- /.dynamic-attr -->
									</div>
                            </div>
							<div class="quantity-container info-container">
								<div class="row">
									<div class="col-sm-2">
										<span class="label">Qty :</span>
									</div>
									<div class="col-sm-2">
										<div class="cart-quantity">
											<div class="quant-input">
								                <div class="arrows">
								                  <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
								                  <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
								                </div>
								                <input id="quantity" type="text" value="1">
							              </div>
							            </div>
									</div>
                                    <div class="col-sm-7">
										<a href="javascript:void(0);" id="addto-cart" class="btn btn-primary">ADD TO CART</a>
									</div>
                                </div><!-- /.row -->
							</div><!-- /.quantity-container -->							
					    </div><!-- /.product-info -->
					</div><!-- /.col-sm-7 -->
				</div><!-- /.row -->
			
				<div class="product-tabs inner-bottom-xs  wow fadeInUp">
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#description">Specification</a></li>
								<li><a data-toggle="tab" href="#tags">Description</a></li>
                                <li><a data-toggle="tab" href="#review">REVIEW <?php if(ItemReview::countReviewByItemId($model->id)>1){echo '';} echo ' ('.ItemReview::countReviewByItemId($model->id).')';?></a></li>	
                                <li><a data-toggle="tab" href="#payment">Payment</a></li>                                
								
							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">
							<div class="tab-content">
								<div id="description" class="tab-pane in active">
									<div class="product-tab">
										<p class="text"><?php echo $model->specification;?></p>
									</div>	
								</div>
                                <div id="payment" class="tab-pane">
									<div class="product-tab">
										<p class="text"><?php echo $model->description;?></p>
									</div>	
								</div>
                                <!-- /.tab-pane -->
                                <div id="tags" class="tab-pane">
									<div class="product-tag">
										<p class="text"><?php echo $model->description;?></p>										
									</div><!-- /.product-tab -->
								</div>
                                
								<div id="review" class="tab-pane">
									<div class="product-tab">
										<div class="product-reviews">
                                        <?php if(!empty($modelItemReview)): ?>
											<h4 class="title">Customer Reviews</h4>
											<div class="reviews">
                                            <?php $i=1;  foreach($modelItemReview as $reviewProductData):?>
												<div class="review">
													<div class="review-title"><span class="summary">Best Product For me :) </span><span class="date"><i class="fa fa-calendar"></i><span><?php echo date('H:i:s', strtotime($reviewProductData->crAt));   ?></span></span></div>
													<div class="text"><?php echo 'Comments :'.$reviewProductData->comment; ?></div>
													<div class="text"> Rating  :
                                                    <?php
                                                    $rate=$reviewProductData['rating'];
                                                        $this->widget('CStarRating',array(
                                                            'name'=>'rating'.$i,
                                                            'minRating'=>1,
                                                            'maxRating'=>5,
                                                            'starCount'=>5,
                                                            'value'=>$rate,
                                                            'readOnly'=>true,
                                                        ));
                                                    $i++;
                                                    ?>
                                                    </div>
													<div class="author m-t-15"><i class="fa fa-pencil-square-o"></i> <span class="name"><?php echo 'Name :'.$reviewProductData->name; ?></span></div>									
                                                </div>                                                
											</div><!-- /.reviews -->
                                            <?php
                                            endforeach;
                                            endif; ?>
										</div><!-- /.product-reviews -->
                                        <br/>
										<div class="product-add-review">
											<h4 class="title">Write your own review</h4>
											<div class="review-table">	
                                                <div id="reviewStatus" style="display: none;">
                                                    <div class="notification note-success"><p style="margin-bottom:0px;">Thank You For Your Review.Your Review is Waiting For Approval !</p></div>
                                                </div>											
											</div><!-- /.review-table -->											
											<div class="review-form form">
												<div class="form-container">
													<?php $form=$this->beginWidget('CActiveForm', array(
                                                        'id'=>'item-Review',
                                                        'enableAjaxValidation'=>true,
                                                        'action' => Yii::app()->createUrl('eshop/itemReview'),                                    
                                                        'htmlOptions'=>array(
                                                                'onsubmit'=>"return false;", //Disable normal form submit
                                                                'onkeypress'=>" if(event.keyCode == 13){ sendSubmit(); } " ,// Do ajax call when user presses enter key
                                                                'class'=>'cnt-form'
                                                            ),
                                                        )); ?>
													    <div class="row">
															<div class="col-sm-6">
																<div class="form-group">
																	<?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$model->id));?> 
                                                                    <?php echo $form->labelEx($modelReview,'name'); ?>
                                                                    <?php echo $form->textField($modelReview,'name',array('class'=>'form-control txt','size'=>60,'maxlength'=>255,'placeholder'=>'Name')); ?>
                                                                    <?php echo $form->error($modelReview,'name'); ?>
																</div><!-- /.form-group -->
																<div class="form-group">
                                                                 <?php echo $form->labelEx($modelReview,'email'); ?>
																	<?php echo $form->textField($modelReview,'email',array('class'=>'form-control txt','size'=>60,'maxlength'=>255,'placeholder'=>'Email')); ?>
                                                                    <?php echo $form->error($modelReview,'email'); ?>
																</div><!-- /.form-group -->
                                                                <div class="form-group">
																	<?php echo $form->labelEx($modelReview,'rating'); ?>
                                                                    <?php
                                                                    $this->widget('CStarRating',array(
                                                                        'model'=>$modelReview,
                                                                        'name'=>$modelReview['rating'],
                                                                        'attribute'=>'rating',
                                                                        'minRating'=>1, //minimal value
                                                                        'maxRating'=>5,//max value
                                                                        'starCount'=>5, //number of stars
                                                                        'titles'=>array(
                                                                            '1'=>'Normal',
                                                                            '2'=>'Average',
                                                                            '3'=>'OK',
                                                                            '4'=>'Good',
                                                                            '5'=>'Excellent'
                                                                        ),
                                                                    ));
                                                                    ?>
                                                                    <?php echo $form->error($modelReview,'rating'); ?>                                                              
																</div><!-- /.form-group -->
															</div>
															<div class="col-md-6">
																<div class="form-group">
																	<?php echo $form->labelEx($modelReview,'comment'); ?>
                                                                    <?php echo $form->textArea($modelReview,'comment',array('class'=>'form-control txt txt-review','size'=>60,'maxlength'=>255)); ?>
                                                                    <?php echo $form->error($modelReview,'comment'); ?>
                                        
																</div><!-- /.form-group -->
															</div>
														</div><!-- /.row -->
														<div class="action text-right">
															<?php echo CHtml::Button('SUBMIT',array('onclick'=>'sendSubmit();','class'=>'btn btn-primary btn-upper')); ?>
                                                        </div><!-- /.action -->
													<?php $this->endWidget(); ?><!-- /.cnt-form -->
												</div><!-- /.form-container -->
											</div><!-- /.review-form -->
										</div><!-- /.product-add-review -->										
									</div><!-- /.product-tab -->
								</div><!-- /.tab-pane -->							
                            </div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->
				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
            <?php $this->widget('EshopRecommanded',array('limit'=>15));?>              
            </div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.body-content -->

<script>
    function sendSubmit() 
	{
        var ItemReviewName= $('#ItemReview_name').val();
        var ItemReviewEmail= $('#ItemReview_email').val();
        var ItemReviewComment= $('#ItemReview_comment').val();
        var rating= $('.star-rating-applied').val();
        if(ItemReviewName==''){
            $("#ItemReview_name_em_").show();
            $('#nameAddClass').addClass('error');
            $("#ItemReview_name_em_").html('Name cannot be blank.');
        }
        if(ItemReviewEmail==''){
            $("#ItemReview_email_em_").show();
            $('#emailAddClass').addClass('error');
            $("#ItemReview_email_em_").html('Email cannot be blank.');
        }
        if(ItemReviewComment==''){
            $("#ItemReview_comment_em_").show();
            $('#contentAddClass').addClass('error');
            $("#ItemReview_comment_em_").html('Comment cannot be blank.');
        }
        var data=$("#item-Review").serialize();
        if(ItemReviewName!='' && ItemReviewEmail!='' && ItemReviewComment!='' ){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl("eshop/itemReview"); ?>',
                data:data,
                success:function(data){
                    $('#reviewStatus').show('slow');
                },
                error: function(data) { // if error occured
                    alert("Error occured.please try again");
                }
            });
        }
    }

	$(document).ready(function(){
	    $("#qtyPlus").click(function(){
	    	var inp = $('#quantity').val();	       
	        $('#quantity').val(parseInt(inp)+1);	       
	    });

	    $("#qtyMinus").click(function(){
	    	var inp = $('#quantity').val();	  
	    	if(parseInt(inp)>1){ 
	        	$('#quantity').val(parseInt(inp)-1);	  
	        }     
	    });
	}); 
</script>