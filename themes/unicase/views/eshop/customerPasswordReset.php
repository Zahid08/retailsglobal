<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" language="javascript">
$(function() 
{
	//-------enter press ------------//
	$(document).keypress(function(e) {
		if(e.which  == 13) 
		{
			$('#login-form').get(0).submit();
		}
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<section>
    <div class="container">
        <div class="breadcrumbs">
            <ol class="breadcrumb">
                <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';//echo $catModel->subdept->dept->name;?></a></li>
                <li class="active"><?php echo 'Reset Password'; ?></li>
            </ol>
        </div>  
    </div>
</section><!-- ========================================= BREADCRUMB : END ========================================= -->
<!-- ========================================= BREADCRUMB : END ========================================= -->
<section id="form" style="margin-bottom: 150px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="login-form form"><!--login form-->
                    <h2 class="bordered">Reset Password</h2>
                        <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>                 
                        <?php echo CHtml::beginForm('','post',array('id'=>'customer-reset-password','class'=>'cf-style-1'));?>

                        <?php echo CHtml::errorSummary($form); ?>  
                        
                        <?php echo CHtml::activePasswordField($form,'password' ,array('value'=>'','placeholder'=>'Password')); ?>
                        <?php echo CHtml::activePasswordField($form,'verifyPassword' ,array('value'=>'','placeholder'=>'Confirm Password')); ?>
                        
                        <button class="btn btn-default" type="submit">Submit</button>
                        <?php //echo CHtml::submitButton('Submit', array('class'=>'btn btn-default')); ?>
                    <?php echo CHtml::endForm(); ?>  
                </div><!--/login form-->
            </div>
            <div class="col-sm-1">
                <h2 class="or">OR</h2>
            </div>
            <div class="col-sm-4">
                <div class="signup-form form"><!--sign up form-->
                    <h2 class="bordered">Signin as Customer </h2>
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'login-form',
                            'enableAjaxValidation'=>true,
                            'htmlOptions'=>array(
                                'class'=>'login-form cf-style-1',
                                'role'=>'form'
                            )
                        )); 
                        echo $form->hiddenField($model,'branchId', array('value'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE))); ?>
                        <?php echo $form->errorSummary($model); ?>

                        <?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>255,'placeholder'=>'Username')); ?>
                        <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255,'placeholder'=>'Password')); ?>
                        <button class="btn btn-default" type="submit">Signup</button>
                     <?php $this->endWidget(); ?>
                </div><!--/sign up form-->
            </div>
        </div>
    </div>
</section>



<?php /*

<!-- END PAGE TITLE & BREADCRUMB-->
<main class="inner-bottom-md" id="authentication">
    <div class="container">
        <div class="row">
            <div class="col-md-6 form">
				<section style="margin: 0px;" class="section sign-in inner-right-xs">
                    <h2 class="bordered">Reset Password</h2>
                    <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>                 
                    <?php echo CHtml::beginForm('','post',array('id'=>'customer-reset-password','class'=>'cf-style-1'));?>

                    <?php echo CHtml::errorSummary($form); ?>  

                    <div class="field-row">                        
                        <?php echo CHtml::activeLabelEx($form,'password'); ?>
                        <?php echo CHtml::activePasswordField($form,'password' ,array('value'=>'','class'=>'le-input')); ?>
                    </div>                    
                    <div class="field-row">
                        <?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
                        <?php echo CHtml::activePasswordField($form,'verifyPassword' ,array('value'=>'','class'=>'le-input')); ?>
                    </div>
                    <div class="buttons-holder">
                        <?php echo CHtml::submitButton('Submit', array('class'=>'le-button hug')); ?>
                    </div><!-- /.buttons-holder -->
                   <?php echo CHtml::endForm(); ?>   
                </section><!-- /.sign-in -->
            </div><!-- /.col -->
            <div class="col-md-6 form">
                <section style="margin: 0px;" class="section register inner-left-xs">
                    <h2 class="bordered">Signin as Customer </h2>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'login-form',
                        'enableAjaxValidation'=>true,
                        'htmlOptions'=>array(
                            'class'=>'login-form cf-style-1',
                            'role'=>'form'
                        )
                    )); 
					echo $form->hiddenField($model,'branchId', array('value'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE))); ?>
                    <?php echo $form->errorSummary($model); ?>
                    <div class="field-row">
                        <?php echo $form->labelEx($model,'username'); ?>
                        <?php echo $form->textField($model,'username',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                    <div class="field-row">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'password'); ?>
                    </div>
                    <div class="buttons-holder">
                        <?php echo CHtml::submitButton('Signin', array('class'=>'le-button hug')); ?>
                    </div><!-- /.buttons-holder -->
                    <?php $this->endWidget(); ?>
                </section><!-- /.register -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main>  */ ?>
