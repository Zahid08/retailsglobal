<div class="col-xs-9 col-md-9 items-holder no-margin">
   <!-- /.cart-item starts-->
   <div id="cart_item_holder_21" class="row no-margin cart-item">
      <div class="row">
         <div class="col-lg-12 cont padding-top-0" id="checkout-form">
            <div class="row">
               <div class="col-md-12 no-padding">
                  <h4 class="margin-btm-20 panel-heading">INVOICE : <?php echo $orderModel->invNo; ?></h4>
               </div>
               <div class="col-lg-6">
                  <address>
                     <h4>Shipping Address:</h4>
                     <p>
                        <em><?php echo $orderModel->billingAddress; ?></em>
                     </p>
                  </address>
               </div>
               <div class="col-lg-6">
                  <address>
                     <h4>Billing Address:</h4>
                     <p>
                        <em><?php echo $orderModel->shippingAddress; ?></em>
                     </p>
                  </address>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <h4>Product List</h4>
                  <ul class="product list-inline">
                     <?php
                     if (!empty($orderDetailsModel)) :
                        foreach ($orderDetailsModel as $key => $data) :
                           ?>
                           <li>
                              <div class="lazyImage catalog-item loaded sprite-loaded">
                                 <figure class="image" id="<?php echo $data->item->itemImages[0]->image; ?>">
                                    <?php if (!empty($data->item->itemImages)) echo '<img class="lazy" alt="" src="' . $data->item->itemImages[0]->image . '" style="height:170px; width: 100%;" />'; ?>
                                 </figure>
                              </div>

                           <figcaption>
                              <div style="width:160px;margin:5px;"><h5><?php echo $data->item->itemName; ?></h5></div>
                              <div style="width:160px;margin:5px;">Quantity : <?php echo $data->qty; ?></div>
                              <div style="margin-left:5px;">
                                 <?php echo Company::getCurrency() . '&nbsp;' . ($data->salesPrice - $data->discountPrice) * $data->qty; ?>
                              </div>
                           </figcaption>
                           </li>
                        <?php endforeach; ?>
                     </ul>
                  <?php else : ?>
                     <div id="mycart_empty" style="border:1px dashed #ddd; padding:5px 10px;">
                        <p>You currently have no items saved in your Shopping Bag.</p>
                        <a href="javascript:void(0);" class="btn btn-continue btn-primary pull-right" onclick="window.history.back();" style=" float:right;">Shopping</a>
                     </div>
                  <?php endif; ?>
               </div>
            </div>
         </div>
         <div class="col-lg-12">
            <p class="text-center">
               <em><strong><?php if (!empty($orderModel->shippingId)) echo $orderModel->shipping->day; ?></strong> business days from <?php if (!empty($companyModel)) echo $companyModel->name; ?> <strong><?php if (!empty($shippingModel)) echo $orderModel->shipping->title; ?></strong></em></p>

            <div class="row">
               <div class="col-sm-6">
                  <div class="chose_area">
                     <h4>Payment Method : <?php if (!empty($orderModel->paymentId)) echo $orderModel->payment->title; ?></h4>
                     <?php if (!empty($orderModel->paymentId)) : ?>
                        <img src="<?php echo $orderModel->payment->image; ?>" alt="<?php echo $orderModel->payment->title; ?>">
                     <?php endif; ?>
                  </div>
                  <p class="text-center"><em>Under our Terms & Conditions</em></p>
               </div>
               <div class="col-sm-6">
                  <div class="total_area">
                     <h4><?php if (!empty($companyModel)) echo $companyModel->name; ?></h4>
                     <ul>
                        <li>Cart Sub Total : <span><?php echo Company::getCurrency() . '&nbsp;' . $orderModel->totalPrice; ?></span></li>
                        <li>Shipping Cost : <span><?php echo Company::getCurrency() . '&nbsp;' . UsefulFunction::formatMoney($orderModel->totalTax); ?></span></li>
                        <li>Total : <span><?php echo Company::getCurrency() . '&nbsp;' . UsefulFunction::formatMoney($orderModel->totalPrice + $orderModel->totalTax); ?></span></li>
                        <li>Discount : <span><?php echo Company::getCurrency() . '&nbsp;' . UsefulFunction::formatMoney($orderModel->totalDiscount); ?></span></li>
                        <li>Grand Total : <span><?php echo Company::getCurrency() . '&nbsp;' . UsefulFunction::formatMoney(($orderModel->totalPrice + $orderModel->totalTax) - $orderModel->totalDiscount); ?></span></li>
                     </ul>
                     <a href="javascript:void(0);" class="margin-top-20 le-button btn btn-danger btn-sm update">Thank You</a>

                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>