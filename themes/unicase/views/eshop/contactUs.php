<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<?php
$socialModel = SocialNetwork::model()->findAll('status=:status', array(':status' => SocialNetwork::STATUS_ACTIVE));
?>
<section>
   <div class="container">
      <div class="breadcrumbs">
         <ol class="breadcrumb">
            <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home'; //echo $catModel->subdept->dept->name;      ?></a></li>
            <li class="active"><?php echo 'Contact Us'; ?></li>
         </ol>
      </div>
   </div>
</section>


<div class="row inner-bottom-sm">
   <div class="contact-page">
      <?php if (Yii::app()->user->hasFlash('contact')): ?>
         <div class="row">
            <div class="col-sm-12">
               <div class="status alert alert-success"><?php echo Yii::app()->user->getFlash('contact'); ?></div>
            </div>
         </div>
      <?php endif; ?>
      <div class="col-md-9 contact-form form">
         <div class="col-md-12 contact-title">
            <h4>Contact Form</h4>
         </div>
         <?php
         $form = $this->beginWidget('CActiveForm', array(
             'id' => 'contact-form',
             'enableClientValidation' => true,
             'clientOptions' => array(
                 'validateOnSubmit' => true,
             ),
             'htmlOptions' => array(
                 'class' => 'contact-form',
             ),
         ));
         ?>
         <?php echo $form->errorSummary($model); ?>
         <div class="col-md-4 ">
            <div class="form-group">
               <?php echo $form->labelEx($model, 'name', array('class' => 'info-title')); ?>
               <?php echo $form->textField($model, 'name', array('class' => 'form-control unicase-form-control text-input', 'placeholder' => 'Name')); ?>
               <?php echo $form->error($model, 'name'); ?>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <?php echo $form->labelEx($model, 'email', array('class' => 'info-title')); ?>
               <?php echo $form->textField($model, 'email', array('class' => 'form-control unicase-form-control text-input', 'placeholder' => 'Email')); ?>
               <?php echo $form->error($model, 'email'); ?>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group">
               <?php echo $form->labelEx($model, 'subject', array('class' => 'info-title')); ?>
               <?php echo $form->textField($model, 'subject', array('class' => 'form-control unicase-form-control text-input', 'placeholder' => 'Subject')); ?>
               <?php echo $form->error($model, 'subject'); ?>
            </div>
         </div>
         <div class="col-md-12">
            <div class="form-group">
               <?php echo $form->labelEx($model, 'body', array('class' => 'info-title')); ?>
               <?php echo $form->textArea($model, 'body', array('rows' => '4', 'cols' => '50', 'class' => 'form-control unicase-form-control', 'placeholder' => 'Your Message Here')); ?>
               <?php echo $form->error($model, 'body'); ?>
            </div>
         </div>
         <div class="col-md-12">
            <div class="form-group">
               <?php if (CCaptcha::checkRequirements()): ?>
                  <?php $this->widget('CCaptcha'); ?>
                  <?php echo $form->textField($model, 'verifyCode'); ?>
                  <?php echo $form->error($model, 'verifyCode'); ?>
               <?php endif; ?>
            </div>
         </div>
         <div class="col-md-12 outer-bottom-small m-t-20">
            <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Send Message</button>
         </div>
         <?php $this->endWidget(); ?>
      </div>

      <div class="col-md-3 contact-info">
         <div class="contact-title">
            <h4>INFORMATION</h4>
         </div>
         <div class="clearfix address">
            <span class="contact-i"><i class="fa fa-map-marker"></i></span>
            <span class="contact-span"><?php echo $branchModel->addressline . '&nbsp,' . $branchModel->city . '&nbsp,' . $branchModel->postalcode; ?></span>
         </div>
         <div class="clearfix phone-no">
            <span class="contact-i"><i class="fa fa-mobile"></i></span>
            <span class="contact-span"><?php echo $branchModel->phone; ?></span>
         </div>
         <div class="clearfix email">
            <span class="contact-i"><i class="fa fa-envelope"></i></span>
            <span class="contact-span"><?php echo $companyModel->email; ?></span>
         </div>

         <div class="social-networks margin-top-20">
            <?php if (!empty($socialModel)): ?>
               <div class="contact-title">
                  <h4>Social Networking</h4>
               </div>
               <ul>
                  <?php
                  foreach ($socialModel as $key => $socialData):
                     $socialNet = SocialNetwork::getDomainName($socialData->url);
                     ?>
                     <?php if ($socialNet == 'facebook') { ?>
                        <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-facebook"></i></a></li>
                     <?php } else if ($socialNet == 'twitter') { ?>
                        <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-twitter"></i></a></li>
                     <?php } else if ($socialNet == 'linkedin') { ?>
                        <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                     <?php } else if ($socialNet == 'dribbble') { ?>
                        <li><a href="<?php echo $socialData->url; ?>" title='<?php echo $socialData->name; ?>' target="_blank" ><i class="fa fa-dribbble"></i></a></li>
                     <?php } ?>
                  <?php endforeach; ?>
               </ul>
            <?php endif; ?>
         </div>


      </div>
   </div><!-- /.contact-page -->
</div><!-- /.row -->

<style>
   .formText
   {
      background-color: #fff;
      border: 1px solid #ddd;
      border-radius: 4px;
      box-shadow: inherit;
      color: #696763;
      font-size: 16px;
      padding: 6px 12px;
      width: 100%;
   }
   .formText:focus,.formText:hover
   {
      border-color: #fdb45e;
      box-shadow: inherit;
   }
   .formText:focus,.formText.error:hover
   {
      border-color: #fdb45e;
      box-shadow: inherit;	 border: 1px solid #fe980f;
   }
   div.form div.error input, div.form div.error textarea, div.form div.error select, div.form input.error, div.form textarea.error, div.form select.error {
      background-color: #fff;
      border: 1px solid #ddd;

   }

</style>