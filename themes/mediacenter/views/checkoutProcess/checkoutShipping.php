<script type="text/javascript" language="javascript">
	$(function() 
	{
		$("#optionsRadiosSh1").click(function() {
			$("#displayShipping").hide("slow");
		});
		$("#optionsRadiosSh2").click(function() {
			$("#displayShipping").show("slow");
		});
	});
</script>
<div class="row">
    <div class="col-lg-12">
		<div class="progressbar">
			<ul class="list-inline">
				<li class="active-pgb">
					<span>1</span>
					<h5>Shopping Bag</h5>
				</li>
				<li class="active-pgb">
					<span>2</span>
					<h5>Checkout</h5>
				</li>
				<li>
					<span>3</span>
					<h5>Thank You</h5>
				</li>
			</ul>
		</div>
		<!-- .End Progressbar -->
		<div class="Breadcrumbs">
			<div id="crumbs">
				<ul class="list-inline">
                    <li><a href="">
                       <div><svg class="svg logo-svg" width="28" height="23" viewBox="0 0 31 30" style=""><g transform="scale(0.03125 0.03125)"><path d="M928 128h-832c-52.8 0-96 43.2-96 96v576c0 52.8 43.2 96 96 96h832c52.8 0 96-43.2 96-96v-576c0-52.8-43.2-96-96-96zM96 192h832c17.346 0 32 14.654 32 32v96h-896v-96c0-17.346 14.654-32 32-32zM928 832h-832c-17.346 0-32-14.654-32-32v-288h896v288c0 17.346-14.654 32-32 32zM128 640h64v128h-64zM256 640h64v128h-64zM384 640h64v128h-64z"/></g></svg>
                        </div>Checkout</a>
					</li>
					<li class="finish-crumbs"><a href="#1"><span></span>Login or<br>checkout as a guest</a></li>
					<li class="active-crumbs"><a href="#2"><span></span>Billing <br> & Shipping Details</a></li>
					<li><a href="#3"><span></span>Shipping Options</a></li>
					<li><a href="#4"><span></span>Payment <br> Options</a></li>
					<li><a href="#5"><span></span>Review</a></li>
				</ul>
			</div>
			<!-- #End crumbs -->
		  </div>
		  <!-- .End Breadcrumbs -->
    </div>
    <!-- .End col-lg-12 -->
</div>
<!-- .End row -->

<div class="row">
	<?php if(isset($errMsg) && !empty($errMsg)) : ?>
		<div class="alert-box error"><span>Error : </span><?php echo $errMsg;?></div>
    <?php endif;
    
	$form=$this->beginWidget('CActiveForm', array(
			'id'=>'billing-shipping-form',
			'enableAjaxValidation'=>true,
			'htmlOptions'=>array(
				'class'=>'login-form cf-style-1',
				'role'=>'form'
			)
	)); ?>  
	<?php // echo $form->errorSummary($model); ?>
    <div class="form" id="checkout-form">
        <div class="col-lg-6">
            <h3>Your Billing Address</h3>
            <div class="row">
                <div class="col-lg-6 frist">
					<div class="form-group">
						<label for="input-email" class="label-title">Frist Name <span class="required">*</span></label>
						<?php echo $form->textField($model,'title',array('class'=>"form-control",'placeholder'=>"Your Frist Name")); ?>
						<?php echo $form->error($model,'title'); ?>	
					</div>
				</div>
				<div class="col-lg-6 last">
					<div class="form-group">
						<label for="input-email" class="label-title">Last Name <span class="required">*</span></label>
						<?php echo $form->textField($model,'name',array('class'=>"form-control",'placeholder'=>"Your Last Name")); ?>
						<?php echo $form->error($model,'name'); ?>	
					</div>
				</div>
            </div>
            <div class="form-group">
                <label class="label-title" for="address">Street Address <span class="required">*</span></label>
                <?php echo $form->textField($model,'addressline',array('class'=>"form-control",'placeholder'=>"eg. Surry Hills")); ?>
                <?php echo $form->error($model,'addressline'); ?>	
            </div>
            <div class="form-group">
                <label class="label-title" for="city">City <span class="required">*</span></label>
                <?php echo $form->textField($model,'city',array('class'=>"form-control",'placeholder'=>"eg. Surry Hills")); ?>
                <?php echo $form->error($model,'city'); ?>	
            </div>
			<div class="form-group">
                <label class="label-title" for="address">Zip <span class="required">*</span></label>
                <?php echo $form->textField($model,'zipCode',array('class'=>"form-control",'placeholder'=>"eg. 1219")); ?>
                <?php echo $form->error($model,'zipCode'); ?>	
            </div>
			<div class="form-group">
                <label class="label-title" for="address">Email <span class="required">*</span></label>
                <?php echo $form->textField($model,'email',array('readonly'=>'readonly','class'=>"form-control",'placeholder'=>"Your Email Address")); ?>
                <?php echo $form->error($model,'email'); ?>	
            </div>
            <div class="form-group">
                <label class="label-title" for="address">Phone <span class="required">*</span></label>
                <?php echo $form->textField($model,'phone',array('class'=>"form-control",'placeholder'=>"Your Phone Number")); ?>
                <?php echo $form->error($model,'phone'); ?>	
            </div>
        </div>
        
		<div class="col-lg-6">
			<h3>Your Shipping Address</h3>
			<div class="radio">
				<label>
					<input type="radio" name="optionsRadiosShipping" id="optionsRadiosSh1" value="yes" <?php echo (!empty($shipping) && $shipping=='yes')?'checked':'';?>><span class="check"></span>Deliver to the same address
				</label>
			</div>
			<div class="radio">
				<label>
					<input type="radio" name="optionsRadiosShipping" id="optionsRadiosSh2" value="no" <?php echo (!empty($shipping) && $shipping=='no')?'checked':'';?>><span class="check"></span>Deliver to the different address
				</label>
			</div>
			<section id="displayShipping" <?php echo $shDisplay;?>>
			   <div class="row">
				   <div class="col-lg-6">
						<div class="form-group">
							<label for="input-email" class="label-title">Frist Name <span class="required">*</span></label>
							<?php echo $form->textField($modelCheckout,'sh_fname',array('class'=>"form-control",'placeholder'=>"Your Frist Name")); ?>
							<?php echo $form->error($modelCheckout,'sh_fname'); ?>	
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group">
							<label for="input-email" class="label-title">Last Name <span class="required">*</span></label>
							<?php echo $form->textField($modelCheckout,'sh_lastname',array('class'=>"form-control",'placeholder'=>"Your Last Name")); ?>
							<?php echo $form->error($modelCheckout,'sh_lastname'); ?>	
						</div>
					</div>
			   </div>
			   <div class="form-group">
					<label class="label-title" for="address">Street Address <span class="required">*</span></label>
				   	<?php echo $form->textField($modelCheckout,'sh_streetaddress',array('class'=>"form-control",'placeholder'=>"eg. Surry Hills")); ?>
				   	<?php echo $form->error($modelCheckout,'sh_streetaddress'); ?>	
				</div>
				<div class="form-group">
					<label class="label-title" for="address">City <span class="required">*</span></label>
					<?php echo $form->textField($modelCheckout,'sh_city',array('class'=>"form-control",'placeholder'=>"eg. Surry Hills")); ?>
					<?php echo $form->error($modelCheckout,'sh_city'); ?>	
				</div>
				<div class="form-group">
					<label class="label-title" for="address">Zip <span class="required">*</span></label>
					<?php echo $form->textField($modelCheckout,'sh_zip',array('class'=>"form-control",'placeholder'=>"eg. 1219")); ?>
					<?php echo $form->error($modelCheckout,'sh_zip'); ?>	
				</div>
				<div class="form-group">
					<label class="label-title" for="address">Phone <span class="required">*</span></label>
					<?php echo $form->textField($modelCheckout,'sh_phone',array('class'=>"form-control",'placeholder'=>"Your Phone Number")); ?>
					<?php echo $form->error($modelCheckout,'sh_phone'); ?>	
				</div>
			</section>
		</div>
		
        <div class="col-lg-12">

            <?php echo CHtml::submitButton('Continue', array('class'=>'btn btn-continue btn-primary pull-right')); ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
<!-- .End row -->