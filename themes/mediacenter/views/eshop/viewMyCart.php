
<script>
 $(function() {
	// item quantity updated +/-
	$(".quantity").click(function() {
		var itemId = $(this).attr('id');
		var quantity = $("#quantity_"+itemId).val();

		// price calculation
		var item_price     = parseFloat($("#priceInd_"+itemId).val());
		var item_price_upd = item_price*quantity;
		
		$.ajax({
			type: "POST",
			dataType: "json",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/updateQty/",
			data: {
				itemId: itemId, 
				quantity : quantity
			},					
			success: function(data) {
				$(".preloader").hide();

				// update price
				$("#price_"+itemId).html(item_price_upd);
				$("#total_subtotal").html(data.total_price);
                $("#total_discount").html(data.total_discount);
                var dataGrandTotal = (data.total_price-data.total_discount);
				$("#total_grandtotal").html(dataGrandTotal);
				
				// update shopping bag 
				$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
			},
			error: function() {}
		});	
	});
	 
	// remove from cart global
	$(".remove_from_cart_my_cart").click(function() 
	{
		// values & validations
		var itemId = $(this).attr('id');
		
		// price calculation
		var item_price     = parseFloat($("#price_"+itemId).html());
		var total_subtotal = parseFloat($("#total_subtotal").html());
		var totalbal_subtotal = total_subtotal-item_price;
		
		var total_grandtotal = parseFloat($("#total_grandtotal").html());
		var totalbal_grandtotal = total_grandtotal-item_price;
		
		$.ajax({
			type: "POST",
			dataType: "json",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/removeFromCart/",
			data: {
				itemId: itemId, 
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#cart_item_holder_"+itemId).fadeOut(500, function() {
					$(this).remove();
				});
				
				// update price
				$("#total_subtotal").html(totalbal_subtotal);
				$("#total_grandtotal").html(totalbal_grandtotal);
				
				// update shopping bag 
				$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
			},
			error: function() {}
		});	
	});
	 
	// item moved to wishlist
	$(".move_to_wishlist").click(function() {
		<?php if(!empty(Yii::app()->session['custId'])) : ?>
			var itemId = $(this).attr('id');

			// price calculation
			var item_price     = parseFloat($("#price_"+itemId).html());
			var total_subtotal = parseFloat($("#total_subtotal").html());
			var totalbal_subtotal = total_subtotal-item_price;

			var total_grandtotal = parseFloat($("#total_grandtotal").html());
			var totalbal_grandtotal = total_grandtotal-item_price;

			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addMoveToWishList/",
				data: {
					itemId: itemId, 
				},					
				success: function(data) {
					$(".preloader").hide();
					if(data=='exists') alert("Already added to wishlist !");
					else 
					{
						$("#cart_item_holder_"+itemId).fadeOut(500, function() {
							$(this).remove();
						});

						// update price
						$("#total_subtotal").html(totalbal_subtotal);
						$("#total_grandtotal").html(totalbal_grandtotal);

						// update wishlist count
						$("#wishListCount").html(data);
						// update shopping bag 
						$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {});
					}
				},
				error: function() {}
			});
		<?php else : ?> alert("please signin first !"); <?php endif;?>
	});
});
</script>
<!-- ========================================= BREADCRUMB ========================================= -->
<div class="animate-dropdown">
	<div id="breadcrumb-alt">
		<div class="container">
			<div class="breadcrumb-nav-holder minimal">
				<ul>
					<li class="dropdown breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
					<li class="breadcrumb-item current"><a href="javascript:void(0);"><?php echo $this->metaTitle;?></a></li>
				</ul>
			</div>
		</div><!-- /.container -->
	</div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<section id="cart-page">
    <div class="container">
        <!-- ========================================= CONTENT ========================================= -->
		<?php if($total_qty>0) : ?>
        <div class="col-xs-12 col-md-9 items-holder no-margin">
			<div class="row no-margin cart-item" id="cart_item_holder">
				<div class="col-xs-12 col-sm-1 no-margin">					
					<strong>Sl. No</strong>
                </div>
				<div class="col-xs-12 col-sm-2 no-margin">			
					<strong>Image</strong>
                </div>
				 <div class="col-xs-12 col-sm-4 ">				
					<strong>Product Name </strong>
                </div>	
				<div class="col-xs-12 col-sm-3 no-margin">				
					 <strong style="margin-left: 29px;">Quantity</strong>
                </div>
				<div class="col-xs-12 col-sm-2 no-margin">
					 <strong style="margin-left: 29px;">Price</strong>
                </div>
			
			</div>
		
            <!-- /.cart-item starts-->
			<?php $slNo = 0;  foreach ($cart->getItems() as $order_code=>$quantity) : $slNo++; ?>
            <div class="row no-margin cart-item" id="cart_item_holder_<?php echo $order_code;?>">
                <div class="col-xs-12 col-sm-1 no-margin">					
					<?php echo  $slNo; ?>
                </div>
				<div class="col-xs-12 col-sm-2 no-margin">
					<?php if(!empty($cart->getItemImages($order_code))) : echo CHtml::link('<img class="lazy" alt="" src="'.$cart->getItemImages($order_code).'" style="width:73px;height:73px;" />',array('eshop/proDetails','id'=>$order_code),  array('class'=>'thumb-holder','title'=>$cart->getItemName($order_code)));?>
					<?php endif;?>
                </div>

                <div class="col-xs-12 col-sm-4 ">
                    <div class="title">
						<?php echo CHtml::link($cart->getItemName($order_code),array('eshop/proDetails','id'=>$order_code),  array('title'=>$cart->getItemName($order_code)));?>
                    </div>
                    <div class="brand"><?php echo $cart->getItemBrand($order_code);?></div>
					<div class="brand"><a id="<?php echo $order_code;?>" class="move_to_wishlist" href="javascript:void(0);" title="move to wishlist"><i class="fa fa-heart"></i> move to wishlist</a></div>
                </div> 

                <div class="col-xs-12 col-sm-3 no-margin">
                    <div class="quantity" id="<?php echo $order_code;?>">
                        <div class="le-quantity">
							<a class="minus" href="#reduce"></a>
							<input id="quantity_<?php echo $order_code;?>" name="quantity" readonly="readonly" type="text" value="<?php echo $quantity;?>" />
							<a class="plus" href="#add"></a>
                        </div>
                    </div>
                </div> 
				
				<div class="col-xs-12 col-sm-2 no-margin">
                    <div class="price">
						<?php if(Items::getItemWiseOfferPrice($order_code)>0) :?>
							<input type="hidden" id="priceInd_<?php echo $order_code;?>" value="<?php echo Items::getItemWiseOfferPrice($order_code);?>" />
							<?php echo Company::getCurrency();?> <span id="price_<?php echo $order_code;?>"><?php echo Items::getItemWiseOfferPrice($order_code)*$quantity;?></span>
						<?php else : ?>
							<input type="hidden" id="priceInd_<?php echo $order_code;?>" value="<?php echo $cart->getItemPrice($order_code);?>" />
							<?php echo Company::getCurrency();?> <span id="price_<?php echo $order_code;?>"><?php echo $cart->getItemPrice($order_code)*$quantity;?></span>
						<?php endif;?>
                    </div>
					<a id="<?php echo $order_code;?>" class="close-btn remove_from_cart_my_cart" href="javascript:void(0);"></a>
                </div>
            </div><!-- /.cart-item ends-->
			<?php endforeach;?>
        </div>
        <!-- ========================================= CONTENT : END ========================================= -->

        <!-- ========================================= SIDEBAR ========================================= -->
        <div class="col-xs-12 col-md-3 no-margin sidebar ">
            <div class="widget cart-summary">
                <h1 class="border">shopping cart</h1>
                <div class="body">
                    <ul class="tabled-data no-border inverse-bold">
                        <li>
                            <label>cart subtotal</label>
                            <div class="value pull-right">
								<span class="sign"><?php echo Company::getCurrency();?></span>
								<span id="total_subtotal"><?php echo $total_price;?></span>
							</div>
                        </li>
                        <li>
                            <label>discount</label>
                            <div class="value pull-right">
                                <span class="sign"><?php echo Company::getCurrency();?></span>
								<span id="total_discount"><?php echo $total_discount;?></span>
                            </div>
                        </li>
                    </ul>
                    <ul id="total-price" class="tabled-data inverse-bold no-border">
                        <li>
                            <label>order total</label>
                            <div class="value pull-right">
								<span class="sign"><?php echo Company::getCurrency();?></span>
								<span id="total_grandtotal"><?php echo ($total_price-$total_discount);?></span>
							</div>
                        </li>
                    </ul>
                    <div class="buttons-holder">
						<?php 
							if(!empty(Yii::app()->session['custId']))
								echo CHtml::link('Checkout',array('checkoutProcess/checkoutShipping'),array('class'=>'le-button big')); 
							else echo CHtml::link('Checkout',array('checkoutProcess/checkout'),array('class'=>'le-button big')); 
						?>
                        <a class="simple-link block" href="javascript: window.history.go(-1);" >continue shopping</a>
                    </div>
                </div>
            </div><!-- /.widget -->
        </div><!-- /.sidebar -->
		<?php 
		else : echo 'Your cart is empty !';
		endif;?>
        <!-- ========================================= SIDEBAR : END ========================================= -->
    </div>
</section>	
<!-- ========================================= Scart-page : END ========================================= -->

<!-- ========================================= EshopRecommanded Start ========================================= -->
<?php $this->widget('EshopRecommanded');?> 
<!-- ========================================= EshopRecommanded Ends ========================================= -->