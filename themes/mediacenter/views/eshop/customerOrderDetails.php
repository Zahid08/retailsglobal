<style>
.ribbon {
  z-index: 100;
  top: 2.5%;
  font-size: 11px;
  line-height: 20px;
  padding: 4px 14px;
  font-weight: 700;
  text-transform: uppercase;
}
.ribbon:after {
  content: "";
  display: inline-block;
  border-top-width: 28px;
  border-top-style: solid;
  border-left: 0px;
  border-right: 14px solid transparent;
  height: 0;
  min-width: 68px;
  width: auto; 
  position: absolute;
  top: 0;
  left: 0;
  display: table;
  padding: 37px;
}
</style>
<div class="col-xs-12 col-md-9 items-holder no-margin">
    <!-- /.cart-item starts-->
    <div id="cart_item_holder_21" class="row no-margin cart-item">
        <div class="row">
			<div class="col-lg-12 cont" id="checkout-form">
				<div class="row">
                    <div class="col-md-12"><h4 class="margin-btm-20">INVOICE : <?php echo $orderModel->invNo;?></h4></div>
					<div class="col-lg-6">
						<address>
						 <h4>Shipping Address:</h4>
						  <p>
							  <em><?php echo $orderModel->billingAddress;?></em>
						  </p>
					   </address>
					</div>
					<div class="col-lg-6">
						<address>
						 <h4>Billing Address:</h4>
						  <p>
							  <em><?php echo $orderModel->shippingAddress;?></em>
						  </p>
					   </address>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<ul class="product list-inline">
						<?php 
						if(!empty($orderDetailsModel)) : 
							  foreach ($orderDetailsModel as $key=>$data) : ?>
								<li>       
                                    <?php echo (Items::getIsReturnItem($orderModel->id,$data->itemId))?'<div class="ribbon red"><span>return!</span></div>':'';?>
									<div class="lazyImage catalog-item loaded sprite-loaded">
										<figure class="image" id="<?php echo $data->item->itemImages[0]->image;?>">
											<?php if(!empty($data->item->itemImages)) echo '<img class="lazy" alt="" src="'.$data->item->itemImages[0]->image.'" style="width:170px;" />';?>
										</figure>
									</div>

									<figcaption> 
										<div style="width:160px;margin:5px;"><?php echo $data->item->itemName;?></div>  
										<div style="width:160px;margin:5px;">Quantity : <?php echo $data->qty;?></div>  
										<div style="margin-left:5px;">
											<?php echo Company::getCurrency().'&nbsp;'.((($data->salesPrice*$data->qty)+$data->vatPrice)-$data->discountPrice);?>
										</div>       
									</figcaption>
								</li>
							 <?php endforeach; ?>       
							</ul>
							<?php else : ?>
							<div id="mycart_empty" style="border:1px dashed #ddd; padding:5px 10px;">
								 <p>You currently have no items saved in your Shopping Bag.</p>
								 <a href="javascript:void(0);" class="btn btn-continue btn-primary pull-right" onclick="window.history.back();" style=" float:right;">Shopping</a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<p class="text-center">
						<em><strong><?php if(!empty($orderModel->shippingId)) echo $orderModel->shipping->day;?></strong> business days from <?php if(!empty($companyModel)) echo $companyModel->name;?> <strong><?php if(!empty($shippingModel)) echo $orderModel->shipping->title;?></strong></em></p>

					<div class="row">
						<div class="col-lg-8">
							<div class="well ppl">
								<h4>Payment Method : <?php if(!empty($orderModel->paymentId)) echo $orderModel->payment->title;?></h4>
								<?php if(!empty($orderModel->paymentId)) : ?>
									<img src="<?php echo $orderModel->payment->image;?>" alt="<?php echo $orderModel->payment->title;?>">
								<?php endif;?>
							</div>
							<p class="text-center"><em>Under our Terms & Conditions</em></p>
						</div>
						<div class="col-lg-4">
							<div class="bill">
								<h4><?php if(!empty($companyModel)) echo $companyModel->name;?></h4>
								<div class="ammount">
									<ul class="list-unstyled pull-left">
										<li>Sub Total:</li>
										<li>Shipping:</li>
									</ul>
									<ul class="list-unstyled pull-right">
										<li><?php echo Company::getCurrency().'&nbsp;'.($orderModel->totalPrice+$orderModel->totalTax);?></li>
										<li><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalShipping);?></li>
									</ul>
									
								</div>
								<div class="total" style=" border-bottom: 1px dashed #c2c2c2; padding-bottom:8px;">
									<ul class="list-unstyled pull-left">
                                        <li>
                                            <h4>Total:</h4>
                                        </li>
                                        <li>Discount:</li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li>
                                            <h4><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalPrice+$orderModel->totalTax+$orderModel->totalShipping);?></h4>
                                        </li>
                                        <li class="pull-right"><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney($orderModel->totalDiscount);?></li>
                                    </ul>
								</div>
                                <div class="total">
                                    <ul class="list-unstyled pull-left">
                                        <li><h4>Grand Total:</h4></li>
                                    </ul>
                                    <ul class="list-unstyled">
                                        <li><h4><?php echo Company::getCurrency().'&nbsp;'.UsefulFunction::formatMoney(($orderModel->totalPrice+$orderModel->totalTax+$orderModel->totalShipping)-$orderModel->totalDiscount);?></h4></li> 
                                    </ul>
                                </div>
                                <p><small>Thank you</small></p>
							</div>
						</div>
					</div>
				</div>
			</div>
      </div>
</div>