<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<link href='<?php echo Yii::app()->request->baseUrl; ?>/css/etalage.css' type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl ?>/js/jquery.etalage.min.js"></script> 
<script>
 $(function() {
	// image slider zoom 
	$('#etalage').etalage({
		thumb_image_width: 250,
		thumb_image_height: 250,
		source_image_width: 900,
		source_image_height: 1200,
		show_hint: true,
		click_callback: function(image_anchor, instance_id){
			return false;
		}
	    //change_callback(){ alert('it moved'); } 
	});
	 
	// add to cart
	$("#addto-cart").click(function() 
	{
		// values & validations
		var isError = 0,
			isAttributes = $("#isAttributes").val(),
			quantity = $('#quantity').val();

		if(isAttributes==1) { // has attributes
			var itemId = $('input[name="itemId"]:checked').val();
			if(typeof(itemId)=="undefined" || itemId=="") {
				$("#attError").html("Please select at least one Attributes");
				isError = 1;
			}
			else
			{
				$("#attError").html("");
				isError = 0;	
			}
		}
		else var itemId = <?php echo $model->id;?>;
		
		// cart processing
		if(isError==0)
		{
			$.ajax({
				type: "POST",
				dataType: "json",
				beforeSend : function() {
					$(".preloader").show();
				},
				url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/addToCart/",
				data: {
					itemId: itemId, 
					quantity : quantity,
				},					
				success: function(data) {
					$(".preloader").hide();
					
					// update shopping bag 
					$(".basket").load('<?php echo Yii::app()->request->baseUrl; ?>' + '/index.php/eshop/updateCart/', function() {
						$(this).removeClass("basket").addClass("basket open");
						$(this).slideDown(200, function() {
							$('html, body').delay('200').animate({
							scrollTop: $(this).offset().top - 111
							}, 200);
						});
					});
				},
				error: function() {}
			});	
		}
	});
     
    // item att wise price change
	$('.radio_item_attributes').click(function()
	{
        var itemId = $(this).attr('id');
        var urlajax = "<?php  echo Yii::app()->request->baseUrl; ?>/index.php/eshop/changeItemPriceStrByAttributes/";
        $.ajax({
            type: "POST",
            url: urlajax,
            beforeSend: function() 
            {
                $(".preloader").show();
            },
            data: 
            {
                itemId : itemId,
            },
            success: function(data) 
            {
                $(".preloader").hide();
                $('.prices').html(data); 
            },
            error: function() {}
        });
	});
	
});
</script>
<!-- ========================================= BREADCRUMB ========================================= -->
<div class="animate-dropdown">
	<div id="breadcrumb-alt">
		<div class="container">
			<div class="breadcrumb-nav-holder minimal">
				<ul>
					<li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo $model->cat->subdept->dept->name;?></a></li>
					<li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo $model->cat->subdept->name;?></a></li>
					<li class="breadcrumb-item current"><a href="javascript:void(0);"><?php echo $model->cat->name;?></a></li>
				</ul>
			</div>
		</div><!-- /.container -->
	</div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<div id="single-product">
    <div class="container">
         <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
            <div class="product-item-holder size-big single-product-gallery small-gallery">
                <!-- image zoomer starts -->
				<ul id="etalage" class="etalage">
					<?php 
					  if(!empty($model->itemImages)) : // main image ?>
					  <li class="etalage_thumb thumb_<?php echo $model->itemImages[0]->id;?> etalage_thumb_active" style="display: list-item; background-image: none; opacity: 1;">
							<img title="<?php echo $model->itemName;?>" src="<?php echo $model->itemImages[0]->image;?>" class="etalage_source_image" style="display: inline; opacity: 1;" />
					  </li>

					<?php foreach($model->itemImages as $image) : // thumbs ?>                                
						<li class="etalage_thumb thumb_<?php echo $image->id;?>" style="background-image: none; display: none; opacity: 0;">
							<img title="<?php echo $model->itemName;?>" src="<?php echo $image->image;?>" class="etalage_source_image">
						</li>
					<?php endforeach;
					endif; ?>
				</ul>
                <!-- image zoomer ends -->   
            </div><!-- /.single-product-gallery -->
        </div><!-- /.gallery-holder -->   
             
        <div class="no-margin col-xs-12 col-sm-7 body-holder">
            <div class="body">
                <div class="title"><a href="javascript:void(0);"><?php echo $model->itemName;?></a></div>
                <div class="brand">
					<?php echo $model->brand->name;?>
					<div class="buttons-holder pull-right">
						<a id="<?php echo $model->id;?>" class="btn-add-to-wishlist" href="javascript:void(0);">add to wishlist</a>
					</div>
				</div>
        
                <!--<div class="social-row"></div>-->
                <div class="excerpt">
                    <p><?php echo substr($model->specification,0,200).'..';?></p>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="prices">
							<?php if(Items::getItemWiseOfferPrice($model->id)>0) :?>
								<div class="price-current"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($model->id);?></div>
								<div class="price-prev" style="text-decoration: line-through;">
									<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?>
								</div>
							<?php else : ?>
								<div class="price-current"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($model->id);?></div>
							<?php endif;?>
                        </div>
                    </div>
                </div>
                
                <div class="dynamic-attr">
					<span id="attError" style="color:#F00;"></span>
					<!-- dynamic attributes starts here -->
					<?php if(!empty(ItemAttributes::getAttributesByAllItemsGroup($model->itemName))) : 
						foreach(ItemAttributes::getAttributesByAllItemsGroup($model->itemName) as $keyDefault=>$dataDefault) : ?>
						<div class="attr-row">
							<div class="">
							<?php 
                                if($dataDefault->attType->isHtml==AttributesType::STATUS_ACTIVE) // html color
                                {
                                    echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;margin-right:25px;" />';
	                                echo '<span class="color-box" style="float:left;clear:right;background:'.$dataDefault->att->name.';"></span>';
                                }
                                else 
                                {
                                    echo '<input type="radio" name="itemId" id="'.$dataDefault->itemId.'" value="'.$dataDefault->itemId.'" class="radio_item_attributes" style="float:left;clear:right; margin-top:10px;" />';
                                    echo '<label for="'.$dataDefault->itemId.'">'.$dataDefault->att->name.'</label>';
                                }
							?>
							</div>
							<?php 
							if(!empty($defaultAttTypeModel)) :
								foreach($defaultAttTypeModel as $keyType=>$dataType) : 
                                if($dataType->id!=$dataDefault->attTypeId) :?>
								<div class="">
								<?php 
									if($dataType->isHtml==AttributesType::STATUS_ACTIVE) // html color
									{
										foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $cKey=>$cVal) :
										echo '<span class="color-box" style="background:'.$cVal->att->name.';"></span>';
										endforeach;
									}
									else // non html 
									{
										foreach(ItemAttributes::getAttributesByAllItemsGroupMatched($dataType->id,$dataDefault->itemId,$model->itemName) as $key=>$data) :
											echo '<span>'.$data->att->name.'</span>';
										endforeach;	
									}
								?>
								</div>
								<?php endif;
                                endforeach;
							endif;?>
						</div>
					<?php endforeach;
					echo '<input type="hidden" id="isAttributes" value="1" />';
				else : echo '<input type="hidden" id="isAttributes" value="0" />';
				endif;?>
				<!-- dynamic attributes ends here -->
                </div> <!-- /.dynamic-attr -->
                <div class="qnt-holder">
                    <div class="le-quantity">
                        <a class="minus" href="#reduce"></a>
                        <input id="quantity" name="quantity" readonly="readonly" type="text" value="1" />
                        <a class="plus" href="#add"></a>
                    </div>
                    <a id="addto-cart" href="javascript:void(0);" class="le-button huge">add to cart</a>
                </div><!-- /.qnt-holder -->
            </div><!-- /.body -->  
        </div><!-- /.body-holder -->
    </div><!-- /.container -->
</div><!-- /.single-product -->

<!-- ========================================= SINGLE PRODUCT TAB ========================================= -->
<section id="single-product-tab">
    <div class="container">
        <div class="tab-holder">
            
            <ul class="nav nav-tabs simple" >
                <li class="active"><a href="#specification" data-toggle="tab">Specification</a></li>
				<li><a href="#description" data-toggle="tab">Description</a></li>
                <li><a href="#reviews" data-toggle="tab">Review<?php if(ItemReview::countReviewByItemId($model->id)>1){echo 's';} echo ' ('.ItemReview::countReviewByItemId($model->id).')';?></a></li>
				<li><a href="#shipping_payment" data-toggle="tab">Shipping & Payments</a></li>
            </ul><!-- /.nav-tabs -->

            <div class="tab-content">
				<div class="tab-pane active" id="specification">
                    <p><?php echo $model->specification;?></p>
                </div><!-- /.tab-pane #specification -->
                <div class="tab-pane" id="description">
                    <p><?php echo $model->description;?></p>
                </div><!-- /.tab-pane #description -->
                <div class="tab-pane" id="reviews">
                    <div class="comments">
                        <?php if(!empty($modelItemReview)):
                              $i=1;  foreach($modelItemReview as $reviewProductData):
                            ?>
                            <div class="comment-item">
                                <div class="row no-margin">
                                    <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                                        <div class="avatar">
                                            <img alt="avatar" src="<?php echo Yii::app()->session['themesnames'];?>/images/default-avatar.jpg">

                                        </div><!-- /.avatar -->
                                    </div><!-- /.col -->

                                    <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                                        <div class="comment-body">
                                            <div class="meta-info">
                                                <div class="author inline">
                                                    <?php echo CHtml::link($reviewProductData->name,array('#'),array('class'=>'bold'))?>
                                                </div>
                                                <div class="star-holder inline">
                                                    <?php
                                                    $rate=$reviewProductData['rating'];
                                                        $this->widget('CStarRating',array(
                                                            'name'=>'rating'.$i,
                                                            'minRating'=>1,
                                                            'maxRating'=>5,
                                                            'starCount'=>5,
                                                            'value'=>$rate,
                                                            'readOnly'=>true,
                                                        ));
                                                    $i++;
                                                    ?>
                                                </div>
                                                <div class="date inline pull-right">
                                                   <?php  echo date('d M Y', strtotime($reviewProductData->crAt));   ?>
                                                </div>
                                            </div><!-- /.meta-info -->
                                            <p class="comment-text">
                                                <?php echo $reviewProductData->comment; ?>
                                            </p><!-- /.comment-text -->
                                        </div><!-- /.comment-body -->
                                    </div><!-- /.col -->
                                </div><!-- /.row -->
                            </div><!-- /.comment-item -->
                        <?php
                            endforeach;
                        endif; ?>
                    </div><!-- /.comments -->

                    <div class="add-review row form">
                        <div class="col-sm-8 col-xs-12">
                            <div class="new-review-form">
                                <h2>Add review</h2>
                                <div id="reviewStatus" style="display: none;">
                                    <div class="notification note-success"><p style="margin-bottom:0px;">Thank You For Your Review.Your Review is Waiting For Approval !</p></div>
                                </div>
                                <?php $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'item-Review',
                                    'enableAjaxValidation'=>true,
                                    'action' => Yii::app()->createUrl('eshop/itemReview'),
                                    /*'htmlOptions'=>array(
                                        'class'=>'login-form cf-style-1',
                                        'role'=>'form'
                                        ) */
                                         'htmlOptions'=>array(
                                            'onsubmit'=>"return false;", //Disable normal form submit
                                            'onkeypress'=>" if(event.keyCode == 13){ sendSubmit(); } " // Do ajax call when user presses enter key
                                        ),
                                    )); ?>

                                    <div class="row field-row">
                                        <div id="nameAddClass" class="col-xs-12 col-sm-5 no-margin">
                                            <?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$model->id));?>
                                            <?php echo $form->labelEx($modelReview,'name'); ?>
                                            <?php echo $form->textField($modelReview,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                            <?php echo $form->error($modelReview,'name'); ?>
                                        </div>
                                        <div id="emailAddClass" class="col-xs-12 col-sm-7">
                                            <?php echo $form->labelEx($modelReview,'email'); ?>
                                            <?php echo $form->textField($modelReview,'email',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                            <?php echo $form->error($modelReview,'email'); ?>
                                        </div>
                                    </div><!-- /.field-row -->

                                    <div class="field-row star-row">
                                        <?php echo $form->labelEx($modelReview,'rating'); ?>
                                        <?php //echo $form->textField($modelReview,'rating',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                        <?php
                                        $this->widget('CStarRating',array(
                                            'model'=>$modelReview,
                                            'name'=>$modelReview['rating'],
                                            'attribute'=>'rating',
                                            'minRating'=>1, //minimal value
                                            'maxRating'=>5,//max value
                                            'starCount'=>5, //number of stars
                                            'titles'=>array(
                                                '1'=>'Normal',
                                                '2'=>'Average',
                                                '3'=>'OK',
                                                '4'=>'Good',
                                                '5'=>'Excellent'
                                            ),
                                        ));
                                        ?>
                                        <?php echo $form->error($modelReview,'rating'); ?>
                                    </div><!-- /.field-row -->

                                    <div id="contentAddClass" class="field-row">
                                        <?php echo $form->labelEx($modelReview,'comment'); ?>
                                        <?php echo $form->textArea($modelReview,'comment',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                        <?php echo $form->error($modelReview,'comment'); ?>
                                    </div><!-- /.field-row -->

                                    <div class="buttons-holder">
                                        <?php echo CHtml::Button('SUBMIT',array('onclick'=>'sendSubmit();','class'=>'le-button huge')); ?>
                                    </div><!-- /.buttons-holder -->
                                <?php $this->endWidget(); ?><!-- /.contact-form -->
                            </div><!-- /.new-review-form -->
                        </div><!-- /.col -->
                    </div><!-- /.add-review -->

                </div><!-- /.tab-pane #reviews -->
				<div class="tab-pane" id="shipping_payment">
                    <p><?php echo $model->shipping_payment;?></p>
                </div><!-- /.tab-pane #shipping_payment -->
            </div><!-- /.tab-content -->

        </div><!-- /.tab-holder -->
    </div><!-- /.container -->
</section><!-- /#single-product-tab -->
<!-- ========================================= SINGLE PRODUCT TAB : END ========================================= -->

<!-- ========================================= EshopRecommanded Start ========================================= -->
<?php $this->widget('EshopRecommanded');?> 
<!-- ========================================= EshopRecommanded Ends ========================================= -->
<script>
    function sendSubmit() 
	{
        var ItemReviewName= $('#ItemReview_name').val();
        var ItemReviewEmail= $('#ItemReview_email').val();
        var ItemReviewComment= $('#ItemReview_comment').val();
        var rating= $('.star-rating-applied').val();
        if(ItemReviewName==''){
            $("#ItemReview_name_em_").show();
            $('#nameAddClass').addClass('error');
            $("#ItemReview_name_em_").html('Name cannot be blank.');
        }
        if(ItemReviewEmail==''){
            $("#ItemReview_email_em_").show();
            $('#emailAddClass').addClass('error');
            $("#ItemReview_email_em_").html('Email cannot be blank.');
        }
        if(ItemReviewComment==''){
            $("#ItemReview_comment_em_").show();
            $('#contentAddClass').addClass('error');
            $("#ItemReview_comment_em_").html('Comment cannot be blank.');
        }
        var data=$("#item-Review").serialize();
        if(ItemReviewName!='' && ItemReviewEmail!='' && ItemReviewComment!='' ){
            $.ajax({
                type: 'POST',
                url: '<?php echo Yii::app()->createAbsoluteUrl("eshop/itemReview"); ?>',
                data:data,
                success:function(data){
                    $('#reviewStatus').show('slow');
                },
                error: function(data) { // if error occured
                    alert("Error occured.please try again");
                }
            });
        }
    }
</script>