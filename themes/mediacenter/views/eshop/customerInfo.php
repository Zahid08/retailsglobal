<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="col-md-9 form">
    <?php if(!empty($msg)) :?>
    <div class="notification note-success"><p><?php echo $msg;?></p></div>
    <?php endif; ?>
    <section style="margin: 0px;" class="section register inner-left-xs">
        <h2 class="bordered">Update <?php echo $modelCustomer->name;?> Inforamtion </h2>
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'customer-form',
            'enableAjaxValidation'=>true,
            'htmlOptions'=>array(
                'class'=>'login-form cf-style-1',
            )
        )); ?>
        <?php echo $form->errorSummary($modelCustomer); ?>
        <?php echo $form->hiddenField($modelCustomer,'custId', array('id'=>'custId','class'=>'le-input','readonly'=>'true', 'value'=>$modelCustomer->custId)); ?>
        <?php echo $form->hiddenField($modelCustomer,'custType', array('id'=>'custType','class'=>'le-input','value'=>Customer::STATUS_GENERAL)); ?>
        <?php echo $form->hiddenField($modelCustomer,'status', array('id'=>'status','value'=>Customer::STATUS_INACTIVE)); ?>

        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'title'); ?>
            <?php echo $form->dropDownList($modelCustomer,'title', UsefulFunction::getCustomerTitle(), array('class'=>'le-input','prompt'=>'Select Title')); ?>
            <?php echo $form->error($modelCustomer,'title'); ?>
        </div>

        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'name'); ?>
            <?php echo $form->textField($modelCustomer,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($modelCustomer,'name'); ?>
        </div>
        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'email'); ?>
            <?php echo $form->textField($modelCustomer,'email',array('class'=>'le-input','size'=>60,'maxlength'=>155)); ?>
            <?php echo $form->error($modelCustomer,'email'); ?>
        </div>
        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'addressline'); ?>
            <?php echo $form->textArea($modelCustomer,'addressline',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
            <?php echo $form->error($modelCustomer,'addressline'); ?>
        </div>
        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'city'); ?>
            <?php echo $form->textField($modelCustomer,'city',array('class'=>'le-input','size'=>60,'maxlength'=>150)); ?>
            <?php echo $form->error($modelCustomer,'city'); ?>
        </div>
        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'phone'); ?>
            <?php echo $form->textField($modelCustomer,'phone',array('class'=>'le-input','size'=>60,'maxlength'=>155)); ?>
            <?php echo $form->error($modelCustomer,'phone'); ?>
        </div>
        <div class="field-row">
            <?php echo $form->labelEx($modelCustomer,'gender'); ?>
            <?php echo $form->dropDownList($modelCustomer,'gender', UsefulFunction::getCustomerGender(), array('class'=>'le-input','prompt'=>'Select Gender')); ?>
            <?php echo $form->error($modelCustomer,'gender'); ?>
        </div>
        <div class="buttons-holder">
            <?php echo CHtml::submitButton('Update', array('class'=>'le-button hug')); ?>
        </div><!-- /.buttons-holder -->
        <?php $this->endWidget(); ?>
    </section><!-- /.register -->

</div>
