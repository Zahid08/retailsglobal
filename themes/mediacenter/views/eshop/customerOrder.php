<div class="col-xs-12 col-md-9 items-holder no-margin">
    <!-- /.cart-item starts-->
    <div id="cart_item_holder_21" class="row no-margin cart-item">
        <?php if(count($orderModel)>0) :?>
  
        <table class="tblWidth">
            <thead>
                <tr>
                    <th class="classSl" >Sl.No</th>
                    <th class="myorder" >Invoice No</th>
                    <th class="myorder" >Quantity</th>
                    <th class="myorder" >Price</th>
					<th class="myorder" >Shipping</th>
					<th class="myorder" >Discount</th>
                    <th class="myorder" >Date</th>	
					<th class="myorder" >Status</th>
                    <th class="myorder" >Details</th>
                </tr>
            </thead>
            <?php  foreach($orderModel as $keyOrder=>$orderValue) : ?>
            <tr>
                <td class="classSl" ><?php echo $keyOrder+1;?></td>
                <td class="myorder2" ><?php echo $orderValue->invNo;?></td>
                <td class="myorder2" ><?php echo $orderValue->totalQty;?></td>
                <td class="myorder2" ><?php echo Company::getCurrency().'&nbsp;'.($orderValue->totalPrice+$orderValue->totalTax);?></td>
				<td class="myorder2" ><?php echo Company::getCurrency().'&nbsp;'.ShippingMethod::getShippingMethodPrice($orderValue->shippingId,$orderValue->totalPrice);?></td>
				<td class="myorder2" ><?php echo Company::getCurrency().'&nbsp;'.$orderValue->totalDiscount;?></td>
                <td class="myorder2" ><?php echo date( "Y-m-d", strtotime($orderValue->orderDate) );?></td>
				<td class="myorder2" ><?php echo Lookup::item('Status',$orderValue->status);?></td>
                <td class="myorder2" >
                    <?php echo CHtml::link('View',array('eshop/customerOderDetails','id'=>$orderValue->id),array('class'=>'button hug'));?>
                </td>
            </tr>
            <?php endforeach;?>
        </table>
        <?php endif; ?>


    </div>
</div>