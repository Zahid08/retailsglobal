<div class="col-xs-12 col-sm-4 no-margin product-item-holder size-medium hover">
	<div class="product-item">
		<?php // mark up new + sales + best sales
			echo (Items::getIsNewItem($data->id))?'<div class="ribbon blue"><span>new!</span></div>':'';
			echo (Items::getIsSaleItem($data->id))?'<div class="ribbon red"><span>sale</span></div>':'';
			echo (Items::getIsBestSaleItem($data->id))?'<div class="ribbon green"><span>bestseller</span></div>':'';
		?>
		<div class="image">
			<?php if(!empty($data->itemImages)) : 
				echo CHtml::link('<img alt="" src="'.$data->itemImages[0]->image.'" data-echo="'.$data->itemImages[0]->image.'" style="width:194px;height:143px;" />',array('eshop/proDetails','id'=>$data->id),  array('title'=>$data->itemName));?>
			<?php endif;?>
        </div>
		<div class="body">
			<div class="title">
				<?php echo CHtml::link($data->itemName,array('eshop/proDetails','id'=>$data->id),  array('title'=>$data->itemName));?>
			</div>
			<div class="brand"><?php echo $data->brand->name;?></div>
		</div>
		<div class="prices">
			<?php if(Items::getItemWiseOfferPrice($data->id)>0) :?>
				<div class="price-prev" style="text-decoration: line-through;">
					<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($data->id);?>
				</div>
				<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($data->id);?></div>
			<?php else : ?>
				<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($data->id);?></div>
			<?php endif;?>
		</div>
		<div class="hover-area">
			<div class="add-cart-button">
				<?php echo CHtml::link('Add to cart',array('eshop/proDetails','id'=>$data->id),  array('class'=>'le-button'));?>
			</div>
			<div class="wish-compare">
				<a href="javascript:void(0);" id="<?php echo $data->id;?>" class="btn-add-to-wishlist" title="add to wishlist">Wishlist</a>
				<?php echo CHtml::link('Review & Rate',array('eshop/proReview','id'=>$data->id),  array('class'=>'btn-add-to-compare'));?>
			</div>
		</div>
	</div>
</div><!-- /.product-item-holder -->
                        