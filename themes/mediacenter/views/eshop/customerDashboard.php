<style>
    .ginfo_left{
        width: 300px;
        float: left;
    }
    .ginfo_right{
        width: 300px;
        float: left;
    }
    .generalInfo tr {
        height: 30px;
        line-height: 29px;
    }
    .generalInfo{
        margin-top: 25px;
    }
    .infoLeft{
        width: 80%;
        float: left;
    }
    .infoRight{
        width: 20%;
        float: left;
    }
</style>
<div class="col-xs-12 col-sm-8 col-md-9">
    <div id="hero">
        <?php if(!empty($model)):  ?>
            <div class="infoLeft">
                <div>
                    <h3>
                        General Information <?php echo $model->title.'.'.$model->name; ?>
                    </h3>
                </div>
                <table class="generalInfo">
                    <tr>
                        <td class="ginfo_left">User Name</td>
                        <td class="ginfo_right"><?php echo $modelUser->username; ?></td>
                    </tr>
                    <tr>
                        <td class="ginfo_left">Email</td>
                        <td class="ginfo_right"><?php echo $model->email; ?></td>
                    </tr>
                    <tr>
                        <td class="ginfo_left">Phone</td>
                        <td class="ginfo_right"><?php echo $model->phone; ?></td>
                    </tr>
                    <tr>
                        <td class="ginfo_left">Gender</td>
                        <td class="ginfo_right"><?php echo $model->gender; ?></td>
                    </tr>
                    <tr>
                        <td class="ginfo_left">Address</td>
                        <td class="ginfo_right"><?php echo $model->addressline; ?></td>
                    </tr>
                    <tr>
                        <td class="ginfo_left">City</td>
                        <td class="ginfo_right"><?php echo $model->city; ?></td>
                    </tr>
                </table>
            </div>
            <div class="infoRight">
                <div class="buttons-holder">
                    <?php echo CHtml::link('Update',array('eshop/CustomerInfo','id'=>$model->id),array('class'=>'le-button hug'));?>
                </div>
            </div>


        <?php  endif; ?>
    </div>
</div>