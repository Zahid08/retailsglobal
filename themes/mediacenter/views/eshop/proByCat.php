<?php
$minPrice =0;
$maxPrice = 500;
if(!empty($catModel))
{					 
 	$priceRange = Category::priceDynamic($catModel->id);
 	if(!empty($priceRange))
 	{
 		if(!empty($priceRange['minPrice']) && !empty($priceRange['maxPrice']))
 		{
 			$minPrice=$priceRange['minPrice'];
 			$maxPrice=$priceRange['maxPrice'];
 		}
 	}
} ?>

<script>
 $(function() {
	// brand filter
	$('.le-checkbox').click(function() 
	{
		var isCheckedBrand = '';
		$(".le-checkbox:checked").each(function (i) {
			isCheckedBrand+=$(this).val()+',';
		});
		isCheckedBrand = isCheckedBrand.slice(0,-1);
		var startPrice = parseFloat($("#startPrice").html()),
			endPrice = parseFloat($("#endPrice").html());
		
		// ajax call
		$.ajax({
			type: "POST",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
			data: {
				catId : <?php echo $catModel->id;?>,
				brand : isCheckedBrand,
				startPrice : startPrice,
				endPrice : endPrice
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#catGridFilterByAjax").html(data);
			},
			error: function() {}
		});   
	});
	
	// price filtering
	$('#priceFilter').slider({
		   range:true,
		   min: <?php echo $minPrice;?>,
		   max: <?php echo $maxPrice;?>,
		   step: 100,
		   value: [<?php echo $minPrice;?>, <?php echo $maxPrice;?>],
		   handle: "square",
	});
	 
	$('#priceFilter').slider().on('slide', function(event) {
		var isCheckedBrand = '';
		$(".le-checkbox:checked").each(function (i) {
			isCheckedBrand+=$(this).val()+',';
		});
		isCheckedBrand = isCheckedBrand.slice(0,-1);
		var startPrice = event.value[0],
			endPrice = event.value[1];
		
		$("#startPrice").html(startPrice);
		$("#endPrice").html(endPrice);
		
		// ajax call
		$.ajax({
			type: "POST",
			beforeSend : function() {
				$(".preloader").show();
			},
			url: "<?php echo Yii::app()->request->baseUrl; ?>/index.php/eshop/proByCatAjax/",
			data: {
				catId : <?php echo $catModel->id;?>,
				brand : isCheckedBrand,
				startPrice : startPrice,
				endPrice : endPrice
			},					
			success: function(data) {
				$(".preloader").hide();
				$("#catGridFilterByAjax").html(data);
			},
			error: function() {}
		}); 
	});
	
});
</script>

<!-- ========================================= BREADCRUMB ========================================= -->
<div class="animate-dropdown">
	<div id="breadcrumb-alt">
		<div class="container">
			<div class="breadcrumb-nav-holder minimal">
				<ul>
				<?php if(!empty($catModel)) : ?>
					<li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo $catModel->subdept->dept->name;?></a></li>
					<li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo $catModel->subdept->name;?></a></li>
					<li class="breadcrumb-item current"><a href="javascript:void(0);"><?php echo $catModel->name;?></a></li>
				<?php endif;?>
				</ul>
			</div>
		</div><!-- /.container -->
	</div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <!-- ========================================= SIDEBAR ========================================= -->
    <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">
        <!-- ========================================= PRODUCT FILTER ========================================= -->
		<div class="widget">
			<h1>Product Filters</h1>
			<div class="body bordered">
				<?php if(!empty($brandModel)) : ?>
				<div class="category-filter">
					<h2>Brands</h2><hr>
					<ul>
						<?php foreach($brandModel as $keyBrand=>$dataBrand) : ?>
							<li>
								<input class="le-checkbox" type="checkbox" value="<?php echo $dataBrand->brandId;?>" />
								<i class="fake-box"></i><label><?php echo $dataBrand->brand->name;?></label> 
								<span class="pull-right">(<?php echo Items::getItemsCountByBrand($dataBrand->brandId,$catModel->id);?>)</span>
							</li>
						<?php endforeach;?>
					</ul>
				</div><!-- /.category-filter -->
				<?php endif;?>
				
				<div class="price-filter">
					<h2>Price</h2>
					<hr>
					<div class="price-range-holder">
						<div id="priceFilter"><input type="text" class="price-slider" value="" /></div>
						<span class="min-max">
							Price: <?php echo Company::getCurrency();?> <span id="startPrice"><?php echo $minPrice;?></span> - <span id="endPrice"><?php echo $maxPrice;?></span>
						</span>
					</div>
				</div><!-- /.price-filter -->
			</div><!-- /.body -->
		</div><!-- /.widget -->
		 <?php
         	$this->widget('EshopSpecialOffersLeft');
         	$this->widget('EshopFeturedProductsLeft');
         ?>
	</div>
	<!-- ========================================= SIDEBAR : END ========================================= -->

	<!-- ========================================= CONTENT ========================================= -->
	<div class="col-xs-12 col-sm-9 no-margin wide sidebar">
		<section id="gaming">
			<div class="grid-list-products">
				<h2 class="section-title"><?php if(!empty($catModel)) echo $catModel->name;?></h2>
				<div class="product-grid-holder medium clearfix" id="catGridFilterByAjax">
            		<div class="col-xs-12 col-md-12 no-margin">
						<div class="row no-margin">
							<?php $this->widget('zii.widgets.CListView', array(
								'dataProvider'=>$dataProvider,					
								'ajaxUpdate'=>true,
								'enableSorting' => true,
								'enablePagination'=>true,
								'emptyText' => 'No records found.',
								//'summaryText' => "{start} - {end} of {count}",
								'template' => '{items} {pager}', // {summary} {sorter} 
								//'sorterHeader' => 'Sort by:',
								//'sortableAttributes' => array('title', 'price'),
								'itemView'=>'_proByCatGrid',
								//'htmlOptions'=>array('class'=>'clearfix'),
								'pager'=> array('cssFile'=>true, 'pageSize' => 1,
									'header'=>false,
									'class'          => 'CLinkPager',
									'firstPageLabel' => 'First',
									'prevPageLabel'  => '<',
									'nextPageLabel'  => '>',
									'lastPageLabel'  => 'Last',
									'htmlOptions'=>array('class'=>'pagination')
								  ),
							 )); ?>
						</div>
					</div><!-- /.col-xs-12 col-md-12 -->
				</div><!-- /.product-grid-holder -->
			</div><!-- /.grid-list-products -->
		</section><!-- /#gaming -->
	</div><!-- /.col -->
</div><!-- /.container -->