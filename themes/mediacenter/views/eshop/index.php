<div id="top-banner-and-menu">
	<div class="container">
        <div class="row">
		<div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
            <?php $this->widget('EshopLeftMenu');?>
        </div><!-- /.sidemenu-holder -->

		<div class="col-xs-12 col-sm-8 col-md-7 @homebanner-holder">
			<!-- ========================================== SECTION – HERO ========================================= -->
            <div id="hero">
            <?php
                if(!empty($sliderModel)) : ?>
				<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                <?php foreach($sliderModel as $keySlider=>$dataSlider) : ?>
                    <div class="item" style="background-image: url(<?php echo (!empty($dataSlider->image))?$dataSlider->image:''?>)">
                        <div class="container-fluid">
                            <div class="caption vertical-center text-left">
                                <?php /*<div class="big-text fadeInDown-1">
                                    <?php if(Items::getItemWiseOfferPrice($dataSlider->id)>0) :?>
                                        <div class="price-prev" style="text-decoration: line-through;">
                                             <?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($dataSlider->id);?>
                                        </div>
                                        <div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($dataSlider->id);?></div>
                                    <?php else : ?>
                                        <div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($dataSlider->id);?></div>
                                    <?php endif;?>
                                </div>

                                <div class="excerpt fadeInDown-2">
                                    <?php if(Items::getItemWiseOfferPrice($dataSlider->id)>0) echo 'SAVE&nbsp;'.Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferOriPrice($dataSlider->id);?>
                                </div>
                                <div class="small fadeInDown-2">
                                    <?php echo $dataSlider->brand->name;?><br>
                                    <?php if(Items::getItemWiseOfferPrice($dataSlider->id)>0) echo 'terms and conditions apply';?>
									
                                </div>  */ ?>
                                <div class="button-holder fadeInDown-3">
                                    <?php if(!empty($dataSlider->url)) : ?>
                                        <a href="<?php echo $dataSlider->url;?>" class="big le-button" title="<?php echo $dataSlider->title;?>">Shop Now</a>
                                    <?php else : ?>
                                        <a class="big le-button" href="javascript:void(0);"><?php echo $dataSlider->title;?></a>
                                    <?php endif;?>
								    <?php //echo CHtml::link($dataSlider->title,array('eshop/proDetails','id'=>$dataSlider->id),  array('class'=>'big le-button '));?>
                                </div>
                            </div><!-- /.caption -->
                        </div><!-- /.container-fluid -->  
					
                    </div><!-- /.item -->
               <?php endforeach;?>
			   </div><!-- /.owl-carousel -->
               <?php endif;?>
		  </div>
		<!-- ========================================= SECTION – HERO : END ========================================= -->			
		</div><!-- /.homebanner-holder -->
		
			<!-- ========================================= Top Promo Box ========================================= -->	
			<?php if(!empty($itemAdvertiseModel)) : ?>
			<div class="col-xs-12 col-sm-6 col-md-2">
				<div class="no-margin home-page-ad">
					<?php foreach($itemAdvertiseModel as $keyItemAd=>$dataItemAd) : ?>
						<div class="product-item clearfix">
							<div class="image">
								<?php if(!empty($dataItemAd->itemImages)) : 
									echo CHtml::link('<img alt="" src="'.$dataItemAd->itemImages[0]->image.'" data-echo="'.$dataItemAd->itemImages[0]->image.'" style="width:194px;height:143px;" />',array('eshop/proDetails','id'=>$dataItemAd->id),  array('title'=>$dataItemAd->itemName));?>
								<?php endif;?>
							</div>
							<div class="body">
								<div class="title">
									<?php echo CHtml::link($dataItemAd->itemName,array('eshop/proDetails','id'=>$dataItemAd->id),  array('title'=>$dataItemAd->itemName));?>
								</div>
								<div class="brand"><?php echo $dataItemAd->brand->name;?></div>
							</div>
							<div class="price">
								<?php if(Items::getItemWiseOfferPrice($dataItemAd->id)>0) :?>
									<div class="price-prev" style="text-decoration: line-through;">
										<?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($dataItemAd->id);?>
									</div>
									<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemWiseOfferPrice($dataItemAd->id);?></div>
								<?php else : ?>
									<div class="price-current pull-right"><?php echo Company::getCurrency().'&nbsp;'.Items::getItemPriceWithVat($dataItemAd->id);?></div>
								<?php endif;?>
							</div>
						</div><!-- /.product-item -->
					<?php endforeach;?>
				</div>
			</div>
			<?php endif;?>
			<!-- ========================================= End Top Promo Box ========================================= -->	
        </div>
	</div><!-- /.container -->
</div><!-- /#top-banner-and-menu -->
<!-- ========================================= HOME BANNERS ========================================= -->

<?php
	// widget part
    $this->widget('EshopAdvertisement',array('position'=>Advertisement::POSITION_TOP));
    $this->widget('EshopHomePageProductsTab');
    $this->widget('EshopBestSellers');
    $this->widget('EshopRecentlyViewed');
    $this->widget('EshopTopBrands',array('limit'=>20));
?>
