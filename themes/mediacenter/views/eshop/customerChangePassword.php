<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE TITLE & BREADCRUMB-->

<div class="col-md-9 form">
    <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?> 
    <section style="margin: 0px;" class="section register inner-left-xs">
        <h2 class="bordered">Change Password </h2>
        <?php echo CHtml::beginForm('','post',array('id'=>'customer-change-password','class'=>'cf-style-1'));?>
        <?php echo CHtml::errorSummary($form); ?>  
        
        <div class="field-row">
            <?php echo CHtml::activeLabelEx($form,'verifyoldpass'); ?>
            <?php echo CHtml::activePasswordField($form,'verifyoldpass' ,array('value'=>'','class'=>'le-input')); ?>
        </div>

        <div class="field-row">
            <?php echo CHtml::activeLabelEx($form,'password'); ?>
            <?php echo CHtml::activePasswordField($form,'password' ,array('value'=>'','class'=>'le-input')); ?>
        </div>
        <div class="field-row">
            <?php echo CHtml::activeLabelEx($form,'verifyPassword'); ?>
            <?php echo CHtml::activePasswordField($form,'verifyPassword' ,array('value'=>'','class'=>'le-input')); ?>
        </div>
        <div class="buttons-holder">
            <?php echo CHtml::submitButton('Update', array('class'=>'le-button hug')); ?>
        </div><!-- /.buttons-holder -->
        <?php echo CHtml::endForm(); ?>  
    </section><!-- /.register -->
</div>
