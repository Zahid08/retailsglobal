<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<!-- ========================================= BREADCRUMB ========================================= -->
<div class="animate-dropdown">
    <div id="breadcrumb-alt">
        <div class="container">
            <div class="breadcrumb-nav-holder minimal">
                <ul>
                    <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';?></a></li>
                    <li class="dropdown breadcrumb-item current"><a href="javascript:void(0);"><?php echo 'Contact Us'; ?></a></li>
                 </ul>
            </div>
        </div><!-- /.container -->
    </div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<div class="container">
    <?php if(Yii::app()->user->hasFlash('contact')): ?>
        <div class="row">    		
            <div class="col-sm-12">    			   			
                <div class="status alert alert-success"><?php echo Yii::app()->user->getFlash('contact'); ?></div>			 		
            </div>			
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-8 form">
            <section class="section leave-a-message" style="margin: 0px;">
                <h2 class="bordered">Leave a Message</h2>
                <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'contact-form',
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                        ),
                        'htmlOptions'=>array(
                            'class'=>'contact-form row',
                        ),
                    )); ?>
                    <?php echo $form->errorSummary($model); ?>
                    <div class="form-group col-md-6">
                        <?php echo $form->textField($model,'name',array('class'=>'le-input','placeholder'=>'Name','style'=>'height:40px;')); ?>
                        <?php echo $form->error($model,'name'); ?>
                    </div>
                    <div class="form-group col-md-6">			                
                        <?php echo $form->textField($model,'email',array('class'=>'le-input','placeholder'=>'Email','style'=>'height:40px;')); ?>
                        <?php echo $form->error($model,'email'); ?> 
                    </div>
                    <div class="form-group col-md-12">
                        <?php echo $form->textField($model,'subject',array('class'=>'le-input','placeholder'=>'Subject','style'=>'height:40px;')); ?>
                        <?php echo $form->error($model,'subject'); ?> 
                    </div>			          
                    <div class="form-group col-md-12">

                        <?php echo $form->textArea($model,'body',array('rows'=>'4','cols'=>'50','class'=>'le-input','placeholder'=>'Your Message Here')); ?>
                        <?php echo $form->error($model,'body'); ?>
                    </div> 

                    <div class="form-group col-md-6">	
                        <?php if(CCaptcha::checkRequirements()): ?>		              
                            <?php $this->widget('CCaptcha'); ?>
                            <?php echo $form->textField($model,'verifyCode',array('class'=>'le-input','style'=>'height:40px;')); ?>
                            <?php echo $form->error($model,'verifyCode'); ?>
                        <?php endif; ?> 
                    </div>		            

                    <div class="form-group col-md-6">			                
                        <?php echo CHtml::submitButton('Submit',array('class'=>'le-button huge pull-right')); ?>
                    </div>			            
              <?php $this->endWidget(); ?>
            </section><!-- /.leave-a-message -->
        </div><!-- /.col -->

        <div class="col-md-4">
            <section class="our-store section inner-left-xs">
                <h2 class="bordered">Our Store</h2>
                <address>
                    <p><?php echo $companyModel->name;?></p>
						<p><?php echo $branchModel->addressline.'&nbsp,'.$branchModel->city.'&nbsp,'.$branchModel->postalcode;?></p>
						<p>Mobile : <?php echo $branchModel->phone;?></p>
						<p>Email : <?php echo $companyModel->email;?></p>
                </address>
            </section><!-- /.our-store -->
        </div><!-- /.col -->

    </div><!-- /.row -->
</div><!-- /.container -->