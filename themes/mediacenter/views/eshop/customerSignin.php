<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" language="javascript">
$(function() 
{
	//-------enter press ------------//
	$(document).keypress(function(e) {
		if(e.which  == 13) 
		{
			$('#login-form').get(0).submit();
		}
	});
	
});
</script>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
<div class="animate-dropdown">
    <div id="breadcrumb-alt">
        <div class="container">
            <div class="breadcrumb-nav-holder minimal">
                <ul>
                    <li class="dropdown breadcrumb-item"><a href="javascript:void(0);"><?php echo 'Home';//echo $catModel->subdept->dept->name;?></a></li>
                    <li class="dropdown breadcrumb-item current"><a href="javascript:void(0);"><?php echo 'Signin'; ?></a></li>
                </ul>
            </div>
        </div><!-- /.container -->
    </div>
</div> <!-- ========================================= BREADCRUMB : END ========================================= -->

<!-- END PAGE TITLE & BREADCRUMB-->
<main class="inner-bottom-md" id="authentication">
    <div class="container">
        <div>
            <p>All star marked <span class="required" style="color:red">*</span> fields are mandatory.</p>
        </div>
        <div class="row">
            <div class="col-md-6 form">
				<section style="margin: 0px;" class="section sign-in inner-right-xs">
                    <h2 class="bordered">Forget Password</h2>
                    <?php if(!empty($msg)) echo '<div class="row">'.$msg.'</div>';?>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'forgot-login-form',
                        'enableAjaxValidation'=>true,                     
                        'action'=>Yii::app()->createUrl('/eshop/resetPassword'),
                        'htmlOptions'=>array(
                            'class'=>'login-form cf-style-1',
                            'role'=>'form'
                        )
                    )); ?>
                    <div class="field-row">                        
                        <label>User Name</label>                        
                        <?php echo CHtml::textField('username', '', array('class'=>'le-input',  'size'=>50, 'maxlength'=>50)); ?>
                        <?php echo CHtml::hiddenField('reset', ''); ?>
                    </div>
                    <div class="field-row">
                       Or
                    </div>
                    <div class="field-row">
                        <label>Email</label>                        
                        <?php echo CHtml::emailField('email', '',array('class'=>'le-input')); ?>
                    </div>
                    <div class="buttons-holder">
                        <?php echo CHtml::submitButton('Submit', array('class'=>'le-button hug')); ?>
                    </div><!-- /.buttons-holder -->
                    <?php $this->endWidget(); ?>
                </section><!-- /.sign-in -->
            </div><!-- /.col -->
            <div class="col-md-6 form">
                <section style="margin: 0px;" class="section register inner-left-xs">
                    <h2 class="bordered" style="width:50%;text-align:left; float:left;">Signin as Customer </h2>
                    <h4 style="width:50%;text-align:right; float:left; margin-top:11px;"> <?php echo CHtml::link('Click Here For Supplier Signin',array('eshop/login'),array('target'=>'_blank')) ?> </h4>
                    <div style="clear:both"></div>
                    <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'login-form',
                        'enableAjaxValidation'=>true,
                        'htmlOptions'=>array(
                            'class'=>'login-form cf-style-1',
                            'role'=>'form'
                        )
                    )); 
					echo $form->hiddenField($model,'branchId', array('value'=>Branch::getDefaultBranch(Branch::STATUS_ACTIVE))); ?>
                    <?php echo $form->errorSummary($model); ?>
                    <div class="field-row">
                        <?php echo $form->labelEx($model,'username'); ?>
                        <?php echo $form->textField($model,'username',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'username'); ?>
                    </div>
                    <div class="field-row">
                        <?php echo $form->labelEx($model,'password'); ?>
                        <?php echo $form->passwordField($model,'password',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'password'); ?>
                    </div>
                    <div class="buttons-holder">
                        <?php echo CHtml::submitButton('Signin', array('class'=>'le-button hug')); ?>
                    </div><!-- /.buttons-holder -->
                    <?php $this->endWidget(); ?>
                </section><!-- /.register -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main>
