<link href="<?php echo Yii::app()->baseUrl; ?>/css/form.css" rel="stylesheet" type="text/css"/>
<div class="animate-dropdown">
    <div id="breadcrumb-alt">
        <div class="container">
            <div class="breadcrumb-nav-holder minimal">
                <ul>
                    <li class="dropdown breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
                    <li class="dropdown breadcrumb-item current"><a href="javascript:void(0);"><?php echo $this->metaTitle;?></a></li>
                </ul>
            </div>
        </div><!-- /.container -->
    </div>
</div>

<main id="authentication" class="inner-bottom-md">
    <div class="container">
        <div class="row">
            <div class="col-md-6 form">
                <section class="section sign-in inner-right-xs" style="margin: 0px;">
                    <h2 class="bordered">Customer Review</h2>
                    <?php if(!empty($modelItemReview)):
                        $i=1;  foreach($modelItemReview as $reviewProductData):
                        ?>
                        <div class="comment-item">
                            <div class="row no-margin">
                                <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                                    <div class="avatar">
                                        <img alt="avatar" src="<?php echo Yii::app()->session['themesnames'];?>/images/default-avatar.jpg">
                                    </div><!-- /.avatar -->
                                </div><!-- /.col -->
                                <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                                    <div class="comment-body">
                                        <div class="meta-info">
                                            <div class="author inline">
                                                <?php echo CHtml::link($reviewProductData->name,array('#'),array('class'=>'bold'))?>
                                            </div>
                                            <div class="star-holder inline">
                                             <?php
                                                $rate=$reviewProductData['rating'];
                                                $this->widget('CStarRating',array(
                                                    'name'=>'rating'.$i,
                                                    'minRating'=>1,
                                                    'maxRating'=>5,
                                                    'starCount'=>5,
                                                    'value'=>$rate,
                                                    'readOnly'=>true,
                                                ));       $i++;
                                             ?>
                                            </div>
                                            <div class="date inline pull-right">
                                                <?php  echo date('d M Y', strtotime($reviewProductData->crAt));   ?>
                                            </div>
                                        </div><!-- /.meta-info -->
                                        <p class="comment-text">
                                            <?php echo $reviewProductData->comment; ?>
                                        </p><!-- /.comment-text -->
                                    </div><!-- /.comment-body -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.comment-item -->
                    <?php
                          endforeach;
                    endif; ?>
                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6 form">
                <section class="section register inner-left-xs" style="margin: 0px;">
                    <h2 class="bordered">Add Review</h2>
                    <?php if(!empty($msg)) :?>
                        <div class="notification note-success">
                            <p><?php echo $msg;?></p>
                        </div>
                    <?php endif; ?>
                    <div class="new-review-form">
                        <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'item-Review-Item',
                            'enableAjaxValidation'=>true,

                        )); ?>

                        <div class="row field-row">
                            <div id="nameAddClass" class="col-xs-12 col-sm-5 no-margin">
                                <?php echo $form->hiddenField($modelReview,'itemId', array('value'=>$id));?>
                                <?php echo $form->labelEx($modelReview,'name'); ?>
                                <?php echo $form->textField($modelReview,'name',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                <?php echo $form->error($modelReview,'name'); ?>
                            </div>
                            <div id="emailAddClass" class="col-xs-12 col-sm-7">
                                <?php echo $form->labelEx($modelReview,'email'); ?>
                                <?php echo $form->textField($modelReview,'email',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                                <?php echo $form->error($modelReview,'email'); ?>
                            </div>
                        </div><!-- /.field-row -->

                        <div class="field-row star-row">
                            <?php echo $form->labelEx($modelReview,'rating'); ?>
                            <?php //echo $form->textField($modelReview,'rating',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                            <?php
                            $this->widget('CStarRating',array(
                                'model'=>$modelReview,
                                'name'=>$modelReview['rating'],
                                'attribute'=>'rating',
                                'minRating'=>1, //minimal value
                                'maxRating'=>5,//max value
                                'starCount'=>5, //number of stars
                                'titles'=>array(
                                    '1'=>'Normal',
                                    '2'=>'Average',
                                    '3'=>'OK',
                                    '4'=>'Good',
                                    '5'=>'Excellent'
                                ),

                            ));
                            ?>
                            <?php echo $form->error($modelReview,'rating'); ?>
                        </div><!-- /.field-row -->
                        <div style="clear: both;"></div>
                        <div id="contentAddClass" class="field-row">
                            <?php echo $form->labelEx($modelReview,'comment'); ?>
                            <?php echo $form->textArea($modelReview,'comment',array('class'=>'le-input','size'=>60,'maxlength'=>255)); ?>
                            <?php echo $form->error($modelReview,'comment'); ?>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder" style="margin-top:10px;">
                            <?php echo CHtml::submitButton('SUBMIT', array('class'=>'le-button huge')); ?>
                        </div><!-- /.buttons-holder -->
                        <?php $this->endWidget(); ?><!-- /.contact-form -->
                    </div><!-- /.new-review-form -->
                 </section><!-- /.register -->

            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</main>

<style>
    #ItemReview_rating_em_{
        margin-top: 30px;
    }
</style>